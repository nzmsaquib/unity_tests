"""
Implements action listeners for the GUI buttons

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTLatLon as ASTLatLon
import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTMisc import SHOW_ERRORS, HIDE_ERRORS, HMSFORMAT, DECFORMAT
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

import Chap3.ChapEnums

def calculateListener(gui):
    """
    Handle a click on the Calculate button
    
    :param tkwidget gui: GUI object from which the convert request came
    """
    cen = Chap3.ChapEnums.CalculationType  # shorten to make typing easier!!
   
    calcToDo = gui.getCalcToDo()
    
    #**************** Time Conversions
    if (calcToDo == cen.LEAP_YEARS):
        calcLeapYear(gui) 
    elif (calcToDo == cen.CALENDAR_2_JD):
        calcCalendar2JD(gui) 
    elif (calcToDo == cen.JD_2_CALENDAR):
        calcJD2Calendar(gui)
    elif (calcToDo == cen.DAY_OF_WEEK):
        calcDayOfWeek(gui)
    elif (calcToDo == cen.DATE_2_DAYS_INTO_YEAR):
        calcDaysIntoYear(gui)
    elif (calcToDo == cen.DAYS_INTO_YEAR_2_DATE):
        calcDaysIntoYear2Date(gui)
 
    #**************** Local vs Greenwich
    elif (calcToDo == cen.LCT_2_UT):
        calcLCT2UT(gui)
    elif (calcToDo == cen.UT_2_LCT):
        calcUT2LCT(gui)
    elif (calcToDo == cen.UT_2_GST):
        calcUT2GST(gui)
    elif (calcToDo == cen.GST_2_UT):
        calcGST2UT(gui)
    elif (calcToDo == cen.GST_2_LST):
        calcGST2LST(gui)
    elif (calcToDo == cen.LST_2_GST):
        calcLST2GST(gui) 
       
    else:
        ASTMsg.errMsg("No menu item has been selected. Choose a menu item from\n" + 
                      "'Time Conversions' or 'Local vs Greenwich' and  try again.", "No Menu Item Selected")



#====================================================================
# Routines to do the actual work for the Time Conversions menu
#====================================================================

def calcCalendar2JD(gui):
    """
    Convert a calendar date to a Julian day number
    
    :param tkwidget gui: GUI object from which the request came
    """ 
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())
    
    prt.clearTextArea()
    
    # The "true" flag in isValidDate indicates that we will allow fractional days in the date
    # as well as 0 (so that we can find the JD for 1/0/2000)
    dateObj = ASTDate.isValidDate(strInput, SHOW_ERRORS, True)
    if (not dateObj.isValidDateObj()):
        return
    iYear = dateObj.getYear()
    iMonth = dateObj.getMonth()
    dDay = dateObj.getdDay()
    
    result = "Convert " + strInput
    if (dateObj.getYear() < 0):
        result = result + " BC"
    else:
        result = result + " AD"
    
    prt.setBoldFont(True)
    gui.printlnCond(result + " to a Julian day number", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()

    prt.setFixedWidthFont()
    
    gui.printlnCond("1. If the month is greater than 2, set y = year and m = month")
    gui.printlnCond("   else set y = year - 1 and m = month + 12")
    gui.printCond("   Since month = " + str(iMonth) + ", ")
    if (iMonth > 2):
        iY = iYear
        iM = iMonth
        gui.printlnCond("which is greater than 2, set y = " + str(iYear))
        gui.printlnCond("   and m = " + str(iM))
    else:
        iY = iYear - 1
        iM = iMonth + 12
        gui.printlnCond("which is not greater than 2, set y = year - 1 = " + str(iY))
        gui.printlnCond("   and m = month + 12 = " + str(iM))
    gui.printlnCond()
    
    gui.printlnCond("2. If the year is less than 0, set T = 0.75 else set T = 0")
    gui.printCond("   Since year = " + str(iYear) + " is ")
    if (iYear < 0):
        dT = 0.75
        gui.printlnCond("less than 0, set T = 0.75")
    else:
        dT = 0.0
        gui.printlnCond("not less than 0, set T = 0")
    gui.printlnCond()
    
    gui.printlnCond("3. Determine if the date is in the Gregorian Calendar")
    gui.printlnCond("   The date is Gregorian if it is later than Oct. 15, 1582.")
    gui.printlnCond("   (Compare YYYY.MMDDdd against 1582.1015. If YYYY.MMDDdd is")
    gui.printlnCond("   greater than or equal to 1582.1015, the date is Gregorian.)")

    dTemp = float(iYear) + (float(iMonth) / 100.0) + (dDay / 10000.0)
    bGregorian = (dTemp >= 1582.1015)
    gui.printCond("   Since YYYY.MMDDdd = " + ASTStr.strFormat("%04.06f", dTemp) + ", the date is ")
    if (not bGregorian):
        gui.printCond("not ")
    gui.printlnCond("Gregorian.")
    gui.printlnCond()
    
    gui.printlnCond("4. If the date is Gregorian then compute")
    gui.printlnCond("   A = FIX(y / 100) and B = 2 - A + FIX(A / 4).")
    gui.printlnCond("   If the date is not Gregorian, set A = 0 and B = 0")
    gui.printCond("   Since the date is ")
    if (bGregorian):
        dA = ASTMath.Trunc(float(iY) / 100.0)
        dB = 2.0 - dA + ASTMath.Trunc(dA / 4.0)
        gui.printlnCond("Gregorian, A = FIX(" + str(iY) + " / 100) = " + str(dA))
        gui.printlnCond("   and B = 2 - " + str(dA) + " + FIX(" + str(dA) + " / 4) = " + str(dB))
    else:
        dA = 0.0
        dB = 0.0
        gui.printlnCond("not Gregorian, A = 0 and B = 0")
    gui.printlnCond()
    
    JD = dB + ASTMath.Trunc(365.25 * iY - dT) + ASTMath.Trunc(30.6001 * (iM + 1)) + dDay + 1720994.5
    gui.printlnCond("5. Finally, JD = B + FIX(365.25*y - T) + FIX[30.6001*(m+1)] + day + 1720994.5")
    gui.printlnCond("               = " + str(dB) + " + FIX(365.25*" + str(iY) + "-" + str(dT) + 
                    ") + FIX[30.6001*(" + str(iM) + "+1)]" + " + " + str(dDay) + " + 1720994.5")
    gui.printlnCond("               = " + ASTStr.strFormat(ASTStyle.JDFormat, JD))
    gui.printlnCond()      
    
    result = strInput + " = " + ASTStr.strFormat(ASTStyle.JDFormat, JD) + " JD"
    gui.printCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()
   


def calcDayOfWeek(gui):
    """
    Calculate the day of the week
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())
    
    prt.clearTextArea()
    
    dateObj = ASTDate.isValidDate(strInput)
    if (not dateObj.isValidDateObj()):
        return
    
    result = ASTDate.dateToStr_obj(dateObj)
 
    prt.setBoldFont(True)
    gui.printlnCond("Determine what Day of the Week " + result + " falls on", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
 
    prt.setFixedWidthFont()
 
    JD = ASTDate.dateToJD_obj(dateObj)
    gui.printlnCond("1. Convert " + result + " to a Julian day number at 00:00:00 UT")
    gui.printlnCond("   " + result + " = " + ASTStr.strFormat(ASTStyle.JDFormat, JD) + 
                    " Julian day number at 00:00:00 UT")
    gui.printlnCond()

    dA = (JD + 1.5) / 7.0
    gui.printlnCond("2. Calculate A = (JD + 1.5) / 7 = (" + ASTStr.strFormat(ASTStyle.JDFormat, JD) + 
                    " + 1.5) / 7 = " + ASTStr.strFormat(ASTStyle.JDFormat, dA))
    gui.printlnCond()
    
    dB = 7.0 * ASTMath.Frac(dA)
    gui.printlnCond("3. Let B = 7 * FRAC(A) = 7 * FRAC(" + ASTStr.strFormat(ASTStyle.JDFormat, dA) + 
                    ") = " + ASTStr.strFormat("%.6f", dB))
    gui.printlnCond()
    
    N = int(ASTMath.Round(dB))
    gui.printlnCond("4. Finally, let N = ROUND(B) to get the day number for the day")
    gui.printlnCond("   of the week (0=Sunday, 1=Monday, etc.)")
    gui.printlnCond("   N = ROUND(B) = ROUND(" + ASTStr.strFormat("%.6f", dB) + ") = " + str(N))
    gui.printlnCond()

    if ((N < 0) or (N > 6)):
        ASTMsg.criticalErrMsg("Internal error - N is out of range")
        return
    
    result = result + " falls on " + ASTDate.daysOfWeek[N]
    gui.printCond("Therefore, " + result)

    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcDaysIntoYear(gui):
    """
    Calculate how many days into a year a specific date is
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())

    prt.clearTextArea()
    
    dateObj = ASTDate.isValidDate(strInput)
    if (not dateObj.isValidDateObj()):
        return
    iMonth = dateObj.getMonth()
    iDay = dateObj.getiDay()
    iYear = dateObj.getYear()
    result = ASTDate.dateToStr_obj(dateObj)
    
    prt.setBoldFont(True)
    gui.printlnCond("Determine the number of days into the year for " + result, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()

    gui.printlnCond("1. If this is a leap year, set T = 1 else set T = 2")
    gui.printCond("   " + result + " is ")
    if (ASTDate.isLeapYear(iYear)):
        T = 1
    else:
        T = 2
        gui.printCond("not ")
    gui.printlnCond("a leap year, so T = " + str(T))
    gui.printlnCond()

    N = ASTMath.Trunc(275.0 * iMonth / 9.0) - T * ASTMath.Trunc((iMonth + 9) / 12.0) + iDay - 30
    gui.printlnCond("2. The number of days into the year is given by the formula")
    gui.printlnCond("   N = FIX(275 * MONTH / 9) - T * FIX[(MONTH + 9) / 12] + DAY - 30")
    gui.printlnCond("     = FIX(275 * " + str(iMonth) + " / 9) - " + str(T) + " * FIX[(" + 
                    str(iMonth) + " + 9) / 12] + " + str(iDay) + " - 30 = " + str(N))
    gui.printlnCond()
    
    result = result + " is " + str(N) + " days into the year"
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcDaysIntoYear2Date(gui):
    """
    Calculate date when given the number of days into a year
    
    :param tkwidget gui: GUI object from which the request came 
    """
    prt = gui.getPrtInstance()
    strDOY = ASTStr.removeWhitespace(gui.getData1())
    strYear = ASTStr.removeWhitespace(gui.getData2())
    
    prt.clearTextArea()
    intObj = ASTInt.isValidInt(strDOY, HIDE_ERRORS)
    if (not intObj.isValidIntObj()):
        ASTMsg.errMsg("The number of days into the year is invalid - try again", "Days into Year")
        return
    N = intObj.getIntValue()
    if ((N < 0) or (N > 365)):
        ASTMsg.errMsg("The number of days into the year is out of range - try again", "Days into Year")
        return
    
    intObj = ASTInt.isValidInt(strYear, HIDE_ERRORS)
    if (not intObj.isValidIntObj()):
        ASTMsg.errMsg("The Year entered is invalid - try again", "Invalid Year")
        return
    iYear = intObj.getIntValue()

    prt.setBoldFont(True)
    gui.printlnCond("Determine the date that is " + str(N) + " days into the year " + str(iYear), CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1. If Year is a leap year, set A = 1523, otherwise set A = 1889")
    gui.printCond("   Since " + str(iYear) + " is ")
    if (ASTDate.isLeapYear(iYear)):
        iA = 1523
    else:
        iA = 1889
        gui.printCond("not ")
    gui.printlnCond("a leap year, A = " + str(iA))
    gui.printlnCond()
    
    iB = ASTMath.Trunc((N + iA - 122.1) / 365.25)
    gui.printlnCond("2. B = FIX[(N + A - 122.1) / 365.25] = FIX[(" + str(N) + 
                    " + " + str(iA) + " - 122.1) / 365.25]")
    gui.printlnCond("     = " + str(iB))
    gui.printlnCond()
    
    iC = N + iA - ASTMath.Trunc(365.25 * iB)
    gui.printlnCond("3. C = N + A - FIX(365.25 * B) = " + str(N) + " + " + 
                    str(iA) + " - FIX(365.25 * " + str(iB) + ")")
    gui.printlnCond("     = " + str(iC))
    gui.printlnCond()
    
    iE = ASTMath.Trunc(iC / 30.6001)
    gui.printlnCond("4. E = FIX(C / 30.6001) = FIX(" + str(iC) + " / 30.6001) = " + str(iE))
    gui.printlnCond()

    gui.printlnCond("5. If E < 13.5, then Month = E - 1, otherwise, Month = E - 13")
    if (iE < 13.5):
        iMonth = iE - 1
    else:
        iMonth = iE - 13
    gui.printlnCond("   Thus, Month = " + str(iMonth))
    gui.printlnCond()
    
    iDay = iC - ASTMath.Trunc(30.6001 * iE)
    gui.printlnCond("6. Finally, Day = C - FIX(30.6001 * E) = " + str(iC) + 
                    " - FIX(30.6001 * " + str(iE) + ") = " + str(iDay))
    gui.printlnCond()
    
    result = str(N) + " days into the year " + str(iYear) + " is the date " + \
             ASTDate.dateToStr_mdy(iMonth, iDay, iYear)
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcJD2Calendar(gui):
    """
    Convert a Julian day number to a calendar date
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())

    prt.clearTextArea()
    
    dTempObj = ASTReal.isValidReal(strInput, HIDE_ERRORS)
    if (not dTempObj.isValidRealObj()):
        ASTMsg.errMsg("Julian day number entered is invalid. Try again.", "Invalid JD")
        return

    JD = dTempObj.getRealValue()
    result = ASTStr.strFormat(ASTStyle.JDFormat, JD)
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " JD to a Calendar Date", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()

    gui.printlnCond("1.  Add 0.5 to the Julian day number")
    JD1 = JD + 0.5
    gui.printlnCond("    JD1 = JD + 0.5 = " + str(JD) + " + 0.5 = " + str(JD1))
    gui.printlnCond()
    
    iJD1 = ASTMath.Trunc(JD1)
    fJD1 = ASTMath.Frac(JD1)
    gui.printlnCond("2.  Set I = FIX(JD1) and F = FRAC(JD1)")
    gui.printlnCond("    Thus, I = FIX(" + str(JD1) + ") = " + str(iJD1))
    gui.printlnCond("    and F = FRAC(" + str(JD1) + ") = " + ASTStr.strFormat("%.5f", fJD1))
    gui.printlnCond()

    gui.printlnCond("3.  If I is larger than 2299160 set A = FIX[(I - 1867216.25) / 36524.25]")
    gui.printlnCond("    and set B = I + 1 + A - FIX(A / 4). Otherwise, set B = I.")
    gui.printlnCond()
    gui.printCond("    Since I is ")
    if (iJD1 > 2299160):
        iA = ASTMath.Trunc((iJD1 - 1867216.25) / 36524.25)
        iB = iJD1 + 1 + iA - ASTMath.Trunc(iA / 4.0)
        gui.printlnCond(" greater than 2299160,")
        gui.printlnCond("    A = FIX[(" + str(iJD1) + " - 1867216.25) / 36524.25] = " + str(iA))
        gui.printlnCond("    and B = " + str(iJD1) + " + 1 + " + str(iA) + 
                        " - FIX(" + str(iA) + " / 4) = " + str(iB))
    else:
        iB = iJD1
        gui.printlnCond(" not greater than 2299160, B = " + str(iB))
    gui.printlnCond()

    iC = iB + 1524
    gui.printlnCond("4.  Set C = B + 1524 = " + str(iB) + " + 1524 = " + str(iC))
    gui.printlnCond()
    
    iD = ASTMath.Trunc((iC - 122.1) / 365.25)
    gui.printlnCond("5.  Set D = FIX[(C - 122.1) / 365.25] = FIX[(" + str(iC) + " - 122.1) / 365.25]")
    gui.printlnCond("          = " + str(iD))
    gui.printlnCond()
    
    iE = ASTMath.Trunc(365.25 * iD)
    gui.printlnCond("6.  Set E = FIX(365.25 * D) = FIX(365.25 * " + str(iD) + ") = " + str(iE))
    gui.printlnCond()

    iG = ASTMath.Trunc((iC - iE) / 30.6001)
    gui.printlnCond("7.  Set G = FIX[(C - E) / 30.6001] = FIX[(" + str(iC) + " - " + str(iE) + ") / 30.6001]")
    gui.printlnCond("          = " + str(iG))
    gui.printlnCond()
    
    dDay = iC - iE + fJD1 - ASTMath.Trunc(30.6001 * iG)
    gui.printlnCond("8.  The day is given by Day = C - E + F - FIX(30.6001 * G)")
    gui.printlnCond("                            = " + str(iC) + " - " + str(iE) + 
                    " + " + ASTStr.strFormat("%5f", fJD1) + " - FIX(30.6001 * " + str(iG) + ")")
    gui.printlnCond("                            = " + str(dDay))
    gui.printlnCond()

    gui.printlnCond("9.  The month is given by month = G - 1 if G is less than 13.5")
    gui.printlnCond("    and month = G - 13 if G is greater than 13.5")
    gui.printCond("    Thus, since G is ")
    if (iG > 13.5):
        iMonth = int(iG - 13)
        gui.printlnCond("greater than 13.5, month = G - 13 = " + str(iG) + " - 13 = " + str(iMonth)) 
    else:
        iMonth = int(iG - 1)
        gui.printlnCond("less than 13.5, month = G - 1 = " + str(iG) + " - 1 = " + str(iMonth))  
    gui.printlnCond()

    gui.printlnCond("10. Finally, the year is given by year = D - 4716 if month is more than 2.5")
    gui.printlnCond("    and year = D - 4715 if month is less than 2.5")
    gui.printCond("    Thus, since month is ")
    if (iMonth > 2.5):
        iYear = int(iD - 4716)
        gui.printlnCond("more than 2.5, year = D - 4716 = " + str(iD) + " - 4716 = " + str(iYear))
    else:
        iYear = int(iD - 4715)
        gui.printlnCond("less than 2.5, year = D - 4715 = " + str(iD) + " - 4715 = " + str(iYear))
    gui.printlnCond()
    
    result = result + " JD = " + ASTDate.dateToStr_mdy(iMonth, dDay, iYear)
    gui.printCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcLeapYear(gui):
    """
    See if a year is a leap year
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())
    bLeapYear = False
    
    prt.clearTextArea()

    iObj = ASTInt.isValidInt(strInput) 
    if (not iObj.isValidIntObj()):
        return

    iYear = iObj.getIntValue()
    if (iYear < 1582):
        ASTMsg.errMsg("Year must be > 1581. Try again", "Invalid Year")
        gui.setResults("")
        return

    sfYear = ASTStr.strFormat("%d", iYear)
    result = "The year " + sfYear + " is "
    
    prt.setBoldFont(True)
    gui.printlnCond("Determine if " + sfYear + " is a leap year", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()

    gui.printlnCond("A year is a leap year if two conditions are satisfied:")
    gui.printlnCond("  (a) the year must be evenly divisible by 4")
    gui.printlnCond("  (b) if the year is a century year, it must also be evenly divisible by 400")
    gui.printlnCond()
    
    gui.printlnCond("1. Check condition 'a'")
    bPass = ASTMath.isEvenlyDivisible(iYear, 4)
    gui.printlnCond("   year / 4 = " + str(sfYear) + " / 4 = " + str(iYear / 4))
    gui.printCond("   So, " + str(sfYear) + " is ")
    if (not bPass):
        gui.printCond("not ")
    gui.printlnCond("evenly divisible by 4.")
    if (not bPass):
        gui.printlnCond("   No need to check any further.")
 
    if (bPass):
        gui.printlnCond()
        gui.printlnCond("2. Check condition 'b'. A year is a century year if it is evenly divisible by 100.")
        bPass = ASTMath.isEvenlyDivisible(iYear, 100)
    
        gui.printlnCond("   year / 100 = " + str(sfYear) + " / 100 = " + str(iYear / 100))
        gui.printCond("   So, " + str(sfYear) + " is ")
        if (not bPass):
            gui.printCond("not ")
            bLeapYear = True
        gui.printlnCond("evenly divisible by 100.")
        if (not bPass):
            gui.printlnCond("   No need to check any further since year is not a century year.")

    if (bPass):
        gui.printlnCond()
        gui.printlnCond("3. Since " + str(sfYear) + " is a century year, check if it is evenly divisible by 400.")
        bPass = ASTMath.isEvenlyDivisible(iYear, 400)
        gui.printlnCond("   year / 400 = " + str(sfYear) + " / 400 = " + str(iYear / 400))
        gui.printCond("   So, " + str(sfYear) + " is ")
        if (bPass):
            bLeapYear = True
        else:
            gui.printCond("not ")
            bLeapYear = False
        gui.printlnCond("evenly divisible by 400.")
    
    if (not bLeapYear):
        result = result + "not "
    result = result + "a leap year"
    gui.printlnCond()
    gui.printlnCond(result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()

#=================================================================
# Routines to do the actual work for the Local vs Greenwich menu
#=================================================================

def calcGST2LST(gui):
    """
    Convert GST to LST
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strData1 = ASTStr.removeWhitespace(gui.getData1())
    strLon = ASTStr.removeWhitespace(gui.getLongitudeData())
    
    prt.clearTextArea()
    
    timeObj = ASTTime.isValidTime(strData1)
    if (not timeObj.isValidTimeObj()):
        return
    dLonObj = ASTLatLon.isValidLon(strLon)
    if (not dLonObj.isLonValid()):
        return
    
    dLongitude = dLonObj.getLon()
    strtmp = ASTTime.timeToStr_obj(timeObj, HMSFORMAT)
    result = strtmp + " GST at Longitude " + strLon

    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to LST", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    GST = timeObj.getDecTime()
    gui.printlnCond("1. Convert the GST entered to decimal format. GST = " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST))
    gui.printlnCond()
    
    adjustment = dLongitude / 15.0
    gui.printlnCond("2. Calculate an adjustment for longitude.")
    gui.printlnCond("   ADJUST = (Longitude) / 15 = (" + str(dLonObj.getLon()) + ") / 15")
    gui.printlnCond("          = " + ASTStr.strFormat("%.6f",adjustment))
    gui.printlnCond()

    LST = GST + adjustment
    gui.printlnCond("3. LST = GST + ADJUST = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST) + " + (" +
                    ASTStr.strFormat("%.6f",adjustment) + ") = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LST))
    gui.printlnCond()
    
    gui.printlnCond("4. If LST is negative, add 24. If LST is greater than 24, subtract 24.")
    gui.printlnCond("   Otherwise, make no adjustment.")
    gui.printCond("   Thus, LST = ")
    if (LST < 0):
        LST = LST + 24
        gui.printCond(" LST + 24 = ")
    if (LST > 24):
        LST = LST - 24
        gui.printCond(" LST - 24 = ")
    gui.printlnCond(ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LST))
    gui.printlnCond()

    strtmp = ASTTime.timeToStr_dec(LST, HMSFORMAT)
    gui.printlnCond("5. Finally, convert LST to hours, minutes, seconds")
    gui.printlnCond("   So, LST = " + strtmp)
    gui.printlnCond()
    
    result = result + " = " + strtmp + " LST"
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcGST2UT(gui):
    """
    Convert GST to UT
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strData1 = ASTStr.removeWhitespace(gui.getData1())
    strData2 = ASTStr.removeWhitespace(gui.getData2())
    
    prt.clearTextArea()
    
    timeObj = ASTTime.isValidTime(strData1)
    if (not timeObj.isValidTimeObj()):
        return
    dateObj = ASTDate.isValidDate(strData2)
    if (not dateObj.isValidDateObj()):
        return
    
    iYear = dateObj.getYear()
    
    strtmp = ASTTime.timeToStr_obj(timeObj, HMSFORMAT)
    result = strtmp + " GST on " + ASTDate.dateToStr_obj(dateObj)
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to UT", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    JD = ASTDate.dateToJD_obj(dateObj)
    gui.printlnCond("1.  Convert given date to a Julian day number")
    gui.printlnCond("    Thus, " + ASTDate.dateToStr_obj(dateObj) + " = " + 
                    ASTStr.strFormat(ASTStyle.JDFormat,JD) + " JD")
    gui.printlnCond()

    JD0 = ASTDate.dateToJD_mdy(1, 0, iYear)
    gui.printlnCond("2.  Calculate the Julian day number for January 0.0 of the given year")
    gui.printlnCond("    Thus, JD0 = " + ASTDate.dateToStr_mdy(1, 0.0, iYear) + " = " +
                    ASTStr.strFormat(ASTStyle.JDFormat,JD0))
    gui.printlnCond()
    
    days = int(JD - JD0)
    gui.printlnCond("3.  Subtract step 2 from step 1 to get number of days into the year")
    gui.printlnCond("    This gives Days = " + str(days))
    gui.printlnCond()
    
    dT = (JD0 - 2415020.0) / 36525.0
    gui.printlnCond("4.  Let T = [(JD0 - 2415020.0) / 36525] = [(" + 
                    ASTStr.strFormat(ASTStyle.JDFormat,JD0) + " - 2415020.0) / 36525]")
    gui.printlnCond("          = " + ASTStr.strFormat("%.9f",dT))
    gui.printlnCond()

    dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
    gui.printlnCond("5.  Let R = 6.6460656 + T * 2400.051262 + T * T * 0.00002581")
    gui.printlnCond("          = 6.6460656+" + ASTStr.strFormat("%.9f",dT) + "*2400.05162" +
                    "+" + ASTStr.strFormat("%.9f",dT) + "*" + ASTStr.strFormat("%.9f",dT) + "*0.00002581")
    gui.printlnCond("          = " + ASTStr.strFormat("%.9f",dR))
    gui.printlnCond()
    
    dB = 24.0 - dR + 24.0 * (iYear - 1900)
    gui.printlnCond("6.  Let B = 24 - R + 24*(year - 1900) = 24 - " + ASTStr.strFormat("%.9f",dR) +
                    " + 24*(" + str(iYear) + " - 1900)")
    gui.printlnCond("          = " + ASTStr.strFormat("%.9f",dB))
    gui.printlnCond()
    
    T0 = 0.0657098 * (days) - dB
    gui.printlnCond("7.  Let T0 = 0.0657098*days - B = 0.0657098*" + str(days) + " - " +
                    ASTStr.strFormat("%.9f",dB))
    gui.printlnCond("           = " + ASTStr.strFormat("%.9f",T0))
    gui.printlnCond()

    gui.printlnCond("8.  If the result of step 7 is negative, add 24 hours. If the result of step 7")
    gui.printlnCond("    is greater than 24, subtract 24 hours. Otherwise, make no adjustment.")
    gui.printCond("    Thus, T0 = ")
    if (T0 < 0):
        T0 = T0 + 24
        gui.printCond(" T0 + 24 = ")
    gui.printlnCond(ASTStr.strFormat("%.9f",T0))
    gui.printlnCond()
    
    GST = timeObj.getDecTime()
    gui.printlnCond("9.  Convert the GST entered to decimal if HMS was entered. Thus, GST = " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST))
    gui.printlnCond()

    dA = GST - T0
    gui.printCond("10. Let A = GST - T0 = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST) +
                  " - " + ASTStr.strFormat("%.9f",T0))
    gui.printlnCond(" = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dA))
    gui.printlnCond()
    
    gui.printlnCond("11. If A is negative, add 24. Otherwise, make no adjustment.")
    gui.printCond("    Thus, A = ")
    if(dA < 0):
        dA = dA + 24
        gui.printCond(" A + 24 = ")
    gui.printlnCond(ASTStr.strFormat("%.9f",dA))
    gui.printlnCond()
    
    UT = dA * 0.99727
    gui.printlnCond("12. UT = A * 0.997270 = " + ASTStr.strFormat("%.9f",dA) +
                    " * 0.997270 = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT))
    gui.printlnCond()

    strtmp = ASTTime.timeToStr_dec(UT, HMSFORMAT)
    gui.printlnCond("13. Finally, convert UT to hours, minutes, seconds")
    gui.printlnCond("    So, UT = " + strtmp)
    gui.printlnCond()
    
    result = result + " = " + strtmp + " UT"
    gui.printlnCond("Therefore, " + result)

    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcLCT2UT(gui):
    """
    Convert LCT to UT
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())
    strLon = ASTStr.removeWhitespace(gui.getLongitudeData())
    dLonObj = ASTLatLon.ASTLatLon()
    dLongitude = 0.0
    saveTZone = ""
    dayChange = ""
    radbtn = gui.getSelectedRBStatus()
    
    prt.clearTextArea()
    
    timeObj = ASTTime.isValidHMSTime(strInput)
    if (not timeObj.isValidTimeObj()):
        return
    result = ASTTime.timeToStr_obj(timeObj,HMSFORMAT) + " LCT"

    # If necessary, validate the longitude
    if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE):
        dLonObj = ASTLatLon.isValidLon(strLon)
        if (not dLonObj.isLonValid()):
            return
        dLongitude = dLonObj.getLon()
        strLon = ASTLatLon.lonToStr_obj(dLonObj, DECFORMAT)
    
    if (gui.getDSTStatus()):
        saveTZone = "(DST, "
    else:
        saveTZone = "("
    
    if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE):
        saveTZone = saveTZone + "at Longitude " + strLon + ")"
    else:
        saveTZone = saveTZone + "in " + radbtn.toString() + " time zone)"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " " + saveTZone + " to UT", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    LCT = timeObj.getDecTime()
    gui.printlnCond("1. Convert LCT to decimal format. LCT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LCT))
    gui.printlnCond()
    
    gui.printlnCond("2. If necessary, adjust for daylight saving time.")
    if (gui.getDSTStatus()):
        decTime = LCT - 1
        gui.printlnCond("   Since this is on daylight saving time, subtract 1 hour.") 
    else:
        decTime = LCT
        gui.printlnCond("   No adjustment is necessary since this is not daylight saving time.")

    gui.printlnCond("   This gives T = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,decTime))
    gui.printlnCond()

    gui.printlnCond("3. Calculate a time zone adjustment")
    dAdjustment = __timeZoneAdjustment(gui,True, radbtn, dLongitude)
    gui.printlnCond()
    
    dT = decTime - dAdjustment
    gui.printlnCond("4. Subtract the time zone adjustment in step 3 from the")
    gui.printlnCond("   result of step 2 giving")
    gui.printlnCond("   UT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,decTime) +
                    " - (" + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dAdjustment) +
                    ") = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dT))
    gui.printlnCond()
 
    UT = dT
    gui.printlnCond("5. If the result of step 4 is negative, add 24. If the result of step 4")
    gui.printlnCond("   is greater than 24, subtract 24. Otherwise, make no further adjustments.")
    gui.printCond("   Thus, UT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT))
    if (dT > 24):
        UT = dT - 24.0
        dayChange = " (next day)"
        gui.printCond(" - 24 = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT) + dayChange)
    if (dT < 0):
        UT = dT + 24.0
        dayChange = " (previous day)"
        gui.printCond(" + 24 = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT) + dayChange)
    gui.printlnCond()
    gui.printlnCond()

    strtmp = ASTTime.timeToStr_dec(UT, HMSFORMAT)
    gui.printlnCond("6. Finally, convert to hours, minutes, seconds format, which gives")
    gui.printlnCond("   UT = " + strtmp)
    gui.printlnCond()
    
    result = result + " " + saveTZone + " = " + strtmp + " UT" + dayChange
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcLST2GST(gui):
    """
    Convert LST to GST
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strData1 = ASTStr.removeWhitespace(gui.getData1())
    strLon = ASTStr.removeWhitespace(gui.getLongitudeData())
    
    prt.clearTextArea()
    
    timeObj = ASTTime.isValidTime(strData1)
    if (not timeObj.isValidTimeObj()):
        return
    dLonObj = ASTLatLon.isValidLon(strLon)
    if (not dLonObj.isLonValid()):
        return
    dLongitude = dLonObj.getLon()
    strtmp = ASTTime.timeToStr_obj(timeObj, HMSFORMAT)
    result = strtmp + " LST at Longitude " + strLon

    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to GST", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    LST = timeObj.getDecTime()
    gui.printlnCond("1. Convert the LST entered to decimal format. LST = " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LST))
    gui.printlnCond()
    
    adjustment = dLongitude / 15.0
    gui.printlnCond("2. Calculate an adjustment for longitude.")
    gui.printlnCond("   ADJUST = (Longitude) / 15 = (" + str(dLonObj.getLon()) + ") / 15")
    gui.printlnCond("          = " + ASTStr.strFormat("%.6f",adjustment))
    gui.printlnCond()

    GST = LST - adjustment
    gui.printlnCond("3. GST = LST - ADJUST = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LST) + " - (" +
                    ASTStr.strFormat("%.6f",adjustment) + ") = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST))
    gui.printlnCond()
    
    gui.printlnCond("4. If GST is negative, add 24. If GST is greater than 24, subtract 24.")
    gui.printlnCond("   Otherwise, make no adjustment.")
    gui.printCond("   Thus, GST = ")
    if (GST < 0):
        GST = GST + 24
        gui.printCond(" GST + 24 = ")
    if (GST > 24):
        GST = GST - 24
        gui.printCond(" GST - 24 = ")
    gui.printlnCond(ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST))
    gui.printlnCond()
    
    strtmp = ASTTime.timeToStr_dec(GST, HMSFORMAT)
    gui.printlnCond("5. Finally, convert GST to hours, minutes, seconds")
    gui.printlnCond("   So, GST = " + strtmp)
    gui.printlnCond()
    
    result = result  +  " = "  +  strtmp  +  " GST"
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()        



def calcUT2GST(gui):
    """
    Converts UT to GST
    
    :param tkwidget gui: GUI object from which the request came
    """
    
    prt = gui.getPrtInstance()
    strData1 = ASTStr.removeWhitespace(gui.getData1())
    strData2 = ASTStr.removeWhitespace(gui.getData2())
    
    prt.clearTextArea()
    
    timeObj = ASTTime.isValidTime(strData1)
    if (not timeObj.isValidTimeObj()):
        return
    dateObj = ASTDate.isValidDate(strData2)
    if (not dateObj.isValidDateObj()):
        return
    
    iYear = dateObj.getYear()
    
    result = ASTTime.timeToStr_obj(timeObj, HMSFORMAT)+ " UT on " + ASTDate.dateToStr_obj(dateObj)
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to GST", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    JD = ASTDate.dateToJD_obj(dateObj)
    gui.printlnCond("1.  Convert given date to a Julian day number")
    gui.printlnCond("    Thus, " + ASTDate.dateToStr_obj(dateObj) + " = " + 
                    ASTStr.strFormat(ASTStyle.JDFormat,JD)  + " JD")
    gui.printlnCond()

    JD0 = ASTDate.dateToJD_mdy(1, 0, iYear)
    gui.printlnCond("2.  Calculate the Julian day number for January 0.0 of the given year")
    gui.printlnCond("    Thus, JD0 = " + ASTDate.dateToStr_mdy(1, 0.0, iYear) + " = " + 
                    ASTStr.strFormat(ASTStyle.JDFormat,JD0))
    gui.printlnCond()
    
    days = JD - JD0
    gui.printlnCond("3.  Subtract step 2 from step 1 to get number of days into the year")
    gui.printlnCond("    This gives Days = " + str(days))
    gui.printlnCond()
    
    dT = (JD0 - 2415020.0) / 36525.0
    gui.printlnCond("4.  Let T = [(JD0 - 2415020.0) / 36525] = [(" + 
                    ASTStr.strFormat(ASTStyle.JDFormat,JD0) + " - 2415020.0) / 36525]")
    gui.printlnCond("          = " + ASTStr.strFormat("%.9f",dT))
    gui.printlnCond()

    dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
    gui.printlnCond("5.  Let R = 6.6460656 + T * 2400.051262 + T * T * 0.00002581")
    gui.printlnCond("          = 6.6460656+" + ASTStr.strFormat("%.9f",dT) + "*2400.05162" +
                    "+" + ASTStr.strFormat("%.9f",dT) + "*" + ASTStr.strFormat("%.9f",dT) + "*0.00002581")
    gui.printlnCond("          = " + ASTStr.strFormat("%.9f",dR))
    gui.printlnCond()
    
    dB = 24.0 - dR + 24.0 * (iYear - 1900)
    gui.printlnCond("6.  Let B = 24 - R + 24*(year - 1900) = 24 - " + ASTStr.strFormat("%.9f",dR) +
                    " + 24*(" + str(iYear) + " - 1900)")
    gui.printlnCond("          = " + ASTStr.strFormat("%.9f",dB))
    gui.printlnCond()
    
    T0 = 0.0657098 * (days) - dB
    gui.printlnCond("7.  Let T0 = 0.0657098*days - B = 0.0657098*" + str(days) + " - " + 
                    ASTStr.strFormat("%.9f",dB))
    gui.printlnCond("           = " + ASTStr.strFormat("%.9f",T0))
    gui.printlnCond()
    
    UT = timeObj.getDecTime()
    gui.printlnCond("8.  Convert the UT entered to decimal if HMS was entered. Thus, UT = " +
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT))
    gui.printlnCond()
    
    GST = T0 + UT * 1.002738
    gui.printlnCond("9.  GST = T0 + UT * 1.002738 = " + ASTStr.strFormat("%.9f",T0) + " + " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT) +    " * 1.002738 = " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST))
    gui.printlnCond()

    gui.printlnCond("10. If GST is negative, add 24. If GST is greater than 24, subtract 24.")
    gui.printlnCond("    Otherwise, make no adjustment.")
    gui.printCond("    Thus, GST = ")
    if (GST < 0):
        GST = GST + 24
        gui.printCond(" GST + 24 = ")
    if (GST > 24):
        GST = GST - 24
        gui.printCond(" GST - 24 = ")
    gui.printlnCond(ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,GST))
    gui.printlnCond()
    
    strtmp = ASTTime.timeToStr_dec(GST, HMSFORMAT)
    gui.printlnCond("11. Finally, convert GST to hours, minutes, seconds")
    gui.printlnCond("    So, GST = " + strtmp)
    gui.printlnCond()
    
    result = result + " = " + strtmp + " GST"
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcUT2LCT(gui):
    """
    Convert UT to LCT
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    strInput = ASTStr.removeWhitespace(gui.getData1())
    strLon = ASTStr.removeWhitespace(gui.getLongitudeData())
    dLongitude = 0.0
    saveTZone = ""
    dayChange=""
    radbtn = gui.getSelectedRBStatus()
    
    prt.clearTextArea()
    
    timeObj = ASTTime.isValidTime(strInput)
    if (not timeObj.isValidTimeObj()):
        return
    result = ASTTime.timeToStr_obj(timeObj, HMSFORMAT) + " UT"
    
    # If necessary, validate the longitude
    if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE):
        dLonObj = ASTLatLon.isValidLon(strLon)
        if (not dLonObj.isLonValid()):
            return
        dLongitude = dLonObj.getLon()
        strLon = ASTLatLon.lonToStr_obj(dLonObj, DECFORMAT)
    
    if (gui.getDSTStatus()):
        saveTZone = "(DST, "
    else:
        saveTZone = "("
    
    if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE):
        saveTZone = saveTZone + "at Longitude " + strLon + ")"
    else:
        saveTZone = saveTZone + "in " + radbtn.toString() + " time zone)"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to LCT " + saveTZone, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()

    UT = timeObj.getDecTime()
    gui.printlnCond("1. Convert UT to decimal format. UT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT))
    gui.printlnCond()
    
    gui.printlnCond("2. Calculate a time zone adjustment")
    dAdjustment = __timeZoneAdjustment(gui,True, radbtn, dLongitude)
    gui.printlnCond()
    
    dT = UT + dAdjustment
    gui.printlnCond("3. Add the time zone adjustment from step 2 to the")
    gui.printlnCond("   result of step 1 giving")
    gui.printlnCond("   LCT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,UT) + " + (" + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dAdjustment) + ") = " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dT))
    gui.printlnCond()
    
    gui.printlnCond("4. If the result of step 3 is negative, add 24. If the result of step 3")
    gui.printlnCond("   is greater than 24, subtract 24. Otherwise, make no further adjustments.")
    gui.printCond("   Thus, LCT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dT))
    LCT = dT
    if (dT > 24):
        LCT = dT - 24.0
        dayChange = " (next day)"
        gui.printCond(" - 24 = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LCT) + dayChange)
    if (dT < 0):
        LCT = dT + 24.0
        dayChange = " (previous day)"
        gui.printCond(" + 24 = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LCT) + dayChange)
    gui.printlnCond()
    gui.printlnCond()
    
    gui.printlnCond("5. If necessary, adjust for daylight saving time.")
    if (gui.getDSTStatus()):
        LCT = LCT + 1
        gui.printlnCond("   Since this is on daylight saving time, add 1 hour.")
    else:
        gui.printlnCond("   No adjustment is necessary since this is not daylight saving time.")
    gui.printlnCond("   This gives LCT = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,LCT))
    gui.printlnCond()
    
    strtmp = ASTTime.timeToStr_dec(LCT, HMSFORMAT)
    gui.printlnCond("6. Finally, convert to hours, minutes, seconds format, which gives")
    gui.printlnCond("   LCT = " + strtmp)
    gui.printlnCond()
    
    result = result + " = " + strtmp + " LCT " + dayChange + " " + saveTZone
    gui.printlnCond("Therefore, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()     



#=====================================================================
# Private functions used only in this module
#=====================================================================

def __timeZoneAdjustment(gui,flag,radbtn,longitude):
    """
    Determine a time zone adjustment.
    
    :param tkwidget gui: GUI object from which the request came
    :param bool flag: if true, print out intermediate results
    :param TimeZoneType radbtn: radio button setting from the GUI
    :param float longitude: longitude value from the GUI
    :return:s a time zone adjustment
    """
    if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE):
        adjustment = ASTMath.Round((longitude / 15.0),0)
        if (flag):
            gui.printlnCond("   For a given longitude, the time zone adjustment factor is")
            gui.printlnCond("   determined by the formula")
            gui.printlnCond("   Adjustment = ROUND(Longitude / 15)")
            gui.printlnCond("              = ROUND(" + ASTStr.strFormat(ASTStyle.LATLONFORMAT,longitude) + " / 15)")
            gui.printlnCond("              = ROUND(" + ASTStr.strFormat(ASTStyle.LATLONFORMAT,longitude / 15.0) +
                            ") = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,adjustment))
    else:
        adjustment = radbtn.getAdjust()
        if (flag):
            gui.printlnCond("   For the " + radbtn.toString() +
                            " time zone, the time zone adjustment is " + 
                            ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,adjustment))

    return adjustment
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass 
