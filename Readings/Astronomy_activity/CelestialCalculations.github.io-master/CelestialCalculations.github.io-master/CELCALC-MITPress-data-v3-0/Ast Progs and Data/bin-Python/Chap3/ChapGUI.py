"""
Implements the main GUI for the application
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.scrolledtext as tkst

import ASTUtils.ASTAboutBox as ASTAboutBox
import ASTUtils.ASTBookInfo as ASTBookInfo
import ASTUtils.ASTLatLon as ASTLatLon
from ASTUtils.ASTMisc import centerWindow
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTPrt as ASTPrt
import ASTUtils.ASTStyle as ASTStyle

import Chap3.ChapEnums
import Chap3.GUIButtonsListener as gbl
import Chap3.ChapMenuItems as cmi
import Chap3.MenusListener as ml

class ChapGUI():
    """
    Define a class for this chapter's main application window.
    Although the code is written to allow multiple top level
    application windows, there really should only be one.
    """
    #==================================================
    # Class variables
    #==================================================
    WINDOW_TITLE = "Chapter 3 - Time Conversions"
    
    #==================================================
    # Class instance variables
    #==================================================
    # aboutCallback                   callback for the About Box menu item
    # exitCallback                    callback for the Exit menu item
    # instructionsCallback            callback for the Instructions menu item
    # radBtnCallback                  callback for changing the radio buttons
    # guiBtnsCallback                 callback for clicking on the Calculate button
    # aboutBox                        instance of an ASTAboutBox object
    # chkboxShowInterimCalcs          current state of the checkbox
    # lblResults                      label for showing the results
    # prt                             instance of an ASTPrt object for doing output
    # textPane                        the scrollable text output area
    
    # chkboxDST                       current state of the DST checkbox
    # txtboxData1                     text field for user input
    # lblData1                        label for text field 1
    # txtboxData2                     text field for user input
    # lblData2                        label for text field 2
    # txtboxLon                       text field when enter longitude rather than time zone
    # radBtns                         time zone radio buttons group dynamic variable
    # calculationToDo                 which calculation to do when Calculate button is selected
    
    def __init__(self,root):
        """
        Constructor for creating a GUI object in the root window.
        The root window (e.g., root = Tk()) must be created external
        to this object.

        :param tkwidget root: parent for this object
        """
        # To avoid an annoying 'flash' as widgets are added to the GUI,
        # we must hide it and then make it visible after all widgets have
        # been created.
        root.withdraw()
        
        # create dynamic variables for various widgets
        self.lblResults = tk.StringVar()
        self.chkboxShowInterimCalcs = tk.BooleanVar()
        self.chkboxDST = tk.BooleanVar()
        self.txtboxLon = tk.StringVar()
        self.txtboxData1 = tk.StringVar()
        self.lblData1 = tk.StringVar()
        self.txtboxData2 = tk.StringVar()
        self.lblData2 = tk.StringVar()
        self.radBtns = tk.IntVar()
        
        # define listeners for menu items
        self.aboutCallback = lambda: ml.aboutMenuListener(self)
        self.exitCallback = lambda: ml.exitMenuListener()
        self.instructionsCallback = lambda: ml.instructionsMenuListener(self.prt)
        self.guiBtnsCallback = lambda: gbl.calculateListener(self)
        
        # define a listener for changes to the radio buttons
        self.radBtnCallback = lambda: self.__radBtnCallback()
            
        # Initial size of the application window
        winWidth = 920
        winHeight = 800
           
        # create the root window and center it after all the widgets are created
        root.title(ChapGUI.WINDOW_TITLE)
        root.iconbitmap('BlueMarble.ico')
        root.geometry('{}x{}+{}+{}'.format(winWidth,winHeight,100,100))
        
        #=====================================================
        # create the menu bar
        #=====================================================
        # Note: In the code below, the menu font for cascades may not work because
        # tkinter conforms to the underlying OS's rules for the font and style for
        # top level menu items. If the underlying OS handles the font at the system
        # level (as MS Windows does), then any attempt in the code to change the menu font
        # will not work. However, setting the menu font for individual commands
        # does work.        
        
        menubar = tk.Menu(root)
        
        # create the File menu
        filemenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" File ",menu=filemenu)
        filemenu.add_command(label="Exit",command=self.exitCallback)

        # create the chapter menus
        cmi.createChapMenuItems(self,menubar)
                
        # create the Help menu
        helpmenu=tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" Help",menu=helpmenu)
        helpmenu.add_command(label="Instructions",command=self.instructionsCallback)
        helpmenu.add_command(label="About",command=self.aboutCallback)

        root.config(menu=menubar)
        
        #=====================================================
        # create the rest of the gui widgets
        #=====================================================
     
        # create the book title
        tk.Label(root,text=ASTBookInfo.BOOK_TITLE,font=ASTStyle.BOOK_TITLE_BOLDFONT,
                 fg=ASTStyle.BOOK_TITLE_COLOR,bg=ASTStyle.BOOK_TITLE_BKG).pack(fill=tk.X,expand=False)
                 
        # create the intermediate calculations and DST checkboxes
        f = tk.LabelFrame(root,padx=2,pady=2,bd=0)
        tk.Checkbutton(f,text="Show Intermediate Calculations",
                       variable=self.chkboxShowInterimCalcs,
                       onvalue=True,font=ASTStyle.CBOX_FONT).grid(row=0,column=0,padx=5)
        tk.Checkbutton(f,text="Daylight Saving Time",
                       variable=self.chkboxDST,
                       onvalue=True,font=ASTStyle.CBOX_FONT).grid(row=0,column=1,padx=5)
        f.pack()
                    
        # create the Time Zone grouping
        f = tk.LabelFrame(root,padx=2,pady=0,bd=2,relief=tk.RIDGE)
        tk.Label(f,text="Time Zone",font=ASTStyle.TEXT_BOLDFONT,justify=tk.CENTER).grid(row=0,column=0,columnspan=7)
        col = 0
        for zone in ASTLatLon.TimeZoneType:
            # Use the time zone adjustment as the value to be returned for the radio button
            tk.Radiobutton(f,text=zone.toString(),font=ASTStyle.RADBTN_FONT,
                           value=zone.getAdjust(),command=self.radBtnCallback,
                           variable=self.radBtns).grid(row=1,column=col,padx=2)
            col += 1
        tk.Entry(f,width=18,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxLon).grid(row=1,column=5,padx=2)
        tk.Label(f,text="(ex: 96.5E, 30.45W) ",font=ASTStyle.TEXT_FONT,justify=tk.LEFT).grid(row=1,column=6)               
        f.pack()
        
        # create the user input areas
        txtWidth = 30
        f = tk.LabelFrame(root,padx=5,pady=4,bd=0)
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxData1).grid(row=0,column=0,padx=2,pady=2)
        tk.Label(f,textvariable=self.lblData1,justify=tk.LEFT,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=0,column=1,sticky=tk.W)
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxData2).grid(row=1,column=0,padx=2,pady=2)
        tk.Label(f,textvariable=self.lblData2,justify=tk.LEFT,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=1,column=1,sticky=tk.W)
        f.pack(fill=tk.X,expand=False)
        
        # create the results label and calculate button
        f = tk.LabelFrame(root,padx=2,pady=2,bd=0)
        tk.Label(f,textvariable=self.lblResults,font=ASTStyle.TEXT_BOLDFONT,
                 justify=tk.LEFT,anchor="w").pack(side="left",padx=2)
        tk.Button(f,text="Calculate",width=12,font=ASTStyle.BTN_FONT,
                  command=self.guiBtnsCallback).pack(side="right",padx=15)
        f.pack(fill=tk.X,expand=False)
                                    
        # create the scrollable text area
        self.textPane = tkst.ScrolledText(root,font=ASTStyle.TEXT_FONT,padx=5,pady=2,borderwidth=5,wrap="word")
        self.textPane.pack(fill=tk.BOTH,expand=True)

        # center the window
        centerWindow(root,winWidth,winHeight)
        
        # Initialize the rest of the GUI
        ASTMsg.setParentFrame(root)
        self.prt = ASTPrt.ASTPrt(self.textPane)
        
        self.clearAllTextAreas()
        self.chkboxShowInterimCalcs.set(False)
        self.chkboxDST.set(False)
        self.txtboxLon.set("")
        
        self.setLonRadBtn()
        self.calculationToDo = Chap3.ChapEnums.CalculationType.NO_CALC_SELECTED
        
        # Finally, make the GUI visible and create a
        # reusable About Box. For some reason, tkinter requires
        # the parent window to be visible before the About Box
        # can be created.
        root.deiconify()
        self.aboutBox = ASTAboutBox.ASTAboutBox(root,ChapGUI.WINDOW_TITLE)
        
    #=====================================================================
    # The methods below conditionally print to the scrollable text area
    #=====================================================================
    
    def printCond(self,txt,center=False):
        """
        Print text to scrollable output area if the
        'Show Intermediate Calculations' checkbox
        is checked.

        :param str txt: text to be printed
        :param bool center: True if text is to be centered  
        """        
        if self.chkboxShowInterimCalcs.get():
            self.prt.printnoln(txt,center)

    def printlnCond(self,txt="",centerTxt=False):
        """
        Routines to handle sending output text to the scrollable
        output area. These are wrappers around ASTPrt.println
        that see if the 'Show Intermediate Calculations' checkbox
        is checked before invoking the println functions.

        :param str txt: text to be printed
        :param bool centerTxt: true if the text is to be centered     
        """       
        if self.chkboxShowInterimCalcs.get():
            self.prt.println(txt,centerTxt)

    #==================================================================================
    # Define various methods for manipulating the GUI
    #==================================================================================
    
    def clearAllTextAreas(self):
        """Clears all of the text areas in the GUI, except Longitude"""
        self.setDataLabels("","")
        self.prt.clearTextArea()
        self.setResults("")
        # Must also clear the input data areas
        self.txtboxData1.set("")
        self.txtboxData2.set("")
    
    def setDataLabels(self,s1,s2=""):
        """
        Sets data labels in the GUI
        
        :param str s1: string for the 1st data label in the GUI
        :param str s2: string for the 2nd data label in the GUI
        """
        self.lblData1.set(s1)
        self.lblData2.set(s2)
            
    def setResults(self,results):
        """
        Set the results label in the GUI.
        
        :param str results: string to be displayed in the results label
        """
        self.lblResults.set(results)
        
    #===============================================================================
    # The methods below are 'getters' and 'setters' for various items in the GUI.
    #===============================================================================
    
    def getData1(self):
        """Gets the string data associated with the 1st input area"""
        return self.txtboxData1.get()
    
    def getData2(self):
        """Gets the string data associated with the 2nd input area"""
        return self.txtboxData2.get()
    
    def getLongitudeData(self):
        """Gets the string the user entered for the Longitude"""
        return self.txtboxLon.get()
    
    def getDSTStatus(self):
        """
        Gets the current status of the DST check box
        
        :return: true if the DST check box is checked, else false
        """
        return self.chkboxDST.get()

    def getCalcToDo(self):
        """Gets the calculation to perform based upon the most recent menu selection."""
        return self.calculationToDo
    
    def setCalcToDo(self,calcToDo):
        """
        Saves the calculation to perform based upon the just selected menu item
        
        :param CalculationType calcToDo: which calculation to perform
        """
        self.calculationToDo = calcToDo      

    #==================================================================
    # Define functions to manage the time zone radio buttons.
    # Python manages radio buttons as a group to ensure that only
    # one radio button in a group is selected at a time. However,
    # we need routines to set/get a radio button status.
    #==================================================================
    
    def getSelectedRBStatus(self):
        """
        Determine what time zone radio button is selected
        
        :return: a time zone type for whatever is the currently selected time zone radio button
        """
        return ASTLatLon.intToTZType(self.radBtns.get())  
    
    def setLonRadBtn(self):
        """Sets the Longitude radio button"""
        self.txtboxLon.set("")
        # Pydev in the Eclipse IDE may show the next line as an error, but it is not.
        # The @UndefinedVariable is added to avoid the erroneous error message.
        self.radBtns.set(ASTLatLon.TimeZoneType.LONGITUDE.getAdjust())  # @UndefinedVariable
    
    def __radBtnCallback(self):
        """Handles changes to which Time Zone radio button is selected"""
        self.prt.clearTextArea()
        if (self.radBtns.get() == ASTLatLon.TimeZoneType.LONGITUDE.getAdjust()): # @UndefinedVariable
            self.setLonRadBtn()
        
    #============================================================================
    # The methods below return references to various GUI components such as the
    # scrollable output text area.
    #============================================================================        
        
    def getPrtInstance(self):
        """
        Gets the ASTPrt instance for this application's scrollable text pane area.
        
        :return: the ASTPrt object for printing to the scrollable text area
        """       
        return self.prt
    
    def showAboutBox(self):
        """Shows the About Box."""
        self.aboutBox.show()              
        


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
