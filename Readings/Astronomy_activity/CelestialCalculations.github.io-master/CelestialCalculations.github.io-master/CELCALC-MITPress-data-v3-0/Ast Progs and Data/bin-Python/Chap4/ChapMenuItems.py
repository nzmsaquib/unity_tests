"""
Create menu entries for this chapter's Coordinate System Conversions
and Kepler's Equation menus. This module also creates a
class and structure that associates data items with menu
entries to make it easier to determine what actions to 
take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap4.ChapEnums
import Chap4.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    


#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what type of conversion to do
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""    
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what type of calculation is to be done
    
    def __init__(self,eCalcType):
        """
        Create a ChapMenuItem instance
        
        :param CalculationType eCalcType: what type of calculation is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getCalcType(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createMenuItem(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what type of conversion is to be done
    :return: the menu object that got created
    """    
    # define a listener for responding to clicks on a menu item 
    menuItemCallback = lambda: ml.menuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=menuItemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: GUI object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """    
    cen = Chap4.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create Coordinate System Conversions menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Coord Systems ",menu=tmpmenu)  
    __createMenuItem(gui,tmpmenu, "Hour Angle to Right Ascension", cen.HA_RA)
    __createMenuItem(gui,tmpmenu, "Right Ascension to Hour Angle", cen.RA_HA)
    tmpmenu.add_separator()
    __createMenuItem(gui,tmpmenu, "Horizon to Equatorial", cen.HORIZON_EQUATORIAL)
    __createMenuItem(gui,tmpmenu, "Equatorial to Horizon", cen.EQUATORIAL_HORIZON)
    tmpmenu.add_separator()  
    __createMenuItem(gui,tmpmenu, "Ecliptic to Equatorial", cen.ECLIPTIC_EQUATORIAL)
    __createMenuItem(gui,tmpmenu, "Equatorial to Ecliptic", cen.EQUATORIAL_ECLIPTIC)
    tmpmenu.add_separator()  
    __createMenuItem(gui,tmpmenu, "Galactic to Equatorial", cen.GALACTIC_EQUATORIAL)
    __createMenuItem(gui,tmpmenu, "Equatorial to Galactic", cen.EQUATORIAL_GALACTIC)
    tmpmenu.add_separator()
    __createMenuItem(gui,tmpmenu, "Precession Correction", cen.PRECESSION_CORR)
    
    # Create Kepler's Equation menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Kepler's Equation ",menu=tmpmenu)      
    __createMenuItem(gui,tmpmenu, "Simple Iteration", cen.KEPLER_SIMPLE)
    __createMenuItem(gui,tmpmenu, "Newton/Raphson", cen.KEPLER_NEWTON)    
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass 
