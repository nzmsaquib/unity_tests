"""
Creates an "About Box" for the programs.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import os, inspect

import ASTUtils.ASTBookInfo as ASTBookInfo
from ASTUtils.ASTMisc import centerWindow
from ASTUtils.ASTPhoto import ABOUT_BOX_IMAGE
import ASTUtils.ASTStyle as ASTStyle

class ASTAboutBox():
    """
    Define a class for the program's About Box. Although the code
    is written to allow multiple About Box instances to be created,
    it only makes sense to create one. The design approach is
    to create an About Box when the main application GUI is created,
    but don't make the About Box visible until explicitly requested
    via the show method. Also, the About Box created is reusable and
    it is up to the routine that instantiates an About Box to destroy
    it. This isn't really burdensome because the About Box is
    automatically destroyed when the main program terminates.
    
    ======================= IMPORTANT NOTE ============================
    Apparently tkinter requires the parent frame to be visible before
    a child window can be created. So, the routine that instantiates
    an ASTAboutBox object must be visible (e.g., parent parameter to
    the constructor).
    ======================= IMPORTANT NOTE ============================
    """
    
    #==================================================================
    # Class variables
    #==================================================================
    # File containing the About Box image.
    __abtImageFilename = "PillarsOfCreation-EagleNebula200x200.gif"
    
    #==================================================================
    # Class instance variables
    #==================================================================
    # abtBoxImage         image to use in the About Box
    # parent              parent window for this object
    # root                top level window for this object
       
    def __init__(self,parent,chapter):
        """
        Constructor for creating an About Box as a child of the parent
        passed in.
        
        :param tkwidget parent: parent window for this object
        :param str chapter: title of the chapter for this about box
        """        
        caption = "'Pillars of Creation' in the Eagle Nebula taken in 2014 through the Hubble\n" +\
                  "Space Telescope. (Image courtesy of NASA/ESA/Hubble Heritage Team)"
                
        # Determine the directory where the currently executing script
        # is located. Note that os.getcwd returns the working directory
        # from which the python interpreter was executed, which is not
        # necessarily the same as code directory.      
        codeDir = os.path.realpath(os.path.abspath(
                            os.path.split(inspect.getfile(inspect.currentframe()))[0]))
        
        # If the image file exists, use it. Otherwise we'll default to
        # the image in ASTUtils.ASTPhoto. 
        imageFilename = os.path.join(codeDir,ASTAboutBox.__abtImageFilename)
        useImageFile = os.path.isfile(imageFilename)
      
        self.parent = parent
        abtBox = self.root = tk.Toplevel(parent)
        abtBox.transient(parent)
               
        # To avoid an annoying 'flash' as widgets are added to the About Box,
        # we must hide it and then make it visible after all widgets have
        # been created. The About Box will not be visible until the show
        # method is explicitly invoked.
        abtBox.withdraw()
        
        abtBox.title("About ...")
        tk.Label(abtBox,text=ASTBookInfo.BOOK_TITLE,font=ASTStyle.ABOUTBOOKTITLE_BOLDFONT,
                 fg=ASTStyle.BOOK_TITLE_COLOR,bg=ASTStyle.BOOK_TITLE_BKG).pack(fill=tk.X,expand=False)
        
        # Create a frame (f2Pane) to have 2 panes. The left pane will hold an image
        # while the right pane will hold info about the book.
        f2Pane = tk.LabelFrame(abtBox,padx=2,pady=2,bd=0)
        
        imgPane = tk.LabelFrame(f2Pane,bd=2)
        # Get the image from a file or from ASTPhoto. If we
        # read it from a file, resize to 200x200 if needed.
        if useImageFile:
            self.abtBoxImage = tk.PhotoImage(file=imageFilename)
            x = int(self.abtBoxImage.width()/200.0)
            y = int(self.abtBoxImage.height()/200.0)       
            if (x > 0) and (y > 0):       
                self.abtBoxImage = self.abtBoxImage.subsample(x,y)            
        else:
            self.abtBoxImage = tk.PhotoImage(data=ABOUT_BOX_IMAGE)       

        tk.Label(imgPane,image=self.abtBoxImage).pack()
        imgPane.grid(row=0,column=0)
        
        bookPane = tk.LabelFrame(f2Pane,bd=0)
        tk.Label(bookPane,width=25,text=ASTBookInfo.BOOK_EDITION,\
                 font=ASTStyle.ABOUTEDITION_BOLDFONT).pack(fill=tk.X,expand=False)
        tk.Label(bookPane,text=ASTBookInfo.BOOK_COPYRIGHT,font=ASTStyle.ABOUTCOPYRIGHT_FONT).pack(fill=tk.X,expand=False)
        tk.Label(bookPane,text=ASTBookInfo.BOOK_AUTHOR,font=ASTStyle.ABOUTAUTHOR_FONT,height=3).pack(fill=tk.X,expand=False)
        ff = tk.LabelFrame(bookPane,padx=4,pady=4,bd=4,relief=tk.SUNKEN,height=4)
        tk.Label(ff,text=chapter,font=ASTStyle.ABOUTCHAPTER_BOLDFONT).pack(fill=tk.X,expand=False)
        tk.Label(ff,text="(S/W Version "+ASTBookInfo.CODE_VER+")",
                 font=ASTStyle.ABOUTVERSION_FONT).pack(fill=tk.X,expand=False)
        ff.pack()
        bookPane.grid(row=0,column=1)
         
        f2Pane.pack()
       
        tk.Label(abtBox,text=caption,font=ASTStyle.ABOUTCAPTION_FONT,justify=tk.LEFT).pack(fill=tk.X,expand=False)

        tk.Button(abtBox,width=8,text="OK",font=ASTStyle.ABOUTBTN_FONT,
                  command=self.__okCallback).pack(pady=8)
        
        # Get window size so that we can center it. Have to do an update
        # first to be sure that all packing of widgets in the window is done
        abtBox.update()
        x = abtBox.winfo_width()
        y = abtBox.winfo_height()
        abtBox.update_idletasks()  
        abtBox.geometry(centerWindow(abtBox,x,y))
        
        # Set the icon and disable the ability to resize the About Box
        abtBox.iconbitmap('BlueMarble.ico')
        abtBox.resizable(0,0)
        
        # Install a handler in case the user clicks on the window frame's 'X'
        # button to close the window rather than the OK button that we created.
        # We'll use the OK button call back to handle it.
        abtBox.protocol("WM_DELETE_WINDOW",self.__okCallback)
         
    def __okCallback(self):
        """Handle when the user clicks on the OK button or the window frame's "X" decoration."""    
        # We **must** release the focus or else the main application will be hung!
        # Also, we'll just hide the window rather than destroy it.
        self.root.grab_release()
        self.root.withdraw()
        
    def show(self):
        """Make the About Box visible and grab focus to make the About Box modal."""      
        abtBox = self.root
        abtBox.deiconify()
        
        # Grab the focus to force the user to explicitly close the About Box.
        # Note that on Windows, when the About Box is visible, if the user iconifies
        # all windows, the main application cannot be de-iconified. The user will have
        # to de-iconify all windows (which on Windows is by clicking on the bottom right
        # of the screen, the 'Show Desktop' feature) so that the main application window
        # is de-iconified.
        abtBox.grab_set()
        abtBox.focus_set()        



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
