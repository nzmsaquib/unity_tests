"""
Defines miscellaneous constants and utility functions that are used 
throughout the programs. They are collected here to ensure uniformity
across all the programs. 

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

#=====================================================================
# Define various flags that indicate what to do in several of the
# utility functions.
#=====================================================================
ABORT_PROG = True                   # Whether to abort the program if an error is detected.
SHOW_ERRORS = True                  # Show errors when doing validations.
HIDE_ERRORS = not SHOW_ERRORS       # Don't show errors when doing validations. The invoking routine will display its own messages.
HMSFORMAT = True                    # Display time in HMS format
DMSFORMAT = HMSFORMAT               # Display angles in DMS format
DECFORMAT = not HMSFORMAT           # Display time/angles in decimal format
POS_DIRECTION = True                # Direction is positive (e.g., North, East)
NEG_DIRECTION = not POS_DIRECTION   # Direction is negative (e.g., South, West)
ASCENDING_ORDER = True              # Sort lists in ascending order
DESCENDING_ORDER = not ASCENDING_ORDER



#============================================================
# Define various constants related to astronomy
#===========================================================*/
AST_UNDEFINED = 99999999.999999         # Value indicating that something is undefined
DEFAULT_EPOCH = 2000.0                  # Default Epoch to use when none is specified.
UNKNOWN_RA_DECL = AST_UNDEFINED         # Value to use when RA or Decl is unknown. Any 
                                        # object with this RA or Decl value **must** be ignored!
UNKNOWN_mV = AST_UNDEFINED              # Value to use when the visual magnitude is unknown
mV_NAKED_EYE = 6.5                      # Approximate visual magnitude limit for objects visible with the naked eye
mV_BINOCULARS = 10.0                    # Approximate visual magnitude limit for objects visible with binoculars
mV_10INCH = 14.7                        # Approximate visual magnitude limit for objects visible with a 10-inch telescope



#=======================================================================
# Define various constants related to creating star charts. These
# values are likely to need to be adjusted (or computed dynamically)
# to match a particular computer's graphics resolution.
#=======================================================================
    
# Define radius of a dot to represent a magnitude 0 object and the max/min size of dots.
mV0RADIUS = 5               # radius when vis magnitude is 0 
mVMAX_RADIUS = 12           # max radius for any vis magnitude
mVMIN_RADIUS = 2            # min radius for any vis magnitude

# Since star catalogs can be quite large, it may take a long time to plot all objects.
# The MAX_OBJS_TO_PLOT constant is the recommended maximum number of objects to plot
# before asking the user if they want to continue. However, if there are OBJS_LIMIT or
# less, this limit is ignored since the objects will still plot in a reasonable amount of time.
# The proper value of these constants depends upon a particular computer's CPU speed
# and its graphics card speed.
MAX_OBJS_TO_PLOT = 50000                # max number of objects to plot
OBJS_LIMIT = 75000                      # max # of objects before worrying about needing to pause



#=======================================================================
# Miscellaneous useful routines
#=======================================================================

def centerWindow(win,width=0,height=0):
    """
    Compute the geometry necessary to center a window on the screen.
    
    :param tkwidget win: window that is to be centered
    :param int width: width of the window; if 0, calculate it
    :param int height: height of the window; if 0, calculate it
    """
    win.update_idletasks()
    if (width == 0):
        winWidth = win.winfo_reqwidth()
    else:
        winWidth = width
    if (height == 0):
        winHeight = win.winfo_reqheight()
    else:
        winHeight = height
    
    frm_width = win.winfo_rootx() - win.winfo_x()
    titlebar_height = win.winfo_rooty() - win.winfo_y()
    x = (win.winfo_screenwidth() - winWidth - frm_width) // 2
    y = (win.winfo_screenheight() - winHeight - titlebar_height) // 2
    win.geometry("{}x{}+{}+{}".format(winWidth,winHeight,x,y))



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
