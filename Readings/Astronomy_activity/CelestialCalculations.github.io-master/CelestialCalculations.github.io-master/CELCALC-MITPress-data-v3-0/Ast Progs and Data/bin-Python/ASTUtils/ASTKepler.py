"""
Defines functions to solve Kepler's equation via a simple iteration
scheme and by the Newton/Raphson method.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

from enum import Enum, auto
import math

import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle

# Default max iterations and minimum termination criteria for Kepler's equation
KeplerMaxIterations = 2500
KeplerMinCriteria = 0.00000001



class TrueAnomalyType(Enum):
    """
    Define possible ways to determine the true anomaly. Solving the equation of
    the center is specific to the Sun and Moon.
    """
    SOLVE_EQ_OF_CENTER = auto()               # Solve equation of the center
    SOLVE_SIMPLE_ITERATION_KEPLER = auto()    # Solve Kepler's equation by simple iteration
    SOLVE_NEWTON_METHOD_KEPLER = auto()       # Solver Kepler's equation by Newton's method



def calcSimpleKepler(dMA,dEcc,dTerm,prt=None):
    """
    Solve Kepler's equation via a simple iteration method.
    
    :param float dMA: mean anomaly in degrees
    :param float dEcc: orbital eccentricity
    :param float dTerm: termination criteria in radians
    :param ASTPrt prt: object that is the caller's text output area; if
                       null, no interim results are displayed.
    :return: a list with the eccentric anomaly in degrees in element 0
             and the number of iterations required to solve
             Kepler's equation in element 1
    """
    result = [0.0]*2
    
    # Validate the termination criteria
    if (dTerm < KeplerMinCriteria):
        dTerm = KeplerMinCriteria
    
    __printlncond(prt, "    Use Simple Iteration to solve Kepler's equation")
    __printlncond(prt, "    Iterate until difference is less than " + str(dTerm) +
                  " radians (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, ASTMath.rad2deg(dTerm) * 3600.0) +
                  " arcseconds)")
    __printlncond(prt)
    
    dMARad = ASTMath.deg2rad(dMA)
    dEA = dMARad
    __printlncond(prt, "      E0 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dEA))
    
    for iteration in range(1,KeplerMaxIterations + 1):
        # Note that the sine function here uses radians, not degrees!
        dEANext = dMARad + dEcc * math.sin(dEA)
        delta = abs(dEANext - dEA)
        __printlncond(prt, "      E" + str(iteration) + " = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEANext) + 
                      ", Delta = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, delta))
        dEA = dEANext
        if (delta <= dTerm):
            break
    
    if (iteration >= KeplerMaxIterations):
        __printlncond(prt, "WARNING: Did not converge, so stopped after " + str(KeplerMaxIterations) + " iterations.")
    __printlncond(prt)
    
    dEA = ASTMath.rad2deg(dEA)
     
    result[0] = dEA
    result[1] = iteration
    return result



def calcNewtonKepler(dMA,dEcc,dTerm,prt=None):
    """
    Solve Kepler's equation via the Newton/Raphson method.
    
    :param float dMA: mean anomaly in degrees
    :param float dEcc: orbital eccentricity
    :param float dTerm: termination criteria in radians
    :param ASTPrt prt: object that is the caller's text output area; if
                       null, no interim results are displayed.
    :return: a list with the eccentric anomaly in degrees in element 0
             and the number of iterations required to solve
             Kepler's equation in element 1
    """
    result = [0.0]*2
    
    # Validate the termination criteria
    if (dTerm < KeplerMinCriteria):
        dTerm = KeplerMinCriteria
    
    __printlncond(prt, "    Use the Newton/Raphson method to solve Kepler's equation")
    __printlncond(prt, "    Iterate until difference is less than " + str(dTerm) + " radians (" +
                  ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTMath.rad2deg(dTerm) * 3600.0) + " arcseconds)")
    __printlncond(prt)
    
    dMARad = ASTMath.deg2rad(dMA)
    if (dEcc < 0.75):
        dEA = dMARad
    else:
        dEA = math.pi
    __printlncond(prt, "      E0 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEA))
    
    for iteration in range(1,KeplerMaxIterations + 1):
        # Note that the sine & cosine functions here uses radians, not degrees!
        dEANext = dEA - (dEA - dEcc * math.sin(dEA) - dMARad) / (1 - dEcc * math.cos(dEA))
        delta = abs(dEANext - dEA)
        __printlncond(prt, "      E" + str(iteration) + " = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEANext) +
                      ", Delta = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,delta))
        dEA = dEANext
        if (delta <= dTerm):
            break
    
    if (iteration >= KeplerMaxIterations):
        __printlncond(prt, "WARNING: Did not converge, so stopped after " + str(KeplerMaxIterations) + " iterations.")
    __printlncond(prt)
    
    dEA = ASTMath.rad2deg(dEA)
     
    result[0] = dEA
    result[1] = iteration
    return result



#============================================================
# Some common helper routines used only in this module
#============================================================

def __printlncond(prt,txt=""):
    """
    Provide some routines that allow printing text messages to
    a calling routine's scrollable text output area. These functions
    are provided to avoid tying this Kepler class to a particular
    chapter's GUI, but at the cost of the calling routine having to pass
    a prt reference.

    :param ASTPrt prt: object that is the caller's output area
    :param str txt: text to be displayed in the output area
    """
    if (prt != None):
        prt.println(txt)



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
