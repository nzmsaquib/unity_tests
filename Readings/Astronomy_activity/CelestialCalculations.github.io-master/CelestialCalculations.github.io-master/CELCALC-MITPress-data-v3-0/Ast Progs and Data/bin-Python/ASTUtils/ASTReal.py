"""
Provides a class for creating real objects and validating that a string has a real value.

This class is useful because Python passes all parameters by value. Hence, in Python
one cannot have a statement such as

    bValid = isValidReal(inputStr, dResult)
    
where the isValidReal method returns a validity (true/false) and
modifies dResult to have the value parsed from inputStr.
Passing objects rather than primitives such as double avoids this problem.
With Python, one could also return tuples or lists, but creating an object class
makes it easier to port to other languages, such as Java.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

from ASTUtils.ASTMisc import SHOW_ERRORS
import ASTUtils.ASTMsg as ASTMsg

class ASTReal():
    """Defines a class for real numbers."""
    #===============================================================
    # Class instance variables
    #===============================================================
    # bValid                    true if the object represents a valid real number
    # dValue                    the object's value
    
    #================== IMPORTANT NOTE ==========================
    # Since Python does not do type checking until runtime, it
    # is possible to store data other than real values in an
    # ASTReal object. So, Python is perfectly happy to do
    # something like
    #    obj.setRealValue("Str, not a number!")
    # without complaining. This will create problems if we
    # then do a math expression such as
    #    x = obj.getRealValue() + 5
    # Attempts are made to prevent such errors, but they
    # may occur, so the calling routine must be careful!
    #================== IMPORTANT NOTE ==========================    
    
    def __init__(self,**kwargs):
        """
        Since much of the usage of this class involves validating that a string has a valid
        real number, assume a new object is invalid until it is proven otherwise.
        
        :param **kwargs: optional 'value=float' keyword
        """
        self.bValid = False
        self.dValue = 0.0
        
        if ('value' in kwargs):
            x = kwargs['value']
            if isinstance(x,(int,float)):
                self.bValid = True
                self.dValue = x
                    
    #========================================================
    # Define 'get/set' accessors for the object's fields
    #========================================================
    def isValidRealObj(self):
        return self.bValid
    def getRealValue(self):
        return self.dValue
    def setRealValue(self,r):
        if isinstance(r,(int, float)):
            self.bValid = True
            self.dValue = r
        else:
            self.bValid = False
            self.dValue = 0.0
     


#============================================================================
# Define a function for validating that a string has a valid real in it
#============================================================================
    
def isValidReal(inputStr,flag=SHOW_ERRORS):
    """
    Checks to see if a valid number was entered, but don't display
    any error messages unless flag is SHOW_ERRORS to let the calling 
    routine display its own error messages.

    :param str inputStr: string to be validated
    :param bool flag: SHOW_ERRORS if error msgs are to be displayed
    :return: an ASTReal object containing the result
    """
    resultObj = ASTReal()
        
    try:
        resultObj.dValue = float(inputStr)
        resultObj.bValid = True
    except ValueError:
        resultObj.bValid = False
        ASTMsg.errMsg("Either nothing was entered or what was entered\n" +
                      "is invalid. Please enter a valid number.","Invalid Number",flag);
            
    return resultObj



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
