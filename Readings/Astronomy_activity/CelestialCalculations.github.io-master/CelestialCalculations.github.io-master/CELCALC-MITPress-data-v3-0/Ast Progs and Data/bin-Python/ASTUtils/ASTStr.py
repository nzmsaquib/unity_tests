"""
Provides several string and character manipulation utilities.


These routines are provided to simplify translating the code
to other languages.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

def abbrevRight(strIn,maxlen):
    """
    Abbreviates a string on the right, if necessary.
    
    :param str strIn: input string
    :param int maxlen: maximum length not counting ellipses
    :return: rightmost maxlen characters of the input string
             along with ellipses if necessary
    """
    k = len(strIn)
    if (k >= maxlen):
        stmp = "... " + strIn[k-maxlen:k]
    else:
        stmp = strIn
    return stmp



def compareAlphaNumeric(str1,str2):
    """
    Compares two strings that may contain alphanumeric characters so that a string such
    as 'A9' is "less" than the string 'A11'. If the strings are strictly numeric, a numeric
    comparison is done. Alphabetic comparisons are not case sensitive.
    
    Note that this method only supports integers (e.g., numbers such as 5.9 vs 3.01 may not
    compare as expected) and that the maximum size integer in the string is limited to that
    which can be represented by a Python int data type.

    :param str str1: First string to compare.
    :param str str2: Second string to compare.
    :return:  -1 if str1 is "less than" str2, 0 if str1 is "equal" to str2, and 1
              if str1 is "greater" than str2.
    """
    idx1 = 0
    idx2 = 0
    len1 = len(str1)
    len2 = len(str2)
    result = 0
    
    # The strategy is to recursively extract a numeric or alphabetic substring
    # from both strings and compare them. We have to do this until we reach
    # the end of a string or until two substrings are not equal
    
    while ((idx1 < len1) and (idx2 < len2)):
        s1 = extractAlphaNumSubstr(str1,idx1)
        idx1 += len(s1)
        
        s2 = extractAlphaNumSubstr(str2,idx2)
        idx2 += len(s2)
        
        if (s1[0].isdigit() and s2[0].isdigit()):   # Compare as numbers
            n1 = float(s1)      # compare as float since we can't be sure they are integers
            n2 = float(s2)
            if (n1 < n2):
                result = -1
            elif (n1 > n2):
                result = 1
            else:
                result = 0
        else:
            if (s1.lower() < s2.lower()):
                result = -1
            elif (s1.lower() > s2.lower()):
                result = 1
            else:
                result = 0
        
        if (result != 0):      # We only need to continue comparison if they're still equal
            if (result < 0):
                return -1
            else:
                return 1
    
    # If we get here, either the strings are truly equal, or one is longer than the other.
    if (len1 < len2):
        return -1
    elif (len1 > len2):
        return 1
    else:
        return 0



def compareIgnoreCase(s1,s2):
    """
    Compare two strings ignoring case
    
    :param str s1,s2        two strings to compare
    :return: true if the two strings are identical (ignoring case), else false
    """
    return (s1.lower() == s2.lower())



def countChars(strIn,ch):
    """
    Counts all occurrences of a specified character in a string.
    
    :param str strIn: input string
    :param char ch: character to search for
    :return: a count of the number of times ch appears in strIn
    """
    
    if (strIn == None) or (len(strIn) <= 0):
        return 0
    
    return strIn.count(ch,0,len(strIn))



def extractAlphaNumSubstr(strIn,start):
    """
    Extract a substring from the input string that is all numeric or all alphabetic.
    
    :param str strInthe input string
    :param int start: where in strIn to begin extraction (0-based indexing!)
    :return: a purely numeric or a purely alphabetic string
    """
    result = ""
    
    n = len(strIn)
    if (start >= n):
        return ""        # already reached end of string
 
    c = strIn[start]
    if (c.isdigit()):        # accumulate digits
        result = result + c
        for i in range(start+1,n):
            c=strIn[i]
            if not (c.isdigit()):
                break
            result = result + c
    else:                     # accumulate alphabetic characters
        result = result + c
        for i in range(start+1,n):
            c=strIn[i]
            if (c.isdigit()):
                break
            result = result + c
            
    return result
 


def insertCommas(x,d=2):
    """
    Convert a number to a string and insert commas.
    This makes large numbers easier to read.

    :param int/float x: number to convert
    :param int d: number of digits after the decimal point
    :return: a string with commas in it (e.g., 1,245,980.1234)
    """
    s = ",."+str(d)+"f"
    return format(x,s)
   


def removeWhitespace(sStr):
    """
    Removes all whitespace (spaces, tabs, ctrl-lf, etc.) from a string, including
    embedded whitespace.

    :param str sStr: input string
    :return: string with all whitespace removed
    """
    if ((sStr==None) or (len(sStr) <= 0)):
        return ""

    return ''.join(sStr.split())



def replaceStr(strIn,strTarget,strReplace):
    """
    Replaces all occurrences of strTarget in strIn with strReplace.
    
    :param str strIn: input string to be modified
    :param str strTarget: target string to be searched for in strIn
    :param str strReplace: replacement string
    :return: string with the requested replacement
    """
    return strIn.replace(strTarget,strReplace)



def strFormat(fmtStr,value):
    """
    Format a string.
    
    :param str fmtStr: C style format string (e.g., 0.6f, 4d)
    :param int/float value: object to be formatted
    :return: string formatted as per the requested format
    """
    # Note: We could use python's more modern str.format approach, but
    # this function is created instead to simplify porting to other languages.
    
    # WARNING: No check is made to ensure that the format string and value are compatible
    return fmtStr % value



#=========== Main entry point ===============
if __name__ == '__main__':
    pass

