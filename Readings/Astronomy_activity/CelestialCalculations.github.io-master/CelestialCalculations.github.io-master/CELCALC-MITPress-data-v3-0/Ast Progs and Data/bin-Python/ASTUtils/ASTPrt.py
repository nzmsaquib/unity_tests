"""
Output text methods.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
 
import ASTUtils.ASTStyle as ASTStyle
 


CENTERTXT = True        # Boolean indicating whether text is to be centered in the printnoln and println methods.
 


class ASTPrt():
    """
    This class implements methods that print to a scrollable text
    area that the invoking routine sets up. The methods are collected
    into a class to standardize behavior across the programs.
    """
    #===========================================================
    # Class instance variables
    #===========================================================
    # boldFlag                    true if text is to be in a bold font
    # fixedWidthFlag              true if text is to be in a fixed width font (e.g., Courier)    
    # italicFlag                  true it text is to be in an italic font
    # outputTxtBox                reference to calling routine's scrollable output area
            
    def __init__(self,outTextPane):
        """
        Constructor saves a reference to the caller's output text area.
        
        :param textwidget outTextPane: object created by calling routine for text output
        """       
        # Save a reference to the invoking routine's scrollable text area so that
        # we don't have to constantly get it and so that the calling routines
        # don't have to pass in a reference to the scrollable text area.
        # Since we must create an object instance and it sets outputTxtBox, there's no need
        # to check outputTxtBox for null in the rest of this class
        self.outputTxtBox = outTextPane
         
        # Python doesn't have a readily apparent, simple way to mix text font attributes
        # to apply to a subset of the text that is in a text widget. There are ways to
        # apply multiple attributes to an entire widget, but that's not what we want. So,
        # we have to resort to setting flags and tag_configure settings, which will
        # be dynamically determined based on the flag settings. Note that fixed width
        # font text can be centered but not bolded or italicized. To add those features
        # requires additionial code to create appropriate tag_configure settings.
        self.boldFlag = False
        self.italicFlag = False
        self.fixedWidthFlag = False
         
        # It doesn't appear that tags can be redefined, so we have to enumerate all of them
        self.outputTxtBox.tag_configure("set-normal-center",justify="center",font=ASTStyle.TEXT_FONT)
        self.outputTxtBox.tag_configure("set-bold-center",justify='center',font=ASTStyle.TEXT_BOLDFONT)
        self.outputTxtBox.tag_configure("set-italic-center",justify='center',font=ASTStyle.TEXT_ITALICFONT)
        self.outputTxtBox.tag_configure("set-italic-bold-center",justify='center',font=ASTStyle.TEXT_ITALICBOLDFONT)
        self.outputTxtBox.tag_configure("set-normal",font=ASTStyle.TEXT_FONT)
        self.outputTxtBox.tag_configure("set-bold",font=ASTStyle.TEXT_BOLDFONT)
        self.outputTxtBox.tag_configure("set-italic",font=ASTStyle.TEXT_ITALICFONT)
        self.outputTxtBox.tag_configure("set-italic-bold",font=ASTStyle.TEXT_ITALICBOLDFONT)
        self.outputTxtBox.tag_configure("set-fixedwidth",font=ASTStyle.FW_OUT_TEXT_FONT)
        self.outputTxtBox.tag_configure("set-fixedwidth-center",justify="center",font=ASTStyle.FW_OUT_TEXT_FONT)
         
        self.clearTextArea();
        self.resetCursor();
        
    #=================================================================================
    # Define various methods for manipulating text attributes
    #=================================================================================                
         
    def __setAttribs(self,center):
        """
        Set text attributes based on current settings for plain, bold, and italicized.
        
        :param bool center: True if text is to be centered
        :return: the name of the attribute tag for the current font and justify settings
        """
        if (self.fixedWidthFlag):
            attr = "fixedwidth"
        elif (self.boldFlag and self.italicFlag):
            attr = "italic-bold"
        elif self.boldFlag:
            attr = "bold"
        elif self.italicFlag:
            attr = "italic"
        else:
            attr = "normal"
         
        attr = "set-" + attr
         
        if center:
            attr += "-center"
 
        return attr
    
    def setBoldFont(self,boldFont=False):
        """
        Sets whether the output font is to be bold or plain.
        
        :param bool boldFont: True if the text is to be printed in bold font
        """  
        self.boldFlag = boldFont
        
    def setFixedWidthFont(self):
        """Sets the output font to be a fixed width font."""
        self.fixedWidthFlag = True    
  
    def setItalicFont(self,italicFont=False):
        """
        Sets whether the output font is to be italicized.
        
        :param bool italicFont: True if the text is to be printed in italicized font
        """  
        self.italicFlag = italicFont  
        
    def setProportionalFont(self):
        """Sets the output font to be a proportional font."""
        self.fixedWidthFlag = False 

    #=================================================================================
    # Define various methods to write text to the caller's scrollable text box area
    #=================================================================================
     
    def clearTextArea(self):
        """Clears the caller's text output area"""
        self.outputTxtBox.delete(1.0,tk.END)
        self.resetCursor()
        self.setProportionalFont()
        self.setBoldFont(False)
        self.setItalicFont(False)

    def printnoln(self,txt,center=False):
        """
        Outputs text to the caller's scrollable output area and optionally centers it.
        
        :param str txt: text to be printed
        :param bool center: True if text is to be centered
        """       
        self.outputTxtBox.insert(tk.END,txt,self.__setAttribs(center))
         
    def println(self,txt="",center=False):
        """
        Outputs text and a newline to the caller's scrollable output area, and optionally centers it
        
        :param str txt: text to be printed
        :param bool center: True if text is to be centered
        """           
        self.outputTxtBox.insert(tk.END,txt+"\n",self.__setAttribs(center))
 
    def resetCursor(self):
        """
        Resets the cursor to the beginning of the scrollable text area.
         
        This method is intended to be used at the end of a lengthy series
        of prints so that the scrollable list presented to the user will be
        at the beginning of the list, not the end.
        """
        self.outputTxtBox.mark_set("insert", 0.0)
 


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
