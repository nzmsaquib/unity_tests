"""
Provides a class for creating star charts.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.font as tkfont

import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTMisc as ASTMisc
import ASTUtils.ASTStr as ASTStr

#======================================= Graphics Notes =============================================
"""A design decision made for all of the programs for the Celestial Calculations
book is to use only those language features available in standard Python 3 and tkinter.
The reason for this design decision is so that users will not have to locate, download, and install
any other Python packages. As a consequence, advanced math capabilities (from packages such
as numpy) and advanced graphics (from packages such as PyQtGraph and mathplotlib) are either not
available or have to be implemented from scratch.

Among other weaknesses, standard off-the-shelf tkinter does not directly support:
(a) double buffering, (b) anti-aliasing, (c) affine transformation
matrices, or (d) clipping.
    (a) Double buffer - double buffering is not required for our purposes because native tkinter
        is fast enough for the star charts we will produce. Double buffering would do little
        to improve perceived performance.
    (b) Anti-aliasing - this is not a significant issue for our purposes. All lines that are
        drawn are either vertical or horizontal, for which anti-aliasing is not an issue.
        Filled circles are drawn to represent celestial objects, but they are generally
        small enough that jagged edges aren't noticeable. The exception is the circle drawn
        to represent an observer's horizon in plotting horizon coordinates. The border width for
        this circle is increased to make the jagged edges less noticeable. The "smooth" parameter
        could be used for some of the tkinter "create_*" methods, but this doesn't help much.
    (c) Affine transformations - affine transformations are very useful in graphics, but are not
        implemented in tkinter or Python's math library. Consequently, we implement them in
        our ASTMath module. It appears that the @ operator will eventually be implemented in
        some version of Python for doing matrix multiplications. Until then, we created our
        own function for multiplying 3x3 affine transformation matrices.
    (d) Clipping - this is the most significant weakness for our purposes. It would be useful
        to define rectangular and circular clipping regions for the star charts we produce.
        Significant effort is required to implement clipping, so because tkinter does not
        provide clipping, we must live with the consequences. This affects plotting objects
        and text close to a border (e.g., edges of a rectangle for RA/Decl plots, a circle
        for horizon coordinate plotting). The code below attempts to minimize the adverse
        affects of not having clipping, but the results are not entirely satisfactory.
        The code below refers to clipping surfaces, which we calculate, but do not actually
        implement clipping.

Since tkinter assumes that the coordinate (0,0) is the top left point on the drawing surface,
it is convenient to do a transformation to move the point (0,0) to be the bottom left point
on the drawing surface. The function __setWorldCoordSysOrigin below moves the coordinate 
system origin from the upper left corner to the bottom left corner. It does so with an affine
transformation that is equivalent to a translate(0,yMax) transformation followed by a scale(1,-1)
transformation. Both transformations are required because while the translate transformation
moves the coordinate system origin, it does **not** change the direction of the positive
y-axis. The scale transformation Is required to make the y-axis increase from bottom to
top in the coordinate system. Changing the direction of the y-axis could be done with
a rotation transformation of 90 degrees, but the equivalent scale(1,-1) is done instead
to avoid the need to use sine/cosine calculations to do the transformation.    

It is also a simple matter to do a transformation that maps the range for a coordinate, such
as right ascension and declination, to the width/height of the drawing surface. However, this
requires a scaling transformation that will stretch and distort text and make it more difficult
to draw objects that will appear on the screen as circles. Hence, excepting the aforementioned
scale(1,-1) transformation, we will avoid scaling transformations so that we will not to have 
to worry about how graphics objects, such as text, are stretched as a result of a scaling transformation.

Note that our drawing surface will be the same area in the GUI that is used for displaying text, which is
given to this class by the GUI when a chart object is created."""
#======================================= Graphics Notes =============================================

# Constants related to what type of charts to draw
EQ_CHART = True                 # Draw equatorial coordinates chart
HORIZ_CHART = not EQ_CHART      # Draw horizon coordinates chart
WHITE_BKG = True                # Draw white background for chart
BLACK_BKG = not WHITE_BKG       # Draw black background for chart

# Unicode symbol for degrees
DEGREE  = "\u00b0"



class ASTCharts():
    """Creates a class for star charts"""
    #==================================================================
    # Class variables
    #==================================================================
    # Define default pens and drawing backgrounds. Some graphics packages distinguish
    # between a pen and a brush, but tkinter does not, so we'll not create any brushes.
    __WHITEPEN = "white"
    __BLACKPEN = "black"
    __WHITEBKG = "white"
    __BLACKBKG = "black"
    # Define a font for text on the canvas. An actual font will be created in the class
    # constructor below from tkfont because we need to be able to measure a string's size in pixels.
    __CANVASFONT = None
        
    #==================================================================
    # Class instance variables
    #==================================================================
    # container                    container for the canvas
    # drawingCanvas                canvas on which to draw star charts
    # textPane                     scrollable text area that the canvas overlays
    # currentPen                   current pen color for drawings
    # currentBKG                   current background color for the canvas
    # currentFont                  current font for text
    # xc,yc                        xlated,scaled coordinates to move origin to bottom left of canvas
    # xScale,yScale                scaling factors for converting RA/Decl to x,y in the clipping rectangle
    # horizonRadius                radius of horizon circle - used for scaling purposes
    # currentXform                 current transformation matrix (doesn't include transformation required
    #                              to move the screen origin from top,left to bottom,left)

    def __init__(self,container,textPane):
        """
        This constructor creates a Star Chart instance.
        
        :param framewidget container: container in the GUI that will contain a drawing canvas. This is
                                      also the container that has the scrollable text area
                                      so that the drawing canvas will overlay the
                                      scrollable text area.
        :param textwidget textPane: scrollable text area that the canvas will overlay
        """
        # set up initial defaults
        ASTCharts.__CANVASFONT=tkfont.Font(family="Arial",size=10,weight="bold")    
        self.currentPen = ASTCharts.__BLACKPEN
        self.currentBKG = ASTCharts.__WHITEBKG
        self.currentFont = ASTCharts.__CANVASFONT
        
        # Default values for moving the coordinate system origin
        # These *must* be set before any drawings are done
        self.xc = 0.0
        self.yc = 0.0
        self.xScale = 1.0
        self.yScale = 1.0
        self.currentXform = ASTMath.createIdentityMatrix()
        
        # save the container and the original text area
        self.container = container
        self.textPane = textPane        
        
        # Temporarily hide the scrollable text area and create a canvas on top of where the text goes 
        textPane.update_idletasks()       
        textPane.pack_forget()
        self.drawingCanvas = tk.Canvas(container)
        self.clearCanvas()
        # Now make the text area visible again
        self.drawingCanvas.pack_forget()    
        textPane.pack(fill=tk.BOTH,expand=True)
        
    def clearCanvas(self):
        """Erase everything currently on the canvas and set the background color"""
        self.drawingCanvas.pack(fill=tk.BOTH,side="top",expand= True) 
        self.drawingCanvas.delete("all")
        self.drawingCanvas.configure(bg=self.currentBKG,bd=2,relief=tk.RIDGE) 
        self.drawingCanvas.update_idletasks()
        self.container.pack(fill=tk.BOTH,expand=True)
        
    def releaseChart(self):
        """Hide the drawing canvas when a star chart is no longer to be displayed"""
        self.drawingCanvas.update_idletasks()
        self.drawingCanvas.pack_forget()
        self.textPane.pack(fill=tk.BOTH,expand=True)  
        self.container.pack(fill=tk.BOTH,expand=True)     
        
    def showCanvas(self):
        """Hide the scrollable text area and make the canvas visible"""
        self.textPane.pack_forget()
        self.textPane.update_idletasks()

        self.drawingCanvas.pack(fill=tk.BOTH,side="top",expand=True)
        self.drawingCanvas.update_idletasks()
        self.container.pack(fill=tk.BOTH,expand=True)
    
    #======================================================
    # Define 'getters' and 'setters' for various
    # attributes of the drawing canvas.
    #======================================================
    
    def getCanvas(self):
        """Get the drawing canvas"""
        return self.drawingCanvas
    
    def getCanvasSize(self):
        """
        Get the drawing surface's size. The drawing canvas must
        be visible or the results are meaningless.
        
        :return: size as the list [width,height]
        """
        h = self.drawingCanvas.winfo_height() 
        w = self.drawingCanvas.winfo_width()
        return [w,h]
    
    def getCurrentBKG(self):
        """Get the current background color"""
        return self.currentBKG
    
    def getCurrentFont(self):
        """Get the current font for graphics text"""
        return self.currentFont
    
    def getCurrentPen(self):
        """Get the current pen color"""
        return self.currentPen
    
    def setBKGColor(self,bkgColor):
        """
        Set the canvas background color. This doesn't take
        effect until the next time the canvas is cleared.
        
        :param str bkgColor: new color for the background
        """
        self.currentBKG = bkgColor
        
    def setFont(self,font):
        """
        Set the canvas text font. This takes effect
        for the next text drawn on the canvas.
        
        :param str font: new font for the canvas
        """
        self.currentFont = font   
    
    def setPenColor(self,penColor):
        """
        Set the canvas pen color for drawing objects. This takes
        effect for the next object drawn on the canvas.
        
        :param str penColor: new pen color
        """
        self.currentPen = penColor
        
    #=====================================================
    # Define various methods to draw objects and set
    # up coordinate systems
    #=====================================================
    
    def clearAffine(self):
        """
        Clear any existing affine transformations. This doesn't
        affect the work done to move the canvas origin to bottom left.
        """
        self.currentXform = ASTMath.createIdentityMatrix()
        
    def drawCircle(self,xc,yc,r,fillcolor=None,border=None,width=1):
        """
        Draw a circle
        
        :param float xc,yc: center of circle in world coordinates
        :param int r: radius of circle in screen coordinates
        :param str fillcolor: color to use for the circle interior (none if not specified)
        :param str border: color to use for the circle border (currentPen if not specified)
        :param int width: optional width of the border in pixels
        """
        if (border == None):
            c = self.currentPen
        else:
            c = border
        
        # Apply any affine transformation required and then convert world to screen coordinates
        p = ASTMath.multMatrixVect(self.currentXform,[xc,yc,1])
        (x,y) = self.world2screen(p[0],p[1])
           
        self.drawingCanvas.create_oval(x-r,y-r,x+r,y+r,fill=fillcolor,outline=c,width=width)
    
    def drawLine(self,x0,y0,xn,yn,color=None,width=1):
        """
        Draw a line
        
        :param float x0,y0: starting point in world coordinates
        :param float xn,yn: ending point in world coordinates
        :param str color: color to use for the line. If None,
                          use current pen color
        :param int width: optional width of the line in pixels
        """
        if (color == None):
            c = self.currentPen
        else:
            c = color
        
        # Apply any affine transformation required and then convert world to screen coordinates
        p = ASTMath.multMatrixVect(self.currentXform,[x0,y0,1])
        (x0scr,y0scr) = self.world2screen(p[0],p[1])
        p = ASTMath.multMatrixVect(self.currentXform,[xn,yn,1])        
        (xnscr,ynscr) = self.world2screen(p[0],p[1])
       
        self.drawingCanvas.create_line(x0scr,y0scr,xnscr,ynscr,fill=c,width=width)
        
    def drawRect(self,x0,y0,xn,yn,fillColor=None,border=None,width=1):      
        """
        Draw a rectangle in the current pen color
        
        :param float x0,y0: top left corner of the rectangle in world coordinates
        :param float xn,yn: bottom right corner of the rectangle in world coordinates
        :param str fillColor: color to use for rectangle interior (none if not specified)
        :param str border: color to use for rectangle border (currentPen if not specified)
        :param int width: width of the rectangle border
        """
        if (border == None):
            c = self.currentPen
        else:
            c = border
            
        # Apply any affine transformation required and then convert world to screen coordinates
        p = ASTMath.multMatrixVect(self.currentXform,[x0,y0,1])
        (x0scr,y0scr) = self.world2screen(p[0],p[1])
        p = ASTMath.multMatrixVect(self.currentXform,[xn,yn,1])        
        (xnscr,ynscr) = self.world2screen(p[0],p[1])
                            
        # Note: must use xn+1,yn+1 because tkinter says that xn,yn is the point
        # just *outside* the rectangle's bottom right corner             
        self.drawingCanvas.create_rectangle(x0scr,y0scr,xnscr+1,ynscr+1,fill=fillColor,width=width,outline=c)
        
    def drawText(self,x,y,txt,color=None):    
        """
        Draw text on the canvas in the current font
        
        :param float x,y: point in world coordinates at which to place the text.
                          The text will be placed so that x,y is the top left
                          corner of the text.
        :param str text: text to display
        :param str color: color to use (use currentPen if not specified)
        """
        if (color == None):
            c = self.currentPen
        else:
            c = color
            
        # Apply any affine transformation required and then convert world to screen coordinates
        p = ASTMath.multMatrixVect(self.currentXform,[x,y,1])       
        (xscr,yscr) = self.world2screen(p[0],p[1])

        self.drawingCanvas.create_text(xscr,yscr,text=txt,anchor=tk.NW,fill=c,font=self.currentFont)
        
    def getAffine(self):
        """
        Get the current affine transformation matrix.
        
        :return: the current affine transformation
        """
        return self.currentXform        
        
    def getTextSize(self,txt):
        """
        Get the width and height of a string in pixels for the current canvas font.
        
        :param str txt: string whose size is desired
        :return: [width,height]
        """
        font = self.getCurrentFont()
        return [font.measure(txt),font.metrics("linespace")]    
        
    def moveCanvasOrigin(self,xc,yc):
        """
        Save parameters for moving the canvas origin to bottom left of the canvas.
        A point (x,y) in world coordinates will be mapped to (x+xc,yc-y) in screen
        coordinates for the canvas.
        
        :param float xc,yc: scaling/xlation factors (typically the point (0,ymax))
        """
        # Note: we could use matrix math to multiply a translation and scaling matrix,
        # but this is simpler and gives the same result.
        self.xc = xc
        self.yc = yc
        
    def setAffine(self,A):
        """
        Set the current affine transformation matrix.
        
        :param matrix A: the affine transformation desired
        """
        self.currentXform = A
        
    def world2screen(self,x,y):
        """
        Convert the point x,y in world coordinates (origin at bottom left)
        to screen coordinates (origin at top left)
        
        :param float x,y: coordinates in world coordinate system
        :result: int [xp,yp] where xp,yp assume origin at top left of canvas
        """
        # Note: we could use matrix math to multiply a translation and scaling matrix,
        # but this is simpler and gives the same result.     
        return [int(x+self.xc),int(self.yc-y)]             
        
    #===========================================================
    # Other public methods for manipulating the drawing canvas.
    #===========================================================
    
    def initDrawingCanvas(self,bkgWhite,eqChart):
        """Initialize a drawing canvas area for a star chart.
         
        :param str bkgWhite: WHITE_BKG if user wants a white background, else use a black background
        :param bool eqChart: EQ_CHART if doing an equatorial coordinates chart, otherwise 
                             do a horizon coordinates chart
        """
        # set background color and pen color
        if (bkgWhite):
            self.setBKGColor(ASTCharts.__WHITEBKG)
            self.setPenColor(ASTCharts.__BLACKPEN)
        else:
            self.setBKGColor(ASTCharts.__BLACKBKG)
            self.setPenColor(ASTCharts.__WHITEPEN)
            
        self.setFont(ASTCharts.__CANVASFONT)
            
        # Hide the scrollable text area, make the drawing canvas visible,
        # and delete any prior objects on the canvas            
        self.showCanvas()
        self.clearCanvas()
        
        # Clear any  prior affine transformations and move the canvas origin
        self.clearAffine()
        self.__setWorldCoordSysOrigin()
        
        if eqChart:
            self.__initEQCanvas()
        else:
            self.__initHorizonCanvas()
            
    def drawLabelAltAz(self,txt,Alt,Az,color=None):
        """
        This method labels an object located at Alt, Az.
        
        Note: This method does not attempt to draw
        a label if the coordinates are below the
        observer's horizon.
        
        :param str txt: label to display
        :parma float Alt: altitude to plot
        :param float Az: azimuth to plot
        :param str color: if specified, color to use for plotting
        """
        if (color == None):
            c = self.currentPen
        else:
            c = color
            
        # if object is below the horizon, no need to try to label it
        if (Alt < 0.0):
            return

        # Convert coord to x,y and then adjust based on txt size
        x,y = self.__AltAz2XY(Alt, Az)
        w,h = self.getTextSize(txt)
        y = y - int(h/2.0) - 2      # pad a little to make it look better
        x = x - int(w/2.0)
        self.drawText(x, y, txt, color=c)

    def drawLabelRADecl(self,txt,RA,Decl,color=None):
        """
        This method labels an object located at RA, Decl.
        
        :param str txt: label to display
        :param float RA: right ascension for the object
        :param float Decl: declination for the object
        :param str color: if specified, color to use for plotting
        """
        if (color == None):
            c = self.currentPen
        else:
            c = color
        
        # Convert coordinates to x,y and then adjust based on txt size 
        x,y = self.__RADecl2XY(RA, Decl)
        w,h = self.getTextSize(txt)
        y = y - int(h/2.0) - 2      # pad a little to make it look better
        x = x + int(w/2.0)          # add because x decreases going left to right 
        
        self.drawText(x, y, txt, color=c)
            
    def plotAltAz(self,Alt,Az,mV,color=None):
        """
        Plot an object, given its horizon coordinates, on a
        circular plotting surface.
        
        Note: This method does not attempt to plot any
        object that is below the observer's horizon.
        
        :param float Alt: altitude to plot
        :param float Az: azimuth to plot
        :param float mV: visual magnitude of the object
        :param str color: if specified, color to use for plotting
        """
        if (color == None):
            c = self.currentPen
        else:
            c = color
            
        # If object is below the horizon, no need to try to plot it
        if (Alt < 0.0):
            return
        
        r = _mVtoRadius(mV)
        x,y = self.__AltAz2XY(Alt,Az)
        self.drawCircle(x,y,r,fillcolor=c,border=c)
            
    def plotRADecl(self,RA,Decl,mV,color=None):
        """
        Plot an object, given its equatorial coordinates, on
        a rectangular plotting surface.
        
        :param float RA: right ascension to plot
        :param float Decl: declination to plot
        :param float mV: visual magnitude of the object
        :param str color: if specified, color to use for plotting
        """
        if (color == None):
            c = self.currentPen
        else:
            c = color
        
        r = _mVtoRadius(mV)
        x,y = self.__RADecl2XY(RA,Decl)
        self.drawCircle(x,y,r,fillcolor=c,border=c)                   

    def refreshChart(self):
        """
        Refresh a star chart. This method is provided to allow an external caller
        to request the current state of the chart be displayed to show the user
        progress as the star chart is being created.
        """
        self.drawingCanvas.update_idletasks()

    #===========================================================
    # Private methods used only in this class
    #===========================================================
    
    def __initEQCanvas(self):
        """
        Initialize the drawing canvas for a star chart based on equatorial
        coordinates. The drawing area will be a rectangle with the RA on the
        y-axis and the Decl plotted on the x-axis. RA decreases from 24h on the
        left of the display to 0h on the right. Decl increases from -180 degrees
        at the bottom to 180 degrees at the top. The size of the plotting
        rectangle is adjusted to allow RA/Decl marks and labels on the plotting
        rectangle boundary.
        """
        xPad = 4
        yPad = 3
        xTicLen = 5
        yTicLen = 5
        
        # Figure out how much to adjust the plotting rectangle to allow
        # tic marks and RA/Decl labels outside the plotting rectangle.
        RAFontWidth, RAFontHeight = self.getTextSize("00h")
        DeclFontWidth, DeclFontHeight = self.getTextSize("+180"+DEGREE)
                
        # Calculate the actual size of our plotting surface when adjusted
        # for tic marks and labels on the RA/Decl axes. This will
        # ultimately be used to create a clipping rectangle.
        size = self.getCanvasSize()     # [width, height] of drawing canvas
        CRx = DeclFontWidth + xPad + xTicLen
        CRy = RAFontHeight + yPad + yTicLen
        CRwidth = size[0]-CRx
        CRheight = size[1]-CRy
        
        self.xScale = CRwidth/24.0       # RA goes from 0 to 24h
        self.yScale = CRheight/360.0     # Decl goes from -180 to 180 deg
        
        # Draw the clipping rectangle, but move the coordinate system origin
        # to place the rectangle origin at [CRx,CRy]
        A = ASTMath.createXlateMatrix(CRx,CRy)              # Move the origin
        self.setAffine(A)
        self.drawRect(0,0,CRwidth,CRheight,width=2)       
        
        # At this point, the coordinate system origin (0,0) is at the
        # bottom left of the clipping rectangle. Because we're plotting
        # RA on the x-axis and Decl on the y-axis, it would be useful to do a
        # scaling transformation to map the x range to be [24.0, 0.0] and y
        # to be [-180.0, 180.0] to match RA and Decl ranges. However, a scaling
        # transformation would also scale text for the RA/Decl labels and the
        # circles used to plot stars. So, we'll settle for just (1) making the
        # x-axis range from [CRwidth,0], and (2) making the y-axis range from
        # [-CRheight/2, CRheight/2]. We'll do the entire transformation
        # with a single affine transformation matrix that first makes the x-axis
        # range from [CRwidth,0] and then translates so that the origin (0,0) is
        # at the left and middle of the clipping rectangle. Doing this then allows
        # RA/Decl coordinates to be very easily mapped to the clipping rectangle
        # via a simple scaling.
        
        B = ASTMath.createAffineMatrix([-1.0, 0.0, CRwidth],[0.0, 1.0, CRheight/2.0])
        self.setAffine(ASTMath.mult3DMatrices(A, B))        # have to preserve the xform to move the rectangle origin
        
        # Draw the RA tic marks and labels on the x-axis
        y = ASTMath.Round(self.__Decl2Y(-180.0))
        for i in range(1,24):
            x = int(self.__RA2X(i))
            self.drawLine(x, y, x, y - yTicLen)
            if ((i%2) == 0):
                st = ASTStr.strFormat("%02d",i) + "h"
                x = x + RAFontWidth/2
                self.drawText(x,y-yTicLen,st)
                
        # Now draw the Decl tic marks and labels on the y-axis
        x = ASTMath.Round(self.__RA2X(24.0))
        i = -180
        while (i < 175):
            y = ASTMath.Round(self.__Decl2Y(i))
            self.drawLine(x, y, x + xTicLen, y)
            if ((i % 20) == 0):
                if (i == 0):
                    st = ASTStr.strFormat("%5d",0) + DEGREE
                else:
                    st = ASTStr.strFormat("%+4d",i) + DEGREE
                y = y + DeclFontHeight/2
                self.drawText(x + xTicLen + DeclFontWidth - xPad, y,st)
            i += 10
                
        # We should now set [CRx,CRy,CRwidth,CRheight] to be a clipping rectangle.
        # Unfortunately, tkinter doesn't provide clipping support so we'll have to
        # forego creating a clipping region. This will only affect putting text
        # on our star chart since calculating star locations are done in RA, Decl
        # and converted to fit solely within the clipping rectangle.
    
    def __initHorizonCanvas(self):
        """
        Initialize the drawing canvas for a star chart, drawn as a circular
        chart, of horizon coordinates.
        """
        # Using the current size of the drawing canvas surface, determine the
        # diameter of a circle to use in plotting the star chart objects. The
        # diameter will be the canvas width or height, whichever is smaller. This
        # diameter will be adjusted so that compass directions can be displayed
        # outside the circle.
        
        # how much spacing to add around the horizon circle
        padX = 4
        padY = 4 
        
        # Determine where the center of the plotting circle will be
        canvasWidth, canvasHeight = self.getCanvasSize()
        centerX = canvasWidth / 2.0
        centerY = canvasHeight / 2.0
        
        # Calculate size of adjustment needed for the compass directions
        # based on the size of the font that will be used. We'll use
        # the character W as representative of the font's width/height
        WFontWidth, WFontHeight = self.getTextSize("W")
        
        # Figure out a proper circle to represent the observer's horizon,
        # which is adjusted for the compass direction strings
        if (canvasWidth > canvasHeight):
            diameter = canvasHeight - 2 * (WFontHeight + 1.5 * padY)
        else:
            diameter = canvasWidth - 2 * (WFontWidth + 1.5 * padX)
        self.horizonRadius = diameter / 2.0         # save as instance var for converting Alt/Az to x/y
 
        # Draw a circle representing the plotting surface and move the origin
        # to its center
        self.drawCircle(centerX, centerY, self.horizonRadius,width=2)
        self.setAffine(ASTMath.createXlateMatrix(centerX,centerY))
        
        # Draw the compass directions. Recalculate font metrics for better accuracy
        WFontWidth, WFontHeight = self.getTextSize("N")
        x = int(-0.5*WFontWidth)
        self.drawText(x,self.horizonRadius+WFontHeight+padY/2,"N")
        self.drawText(x,-(self.horizonRadius + padY),"S")
        
        WFontWidth, WFontHeight = self.getTextSize("W")
        y = int(WFontHeight/2.0)
        self.drawText(-(self.horizonRadius+padX+WFontWidth),y,"W")
        self.drawText(self.horizonRadius+padX,y,"E")
        
        # We should now set the circle just drawn to be a clipping region.
        # Unfortunately, tkinter doesn't provide clipping support so we'll have to
        # forego creating a clipping region. This will only affect putting text
        # on our star chart since calculating star locations are done in horizon
        # coordinates and converted to fit solely within the clipping region.
    
    def __setWorldCoordSysOrigin(self):
        """
        This method sets us the transformation necessary to move the canvas
        coordinate system origin from the top left corner to the bottom right
        corner of the canvas. It also changes y-axis orientation so that y values
        increase going from the origin upwards as in a normal Cartesian coordinate
        system.
        
        We could use a translation and scale (or rotate) transformation matrix,
        but it's much simpler to do the matrix math ahead of time and save the
        results via the object's moveCanvasOrigin method.
        """
        d = self.getCanvasSize()
        self.moveCanvasOrigin(0, d[1])        

    #===========================================================
    # Private methods that convert Alt/Az to world coordinates
    # for a circular plotting surface.
    #===========================================================
    
    def __AltAz2XY(self,Alt,Az):
        """
        Convert Alt and Az to the appropriate x,y coordinates for
        plotting. This assumes that the x,y coordinates are for
        a circular plotting surface.
        
        :param float Alt,Az: horizon coordinates to convert
        :return: float [x,y] structure containing the converted coordinates.
        """
        x = ASTMath.COS_D(Alt) * ASTMath.SIN_D(Az)
        y = ASTMath.COS_D(Alt) * ASTMath.COS_D(Az)
        
        # scale x,y appropriately and return results
        return [x*self.horizonRadius,y*self.horizonRadius]
    
    #===========================================================
    # Private methods that convert RA/Decl to world coordinates
    # for a rectangular plotting surface.
    #===========================================================
    
    def __Decl2Y(self,Decl):
        """
        Convert Decl to a Y value, assuming a rectangular plotting surface.
        
        :param float Decl: declination
        :return: float y for current drawing surface
        """
        return Decl*self.yScale
    
    def __RA2X(self,RA):
        """
        Convert RA to an X value, assuming a rectangular plotting surface.
        
        :param float RA: right ascension
        :return: float x for current drawing surface
        """        
        return RA*self.xScale
    
    def __RADecl2XY(self,RA,Decl):
        """
        Return both RA and Decl converted to x,y
        
        :param float RA, Decl: right ascension, declination
        :return: float [x,y] for current drawing surface
        """
        return [self.__RA2X(RA),self.__Decl2Y(Decl)]
    


#===========================================================
# Private methods that are used only in this module, but
# that do not require an object instance.
#===========================================================

def _mVtoRadius(mV):
    """
    Determine how big a "dot" to plot for an object based upon its mV.
    
    :param float mV: visual magnitude of the object
    :return: int radius (in pixels) of a circle to plot for the object
             where a larger radius represents a brighter object.
    """
    # Brightness increases/decreases by 1 magnitude every 2.512 units
    rdot = int(ASTMisc.mV0RADIUS - (mV/2.512))
    if (rdot < ASTMisc.mVMIN_RADIUS):
        rdot = ASTMisc.mVMIN_RADIUS
    elif (rdot > ASTMisc.mVMAX_RADIUS):
        rdot = ASTMisc.mVMAX_RADIUS
    
    return rdot



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
