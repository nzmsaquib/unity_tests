"""
Provides a class for time objects and time-related methods,
including validating strings in the form h:m:s.s where h is
required but m and s.s are optional.

The code allows for the possibility that h is negative as well
as situations where the time is negative but the integer hours is 0
(e.g., -0:13:14.45). Time objects maintain the time
in both HMS and decimal format so that either format can be
readily obtained directly from the object.

Different programming languages have time-related routines,
but they are not used here because (a) we need to allow
time to be negative and (b) implementing the time-related routines
needed makes it easier to translate to a different programming
language.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import datetime

import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTLatLon as ASTLatLon
import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTMisc import HMSFORMAT, SHOW_ERRORS, HIDE_ERRORS
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
from ASTUtils.ASTStyle import DEC_HRS_FMT

class ASTTime():
    """Defines a class for time objects."""
    #=======================================================
    # Class instance variables
    #======================================================= 
    # bValid                true if the object contains a valid time
    # bNeg                  true if the time is negative
    # dDecValue             decimal value for the time
    # iHours
    # iMinutes
    # dSeconds    
    
    def __init__(self,**kwargs):
        """
        Since much of the usage of this class involves validating that a string has a valid
        time, assume a new object is invalid until it is proven otherwise.
        """
        self.bValid = False
        self.bNeg = False
        self.dDecValue = 0.0
        self.iHours = 0
        self.iMinutes = 0
        self.dSeconds = 0.0

        if ('value' in kwargs):
            x = kwargs['value']
            self.setDecTime(x)

    #========================================================
    # Define 'get/set' accessors for the object's fields
    #========================================================   
    def getDecTime(self):
        return self.dDecValue
    def getHours(self):
        return self.iHours
    def getMinutes(self):
        return self.iMinutes
    def getSeconds(self):
        return self.dSeconds
    def isNegTime(self):
        return self.bNeg   
    def isValidTimeObj(self):
        return self.bValid

    def setDecTime(self,decValue):
        """
        Sets the decimal value for the time
        
        :param float decValue: value to assign to the object
        """
        self.bValid = False
        self.bNeg = False
        self.dDecValue = 0.0
        self.iHours = 0
        self.iMinutes = 0
        self.dSeconds = 0.0
        
        # Try to protect the user as much as possible since Python
        # doesn't do type checking. Return an invalid object if 
        # decValue isn't a valid number.
        if not isinstance(decValue,(int,float)):
            return        
         
        tmp = abs(decValue)
        self.bValid = True
        self.bNeg = (decValue < 0.0)
        self.dDecValue = decValue
        self.iHours = ASTMath.Trunc(tmp)
        self.iMinutes = ASTMath.Trunc(ASTMath.Frac(tmp) * 60.0)
        self.dSeconds = 60.0 * ASTMath.Frac(60 * ASTMath.Frac(tmp))     
     


#===================================================================================
# Define some general functions for validating that a string contains a valid time
#===================================================================================

def isValidDecTime(inputStr,flag=SHOW_ERRORS):
    """
    Checks a string to see if it contains a valid decimal
    format value. Don't display any error messages unless
    flag is SHOW_ERRORS.

    :param str inputStr: string to validate
    :param bool flag: whether to display error messages
    :return: an ASTTime object containing the result
    """
    timeObj = ASTTime()
    
    timeObj.bValid = False            # assume invalid till we parse and prove otherwise
    
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter time as a valid decimal value", "Invalid Time Format", flag)
        return timeObj

    dTempObj = ASTReal.isValidReal(inputStr,HIDE_ERRORS)
    
    if (dTempObj.isValidRealObj()):
        timeObj = DecToHMS(dTempObj.getRealValue())
        if (timeObj.getHours() > 23):
            ASTMsg.errMsg("The Hours value entered is not a valid value.\n" +
                          "Please enter a valid decimal value", "Invalid Time Format (Hours)", flag)
            timeObj.bValid = False
        else:
            timeObj.bValid = True
    else:
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter time as a valid decimal value", "Invalid Time Format", flag)
    
    return timeObj



def isValidHMSTime(inputStr,flag=SHOW_ERRORS):
    """
    Checks a string to see if contains a valid HMS value.
    The string format is h:m:s.s where h is required but m
    and s.s are optional. Do not display any error message
    if flag is HIDE_ERRORS to let the calling routine display
    its own messages.
    
    The custom code here is needed because we want to allow for
    the possibility that h could be negative, and to allow both
    minutes and seconds to be optional. Also note that to
    handle a situation where the time is negative but h is 0
    (e.g., -0:13:15.45), the bNeg field is required.

    :param str inputStr: string to validate
    :param bool flag: whether to display error messages
    :return: an ASTTime object containing the results. The returned 'hours' value in 
             the ASTTime object will always be non-negative with bNeg set to true
             if the time is negative. This is because we must allow for the
             possibility of the hours being -0.
    """
    timeObj = ASTTime()
    timeObj.bValid = False        # assume invalid till we parse and prove otherwise
 
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS format",flag)
        return timeObj

    timeObj.bNeg = (inputStr[0] == '-')        # account for negative time 
    
    parts = inputStr.split(":")
    if (len(parts) > 3):
        ASTMsg.errMsg("The HMS value entered is not in a valid format.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS format",flag)
        return timeObj
 
    # Get hours, which must have been entered if we get this far
    iTempObj = ASTInt.isValidInt(parts[0],HIDE_ERRORS)
    if (iTempObj.isValidIntObj()):
        timeObj.iHours = abs(iTempObj.getIntValue())            # timeObj.bNeg will contain the sign
    else:
        ASTMsg.errMsg("The Hours value entered is not a valid value.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Hours)",flag)
        return timeObj
    if (timeObj.iHours > 23):
        ASTMsg.errMsg("The Hours value entered is not a valid value.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Hours)",flag)
        return timeObj
 
    # Get minutes, if they were entered
    if (len(parts) == 1):           # if true, neither m or s were entered, but still need to set dec value
        timeObj.dDecValue = HMStoDec(timeObj.bNeg,timeObj.iHours,0,0.0)
        timeObj.bValid = True
        return timeObj
 
    iTempObj = ASTInt.isValidInt(parts[1],HIDE_ERRORS)
    if (iTempObj.isValidIntObj()):
        timeObj.iMinutes = iTempObj.getIntValue()
    else:
        ASTMsg.errMsg("The Minutes value entered is not a valid value.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Minutes)",flag)
        return timeObj
    if ((timeObj.iMinutes > 59) or (timeObj.iMinutes < 0)):
        ASTMsg.errMsg("The Minutes value entered is not a valid value.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Minutes)",flag)
        return timeObj
 
    # Get seconds, if they were entered
    if (len(parts) == 2):                   # if true, s was not entered
        timeObj.dDecValue = HMStoDec(timeObj.bNeg,timeObj.iHours,timeObj.iMinutes,0.0)
        timeObj.bValid = True
        return timeObj
 
    dTempObj = ASTReal.isValidReal(parts[2],HIDE_ERRORS)
    if (dTempObj.isValidRealObj()):
        timeObj.dSeconds = dTempObj.getRealValue()
    else:
        ASTMsg.errMsg("The Seconds value entered is not a valid value.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Seconds)",flag)
        return timeObj
    if ((timeObj.dSeconds >= 60.0) or (timeObj.iMinutes < 0.0)):
        ASTMsg.errMsg("The Seconds value entered is not a valid value.\n" +
                      "Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Seconds)",flag)
        return timeObj
 
    timeObj.dDecValue = HMStoDec(timeObj.bNeg, timeObj.iHours, timeObj.iMinutes, timeObj.dSeconds)
    timeObj.bValid = True
    return timeObj



def isValidTime(inputStr,flag=SHOW_ERRORS):
    """
    Validate a string that may be in either HMS or decimal
    format. Do not display any error messages unless flag
    is SHOW_ERRORS.

    :param str inputStr: string to be validated
    :param bool flag: whether to display error messages
    :return: an ASTTime object containing the results
    """
    resultObj = ASTTime()
    
    resultObj.bValid = False
    
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter time as a valid decimal or HMS (hh:mm:ss.ss) value", "Invalid Time Format",flag)
        return resultObj
    
    if (inputStr.find(':') >= 0):
        resultObj = isValidHMSTime(inputStr,flag)
    else:
        resultObj = isValidDecTime(inputStr,flag)
    
    return resultObj    



#=================================================================
# Define some functions to convert time to a string
#=================================================================

#================ IMPORTANT NOTE =================================
# Python doesn't allow overloading functions and named parameters
# would be too cumbersome in this case. single-dispatch generic 
# functions are in the latest Python 3, but if we used them, it 
# would make it more difficult to back port to Python 2. So, we're
# stuck with having unique functions for each case.
#================ IMPORTANT NOTE =================================

def timeToStr_dec(decTime,formatFlag):
    """
    Convert time to an HMS or decimal string format depending upon the formatFlag,
    which can be HMSFORMAT or DECFORMAT.

    :param float decTime: decimal value to convert
    :param bool formatFlag: what format to return
    :return: decimal time converted to a string
    """
    return timeToStr_obj(DecToHMS(decTime),formatFlag)



def timeToStr_hms(neg,h,m,s,formatFlag):
    """
    Convert time to an HMS or decimal string format depending upon the formatFlag,
    which can be HMSFORMAT or DECFORMAT.

    :param bool neg: true if the time is negative
    :param int h: integer hours
    :param int m:integer minutes
    :param float s: decimal seconds
    :param bool formatFlag: what format to return
    :return: time converted to a string
    """
    if (formatFlag == HMSFORMAT):
        return __HMStoStr_hms(neg, h, m, s)
    else:
        return ASTStr.strFormat(DEC_HRS_FMT,HMStoDec(neg,h,m,s))



def timeToStr_obj(timeObj,formatFlag):
    """
    Convert time to an HMS or decimal string format depending upon the formatFlag,
    which can be HMSFORMAT or DECFORMAT.

    :param ASTTime timeObj: time object to be converted
    :param bool formatFlag: what format to return
    :return: time converted to a string
    """
    if (formatFlag == HMSFORMAT):
        return __HMStoStr_obj(timeObj)
    else:
        return ASTStr.strFormat(DEC_HRS_FMT,timeObj.getDecTime())



def getCurrentTime():
    """
    Define a method for returning the current time. This is easy
    to do, but is provided as a method to ease portability to other languages.

    :return: current system time
    """
    now = datetime.datetime.now()
    return timeToStr_hms(False,now.hour,now.minute,now.second,HMSFORMAT)



#================================================================
# Functions to convert time to/from HMS and to/from decimal
#================================================================

def DecToHMS(decValue):
    """
    Converts a decimal value into an ASTTime object
    
    :param float decValue: decimal value to be converted
    :return: an ASTTime object
    """
    return ASTTime(value=decValue)



def HMStoDec(neg,h,m,s):
    """
    Convert HMS values to decimal
    
    :param bool neg: true if the time is negative
    :param int h: integer hours (always non-negative)
    :param int m: integer minutes
    :param float s: decimal seconds
    :return: time converted to a decimal value
    """
    result = h + (m/60.0) + (s/3600.0)
    if (neg):
        result = -result
    
    return result



#==================================================
# These functions perform various time conversions
#==================================================

def adjustDatebyDays(dateObj, days):
    """
    Adjust the date by some number of days
    
    :param ASTDate dateObj: original date
    :param float days: number of days by which to adjust the date
    :return: adjusted date
    """
    if (days == 0):
        return dateObj
    
    JD = ASTDate.dateToJD_obj(dateObj) + days
    return ASTDate.JDtoDate(JD)



def GSTtoLST(GST,dLongitude):
    """
    Convert GST to LST
    
    :param float GST: GST time to convert
    :param float dLongitude: observer's longitude
    :return: LST time for the given GST and longitude
    """
    LST = GST + (dLongitude / 15.0)
    
    if (LST < 0.0):
        LST = LST + 24.0
    if (LST > 24.0):
        LST = LST - 24.0
    
    return LST



def GSTtoUT(GST, dateObj):
    """
    Convert GST to UT
    
    :param float GST: GST time to convert
    :param ASTDate dateObj: date for which UT is desired
    :return: UT time for the given GST and date
    """
    # must use iDay rather than dDay because dDay may contain time of day
    # which will throw the results off
    JD = ASTDate.dateToJD_mdy(dateObj.getMonth(), float(dateObj.getiDay()), dateObj.getYear())
    JD0 = ASTDate.dateToJD_mdy(1, 0, dateObj.getYear())
    
    days = int(JD - JD0)
    dT = (JD0 - 2415020.0) / 36525.0
    dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
    dB = 24.0 - dR + 24.0 * (dateObj.getYear() - 1900)
    T0 = 0.0657098 * (days) - dB
    if (T0 < 0):
        T0 = T0 + 24
    
    dA = GST - T0
    if (dA < 0):
        dA = dA + 24
    UT = dA * 0.99727
    return UT



def LCTtoUT(LCT, DST, timeZone, lon, dateAdjustObj):
    """
    Convert LCT to UT
    
    :param float LCT: time to convert
    :param bool DST: true if on daylight saving time
    :param TimeZoneType timeZone: observer's time zone
    :param float lon: observer's longitude
    :param ASTInt dateAdjustObj: amount by which to adjust date
    :return: UT and also dateAdjustObj
    
    Note: Must pass dateAdjustObj as an object because Python won't allow
          input parameters to be modified, and we want to return both UT and dateAdjustObj.
          We could do return [LCT,dateAdjust] but that would make the code too Python-specific.
    """
    dateAdjustObj.setIntValue(0)        # amount to adjust date by if the conversion results in next or prev day
    
    UT = LCT
    
    # Adjust for daylight saving time
    if (DST):
        UT = LCT - 1
    
    UT = UT - ASTLatLon.timeZoneAdjustment(timeZone, lon)
    if (UT > 24):
        UT = UT - 24.0
        dateAdjustObj.setIntValue(1)
    elif (UT < 0):
        UT = UT + 24.0
        dateAdjustObj.setIntValue(-1)
    
    return UT    



def LSTtoGST(LST, dLongitude):
    """
    Convert LST to GST
    
    :param float LST: LST time to convert
    :param float dLongitude: observer's longitude
    :return: the LST converted to GST.
    """
    GST = LST - (dLongitude / 15.0)
    if (GST < 0):
        GST = GST + 24
    if (GST > 24):
        GST = GST - 24
    
    return GST



def UTtoGST(UT,dateObj):
    """
    Convert UT to GST
    
    :param float UT: UT time to convert
    :param ASTDate dateObj: date as an object
    :return: GST time for the given UT and date
    """
    # must use iDay rather than dDay because dDay may contain time of day
    # which will throw the results off
    JD = ASTDate.dateToJD_mdy(dateObj.getMonth(), float(dateObj.getiDay()), dateObj.getYear())
    JD0 = ASTDate.dateToJD_mdy(1, 0.0, dateObj.getYear())             # JD for Jan 0.0
    dDays = JD - JD0                                             # number of days since Jan 0.0
    dT = (JD0 - 2415020.0) / 36525.0
    dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
    dB = 24.0 - dR + 24.0 * (dateObj.getYear() - 1900)
    T0 = 0.0657098 * (dDays) - dB
    GST = T0 + UT * 1.002738
    
    if (GST < 0.0):
        GST = GST + 24.0
    if (GST > 24.0):
        GST = GST - 24.0
    
    return GST



def UTtoLCT(UT, DST, timeZone, lon, dateAdjustObj):
    """
    Convert UT to LCT
    
    :param float UT: time to convert
    :param bool DST: true if on daylight saving time
    :param TimeZoneType timeZone: observer's time zone
    :param float lon: observer's longitude
    :param ASTInt dateAdjustObj: amount by which to adjust date
    :return: LCT and also dateAdjustObj
    """
    # Note: Must pass dateAdjustObj as an object because Python won't allow
    # input parameters to be modified. We could do return LCT, dateAdjust but
    # that would make the code too Python-specific.

    dateAdjustObj.setIntValue(0)                # amount to adjust date by if the conversion results in next or prev day
    
    LCT = UT + ASTLatLon.timeZoneAdjustment(timeZone, lon)
    if (LCT > 24):                # next day
        LCT = LCT - 24.0
        dateAdjustObj.setIntValue(1)

    if (LCT < 0):                 # previous day
        LCT = LCT + 24.0
        dateAdjustObj.setIntValue(-1)

    if (DST):
        LCT = LCT + 1
    
    return LCT



#============================================================
# Functions that are intended to be private and used only
# in this package.
#============================================================

def __HMStoStr_hms(neg,h,m,s):
    """
    Convert time to an HMS-formatted string
    
    :param bool neg: true if time is negative
    :param int h: integer hours
    :param int m: integer minutes
    :param float s: decimal seconds
    :return: time converted to a string
    """
    # Due to round off and truncation errors, build up the result
    # starting with s, then m, then h and worry about h, m, and s
    # values being out of bounds. So, convert s to a string, convert
    # the string back to a number, and adjust both s and m if required.
    # Similarly, handle m and do h last.
    
    sTmp = ASTStr.strFormat("%05.2f",s)
    x = float(sTmp)
    if (x >= 60.0):
        sTmp = "00.00"
        m += 1
    result = sTmp
 
    sTmp = ASTStr.strFormat("%02d", m)
    n = int(sTmp)
    if (n >= 60):
        sTmp = "00"
        n = abs(h) + 1
    else:
        n = abs(h)
    result = sTmp + ":" + result
 
    result = ASTStr.strFormat("%d",n) + ":" + result
    if (neg):
        result = "-" + result
        
    return result        



def __HMStoStr_obj(timeObj):
    """
    Convert time to an HMS-formatted string
    
    :param ASTTime timeObj: time object to convert
    :return: time converted to a string
    """  
    return __HMStoStr_hms(timeObj.isNegTime(),timeObj.getHours(),
                          timeObj.getMinutes(),timeObj.getSeconds())
 


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
