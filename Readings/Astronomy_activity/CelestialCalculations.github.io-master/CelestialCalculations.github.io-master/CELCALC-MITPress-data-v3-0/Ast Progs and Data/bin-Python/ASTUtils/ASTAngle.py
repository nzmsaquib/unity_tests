"""
Provides a class for angle objects and related methods.

This class is primarily used for determining whether a string has a valid
angle. Methods provided include validation routines in which an angle can
be expressed in DMS format (###d ##m ##.##s) or decimal format. When
expressed in DMS format, the d, m, and s values are optional as long as one
is provided. They must be given in the correct order (i.e., 5m 30d is
invalid because degrees must be specified before minutes). The angle object
maintains the object's value in both DMS and decimal format so that either
format can be readily obtained directly from the object.

Note that to handle situations where the angle is negative but the integer
degrees is 0 (e.g., -0d 13m 14.45s), the bNeg field is used to indicate whether
the DMS format of the value is negative. The decimal value of the angle will be
negative or positive as required and kept in synch with the bNeg field. Also
note that due to roundoff errors, it is possible that minutes and seconds can
exceed the value 60.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import DMSFORMAT, SHOW_ERRORS, HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
from ASTUtils.ASTStyle import DEC_DEG_FMT

class ASTAngle():
    """Defines a class for angles."""
    
    #==========================================================
    # Class instance variables
    #==========================================================
    # Save object value in both decimal and d,m,s formats
    # bValid                    true if the object is a valid angle
    # dDecValue                 decimal value for the angle
    # bNeg                      true if the angle is negative
    # Degrees
    # iMinutes
    # dSeconds
    
    def __init__(self,**kwargs):
        """
        Since much of the usage of this class involves validating that a string has a valid
        angle, assume a new object is invalid until it is proven otherwise.
        """
        self.bValid = False
        self.dDecValue = 0.0
        self.bNeg = False
        self.iDegrees = 0
        self.iMinutes = 0
        self.dSeconds = 0.0

        if ('value' in kwargs):
            x = kwargs['value']
            self.setDecAngle(x)   

    #========================================================
    # Define 'get/set' accessors for the object's fields
    #========================================================   
    def getDecAngle(self):
        """
        :return: decimal value of the angle (in degrees)
        """
        return self.dDecValue
    def getDegrees(self):
        return self.iDegrees
    def getMinutes(self):
        return self.iMinutes
    def getSeconds(self):
        return self.dSeconds
    def isNegAngle(self):
        """
        Determines whether the angle is negative.
        
        This is needed because an angle such as -0d 15m 30s is
        valid, but some variable is required to indicate that the
        value is negative.
        """
        return self.bNeg
    def isValidAngleObj(self):
        return self.bValid
    
    # We only need one 'set' accessor
    def setDecAngle(self,decValue):
        """
        :param float decValue: value to assign to the object
        """
        self.bValid = False
        self.dDecValue = 0.0
        self.bNeg = False
        self.iDegrees = 0
        self.iMinutes = 0
        self.dSeconds = 0.0        
        
        # Try to protect the user as much as possible since Python
        # doesn't do type checking. Return an invalid object if 
        # decValue isn't a valid number.
        if not isinstance(decValue,(int,float)):
            return
         
        tmp = abs(decValue)
        self.bValid = True
        self.bNeg = (decValue < 0.0)
        self.dDecValue = decValue
        self.iDegrees = ASTMath.Trunc(tmp)
        self.iMinutes = ASTMath.Trunc(ASTMath.Frac(tmp) * 60.0)
        self.dSeconds = 60.0 * ASTMath.Frac(60 * ASTMath.Frac(tmp))
        
#==============================================================================
# Define some general functions for validating that a string has a valid angle
# expressed in either decimal format or DMS format.
#==============================================================================           

def isValidAngle(inputStr,flag=SHOW_ERRORS):
    """
    Validate a string that may be in either DMS or decimal
    format. Do not display any error messages unless flag
    is SHOW_ERRORS.
    
    :param str inputStr: string to be validated
    :param bool flag: flag indicating whether to display error message
    :return: an ASTAngle object containing the angle extracted from the string.
    
    The returned value for iDegrees will always be non-negative with bNeg set to true
    if the value is negative. This is because we must allow for the
    possibility of the integer degrees being -0.
    """
    angleObj = ASTAngle()
    angleObj.bValid = False
    
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter an angle as a valid decimal or DMS (###d ##m ##.##s) value","Invalid Angle Format",flag)
        return angleObj

    # if 'd', 'm', or 's' is in the string, it is in DMS format
    if ((inputStr.find('d') >= 0) or (inputStr.find('m') >= 0) or (inputStr.find('s') >= 0)):
        angleObj = isValidDMSAngle(inputStr,flag)
    else:
        angleObj = isValidDecAngle(inputStr,flag)
    
    return angleObj



def isValidDecAngle(inputStr,flag=SHOW_ERRORS):
    """
    Validate that a string contains a valid decimal format value
    Don't display any error messages unless flag is SHOW_ERRORS.
    
    :param str inputStr: string to be validated
    :param bool flag: whether to display error messages
    :return: an ASTAngle object containing the angle extracted from the string.
    
    The returned value for iDegrees will always be non-negative with bNeg set to true
    if the value is negative. This is because we must allow for the
    possibility of the integer degrees being -0.
    """
    angleObj = ASTAngle()
    
    angleObj.bValid = False                # assume invalid till we parse and prove otherwise
    
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter an angle as a valid decimal value", "Invalid Angle Format", flag)
        return angleObj

    dTempObj = ASTReal.isValidReal(inputStr,HIDE_ERRORS)
    if (dTempObj.isValidRealObj()):
        angleObj = DecToDMS(dTempObj.getRealValue())
    else:
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter an angle as a valid decimal value", "Invalid Angle Format", flag)
    
    return angleObj



def isValidDMSAngle(inputStr,flag=SHOW_ERRORS):
    """
    Validate that a string contains a valid DMS format value.
    Don't display any error messages unless flag is SHOW_ERRORS.
    
    :param str inputStr: string to be validated
    :param bool flag: whether to display error messages
    :return: an ASTAngle object containing the angle extracted from the string.
    
    The returned value for iDegrees will always be non-negative with bNeg set to true
    if the value is negative. This is because we must allow for the
    possibility of the integer degrees being -0.
    """
    angleObj = ASTAngle()
    angleObj.bValid = False             # assume invalid till we parse and prove otherwise
    iTempObj = ASTInt.ASTInt()
    dTempObj = ASTReal.ASTReal()
    
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag)
        return angleObj

    inputStr = inputStr.lower()
    angleObj.bNeg = (inputStr[0] == '-')        # account for -0d situation
 
    # Figure out where the d, m, and s values are located
    posD = inputStr.find('d')
    posM = inputStr.find('m')
    posS = inputStr.find('s')

    # First, be sure that the delimiters d, m, and s appear no more than once in the input string
    if (ASTStr.countChars(inputStr,'d') > 1) or (ASTStr.countChars(inputStr,'m') > 1) or (ASTStr.countChars(inputStr,'s') > 1):
        ASTMsg.errMsg("Only one value for Degrees, Minutes, and Seconds can be entered.\n" +
                      "Please enter an angle as a valid DMS (###d ##m ##.##s) value","Invalid Angle Format",flag)
        return angleObj
 
    # Now be sure they're in the correct order (i.e., dms) if they exist
    bValidData = True
    if (posD >= 0):                     # if true, the user specified degrees
        if (posM >= 0):
            bValidData = (posM > posD) and bValidData
        if (posS >= 0):
            bValidData = (posS > posD) and bValidData

    if (posM >= 0):                     # if true, the user specified minutes
        if (posS >= 0):
            bValidData = (posS > posM) and bValidData
  
    if (not bValidData):
        ASTMsg.errMsg("The DMS values must be entered in the correct order.\n" +
                      "Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag)
        return angleObj
 
    # At this point everything is in the correct order and appears no more than once.
    # Get the degrees, which is the first in the string if the user entered degrees.
    if (posD >= 0):
        temp = inputStr[0:posD]
        # Now throw away the degrees. No need to worry if the result is null since that would mean
        # that the user didn't enter minutes or seconds. This will be correctly caught below with posM and/or posS.
        inputStr = inputStr[posD+1:len(inputStr)]          # throw away the degrees
 
        iTempObj = ASTInt.isValidInt(temp,HIDE_ERRORS)
        if (iTempObj.isValidIntObj()):
            angleObj.iDegrees = abs(iTempObj.getIntValue())  # angleObj.bNeg already contains the sign
        else:
            ASTMsg.errMsg("The value for Degrees that was entered is invalid.\n" +
                          "Please enter a valid DMS (###d ##m ##.##s) value","Invalid Angle Format (Degrees)",flag)
            return angleObj

    # Get minutes if they were entered, and they must be after the degrees, which we've already checked
    if (posM >= 0):
        # Have to update the value of posM since inputStr may have changed in the previous step for getting degrees
        posM = inputStr.find('m')
        temp = inputStr[0:posM]        
        inputStr = inputStr[posM+1:len(inputStr)] 
 
        iTempObj = ASTInt.isValidInt(temp,HIDE_ERRORS)
        if (iTempObj.isValidIntObj()):
            angleObj.iMinutes = iTempObj.getIntValue()
        else:
            ASTMsg.errMsg("The value for Minutes that was entered is invalid.\n" +
                          "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                          "Invalid Angle Format (Minutes)",flag)
            return angleObj

    if ((angleObj.iMinutes < 0) or (angleObj.iMinutes > 59)):
        ASTMsg.errMsg("The value entered for Minutes is out of range.\n" +
                      "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                      "Invalid Angle Format (Minutes)",flag)
        return angleObj
    
    # Finally, get seconds if they were entered.
    if (posS >= 0):
        posS = inputStr.find('s')
        temp = inputStr[0:posS]
 
        dTempObj = ASTReal.isValidReal(temp,HIDE_ERRORS)
        if (dTempObj.isValidRealObj()):
            angleObj.dSeconds = dTempObj.getRealValue()
        else:
            ASTMsg.errMsg("The value for Seconds that was entered is invalid.\n" +
                          "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                          "Invalid Angle Format (Seconds)", flag)
            return angleObj
 
    if (not ((angleObj.dSeconds >= 0.0) and (angleObj.dSeconds < 60.0))):
        ASTMsg.errMsg("The value entered for Seconds is out of range.\n" +
                      "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                      "Invalid Angle Format (Seconds)", flag)
        return angleObj
    
    # Handle the pathological case where the user didn't specify d, m, or s. Default to seconds
    if (posD < 0) and (posM < 0) and (posS < 0):
        dTempObj = ASTReal.isValidReal(inputStr,HIDE_ERRORS)
        if (dTempObj.isValidRealObj()):
            angleObj.dSeconds = dTempObj.getRealValue()
        else:
            ASTMsg.errMsg("The value for Seconds that was entered is invalid.\n" +
                          "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                          "Invalid Angle Format (Seconds)", flag)
            return angleObj

    angleObj.dDecValue = DMStoDec(angleObj.bNeg,angleObj.iDegrees,angleObj.iMinutes,angleObj.dSeconds)
    angleObj.bValid = True
    return angleObj



#==============================================================================
# Define some general functions for converting an angle to a string format
#==============================================================================

#================ IMPORTANT NOTE =================================
# Python doesn't allow overloading functions and named parameters
# would be too cumbersome in this case. single-dispatch generic 
# functions are in the latest Python 3, but if we used them, it 
# would make it more difficult to back port to Python 2. So, we're
# stuck with having unique functions for each case.
#================ IMPORTANT NOTE =================================

def angleToStr_dms(neg,d,m,s,formatFlag):
    """
    Convert an angle to DMS or decimal string format
    depending upon the formatFlag.
    
    :param bool neg: true if angle is negative
    :param int d: degrees
    :param int m: minutes
    :param float s: seconds
    :param bool formatFlag: either DMSFORMAT or DECFORMAT
    :return: angle as a string
    """
    if (formatFlag == DMSFORMAT):
        return __DMStoStr_dms(neg,d,m,s)
    else:
        return ASTStr.strFormat(DEC_DEG_FMT,DMStoDec(neg,d,m,s))



def angleToStr_dec(angle,formatFlag):
    """
    Convert an angle object to DMS or decimal string format
    depending upon the formatFlag.
    
    :param float angle: angle as a decimal value
    :param bool formatFlag: either DMSFORMAT or DECFORMAT
    :return: angle as a string
    """
    return angleToStr_obj(DecToDMS(angle),formatFlag)



def angleToStr_obj(angleObj,formatFlag):
    """
    Convert an angle object to DMS or decimal string format
    depending upon the formatFlag.
    
    :param ASTAngle angleObj: angle object to be converted
    :param bool formatFlag: either DMSFORMAT or DECFORMAT
    :return: angle as a string
    """
    if (formatFlag == DMSFORMAT):
        return __DMStoStr_obj(angleObj)
    else:
        return ASTStr.strFormat(DEC_DEG_FMT,angleObj.getDecAngle())
    


#======================================================================
# Convert an angle to/from DMS and to/from decimal
#======================================================================

def DecToDMS(decValue):
    """
    Convert a decimal angle to DMS format
    
    :param float decValue: decimal angle to be converted
    :return: an angle object   
    """
    return ASTAngle(value=decValue)



def DMStoDec(neg,d,m,s):
    """
    Convert individual DMS values to a decimal value.
    
    The neg flag is required because the integer
    degrees could be 0 and we may want -0d angles.
    :param bool neg: true if the angle is negative
    :param int d: degrees
    :param int m: minutes
    :param float s: seconds
    :return: DMS value converted to a decimal value
    """
    sign = 1
    
    if (neg):
        sign = -1

    return sign*(d + (m/60.0) + (s/3600.0))



#======================================================================
# Define some private functions used only in this package
#======================================================================

def __DMStoStr_dms(neg,d,m,s):
    """
    Convert DMS format to a string.
    
    :param bool neg: true if the angle is negative
    :param int d: degrees
    :param int m: minutes
    :param float s: seconds
    :return: angle as a DMS string (xxd yym zz.zzs)
    """
    # Due to round off and truncation errors, build up the result
    # starting with s, then m, then d and worry about d, m, and s
    # values being out of bounds. So, convert s to a string, convert
    # the string back to a number, and adjust both s and m if required.
    # Similarly, handle m and do d last.

    sTmp = ASTStr.strFormat("%05.2f",s)       
    x = float(sTmp)
    if (x >= 60.0):
        sTmp = "00.00"
        m += 1
    result = sTmp + "s"

    sTmp = ASTStr.strFormat("%02d", m)
    n = int(sTmp)
    if (n >= 60):
        sTmp = "00"
        n = abs(d) + 1
    else:
        n = abs(d)
    result = sTmp + "m " + result
    
    result = ASTStr.strFormat("%d",n) + "d " + result
    if (neg):
        result = "-" + result
        
    return result  



def __DMStoStr_obj(angleObj):
    """
    Convert DMS format to a string.

    :param ASTAngle angleObj: angle as an object
    :return: angle as a DMS string (xxd yym zz.zzs)
    """
    return __DMStoStr_dms(angleObj.isNegAngle(),angleObj.getDegrees(),
                          angleObj.getMinutes(),angleObj.getSeconds())



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
