"""
This is the ASTUtils package, which contains a collection
of modules related to astronomy and to support sample
programs for the book 'Celestial Calculations.'


Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

# No initialization is needed
pass
