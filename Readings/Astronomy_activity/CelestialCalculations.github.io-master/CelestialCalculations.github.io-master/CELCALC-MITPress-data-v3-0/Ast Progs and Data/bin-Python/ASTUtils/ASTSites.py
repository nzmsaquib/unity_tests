"""
This module provides the ability to load launch sites
from a data file and to store them in an accessible
structure. The sample data file Launch-Sites.dat
is under the data files directory.
 
A launch site data file **MUST** be in the format
specifically designed for this book. The format can be
gleaned by opening the sample Launch-Sites.dat data file and examining
its contents. The format is straightforward and is
documented in comments within the data file itself.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import os, io

import ASTUtils.ASTFileIO as ASTFileIO
import ASTUtils.ASTLatLon as ASTLatLon
from ASTUtils.ASTMisc import HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTStr as ASTStr

class __SitesException(Exception):
    """Define an exception for errors while reading a Launch Sites data file"""
    pass



class ASTSites():
    """Provides a class for launch sites"""
    # Note: Launch site data bases are likely fairly small, so they aren't sorted by this class.
    # This means that any searches must be brute force through all the sites.

    #==================================================================
    # Class variables
    #==================================================================
    # File types and default directory for launch data files
    _fileExtFilter = [("Launch Sites","*.dat"),("All Files",".*")]
    _dataDir = os.path.realpath(os.path.join("..","..","Ast Data Files"))
    
    _SitesLoaded = False
    _SitesFilename = ""                            # The currently loaded sites data file
  
    _SitesDB = []            # This array will hold Launch Site data



class __LaunchSite():
    """Define a private class for the launch sites"""  
    #===============================================================
    # Class instance variables
    #===============================================================

    # sName                Launch site name
    # dLat                 Launch site latitude

    def __init__(self):
        """Create a launch site instance"""
        self.sName = ""
        self.dLat = 0.0



#=================================================================
# Define 'getter' methods to return information about
# the Launch Sites database.
#=================================================================

def getDataDir():
    """
    Gets the default data dir for Launch Sites data
    
    :return: data directory path
    """
    return ASTSites._dataDir



def getFileExtFilter():
    """
    Gets a filter for filtering Launch Sites data files by extension.
    
    :return: file extensions filter
    """  
    return ASTSites._fileExtFilter



def getSitesFilename():
    """
    Gets the filename for the currently loaded Launch Sites database.
    
    :return: filename for the Launch Sites db
    """
    return ASTSites._SitesFilename



def getSiteLat(idx):
    """
    Returns launch site latitude

    :param int idx: index into Launch Sites database
    :return: latitude object. The latitude (for valid idx values) is valid
             because it is checked during loading Launch Sites data from disk.
    """
    # Intentionally parse bad data in case there is a problem
    result = ASTLatLon.isValidLat("xyz",HIDE_ERRORS)
        
    if ((idx < 0) or (idx >= getNumSites())):
        return result
    else:
        result = ASTLatLon.ASTLatLon()
        result.setLat(ASTSites._SitesDB[idx].dLat)
     
    return result



def getSiteName(idx):
    """
    Returns launch site name
    
    :param int idx: index into Launch Sites database
    :return: site name. If idx is out of range or any other 
             error occurs, null is returned.
    """
    if ((idx < 0) or (idx >= getNumSites())):
        return None
    else:
        return ASTSites._SitesDB[idx].sName



def getNumSites():
    """
    Gets the number of Launch Sites in the currently loaded Sites database.
    
    :return: number of sites in the Launch Sites db
    """
    return len(ASTSites._SitesDB)



#============================================================
# Public methods for manipulating the Launch Sites database
#============================================================

def areSitesLoaded():
    """
    Check to see if a Launch Sites database has been loaded.
    
    :return: true if Launch Sites db has been loaded
    """
    return ASTSites._SitesLoaded



def clearLaunchSites():
    """Clear all the currently loaded Launch Sites data"""
    # Delete all of the Launch Sites data. In some languages, we'd need
    # to loop through the database and and delete objects individually.
    ASTSites._SitesDB.clear()
    del ASTSites._SitesDB[:]
    ASTSites._SitesFilename = ""
    ASTSites._SitesLoaded = False
    
    ASTSites._SitesDB = []



def getSitesFileToOpen():
    """
    Puts up a browser window and gets a Launch Sites data filename.

    :return: full name (file and path) of the Sites data file
             to open or null if no file is selected or
             file doesn't exist. This routine does **not**
             check to see if the file is a valid Sites data file, 
             but it **does** check to see that the file exists.
    """
    fileToRead = ASTFileIO.getFileToRead("Select Launch Sites Data File to Load ...",getFileExtFilter(),getDataDir())

    # See if the file exists
    if not (ASTFileIO.doesFileExist(fileToRead)):
        ASTMsg.errMsg("The Launch Sites data file specified does not exist","Launch Sites Data File Does Not Exist")
        return None
    
    return fileToRead



def findSiteName(name):
    """
    Search the currently loaded sites database and return an index into the
    database for the requested site. The search performed is **not**
    case sensitive, and a partial match is counted as successful.

    :param str name: name of the site to find
    :return: If successful, returns an index into the currently loaded sites 
             database for the object requested. If unsuccessful, -1 is 
             returned. Note that this assumes 0-based indexing!
    """
    if not (areSitesLoaded()):
        return -1
    
    targ = ASTStr.removeWhitespace(name).lower()
    for i in range(0,getNumSites()):
        strTmp = ASTStr.removeWhitespace(ASTSites._SitesDB[i].sName).lower()
        if (strTmp.find(targ) != -1):
            return i

    return -1



def loadSitesDB(filename):
    """
    Loads Launch Sites database from disk.

    :param str filename: Full filename (including path) to load
    :return: true if successful, else false.
    """
    count = 0
    Lat = 0.0
    
    clearLaunchSites()
    
    # Note that we're using 'with open' so that we don't have to explicitly close the stream when we're done
    try:      
        with io.open(filename,"rt") as br:
            # Validate that the file is indeed a Launch Sites data file
            strIn = ASTFileIO.readTillStr(br,"<LaunchSites>",HIDE_ERRORS)
            if (strIn == None):
                raise __SitesException
            
            # Look for the beginning of the data section, then cycle through
            # and extract each launch site data line.
            strIn = ASTFileIO.readTillStr(br,"<Data>")
            if (strIn == None):
                raise __SitesException
 
            for strIn in br:
                # Get rid of the line terminator, which differs between Windows (CRLF),
                # Unix (LF), MAC OS (CR).
                strIn = strIn.strip()
                
                if (strIn == None) or (len(strIn) <= 0):
                    continue                    # ignore blank lines
                
                count = count + 1

                strIn = strIn.strip()
                str2 = ASTStr.removeWhitespace(strIn.lower())
                if (str2.find("</data>") >= 0):                # found end of data section
                    break
 
                # See if this is a <Site> tag
                if not (ASTStr.compareIgnoreCase(str2,"<Site>")):
                    ASTMsg.errMsg("Expected <Site> tag but found '" + strIn + "' instead", "Invalid Tag")
                    raise __SitesException                 
 
                strIn = ASTFileIO.getSimpleTaggedValue(br, "<Name>")
                if ((strIn == None) or (len(strIn) <= 0)):
                    ASTMsg.errMsg("Expected <Name> tag but found '" + strIn + "' instead", "Missing <Name> tag")
                    raise __SitesException
                name = strIn.strip()

                strIn = ASTFileIO.getSimpleTaggedValue(br, "<Lat>")
                if ((strIn == None) or (len(strIn) <= 0)):
                    ASTMsg.errMsg("Expected <Lat> tag but found '" + strIn + "' instead", "Missing <Lat> tag")
                    raise __SitesException
                # Convert data to a string so that we can validate it is
                # a valid latitude.
                latObj = ASTLatLon.isValidLat(strIn,HIDE_ERRORS)
                if (latObj.isLatValid()):
                    Lat = latObj.getLat()
                else:
                    ASTMsg.errMsg("Invalid latitude for site " + str(count - 1) + 
                                  "\nat string [" + strIn + "]", "Invalid site latitude")
                    raise __SitesException

                obj = __LaunchSite()
                obj.sName = name
                obj.dLat = Lat
                ASTSites._SitesDB.append(obj)
 
                # Read and throw away the ending </Site> tag
                strIn = ASTFileIO.getNonBlankLine(br)
                if (strIn == None):
                    break

            br.close()                # don't really have to do this ...     
            
    except __SitesException:
        ASTMsg.errMsg("The specified file does not appear to\n" +
                      "be a valid Launch Sites data file - try again.", "Invalid Launch Sites Data File")
        br.close()                  # don't really have to do this ...
        return False  

    ASTSites._SitesFilename = filename
    ASTSites._SitesLoaded = True
    return ASTSites._SitesLoaded



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
