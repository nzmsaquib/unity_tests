"""
This class implements a class for performing coordinate
system conversions and a coordinate object.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import DECFORMAT,DMSFORMAT,AST_UNDEFINED
import ASTUtils.ASTOrbits as ASTOrbits
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime
import ASTUtils.ASTVect as ASTVect
from ASTUtils.ASTTime import adjustDatebyDays

class ASTCoord():
    """Define a class for coordinates"""
    # A generic coordinate is simply two or three angles. When an equatorial coordinate, the 1st angle
    # is actually in hours rather than degrees. See below for how the class instance variables are
    # to be used depending upon the coordinate system (e.g, equatorial, horizon) under consideration
    #=======================================================
    # Class instance variables
    #======================================================= 
    # time1
    # angle1
    # angle2
    # angle3
    
    def __init__(self,ang1=None,ang2=None,ang3=None):
        """Create an instance with the given angles"""
        if (ang1 == None):
            self.time1 = ASTTime.ASTTime(value=0.0)
            self.angle1 = ASTAngle.ASTAngle(value=0.0)
        else:         
            self.time1 = ASTTime.ASTTime(value=(ang1/15.0))
            self.angle1 = ASTAngle.ASTAngle(value=ang1)
        if (ang2 == None):            
            self.angle2 = ASTAngle.ASTAngle(value=0.0)
        else:
            self.angle2 = ASTAngle.ASTAngle(value=ang2)
        if (ang3 == None):            
            self.angle3 = ASTAngle.ASTAngle(value=0.0)
        else:
            self.angle3 = ASTAngle.ASTAngle(value=ang3)

    #===============================================================
    # Define 'get' and 'set' accessors for the object's fields
    #===============================================================
    def getAngle1(self):
        return self.angle1
    def getAngle2(self):
        return self.angle2
    def getAngle3(self):
        return self.angle3
    # We'll use angle 1 for altitude, angle 2 for azimuth when this is a horizon/topocentric coordinate
    def getAltAngle(self):
        return self.angle1
    def getAzAngle(self):
        return self.angle2
    # We'll use angle 1 for latitude, angle 2 for longitude when this
    # is an ecliptic or terrestrial coordinate
    def getLatAngle(self):
        return self.angle1
    def getLonAngle(self):
        return self.angle2
    # We'll use time1 for hour angle and RA (in hours), angle 2 for decl when this is an equatorial coordinate
    def getRAAngle(self):
        return self.time1
    def getHAAngle(self):
        return self.time1
    def getDeclAngle(self):
        return self.angle2
    # For cartesian coordinates, use the order x, y, z
    def getCartX(self):
        return self.angle1
    def getCartY(self):
        return self.angle2
    def getCartZ(self):
        return self.angle3
    # For spherical coordinates, use the order r, alpha, phi
    def getSpherR(self):
        return self.angle1
    def getSpherAlpha(self):
        return self.angle2
    def getSpherPhi(self):
        return self.angle3

    def setAngle1(self,angle):
        self.angle1.setDecAngle(angle)
    def setAngle2(self, angle):
        self.angle2.setDecAngle(angle)
    def setAngle3(self,angle):
        self.angle3.setDecAngle(angle)
    def setTime1(self,tme):
        self.time1.setDecTime(tme)
        


#=============================================================
# Define some general functions for manipulating coordinates
# and doing coordinate system conversions.
#=============================================================

def calcEclipticObliquity(Epoch):
    """
    Calculate the obliquity of the ecliptic for a given epoch
    
    :param float Epoch: epoch for which obliquity is desired
    :return: obliquity of the ecliptic calculated for the stated epoch.
    """
    iEpoch = ASTMath.Trunc(Epoch)
    
    JD = ASTDate.dateToJD_mdy(1, 0.0, iEpoch)
    dT = (JD - 2451545.0) / 36525.0
    dDE = 46.815 * dT + 0.0006 * dT * dT - 0.00181 * dT * dT * dT
    dE = 23.439292 - (dDE / 3600.0)
    return dE



def CartesianToSpherical(x, y, z):
    """
    Convert Cartesian coordinates to spherical coordinates.
    
    :param float x: x Cartesian coordinate
    :param float y: y Cartesian coordinate
    :param float z: z Cartesian coordinate
    :return: spherical coordinates with angle1 as r, angle2 as alpha, and angle3 as phi
    """
    spherCoord = ASTCoord(0.0, 0.0, 0.0)
    
    r = math.sqrt(x*x + y*y + z*z)
    alpha = ASTMath.INVTAN_D(y / x) + ASTMath.quadAdjust(y, x)
    phi = ASTMath.INVCOS_D(z / r)
    
    spherCoord.angle1.setDecAngle(r)
    spherCoord.angle2.setDecAngle(alpha)
    spherCoord.angle3.setDecAngle(phi)
    
    return spherCoord



def EclipticToEquatorial(EclLat, EclLon, Epoch):
    """
    Convert ecliptic coordinates to equatorial coordinates.
    
    :param float EclLat: ecliptic latitude
    :param float EclLon: ecliptic longitude
    :param float Epoch: epoch coordinates are in
    :return: equatorial coordinates with angle1 as the RA and angle2 as the Decl
    """
    eqCoord = ASTCoord(0.0, 0.0)
    
    dE = calcEclipticObliquity(Epoch)
    dT = ASTMath.SIN_D(EclLat) * ASTMath.COS_D(dE) + ASTMath.COS_D(EclLat) * ASTMath.SIN_D(dE) * ASTMath.SIN_D(EclLon)
    Decl = ASTMath.INVSIN_D(dT)
    dY = ASTMath.SIN_D(EclLon) * ASTMath.COS_D(dE) - ASTMath.TAN_D(EclLat) * ASTMath.SIN_D(dE)
    dX = ASTMath.COS_D(EclLon)
    dR = ASTMath.INVTAN_D(dY / dX)
    dQA = ASTMath.quadAdjust(dY, dX)
    RA = dR + dQA
    RA = RA / 15.0
    
    eqCoord.time1.setDecTime(RA)
    eqCoord.angle2.setDecAngle(Decl)
    
    return eqCoord



def HADecltoHorizon(HA, Decl, Lat):
    """
    Convert an equatorial coordinate expressed an hour angle
    and declination to a horizon coordinate.

    :param float HA: hour angle (in hours)
    :param float Decl: declination in degrees
    :param float Lat: observer latitude
    :return: a coord object in which the 1st angle is the altitude, 2nd angle is the azimuth.
    """
    horizCoord = ASTCoord(0.0, 0.0)
    
    dHA = HA * 15.0            # Convert HA to degrees
    dT = ASTMath.SIN_D(Decl) * ASTMath.SIN_D(Lat) + ASTMath.COS_D(Decl) * ASTMath.COS_D(Lat) * ASTMath.COS_D(dHA)
    Alt = ASTMath.INVSIN_D(dT)
    dT1 = ASTMath.SIN_D(Decl) - ASTMath.SIN_D(Lat) * ASTMath.SIN_D(Alt)
    dT2 = dT1 / (ASTMath.COS_D(Lat) * ASTMath.COS_D(Alt))
    Az = ASTMath.INVCOS_D(dT2)
    
    dT1 = ASTMath.SIN_D(dHA)
    if (dT1 > 0):
        Az = 360.0 - Az
    
    horizCoord.angle1.setDecAngle(Alt)
    horizCoord.angle2.setDecAngle(Az)
    
    return horizCoord



def HAtoRA(HA, LST):
    """
    Convert Hour Angle to Right Ascension
    
    :param float HA: hour angle (in hours)
    :param float LST: LST for the observer
    :return: right ascension
    """
    RA = LST - HA
    if (RA < 0.0):
        RA = RA + 24.0
    
    return RA



def HorizontoHADecl(Alt, Az, Lat):
    """
    Convert a horizon coordinate to an equatorial coordinate
    expressed as an hour angle (in hours) and declination.

    :param float Alt: altitude
    :param float Az: azimuth
    :param float Lat: observer latitude
    :return: a coord object in which the 1st angle is the hour angle (in hours),
             2nd angle is the declination
    """
    eqCoord = ASTCoord(0.0, 0.0)
    
    dTmp = ASTMath.SIN_D(Alt) * ASTMath.SIN_D(Lat) + ASTMath.COS_D(Alt) * ASTMath.COS_D(Lat) * ASTMath.COS_D(Az)
    Decl = ASTMath.INVSIN_D(dTmp)
    
    dTmp = ASTMath.SIN_D(Alt) - ASTMath.SIN_D(Lat) * ASTMath.SIN_D(Decl)
    HA = dTmp / (ASTMath.COS_D(Lat) * ASTMath.COS_D(Decl))
    HA = ASTMath.INVCOS_D(HA)
    
    dTmp = ASTMath.SIN_D(Az)
    if (dTmp > 0):
        HA = 360 - HA
    HA = HA / 15.0
    
    eqCoord.time1.setDecTime(HA)
    eqCoord.angle2.setDecAngle(Decl)
    
    return eqCoord



def HorizontoRADecl(Alt, Az, Lat, LST):
    """
    Convert a horizon coordinate to right ascension and declination.
    
    :param float Alt: altitude in degrees
    :param float Az: azimuth in degrees
    :param float Lat: observer latitude
    :param float LST: sidereal time for observer
    :return: a coord object in which the 1st angle is the right
             ascension (in hours) and the 2nd angle is the declination.
    """
    eqCoord = HorizontoHADecl(Alt, Az, Lat)
    RA = HAtoRA(eqCoord.time1.getDecTime(), LST)
    eqCoord.time1.setDecTime(RA)
    
    return eqCoord



def Keplerian2Topo(iStp,obs,h_sea,DST,epochDate,epochUT,inclin,ecc,axis,RAAN,argofperi,M0,\
                   MMotion,solveKepler,termCriteria,prt=None):
    """
    Convert satellite Keplerian elements to topocentric coordinates.
    
    :param int iStp: starting step for printing results
    :param ASTObserver obs: observer object w/date and LCT
    :param float h_sea: observer's height above sea level (meters)
    :param bool DST: true if on Daylight Saving Time
    :param ASTDate epochDate: calendar date for the epoch
    :param float epochUT: UT time for the epoch
    :param float inclin: orbital inclination
    :param float ecc: orbital eccentricity
    :param float axis: orbital semi-major axis
    :param float RAAN: orbital RAAN
    :param float argofperi: orbital argument of perigee
    :param float M0: orbital mean anomaly
    :param float MMotion: orbital mean motion
    :param TrueAnomalyType solveKepler: what method to use to solve Kepler's equation
    :param float termCriteria: termination criteria for stopping iteration to solve Kepler's equation
    :param ASTPrt prt: window for displaying intermediate results. If null, no intermediate
                       results are displayed.
    :return: horizon coordinates. The calling routine should check the altitude's value
             because it is set to AST_UNDEFINED if an error occurs.
    """   
    # Note: iStp is used so that output printed out by this function
    # can be merged in with the caller's computational steps.

    topoCoord = ASTCoord(0.0, 0.0)
    i = iStp
    dateAdjustObj = ASTInt.ASTInt()
    R = ASTVect.ASTVect(0.0,0.0,0.0)
    V = ASTVect.ASTVect(0.0,0.0,0.0)
    R_obs = ASTVect.ASTVect(0.0,0.0,0.0)
    Rp = ASTVect.ASTVect(0.0,0.0,0.0)
    Rpp = ASTVect.ASTVect(0.0,0.0,0.0)

    # Do all the time-related calculations
    LCT = obs.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, DST, obs.getObsTimeZone(), obs.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    adjustedDate = ASTTime.adjustDatebyDays(obs.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST,obs.getObsLon())

    __printlnCond(prt,str(i) + ".  Convert the observer LCT to UT, GST, and LST and adjust date if needed.")
    i = i + 1
    __printlnCond(prt,"    LCT = " + ASTTime.timeToStr_obj(obs.getObsTime(), DECFORMAT) +
                  " hours, date is " + ASTDate.dateToStr_obj(obs.getObsDate()))
    __printCond(prt,"     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        __printCond(prt,"previous day ")
    elif (dateAdjust > 0):
        __printCond(prt,"next day ")
    __printlnCond(prt,ASTDate.dateToStr_obj(adjustedDate) + ")")
    __printlnCond(prt,"    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    __printlnCond(prt,"    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    __printlnCond(prt)
    
    JDe = ASTDate.dateToJD_mdy(epochDate.getMonth(), epochDate.getdDay() + (epochUT / 24.0), epochDate.getYear())
    __printlnCond(prt,str(i) + ".  Compute the Julian day number for the epoch at which the")
    i = i + 1
    __printlnCond(prt,"    Keplerian elements were captured.")
    __printlnCond(prt,"    JDe = " + ASTStr.strFormat(ASTStyle.JDFormat,JDe))
    __printlnCond(prt)
    
    JD = ASTDate.dateToJD_mdy(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear())   
    __printlnCond(prt,str(i) + ".  Compute the Julian day number for the desired date, being sure to use the")
    i = i + 1
    __printlnCond(prt,"    Greenwich date and UT from step 3, and including the fractional part of the day.")
    __printlnCond(prt,"    JD = " + ASTStr.strFormat(ASTStyle.JDFormat,JD))
    __printlnCond(prt)
    
    deltaT = JD - JDe
    __printlnCond(prt,str(i) + ".  Compute the total number of elapsed days, including fractional days,")
    i = i + 1
    __printlnCond(prt,"    since the epoch.")
    __printlnCond(prt,"    deltaT = JD - JDe = " + ASTStr.strFormat(ASTStyle.JDFormat,JD) +
                  " - " + ASTStr.strFormat(ASTStyle.JDFormat,JDe))
    __printlnCond(prt,"           = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,deltaT) + " days")
    __printlnCond(prt)
    
    Mt = M0 + 360 * deltaT * MMotion
    __printlnCond(prt,str(i) + ".  Propagate the mean anomaly by deltaT days")
    i = i + 1
    __printlnCond(prt,"    Mt = M0 + 360*deltaT*(Mean Motion) = " + 
                  ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " + 360*" +
                  ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,deltaT) + "*(" + 
                  ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + ")")
    __printlnCond(prt,"       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Mt) + " degrees")
    __printlnCond(prt)
    
    __printlnCond(prt,str(i) + ".  If necessary, adjust Mt to be in the range [0,360]")
    i = i + 1
    __printCond(prt,"    Mt = Mt MOD 360 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Mt) + " MOD 360 = ")
    Mt = ASTMath.xMOD(Mt, 360.0)
    __printlnCond(prt,ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Mt) + " degrees")
    __printlnCond(prt)
    
    __printlnCond(prt,str(i)  + ".  Using Mt in place of M0, convert the Keplerian elements to a state vector")
    i = i + 1
    result = __Keplerian2State(inclin, ecc, axis, RAAN, argofperi, Mt, solveKepler, termCriteria, prt)
    if (result[2].x() < 0):                # conversion to a state vector failed
        __printlnCond(prt,"***An error occurred in converting the Keplerian elements to horizon coordinates")
        topoCoord.angle1.setDecAngle(AST_UNDEFINED)
        topoCoord.angle2.setDecAngle(AST_UNDEFINED)
        return topoCoord
    else:
        R = result[0]
        V = result[1]

    __printlnCond(prt,"    Resulting State Vector:")
    __printlnCond(prt,"        [" + ASTStr.insertCommas(R.x(), 3) + ", " + ASTStr.insertCommas(R.y(), 3) +
                  ", " + ASTStr.insertCommas(R.z(), 3) + "] (positional vector)")
    __printlnCond(prt,"        [" + ASTStr.insertCommas(V.x(), 3) + ", " + ASTStr.insertCommas(V.y(), 3) +
                  ", " + ASTStr.insertCommas(V.z(), 3) + "] (velocity vector)")
    __printlnCond(prt,"    State Vector gives a velocity of " + str(ASTMath.Round(V.len(), 3)) + " km/s at a distance")
    __printlnCond(prt,"    of " + ASTStr.insertCommas(R.len(), 3) + " km (" + 
                  ASTStr.insertCommas(ASTMath.KM2Miles(R.len()), 3) +
                  " miles) above the center of the Earth.")
    __printlnCond(prt)

    __printlnCond(prt,str(i) + ". Convert the positional vector (i.e., ECI coordinates) to topocentric coordinates.")
    i = i + 1
    LSTd = LST * 15.0
    obsLat = obs.getObsLat()
    # Convert obs location to ECI
    rp_e = ASTOrbits.EARTH_RADIUS + (h_sea / 1000)
    r_eq = rp_e * ASTMath.COS_D(obsLat)
    R_obs.setVect(r_eq * ASTMath.COS_D(LSTd), r_eq * ASTMath.SIN_D(LSTd), rp_e * ASTMath.SIN_D(obsLat))
    # Compute range vector
    Rp.setVect(R.x() - R_obs.x(), R.y() - R_obs.y(), R.z() - R_obs.z())
    # Rotate range vector
    x = Rp.x()*ASTMath.SIN_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.SIN_D(obsLat)*ASTMath.SIN_D(LSTd) - Rp.z()*ASTMath.COS_D(obsLat)
    y = -Rp.x()*ASTMath.SIN_D(LSTd) + Rp.y()*ASTMath.COS_D(LSTd)
    z = Rp.x()*ASTMath.COS_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.COS_D(obsLat)*ASTMath.SIN_D(LSTd) + Rp.z()*ASTMath.SIN_D(obsLat)
    Rpp.setVect(x, y, z)
    # Compute topo Alt, Az
    r_dist = Rpp.len()
    topoCoord.setAngle2(ASTMath.INVTAN2_D(-Rpp.y(), Rpp.x()))          # Azimuth
    topoCoord.setAngle1(ASTMath.INVSIN_D(Rpp.z() / r_dist))            # Altitude
    
    __printlnCond(prt,"    Alt_topo = " + ASTAngle.angleToStr_dec(topoCoord.getAltAngle().getDecAngle(), DMSFORMAT) + 
                  ", Az_topo = " + ASTAngle.angleToStr_dec(topoCoord.getAzAngle().getDecAngle(), DMSFORMAT))
    __printlnCond(prt)
    
    return topoCoord



def PrecessEqCoord(RAIn, DeclIn, EpochIn, EpochTo):
    """
    Compute a precession correction
    
    :param float RAIn: Right Ascension in hours for EpochIn
    :param float DeclIn: Declination in degrees for EpochOut
    :param float EpochIn: Epoch that RAIn/DeclIn is given in
    :param float EpochTo: Epoch to convert to
    :return: a coordinate object with the input coordinates precessed to EpochOut
    """
    eqCoord = ASTCoord(0.0, 0.0)
    
    # Convert RA to degrees
    RAdeg = RAIn * 15.0
    
    dT = (EpochTo - 1900.0) / 100.0
    dM = 3.07234 + 0.00186 * dT
    dNd = 20.0468 - 0.0085 * dT
    dNt = dNd / 15.0
    
    dDiff = (EpochTo - EpochIn)
    deltaRA = (dM + dNt * ASTMath.SIN_D(RAdeg) * ASTMath.TAN_D(DeclIn)) * dDiff
    deltaDecl = dNd * ASTMath.COS_D(RAdeg) * dDiff
    deltaRA = deltaRA / 3600.0
    deltaDecl = deltaDecl / 3600.0
    
    eqCoord.getRAAngle().setDecTime(RAIn + deltaRA)
    eqCoord.getDeclAngle().setDecAngle(DeclIn + deltaDecl)
    
    return eqCoord



def RADecltoHorizon(RA, Decl, Lat, LST):
    """
    Convert an equatorial coordinate expressed as right ascension
    and declination to a horizon coordinate.

    :param float RA: right ascension (in hours)
    :param float Decl: declination in degrees
    :param float Lat: observer latitude
    :param float LST: sidereal time for observer
    :return: a coord object in which the 1st angle is the altitude, 2nd angle is the azimuth.
    """
    return HADecltoHorizon(RAtoHA(RA, LST), Decl, Lat)



def RAtoHA(RA, LST):
    """
    Convert Right Ascension to Hour Angle
    
    :param float RA: right ascension (in hours)
    :param float LST: LST for the observer
    :return: hour angle
    """
    HA = LST - RA
    if (HA < 0.0):
        HA = HA + 24.0
    return HA



def SphericalToCartesian(r, alpha, phi):
    """
    Convert spherical coordinates to Cartesian coordinates.
    
    :param float r: radius for the spherical coordinate
    :param float alpha: alpha angle for the spherical coordinate
    :param float phi: phi angle for the spherical coordinate
    :return: Cartesian coordinates with angle1 as x, angle2 as y, and angle3 as z
    """
    cartCoord = ASTCoord(0.0, 0.0, 0.0)
    
    x = r * ASTMath.COS_D(alpha) * ASTMath.SIN_D(phi)
    y = r * ASTMath.SIN_D(alpha) * ASTMath.SIN_D(phi)
    z = r * ASTMath.COS_D(phi)
    
    cartCoord.angle1.setDecAngle(x)
    cartCoord.angle2.setDecAngle(y)
    cartCoord.angle3.setDecAngle(z)
    
    return cartCoord



#===================================================================
# Some common helper routines used only in this module
#===================================================================

def __Keplerian2State(inclin,ecc,axis,RAAN,argofperi,M0,solveKepler,termCriteria,prt):
    """
    Convert Keplerian elements to a state vector
    
    :param float inclin: orbital inclination
    :param float ecc: orbital eccentricity
    :param float axis: length of the semi-major axis
    :param float RAAN: RAAN
    :param float argofperi: argument of perigee
    :param float M0: mean anomaly
    :param TrueAnomalyType solveKepler: which method to use to solve Kepler's equation
    :param float termCriteria: when to stop iterating for Kepler's equation
    :param ASTPrt prt: area in which to display errors, if they occur
    :return: returns 3 ASTVect objects. The 1st is the position vector R, the 2nd is the 
             velocity vector V, and the third represents true (1.0) if successful and 
             false (-1.0) if not. We could use True/False for the 3rd element because
             Python allows lists to have mixed types, but the +1.0/-1.0 convention
             is used to make it easier to port to languages that do not allow mixing types.
    """
    result = [0.0]*3
    R = ASTVect.ASTVect (0.0,0.0,0.0)
    V = ASTVect.ASTVect(0.0,0.0,0.0)
    status = ASTVect.ASTVect(-1.0,-1.0,-1.0)        # assume false
    
    # compute semi-latus rectum
    rho = axis * (1.0 - ecc*ecc)
    
    # initialize as all false values
    result[0] = status
    result[1] = status
    result[2] = status
    
    # Solve Kepler's equation for the eccentric anomaly and use that to get true anomaly
    if (solveKepler == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
        tmpKepler = ASTKepler.calcSimpleKepler(M0, ecc, termCriteria,prt)
        EA = tmpKepler[0]
    else:
        tmpKepler = ASTKepler.calcNewtonKepler(M0, ecc, termCriteria,prt)
        EA = tmpKepler[0]
    dv = math.sqrt((1 + ecc) / (1 - ecc)) * ASTMath.TAN_D(EA / 2.0)
    dv = 2.0 * ASTMath.INVTAN_D(dv)
    
    # Compute the positional vector
    dLen = rho / (1 + ecc * ASTMath.COS_D(dv))
    x = dLen * ASTMath.COS_D(dv)
    y = dLen * ASTMath.SIN_D(dv)
    z = 0.0
    R.setVect(x, y, z)
    
    A = math.sqrt(ASTOrbits.muEARTH / rho)
    
    # Compute the velocity vector
    x = -A * ASTMath.SIN_D(dv)
    y = A * (ecc + ASTMath.COS_D(dv))
    z = 0.0
    V.setVect(x, y, z)
    
    # Make adjustments based on orbit type
    Otype = ASTOrbits.getSatOrbitType(ecc, inclin)
    if (Otype < 0):
        __printlnCond(prt,"Orbit must be elliptical. The computed eccentricity\nand inclination are not supported.")
        return result
    
    if ((Otype == 1) or (Otype == 2)):
        RAANp = 0.0
    else:
        RAANp = RAAN
    if ((Otype == 1) or (Otype == 3)):
        argofperip = 0.0
    else:
        argofperip = argofperi
    
    # Do rotations to xlate R from perifocal to ECI coordinates
    R = ASTMath.RotateZ(-argofperip, R)
    R = ASTMath.RotateX(-inclin, R)
    R = ASTMath.RotateZ(-RAANp, R)
     
    # Now do the same for the velocity vector
    V = ASTMath.RotateZ(-argofperip, V)
    V = ASTMath.RotateX(-inclin, V)
    V = ASTMath.RotateZ(-RAANp, V)
    
    status.setVect(1.0, 1.0, 1.0)        # true result
    result[0] = R
    result[1] = V
    result[2] = status
    return result



#==================================================================
# Provide some routines that allow printing text messages to
# a calling routine's scrollable text output area. These methods
# are provided to avoid tying this class to a particular
# chapter's GUI, but at the cost of the calling routine having to
# pass a prt reference.
#==================================================================

def __printCond(prt,txt):
    """
    Conditionally print a string of text w/o a newline
    
    :param ASTPrt prt: object that is the caller's output area
    :param str txt: text to be displayed in the output area
    """
    if (prt != None):
        prt.printnoln(txt)



def __printlnCond(prt,txt=""):
    """
    Conditionally print a string of text with a newline
    
    :param ASTPrt prt: object that is the caller's output area
    :param str txt: text to be displayed in the output area
    """
    if (prt != None):
        prt.println(txt)   



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
