"""
Create menu entries for this chapter's Constellations,
Star Catalogs, and Space Objects menus. This module also creates
a class and structure that associates data items with
menu entries to make it easier to determine what actions
to take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap1.ChapEnums
import Chap1.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    
#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what menu action to do
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what menu item is to be done
    
    def __init__(self,eCalcType):
        """
        :param CalculationType eCalcType: what menu item is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getMenuItem(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createCalcType(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what menu item is to be done
    :return: the menu object that got created
    """
    # define a listener for responding to clicks on a menu item
    menuItemCallback = lambda: ml.menuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the conversions menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=menuItemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: gui object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """
    cen = Chap1.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create the Constellations menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Constellations ",menu=tmpmenu)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="List a Constellation by ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"Name",cen.LISTCONSTBYNAME)
    __createCalcType(gui,tmpbar,"Abbreviated Name",cen.LISTCONSTBYABBREVNAME)
    __createCalcType(gui,tmpbar,"Meaning",cen.LISTCONSTBYMEANING)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"List All Constellations", cen.LISTALLCONST)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Find Constellation an Object is In", cen.FINDCONST)    

    # Create the Star Catalogs menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Star Catalogs ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu,"Clear Catalog", cen.CLEARCAT)      
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Load a Star Catalog", cen.LOADCAT)   
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Show Catalog Information", cen.SHOWCATINFO) 

    # Create the Space Objects menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Space Objects ",menu=tmpmenu)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="List a Catalog Object by ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"Name",cen.LISTOBJBYNAME)
    __createCalcType(gui,tmpbar,"Alternate Name",cen.LISTOBJBYALTNAME)
    __createCalcType(gui,tmpbar,"Catalog Comments",cen.LISTOBJBYCOMMENTS)
    tmpmenu.add_separator()
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="List All Space Objects ...",menu=tmpbar)    
    __createCalcType(gui,tmpbar,"In the Entire Catalog",cen.LISTALLOBJSINCAT)
    __createCalcType(gui,tmpbar,"In a Range",cen.LISTALLOBJSINRANGE)
    __createCalcType(gui,tmpbar,"In a Constellation",cen.LISTALLOBJSINCONST)
    tmpmenu.add_separator()
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Sort Catalog by ...",menu=tmpbar)    
    __createCalcType(gui,tmpbar,"Constellation",cen.SORTCATBYCONST)    
    __createCalcType(gui,tmpbar,"Constellation and Obj Name",cen.SORTCATBYCONSTANDOBJNAME)    
    __createCalcType(gui,tmpbar,"Object's Name",cen.SORTCATBYOBJNAME)    
    __createCalcType(gui,tmpbar,"Object's Alternate Name",cen.SORTCATBYOBJALTNAME)    
    __createCalcType(gui,tmpbar,"Object's Right Ascension",cen.SORTCATBYOBJRA)    
    __createCalcType(gui,tmpbar,"Object's Declination",cen.SORTCATBYOBJDECL)    
    __createCalcType(gui,tmpbar,"Object's Visual Magnitude",cen.SORTCATBYOBJMV)    
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
