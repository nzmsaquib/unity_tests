"""
Create menu entries for this chapter's Lunar Info and
Orbital Elements menus. This module also creates a
class and structure that associates data items with menu
entries to make it easier to determine what actions to 
take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap7.ChapEnums
import Chap7.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    


#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what menu action to do.
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what menu item is to be done
    
    def __init__(self,eCalcType):
        """
        Creates a ChapMenuItem instance
        
        :param CalculationType eCalcType: what menu item is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getCalcType(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createCalcType(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what type of conversion is to be done
    :return: the menu object that got created
    """ 
    # define a listener for responding to clicks on a menu item 
    menuItemCallback = lambda: ml.menuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=menuItemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: GUI object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """
    cen = Chap7.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create Lunar Info menu items
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Lunar Info ",menu=tmpmenu)  
    __createCalcType(gui,tmpmenu, "Moon's Position", cen.MOON_LOCATION)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu, "Moonrise/Moonset", cen.MOON_RISE_SET)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu, "Lunar Distance and Angular Diameter", cen.MOON_DIST_AND_ANG_DIAMETER)  
    __createCalcType(gui,tmpmenu, "Phase of the Moon", cen.MOON_PHASES_AND_AGE)
    __createCalcType(gui,tmpmenu, "Percent of Moon Illuminated", cen.MOON_PERCENT_ILLUMINATED);     
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu, "Set Termination Criteria", cen.TERM_CRITERIA) 
    
    # Create Orbital Elements menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Orbital Elements ",menu=tmpmenu)  
    __createCalcType(gui,tmpmenu, "Load Orbital Elements", cen.LOAD_ORBITAL_ELEMENTS)
    __createCalcType(gui,tmpmenu, "Display Moon's Orbital Elements", cen.SHOW_ORBITAL_ELEMENTS)
     


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
