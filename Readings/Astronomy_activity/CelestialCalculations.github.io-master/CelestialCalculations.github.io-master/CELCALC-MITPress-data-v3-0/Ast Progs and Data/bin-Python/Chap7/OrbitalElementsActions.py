"""
Handles the Orbital Elements menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

from ASTUtils.ASTMisc import HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits

def loadOrbitalElements(gui):
    """
    Load orbital elements from disk
       
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    gui.clearTextAreas()
    
    if (gui.checkOEDBLoaded(HIDE_ERRORS)): 
        if not (ASTMsg.pleaseConfirm("Orbital Elements have already been loaded. Are you\n" +
                                     "sure you want to load a new set of orbital elements?",
                                     "Clear Orbital Elements Data")):
            prt.println("The currently loaded orbital elements were not cleared ...")
            return
    
    fullFilename = ASTOrbits.getOEDBFileToOpen()
    if ((fullFilename == None) or (len(fullFilename) <= 0)):
        prt.println("The currently loaded orbital elements were not cleared ...")
        return
    
    if (ASTOrbits.loadOEDB(fullFilename)):
        prt.println("New orbital elements have been successfully loaded ...")
        gui.setEpoch(ASTOrbits.getOEEpochDate())
    else:
        prt.println("The currently loaded orbital elements were cleared, but")
        prt.println("new orbital elements could not be loaded from the selected file.")
        prt.println("Please try again ...")
    
    prt.resetCursor()



def showMoonsOrbitalElements(gui):
    """
    Shows the Moon's orbital elements and other data from whatever
    orbital elements database is currently loaded.

    :param tkwidget gui: GUI object from which the request came
    """ 
    prt = gui.getPrtInstance()
    
    prt.clearTextArea()
     
    if not (gui.checkOEDBLoaded()):     
        return

    ASTOrbits.displayObjOrbElements(ASTOrbits.getOEDBMoonIndex())
    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
