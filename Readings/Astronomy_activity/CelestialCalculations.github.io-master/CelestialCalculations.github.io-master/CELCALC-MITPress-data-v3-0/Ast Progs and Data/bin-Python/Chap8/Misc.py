"""
Provides some miscellaneous helper methods used throughout the
Solar System chapter

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTCoord as ASTCoord

import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HMSFORMAT, DECFORMAT
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

# Set a default termination criteria (in radians) for solving Kepler's equation
termCriteria = 0.000002
 
# These constants are used purely to make code more readable when
# asking the user for a Solar System object
GETSUN = True               # whether to ask user for the Sun
GETEARTH = True             # whether to ask user for the Earth
GETMOON = True              # whether to ask user for the Moon



def calcMiscData(gui,idx,summary):
    """
    Calculate miscellaneous data items about an object. This
    method is intended to be called twice: 1st to show how the
    calculations are done (if user wants to see intermediate
    results) and then to do a summary. It is done this way
    because mixing the summary and details makes for a
    messy combination of summary and details for the user
    to weed through.

    :param tkwidget gui: GUI object from which the request came
    :param int idx: which object is of interest
    :param bool summary: true if this is the summary pass
    """
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    idxSun = ASTOrbits.getOEDBSunIndex()
    idxMoon = ASTOrbits.getOEDBMoonIndex()
    idxEarth = ASTOrbits.getOEDBEarthIndex()
     
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    # Note that if idx = idxEarth, the "_p" variables are actually the Earth
    objName = ASTOrbits.getOEObjName(idx)
    Ecc_p = ASTOrbits.getOEObjEccentricity(idx)
    a_p = ASTOrbits.getOEObjSemiMajAxisAU(idx)
    Tp = ASTOrbits.getOEObjPeriod(idx)
    m_p = ASTOrbits.getOEObjMass(idx)
    r_p = ASTOrbits.getOEObjRadius(idx)
    RVL_p = 1.0    # This is actually calculated below, but initialized here to avoid compiler warning
    m_e = ASTOrbits.getOEObjMass(idxEarth)
    r_e = ASTOrbits.getOEObjRadius(idxEarth)
    RVL_e = ASTOrbits.calcEarthRVLength(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                        observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria)

    if (summary):
        prt.setBoldFont(True)
        prt.println("Summary of miscellaneous data items for " + objName, CENTERTXT)
        prt.setBoldFont(False)
        prt.println()
    
    prt.setFixedWidthFont()
    
    # Calculate weight on any object except the Earth
    if (idx != idxEarth):
        gui.printlnCond("**************************************")
        dT = (m_p * r_e*r_e) / (r_p*r_p * m_e)
        gui.printCond("Calculate the weight of an object on ")
        if ((idx == idxSun) or (idx == idxMoon)):
            gui.printCond("the ")
        gui.printlnCond(objName)
        gui.printlnCond("  Weight = (m_p*r_e^2)/(r^p*m_e) = (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,m_p) + " * " +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,r_e) + "^2) / (" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,r_p) + "^2 * " +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,m_e) + ")")
        gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT))
        gui.printlnCond()
        if (summary):
            prt.println("Weight on " + objName + ": Wp = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " * We")

    # Calculate transmissions delays for any object except the Sun, Earth, or Moon
    if ((idx != idxEarth) and (idx != idxSun) and (idx != idxMoon)):
        gui.printlnCond("**************************************")
        # Find the object's location and then its distance.
        eclCoord = ASTOrbits.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                  observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, 
                                                  termCriteria)
        L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT]
        RVL_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT]
        L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT]
        RVL_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT]
        dDist = ASTOrbits.calcObjDistToEarth(RVL_e, L_e, RVL_p, L_p)
        dT = 0.1384 * dDist
        gui.printCond("Calculate the transmission delay between ")
        if (idx == idxSun):
            gui.printCond("the ")
        gui.printlnCond(objName + " and the Earth.")
        gui.printlnCond("  Delay = 0.1384*Dist = 0.1384*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist))
        gui.printlnCond("        = " + ASTTime.timeToStr_dec(dT, DECFORMAT) + " hours")
        gui.printlnCond()
        if (summary):
            prt.println("Transmission delay: " + ASTTime.timeToStr_dec(dT,HMSFORMAT))
    
    # Find length of a year relative to Earth for any object except the Earth, Moon, or Sun
    if ((idx != idxEarth) and (idx != idxMoon) and (idx != idxSun)):
        gui.printlnCond("**************************************")
        dT = 365.242191 * Tp
        gui.printlnCond("Calculate length of a year on " + objName + " relative to Earth")
        gui.printlnCond("  Year = 365.242191*Tp = 365.242191 * " + str(Tp))
        gui.printlnCond("       = " + ASTStr.insertCommas(dT) + " Earth days")
        gui.printlnCond()
        if (summary):
            dT = dT / 365.25                            # convert to years
            iTmp = ASTMath.Trunc(dT)
            prt.printnoln("Length of Year: " + str(iTmp) + " years, ")
            dTmp = (dT - iTmp) * 365.25                # days
            iTmp = ASTMath.Trunc(dTmp)
            prt.printnoln(str(iTmp) + " days, ")
            dTmp = (dTmp - iTmp) * 24                # hours
            prt.println(ASTTime.timeToStr_dec(dTmp, HMSFORMAT))

    # Calculate orbital period given the semi-major axis
    gui.printlnCond("**************************************")
    dT = math.sqrt(math.pow(a_p,3))
    gui.printCond("Calculate the orbital period for ")
    if ((idx == idxSun) or (idx == idxMoon)):
        gui.printCond("the ")
    gui.printlnCond(objName + " given its semi-major axis in AUs.")
    gui.printlnCond("  Tp = sqrt(a_p^3) = sqrt(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,a_p) + "^3)")
    gui.printlnCond("     = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " tropical years")
    gui.printlnCond()
    if (summary):
        prt.println("Orbital period calculated from semi-major axis: " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " tropical years")
    
    # Calculate the semi-major axis given the orbital period for all but the Sun and Moon
    if ((idx != idxMoon) and (idx != idxSun)):
        gui.printlnCond("**************************************")
        dT = ASTMath.cuberoot(Tp * Tp)
        gui.printlnCond("Calculate the semi-major axis for " + objName)
        gui.printlnCond("given its orbital period in tropical years.")
        gui.printlnCond("  a_p = ASTMath.cuberoot(Tp^2) = ASTMath.cuberoot(" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Tp) + "^2)")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " AUs")
        gui.printlnCond()
        if (summary):
            prt.println("Semi-major axis calculated from orbital period: " +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " AUs")

    # Calculate orbital velocities for all except the Sun and Moon
    if ((idx != idxSun) and (idx != idxMoon)):
        gui.printlnCond("**************************************")
        dTmp = ASTOrbits.getOEObjGravParm(idxSun)
        dT = dTmp * (1 + Ecc_p) / (a_p * (1 - Ecc_p) * 150000000.0)
        dT = math.sqrt(dT)
        gui.printlnCond("Calculate the velocity at perihelion for " + objName)
        gui.printlnCond("  Vper = sqrt[(Mu_sun*(1+eccentricity_p))/(a_p*(1-eccentricity_p)*1.5E08)]")
        gui.printlnCond("       = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp) + "*(1+" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + "))/(" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,a_p) +
                        "*(1-" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + ")*1.5E08)]")
        gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " km/s")
        gui.printlnCond()
        if (summary):
            prt.println("Vper = " + str(ASTMath.Round(dT, 2)) + " km/s (" + 
                        str(ASTMath.Round(ASTMath.KM2Miles(dT), 2)) + " miles/s)")
    
        dT = 29.865958 * ((a_p * math.sqrt(1 - Ecc_p*Ecc_p)) / Tp)
        gui.printlnCond("Calculate the average orbital velocity for " + objName)
        gui.printlnCond("  Vavg = 29.86598*[(a_p*sqrt(1-eccentricity_p^2)/Tp]")
        gui.printlnCond("       = 29.86598*[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,a_p) + "*sqrt(1-" +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + "^2)/(" +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Tp) + "]")
        gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " km/s")
        gui.printlnCond()
        if (summary):
            prt.println("Vavg = " + str(ASTMath.Round(dT, 2)) + " km/s (" + 
                        str(ASTMath.Round(ASTMath.KM2Miles(dT), 2)) + " miles/s)")
    
        dT = dTmp * (1 - Ecc_p) / (a_p * (1 + Ecc_p) * 150000000.0)
        dT = math.sqrt(dT)
        gui.printlnCond("Calculate the velocity at aphelion for " + objName)
        gui.printlnCond("  Vaph = sqrt[(Mu_sun*(1-eccentricity_p))/(a_p*(1+eccentricity_p)*1.5E08)]")
        gui.printlnCond("       = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp) + "*(1-" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + "))/(" +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,a_p) +
                        "*(1+" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + ")*1.5E08)]")
        gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " km/s")
        gui.printlnCond()
        if (summary):
            prt.println("Vaph = " + str(ASTMath.Round(dT, 2)) + " km/s (" + 
                        str(ASTMath.Round(ASTMath.KM2Miles(dT), 2)) + " miles/s)")
    
        if (idx == idxEarth):
            RVL_x = RVL_e
        else:
            RVL_x = RVL_p
        dT = (dTmp / 150000000.0) * ((2 / RVL_x) - (1 / a_p))
        dT = math.sqrt(dT)
        gui.printlnCond("Calculate the velocity for " + objName + " for the current observer's date")
        gui.printlnCond("  Vdate = sqrt[(Mu_sun/1.5E8)*((2/Rp) - (1/a_p))]")
        gui.printlnCond("        = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp) + "/1.5E8)*((2/" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,RVL_x) + ") - (1/" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,a_p) + "))]")
        gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " km/s")
        if (summary):
            prt.println("Vdate = " + str(ASTMath.Round(dT, 2)) + " km/s (" + str(ASTMath.Round(ASTMath.KM2Miles(dT), 2)) + 
                        " miles/s)" + " Date is " + ASTDate.dateToStr_obj(observer.getObsDate()))

    # Calculate the escape velocity
    gui.printlnCond("**************************************")
    dT = ASTOrbits.getOEObjGravParm(idx)
    gui.printCond("Calculate the escape velocity for ")
    if ((idx == idxSun) or (idx == idxMoon)):
        gui.printCond("the ")
    gui.printlnCond(objName)
    gui.printlnCond("  Vescape = sqrt[(2*Mu_p)/(r_p)] = sqrt[(2*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + ")/(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,r_p) + ")]")
    dT = math.sqrt(2 * dT / r_p)
    gui.printlnCond("          = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " km/s")
    gui.printlnCond()
    if (summary):
        prt.println("Vescape = " + str(ASTMath.Round(dT, 2)) + " km/s (" + 
                    str(ASTMath.Round(ASTMath.KM2Miles(dT), 2)) + " miles/s)")



def computeLST(gui):
    """
    This function computes the LST for the observer's current location
    and time. This is needed in order to convert equatorial coordinates
    to horizon coordinates. The observer's location is retrieved
    from the GUI.

    :param tkwidget gui: GUI object from which the request came
    :return: LST for the current observer data that's in the GUI
    """
    # NOTE: It is assumed that the observer's location/time has already
    # been validated prior to this method being invoked.
  
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()
    
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    
    LST = ASTTime.GSTtoLST(GST, observer.getObsLon())
    
    return LST
 


def getSolarSysEqCoord(gui,idx):
    """
    Get the equatorial coordinates for a solar system object
    for the current observer.

    :param tkwidget gui: GUI object from which the request came
    :param int idx: index into orbital elements for the desired object,
                    which cannot be the Earth but can be any other
                    object in the orbital elements data file
    :return: returns the equatorial coordinates for the object.
    """
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()
    
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    # Get the observer's UT time
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    
    # adjust the date, if needed, since the LCTtoUT conversion could have changed the date
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    
    if (idx == ASTOrbits.getOEDBSunIndex()):
        eclCoord = ASTOrbits.calcSunEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),
                                                  adjustedDate.getYear(),UT, solveTrueAnomaly, termCriteria)
        EclLat = eclCoord[ASTOrbits.SUN_ECLLAT]
        EclLon = eclCoord[ASTOrbits.SUN_ECLLON]
    elif (idx == ASTOrbits.getOEDBMoonIndex()):
        eclCoord = ASTOrbits.calcMoonEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),
                                                   adjustedDate.getYear(), UT, solveTrueAnomaly, termCriteria)
        EclLat = eclCoord[ASTOrbits.MOON_ECLLAT]
        EclLon = eclCoord[ASTOrbits.MOON_ECLLON]
    else:
        eclCoord = ASTOrbits.calcObjEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),
                                                  adjustedDate.getYear(), UT, idx, solveTrueAnomaly, termCriteria)
        EclLat = eclCoord[ASTOrbits.OBJ_ECLLAT]
        EclLon = eclCoord[ASTOrbits.OBJ_ECLLON]
    
    return ASTCoord.EclipticToEquatorial(EclLat, EclLon, ASTOrbits.getOEEpochDate())



def getValidObj(gui,Sun,Earth,Moon):
    """
    Ask user for a Solar System object's name and return its index.
    
    :param tkwidget gui: GUI object from which the request came
    :param bool Sun: true if the Sun is allowed
    :param bool Earth: true if the Earth is allowed
    :param bool Moon: true if the Moon is allowed
    :return: -1 if object is invalid or not allowed, else returns index
             into the orbital elements array.
    """
    exclusions = ""
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return -1
    if not (gui.checkOEDBLoaded()):
        return -1
         
    # See if any objects are to be excluded
    if not Sun:
        exclusions = "Sun"
    if not Earth:
        if (len(exclusions) > 0):
            exclusions = exclusions + ", Earth"
        else:
            exclusions = "Earth"
    if not Moon:
        if (len(exclusions) > 0):
            exclusions = exclusions + ", Moon"
        else:
            exclusions = "Moon"
    if (len(exclusions) > 0):
        exclusions = "(cannot choose " + exclusions + ")"
    else:
        exclusions = "(any Solar System object can be used)"
    
    if (ASTQuery.showQueryForm(["Enter Solar System object's name\n" + exclusions]) != ASTQuery.QUERY_OK):
        return -1
    
    # validate data
    objName = ASTQuery.getData(1)
    if ((objName == None) or (len(objName) <= 0)):
        return -1
    idx = ASTOrbits.findOrbElementObjIndex(objName.strip())
    if (idx < 0):
        ASTMsg.errMsg("No Object with the name '" + objName + "' was found","Invalid Object Name")
        return -1
    
    if ((idx == ASTOrbits.getOEDBSunIndex()) and (not Sun)):
        ASTMsg.errMsg("Cannot choose the Sun", "Invalid Choice")
        return -1
    elif ((idx == ASTOrbits.getOEDBEarthIndex()) and (not Earth)):
        ASTMsg.errMsg("Cannot choose the Earth", "Invalid Choice")
        return -1
    elif ((idx == ASTOrbits.getOEDBMoonIndex()) and (not Moon)):
        ASTMsg.errMsg("Cannot choose the Moon", "Invalid Choice")
        return -1
    
    return idx



#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
