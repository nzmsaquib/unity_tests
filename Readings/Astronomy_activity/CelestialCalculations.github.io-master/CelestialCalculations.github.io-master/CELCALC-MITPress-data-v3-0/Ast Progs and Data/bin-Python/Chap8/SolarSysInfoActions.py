"""
Handles the Solar Sys Info menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import DECFORMAT,DMSFORMAT,HMSFORMAT,HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

import Chap8.Misc as Misc
import Chap8.Planets as Planets

def calcDistAndAngDiameter(gui):
    """
    Calculate an object's distance and angular diameter.

    :param tkwidget gui: GUI object from which the request came
    """ 
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    
    idx = Misc.getValidObj(gui,not Misc.GETSUN,not Misc.GETEARTH,not Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Distance from Earth and the Angular Diameter for " + objName +
                    " for the Observer's Date", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    # Technically, we should be using the UT for the observer rather than UT=0, but
    # the difference is so small that it isn't worth converting LCT to UT
    eclCoord = ASTOrbits.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                              observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, 
                                              Misc.termCriteria)
    L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT]
    R_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT]
    L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT]
    R_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT]
    gui.printlnCond("1.  Calculate the radius vector length and heliocentric ecliptic longitude")
    gui.printlnCond("    for the Earth and " + objName + " at UT = 0.0 hours")
    gui.printlnCond("    Lp = " + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " degrees, Rp = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + " AUs")
    gui.printlnCond("    Le = " + ASTAngle.angleToStr_dec(L_e,DECFORMAT) + " degrees, Re = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + " AUs")
    gui.printlnCond()

    dDist = ASTOrbits.calcObjDistToEarth(R_e, L_e, R_p, L_p)
    gui.printlnCond("2.  Calculate the distance from " + objName + " to the Earth")
    gui.printlnCond("    Dist = sqrt[Re^2 + Rp^2 - 2*Re*Rp*cos(Lp - Le)]")
    gui.printlnCond("         = sqrt[" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + "^2 + " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "^2 - 2*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + "*" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) +
                    "*cos(" + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " - " + 
                    ASTAngle.angleToStr_dec(L_e, DECFORMAT) + ")]")
    gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + " AUs")
    gui.printlnCond()
    
    Theta = ASTOrbits.getOEObjAngDiamArcSec(idx) / dDist
    gui.printlnCond("3.  Compute the angular diameter for " + objName + ".")
    gui.printlnCond("    Theta = Theta_p/Dist = " + 
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjAngDiamArcSec(idx), DECFORMAT) +
                    "/" +ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + " = " + 
                    ASTAngle.angleToStr_dec(Theta, DECFORMAT) + " arcseconds")
    gui.printlnCond("    (Note: distance must be in AUs and Theta_p must be in arcseconds)")
    gui.printlnCond()

    dDist_km = ASTMath.Round(ASTMath.AU2KM(dDist), 0)
    Theta = Theta / 3600.0     # Convert from arcseconds to degrees
    gui.printlnCond("4.  Convert the distance to km or miles, and angular diameter to DMS format.")
    gui.printlnCond("    Dist = " + ASTStr.insertCommas(dDist_km) + " km = " + 
                    ASTStr.insertCommas(ASTMath.KM2Miles(dDist_km), 0) + " miles")
    gui.printlnCond("    Theta = " + ASTAngle.angleToStr_dec(Theta, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("On " + ASTDate.dateToStr_obj(observer.getObsDate()) + ", " + objName + 
                    " is/was " + ASTStr.insertCommas(dDist_km) + " km away")
    gui.printlnCond("and its angular diameter is/was " + ASTAngle.angleToStr_dec(Theta, DMSFORMAT))
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Distance to " + objName + ": " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) +
                   " AUs (" + ASTStr.insertCommas(dDist_km) + " km, " + 
                   ASTStr.insertCommas(ASTMath.KM2Miles(dDist_km), 0) +
                   " miles), Angular Diameter: " + ASTAngle.angleToStr_dec(Theta, DMSFORMAT))



def calcObjIllumination(gui):
    """
    Calculate how much of an object is illuminated.
    
    :param tkwidget gui: GUI object from which the request came
    """ 
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    
    idx = Misc.getValidObj(gui,not Misc.GETSUN,not Misc.GETEARTH,not Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute how much " + objName + " is illuminated as seen from Earth for the Observer's Date", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    # Technically, we should be using the UT for the observer rather than UT=0, but
    # the difference is so small that it isn't worth converting LCT to UT
    eclCoord = ASTOrbits.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                              observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly,
                                              Misc.termCriteria)
    L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT]
    R_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT]
    L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT]
    R_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT]
    gui.printlnCond("1.  Calculate the radius vector length for the Earth and " + objName + " at UT = 0.0 hours")
    gui.printlnCond("    Rp = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + " AUs, Re = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + " AUs")
    gui.printlnCond()

    dDist = ASTOrbits.calcObjDistToEarth(R_e, L_e, R_p, L_p)
    gui.printlnCond("2.  Calculate the distance from " + objName + " to the Earth")
    gui.printlnCond("    Dist = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + " AUs")
    gui.printlnCond()
    
    dKpcnt = (math.pow((R_p + dDist),2) - R_e*R_e) / (4 * R_p * dDist)
    dKpcnt = 100 * dKpcnt
    gui.printlnCond("3.  Calculate the percent illumination.")
    gui.printlnCond("    K% = 100*[((Rp + Dist)^2 - Re^2)/(4*Rp*Dist)]")
    gui.printlnCond("       = 100*[((" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + " + " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + ")^2 - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + "^2)/(4*" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + ")]")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dKpcnt))
    gui.printlnCond()
    
    gui.printlnCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), 
                                                        observer.getObsDate().getiDay(),
                                                        observer.getObsDate().getYear()) +
                    ", " + objName + " is/was " + str(ASTMath.Round(dKpcnt, 1)) + "% illuminated as seen from Earth")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("On " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), 
                                                 observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
                   ", " + objName + " is/was " + str(ASTMath.Round(dKpcnt, 1)) + "% illuminated.")



def calcObjMagnitude(gui):
    """
    Calculate an object's (apparent) visual magnitude.
    
    :param tkwidget gui: GUI object from which the request came
    """ 
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    
    idx = Misc.getValidObj(gui,not Misc.GETSUN,not Misc.GETEARTH,not Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the visual magnitude for " + objName, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    # Technically, we should be using the UT for the observer rather than UT=0, but
    # the difference is so small that it isn't worth converting LCT to UT
    eclCoord = ASTOrbits.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                              observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly,
                                              Misc.termCriteria)
    L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT]
    R_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT]
    L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT]
    R_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT]
    Lon_p = eclCoord[ASTOrbits.OBJ_ECLLON]
    gui.printlnCond("1.  Calculate the object's position at UT = 0.0 hours to get its heliocentric (Lp)")
    gui.printlnCond("    and geocentric longitude (Lon_p), and its radius vector length (Rp).")
    gui.printlnCond("    Lp = " + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " degrees")
    gui.printlnCond("    Lon_p = " + ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " degrees")
    gui.printlnCond("    Rp = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + " AUs")
    gui.printlnCond()

    dDist = ASTOrbits.calcObjDistToEarth(R_e, L_e, R_p, L_p)
    gui.printlnCond("2.  Calculate the distance from " + objName + " to the Earth")
    gui.printlnCond("    Dist = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + " AUs")
    gui.printlnCond()
    
    dPA = (1 + ASTMath.COS_D(Lon_p - L_p)) / 2.0
    gui.printlnCond("3.  Calculate the object's phase angle.")
    gui.printlnCond("    PA = [1 + cos(Lon_p - Lp)]/2 = [1 + cos(" + ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(L_p, DECFORMAT) + ")]/2]")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(dPA, DECFORMAT) + " degrees")
    gui.printlnCond("    (The elongation is Lon_p - Lp = " + ASTAngle.angleToStr_dec(Lon_p - L_p, DECFORMAT) + " degrees)")
    gui.printlnCond()

    Vp = ASTOrbits.getOEObjmV(idx)
    mV = Vp + 5 * math.log10((R_p * dDist) / math.sqrt(dPA))
    gui.printlnCond("4.  Calculate the object's visual magnitude.")
    gui.printlnCond("    mV = Vp + 5*Log10[(Rp*Dist)/sqrt(PA)]")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Vp) + " + 5*Log10[(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "*" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDist) + ")/sqrt(" + 
                    ASTAngle.angleToStr_dec(dPA, DECFORMAT) + ")]")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.mVFORMAT,mV))
    gui.printlnCond()
    
    gui.printCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), 
                                                      observer.getObsDate().getiDay(),
                                                      observer.getObsDate().getYear()) + 
                  ", the visual magnitude for ")
    gui.printlnCond(objName + " is/was " + ASTStr.strFormat(ASTStyle.mVFORMAT,mV))
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("On " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), 
                                                 observer.getObsDate().getiDay(),
                                                 observer.getObsDate().getYear()) +
                   ", for " + objName + " mV = " + ASTStr.strFormat(ASTStyle.mVFORMAT,mV) + " at 1 AU")



def calcObjMiscData(gui):
    """
    Calculate miscellaneous data items about an object.
    
    :param tkwidget gui: GUI object from which the request came
    """ 
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    
    idx = Misc.getValidObj(gui,Misc.GETSUN,Misc.GETEARTH,Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute miscellaneous data items for " + objName, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    # Show how calculations were done, but only if needed
    if (gui.getShowInterimCalcsStatus()):
        Misc.calcMiscData(gui, idx, False)

    # Now do the summary, but temporarily set checkbox to false
    # so that we don't do detailed display again
    saveCBStatus = gui.getShowInterimCalcsStatus()
    gui.setShowInterimCalcsStatus(False)
    prt.setProportionalFont()
    Misc.calcMiscData(gui, idx, True)
    gui.setShowInterimCalcsStatus(saveCBStatus)
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    result = "Misc data items for "
    if ((idx == ASTOrbits.getOEDBMoonIndex()) or (idx == ASTOrbits.getOEDBSunIndex())):
        result = result + "the "
    gui.setResults(result + objName + " are given in the text area below")


def calcObjPeriAphDist(gui):
    """
    Calculate the distance an object will be from the Sun when
    it passes through perhelion/aphelion.
    
    :param tkwidget gui: GUI object from which the request came
    """ 
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()

    idx = Misc.getValidObj(gui,not Misc.GETSUN,Misc.GETEARTH,not Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    prt.setBoldFont(True)
    gui.printlnCond("Determine distance from the Sun when " + objName + " passes through Perihelion/Aphelion", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    a_p = ASTOrbits.getOEObjSemiMajAxisAU(idx)
    e_p = ASTOrbits.getOEObjEccentricity(idx)
    
    Distper = a_p * (1 - e_p)
    gui.printlnCond("1.  Compute the object's distance at perihelion.")
    gui.printlnCond("    Dist_per = a_p * (1 - eccentricity_p) = " + str(a_p) + " * (1 - " + str(e_p) + ")")
    gui.printlnCond("             = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Distper) + " AUs")
    gui.printlnCond()
    
    Distaph = a_p * (1 + e_p)
    gui.printlnCond("2.  Compute the object's distance at aphelion.")
    gui.printlnCond("    Dist_aph = a_p * (1 + eccentricity_p) = " + str(a_p) + " * (1 + " + str(e_p) + ")")
    gui.printlnCond("             = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Distaph) + " AUs")
    gui.printlnCond()

    Distper_km = ASTMath.Round(ASTMath.AU2KM(Distper), 0)
    Distaph_km = ASTMath.Round(ASTMath.AU2KM(Distaph), 0)
    gui.printlnCond("3.  Convert the distances to km or miles, if desired.")
    gui.printlnCond("    Dist_per = " + ASTStr.insertCommas(Distper_km) + " km = " +
                    ASTStr.insertCommas(ASTMath.KM2Miles(Distper_km), 0) + " miles")
    gui.printlnCond("    Dist_aph = " + ASTStr.insertCommas(Distaph_km) + " km = " +
                    ASTStr.insertCommas(ASTMath.KM2Miles(Distaph_km), 0) + " miles")
    gui.printlnCond()
    
    gui.printlnCond(objName + " is " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Distper) + " AUs (" +
                    ASTStr.insertCommas(Distper_km) + " km) from the Sun at perihelion")
    gui.printlnCond("and " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Distaph) + " AUs (" +
                    ASTStr.insertCommas(Distaph_km) + " km) from the Sun at aphelion")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(objName + " Perihelion Dist: " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Distper) +
                   " AUs (" + ASTStr.insertCommas(Distper_km) + " km), Aphelion Dist: " + 
                   ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Distaph) + " AUs (" + ASTStr.insertCommas(Distaph_km) + " km)")



def calcObjPosition(gui):
    """
    Calculate the position of an object, using the currently
    loaded orbital elements and the current observer position.
    The object can be anything in the orbital elements file
    except the Sun, Moon, or Earth.

    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    prtObj = None
    dateAdjustObj = ASTInt.ASTInt()
    idxEarth = ASTOrbits.getOEDBEarthIndex()
    
    gui.clearTextAreas()
    
    idx = Misc.getValidObj(gui,not Misc.GETSUN,not Misc.GETEARTH,not Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    # Set up whether to display interim results when solving Kepler's equation
    if (gui.getShowInterimCalcsStatus()):
        prtObj = prt
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Position of " + objName + " for the Current Observer", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    Ecc_p = ASTOrbits.getOEObjEccentricity(idx)
    inclin_p = ASTOrbits.getOEObjInclination(idx)
    Ecc_e = ASTOrbits.getOEObjEccentricity(idxEarth)
    inclin_e = ASTOrbits.getOEObjInclination(idxEarth)
    
    # Do all the time-related calculations
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST,observer.getObsLon())
    
    gui.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
    gui.printlnCond("    LCT = " + ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) +
                    " hours, date is " + ASTDate.dateToStr_obj(observer.getObsDate()) + ")")
    gui.printCond("     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        gui.printCond("previous day ")
    elif (dateAdjust > 0):
        gui.printCond("next day ")
    gui.printlnCond(ASTDate.dateToStr_obj(adjustedDate) + ")")
    gui.printlnCond("    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond("    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()

    JDe = ASTOrbits.getOEEpochJD()
    gui.printlnCond("2.  Compute the Julian day number for the standard epoch.")
    gui.printlnCond("    Epoch: " + ASTStr.strFormat(ASTStyle.EPOCHFORMAT,ASTOrbits.getOEEpochDate()))
    gui.printlnCond("    JDe = " + ASTStr.strFormat(ASTStyle.JDFormat,JDe))
    gui.printlnCond()
    
    JD = ASTDate.dateToJD_mdy(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear())
    gui.printlnCond("3.  Compute the Julian day number for the desired date, being sure to use the")
    gui.printlnCond("    Greenwich date and UT from step 1, and including the fractional part of the day.")
    gui.printlnCond("    JD = " + ASTStr.strFormat(ASTStyle.JDFormat,JD))
    gui.printlnCond()
    
    De = JD - JDe
    gui.printlnCond("4.  Compute the total number of elapsed days, including fractional days,")
    gui.printlnCond("    since the standard epoch.")
    gui.printlnCond("    De = JD - JDe = " + ASTStr.strFormat(ASTStyle.JDFormat,JD) + 
                    " - " + ASTStr.strFormat(ASTStyle.JDFormat,JDe))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De) + " days")
    gui.printlnCond()

    # Calculate the object's mean and true anomalies
    M_p = (360 * De) / (365.242191 * ASTOrbits.getOEObjPeriod(idx)) + ASTOrbits.getOEObjLonAtEpoch(idx) -\
          ASTOrbits.getOEObjLonAtPeri(idx)
    gui.printlnCond("5.  Compute the mean anomaly for " + objName)
    gui.printlnCond("    Mp = (360*De)/(365.242191*Tp) + Ep - Wp")
    gui.printlnCond("       = (360*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De) + ")/(365.242191*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTOrbits.getOEObjPeriod(idx)) + " + " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtEpoch(idx), DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtPeri(idx), DECFORMAT))
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(M_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("6.  If necessary, adjust Mp so that it falls in the range [0,360]")
    gui.printCond("    Mp = Mp MOD 360 = " + ASTAngle.angleToStr_dec(M_p, DECFORMAT) + " MOD 360 = ")
    M_p = ASTMath.xMOD(M_p, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(M_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    # Find the true anomaly from equation of center or Kepler's equation
    if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER):
        Ec_p = (360.0 / math.pi) * Ecc_p * ASTMath.SIN_D(M_p)
        gui.printlnCond("7.  Solve the equation of the center for " + objName + ".")
        gui.printlnCond("    Ep = (360/pi) * eccentricity_p * sin(Mp)")
        gui.printlnCond("       = (360/3.1415927) * " +
                        str(Ecc_p) + " * sin(" + ASTAngle.angleToStr_dec(M_p, DECFORMAT) + ")")
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Ec_p, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        v_p = M_p + Ec_p
        gui.printlnCond("8.  Add Ep to Mp to get the true anomaly.")
        gui.printlnCond("    vp = Mp + Ep = " + ASTAngle.angleToStr_dec(M_p, DECFORMAT) + " + " + 
                        ASTAngle.angleToStr_dec(Ec_p, DECFORMAT))
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(v_p, DECFORMAT) + " degrees")
    else:
        gui.printlnCond("7.  Solve Kepler's equation to get the eccentric anomaly.")
        if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
            tmpKepler = ASTKepler.calcSimpleKepler(M_p, Ecc_p, Misc.termCriteria,prtObj)
            Ea_p = tmpKepler[0]
            iteration = int(tmpKepler[1])
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(M_p, Ecc_p, Misc.termCriteria,prtObj)
            Ea_p = tmpKepler[0]
            iteration = int(tmpKepler[1])
        gui.printlnCond("    Eap = E" + str(iteration) + " = " + ASTAngle.angleToStr_dec(Ea_p, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        v_p = (1 + Ecc_p) / (1 - Ecc_p)
        v_p = math.sqrt(v_p) * ASTMath.TAN_D(Ea_p / 2.0)
        v_p = 2.0 * ASTMath.INVTAN_D(v_p)
        gui.printlnCond("8.  vp = 2 * inv tan[ sqrt[(1+eccentricity_p)/(1-eccentricity_p)] * tan(Eap/2) ]")
        gui.printlnCond("       = 2 * inv tan[ sqrt[(1+" + str(Ecc_p) + ")/(1-" + str(Ecc_p) + ")] * tan(" +
                        ASTAngle.angleToStr_dec(Ea_p, DECFORMAT) + "/2) ]")
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(v_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    # Calculate the object's heliocentric ecliptic coordinates (L_p, H_p)
    # and radius vector length (R_p)
    L_p = v_p + ASTOrbits.getOEObjLonAtPeri(idx)
    gui.printlnCond("9.  Calculate the object's heliocentric longitude.")
    gui.printlnCond("    Lp = vp + Wp = " + ASTAngle.angleToStr_dec(v_p, DECFORMAT) + " + " + 
                    str(ASTOrbits.getOEObjLonAtPeri(idx)))
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("10. If necessary, adjust Lp to be in the range [0,360]")
    gui.printCond("    Lp = Lp MOD 360 = " + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " MOD 360 = ")
    L_p = ASTMath.xMOD(L_p, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    H_p = ASTMath.SIN_D(L_p - ASTOrbits.getOEObjLonAscNode(idx)) * ASTMath.SIN_D(inclin_p)
    H_p = ASTMath.INVSIN_D(H_p)
    gui.printlnCond("11. Compute the object's heliocentric latitude.")
    gui.printlnCond("    Hp = inv sin[sin(Lp-Omega_p)*sin(inclination_p)]")
    gui.printlnCond("       = inv sin[sin(" + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAscNode(idx), DECFORMAT) + ")*sin(" +
                    ASTAngle.angleToStr_dec(inclin_p, DECFORMAT) + ")]")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(H_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("12. If necessary, adjust Hp to be in the range [0,360]")
    gui.printCond("    Hp = Hp MOD 360 = " + ASTAngle.angleToStr_dec(H_p, DECFORMAT) + " MOD 360 = ")
    H_p = ASTMath.xMOD(H_p, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(H_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    R_p = ASTOrbits.getOEObjSemiMajAxisAU(idx) * (1 - Ecc_p * Ecc_p)
    R_p = R_p / (1 + Ecc_p * ASTMath.COS_D(v_p))
    gui.printlnCond("13. Compute the object's radius vector length")
    gui.printlnCond("    Rp = [a_p*(1-eccentricity_p^2)]/[1+eccentricity_p*cos(vp)]")
    gui.printlnCond("       = [" + str(ASTOrbits.getOEObjSemiMajAxisAU(idx)) + "*(1-" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + "^2)]/[1+" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_p) + "*cos(" + 
                    ASTAngle.angleToStr_dec(v_p, DECFORMAT) + ")]")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + " AUs")
    gui.printlnCond()
    
    # Repeat the steps just done for the object for the Earth
    
    # Calculate the Earth's mean and true anomalies
    M_e = (360 * De) / (365.242191 * ASTOrbits.getOEObjPeriod(idxEarth)) + ASTOrbits.getOEObjLonAtEpoch(idxEarth) -\
          ASTOrbits.getOEObjLonAtPeri(idxEarth)
    gui.printlnCond("14. Compute the mean anomaly for the Earth")
    gui.printlnCond("    Me = (360*De)/(365.242191*Te) + Ee - We")
    gui.printlnCond("       = (360*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De) + ")/(365.242191*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTOrbits.getOEObjPeriod(idxEarth)) + " + " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtEpoch(idxEarth), DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtPeri(idxEarth), DECFORMAT))
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(M_e, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("15. If necessary, adjust Me so that it falls in the range [0,360]")
    gui.printCond("    Me = Me MOD 360 = " + ASTAngle.angleToStr_dec(M_e, DECFORMAT) + " MOD 360 = ")
    M_e = ASTMath.xMOD(M_e, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(M_e, DECFORMAT) + " degrees")
    gui.printlnCond()

    # Find the Earth's true anomaly from equation of center or Kepler's equation
    if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER):
        Ec_e = (360.0 / math.pi) * Ecc_e * ASTMath.SIN_D(M_e)
        gui.printlnCond("16. Solve the equation of the center for the Earth.")
        gui.printlnCond("    Ee = (360/pi) * eccentricity_e * sin(Me) = (360/3.1415927) * " +
                        str(Ecc_e) + " * sin(" + ASTAngle.angleToStr_dec(M_e, DECFORMAT) + ")")
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Ec_e, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        v_e = M_e + Ec_e
        gui.printlnCond("17. Add Ee to Me to get the Earth's true anomaly.")
        gui.printlnCond("    ve = Me + Ee = " + ASTAngle.angleToStr_dec(M_e, DECFORMAT) + " + " + 
                        ASTAngle.angleToStr_dec(Ec_e, DECFORMAT))
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(v_e, DECFORMAT) + " degrees")
    else:
        gui.printlnCond("16. Solve Kepler's equation to get the Earth's eccentric anomaly.")
        if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
            tmpKepler = ASTKepler.calcSimpleKepler(M_e, Ecc_e, Misc.termCriteria,prtObj)
            Ea_e = tmpKepler[0]
            iteration = int(tmpKepler[1])
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(M_e, Ecc_e, Misc.termCriteria,prtObj)
            Ea_e = tmpKepler[0]
            iteration = int(tmpKepler[1])
        gui.printlnCond("    Eae = E" + str(iteration) + " = " + ASTAngle.angleToStr_dec(Ea_e, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        v_e = (1 + Ecc_e) / (1 - Ecc_e)
        v_e = math.sqrt(v_e) * ASTMath.TAN_D(Ea_e / 2.0)
        v_e = 2.0 * ASTMath.INVTAN_D(v_e)
        gui.printlnCond("17. ve = 2 * inv tan[ sqrt[(1+eccentricity_e)/(1-eccentricity_e)] * tan(Eae/2) ]")
        gui.printlnCond("       = 2 * inv tan[ sqrt[(1+" + str(Ecc_e) + ")/(1-" + str(Ecc_e) + ")] * tan(" +
                        ASTAngle.angleToStr_dec(Ea_e, DECFORMAT) + "/2) ]")
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(v_e, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    # Calculate the Earth's heliocentric ecliptic coordinates (L_e, H_e)
    # and radius vector length (R_e)
    L_e = v_e + ASTOrbits.getOEObjLonAtPeri(idxEarth)
    gui.printlnCond("18. Calculate the Earth's heliocentric longitude.")
    gui.printlnCond("    Le = ve + We = " + ASTAngle.angleToStr_dec(v_e, DECFORMAT) + " + " + 
                    str(ASTOrbits.getOEObjLonAtPeri(idxEarth)))
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("19. If necessary, adjust Le to be in the range [0,360]")
    gui.printCond("    Le = Le MOD 360 = " + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " MOD 360 = ")
    L_e = ASTMath.xMOD(L_e, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    H_e = ASTMath.SIN_D(L_e - ASTOrbits.getOEObjLonAscNode(idxEarth)) * ASTMath.SIN_D(inclin_e)
    H_e = ASTMath.INVSIN_D(H_e)
    gui.printlnCond("20. Compute the Earth's heliocentric latitude.")
    gui.printlnCond("    He = inv sin[sin(Le-Omega_e)*sin(inclination_e)]")
    gui.printlnCond("       = inv sin[sin(" + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAscNode(idxEarth), DECFORMAT) + ")*sin(" +
                    ASTAngle.angleToStr_dec(inclin_e, DECFORMAT) + ")]")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(H_e, DECFORMAT) + " degrees")
    gui.printlnCond()

    gui.printlnCond("21. If necessary, adjust He to be in the range [0,360]")
    gui.printCond("    He = He MOD 360 = " + ASTAngle.angleToStr_dec(H_e, DECFORMAT) + " MOD 360 = ")
    H_e = ASTMath.xMOD(H_e, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(H_e, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    R_e = ASTOrbits.getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e)
    R_e = R_e / (1 + Ecc_e * ASTMath.COS_D(v_e))
    gui.printlnCond("22. Compute the Earth's radius vector length")
    gui.printlnCond("    Re = [a_e*(1-eccentricity_e^2)]/[1+eccentricity_e*cos(ve)]")
    gui.printlnCond("       = [" + str(ASTOrbits.getOEObjSemiMajAxisAU(idxEarth)) + "*(1-" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_e) + "^2)]/[1+" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc_e) + "*cos(" + 
                    ASTAngle.angleToStr_dec(v_e, DECFORMAT) + ")]")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + " AUs")
    gui.printlnCond()
    
    # Given the heliocentric location and radius vector length for both the Earth
    # and the object, project the object's location onto the ecliptic plane
    # with respect to the Earth

    y = ASTMath.SIN_D(L_p - ASTOrbits.getOEObjLonAscNode(idx)) * ASTMath.COS_D(inclin_p)
    x = ASTMath.COS_D(L_p - ASTOrbits.getOEObjLonAscNode(idx))
    gui.printlnCond("23. Calculate an adjustment to the object's ecliptic longitude.")
    gui.printlnCond("    y = sin(Lp - Omega_p)*cos(inclination_p)")
    gui.printlnCond("      = sin(" + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAscNode(idx), DECFORMAT) + ")*cos(" + 
                    ASTAngle.angleToStr_dec(inclin_p, DECFORMAT) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y))
    gui.printlnCond("    x = cos(Lp - Omega_p) = cos(" + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAscNode(idx), DECFORMAT) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x))
    gui.printlnCond()
    
    dT = ASTMath.INVTAN_D(y / x)
    gui.printlnCond("24. Compute T = inv tan(y/x) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) + 
                    "/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) + ") = " + 
                    ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
    gui.printlnCond()

    x = ASTMath.quadAdjust(y, x)
    gui.printlnCond("25. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
    gui.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + 
                  " + " + ASTAngle.angleToStr_dec(x, DECFORMAT) + " = ")
    dT = dT + x
    gui.printlnCond(ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Lp_p = ASTOrbits.getOEObjLonAscNode(idx) + dT
    gui.printlnCond("26. Finish computing the object's adjustment for the ecliptic longitude.")
    gui.printlnCond("    Lp_prime = Omega_p + T = " + ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAscNode(idx), DECFORMAT) + " + " +
                    ASTAngle.angleToStr_dec(dT, DECFORMAT) + " = " + ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("27. If necessary, adjust Lp_prime to be in the range [0,360]")
    gui.printCond("    Lp_prime = Lp_prime MOD 360 = " + ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + " MOD 360 = ")
    Lp_p = ASTMath.xMOD(Lp_p, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + " degrees")
    gui.printlnCond()

    # See if this is an inferior or superior object and compute accordingly
    if (ASTOrbits.getOEObjInferior(idx)):
        y = R_p * ASTMath.COS_D(H_p) * ASTMath.SIN_D(L_e - Lp_p)
        x = R_e - R_p * ASTMath.COS_D(H_p) * ASTMath.COS_D(L_e - Lp_p)
        gui.printlnCond("28. This is an inferior object, so compute its geocentric ecliptic longitude.")
        gui.printlnCond("    y = Rp*cos(Hp)*sin(Le - Lp_prime)")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "*cos(" +
                        ASTAngle.angleToStr_dec(H_p, DECFORMAT) + ")*sin(" + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " - " +
                        ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + ")")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y))
        gui.printlnCond("    x = Re - Rp*cos(Hp)*cos(Le - Lp_prime)")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + " - " +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "*cos(" + ASTAngle.angleToStr_dec(H_p,DECFORMAT) +
                        ")*cos(" + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " - " + 
                        ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + ")")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x))
        gui.printlnCond()
    
        dT = ASTMath.INVTAN_D(y / x)
        gui.printlnCond("29. Compute T = inv tan(y/x) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) + 
                        "/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) +
                        ") = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        x = ASTMath.quadAdjust(y, x)
        gui.printlnCond("30. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
        gui.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + " + " + 
                      ASTAngle.angleToStr_dec(x, DECFORMAT) + " = ")
        dT = dT + x
        gui.printlnCond(ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        Lon_p = 180 + L_e + dT
        gui.printlnCond("31. Finish computing the inferior object's geocentric ecliptic longitude.")
        gui.printlnCond("    Lon_p = 180 + Le + T = 180 + " + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + 
                        " + " + ASTAngle.angleToStr_dec(dT, DECFORMAT) +
                        " = " + ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        gui.printlnCond("32. If necessary, adjust Lon_p to be in the range [0,360]")
        gui.printCond("    Lon_p = Lon_p MOD 360 = " + ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " MOD 360 = ")
        Lon_p = ASTMath.xMOD(Lon_p, 360.0)
        gui.printlnCond(ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " degrees")
    else:
        y = R_e * ASTMath.SIN_D(Lp_p - L_e)
        x = R_p * ASTMath.COS_D(H_p) - R_e * ASTMath.COS_D(L_e - Lp_p)
        gui.printlnCond("28. This is a superior object, so compute its geocentric ecliptic longitude.")
        gui.printlnCond("    y = Re*sin(Lp_prime - Le) = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + "*sin(" +
                ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + " - " + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + ")")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y))
        gui.printlnCond("    x = Rp*cos(Hp) - Re*cos(Le - Lp_prime)")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "*cos(" +
                        ASTAngle.angleToStr_dec(H_p, DECFORMAT) + ") - " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + 
                        "*cos(" + ASTAngle.angleToStr_dec(L_e, DECFORMAT) + " - " + 
                        ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + ")")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x))
        gui.printlnCond()
    
        dT = ASTMath.INVTAN_D(y / x)
        gui.printlnCond("29. Compute T = inv tan(y/x) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) + 
                        "/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) + ") = " + 
                        ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        x = ASTMath.quadAdjust(y, x)
        gui.printlnCond("30. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
        gui.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + 
                      " + " + ASTAngle.angleToStr_dec(x, DECFORMAT) + " = ")
        dT = dT + x
        gui.printlnCond(ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        Lon_p = Lp_p + dT
        gui.printlnCond("31. Finish computing the superior object's geocentric ecliptic longitude.")
        gui.printlnCond("    Lon_p = Lp_prime + T = " + ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + 
                        " + " + ASTAngle.angleToStr_dec(dT, DECFORMAT) +
                        " = " + ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        gui.printlnCond("32. If necessary, adjust Lon_p to be in the range [0,360]")
        gui.printCond("    Lon_p = Lon_p MOD 360 = " + ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " MOD 360 = ")
        Lon_p = ASTMath.xMOD(Lon_p, 360.0)
        gui.printlnCond(ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Lat_p = R_p * ASTMath.COS_D(H_p) * ASTMath.TAN_D(H_p) * ASTMath.SIN_D(Lon_p - Lp_p)
    Lat_p = Lat_p / (R_e * ASTMath.SIN_D(Lp_p - L_e))
    Lat_p = ASTMath.INVTAN_D(Lat_p)
    gui.printlnCond("33. Compute the object's geocentric ecliptic latitude.")
    gui.printlnCond("    Lat_p = inv tan[(Rp*cos(Hp)*tan(Hp)*sin(Lon_p-Lp_prime))/(Re*sin(Lp_prime-Le))]")
    gui.printlnCond("          = inv tan[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_p) + "*cos(" + 
                    ASTAngle.angleToStr_dec(H_p, DECFORMAT) + ")*tan(" +
                    ASTAngle.angleToStr_dec(H_p, DECFORMAT) + ")*sin(" + 
                    ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " - " + 
                    ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + "))/")
    gui.printlnCond("                    (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R_e) + 
                    "*sin(" + ASTAngle.angleToStr_dec(Lp_p, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(L_e, DECFORMAT) + "))]")
    gui.printlnCond("          = " + ASTAngle.angleToStr_dec(Lat_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    # All that remains now is to convert (Lat_p,Lon_p) to equatorial and horizon coordinates
    
    eqCoord = ASTCoord.EclipticToEquatorial(Lat_p, Lon_p, ASTOrbits.getOEEpochDate())
    gui.printlnCond("34. Convert the object's ecliptic coordinates (Lat_p, Lon_p) to equatorial coordinates.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(eqCoord.getRAAngle().getDecTime(), DECFORMAT) +
                    " hours, Decl = " +    ASTAngle.angleToStr_dec(eqCoord.getDeclAngle().getDecAngle(), DECFORMAT) + " degrees")
    gui.printlnCond()
    
    horizonCoord = ASTCoord.RADecltoHorizon(eqCoord.getRAAngle().getDecTime(), eqCoord.getDeclAngle().getDecAngle(),
                                            observer.getObsLat(), LST)
    gui.printlnCond("35. Convert the object's equatorial coordinates to horizon coordinates.")
    gui.printlnCond("    Alt = " + ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) +
                    ", Az = " + ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT))
    gui.printlnCond()
    
    result = ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) + " Alt, " +\
             ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT) + " Az"
    gui.printlnCond("Thus, for this observer location and date/time, " + objName)
    gui.printlnCond("is at " + result + ".")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(objName + " is located at " + result)



def calcObjRiseSet(gui):
    """
    Calculate the times that an object will rise and set.

    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    dateAdjustObj = ASTInt.ASTInt()
    
    gui.clearTextAreas()
    
    idx = Misc.getValidObj(gui,not Misc.GETSUN,not Misc.GETEARTH,not Misc.GETMOON)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    prt.setBoldFont(True)
    gui.printlnCond("Calculate when " + objName + " will rise and set for the Current Observer", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    solveTrueAnomaly = gui.getTrueAnomalyRBStatus()
    
    eclCoord = ASTOrbits.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                              observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, Misc.termCriteria)
    Lat_p = eclCoord[ASTOrbits.OBJ_ECLLAT]
    Lon_p = eclCoord[ASTOrbits.OBJ_ECLLON]
    gui.printlnCond("1.  Calculate the ecliptic location for " + objName + " at midnight for the date in question.")
    gui.printlnCond("    For UT=0 hours on the date " +
                    ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(), 
                                          observer.getObsDate().getYear()))
    gui.printlnCond("    " + objName + " has eclipitic coordinates Lat_p = " +
                    ASTAngle.angleToStr_dec(Lat_p, DECFORMAT) +    " degrees, Lon_p = " + 
                    ASTAngle.angleToStr_dec(Lon_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eqCoord = ASTCoord.EclipticToEquatorial(Lat_p, Lon_p, ASTOrbits.getOEEpochDate())
    gui.printlnCond("2.  Convert the object's geocentric ecliptic coordinates to equatorial coordinates.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(eqCoord.getRAAngle().getDecTime(), DECFORMAT) +
                    " hours, Decl = " +    ASTAngle.angleToStr_dec(eqCoord.getDeclAngle().getDecAngle(), DECFORMAT) + " degrees")
    gui.printlnCond()

    LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord.getRAAngle().getDecTime(),eqCoord.getDeclAngle().getDecAngle(),
                                          observer.getObsLat())
    riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0)
    LSTr = LSTTimes[ASTOrbits.RISE_TIME]
    LSTs = LSTTimes[ASTOrbits.SET_TIME]        
    gui.printlnCond("3.  Using these equatorial coordinates, compute the LST rising and setting times.")
    if (riseSet):
        gui.printlnCond("    LSTr = " + ASTTime.timeToStr_dec(LSTr,DECFORMAT) + " hours")
        gui.printlnCond("    LSTs = " + ASTTime.timeToStr_dec(LSTs, DECFORMAT) + " hours")
        gui.printlnCond()
    
        GST = ASTTime.LSTtoGST(LSTr, observer.getObsLon())
        UT = ASTTime.GSTtoUT(GST, observer.getObsDate())
        LCTr = ASTTime.UTtoLCT(UT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    
        GST = ASTTime.LSTtoGST(LSTs, observer.getObsLon())
        UT = ASTTime.GSTtoUT(GST, observer.getObsDate())
        LCTs = ASTTime.UTtoLCT(UT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    
        gui.printlnCond("4.  Convert the LST rising/setting times to their corresponding LCT times.")
        gui.printlnCond("    LCTr = " + ASTTime.timeToStr_dec(LCTr, DECFORMAT) + " hours, LCTs = " +
                        ASTTime.timeToStr_dec(LCTs, DECFORMAT) + " hours")
        gui.printlnCond()
    
        gui.printlnCond("5. Convert the LCT times to HMS format.")
        gui.printlnCond("   LCTr = " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + 
                        ", LCTs = " + ASTTime.timeToStr_dec(LCTs, HMSFORMAT))
        gui.printlnCond()
    
        gui.printlnCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), 
                                                            observer.getObsDate().getiDay(),
                                                            observer.getObsDate().getYear()) + " for the current observer")
        result = objName + " will rise at " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) +\
                " LCT and set at " + ASTTime.timeToStr_dec(LCTs, HMSFORMAT) + " LCT"
    else:
        gui.printlnCond()
        result = objName + " does not rise or set for this observer."
    gui.printlnCond(result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcPlanetPeriAphelion(gui):
    """
    Calculate when a planet, including the Earth, will pass
    through perihelion and aphelieon.

    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    dateAdjustObj = ASTInt.ASTInt()
    periDate = ASTDate.ASTDate()
    aphDate = ASTDate. ASTDate()
    
    gui.clearTextAreas()

    # This only works for the planets, so make sure the user specifies a planet
    # The orbital elements file could have non-planets in it (e.g, Pluto, Ceres)
    idx = Planets.getValidPlanet(gui)
    if (idx < 0):
        return
    
    objName = ASTOrbits.getOEObjName(idx)
    
    iTab = Planets.findPlanetIdx(ASTOrbits.getOEObjName(idx))
    
    if (iTab <0):
        ASTMsg.errMsg("The specified object is not in the planets table", "Object not a Planet")
        return
    
    dK0 = Planets.getk0(iTab)
    dK1 = Planets.getk1(iTab)
    dJ0 = Planets.getj0(iTab)
    dJ1 = Planets.getj1(iTab)
    dJ2 = Planets.getj2(iTab)
    
    prt.setBoldFont(True)
    gui.printlnCond("Determine when " + objName + " will pass through Perihelion/Aphelion", CENTERTXT)
    gui.printlnCond("closest to the date " + ASTDate.dateToStr_obj(observer.getObsDate()), CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    iDays = ASTDate.daysIntoYear(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(), 
                                 observer.getObsDate().getYear())
    gui.printlnCond("1.  Compute the number of days into the year for the given date.")
    gui.printlnCond("    Days = " + str(iDays))
    gui.printlnCond()

    dY = observer.getObsDate().getYear() + (iDays / 365.25)
    gui.printlnCond("2.  Compute Y = Year + (Days/365.25) = " + str(observer.getObsDate().getYear()) + 
                    " + (" + str(iDays) + "/365.25) = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY))
    gui.printlnCond()
    
    dK = dK0 * (dY - dK1)
    gui.printlnCond("3.  Using the appropriate 'k' coefficients for " + objName + ",")
    gui.printlnCond("    Compute K = k0*(Y - k1) = " + str(dK0) + "*(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + " - (" + str(dK1) + "))")
    gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dK))
    gui.printlnCond()
    
    iKper = ASTMath.Trunc(abs(dK) + 0.5)
    if (dK < 0):
        iKper = -iKper
    iTmp = int(ASTMath.signum(dK - iKper))
    if (iTmp == 0):
        iTmp = 1
    dKaph = iKper + 0.5 * iTmp
    gui.printlnCond("4.  Let Kper be the integer value closest to K and let Kaph")
    gui.printlnCond("    be the fraction ending in 0.5 that is closest to K.")
    gui.printlnCond("    Kper = " + str(iKper) + ", Kaph = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dKaph))
    gui.printlnCond()
    
    JDper = dJ0 + dJ1 * iKper + dJ2 * iKper * iKper
    JDaph = dJ0 + dJ1 * dKaph + dJ2 * dKaph * dKaph
    gui.printlnCond("5.  Compute JDper = j0 + j1*Kper + j2*Kper^2. This is the Julian day number for perihelion.")
    gui.printlnCond("                  = " + str(dJ0) + " + (" + str(dJ1) + ")*(" + str(iKper) +
                    ") + (" + str(dJ2) + ")*(" + str(iKper) + ")^2")
    gui.printlnCond("                  = " + ASTStr.strFormat(ASTStyle.JDFormat,JDper))
    gui.printlnCond()
    
    gui.printlnCond("6.  Compute JDaph = j0 + j1*Kaph + j2*Kaph^2. This is the Julian day number for aphelion.")
    gui.printlnCond("                  = " + str(dJ0) + " + (" + str(dJ1) + ")*(" + str(dKaph) + 
                    ") + (" + str(dJ2) + ")*(" + str(dKaph) + ")^2")
    gui.printlnCond("                  = " + ASTStr.strFormat(ASTStyle.JDFormat,JDaph))
    gui.printlnCond()
    
    periDate = ASTDate.JDtoDate(JDper)
    aphDate = ASTDate.JDtoDate(JDaph)
    gui.printlnCond("7.  Convert JDper and JDaph to calendar dates.")
    gui.printlnCond("    JDper gives " + str(periDate.getMonth()) + "/" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,periDate.getdDay()) +
                    "/" + str(periDate.getYear()))
    gui.printlnCond("    JDaph gives " + str(aphDate.getMonth()) + "/" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,aphDate.getdDay()) +
                    "/" + str(aphDate.getYear()))
    gui.printlnCond()
    
    UTperi = ASTMath.Frac(periDate.getdDay()) * 24.0
    UTaph = ASTMath.Frac(aphDate.getdDay()) * 24.0
    gui.printlnCond("8.  Convert the fractional part of the days to a decimal UT.")
    gui.printlnCond("    Perihelion: day " + str(periDate.getiDay()) + ", " + 
                    ASTTime.timeToStr_dec(UTperi, DECFORMAT) + " hours")
    gui.printlnCond("    Aphelion: day " + str(aphDate.getiDay()) + ", " + 
                    ASTTime.timeToStr_dec(UTaph, DECFORMAT) + " hours")
    gui.printlnCond()
    
    result = objName + " will pass through "
    gui.printlnCond("9.  Convert the UT results to HMS format.")
    prt.println("    " + result + "Perihelion on " + ASTDate.dateToStr_obj(periDate) + " at " + 
                ASTTime.timeToStr_dec(UTperi, HMSFORMAT) + " UT")
    prt.println("    " + result + "Aphelion on " + ASTDate.dateToStr_obj(aphDate) + " at " + 
                ASTTime.timeToStr_dec(UTaph, HMSFORMAT) + " UT")
    
    gui.setResults(result + "Perihelion/Aphelion as listed in the text area below")
    prt.setProportionalFont()
    prt.resetCursor()



def setTerminationCriteria(gui):
    """
    Set the termination criteria for solving Kepler's equation
    
    :param tkwidget gui: GUI object from which the request came
    """    
    prt = gui.getPrtInstance()
    dTerm = Misc.termCriteria
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    if (ASTQuery.showQueryForm(["Enter Termination Criteria in radians\n(ex: 0.000002)"]) != ASTQuery.QUERY_OK):
        prt.println("Termination criteria was not changed from " + str(Misc.termCriteria) + " radians")
        return

    # Validate the termination criteria
    rTmpObj = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmpObj.isValidRealObj()):
        dTerm = rTmpObj.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
        prt.println("Termination criteria was not changed from " + str(Misc.termCriteria) + " radians")
        return

    prt.println("Prior termination criteria was: " + str(Misc.termCriteria) + " radians")
    if (dTerm < ASTKepler.KeplerMinCriteria):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too small, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    if (dTerm > 1):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too large, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    Misc.termCriteria = dTerm
    
    prt.println("Termination criteria is now set to " + str(Misc.termCriteria) + " radians")
    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
