"""
This module provides a GUI to allow a user to select
a method for find the true anomaly.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.simpledialog as tkSimpleDialog

import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTStyle as ASTStyle

class TrueAnomalyDialog(tkSimpleDialog.Dialog):
    """Class for displaying a dialog to get a method for solving for the true anomaly"""
    #==================================================================
    # Class variables
    #==================================================================
    # top                top level window for this dialog widget
    # radBtns            variable for holding the radio button value
    
    def __init__(self,parent,initValue):
        """
        Create a dialog instance
          
        :param tkwidget parent: parent widget for the dialog
        :param int initValue: which radio button is initially selected
        """        
        self.radBtns = tk.IntVar()
        self.radBtns.set(initValue.value)
        self.initValue = initValue     
        super().__init__(parent=parent,title="Choose Method ...")
        
    def body(self,parent):
        """
        Create the actual radio buttons
        
        :param: tkwidget parent: which widget the radio buttons will belong to
        """
        # Set the default return value in case the user cancels. This must be done in
        # this method because if done in __init__, it gets overridden
        self.result = self.initValue

        # Note: tkinter will not allow enumerated types as values for a radio button
        # so we must translate them to an integer for use as an RB's value
        
        tat = ASTKepler.TrueAnomalyType
   
        tk.Label(parent,text="True Anomaly Methods",font=ASTStyle.TEXT_BOLDFONT).pack(fill=tk.X,expand=False)
        f = tk.LabelFrame(parent,bd=4,relief=tk.RIDGE)
        tk.Radiobutton(f,text="Equation of Center",font=ASTStyle.RADBTN_FONT,anchor="w",
                       value=tat.SOLVE_EQ_OF_CENTER.value,variable=self.radBtns).grid(row=0,sticky=tk.W)        
        tk.Radiobutton(f,text="Simple Iteration",font=ASTStyle.RADBTN_FONT,anchor="w",
                       value=tat.SOLVE_SIMPLE_ITERATION_KEPLER.value,variable=self.radBtns).grid(row=1,sticky=tk.W) 
        tk.Radiobutton(f,text="Newton/Raphson Method",font=ASTStyle.RADBTN_FONT,anchor="w",
                       value=tat.SOLVE_NEWTON_METHOD_KEPLER.value,variable=self.radBtns).grid(row=2,sticky=tk.W)                 
        f.pack(padx=25,pady=5)
        
    def apply(self):
        # If we get here, the user must have clicked on OK.
        # Translate the radio button to a TrueAnomalyType
        tat = ASTKepler.TrueAnomalyType    
        x = self.radBtns.get()
        if (x == tat.SOLVE_EQ_OF_CENTER.value):
            self.result = tat.SOLVE_EQ_OF_CENTER
        elif (x == tat.SOLVE_SIMPLE_ITERATION_KEPLER.value):
            self.result = tat.SOLVE_SIMPLE_ITERATION_KEPLER
        else:
            self.result = tat.SOLVE_NEWTON_METHOD_KEPLER
        
 

#=========== Main entry point ===============
if __name__ == '__main__':
    pass
