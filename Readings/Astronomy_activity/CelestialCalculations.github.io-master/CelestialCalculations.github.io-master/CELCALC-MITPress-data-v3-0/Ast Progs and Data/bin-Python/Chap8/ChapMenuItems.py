"""
Create menu entries for this chapter's Solar Sys Info, Data Files,
and Star Charts menus. This module also creates a
class and structure that associates data items with menu
entries to make it easier to determine what actions to 
take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap8.ChapEnums
import Chap8.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    


#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what menu action to do.
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what menu item is to be done
    
    def __init__(self,eCalcType):
        """
        Creates a ChapMenuItem instance
        
        :param CalculationType eCalcType: what menu item is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getCalcType(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createCalcType(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what type of conversion is to be done
    :return: the menu object that got created
    """ 
    # define a listener for responding to clicks on a menu item 
    menuItemCallback = lambda: ml.menuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=menuItemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: GUI object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """
    cen = Chap8.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create Solar Sys Info menu items
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Solar Sys Info ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu, "Find Object's Position", cen.OBJ_LOCATION)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Object Rise/Set Times", cen.OBJ_RISE_SET)
    __createCalcType(gui,tmpmenu,"Object Distance and Angular Diameter", cen.OBJ_DIST_AND_ANG_DIAMETER)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Planetary Perihelion and Aphelion", cen.PLANET_PERI_APHELION)
    __createCalcType(gui,tmpmenu,"Distance at Perihelion and Aphelion", cen.OBJ_PERI_APH_DIST)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Object Percent Illumination", cen.OBJ_ILLUMINATION)
    __createCalcType(gui,tmpmenu,"Object Visual Magnitude", cen.OBJ_MAGNITUDE)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Miscellaneous Data", cen.OBJ_MISC_DATA)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Set Termination Criteria", cen.TERM_CRITERIA)
    __createCalcType(gui,tmpmenu,"Select True Anomaly Method", cen.KEPLER_OR_EQOFCENTER)
        
    # Create Data Files menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Data Files ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu,"Load Orbital Elements", cen.LOAD_ORBITAL_ELEMENTS)
    __createCalcType(gui,tmpmenu,"Display All Orbital Elements", cen.SHOW_ALL_ORBITAL_ELEMENTS)
    __createCalcType(gui,tmpmenu,"Display Object's Orbital Elements", cen.SHOW_OBJ_ORBITAL_ELEMENTS)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Load a Star Catalog", cen.LOAD_CATALOG)
    __createCalcType(gui,tmpmenu,"Show Catalog Information", cen.SHOW_CATALOG_INFO)
    __createCalcType(gui,tmpmenu,"List all Objects in the Catalog", cen.LIST_ALL_OBJS_IN_CAT)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"List all Constellations", cen.LIST_ALL_CONST)
    __createCalcType(gui,tmpmenu,"List all Catalog Objects in a Constellation", cen.LIST_ALL_CAT_OBJS_IN_CONST)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Find Constellation a RA/Decl is In", cen.FIND_CONST_CAT_OBJ_IN)
    __createCalcType(gui,tmpmenu,"Find Constellation a Solar System Object is In", cen.FIND_CONST_SOLAR_SYS_OBJ_IN)
        
    # Create Star Charts menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Star Charts ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu, "Set mV Filter", cen.SET_mV_FILTER)
    tmpmenu.add_separator()  
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Draw ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"All Stars in the Catalog",cen.DRAW_ALL_STARS)
    __createCalcType(gui,tmpbar,"All Stars in a Constellation",cen.DRAW_ALL_STARS_IN_CONST)
    __createCalcType(gui,tmpbar,"Brightest Star in each Constellation",cen.DRAW_ALL_CONST)   
    tmpbar.add_separator()
    __createCalcType(gui,tmpbar,"All Solar System Objects",cen.DRAW_ALL_SOLAR_SYS_OBJS) 
    __createCalcType(gui,tmpbar,"Sun, Moon, or a Planet",cen.DRAW_SOLAR_SYS_OBJ)
    tmpmenu.add_separator()  
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Locate ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"An Object in the Catalog",cen.FIND_OBJ)
    __createCalcType(gui,tmpbar,"Brightest Object in the Catalog",cen.FIND_BRIGHTEST_OBJ)
    tmpbar.add_separator()
    __createCalcType(gui,tmpbar,"An Equatorial Coordinate",cen.FIND_EQ_LOC)
    __createCalcType(gui,tmpbar,"A Horizon Coordinate",cen.FIND_HORIZ_LOC)
     


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
