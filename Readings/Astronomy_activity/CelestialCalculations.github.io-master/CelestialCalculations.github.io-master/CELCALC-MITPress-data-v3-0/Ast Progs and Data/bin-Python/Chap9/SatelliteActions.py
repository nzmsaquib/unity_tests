"""
Handles the Satellites menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS, DECFORMAT,DMSFORMAT, HMSFORMAT
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime
import ASTUtils.ASTTLE as ASTTLE

import Chap9.ChapEnums as cen
import Chap9.Misc as Misc
import Chap9.VDator as VDator

def calcAxisFromPeriod(gui,calctype):
    """
    Calculate a satellite's semi-major axis from its
    orbital period
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: AXIS_FROM_PERIOD_FROM_TLE or AXIS_FROM_PERIOD_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    
    # Although these are calculated below, initialize
    # them to avoid compiler warnings.
    axis = 0.0                  # @UnusedVariable
    MMotion = 0.0
    period = 0.0
    
    if (calctype == cen.CalculationType.AXIS_FROM_PERIOD_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter orbital period\n" + "(in decimal minutes)"]) != ASTQuery.QUERY_OK):
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(1), HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            period = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid orbital period - try again", "Invalid Orbital Period")
            return
        if (period < 0.0):
            ASTMsg.errMsg("Invalid orbital period - try again", "Invalid Orbital Period")
            return
        tmpStr = "manually entered data"
    else:
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
    
        if (ASTTLE.getKeplerianElementsFromTLE(idx)):
            # The getKeplerianElementsFromTLE validates that the Keplerian
            # elements are valid, so we don't need to check the 'get' results
            rTmp = ASTTLE.getTLEMeanMotion(idx)
            MMotion = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("The Keplerian elements for TLE data set " + str(idx+1) + 
                          "\nappear to be invalid - try another set", "Invalid Keplerian elements")
            return
        axis = Misc.MeanMotion2Axis(gui,MMotion)        # @UnusedVariable
        tmpStr = "TLE data set " + str(idx+1)

    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Use " + tmpStr + " to calculate the Semi-Major Axis from the Orbital Period",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.AXIS_FROM_PERIOD_FROM_INPUT):
        gui.printlnCond("Orbital Period as entered: " + str(period) + " minutes")
    else:
        gui.printlnCond("Mean Motion from TLE data: " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
        gui.printlnCond()
    
        period = 1440.0 / MMotion
        gui.printlnCond("Convert Mean Motion to orbital period")
        gui.printlnCond("    tau = 1440/(Mean Motion) = 1440/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + ")")
        gui.printlnCond("        = " + str(period) + " minutes")
    gui.printlnCond()
    
    axis = 331.253274 * ASTMath.cuberoot(period*period)
    gui.printlnCond("Compute the length of the object's semi-major axis")
    gui.printlnCond("   a = 331.253274*cuberoot(tau^2) = 331.253274*cuberoot(" + str(period) + "^2)")
    gui.printlnCond("     = " + str(axis) + " km")
    gui.printlnCond()
    
    gui.printlnCond("Object's orbital semi-major axis is " + ASTStr.insertCommas(axis, 4) + " km")
    
    gui.setResults("a=" + ASTStr.insertCommas(axis, 4) + " km for tau = " + str(ASTMath.Round(period, 4)) + " minutes")
    
    prt.setProportionalFont()
    prt.resetCursor()



def calcSatDistance(gui,calctype):
    """
    Calculate a satellite's distance from the center of the
    Earth at a given time.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: SAT_DISTANCE_FROM_TLE or SAT_DISTANCE_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    prtObj = None
    dateAdjustObj = ASTInt.ASTInt()
    
    # Although these are calculated below, initialize
    # them to avoid compiler warnings.
    idx = 0
    epochUT = 0.0
    ecc = 0.0
    M0 = 0.0
    MMotion = 0.0
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    # Figure out which method to use for solving Kepler's equation
    solveKepler = gui.getTrueAnomalyRBStatus()
    
    # Set up whether to display interim results when solving Kepler's equation
    if (gui.getShowInterimCalcsStatus()):
        prtObj = prt
    
    if (calctype == cen.CalculationType.SAT_DISTANCE_FROM_INPUT):
        if (ASTQuery.showQueryForm(["eccentricity", "axis", "M0"], "Enter Keplerian elements ...") != ASTQuery.QUERY_OK):
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS) 
        if (rTmp.isValidRealObj()):
            ecc = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            return
        if ((ecc < 0.0) or (ecc > 1.0)):
            ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            axis = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(3), HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            M0 = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid Mean Anomaly - try again", "Invalid Mean Anomaly")
            return
    
        if (ASTQuery.showQueryForm(["Enter Epoch date (mm/dd/yyyy)","Enter UT time for the Epoch"]) != ASTQuery.QUERY_OK):
            return
        epochDate = ASTDate.isValidDate(ASTQuery.getData(1),HIDE_ERRORS)
        if not (epochDate.isValidDateObj()):
            ASTMsg.errMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date")
            return
        timeObj = ASTTime.isValidTime(ASTQuery.getData(2),HIDE_ERRORS)
        if not (timeObj.isValidTimeObj()):
            ASTMsg.errMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time")
            return
        epochUT = timeObj.getDecTime()
        MMotion = Misc.Axis2MeanMotion(gui,axis)
    else:
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
        if (ASTTLE.getKeplerianElementsFromTLE(idx)):
            # The getKeplerianElementsFromTLE validates that the Keplerian
            # elements are valid, so we don't need to check the 'get' results
            epochDate = ASTTLE.getTLEEpochDate(idx)
            rTmp = ASTTLE.getTLEEpochUT(idx)
            epochUT = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEEccentricity(idx)
            ecc = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanAnomaly(idx)
            M0 = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanMotion(idx)
            MMotion = rTmp.getRealValue()  
        else:
            ASTMsg.errMsg("The Keplerian elements for TLE data set " + str(idx+1) + 
                          "\nappear to be invalid - try another set", "Invalid Keplerian elements")
            return
        axis = Misc.MeanMotion2Axis(gui,MMotion)
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.SAT_DISTANCE_FROM_INPUT):
        gui.printlnCond("Use manually entered Keplerian Elements to calculate distance", CENTERTXT)
    else:
        gui.printlnCond("Use TLE data set " + str(idx + 1) + " to calculate distance", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.SAT_DISTANCE_FROM_INPUT):
        gui.printlnCond("1.  Calculate mean motion from the semi-major axis")
        gui.printlnCond("    Mean Motion = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
    else:
        gui.printlnCond("1.  Calculate length of semi-major axis from mean motion")
        gui.printlnCond("    axis = " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond()
    gui.printlnCond("    Resulting data items to use are ...")
    gui.printlnCond("    Epoch Date: " + ASTDate.dateToStr_obj(epochDate) + " at " +
                    ASTTime.timeToStr_dec(epochUT, HMSFORMAT) + " UT")
    gui.printlnCond("    Keplerian Elements:")
    gui.printlnCond("       Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
    gui.printlnCond("       Length of Semi-major axis = " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond("       Mean anomaly at the epoch = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    gui.printlnCond("       Mean Motion = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
    gui.printlnCond()
    
    # Do all the time-related calculations
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST,observer.getObsLon())
    
    gui.printlnCond("2.  Convert the observer LCT to UT, GST, and LST and adjust date if needed.")
    gui.printlnCond("    LCT = " + ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) +
                    " hours, date is " + ASTDate.dateToStr_obj(observer.getObsDate()))
    gui.printCond("     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        gui.printCond("previous day ")
    elif (dateAdjust > 0):
        gui.printCond("next day ")
    gui.printlnCond(ASTDate.dateToStr_obj(adjustedDate) + ")")
    gui.printlnCond("    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond("    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    JDe = ASTDate.dateToJD_mdy(epochDate.getMonth(), epochDate.getdDay() + (epochUT / 24.0), epochDate.getYear())
    JD = ASTDate.dateToJD_mdy(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear())
    deltaT = JD - JDe
    gui.printlnCond("3.  Compute the total number of elapsed days, including fractional days,")
    gui.printlnCond("    since the epoch.")
    gui.printlnCond("    deltaT = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,deltaT) + " days")
    gui.printlnCond()
    
    Mt = M0 + 360 * deltaT * MMotion
    gui.printlnCond("4.  Propagate the mean anomaly by deltaT days")
    gui.printlnCond("    Mt = M0 + 360*deltaT*(Mean Motion) = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " + 360*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,deltaT) + "*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + ")")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Mt) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("5.  if necessary, adjust Mt to be in the range [0,360]")
    gui.printCond("    Mt = Mt MOD 360 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Mt) + " MOD 360 = ")
    Mt = ASTMath.xMOD(Mt, 360.0)
    gui.printlnCond(ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Mt) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("6.  Solve Kepler's equation to get the eccentric anomaly from Mt")
    if (solveKepler == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
        tmpKepler = ASTKepler.calcSimpleKepler(M0, ecc, Misc.termCriteria,prtObj)
        EA = tmpKepler[0]
    else:
        tmpKepler = ASTKepler.calcNewtonKepler(M0, ecc, Misc.termCriteria,prtObj)
        EA = tmpKepler[0]
    gui.printlnCond("    E = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,EA) + " degrees")
    gui.printlnCond()
    
    dv = math.sqrt((1 + ecc) / (1 - ecc)) * ASTMath.TAN_D(EA / 2.0)
    dv = 2.0 * ASTMath.INVTAN_D(dv)
    gui.printlnCond("7.  Compute the object's true anomaly")
    gui.printlnCond("    v = 2 * inv tan[ sqrt[(1 + e)/(1 - e)] * tan(E/2) ]")
    gui.printlnCond("      = 2 * inv tan[ sqrt[(1+" + str(ecc) + ")/(1-" + str(ecc) + ")] * tan(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,EA) + "/2) ]")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
    gui.printlnCond()
    
    dDist = (axis * (1 - (ecc*ecc))) / (1 + ecc * ASTMath.COS_D(dv))
    gui.printlnCond("8.  Compute the distance from the center of the Earth")
    gui.printlnCond("    Dist = a*(1-e^2)/(1 + e*cos(v))")
    gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,axis) + 
                    "*(1-" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc) +
                    "^2)/(1+" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc) + 
                    "*cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + "))")
    gui.printlnCond("         = " + ASTStr.insertCommas(dDist,3))
    gui.printlnCond()

    gui.printlnCond("On " + ASTDate.dateToStr_obj(observer.getObsDate()) + 
                    ", object is/was " + ASTStr.insertCommas(dDist, 3) + " km")
    gui.printlnCond("(" + ASTStr.insertCommas(ASTMath.KM2Miles(dDist), 3) +
                    " miles) from the center of the Earth")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Distance: " + ASTStr.insertCommas(dDist, 3) + " km (" + ASTStr.insertCommas(ASTMath.KM2Miles(dDist), 3) + 
                   " miles) from the center of the Earth")



def calcSatFootprint(gui):
    """
    Calculate the size of a satellite's footprint, what %
    of the Earth its footprint covers, and how long it will
    be in the footprint.
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    if (ASTQuery.showQueryForm(["Distance from center\nof Earth in km","Orbital Velocity (km/s)"]) != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        dist = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid orbital distance - try again", "Invalid Orbital Distance")
        return
    if (dist < ASTOrbits.EARTH_RADIUS):
        ASTMsg.errMsg("Orbital distance must be > Radius of Earth", "Invalid Orbital Distance")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        V = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid orbital distance - try again", "Invalid Orbital Distance")
        return
    if (V < 0.0):
        ASTMsg.errMsg("Invalid orbital velocity - try again", "Invalid Orbital Velocity")
        return

    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Calculate Basic Data about a Satellite's Footprint", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    r_fp = ASTOrbits.EARTH_RADIUS * ASTMath.deg2rad(ASTMath.INVCOS_D(ASTOrbits.EARTH_RADIUS / dist))
    gui.printlnCond("Compute the max radius of the satellite's footprint")
    gui.printlnCond("   r_fp = (pi/180)*r_e*invcos(r_e/r) = (pi/180)*" + str(ASTOrbits.EARTH_RADIUS) +
                    "*invcos(" + str(ASTOrbits.EARTH_RADIUS) + "/" + str(dist) + ")")
    gui.printlnCond("        = " + str(r_fp) + " km")
    gui.printlnCond()

    Fpct = 50 * (dist - ASTOrbits.EARTH_RADIUS) / dist
    gui.printlnCond("Compute the fraction of the Earth's surface that is in the footprint")
    gui.printlnCond("   F = 50*(r - r_e)/r = 50*(" + str(dist) + " - " + str(ASTOrbits.EARTH_RADIUS) + ")/" + str(dist))
    gui.printlnCond("     = " + str(ASTMath.Round(Fpct, 1)) + "%")
    gui.printlnCond()
    
    Fpt = r_fp / (30 * V)
    gui.printlnCond("Calculate how long the satellite will be in the footprint")
    gui.printlnCond("   FPt = r_fp/(30*V) = " + str(r_fp) + "/(30*" + str(V) + ")")
    gui.printlnCond("       = " + str(Fpt) + " minutes")
    gui.printlnCond()
    
    gui.printlnCond("A satellite at a distance of " + ASTStr.insertCommas(dist, 2) + " km that is")
    gui.printlnCond("travelling at " + str(ASTMath.Round(V, 2)) + 
                    " km/s has a footprint radius of " + str(ASTMath.Round(r_fp, 2)) + " km")
    gui.printlnCond("and covers " + str(ASTMath.Round(Fpct, 1)) + 
                    "% of the Earth's surface. It will remain in the")
    gui.printlnCond("footprint for " + str(ASTMath.Round(Fpt, 2)) + " minutes")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Footprint radius=" + str(ASTMath.Round(r_fp, 2)) + " km, " + 
                   str(ASTMath.Round(Fpct, 1)) + "% coverage, in footprint for " +
                   str(ASTMath.Round(Fpt, 2)) + " minutes")



def calcSatLocation(gui,calctype):
    """
    Calculate a satellite's location.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: LOCATE_SAT_FROM_TLE or LOCATE_SAT_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    epochDate = ASTDate.ASTDate()
    solveKepler = gui.getTrueAnomalyRBStatus()
    DST = gui.getDSTStatus()
    
    # Although these are calculated below, initialize
    # them to avoid compiler warnings.
    idx = 0
    epochUT = 0.0
    inclin = 0.0
    ecc = 0.0
    RAAN = 0.0
    argofperi = 0.0
    M0 = 0.0
    MMotion = 0.0
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()) :
        return
     
    if (ASTQuery.showQueryForm(["Enter Observer's Height\nabove sea level (in meters)"]) != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        h_sea = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Observer Height - try again", "Invalid Height")
        return
    
    if (calctype == cen.CalculationType.LOCATE_SAT_FROM_INPUT):
        if not (VDator.validateKeplerianElements()):
            return   
        inclin = VDator.getInclination()
        ecc = VDator.getEccentricity()
        axis = VDator.getAxisLength()
        RAAN = VDator.getRAAN()
        argofperi = VDator.getArgOfPeri()
        M0 = VDator.getMeanAnomaly()
    
        if (ASTQuery.showQueryForm(["Enter Epoch date (mm/dd/yyyy)","Enter UT time for the Epoch"]) != ASTQuery.QUERY_OK):
            return
        epochDate = ASTDate.isValidDate(ASTQuery.getData(1),HIDE_ERRORS)
        if not (epochDate.isValidDateObj()):
            ASTMsg.errMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date")
            return
        timeObj = ASTTime.isValidTime(ASTQuery.getData(2),HIDE_ERRORS)
        if not (timeObj.isValidTimeObj()):
            ASTMsg.errMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time")
            return
        epochUT = timeObj.getDecTime()
    else:
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
    
        rTmp = ASTTLE.getTLEMeanMotion(idx)
        if (rTmp.isValidRealObj()):
            MMotion = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
            return
        axis = Misc.MeanMotion2Axis(gui,MMotion)
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.LOCATE_SAT_FROM_INPUT):
        gui.printlnCond("Find a satellite's location from manually entered data", CENTERTXT)
    else:
        gui.printlnCond("Find a satellite's location from TLE data set " + str(idx+1), CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.LOCATE_SAT_FROM_INPUT):
        gui.printlnCond("1.  Data entered was")
        gui.printlnCond("    Epoch Date: " + ASTDate.dateToStr_obj(epochDate) + " at " + 
                        ASTTime.timeToStr_dec(epochUT, HMSFORMAT) + " UT")
        gui.printlnCond("    Keplerian Elements are:")
        gui.printlnCond("       Inclination = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
        gui.printlnCond("       Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
        gui.printlnCond("       Length of Semi-major axis = " + 
                        ASTStr.insertCommas(axis, 8) + " km")
        gui.printlnCond("       RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
        gui.printlnCond("       Argument of Perigee = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
        gui.printlnCond("       Mean anomaly at the epoch = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
        gui.printlnCond()
        MMotion = Misc.Axis2MeanMotion(gui,axis)
        gui.printlnCond("2.  Convert Mean Motion to semi-major axis")
        gui.printlnCond("    Mean Motion = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
    else:
        gui.printlnCond("1.  Extract epoch date and Keplerian Elements from TLE data set " + str(idx+1))
        if (gui.getShowInterimCalcsStatus()):
            ASTTLE.displayTLEHeader()
            ASTTLE.displayTLEItem(idx)
            gui.printlnCond()
        if (ASTTLE.getKeplerianElementsFromTLE(idx)):
            # The getKeplerianElementsFromTLE validates that the Keplerian
            # elements are valid, so we don't need to check the 'get' results
            epochDate = ASTTLE.getTLEEpochDate(idx)
            rTmp = ASTTLE.getTLEEpochUT(idx)
            epochUT = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEInclination(idx)
            inclin = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEEccentricity(idx)
            ecc = rTmp.getRealValue()
            rTmp = ASTTLE.getTLERAAN(idx)
            RAAN = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEArgofPeri(idx)
            argofperi = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanAnomaly(idx)
            M0 = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanMotion(idx)
            MMotion = rTmp.getRealValue()  
        else:
            ASTMsg.errMsg("The Keplerian elements for TLE data set " + str(idx+1) + 
                          "\nappear to be invalid - try another set", "Invalid Keplerian elements")
            return        
        gui.printlnCond("    Epoch Date: " + ASTDate.dateToStr_obj(epochDate) + " at " + 
                        ASTTime.timeToStr_dec(epochUT, HMSFORMAT) + " UT")
        gui.printlnCond("    Keplerian Elements are:")
        gui.printlnCond("       Inclination = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
        gui.printlnCond("       Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
        gui.printlnCond("       RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
        gui.printlnCond("       Argument of Perigee = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
        gui.printlnCond("       Mean anomaly at the epoch = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
        gui.printlnCond("       Mean Motion = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
        gui.printlnCond()
        axis = Misc.MeanMotion2Axis(gui,MMotion)
        gui.printlnCond("2.  Convert Mean Motion to semi-major axis")
        gui.printlnCond("    axis = " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond()
    
    topoCoord = ASTCoord.Keplerian2Topo(3,observer, h_sea, DST, epochDate, epochUT, inclin, ecc,
                                        axis, RAAN, argofperi, M0, MMotion, solveKepler, Misc.termCriteria, prt)
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("For this observer, the satellite is at " +
                   ASTAngle.angleToStr_dec(topoCoord.getAltAngle().getDecAngle(), DMSFORMAT) + " Alt_topo, " +
                   ASTAngle.angleToStr_dec(topoCoord.getAzAngle().getDecAngle(), DMSFORMAT) + " Az_topo")



def calcSatOrbitalPeriod(gui,calctype):
    """
    Calculate a satellite's orbital period.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: SAT_PERIOD_FROM_TLE or SAT_PERIOD_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    
    # Although these are calculated below, initialize
    # them to avoid compiler warnings.
    idx = 0
    MMotion = 0.0
    
    if (calctype == cen.CalculationType.SAT_PERIOD_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter length of semi-major\naxis (in km)"]) != ASTQuery.QUERY_OK):
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            axis = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
            return
        if (axis < 0.0):
            ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
            return
        tmpStr = "manually entered data"
    else:
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
        if (ASTTLE.getKeplerianElementsFromTLE(idx)):
            # The getKeplerianElementsFromTLE validates that the Keplerian
            # elements are valid, so we don't need to check the 'get' results
            rTmp = ASTTLE.getTLEMeanMotion(idx)
            MMotion = rTmp.getRealValue()  
        else:
            ASTMsg.errMsg("The Keplerian elements for TLE data set " + str(idx+1) + 
                          "\nappear to be invalid - try another set", "Invalid Keplerian elements")
            return
        axis = Misc.MeanMotion2Axis(gui,MMotion)
        tmpStr = "TLE data set " + str(idx+1)
            
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Use " + tmpStr + " to calculate the Orbital Period", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.SAT_PERIOD_FROM_INPUT):
        gui.printlnCond("Length of semi-major axis as entered: " + ASTStr.insertCommas(axis, 8) + " km")
    else:
        gui.printlnCond("Mean Motion from TLE data: " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
        gui.printlnCond("Semi-major axis computed from Mean Motion: " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond()
    
    period = 0.00016587 * math.sqrt(math.pow(axis,3))
    gui.printlnCond("Compute the object's orbital period")
    gui.printlnCond("   tau = 0.00016587*sqrt(a^3) = 0.00016587*sqrt(" + str(axis) + "^3)")
    gui.printlnCond("       = " + str(period) + " minutes")
    gui.printlnCond()
    
    gui.printlnCond("Object's orbital period is " + str(ASTMath.Round(period, 4)) + " minutes (" +
                    str(ASTMath.Round(period / 60.0, 4)) + " hours)")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("tau=" + str(ASTMath.Round(period, 4)) + " minutes (" +
                   str(ASTMath.Round(period / 60.0, 4)) + " hours) for a = " +
                   ASTStr.insertCommas(axis, 4) + " km")



def calcSatPerigeeApogee(gui,calctype):
    """
    Calculate a satellite's perigee and apogee distances.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: SAT_PERI_APOGEE_FROM_TLE or SAT_PERI_APOGEE_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    
    # Although these are calculated below, initialize
    # them to avoid compiler warnings.
    idx = 0
    ecc = 0.0
    MMotion = 0.0

    if (calctype == cen.CalculationType.SAT_PERI_APOGEE_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter orbital eccentricity", "Enter length of semi-major\n" + 
                                    "axis (in km)"]) != ASTQuery.QUERY_OK):
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            ecc = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            return
        if ((ecc < 0.0) or (ecc > 1.0)):
            ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(2), HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            axis = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
            return
        tmpStr = "manually entered data"
    else:
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
    
        if (ASTTLE.getKeplerianElementsFromTLE(idx)):
            # The getKeplerianElementsFromTLE validates that the Keplerian
            # elements are valid, so we don't need to check the 'get' results
            rTmp = ASTTLE.getTLEEccentricity(idx)
            ecc = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanMotion(idx)
            MMotion = rTmp.getRealValue()  
        else:
            ASTMsg.errMsg("The Keplerian elements for TLE data set " + str(idx+1) + 
                          "\nappear to be invalid - try another set", "Invalid Keplerian elements")
            return
        axis = Misc.MeanMotion2Axis(gui,MMotion)
        tmpStr = "TLE data set " + str(idx+1)
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Use " + tmpStr + " to calculate Perigee/Apogee distances", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.SAT_PERI_APOGEE_FROM_INPUT):
        gui.printlnCond("Eccentricity as entered: " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
        gui.printlnCond("Length of semi-major axis as entered: " + ASTStr.insertCommas(axis, 8) + " km")
    else:
        gui.printlnCond("Eccentricity from TLE data: " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
        gui.printlnCond("Mean Motion from TLE data: " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
        gui.printlnCond("Semi-major axis computed from Mean Motion: " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond()
    
    perigeeDist = axis * (1 - ecc)
    gui.printlnCond("Compute the object's distance at perigee.")
    gui.printlnCond("   r_p = a * (1 - e) = " + str(axis) + " * (1 - " + str(ecc) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(perigeeDist, 8) + " km (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(perigeeDist), 8) + 
                    " miles) from the center of the Earth")
    gui.printlnCond()
    
    apogeeDist = axis * (1 + ecc)
    gui.printlnCond("Compute the object's distance at apogee.")
    gui.printlnCond("   r_a = a * (1 + e) = " + str(axis) + " * (1 + " + str(ecc) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(apogeeDist, 8) + " km (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(apogeeDist), 8) + 
                    " miles) from the center of the Earth")
    gui.printlnCond()
    
    gui.printlnCond("At perigee, object is " + 
                    ASTStr.insertCommas(perigeeDist, 2) + " km (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(perigeeDist), 2) + 
                    " miles) from the center of the Earth.")
    gui.printlnCond("At apogee, object is " + 
                    ASTStr.insertCommas(apogeeDist, 2) + " km (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(apogeeDist), 2) + 
                    " miles) from the center of the Earth")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Perigee Dist: " + ASTStr.insertCommas(perigeeDist, 2) + " km (" +
                   ASTStr.insertCommas(ASTMath.KM2Miles(perigeeDist), 2) + " miles), Apogee Dist: " +
                   ASTStr.insertCommas(apogeeDist, 2) + " km (" +
                   ASTStr.insertCommas(ASTMath.KM2Miles(apogeeDist), 2) + " miles)")



def calcSatVelocityCircular(gui):
    """
    Calculate a satellite's orbital velocity when in a
    circular orbit.
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    # For circular orbits, the radius and the semi-major axis are the same
    if (ASTQuery.showQueryForm(["Enter orbital radius in km\n(i.e., distance above center of Earth)"]) != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        axis = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid orbital radius - try again", "Invalid Orbital Radius")
        return
    if (axis < ASTOrbits.EARTH_RADIUS):
        ASTMsg.errMsg("Orbital radius must be > Radius of Earth", "Invalid Orbital Radius")
        return
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Compute Orbital Velocity for a Circular Orbit", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    period = 0.00016587 * math.sqrt(math.pow(axis, 3))
    gui.printlnCond("Compute the object's orbital period")
    gui.printlnCond("   tau = 0.00016587*sqrt(r^3) = 0.00016587*sqrt(" + str(axis) + "^3)")
    gui.printlnCond("       = " + str(period) + " minutes")
    gui.printlnCond()
    
    V = (math.pi * axis) / (30 * period)
    gui.printlnCond("Compute the object's orbital velocity")
    gui.printlnCond("   V = (pi*r)/(30*tau) = (pi*" + str(axis) + ")/(30*" + str(period) + ")")
    gui.printlnCond("     = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V) + " km/s")
    gui.printlnCond()
    
    V2 = 631.348115 * math.sqrt(1 / axis)
    gui.printlnCond("Using vis-viva law instead,")
    gui.printlnCond("   V = 631.348115*sqrt(1/r) = 631.348115*sqrt(1/" + str(axis) + ")")
    gui.printlnCond("     = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V2) + " km/s")
    gui.printlnCond()
    
    gui.printlnCond("An object in a circular orbit that is " + 
                    ASTStr.insertCommas(axis, 4) + " km")
    gui.printlnCond("above the center of the Earth has an orbital velocity")
    gui.printlnCond("of " + str(ASTMath.Round(V, 2)) + " km/s (" + 
                    str(ASTMath.Round(ASTMath.KM2Miles(V), 2)) + " miles/s, or " +
                    ASTStr.insertCommas(3600 * ASTMath.KM2Miles(V), 2) + " mph)")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("r = " + ASTStr.insertCommas(axis, 2) + 
                   " km, tau = " + str(ASTMath.Round(period, 2)) + " minutes, V = " +
                   str(ASTMath.Round(V, 2)) + " km/s (" + str(ASTMath.Round(ASTMath.KM2Miles(V), 2)) + " miles/s)")



def calcSatVelocityElliptical(gui):
    """
    Calculate a satellite's orbital velocity when in an
    elliptical orbit.
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    if (ASTQuery.showQueryForm(["eccentricity", "distance", "axis"],"Enter orbital data ...") != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        ecc = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
        return
    if ((ecc < 0.0) or (ecc >= 1.0)):
        ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        dist = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid orbital distance - try again", "Invalid Orbital Distance")
        return
    if (dist < ASTOrbits.EARTH_RADIUS):
        ASTMsg.errMsg("Orbital distance must be > Radius of Earth", "Invalid Orbital Distance")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        axis = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
        return
    if (axis < ASTOrbits.EARTH_RADIUS):
        ASTMsg.errMsg("Semi-major axis must be > Radius of Earth", "Invalid Semi-Major Axis")
        return
    
    # Be sure that (2/r) > (1/a)
    if ((1 / axis) >= (2 / dist)):
        ASTMsg.errMsg("Semi-major axis or Dist is incorrect", "Invalid axis or distance")
        return
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Compute Orbital Velocity for an Elliptical Orbit", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    V = 631.348115 * math.sqrt((2 / dist) - (1 / axis))
    gui.printlnCond("Use the vis-viva law to compute the object's orbital velocity")
    gui.printlnCond("   V = 631.348115*sqrt[(2/r)-(1/a)] = 631.348115*sqrt[(2/" + str(dist) + ")-(1/" + str(axis) + ")]")
    gui.printlnCond("     = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V) + " km/s")
    gui.printlnCond()
    
    perigeeDist = axis * (1 - ecc)
    gui.printlnCond("Compute the object's distance at perigee.")
    gui.printlnCond("   r_p = a * (1 - e) = " + str(axis) + " * (1 - " + str(ecc) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(perigeeDist, 8) + " km (" + 
                    ASTStr.insertCommas(ASTMath.KM2Miles(perigeeDist), 8) + 
                    " miles) from the center of the Earth")
    gui.printlnCond()
    
    Vperi = 631.348115 * math.sqrt((2 / perigeeDist) - (1 / axis))
    gui.printlnCond("Use the vis-viva law to compute the object's orbital velocity at perigee")
    gui.printlnCond("   Vperigee = 631.348115*sqrt[(2/r_p)-(1/a)] = 631.348115*sqrt[(2/" + 
                    str(perigeeDist) + ")-(1/" + str(axis) + ")]")
    gui.printlnCond("            = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Vperi) + " km/s")
    gui.printlnCond()
    
    apogeeDist = axis * (1 + ecc)
    gui.printlnCond("Compute the object's distance at apogee.")
    gui.printlnCond("   r_a = a * (1 + e) = " + str(axis) + " * (1 + " + str(ecc) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(apogeeDist, 8) + " km (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(apogeeDist), 8) + 
                    " miles) from the center of the Earth")
    gui.printlnCond()
    
    Vap = 631.348115 * math.sqrt((2 / apogeeDist) - (1 / axis))
    gui.printlnCond("Use the vis-viva law to compute the object's orbital velocity at apogee")
    gui.printlnCond("   Vapogee = 631.348115*sqrt[(2/r_a)-(1/a)] = 631.348115*sqrt[(2/" + 
                    str(apogeeDist) + ")-(1/" + str(axis) + ")]")
    gui.printlnCond("           = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Vap) + " km/s")
    gui.printlnCond()
    
    gui.printlnCond("An object in an elliptical orbit that is " + 
                    ASTStr.insertCommas(dist, 4) + " km")
    gui.printlnCond("above the center of the Earth has an orbital velocity")
    gui.printlnCond("of " + str(ASTMath.Round(V, 2)) + " km/s (" + 
                    str(ASTMath.Round(ASTMath.KM2Miles(V), 2)) + " miles/s, or " +
                    ASTStr.insertCommas(3600 * ASTMath.KM2Miles(V), 2) + " mph)")
    gui.printlnCond()
    gui.printlnCond("Distance at perigee is " +
                    ASTStr.insertCommas(perigeeDist, 4) + " km and")
    gui.printlnCond("orbital velocity is " + str(ASTMath.Round(Vperi, 2)) + " km/s (" + 
                    str(ASTMath.Round(ASTMath.KM2Miles(Vperi), 2)) + " miles/s, or " +
                    ASTStr.insertCommas(3600 * ASTMath.KM2Miles(Vperi), 2) + " mph)")
    gui.printlnCond()
    gui.printlnCond("Distance at apogee is " + 
                    ASTStr.insertCommas(apogeeDist, 4) + " km and")
    gui.printlnCond("orbital velocity is " + str(ASTMath.Round(Vap, 2)) + " km/s (" + 
                    str(ASTMath.Round(ASTMath.KM2Miles(Vap), 2)) + " miles/s, or " +
                    ASTStr.insertCommas(3600 * ASTMath.KM2Miles(Vap), 2) + " mph)")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("V=" + str(ASTMath.Round(V, 2)) + " km/s, Vperigee=" + str(ASTMath.Round(Vperi, 2)) + 
                   " km/s, Vapogee=" + str(ASTMath.Round(Vap, 2)) + " km/s")



def calcSatRiseSet(gui,calctype):
    """
    Calculate a satellite's rise/set times. This method
    only provides an estimate. It does so by periodically
    calculating the horizon coordinates over some
    interval of time. The results are rough estimates
    at best, due in part to using such a simple
    orbit propagator
    
    The starting time is assumed to be the observer LCT. As
    the LCT is incremented, it is rounded to 2 decimal seconds
    so that what is printed as the LCT will actually match
    what is used for the calculations. Failure to do this
    means that the coordinates calculated by a decimal
    LCT value (e.g., 7.5043138888) may differ greatly 
    from that calculated using a less precise HMS
    value (e.g., 7:30:15.53 vs 7:30:15.52999968).
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: LOCATE_SAT_FROM_TLE or LOCATE_SAT_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    epochDate = ASTDate.ASTDate()
    timeObj = ASTTime.ASTTime()
    DST = gui.getDSTStatus()
    solveKepler = gui.getTrueAnomalyRBStatus()
    
    # Although these are calculated below, initialize
    # them to avoid compiler warnings.
    idx = 0
    epochUT = 0.0
    ecc = 0.0
    RAAN = 0.0
    argofperi = 0.0
    M0 = 0.0
    MMotion = 0.0
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
     
    if (ASTQuery.showQueryForm(["Enter Observer's Height\nabove sea level (in meters)"]) != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        h_sea = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Observer Height - try again", "Invalid Height")
        return    

    if (calctype == cen.CalculationType.RISE_SET_FROM_INPUT):
        if not (VDator.validateKeplerianElements()):
            return
        inclin = VDator.getInclination()
        ecc = VDator.getEccentricity()
        axis = VDator.getAxisLength()
        RAAN = VDator.getRAAN()
        argofperi = VDator.getArgOfPeri()
        M0 = VDator.getMeanAnomaly()
    
        if (ASTQuery.showQueryForm(["Enter Epoch date (mm/dd/yyyy)","Enter UT time for the Epoch"]) != ASTQuery.QUERY_OK):
            return
        epochDate = ASTDate.isValidDate(ASTQuery.getData(1), HIDE_ERRORS)
        if not (epochDate.isValidDateObj()):
            ASTMsg.errMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date")
            return
        timeObj = ASTTime.isValidTime(ASTQuery.getData(2), HIDE_ERRORS)
        if not (timeObj.isValidTimeObj()):
            ASTMsg.errMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time")
            return
        epochUT = timeObj.getDecTime()
        MMotion = Misc.Axis2MeanMotion(gui,axis)
    else:
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
        if (ASTTLE.getKeplerianElementsFromTLE(idx)):
            # The getKeplerianElementsFromTLE validates that the Keplerian
            # elements are valid, so we don't need to check the 'get' results
            epochDate = ASTTLE.getTLEEpochDate(idx)
            rTmp = ASTTLE.getTLEEpochUT(idx)
            epochUT = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEInclination(idx)
            inclin = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEEccentricity(idx)
            ecc = rTmp.getRealValue()
            rTmp = ASTTLE.getTLERAAN(idx)
            RAAN = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEArgofPeri(idx)
            argofperi = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanAnomaly(idx)
            M0 = rTmp.getRealValue()
            rTmp = ASTTLE.getTLEMeanMotion(idx)
            MMotion = rTmp.getRealValue()  
        else:
            ASTMsg.errMsg("The Keplerian elements for TLE data set " + str(idx+1) + 
                          "\nappear to be invalid - try another set", "Invalid Keplerian elements")
            return
        axis = Misc.MeanMotion2Axis(gui,MMotion)
    
    if (ASTQuery.showQueryForm(["Maximum Hours from observer LCT\n(enter decimal hours, NOT HMS!)",
                                "Increment (decimal hours or HMS)"]) != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        maxHours = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Max Hours - try again", "Invalid Max Hours")
        return
    if (maxHours < 0):
        ASTMsg.errMsg("Invalid Max Hours - try again", "Invalid Max Hours")
        return
    timeObj = ASTTime.isValidTime(ASTQuery.getData(2), HIDE_ERRORS)
    if not (timeObj.isValidTimeObj()):
        ASTMsg.errMsg("Invalid Time Increment- try again", "Invalid Time Increment")
        return
    deltaT = timeObj.getDecTime()
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.RISE_SET_FROM_INPUT):
        gui.printlnCond("Use manually entered Keplerian Elements to estimate rise/set times", CENTERTXT)
    else:
        gui.printlnCond("Use TLE data set " + str(idx+1) + " to estimate rise/set times", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("    Epoch Date: " + ASTDate.dateToStr_obj(epochDate) + " at " + 
                    ASTTime.timeToStr_dec(epochUT, HMSFORMAT) + " UT")
    gui.printlnCond("    Keplerian Elements are:")
    gui.printlnCond("       Inclination = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    gui.printlnCond("       Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
    gui.printlnCond("       Length of Semi-major axis = " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond("       RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
    gui.printlnCond("       Argument of Perigee = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
    gui.printlnCond("       Mean anomaly at the epoch = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    gui.printlnCond("       Mean Motion = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revs/day")
    gui.printlnCond()
    
    LCT = observer.getObsTime().getDecTime()
    prt.println("Start at " + ASTTime.timeToStr_dec(LCT, HMSFORMAT) + " on " + 
                ASTDate.dateToStr_obj(observer.getObsDate()))
    prt.println("for a maximum of " + str(maxHours) + " hours, incremented by " + 
                ASTTime.timeToStr_dec(deltaT, HMSFORMAT) + " (" +
                ASTTime.timeToStr_dec(deltaT, DECFORMAT) + " hours)")
    prt.println()
    
    i = 1
    incrementT = 0.0
    tmpDate = observer.getObsDate()
    
    strTmp = ASTStr.strFormat("%5s "," ") + ASTStr.strFormat("%10s ","Date   ") +\
             ASTStr.strFormat("%11s ","LCT Time ") + ASTStr.strFormat("%16s ","Topo Alt    ") +\
             ASTStr.strFormat("%15s","Topo Az    ")
    prt.println(strTmp)
    prt.println("="*65)
    
    # Show location at the start time and save the day to determine when a new day starts,
    # save the sign of the altitude to determine rise/set when the sign changes, and
    # save the JD to interpolate a rise/set time. JD is needed rather than saving the LCT
    # because we may cross a date boundary
    strTmp = ASTStr.strFormat("%-5d ",i) + ASTStr.strFormat("%10s ",ASTDate.dateToStr_obj(tmpDate)) +\
             ASTStr.strFormat("%11s ",ASTTime.timeToStr_dec(LCT, HMSFORMAT))
    prt.printnoln(strTmp)
    
    topoCoord = ASTCoord.Keplerian2Topo(0,observer,h_sea, DST, epochDate, epochUT, inclin, ecc, 
                                        axis, RAAN, argofperi, M0, MMotion, solveKepler, Misc.termCriteria, None)
    strTmp = ASTStr.strFormat("%16s ",ASTAngle.angleToStr_dec(topoCoord.getAltAngle().getDecAngle(), DMSFORMAT)) +\
             ASTStr.strFormat("%15s",ASTAngle.angleToStr_dec(topoCoord.getAzAngle().getDecAngle(), DMSFORMAT))
    prt.println(strTmp)
    
    saveDay = tmpDate.getiDay()
    # Use LCT even though it really should be UT. This allows us to interpolate the LCT times
    # w/o the need to interpolate UTs and then convert to the LCT
    saveJD = ASTDate.dateToJD_mdy(tmpDate.getMonth(), tmpDate.getiDay() + (LCT / 24.0), tmpDate.getYear())
    saveSign = ASTMath.signum(topoCoord.getAltAngle().getDecAngle())
    
    # Loop until maxHours is reached
    while (incrementT < maxHours):
        i = i + 1
    
        # add deltaT to the time to get a new LCT - use JDs to do so and round
        # to nearest 100ths of a second so that times printed for the user
        # match the actual times used for determining the horizon coordinates
        JD = saveJD + deltaT / 24.0
        tmpDate = ASTDate.JDtoDate(JD)
        LCT = ASTMath.Frac(tmpDate.getdDay()) * 24.0
        LCT = ASTMath.Round(LCT, 2)
        # Update the observer object with the new time and potentially new date. This
        # will not affect the values displayed in the GUI. We could create a new observer
        # object and use it, but other methods in this program use the global
        # observer object. So, we will just update the global observer object.
        observer.setObsDateTime(tmpDate.getMonth(), tmpDate.getiDay(), tmpDate.getYear(), LCT)
    
        if (saveDay == tmpDate.getiDay()):
            tmpStr = " "
        else:
            tmpStr = ASTDate.dateToStr_obj(tmpDate)
            saveDay = tmpDate.getiDay()
        
        strTmp = ASTStr.strFormat("%-5d ",i) + ASTStr.strFormat("%10s ",tmpStr) +\
                 ASTStr.strFormat("%11s ",ASTTime.timeToStr_dec(LCT, HMSFORMAT))
        prt.printnoln(strTmp)
    
        # propagate to the new time
        topoCoord = ASTCoord.Keplerian2Topo(0,observer,h_sea, DST, epochDate, epochUT, inclin, ecc, 
                axis, RAAN, argofperi, M0, MMotion, solveKepler, Misc.termCriteria, None)
        
        strTmp = ASTStr.strFormat("%16s ",ASTAngle.angleToStr_dec(topoCoord.getAltAngle().getDecAngle(), DMSFORMAT)) +\
                 ASTStr.strFormat("%15s",ASTAngle.angleToStr_dec(topoCoord.getAzAngle().getDecAngle(), DMSFORMAT))
        prt.printnoln(strTmp)
    
        # See if the object has transition above or below the horizon
        if (ASTMath.signum(topoCoord.getAltAngle().getDecAngle()) != saveSign):
            dT = saveJD + (JD - saveJD) / 2.0  # This is the JD halfway between the previous and current time
            dT = ASTMath.Frac(ASTDate.JDtoDate(dT).getdDay()) * 24.0  # Convert JD to a date and get the LCT time from it
            prt.printnoln(" " + ASTTime.timeToStr_dec(dT, HMSFORMAT) + " est. ")
            if (saveSign < 0):
                prt.printnoln("rise")
            else:
                prt.printnoln("set")
        prt.println()
    
        saveSign = ASTMath.signum(topoCoord.getAltAngle().getDecAngle())
        saveJD = JD
        incrementT = incrementT + deltaT
    
    prt.setProportionalFont()
    prt.resetCursor()



def setTerminationCriteria(gui):
    """
    Set the termination criteria for solving Kepler's equation
    
    :param tkwidget gui: main application window
    """    
    prt = gui.getPrtInstance()
    dTerm = Misc.termCriteria
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    if (ASTQuery.showQueryForm(["Enter Termination Criteria in radians\n(ex: 0.000002)"]) != ASTQuery.QUERY_OK):
        prt.println("Termination criteria was not changed from " + str(Misc.termCriteria) + " radians")
        return
    
    # Validate the termination criteria
    rTmpObj = ASTReal.isValidReal(ASTQuery.getData(1), HIDE_ERRORS)
    if (rTmpObj.isValidRealObj()):
        dTerm = rTmpObj.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
        prt.println("Termination criteria was not changed from " + str(Misc.termCriteria) + " radians")
        return
    
    prt.println("Prior termination criteria was: " + str(Misc.termCriteria) + " radians")
    if (dTerm < ASTKepler.KeplerMinCriteria):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too small, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    if (dTerm > 1):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too large, so it has been reset to " + str(dTerm) + " radians")
        prt.println()

    Misc.termCriteria = dTerm
    
    prt.println("Termination criteria is now set to " + str(Misc.termCriteria) + " radians")
    prt.resetCursor()  



#=========== Main entry point ===============
if __name__ == '__main__':
    pass 
