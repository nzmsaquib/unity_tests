"""
Handles the Coord Sys menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS,DECFORMAT,DMSFORMAT
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime
import ASTUtils.ASTVect as ASTVect

import Chap9.Misc as Misc
import Chap9.VDator as VDator

def calcCartesian2Spherical(gui):
    """
    Convert Cartesian coordinates to spherical
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Convert Cartesian Coordinates to Spherical", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (ASTQuery.showQueryForm(["x", "y", "z"], "Enter Cartesian coordinates ...") != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        x = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid x value - try again", "Invalid x")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        y = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid y value - try again", "Invalid y")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        z = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid z value - try again", "Invalid z")
        return
    
    gui.printlnCond("Convert x= " + str(x) + ", y = " + str(y) + ", z = " + str(z) + " to spherical coordinates")
    gui.printlnCond()
    
    r = math.sqrt(x*x + y*y + z*z)
    gui.printlnCond("1.  r = sqrt[ x^2 + y^2 + z^2 ]")
    gui.printlnCond("      = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,x) + ")^2 + (" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,y) + ")^2 + (" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,z) + ")^2]")
    gui.printlnCond("      = " + str(r))
    gui.printlnCond()
    
    alpha = ASTMath.INVTAN_D(y / x)
    gui.printlnCond("2.  alpha = inv tan(y/x) = inv tan(" + str(y) + "/" + str(x) + ")")
    gui.printlnCond("          = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) + " degrees")
    gui.printlnCond()
    
    dT = ASTMath.quadAdjust(y, x)
    gui.printlnCond("3.  Use the algebraic signs of y and x to determine a quadrant adjustment")
    gui.printlnCond("    and apply it to alpha.")
    gui.printCond("    alpha = alpha + Adjustment = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) + " + " +
                  ASTAngle.angleToStr_dec(dT, DECFORMAT) + " = ")
    alpha = alpha + dT
    gui.printlnCond(ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) + " degrees")
    gui.printlnCond()

    phi = ASTMath.INVCOS_D(z / r)
    gui.printlnCond("4.  phi = inv cos(z/r) = inv cos(" + str(z) + "/" + str(r) + ")")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,phi) + " degrees")
    gui.printlnCond()
    
    result = "[x=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) + ", y=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) +\
             ", z=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,z) + "] = [r=" +\
             ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,r) + ", alpha=" +\
             ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,alpha) + ", phi=" +\
             ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,phi) + "]"
    gui.printlnCond("Thus, [x=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,x) + 
                    ", y=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,y) + 
                    ", z=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,z) + "]")
    gui.printlnCond("    = [r=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r) + 
                    ", alpha=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) +
                    ", phi=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,phi) + "]")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(result)



def calcECI2Horizon(gui):
    """
    Convert ECI (Cartesian) coordinates to horizon
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    dateAdjustObj = ASTInt.ASTInt()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Convert ECI (Cartesian) Coordinates to Horizon", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (ASTQuery.showQueryForm(["x", "y", "z"], "Enter Cartesian coordinates ...") != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        x = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid x value - try again", "Invalid x")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        y = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid y value - try again", "Invalid y")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        z = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid z value - try again", "Invalid z")
        return
    
    # Do all the time-related calculations
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    # adjust the date, if needed, since the LCTtoUT conversion could have changed the date
    dateAdjust = dateAdjustObj.getIntValue()
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST,observer.getObsLon())

    gui.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
    gui.printlnCond("    LCT = " + ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) +
                    " hours, date is " + ASTDate.dateToStr_obj(observer.getObsDate()) + ")")
    gui.printCond("     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        gui.printCond("previous day ")
    elif (dateAdjust > 0):
        gui.printCond("next day ")
    gui.printlnCond(ASTDate.dateToStr_obj(adjustedDate) + ")")
    gui.printlnCond("    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond("    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    spherCoord = ASTCoord.CartesianToSpherical(x, y, z)
    gui.printlnCond("2.  Convert ECI (cartesian) coordinates to spherical coordinates")
    gui.printlnCond("    r = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,spherCoord.getSpherR().getDecAngle()))
    gui.printlnCond("    alpha = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,spherCoord.getSpherAlpha().getDecAngle()) + " degrees")
    gui.printlnCond("    phi = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,spherCoord.getSpherPhi().getDecAngle()) + " degrees")
    gui.printlnCond()
    gui.printlnCond("    Note: r is the distance above the center of the Earth.")
    gui.printlnCond()
    
    Decl = 90.0 - spherCoord.getSpherPhi().getDecAngle()
    gui.printlnCond("3.  Convert phi to declination.")
    gui.printlnCond("    Decl = 90 - phi = 90 - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,spherCoord.getSpherPhi().getDecAngle()) +
                    " = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Decl) + " degrees")
    gui.printlnCond()
    
    RA = spherCoord.getSpherAlpha().getDecAngle() / 15.0
    gui.printlnCond("4.  Convert alpha to hours.")
    gui.printlnCond("    RA = alpha/15.0 = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,spherCoord.getSpherAlpha().getDecAngle()) +
                    "/15.0 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RA) + " hours")
    gui.printlnCond()
    
    horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
    gui.printlnCond("5.  Convert the equatorial coordinates (RA, Decl) to horizon coordinates")
    gui.printlnCond("    Alt = " + ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) +
                    ", Az = " + ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT))
    gui.printlnCond()
    
    result = "[x=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) + \
             ", y=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) + \
             ", z=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,z) + "] =  " + \
             ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) +\
             " Alt, " + ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT) + \
             " Az, dist=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,spherCoord.getSpherR().getDecAngle())
    gui.printlnCond("Thus, for this observer location and date/time,")
    gui.printlnCond("  " + result)
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(result)



def calcECI2Topo(gui):
    """
    Convert ECI (Cartesian) to Topocentric coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    prt = gui.getPrtInstance()
    R = ASTVect.ASTVect(0.0,0.0,0.0)            # Satellite's position in ECI coordinates
    R_obs = ASTVect.ASTVect(0.0,0.0,0.0)        # Observer's position in ECI coordinates    
    Rp = ASTVect.ASTVect(0.0,0.0,0.0)           # Range vector
    Rpp = ASTVect.ASTVect(0.0,0.0,0.0)          # Rotated range vector    
    dateAdjustObj = ASTInt.ASTInt()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    obsLat = observer.getObsLat()
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Convert ECI (Cartesian) Coordinates to Topocentric", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (ASTQuery.showQueryForm(["Enter Observer's Height\nabove sea level (in meters)"]) != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        h_sea = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Observer Height - try again", "Invalid Height")
        return
    
    if (ASTQuery.showQueryForm(["x", "y", "z"], "Enter Cartesian coordinates ...") != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        x = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid x value - try again", "Invalid x")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        y = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid y value - try again", "Invalid y")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        z = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid z value - try again", "Invalid z")
        return
    R.setVect(x,y,z)
    
    # Do all the time-related calculations
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    # adjust the date, if needed, since the LCTtoUT conversion could have changed the date
    dateAdjust = dateAdjustObj.getIntValue()
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST,observer.getObsLon())
    
    gui.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
    gui.printlnCond("    LCT = " + ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) +
                    " hours, date is " + ASTDate.dateToStr_obj(observer.getObsDate()) + ")")
    gui.printCond("     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        gui.printCond("previous day ")
    elif (dateAdjust > 0):
        gui.printCond("next day ")
    gui.printlnCond(ASTDate.dateToStr_obj(adjustedDate) + ")")
    gui.printlnCond("    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond("    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    LSTd = LST * 15.0
    gui.printlnCond("2.  Convert the observer's LST to an angle")
    gui.printlnCond("    LSTd = LST*15 = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + "*15 = " +
                    ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    rp_e = ASTOrbits.EARTH_RADIUS + (h_sea / 1000.0)
    r_eq = rp_e * ASTMath.COS_D(obsLat)
    R_obs.setVect(r_eq * ASTMath.COS_D(LSTd), r_eq * ASTMath.SIN_D(LSTd), rp_e * ASTMath.SIN_D(obsLat))
    gui.printlnCond("3.  Convert the observer's location to ECI coordinates")
    gui.printlnCond("    rp_e = r_e + (h_obs/1000) = " + str(ASTOrbits.EARTH_RADIUS) + " + (" + str(h_sea) + "/1000)")
    gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,rp_e) + " km")
    gui.printlnCond("    r_eq = rp_e * cos(lat) = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,rp_e) + " * cos(" +
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")")
    gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r_eq) + " km")
    gui.printlnCond("    x_obs = r_eq * cos(LSTd) = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r_eq) + " * cos(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,LSTd) + ")")
    gui.printlnCond("          = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R_obs.x()))
    gui.printlnCond("    y_obs = r_eq * sin(LSTd) = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r_eq) + " * sin(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,LSTd) + ")")
    gui.printlnCond("          = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R_obs.y()))
    gui.printlnCond("    z_obs = r_eq * sin(lat) = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r_eq) + " * sin(" +
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")")
    gui.printlnCond("          = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R_obs.z()))
    gui.printlnCond()
    
    Rp.setVect(R.x() - R_obs.x(), R.y() - R_obs.y(), R.z() - R_obs.z())
    gui.printlnCond("4.  Compute the range vector")
    gui.printlnCond("    x' = x - x_obs = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) + " - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R_obs.x()))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.x()))
    gui.printlnCond("    y' = y - y_obs = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) + " - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R_obs.y()))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.y()))
    gui.printlnCond("    z' = z - z_obs = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + " - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R_obs.z()))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.z()))
    gui.printlnCond()
    
    x = Rp.x()*ASTMath.SIN_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.SIN_D(obsLat)*ASTMath.SIN_D(LSTd) - Rp.z()*ASTMath.COS_D(obsLat)
    y = -Rp.x()*ASTMath.SIN_D(LSTd) + Rp.y()*ASTMath.COS_D(LSTd)
    z = Rp.x()*ASTMath.COS_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.COS_D(obsLat)*ASTMath.SIN_D(LSTd) + Rp.z()*ASTMath.SIN_D(obsLat)
    Rpp.setVect(x, y, z)
    gui.printlnCond("5.  Rotate the range vector by LSTd and latitude")
    gui.printlnCond("    x'' = x'*sin(lat)*cos(LSTd) + y'*sin(lat)*sin(LSTd) - z'*cos(lat)")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.x()) + "*sin(" + 
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")*cos(" +
                    ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + ") + ")
    gui.printlnCond("          " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.y()) + "*sin(" +
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")*sin(" +
                    ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + ") - ")
    gui.printlnCond("          " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.z()) + "*cos(" + 
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rpp.x()))
    gui.printlnCond("    y'' = -[x'*sin(LSTd)] + y'*cos(LSTd)")
    gui.printlnCond("        = -[" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.x()) + "*sin(" + 
                    ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + ")] + " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.y()) + "*cos(" + ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + ")")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rpp.y()))
    gui.printlnCond("    z'' = x'*cos(lat)*cos(LSTd) + y'*cos(lat)*sin(LSTd) + z'*sin(lat)")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.x()) + "*cos(" + 
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")*cos(" +
                    ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + ") + ")
    gui.printlnCond("          " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.y()) + "*cos(" + 
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")*sin(" +
                    ASTAngle.angleToStr_dec(LSTd, DECFORMAT) + ") + ")
    gui.printlnCond("          " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.z()) + "*sin(" + 
                    ASTAngle.angleToStr_dec(obsLat, DECFORMAT) + ")")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rpp.z()))
    gui.printlnCond()
    
    r_dist = Rpp.len()
    gui.printlnCond("6.  Compute the rotated range vector's magnitude")
    gui.printlnCond("    r_dist = " + ASTStr.insertCommas(r_dist, 8) + " km")
    gui.printlnCond()
    
    Az = ASTMath.INVTAN2_D(-Rpp.y(), Rpp.x())
    gui.printlnCond("7.  Compute the topocentric azimuth and adjust if necessary to put in the correct quadrant")
    gui.printlnCond("    Az_topo = inv tan(-y'' / x'') = inv tan(-" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.y()) +
                    "/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rp.x()) + ")")
    gui.printlnCond("            = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTMath.INVTAN_D(-Rp.y()/Rp.x())) + " degrees")
    gui.printlnCond("    After adjusting to the correct quadrant,")
    gui.printlnCond("    Az_topo = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Az) + " degrees")
    gui.printlnCond()
    
    Alt = ASTMath.INVSIN_D(Rpp.z() / r_dist)
    gui.printlnCond("8.  Compute the topocentric altitude")
    gui.printlnCond("    Alt_topo = inv sin(z'' / r_dist) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Rpp.z()) +
                    "/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r_dist) + ")")
    gui.printlnCond("             = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Alt) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("9.  Finally, convert Altitude and Azimuth to DMS format. Thus,")
    gui.printlnCond("    Alt_topo = " + ASTAngle.angleToStr_dec(Alt,DMSFORMAT) + ", Az_topo = " + 
                    ASTAngle.angleToStr_dec(Az, DMSFORMAT))
    gui.printlnCond()
    
    result = "[x=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) + ", y=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) +\
            ", z=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + "] =  " + ASTAngle.angleToStr_dec(Alt, DMSFORMAT) +\
            " Alt_topo, " + ASTAngle.angleToStr_dec(Az, DMSFORMAT) + " Az_topo"
    gui.printlnCond("Thus, for this observer location and date/time,")
    gui.printlnCond("  " + result)
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(result)



def calcKeplerian2State(gui):
    """
    Convert Keplerian elements to a state vector
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    prtObj = None
    R = ASTVect.ASTVect(0.0,0.0,0.0)
    V = ASTVect.ASTVect(0.0,0.0,0.0)
    
    gui.clearTextAreas()
    
    if not (VDator.validateKeplerianElements()):
        return
    inclin = VDator.getInclination()
    ecc = VDator.getEccentricity()
    axis = VDator.getAxisLength()
    RAAN = VDator.getRAAN()
    argofperi = VDator.getArgOfPeri()
    M0 = VDator.getMeanAnomaly()
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert Keplerian Elements to the corresponding State Vector", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    # Set up whether to display interim results when solving Kepler's equation
    if (gui.getShowInterimCalcsStatus()):
        prtObj = prt
    
    gui.printlnCond("Convert these Keplerian Elements to a State Vector:")
    gui.printlnCond("  Inclination = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    gui.printlnCond("  Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
    gui.printlnCond("  Length of Semi-major axis = " + ASTStr.insertCommas(axis, 8) + " km")
    gui.printlnCond("  RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
    gui.printlnCond("  Argument of Perigee = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
    gui.printlnCond("  Mean anomaly at the epoch = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    gui.printlnCond()
    
    rho = axis * (1.0 - ecc*ecc)
    gui.printlnCond("1.  Compute the semi-latus rectum from the semi-major axis and eccentricity")
    gui.printlnCond("    p = a*(1 - e^2) = " + ASTStr.insertCommas(axis,8) + 
                    "*(1 - " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc) + "^2)")
    gui.printlnCond("      = " + ASTStr.insertCommas(rho,8))
    gui.printlnCond()
    
    gui.printlnCond("2.  Solve Kepler's equation to get the eccentric anomaly from M0")
    if (gui.getSimpleIterationRBStatus()):
        tmpKepler = ASTKepler.calcSimpleKepler(M0, ecc, Misc.termCriteria,prtObj)
        EA = tmpKepler[0]
    else:
        tmpKepler = ASTKepler.calcNewtonKepler(M0, ecc, Misc.termCriteria,prtObj)
        EA = tmpKepler[0]

    gui.printlnCond("    E = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,EA) + " degrees")
    gui.printlnCond()
    
    dv = math.sqrt((1 + ecc) / (1 - ecc)) * ASTMath.TAN_D(EA / 2.0)
    dv = 2.0 * ASTMath.INVTAN_D(dv)
    gui.printlnCond("3.  Compute the object's true anomaly")
    gui.printlnCond("    v = 2 * inv tan[ sqrt[(1 + e)/(1 - e)] * tan(E/2) ]")
    gui.printlnCond("      = 2 * inv tan[ sqrt[(1+" + str(ecc) + ")/(1-" + str(ecc) + ")] * tan(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,EA) + "/2) ]")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
    gui.printlnCond()
    
    dLen = rho / (1 + ecc * ASTMath.COS_D(dv))
    gui.printlnCond("4.  Compute the length of the positional vector")
    gui.printlnCond("    Rlen = p/(1 + e*cos(v)) = " + ASTStr.insertCommas(rho,8) + 
                    "/(1 + " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc) +
                    "*cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + "))")
    gui.printlnCond("         = " + ASTStr.insertCommas(dLen,8))
    gui.printlnCond()
    
    x = dLen * ASTMath.COS_D(dv)
    y = dLen * ASTMath.SIN_D(dv)
    z = 0.0
    R.setVect(x, y, z)
    gui.printlnCond("5.  Compute the positional vector R' in the perifocal coordinate system")
    gui.printlnCond("    x' = Rlen*cos(v) = " + ASTStr.insertCommas(dLen,8) + "*cos(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(x,8))
    gui.printlnCond("    y' = Rlen*sin(v) = " + ASTStr.insertCommas(dLen,8) + "*sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(y,8))
    gui.printlnCond("    z' = 0.0")
    gui.printlnCond()
    
    A = math.sqrt(ASTOrbits.muEARTH / rho)
    gui.printlnCond("6.  Compute a temporary variable A")
    gui.printlnCond("    A = sqrt(muEARTH/p) = sqrt(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ASTOrbits.muEARTH) +
                    "/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,rho) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A))
    gui.printlnCond()
    
    x = -A * ASTMath.SIN_D(dv)
    y = A * (ecc + ASTMath.COS_D(dv))
    z = 0.0
    V.setVect(x, y, z)
    gui.printlnCond("7.  Compute the velocity vector V' in the perifocal coordinate system")
    gui.printlnCond("    Vx' = -A*sin(v) = -(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A) + ")*sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + ")")
    gui.printlnCond("        = " + ASTStr.insertCommas(x,8))
    # E in the next statement is e, the base of the natural log
    gui.printlnCond("    Vy' = A*(e + cos(v)) = (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A) + 
                    ")*(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,math.e) +
                    " + cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + "))")
    gui.printlnCond("        = " + ASTStr.insertCommas(y,8))
    gui.printlnCond("    Vz' = 0.0")
    gui.printlnCond()

    gui.printlnCond("8.  Compute the length of the velocity vector to get the velocity")
    gui.printlnCond("    Vlen' = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.len()))
    gui.printlnCond()
    
    Otype = ASTOrbits.getSatOrbitType(ecc, inclin)
    if (Otype < 0):
        ASTMsg.errMsg("Orbit must be elliptical. The computed eccentricity\n" + 
                      "and inclination are not supported.","Invalid Orbit type")
        return
    gui.printlnCond("9.  Determine the orbit type based on orbital eccentricity and inclination")
    gui.printlnCond("    Otype = " + str(Otype))
    gui.printlnCond()
    
    gui.printlnCond("10. Calculate RAAN' based on the orbit type")
    if ((Otype == 1) or (Otype == 2)):
        RAANp = 0.0
        gui.printlnCond("    For orbit types 1 and 2, RAAN' = 0.0 degrees")
    else:
        RAANp = RAAN
        gui.printlnCond("    For orbit types other than 1 and 2, RAAN' = RAAN")
    gui.printlnCond("    Thus, RAAN' = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAANp) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("11. Compute w' based on the orbit type")
    if ((Otype == 1) or (Otype == 3)):
        gui.printlnCond("    For orbit types 1 and 3, w' = 0.0 degrees")
        argofperip = 0.0
    else:
        argofperip = argofperi
        gui.printlnCond("    For orbit types other than 1 and 3, w' = w")
    gui.printlnCond("    Thus, w' = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperip) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("12. Use the 'g' functions to rotate the positional")
    gui.printlnCond("    vector R' by -w' degrees about the z-axis")
    R = ASTMath.RotateZ(-argofperip, R)
    gui.printlnCond("    Rotating R' by " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,-argofperip) + 
                    " degrees about the z-axis gives")
    gui.printlnCond("    x' = " + ASTStr.insertCommas(R.x(),8))
    gui.printlnCond("    y' = " + ASTStr.insertCommas(R.y(),8))
    gui.printlnCond("    z' = " + ASTStr.insertCommas(R.z(),8))
    gui.printlnCond()
    
    gui.printlnCond("13. Use the 'f' functions to rotate the vector from the previous step")
    gui.printlnCond("    by -inclination degrees about the x-axis")
    R = ASTMath.RotateX(-inclin, R)
    gui.printlnCond("    Rotating R' by " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,-inclin) + 
                    " degrees about the x-axis gives")
    gui.printlnCond("    x' = " + ASTStr.insertCommas(R.x(),8))
    gui.printlnCond("    y' = " + ASTStr.insertCommas(R.y(),8))
    gui.printlnCond("    z' = " + ASTStr.insertCommas(R.z(),8))
    gui.printlnCond()
    
    gui.printlnCond("14. Use the 'g' functions to rotate the vector from the previous step")
    gui.printlnCond("    by -RAANp degrees about the z-axis to get the positional vector")
    R = ASTMath.RotateZ(-RAANp, R)
    gui.printlnCond("    Rotating R' by " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,-RAANp) + 
                    " degrees about the z-axis gives")
    gui.printlnCond("    x = " + ASTStr.insertCommas(R.x(),8))
    gui.printlnCond("    y = " + ASTStr.insertCommas(R.y(),8))
    gui.printlnCond("    z = " + ASTStr.insertCommas(R.z(),8))
    gui.printlnCond()
    
    gui.printlnCond("15. Use the 'g' functions to rotate the velocity")
    gui.printlnCond("    vector V' by -w' degrees about the z-axis")
    V = ASTMath.RotateZ(-argofperip, V)
    gui.printlnCond("    Rotating V' by " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,-argofperip) + 
                    " degrees about the z-axis gives")
    gui.printlnCond("    Vx' = " + ASTStr.insertCommas(V.x(),8))
    gui.printlnCond("    Vy' = " + ASTStr.insertCommas(V.y(),8))
    gui.printlnCond("    Vz' = " + ASTStr.insertCommas(V.z(),8))
    gui.printlnCond()
    
    gui.printlnCond("16. Use the 'f' functions to rotate the vector from the previous step")
    gui.printlnCond("    by -inclination degrees about the x-axis")
    V = ASTMath.RotateX(-inclin, V)
    gui.printlnCond("    Rotating V' by " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,-inclin) + 
                    " degrees about the x-axis gives")
    gui.printlnCond("    Vx' = " + ASTStr.insertCommas(V.x(),8))
    gui.printlnCond("    Vy' = " + ASTStr.insertCommas(V.y(),8))
    gui.printlnCond("    Vz' = " + ASTStr.insertCommas(V.z(),8))
    gui.printlnCond()
    
    gui.printlnCond("17. Use the 'g' functions to rotate the vector from the previous step")
    gui.printlnCond("    by -RAANp degrees about the z-axis to get the velocity vector")
    V = ASTMath.RotateZ(-RAANp, V)
    gui.printlnCond("    Rotating V' by " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,-RAANp) + 
                    " degrees about the z-axis gives")
    gui.printlnCond("    Vx = " + ASTStr.insertCommas(V.x(),8))
    gui.printlnCond("    Vy = " + ASTStr.insertCommas(V.y(),8))
    gui.printlnCond("    Vz = " + ASTStr.insertCommas(V.z(),8))
    gui.printlnCond()
    
    prt.println("Input Keplerian Elements (assuming units are km and km/s):")
    prt.println("  Inclination = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    prt.println("  Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
    prt.println("  Length of Semi-major axis = " + ASTStr.insertCommas(axis, 8) + " km")
    prt.println("  RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
    prt.println("  Argument of Perigee = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
    prt.println("  Mean anomaly at the epoch = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    prt.println()
    
    prt.println("Gives the State Vector:")
    prt.println("  [" + ASTStr.insertCommas(R.x(), 3) + ", " + ASTStr.insertCommas(R.y(), 3) +
                ", " + ASTStr.insertCommas(R.z(), 3) + "] (positional vector)")
    prt.println("  [" + ASTStr.insertCommas(V.x(), 3) + ", " + ASTStr.insertCommas(V.y(), 3) +
                ", " + ASTStr.insertCommas(V.z(), 3) + "] (velocity vector)")
    prt.println()
    prt.println("State Vector gives a velocity of " + str(ASTMath.Round(V.len(), 3)) + " km/s at a distance")
    prt.println("of " + ASTStr.insertCommas(R.len(), 3) + " km (" + 
                ASTStr.insertCommas(ASTMath.KM2Miles(R.len()), 3) +
                " miles) above the center of the Earth.")
    prt.println()
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("State Vector components are listed in the text area below")



def calcSpherical2Cartesian(gui):
    """
    Convert spherical coordinates to Cartesian
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Convert Spherical Coordinates to Cartesian", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (ASTQuery.showQueryForm(["r", "alpha", "phi"], "Enter Spherical coordinates ...") != ASTQuery.QUERY_OK):
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1), HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        r = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid r value - try again", "Invalid r")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2), HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        alpha = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid alpha value - try again", "Invalid alpha")
        return
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3), HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        phi = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid phi value - try again", "Invalid phi")
        return
    
    gui.printlnCond("Convert r= " + str(r) + ", alpha = " + str(alpha) + 
                    ", phi = " + str(phi) + " to cartesian coordinates")
    gui.printlnCond()
    
    x = r * ASTMath.COS_D(alpha) * ASTMath.SIN_D(phi)
    gui.printlnCond("1.  x = r * cos(alpha) * sin(phi)")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r) + " * cos(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) +
                    ") * sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,phi) + ")")
    gui.printlnCond("      = " + str(x))
    gui.printlnCond()
    
    y = r * ASTMath.SIN_D(alpha) * ASTMath.SIN_D(phi)
    gui.printlnCond("2.  y = r * sin(alpha) * sin(phi)")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r) + " * sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) +
                    ") * sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,phi) + ")")
    gui.printlnCond("      = " + str(y))
    gui.printlnCond()
    
    z = r * ASTMath.COS_D(phi)
    gui.printlnCond("3.  z = r * cos(phi) = " + str(r) + " * cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,phi) + ")")
    gui.printlnCond("        = " + str(z))
    gui.printlnCond()
    
    result = "[r=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,r) +\
             ", alpha=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,alpha) +\
             ", phi=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,phi) +\
             "] = [x=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) +\
             ", y=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) +\
             ", z=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,z) + "]"
    gui.printlnCond("Thus, [r=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,r) + 
                    ", alpha=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,alpha) +
                    ", phi=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,phi) + "]")
    gui.printlnCond("    = [x=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,x) + 
                    ", y=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,y) +
                    ", z=" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,z) + "]")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(result)
    


def calcState2Keplerian(gui):
    """
    Convert a state vector to Keplerian elements
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    H = ASTVect.ASTVect()
    N = ASTVect.ASTVect()
    e = ASTVect.ASTVect()
    
    # Note: dv is the satellite's true anomaly. It is undefined for circular
    # orbits. For all other orbits, the true anomaly is calculated below.
    # However, it is initialized here to avoid compiler warnings about
    # a potentially uninitialized variable.
    dv = 1.0
    
    gui.clearTextAreas()
    
    if not (VDator.validateStateVect()):
        return
    R = VDator.getPosVect()
    V = VDator.getVelVect()
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert a State Vector to its Keplerian Elements", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("Convert the state vector")
    gui.printlnCond("  [" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) + 
                    ", " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) +
                    ", " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + "] (positional vector)")
    gui.printlnCond("  [" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.x()) + 
                    ", " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.y()) +
                    ", " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.z()) + "] (velocity vector)")
    gui.printlnCond("to its equivalent Keplerian elements")
    gui.printlnCond()
    
    gui.printlnCond("1.  Compute the length of the position vector R")
    gui.printlnCond("    Rlen = sqrt[x^2 + y^2 + z^2]")
    gui.printlnCond("         = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) + 
                    ")^2 + (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) +
                    ")^2 + (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + ")^2]")
    gui.printlnCond("         = " + ASTStr.insertCommas(R.len(),8))
    gui.printlnCond()
    
    gui.printlnCond("2.  Compute the length of the velocity vector V")
    gui.printlnCond("    Vlen = sqrt[Vx^2 + Vy^2 + Vz^2]")
    gui.printlnCond("         = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.x()) + 
                    ")^2 + (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.y()) +
                    ")^2 + (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.z()) + ")^2]")
    gui.printlnCond("         = " + ASTStr.insertCommas(V.len(),8))
    gui.printlnCond()
    
    x = R.y() * V.z() - R.z() * V.y()
    y = R.z() * V.x() - R.x() * V.z()
    z = R.x() * V.y() - R.y() * V.x()
    H.setVect(x, y, z)
    gui.printlnCond("3.  Compute the angular momentum vector H=[Hx Hy Hz]")
    gui.printlnCond("    Hx = yVz - zVy = (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) +
                    ")*(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.z()) + ") - (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + ")*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.y()) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(H.x(),8))
    gui.printlnCond("    Hy = zVx - xVz = (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + 
                    ")*(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.x()) + ") - (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) + ")*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.z()) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(H.y(),8))
    gui.printlnCond("    Hz = xVy - yVx = (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) +
                    ")*(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.y()) + ") - (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) + ")*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.x()) + ")")
    gui.printlnCond("       = " + ASTStr.insertCommas(H.z(),8))
    gui.printlnCond()
    
    gui.printlnCond("4.  Compute the length of the angular momentum vector H")
    gui.printlnCond("    Hlen = sqrt[Hx^2 + Hy^2 + Hz^2]")
    gui.printlnCond("         = sqrt[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,H.x()) + 
                    ")^2 + (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,H.y()) +
                    ")^2 + (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,H.z()) + ")^2]")
    gui.printlnCond("         = " + ASTStr.insertCommas(H.len(),8))
    gui.printlnCond()
    
    x = -H.y()
    y = H.x()
    z = 0.0
    N.setVect(x, y, z)
    gui.printlnCond("5.  Compute the node vector.")
    gui.printlnCond("    Nx = -Hy = -(" + str(H.y()) + ") = " + ASTStr.insertCommas(N.x(),8))
    gui.printlnCond("    Ny = Hx = " + ASTStr.insertCommas(N.y(),8))
    gui.printlnCond("    Nz = 0.0")
    gui.printlnCond()
    
    gui.printlnCond("6.  Compute the length of the node vector N")
    gui.printlnCond("    Nlen = " + ASTStr.insertCommas(N.len(),8))
    gui.printlnCond()
    
    A = V.len()*V.len() - (ASTOrbits.muEARTH / R.len())
    B = R.x() * V.x() + R.y() * V.y() + R.z() * V.z()
    gui.printlnCond("7.  Calculate temporary variables A and B")
    gui.printlnCond("    A = Vlen^2 - (muEARTH/Rlen) = " + ASTStr.insertCommas(V.len(),8) + "^2 - (" +
                    str(ASTOrbits.muEARTH) + "/" + ASTStr.insertCommas(R.len(),8) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A))
    gui.printlnCond("    B = xVx + yVy + zVz")
    gui.printlnCond("      = (" + ASTStr.insertCommas(R.x(),8) + ")*(" + ASTStr.insertCommas(V.x(),8) + ") + (" +
                    ASTStr.insertCommas(R.y(),8) + ")*(" + ASTStr.insertCommas(V.y(),8) + ") + (" + 
                    ASTStr.insertCommas(R.z(),8) + ")*(" + ASTStr.insertCommas(V.z(),8) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,B))
    gui.printlnCond()
    
    x = (R.x() * A - B * V.x()) / ASTOrbits.muEARTH
    y = (R.y() * A - B * V.y()) / ASTOrbits.muEARTH
    z = (R.z() * A - B * V.z()) / ASTOrbits.muEARTH
    e.setVect(x, y, z)
    gui.printlnCond("8.  Compute the eccentricity vector")
    gui.printlnCond("    ex = [xA - BVx]/muEARTH")
    gui.printlnCond("       = [(" + ASTStr.insertCommas(R.x(),8) + ")*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A) + ") - (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,B) + ")*(" + 
                    ASTStr.insertCommas(V.x(),8) + ")]/" + str(ASTOrbits.muEARTH))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,x))
    gui.printlnCond("    ey = [yA - BVy]/muEARTH")
    gui.printlnCond("       = [(" + ASTStr.insertCommas(R.y(),8) + ")*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A) + ") - (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,B) + ")*(" + 
                    ASTStr.insertCommas(V.y(),8) + ")]/" + str(ASTOrbits.muEARTH))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,y))
    gui.printlnCond("    ez = [zA - BVz]/muEARTH")
    gui.printlnCond("       = [(" + ASTStr.insertCommas(R.z(),8) + ")*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,A) + ") - (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,B) + ")*(" + 
                    ASTStr.insertCommas(V.z(),8) + ")]/" + str(ASTOrbits.muEARTH))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,z))
    gui.printlnCond()
    
    gui.printlnCond("9.  Compute the length of the eccentricity vector to get the orbital eccentricity")
    gui.printlnCond("    e = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()))
    gui.printlnCond()
    
    rho = (H.len()*H.len()) / ASTOrbits.muEARTH
    gui.printlnCond("10. Compute the semi-latus rectum")
    gui.printlnCond("    p = (Hlen^2)/muEARTH = (" + ASTStr.insertCommas(H.len(),8) + "^2)/" + str(ASTOrbits.muEARTH))
    gui.printlnCond("      = " + ASTStr.insertCommas(rho,8))
    gui.printlnCond()
    
    axis = rho / (1 - (e.len()*e.len()))
    gui.printlnCond("11. Use the semi-latus rectum and eccentricity to compute the semi-major axis")
    gui.printlnCond("    a = p/(1 - e^2) = " + ASTStr.insertCommas(rho,8) + "/(1 - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) + "^2)")
    gui.printlnCond("      = " + ASTStr.insertCommas(axis,8))
    gui.printlnCond()
    
    inclin = ASTMath.INVCOS_D(H.z() / H.len())
    gui.printlnCond("12. Compute the orbital inclination")
    gui.printlnCond("    inclin = inv cos(Hz/Hlen) = inv cos(" + ASTStr.insertCommas(H.z(),8) + 
                    "/" + ASTStr.insertCommas(H.len(),8) + ")")
    gui.printlnCond("           = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    gui.printlnCond()
    
    Otype = ASTOrbits.getSatOrbitType(e.len(), inclin)
    if (Otype < 0):
        ASTMsg.errMsg("Orbit must be elliptical. The computed eccentricity\n" +
                      "and inclination are not supported.","Invalid Orbit type")
        return
    gui.printlnCond("13. Determine the orbit type based on orbital eccentricity and inclination")
    gui.printlnCond("    Otype = " + str(Otype))
    gui.printlnCond()
    
    gui.printlnCond("14. Calculate the right ascension of the ascending node")
    if ((Otype == 1) or (Otype == 2)):
        RAAN = 0.0
        gui.printlnCond("    RAAN = 0.0 degrees for orbit types 1 and 2")
    else:
        RAAN = ASTMath.INVCOS_D(-H.y() / N.len())
        gui.printlnCond("    RAAN = inv cos(-Hy/Nlen) = inv cos[-(" + 
                        ASTStr.insertCommas(H.y(),8) + ")/" + ASTStr.insertCommas(N.len(),8) + "]")
        gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
        gui.printlnCond("    if Hx < 0, subtract RAAN from 360 to put it into the correct quadrant")
        if (H.x() < 0.0):
            RAAN = 360.0 - RAAN
            gui.printlnCond("    RAAN = 360 - RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
        else:
            gui.printlnCond("    No adjustment required.")
    gui.printlnCond()
    
    argofperi = 0.0
    gui.printlnCond("15. Calculate the argument of perigee")
    if ((Otype == 1) or (Otype == 3)):
        argofperi = 0.0
        gui.printlnCond("    w = 0.0 degrees for orbit types 1 and 3. Skip to step 18.")
    else:
        gui.printlnCond("    For orbit types 2 and 4, proceed to step 16.")
    gui.printlnCond()
    
    w_t = ASTMath.INVCOS_D(e.x() / e.len())
    if (e.y() < 0.0):
        w_t = 360.0 - w_t
    gui.printlnCond("16. Calculate the argument of perigee")
    if ((Otype == 1) or (Otype == 3)):
        gui.printlnCond("    For orbit types 1 and 3, skip to step 18.")
    elif (Otype == 2):
        gui.printlnCond("    For orbit type 2, set w = w_t and skip to step 18.")
        argofperi = ASTMath.INVCOS_D(e.x() / e.len())
        gui.printlnCond("    w = w_t = inv cos(ex/e) = inv cos(" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.x()) + "/" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) + ")")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
        gui.printlnCond("    if ey < 0, subtract w from 360 to put it into the correct quadrant")
        if (e.y() < 0.0):
            argofperi = 360.0 - argofperi
            gui.printlnCond("    w = 360 - w = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
        else:
            gui.printlnCond("    No adjustment required.")
    else:
        gui.printlnCond("    For orbit type 4, must proceed to step 17, but")
        gui.printlnCond("    w_t = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,w_t) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("17. Calculate the argument of perigee")
    if (Otype != 4):
        gui.printlnCond("   Skip to step 18 for this orbit type. w was calculated in a prior step.")
    else:
        argofperi = ASTMath.INVCOS_D((e.y() * H.x() - e.x() * H.y()) / (e.len() * N.len()))
        gui.printlnCond("    w = inv cos[(eyHx-exHy)/(eNlen)]")
        gui.printlnCond("      = inv cos[((" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.y()) +
                        ")*(" + ASTStr.insertCommas(H.x(),8) + ") - (" +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.x()) + ")*(" + 
                        ASTStr.insertCommas(H.y(),8) + "))/")
        gui.printlnCond("                (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) +
                        ")*(" + ASTStr.insertCommas(N.len(),8) + "))]")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
        gui.printlnCond("     if ez < 0, subtract w from 360 to put it into the correct quadrant")
        if (e.z() < 0.0):
            argofperi = 360.0 - argofperi
            gui.printlnCond("     w = 360 - w = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
        else:
            gui.printlnCond("     No adjustment required.")
    gui.printlnCond()
    
    gui.printlnCond("18. Compute the object's true anomaly")
    if (Otype == 1):
        L_t = ASTMath.INVCOS_D(R.x() / R.len())
        dv = L_t
        gui.printlnCond("    For orbit type 1, v = L_t and skip to step 21.")
        gui.printlnCond("    v = L_t = inv cos(x/Rlen) = inv cos(" + ASTStr.insertCommas(R.x(),8) + 
                        "/" + ASTStr.insertCommas(R.len(),8) + ")")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,L_t) + " degrees")
        gui.printlnCond("    if y < 0, subtract v from 360 to put it into the correct quadrant")
        if (R.y() < 0.0):
            dv = 360.0 - dv
            gui.printlnCond("    v = 360 - v = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
        else:
            gui.printlnCond("    No adjustment required.")
    else:
        gui.printlnCond("    For orbit types other than 1, proceed to next step")
    gui.printlnCond()
    
    u = ASTMath.INVCOS_D((R.x() * N.x() + R.y() * N.y()) / (N.len() * R.len()))
    if (R.z() < 0):
        u = 360.0 - u
    gui.printlnCond("19. Compute the object's true anomaly.")
    if (Otype == 1):
        gui.printlnCond("    For orbit type 1, proceed to step 21.")
    elif (Otype == 3):
        dv = ASTMath.INVCOS_D((R.x() * N.x() + R.y() * N.y()) / (N.len() * R.len()))
        gui.printlnCond("    For orbit type 3, set v = u and skip to step 21.")
        gui.printlnCond("    u = inv cos[(xNx + yNy)/(Nlen*Rlen)])")
        gui.printlnCond("      = inv cos[((" + ASTStr.insertCommas(R.x(),8) + ")*(" +
                        ASTStr.insertCommas(N.x(),8) + ") + (" +
                        ASTStr.insertCommas(R.y(),8) + ")*(" + ASTStr.insertCommas(N.y(),8) + 
                        "))/(" + ASTStr.insertCommas(N.len(),8) + "*" +
                        ASTStr.insertCommas(R.len(),8) + ")]")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
        gui.printlnCond("    if z < 0, subtract v from 360 to put it into the correct quadrant")
        if (R.z() < 0.0):
            dv = 360.0 - dv
            gui.printlnCond("    v = 360 - v = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
        else:
            gui.printlnCond("    No adjustment required.")
    else:
        gui.printlnCond("    For orbit types other than 3, proceed to the next step, but")
        gui.printlnCond("    u = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,u) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("20. Compute the object's true anomaly.")
    if ((Otype == 1) or (Otype == 3)):
        gui.printlnCond("    For orbit types 1 and 3, skip to step 21.")
    else:
        dv = ASTMath.INVCOS_D((e.x() * R.x() + e.y() * R.y() + e.z() * R.z()) / (e.len() * R.len()))
        gui.printlnCond("    v = inv cos[(ex*x + ey*y + ez*z)/(e*Rlen)]")
        gui.printlnCond("      = inv cos[((" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.x()) +
                        ")*(" + ASTStr.insertCommas(R.x(),8) + ") + (" +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.y()) + ")*(" + 
                        ASTStr.insertCommas(R.y(),8) + ") + (" +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.z()) + ")*(" + 
                        ASTStr.insertCommas(R.z(),8) + "))/")
        gui.printlnCond("                (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) +
                        "*" + ASTStr.insertCommas(R.len(),8) + ")]")
        gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
        gui.printlnCond("    if B < 0, subtract v from 360 to put it into the correct quadrant")
        if (B < 0.0):
            dv = 360.0 - dv
            gui.printlnCond("    v = 360 - v = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + " degrees")
        else:
            gui.printlnCond("    No adjustment required.")
    gui.printlnCond()
    
    EA = ASTMath.INVCOS_D((e.len() + ASTMath.COS_D(dv)) / (1 + e.len() * ASTMath.COS_D(dv)))
    gui.printlnCond("21. Use the true anomaly to calculate the eccentric anomaly")
    gui.printlnCond("    E = inv cos[(e + cos(v))/(1 + e*cos(v))]")
    gui.printlnCond("      = inv cos[(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) + 
                    " + cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + "))/(1 + " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) + "*cos(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dv) + "))]")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,EA) + " degrees")
    gui.printlnCond("    if v > 180, then subtract E from 360 to put it into the correct quadrant")
    if (dv > 180.0):
        EA = 360 - EA
        gui.printlnCond("    E = 360 - E = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,EA) + " degrees")
    else:
        gui.printlnCond("    No adjustment necessary")
    gui.printlnCond()
    
    MA = ASTMath.deg2rad(EA) - e.len() * ASTMath.SIN_D(EA)
    gui.printlnCond("22. Use E to compute the mean anomaly at the epoch.")
    gui.printlnCond("    M = E - e*sin(E) where E **must** be in radians")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ASTMath.deg2rad(EA)) + " - " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()) + "*sin(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ASTMath.deg2rad(EA)) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MA) + " radians")
    gui.printlnCond()
    
    M0 = ASTMath.rad2deg(MA)
    gui.printlnCond("23. Convert M to degrees")
    gui.printlnCond("    M0 = (180*M)/PI = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    gui.printlnCond()
    
    prt.println("Input State Vector (assuming units are km and km/s):")
    prt.println("  [" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.x()) + ", " + 
                ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.y()) + ", " + 
                ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,R.z()) + "] (positional vector)")
    prt.println("  [" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.x()) + ", " + 
                ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.y()) +
                ", " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,V.z()) + "] (velocity vector)")
    prt.println()
    prt.println("State Vector gives a velocity of " + str(ASTMath.Round(V.len(), 3)) + " km/s at a distance")
    prt.println("of " + ASTStr.insertCommas(R.len(), 3) + " km (" + 
                ASTStr.insertCommas(ASTMath.KM2Miles(R.len()), 3) +
                " miles) above the center of the Earth.")
    prt.println()
    prt.println("Keplerian Elements are:")
    prt.println("  Inclination = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    prt.println("  Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,e.len()))
    prt.println("  Length of Semi-major axis = " + ASTStr.insertCommas(axis, 8) + " km")
    prt.println("  RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
    prt.println("  Argument of Perigee = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
    prt.println("  Mean anomaly at the epoch = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Keplerian Elements are listed in the text area below")



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
