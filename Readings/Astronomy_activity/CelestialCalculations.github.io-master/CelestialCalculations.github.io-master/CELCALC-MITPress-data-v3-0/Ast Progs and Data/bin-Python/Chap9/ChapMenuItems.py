"""
Create menu entries for this chapter's Coord Sys, TLEs,
Launch Sites, and Satellites menus. This module also creates a
class and structure that associates data items with menu
entries to make it easier to determine what actions to 
take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap9.ChapEnums
import Chap9.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    


#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what menu action to do.
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what menu item is to be done
    
    def __init__(self,eCalcType):
        """
        Creates a ChapMenuItem instance
        
        :param CalculationType eCalcType: what menu item is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getCalcType(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createCalcType(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what type of conversion is to be done
    :return: the menu object that got created
    """  
    # define a listener for responding to clicks on a menu item 
    menuItemCallback = lambda: ml.menuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=menuItemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: GUI object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """
    cen = Chap9.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create Coord Sys menu items
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Coord Sys ",menu=tmpmenu)   
    __createCalcType(gui,tmpmenu,"Cartesian to Spherical",cen.CART_TO_SPHERICAL)
    __createCalcType(gui,tmpmenu,"Spherical to Cartesian",cen.SPHERICAL_TO_CART)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"ECI to Horizon Coordinates",cen.ECI_TO_HORIZON)
    __createCalcType(gui,tmpmenu,"ECI to Topocentric Coordinates",cen.ECI_TO_TOPO)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"State Vector to Keplerian Elements",cen.STATEVECTOR_TO_KEPLERIAN)
    __createCalcType(gui,tmpmenu,"Keplerian Elements to State Vector",cen.KEPLERIAN_TO_STATEVECTOR)
    
 
    # Create TLEs menu items
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" TLEs ",menu=tmpmenu) 
    __createCalcType(gui,tmpmenu,"Load TLE Data",cen.LOAD_TLE_DATA)
    __createCalcType(gui,tmpmenu,"Display Raw TLE Data",cen.DISPLAY_RAW_TLE_DATA)
    tmpmenu.add_separator()    
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Find TLE ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"By Object Name",cen.SEARCH_TLE_BY_NAME)
    __createCalcType(gui,tmpbar,"By Catalog ID",cen.SEARCH_TLE_BY_CATID)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Decode TLE Data ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"By Object Name",cen.DECODE_TLE_BY_NAME)
    __createCalcType(gui,tmpbar,"By Catalog ID",cen.DECODE_TLE_BY_CATID)
    __createCalcType(gui,tmpbar,"By Data Set Number",cen.DECODE_TLE_BY_SETNUM)
    tmpmenu.add_separator()
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Mean Motion to Semi-Major Axis ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.AXIS_FROM_MMOTION_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.AXIS_FROM_MMOTION_FROM_INPUT)
    __createCalcType(gui,tmpmenu,"Semi-Major Axis to Mean Motion",cen.MEAN_MOTION_FROM_AXIS)
    __createCalcType(gui,tmpmenu,"Extract Keplerian Elements from TLE",cen.KEPLERIAN_FROM_TLE)
    
    # Launch Sites menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Launch Sites ",menu=tmpmenu)    
    __createCalcType(gui,tmpmenu,"Load Launch Sites",cen.LOAD_LAUNCH_SITES)
    __createCalcType(gui,tmpmenu,"Display All Launch Sites",cen.DISPLAY_ALL_LAUNCH_SITES)
    tmpmenu.add_separator()
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Calculate Orbital Inclination ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From Sites File",cen.INCLINATION_FROM_FILE)
    __createCalcType(gui,tmpbar,"From User Input",cen.INCLINATION_FROM_INPUT)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Calculate Launch Azimuth ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From Sites File",cen.LAUNCH_AZIMUTH_FROM_FILE)
    __createCalcType(gui,tmpbar,"From User Input",cen.LAUNCH_AZIMUTH_FROM_INPUT)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Calculate Launch Velocity ...",menu=tmpbar)     
    __createCalcType(gui,tmpbar,"From Sites File",cen.VELOCITY_FROM_FILE)
    __createCalcType(gui,tmpbar,"From User Input",cen.VELOCITY_FROM_INPUT)
    
    # Satellites menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Satellites ",menu=tmpmenu) 
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Locate Satellite ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.LOCATE_SAT_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.LOCATE_SAT_FROM_INPUT)
    tmpmenu.add_separator()  
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Rise/Set Times ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.RISE_SET_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.RISE_SET_FROM_INPUT)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Satellite Distance ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.SAT_DISTANCE_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.SAT_DISTANCE_FROM_INPUT)
    tmpmenu.add_separator()
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Perigee/Apogee ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.SAT_PERI_APOGEE_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.SAT_PERI_APOGEE_FROM_INPUT)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Orbital Period ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.SAT_PERIOD_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.SAT_PERIOD_FROM_INPUT)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Axis from Orbital Period ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"From TLE Data Set",cen.AXIS_FROM_PERIOD_FROM_TLE)
    __createCalcType(gui,tmpbar,"From User Input",cen.AXIS_FROM_PERIOD_FROM_INPUT)
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Orbital Velocity ...",menu=tmpbar) 
    __createCalcType(gui,tmpbar,"For Circular Orbit",cen.SAT_VELOCITY_CIRCULAR)
    __createCalcType(gui,tmpbar,"For Elliptical Orbit",cen.SAT_VELOCITY_ELLIPTICAL)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Satellite Footprint",cen.SAT_FOOTPRINT)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Set Termination Criteria",cen.TERM_CRITERIA)
    
     

#=========== Main entry point ===============
if __name__ == '__main__':
    pass
