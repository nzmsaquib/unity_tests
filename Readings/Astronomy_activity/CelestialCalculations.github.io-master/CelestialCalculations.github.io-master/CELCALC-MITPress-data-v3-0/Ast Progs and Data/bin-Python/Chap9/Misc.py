"""
Provides some miscellaneous helper functions and data
used throughout the Satellites chapter

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTLE as ASTTLE

# Set a default termination criteria (in radians) for solving Kepler's equation
termCriteria = 0.000002



def Axis2MeanMotion(gui,axis,showInterim=False):
    """
    Convert length of the semi-major axis to mean motion
    
    :param tkwidget gui: main application window
    :param float axis: length of semi-major axis in km
    :param bool showInterim: show intermediate results if true and checkbox is true
    :return: mean motion in revs/day
    """
    N = (1 / (2 * math.pi * axis)) * math.sqrt(ASTOrbits.muEARTH / axis)
    if (showInterim):
        gui.printlnCond("    Convert length of semi-major axis to rev/s")
        gui.printlnCond("    N = [1/(2*pi*a)]*sqrt(muEARTH/a)")
        gui.printlnCond("      = [1/(2*pi*" + str(axis) + ")]*sqrt(" + 
                        ASTStr.insertCommas(ASTOrbits.muEARTH) + "/" + str(axis) + ")")
        gui.printlnCond("      = " + str(N) + " rev/s")
        gui.printlnCond()
    
    MMotion = N * 86400.0
    if (showInterim):
        gui.printlnCond("    Convert N from rev/s to rev/day")
        gui.printlnCond("    Mean Motion = 86,400*N = 86,400*" + str(N))
        gui.printlnCond("                = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " rev/day")
        gui.printlnCond()
    
    return MMotion



def getValidTLENum():
    """
    Ask user for a valid TLE data set number
    
    :return: valid TLE data set number (0-based indexing!) or -1 if an error occurs
    """
    idx = -1
    
    if not (ASTTLE.areTLEsLoaded()):
        ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
        return -1
    if (ASTQuery.showQueryForm(["Enter desired TLE Data set\n" + "number (1-" +
                                str(ASTTLE.getNumTLEDBObjs()) + ")"]) != ASTQuery.QUERY_OK):
        return -1
    
    iTmp = ASTInt.isValidInt(ASTQuery.getData(1),HIDE_ERRORS)
    if (iTmp.isValidIntObj()):
        idx = iTmp.getIntValue()
        if ((idx < 1) or (idx > ASTTLE.getNumTLEDBObjs())):
            ASTMsg.errMsg("No TLE Data Set number " + str(idx) + " exists", "Invalid TLE Data Set Number")
            return -1
        idx = idx - 1        # adjust for 0-based indexing!
    else:
        ASTMsg.errMsg("Invalid TLE data set number - try again", "Invalid TLE Data Set Number")
        return -1
    
    return idx



def MeanMotion2Axis(gui,MMotion,showInterim=False):
    """
    Convert mean motion to length of the semi-major axis
    
    :param tkwidget gui: main application window
    :param float MMotion: mean motion in revs/day
    :param bool showInterim: show intermediate results if true and checkbox is true
    :return: length of the semi-major axis
    """
    N = MMotion / 86400.0
    if (showInterim):
        gui.printlnCond("    Convert mean motion from rev/day to rev/s")
        gui.printlnCond("    N = (Mean Motion)/86,400 = " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + "/86,400")
        gui.printlnCond("      = " + str(N) + " rev/s")
        gui.printlnCond()
    
    axis = 2.0 * math.pi * N
    axis = ASTOrbits.muEARTH / (axis*axis)
    axis = ASTMath.cuberoot(axis)
    if (showInterim):
        gui.printlnCond("    Calculate length of the semi-major axis")
        gui.printlnCond("    a = cuberoot[muEARTH/((2*pi*N)^2)]")
        gui.printlnCond("      = cuberoot[" + ASTStr.insertCommas(ASTOrbits.muEARTH) + 
                        "/(2*pi*" + str(N) + ")^2)]")
        gui.printlnCond("      = " + str(axis) + " km")
        gui.printlnCond()

    return axis



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
