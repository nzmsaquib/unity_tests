"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what menu item to do."""
    # Coord Sys menu   
    CART_TO_SPHERICAL = auto()              # Convert cartesian to spherical coordinates 
    SPHERICAL_TO_CART = auto()              # Convert spherical to cartesian coordinates 
    ECI_TO_HORIZON = auto()                 # Convert ECI to Horizon coordinates 
    ECI_TO_TOPO = auto()                    # Convert ECI to Topocentric coordinates 
    STATEVECTOR_TO_KEPLERIAN = auto()       # Convert state vector to Keplerian elements 
    KEPLERIAN_TO_STATEVECTOR = auto()       # Convert Keplerian elements to state vector 
     
    # TLEs menu
    LOAD_TLE_DATA = auto()                  # Load TLE Data 
    DISPLAY_RAW_TLE_DATA = auto()           # Display Raw TLE Data 
    SEARCH_TLE_BY_NAME = auto()             # Find TLE by object name 
    SEARCH_TLE_BY_CATID = auto()            # Find TLE by Catalog ID 
    DECODE_TLE_BY_NAME = auto()             # Decode TLE data by Object Name 
    DECODE_TLE_BY_CATID = auto()            # Decode TLE data by Catalog ID 
    DECODE_TLE_BY_SETNUM = auto()           # Decode TLE data by Data Set Number 
    AXIS_FROM_MMOTION_FROM_TLE = auto()     # Convert mean motion to semi-major axis via TLE data set 
    AXIS_FROM_MMOTION_FROM_INPUT = auto()   # Convert mean motion to semi-major axis via User Input 
    MEAN_MOTION_FROM_AXIS = auto()          # Convert semi-major axis to mean motion 
    KEPLERIAN_FROM_TLE = auto()             # Extract Keplerian Elements from TLE 
     
    # Launch Sites menu
    LOAD_LAUNCH_SITES = auto()              # Load Launch Sites 
    DISPLAY_ALL_LAUNCH_SITES = auto()       # Display all launch sites 
    INCLINATION_FROM_FILE = auto()          # Calculate orbital inclination from sites file 
    INCLINATION_FROM_INPUT = auto()         # Calculate orbital inclination from user input 
    LAUNCH_AZIMUTH_FROM_FILE = auto()       # Calculate launch azimuth from sites file 
    LAUNCH_AZIMUTH_FROM_INPUT = auto()      # Calculate launch azimuth from user input 
    VELOCITY_FROM_FILE = auto()             # Calculate launch velocity from sites file 
    VELOCITY_FROM_INPUT = auto()            # Calculate launch velocity from user input 
     
    # Satellite menu
    LOCATE_SAT_FROM_TLE = auto()            # Locate satellite from TLE data set 
    LOCATE_SAT_FROM_INPUT = auto()          # Locate satellite from user input 
    RISE_SET_FROM_TLE = auto()              # Calculate rise/set times from TLE data set 
    RISE_SET_FROM_INPUT = auto()            # Calculate rise/set times from user input 
    SAT_DISTANCE_FROM_TLE = auto()          # Calculate satellite distance from TLE data set 
    SAT_DISTANCE_FROM_INPUT = auto()        # Calculate satellite distance from user input 
    SAT_PERI_APOGEE_FROM_TLE = auto()       # Calculate perigee/apogee from TLE data set 
    SAT_PERI_APOGEE_FROM_INPUT = auto()     # Calculate perigee/apogee from user input 
    SAT_PERIOD_FROM_TLE = auto()            # Calculate orbital period from TLE data set 
    SAT_PERIOD_FROM_INPUT = auto()          # Calculate orbital period from user input 
    AXIS_FROM_PERIOD_FROM_TLE = auto()      # Use TLE data set to calculate axis from orbital period 
    AXIS_FROM_PERIOD_FROM_INPUT = auto()    # Use user input to calculate axis from orbital period 
    SAT_VELOCITY_CIRCULAR = auto()          # Calculate orbital velocity for a circular orbit 
    SAT_VELOCITY_ELLIPTICAL = auto()        # Calculate orbital velocity for an elliptical orbit 
    SAT_FOOTPRINT = auto()                  # Calculate satellite footprint 
    TERM_CRITERIA = auto()                  # Set termination criteria for solving Kepler's equation 
     


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
