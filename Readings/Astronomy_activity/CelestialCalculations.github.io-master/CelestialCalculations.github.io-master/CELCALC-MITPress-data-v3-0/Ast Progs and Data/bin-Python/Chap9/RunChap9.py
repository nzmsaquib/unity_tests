#!/usr/bin/env python3

"""
Chapter 9: Satellites

This code implements the main GUI and required routines
for a program that will calculate various items about
satellites, including the location and times for rising/setting.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import os, sys

"""
The runChap function below is created so that this program
can be started by any of the following methods (the 'X' in
'RunChapX' below refers to the chapter to which this
program pertains):

    1. From a command line as
        python RunChapX
        
    2. From an interactive python session, such as with IDLE
        import RunChapX as rt
        rt.RunChapX()
        
    3. By double clicking on the RunChapX.py filename
"""

def runChap():
    """Run the program for this chapter."""
    # Explicitly set PYTHONPATH to include the ASTUtils package
    # so that the user doesn't have to mess with PYTHONPATH or
    # install the ASTUtils package in python's default
    # site-packages directory. This approach assumes
    # that the ASTUtils directory is one level up from the directory
    # in which this program file resides.
    lib_path= os.path.realpath(os.path.join('..'))
    if lib_path not in sys.path:
        sys.path.insert(0, lib_path)
        
    import tkinter as tk
        
    import Chap9.ChapGUI as gui
    
    root = tk.Tk()              # create a root window for the application
    gui.ChapGUI(root)           # populate the root window with widgets
    root.mainloop()



#=========== Main entry point ===============
if __name__ == '__main__':
    runChap()
