"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class ConvType(Enum):
    """
    Define values for the eConvType field in a ChapMenuItem class. These indicate 
    how to do unit conversions.
    """
    CROSS_MULTIPLY = auto()          # Cross-multiply to find an unknown quantity.
    HorDMS_HRSorDEGS = auto()        # Convert HMS to/from hours or convert DMS to/from degrees.
    CENTIGRADE_FAHRENHEIT = auto()   # Convert between Centigrade and Fahrenheit.
        


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
