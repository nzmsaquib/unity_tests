"""
Implements action listeners for the GUI buttons

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTMsg as ASTMsg

import Chap2.ChapEnums
import Chap2.ConvertBtnActions as cba

def convertListener(gui):
    """
    Handle a click on the Convert button
    
    :param tkwidget gui: GUI object from which the request came
    """
    cen = Chap2.ChapEnums.ConvType          # shorten to make typing easier!!
    
    gui.getPrtInstance().clearTextArea()
    convType = gui.getConvType()
    if (convType == cen.CROSS_MULTIPLY):
        gui.setResults(cba.crossMultiply(gui))
    elif (convType == cen.CENTIGRADE_FAHRENHEIT):
        gui.setResults(cba.convertC_F(gui))
    elif (convType == cen.HorDMS_HRSorDEGS):
        gui.setResults(cba.convertHorDMS_decHrsorDegs(gui))
    else:
        ASTMsg.errMsg("Invalid Conversion Type"," ")
    


def swapListener(gui):
    """
    Handle a click on the Swap button
    
    :param tkwidget gui: GUI object from which the request came
    """
    gui.getPrtInstance().clearTextArea()
    gui.swapUnits()  
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass 
