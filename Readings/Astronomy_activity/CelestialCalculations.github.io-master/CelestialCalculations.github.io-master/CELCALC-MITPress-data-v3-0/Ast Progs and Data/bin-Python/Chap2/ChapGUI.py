"""
Implements the main GUI for the application
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.scrolledtext as tkst

import ASTUtils.ASTAboutBox as ASTAboutBox
import ASTUtils.ASTBookInfo as ASTBookInfo
from ASTUtils.ASTMisc import centerWindow
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTPrt as ASTPrt
import ASTUtils.ASTStyle as ASTStyle

import Chap2.ChapEnums
import Chap2.ChapMenuItems as cmi
import Chap2.GUIButtonsListener as gbl
import Chap2.MenusListener as ml

class ChapGUI():
    """
    Define a class for this chapter's main application window.
    Although the code is written to allow multiple top level
    application windows, there really should only be one.
    """
    #==================================================
    # Class variables
    #==================================================
    WINDOW_TITLE = "Chapter 2 - Unit Conversions"
    
    #==================================================
    # Class instance variables
    #==================================================
    # aboutCallback                     callback for the About Box menu item
    # exitCallback                      callback for the Exit menu item
    # instructionsCallback              callback for the Instructions menu item
    # aboutBox                          instance of an ASTAboutBox object
    # chkboxShowInterimCalcs            current state of the checkbox
    # lblResults                        label for showing the results
    # prt                               instance of an ASTPrt object for doing output
    # textPane                          the scrollable text output area
    # txtboxUserInput                   input data field
         
    # These variables are used to keep track of what conversion to do
    # when the user clicks on the Convert button            
    # blFromUnits                       units to convert from
    # lblToUnits                        units to convert to
    # dFromDenom                        the 'from' denominator for cross-multiplying
    # strFromFormat                     the format to use when displaying the "from" value
    # dToDenom                          the "to" denominator for cross-multiplying
    # strToFormat                       the format to use when displaying the "to" value
    # eConvType                         the type of conversion to do
    
    def __init__(self,root):
        """
        Constructor for creating a GUI object in the root window.
        The root window (e.g., root = Tk()) must be created external
        to this object.

        :param tkwidget root: parent for this object
        """
        cen = Chap2.ChapEnums.ConvType              # shorten to make typing easier!!!!        
        
        # To avoid an annoying 'flash' as widgets are added to the GUI,
        # we must hide it and then make it visible after all widgets have
        # been created.
        root.withdraw()
        
        # create dynamic variables for various widgets
        self.lblResults = tk.StringVar()
        self.txtboxUserInput = tk.StringVar()
        self.lblFromUnits = tk.StringVar()
        self.lblToUnits = tk.StringVar()
        self.chkboxShowInterimCalcs = tk.BooleanVar()
        
        # These variables, along with lblFromUnits and lblToUnts, are used to keep track of
        # what conversion to do when the user clicks on the Convert button
        self.dFromDenom = 1.0
        self.strFromFormat = " "
        self.dToDenom = 1.0
        self.strToFormat = " "
        self.eConvType = cen.CROSS_MULTIPLY
        
        # define listeners for menu items
        self.aboutCallback = lambda: ml.aboutMenuListener(self)
        self.exitCallback = lambda: ml.exitMenuListener()
        self.instructionsCallback = lambda: ml.instructionsMenuListener(self.prt)
        
        # Define listeners for the GUI buttons. We could define the callbacks
        # in this module, but putting them in another module separates the
        # actions that must be performed from the GUI (i.e., model-view-controller approach)
        self.convertCallback = lambda: gbl.convertListener(self)
        self.swapCallback = lambda: gbl.swapListener(self)
        
        # Initial size of the application window
        winWidth = 800
        winHeight = 700
           
        # create the root window and center it after all the widgets are created
        root.title(ChapGUI.WINDOW_TITLE)
        root.iconbitmap('BlueMarble.ico')
        root.geometry('{}x{}+{}+{}'.format(winWidth,winHeight,100,100))
        
        #=====================================================
        # create the menu bar
        #=====================================================
        # Note: In the code below, the menu font for cascades may not work because
        # tkinter conforms to the underlying OS's rules for the font and style for
        # top level menu items. If the underlying OS handles the font at the system
        # level (as MS Windows does), then any attempt in the code to change the menu font
        # will not work. However, setting the menu font for individual commands
        # does work.        
        
        menubar = tk.Menu(root)
        
        # create the File menu
        filemenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" File ",menu=filemenu)
        filemenu.add_command(label="Exit",command=self.exitCallback)
        
        # create the Conversions menu
        convmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" Conversions ",menu=convmenu)
        cmi.createConvMenuItems(self,convmenu)
        
        # create the Help menu
        helpmenu=tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" Help",menu=helpmenu)
        helpmenu.add_command(label="Instructions",command=self.instructionsCallback)
        helpmenu.add_command(label="About",command=self.aboutCallback)

        root.config(menu=menubar)
        
        #=====================================================
        # create the rest of the gui widgets
        #=====================================================
     
        # create the book title
        tk.Label(root,text=ASTBookInfo.BOOK_TITLE,font=ASTStyle.BOOK_TITLE_BOLDFONT,
                 fg=ASTStyle.BOOK_TITLE_COLOR,bg=ASTStyle.BOOK_TITLE_BKG).pack(fill=tk.X,expand=False)
         
        # create the intermediate calculations checkbox
        tk.Checkbutton(root,text="Show Intermediate Calculations",
                       variable=self.chkboxShowInterimCalcs,
                       onvalue=True,font=ASTStyle.CBOX_FONT).pack(fill=tk.X,expand=False)
     
        # create the data entry fields and buttons
        txtWidth = 25           # how wide to make the text fields
        f = tk.LabelFrame(root,padx=5,pady=5,bd=4,relief=tk.RIDGE)
        tk.Button(f,text="Convert",width=10,font=ASTStyle.BTN_FONT,
                  command=self.convertCallback).grid(row=0,column=0,padx=5,pady=2)
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxUserInput).grid(row=0,column=1,padx=2)
        tk.Label(f,width=txtWidth,textvariable=self.lblFromUnits,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=0,column=2,sticky=tk.W)
        tk.Button(f,text="Swap Units",width=10,font=ASTStyle.BTN_FONT,
                  command=self.swapCallback).grid(row=1,column=0,padx=5,pady=2)
        tk.Label(f,width=txtWidth,font=ASTStyle.TEXT_FONT,textvariable=self.lblResults,
                 anchor="w").grid(row=1,column=1,padx=2,sticky=tk.W)
        tk.Label(f,width=txtWidth,textvariable=self.lblToUnits,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=1,column=2,sticky=tk.W)   
        f.pack(fill=tk.X,expand=False,padx=4,pady=4)
     
        # create the scrollable text area
        self.textPane = tkst.ScrolledText(root,font=ASTStyle.TEXT_FONT,padx=5,pady=5,borderwidth=5,wrap="word")
        self.textPane.pack(fill=tk.BOTH,expand=True)

        # center the window
        centerWindow(root,winWidth,winHeight)
        
        # Initialize the rest of the GUI
        self.setResults(" ")
        self.chkboxShowInterimCalcs.set(False)
        # Set parent frame for functions that need to relate to a pane/frame/etc. on the GUI
        ASTMsg.setParentFrame(root)
        self.txtboxUserInput.set("")
        self.prt = ASTPrt.ASTPrt(self.textPane)
        self.prt.clearTextArea()
        
        # Finally, make the GUI visible and create a
        # reusable About Box. For some reason, tkinter requires
        # the parent window to be visible before the About Box
        # can be created.
        root.deiconify()
        self.aboutBox = ASTAboutBox.ASTAboutBox(root,ChapGUI.WINDOW_TITLE)
        
    #=====================================================================
    # The methods below conditionally print to the scrollable text area
    #=====================================================================
    
    def printCond(self,txt,center=False):
        """
        Print text to scrollable output area if the
        'Show Intermediate Calculations' checkbox
        is checked.

        :param str txt: text to be printed
        :param bool center: True if text is to be centered
        """        
        if self.chkboxShowInterimCalcs.get():
            self.prt.printnoln(txt,center)    
        
    def printlnCond(self,txt="",centerTxt=False):
        """
        Routines to handle sending output text to the scrollable
        output area. These are wrappers around ASTPrt.println
        that see if the 'Show Intermediate Calculations' checkbox
        is checked before invoking the println functions.

        :param str txt: text to be printed
        :param bool centerTxt: true if the text is to be centered
        """          
        if self.chkboxShowInterimCalcs.get():
            self.prt.println(txt,centerTxt)

    #==================================================================================
    # Define various methods for manipulating the GUI
    #==================================================================================
    
    def getUserInput(self):
        """
        Gets the data that the user entered in the GUI.
        It is up to the calling routine to perform any error checking.

        :return: the string in the txtboxUserInput widget
        """
        return self.txtboxUserInput.get()
    
    def saveFromToValues(self,fromUnits,fromDenom,fromFormat,toUnits,toDenom,toFormat,convType):
        """
        Save the various from/to conversion related items in the appropriate GUI variables.
        
        :param str fromUnits: text to display for the 'from' units (e.g., km)
        :param float fromDenom: numeric value for the 'from' denominator (e.g, 5.0)
        :param str fromFormat: how to format the 'from' denominator
        :param str toUnits: text to display for the 'to' units (e.g., miles)
        :param float toDenom: numeric value for the 'to' denominator (e.g, 5.0)
        :param str toFormat: how to format the 'to' denominator
        :param ConversionType convType: what type of conversion is to be done
        """
        self.lblFromUnits.set(fromUnits)
        self.dFromDenom = fromDenom
        self.strFromFormat = fromFormat
        self.lblToUnits.set(toUnits)
        self.dToDenom = toDenom
        self.strToFormat = toFormat
        self.eConvType = convType
        self.setResults(" ")      
        
    def setResults(self,results):
        """
        Set the results label in the GUI.
        
        :param str results: string to be displayed in the results label
        """
        self.lblResults.set(results)
        
    def swapUnits(self):
        """Swaps the conversion related information and clears the results label in the GUI."""
        tmp = self.lblToUnits.get()
        self.lblToUnits.set(self.lblFromUnits.get())
        self.lblFromUnits.set(tmp)
      
        strTemp = self.strToFormat
        self.strToFormat = self.strFromFormat
        self.strFromFormat = strTemp
      
        dTemp = self.dToDenom
        self.dToDenom = self.dFromDenom
        self.dFromDenom = dTemp
      
        # Also make the current results the input before clearing the results
        self.txtboxUserInput.set(self.lblResults.get())
        self.setResults(" ")
        
    #============================================================================
    # The methods below return references to various GUI components such as the
    # scrollable output text area.
    #============================================================================        
        
    def getPrtInstance(self):
        """
        Gets the ASTPrt instance for this application's scrollable text pane area.
        
        :return: the ASTPrt object for printing to the scrollable text area 
        """        
        return self.prt
    
    def showAboutBox(self):
        """Shows the About Box."""
        self.aboutBox.show()
        
    #============================================================================
    # The methods below return the various items stored by saveFromToValues.
    #============================================================================
    
    def getFromUnits(self):
        return self.lblFromUnits.get()
    def getFromDenom(self):
        return self.dFromDenom
    def getFromFormat(self):
        return self.strFromFormat
    def getToUnits(self):
        return self.lblToUnits.get()
    def getToDenom(self):
        return self.dToDenom
    def getToFormat(self):
        return self.strToFormat
    def getConvType(self):
        return self.eConvType                  
        


#=========== Main entry point ===============
if __name__ == '__main__':
    pass

