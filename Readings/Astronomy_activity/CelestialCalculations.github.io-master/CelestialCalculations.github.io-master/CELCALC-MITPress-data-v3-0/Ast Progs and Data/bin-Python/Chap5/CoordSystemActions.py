"""
Handles the Coord Systems menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS,HMSFORMAT,DMSFORMAT,DECFORMAT
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

def calcEQ2Horiz(gui):
    """
    Convert equatorial coordinates to horizon coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if (ASTQuery.showQueryForm(["Enter Right Ascension (hh:mm:ss.ss)", 
                                "Enter Declination (xxxd yym zz.zzs)"]) != ASTQuery.QUERY_OK):
        return
    
    # validate data
    raObj = ASTTime.isValidTime(ASTQuery.getData(1),HIDE_ERRORS)
    if not (raObj.isValidTimeObj()):
        ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA")
        return
    declObj = ASTAngle.isValidAngle(ASTQuery.getData(2),HIDE_ERRORS)
    if not (declObj.isValidAngleObj()):
        ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl")
        return
    
    gui.clearTextAreas()
    
    result = ASTTime.timeToStr_obj(raObj, HMSFORMAT) + " RA, " + \
             ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to Horizon Coordinates", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    LCT = observer.getObsTime().getDecTime()
    gui.printlnCond("1.  Convert LCT to decimal format. LCT = " +
                    ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) + " hours")
    gui.printlnCond()
    
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    gui.printCond("2.  Convert LCT to UT. UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours")
    
    # adjust the date, if needed, since the LCTtoUT conversion could have changed the date
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    if (dateAdjust < 0):
        gui.printlnCond(" (previous day " + ASTDate.dateToStr_obj(adjustedDate) + ")")
    elif (dateAdjust > 0):
        gui.printlnCond(" (next day " + ASTDate.dateToStr_obj(adjustedDate) + ")")
    else:
        gui.printlnCond()
    
    gui.printlnCond()
    
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    gui.printlnCond("3.  Convert UT to GST. GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    LST = ASTTime.GSTtoLST(GST, observer.getObsLon())
    gui.printlnCond("4.  Convert GST to LST. LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    HA = ASTCoord.RAtoHA(raObj.getDecTime(), LST)
    gui.printlnCond("5.  Convert Right Ascension to Hour Angle. H = " + ASTTime.timeToStr_dec(HA, DECFORMAT) + " hours")
    gui.printlnCond()
     
    horizCoord = ASTCoord.HADecltoHorizon(HA,declObj.getDecAngle(),observer.getObsLat())
    gui.printlnCond("6.  Convert Hour Angle, Declination to horizon coordinates.")
    gui.printlnCond("    Altitude = " + ASTAngle.angleToStr_obj(horizCoord.getAltAngle(), DECFORMAT) + " degrees")
    gui.printlnCond("    Azimuth = " + ASTAngle.angleToStr_obj(horizCoord.getAzAngle(), DECFORMAT) + " degrees")
    gui.printlnCond()
     
    gui.printlnCond("7.  Convert horizon coordinates to DMS format.")
    gui.printlnCond("    Altitude = " + ASTAngle.angleToStr_obj(horizCoord.getAltAngle(), DMSFORMAT))
    gui.printlnCond("    Azimuth = " + ASTAngle.angleToStr_obj(horizCoord.getAzAngle(), DMSFORMAT))
    gui.printlnCond()
     
    result = result + " = " + ASTAngle.angleToStr_obj(horizCoord.getAltAngle(), DMSFORMAT) + " Alt, " +\
             ASTAngle.angleToStr_obj(horizCoord.getAzAngle(), DMSFORMAT) + " Az"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcHoriz2EQ(gui):
    """
    Convert horizon coordinates to equatorial coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()
 
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if (ASTQuery.showQueryForm(["Enter Altitude (xxxd yym zz.zzs)", 
                                "Enter Azimuth (xxxd yym zz.zzs)"]) != ASTQuery.QUERY_OK):
        return

    # validate data
    altObj = ASTAngle.isValidAngle(ASTQuery.getData(1),HIDE_ERRORS)
    if not (altObj.isValidAngleObj()):
        ASTMsg.errMsg("The Altitude entered is invalid - try again.", "Invalid Altitude")
        return
    azObj = ASTAngle.isValidAngle(ASTQuery.getData(2),HIDE_ERRORS)
    if not (azObj.isValidAngleObj()):
        ASTMsg.errMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth")
        return
    
    gui.clearTextAreas()
    
    result = ASTAngle.angleToStr_obj(altObj, DMSFORMAT) + " Altitude, " + \
             ASTAngle.angleToStr_obj(azObj, DMSFORMAT) + " Azimuth"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to Horizon Coordinates", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    LCT = observer.getObsTime().getDecTime()
    gui.printlnCond("1.  Convert LCT to decimal format. LCT = " +
                    ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) + " hours")
    gui.printlnCond()

    UT = ASTTime.LCTtoUT(LCT,gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    gui.printCond("2.  Convert LCT to UT. UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours")
    # adjust the date, if needed, by converting date to JD, adding adjustment, and converting back to a date
    adjustedDate = observer.getObsDate()
    if (dateAdjust != 0):
        JD = ASTDate.dateToJD_obj(adjustedDate) + dateAdjust
        adjustedDate = ASTDate.JDtoDate(JD)

    if (dateAdjust < 0):
        gui.printlnCond(" (previous day " + ASTDate.dateToStr_obj(adjustedDate) + ")")
    elif (dateAdjust > 0):
        gui.printlnCond(" (next day " + ASTDate.dateToStr_obj(adjustedDate) + ")")
    else:
        gui.printlnCond()
    gui.printlnCond()
    
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    gui.printlnCond("3.  Convert UT to GST. GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    LST = ASTTime.GSTtoLST(GST, observer.getObsLon())
    gui.printlnCond("4.  Convert GST to LST. LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    eqCoord = ASTCoord.HorizontoHADecl(altObj.getDecAngle(), azObj.getDecAngle(), observer.getObsLat())
    gui.printlnCond("5.  Convert horizon coordinates to Hour Angle and Declination.")
    gui.printlnCond("    Hour Angle = " + ASTTime.timeToStr_obj(eqCoord.getHAAngle(), DECFORMAT) + " hours")
    gui.printlnCond("    Decl = " + ASTAngle.angleToStr_obj(eqCoord.getDeclAngle(), DECFORMAT) + " degrees")
    gui.printlnCond()
    
    RA = ASTCoord.HAtoRA(eqCoord.getHAAngle().getDecTime(), LST)
    gui.printlnCond("6.  Convert Hour Angle to Right Ascension. RA = " + ASTTime.timeToStr_dec(RA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("7.  Convert equatorial coordinates to HMS + DMS format.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(RA, HMSFORMAT))
    gui.printlnCond("    Decl = " + ASTAngle.angleToStr_obj(eqCoord.getDeclAngle(), DMSFORMAT))
    gui.printlnCond()
    
    result = result + " = " + ASTTime.timeToStr_dec(RA, HMSFORMAT) + " RA, " +\
             ASTAngle.angleToStr_obj(eqCoord.getDeclAngle(), DMSFORMAT) + " Decl"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcPrecession(gui):
    """
    Precess equatorial coordinates from one epoch to another
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    
    # Get the equatorial coordinates to convert
    if (ASTQuery.showQueryForm(["Enter Right Ascension (hh:mm:ss.ss)", 
                                "Enter Declination (xxxd yym zz.zzs)"]) != ASTQuery.QUERY_OK):
        return

    # validate data
    raObj = ASTTime.isValidTime(ASTQuery.getData(1), HIDE_ERRORS)
    if not (raObj.isValidTimeObj()):
        ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA")
        return
    declObj = ASTAngle.isValidAngle(ASTQuery.getData(2), HIDE_ERRORS)
    if not (declObj.isValidAngleObj()):
        ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl")
        return
    
    # Get the epochs to convert to and from
    if (ASTQuery.showQueryForm(["Epoch to Convert From (ex: 1950.0)", 
                                "Epoch to Convert To (ex: 2000.0)"]) != ASTQuery.QUERY_OK):
        return
    
    # validate data
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("The Epoch to convert from is invalid - try again.", "Invalid Epoch")
        return
    EpochFrom = rTmp.getRealValue()
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("The Epoch to convert to is invalid - try again.", "Invalid Epoch ")
        return
    EpochTo = rTmp.getRealValue()

    gui.clearTextAreas()
    
    result = ASTTime.timeToStr_obj(raObj, HMSFORMAT) + " RA, " + \
            ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl"
    
    prt.setBoldFont(True)
    gui.printlnCond("Precess " + result + " from Epoch " + str(EpochFrom) + " to Epoch " + \
                    str(EpochTo), CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()

    prt.setFixedWidthFont()
    
    newCoord = ASTCoord.PrecessEqCoord(raObj.getDecTime(), declObj.getDecAngle(), EpochFrom, EpochTo)
    
    gui.printlnCond("Thus, " + result + " (" + str(EpochFrom) + ") =")
    gui.printlnCond("      " + ASTTime.timeToStr_dec(newCoord.getRAAngle().getDecTime(), HMSFORMAT) +\
                    " RA, " + ASTAngle.angleToStr_dec(newCoord.getDeclAngle().getDecAngle(), DMSFORMAT) +\
                    " Decl (" + str(EpochTo) + ")")
    
    result = result + "(" + str(EpochFrom) + ") = " + \
             ASTTime.timeToStr_dec(newCoord.getRAAngle().getDecTime(), HMSFORMAT) + " RA, " +\
             ASTAngle.angleToStr_dec(newCoord.getDeclAngle().getDecAngle(), DMSFORMAT) + " Decl (" +\
             str(EpochTo) + ")"
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcStarRiseSet(gui):
    """
    Convert rising and setting times for a given eq coord.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    riseSet = True
    H1 = 0.0
    H2 = 0.0
    LSTr = 0.0
    LSTs = 0.0
    LCTr = 0.0
    LCTs = 0.0
    dateAdjustObj = ASTInt.ASTInt()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if (ASTQuery.showQueryForm(["Enter Object Right Ascension (hh:mm:ss.ss)", 
                                "Enter Object Declination (xxxd yym zz.zzs)"]) != ASTQuery.QUERY_OK):
        return
    
    # validate data
    raObj = ASTTime.isValidTime(ASTQuery.getData(1), HIDE_ERRORS)
    if not (raObj.isValidTimeObj()):
        ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA")
        return
    declObj = ASTAngle.isValidAngle(ASTQuery.getData(2), HIDE_ERRORS)
    if not (declObj.isValidAngleObj()):
        ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl")
        return
    
    gui.clearTextAreas()
    
    result = ASTTime.timeToStr_obj(raObj, HMSFORMAT) + " RA, " + \
             ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl"
    
    prt.setBoldFont(True)
    gui.printlnCond("Find when location " + result + " will rise and set", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1.  Convert RA to decimal format. RA = " + ASTTime.timeToStr_obj(raObj, DECFORMAT) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("2.  Convert Decl to decimal format. Decl = " + ASTAngle.angleToStr_obj(declObj, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("3.  Convert observer's latitude to decimal format. Lat = " +
                    ASTAngle.angleToStr_dec(observer.getObsLat(), DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Ar = ASTMath.SIN_D(declObj.getDecAngle()) / ASTMath.COS_D(observer.getObsLat())
    gui.printlnCond("4.  Compute Ar = sin(Decl)/cos(lat) = sin(" + ASTAngle.angleToStr_obj(declObj, DECFORMAT) +
                    ")/cos(" + ASTAngle.angleToStr_dec(observer.getObsLat(), DECFORMAT) + ")")
    gui.printlnCond("               = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ar))
    gui.printlnCond()
    
    gui.printlnCond("5.  if ABS(Ar) > 1, the location doesn't rise or set.")
    if (abs(Ar) > 1):
        gui.printlnCond("    Location does not rise or set. No need to go further")
        riseSet = False
    else:
        gui.printlnCond("    Location may rise or set, so continue.")
    gui.printlnCond()
    
    if (riseSet):
        R = ASTMath.INVCOS_D(Ar)
        gui.printlnCond("6.  Compute R = inv cos(Ar) = inv cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ar) + ")")
        gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R) + " degrees")
        gui.printlnCond()
    
        S = 360.0 - R
        gui.printlnCond("7.  Compute S = 360 - R = 360 - " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,R))
        gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,S) + " degrees")
        gui.printlnCond()
    
        H1 = ASTMath.TAN_D(observer.getObsLat()) * ASTMath.TAN_D(declObj.getDecAngle())
        gui.printlnCond("8.  Compute H1 = tan(Lat)*tan(Decl) = tan(" + 
                        ASTAngle.angleToStr_dec(observer.getObsLat(), DECFORMAT) +
                        ")*tan(" + ASTAngle.angleToStr_obj(declObj, DECFORMAT) + ")")
        gui.printlnCond("               = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, H1))
        gui.printlnCond()
    
        gui.printlnCond("9.  If ABS(H1) > 1, the location doesn't rise or set.")
        if (abs(H1) > 1):
            gui.printlnCond("    Location does not rise or set. No need to go further")
            riseSet = False
        else:
            gui.printlnCond("    Location will rise and set, so continue.")
        gui.printlnCond()
    
    if (riseSet):
        H2 = ASTMath.INVCOS_D(-H1) / 15.0
        gui.printlnCond("10. Calculate H2 = inv cos(-H1)/15 = inv cos[-(" + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,H1) + ")]/15")
        gui.printlnCond("                 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,H2) + " hours")
        gui.printlnCond()
    
        LSTr = 24 + raObj.getDecTime() - H2
        gui.printlnCond("11. Let LSTr = 24 + RA - H2 = 24 + " + ASTTime.timeToStr_obj(raObj, DECFORMAT) + 
                        " - " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,H2))
        gui.printlnCond("             = " + ASTTime.timeToStr_dec(LSTr, DECFORMAT) + " hours")
        gui.printlnCond("    This is the LST for when the location rises above the observer's horizon.")
        gui.printlnCond()
    
        if (LSTr > 24):
            LSTr = LSTr - 24.0
        gui.printlnCond("12. If LSTr > 24, subtract 24. LSTr = " + ASTTime.timeToStr_dec(LSTr, DECFORMAT) + " hours")
        gui.printlnCond()
    
        LSTs = raObj.getDecTime() + H2
        gui.printlnCond("13. Let LSTs = RA + H2 = " + ASTTime.timeToStr_obj(raObj, DECFORMAT) + " + " +
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,H2))
        gui.printlnCond("             = " + ASTTime.timeToStr_dec(LSTs, DECFORMAT) + " hours")
        gui.printlnCond("    This is the LST for when the location sets below the observer's horizon.")
        gui.printlnCond()
    
        if (LSTs > 24):
            LSTs = LSTs - 24.0
        gui.printlnCond("14. If LSTs > 24, subtract 24. LSTs = " + ASTTime.timeToStr_dec(LSTs, DECFORMAT) + " hours")
        gui.printlnCond()
    
        gui.printlnCond("15. Convert LSTr and LSTs to LCTr and LCTs. These are the LCT rise and set times.")
        tmp = ASTTime.LSTtoGST(LSTr, observer.getObsLon())
        gui.printCond("    GSTr = " + ASTTime.timeToStr_dec(tmp, DECFORMAT) + ", ")
        tmp = ASTTime.GSTtoUT(tmp, observer.getObsDate())
        gui.printCond("UTr = " + ASTTime.timeToStr_dec(tmp, DECFORMAT) + ", ")
        LCTr = ASTTime.UTtoLCT(tmp,gui.getDSTStatus(),observer.getObsTimeZone(),observer.getObsLon(),dateAdjustObj)
        gui.printlnCond("LCTr = " + ASTTime.timeToStr_dec(LCTr, DECFORMAT) + " hours")
    
        tmp = ASTTime.LSTtoGST(LSTs, observer.getObsLon())
        gui.printCond("    GSTs = " + ASTTime.timeToStr_dec(tmp, DECFORMAT) + ", ")
        tmp = ASTTime.GSTtoUT(tmp, observer.getObsDate())
        gui.printCond("UTs = " + ASTTime.timeToStr_dec(tmp, DECFORMAT) + ", ")
        LCTs = ASTTime.UTtoLCT(tmp, gui.getDSTStatus(),observer.getObsTimeZone(),observer.getObsLon(),dateAdjustObj)
        gui.printlnCond("LCTs = " + ASTTime.timeToStr_dec(LCTs, DECFORMAT) + " hours")
        gui.printlnCond()
    
        gui.printlnCond("16. Convert LCTr and LCTs to HMS format.")
        gui.printCond("    LCTr = " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + 
                      ", LCTs = " + ASTTime.timeToStr_dec(LCTs, HMSFORMAT))
        gui.printlnCond()
    
    result = "Location " + result
    if (riseSet):
        result = result + " Rises at " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + " LCT, Sets at " +\
                 ASTTime.timeToStr_dec(LCTs, HMSFORMAT) + " LCT"
    else:
        result = result + " does not rise or set"
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
