package jll.celcalc.chap5;

import java.awt.EventQueue;

import javax.swing.JFrame;

import jll.celcalc.ASTUtils.ASTCatalog;
import jll.celcalc.ASTUtils.ASTConstellation;
import jll.celcalc.ASTUtils.ASTFileIO;
import jll.celcalc.ASTUtils.ASTPrt;

/**
 * <b>Chapter 5: Stars in the Nighttime Sky</b>
 * <p>
 * This code implements the main GUI and required routines
 * for a program that will plot stars from a star
 * catalog. The main GUI was mostly built with the Eclipse
 * WindowBuilder, which creates Swing visual components.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class RunChap5 {
	
	/**
	 * Main entry point for the 'Stars in the Nighttime Sky' program.
	 * 
	 * The code inside main was extracted from the GUI code created by the Eclipse
	 * WindowBuilder and extended for this chapter.
	 * 
	 * @param args			optional values passed to the program
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChapGUI frame = new ChapGUI();
					ASTPrt prt = ChapGUI.getPrtInstance();
					
					ASTFileIO.initFileIO(frame);
					ASTConstellation.initConstellations(prt);
					ASTCatalog.initStarCatalogs(prt);
								
					frame.setLocationRelativeTo(null);	// center window on screen
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setVisible(true);
					
					prt.clearTextArea();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
