package jll.celcalc.chap5;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
* This class extends JMenuItem to add some additional fields, which
* simplifies the code for adding items to the menu bar and determining
* what actions to take as various menu items are selected. 
* <p>
* Copyright (c) 2018
* 
* @author J. L. Lawrence
* @version 3.0, 2018
*/

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {
	
	/** Define values for the eCalcType field in a ChapMenuItem class.
	 * These indicate what menu actions to take. */
	protected enum CalculationType {
		
        // Coordinate Systems menu
        /** Convert Equatorial to Horizon Coordinates */ EQ_TO_HORIZ,
        /** Convert Horizon to Equatorial Coordinates */ HORIZ_TO_EQ,
        /** Calculate a precession correction */ CALC_PRECESSION,
        /** Calculate star rise/set times */ STAR_RISE_SET,
        // Star Catalogs menu
        /** Load a star catalog */ LOAD_CATALOG,
        /** Show catalog header information */ SHOW_CATALOG_INFO,
        /** List all objects in the catalog */ LIST_ALL_OBJS_IN_CAT,
        /** List all constellations */ LIST_ALL_CONST,
        /** List all objects in a constellation */ LIST_ALL_OBJS_IN_CONST,
        /** Find constellation an object is in */ FIND_CONST_OBJ_IN,
        // Star Charts menu
        /** Set a visual magnitude filter */ SET_mV_FILTER,
        /** Draw all stars in the catalog */ DRAW_ALL_STARS,
        /** Draw all stars in a constellation */ DRAW_ALL_STARS_IN_CONST,
        /** Draw brightest star in each constellation */ DRAW_ALL_CONST,
        /** Locate an object in the catalog */ FIND_OBJ,
        /** Locate the brightest object in the catalog */ FIND_BRIGHTEST_OBJ,
        /** Locate an equatorial coordinate location */ FIND_EQ_LOC,
        /** Locate a horizon coordinate location */ FIND_HORIZ_LOC
	}
	
	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Cood Systems, Star Catalogs, and Star Charts menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;

		// Coordinate System Conversions menu
		menu = ChapGUI.getCoordSystemsMenu();
		createMenuItem(menu, "Equatorial to Horizon", CalculationType.EQ_TO_HORIZ);
		createMenuItem(menu, "Horizon to Equatorial", CalculationType.HORIZ_TO_EQ);
		menu.addSeparator();
		createMenuItem(menu, "Precession Correction", CalculationType.CALC_PRECESSION);
		menu.addSeparator();
		createMenuItem(menu, "Star Rise/Set Times", CalculationType.STAR_RISE_SET);
		
        // Create Star Catalogs menu
		menu = ChapGUI.getStarCatalogsMenu();
        createMenuItem(menu,"Load a Star Catalog", CalculationType.LOAD_CATALOG);
        createMenuItem(menu,"Show Catalog Information", CalculationType.SHOW_CATALOG_INFO);
		menu.addSeparator();
        createMenuItem(menu,"List all Objects in the Catalog", CalculationType.LIST_ALL_OBJS_IN_CAT);
		menu.addSeparator();
        createMenuItem(menu,"List all Constellations", CalculationType.LIST_ALL_CONST);
        createMenuItem(menu,"List all Objects in a Constellation", CalculationType.LIST_ALL_OBJS_IN_CONST);
        createMenuItem(menu,"Find Constellation an Object is In", CalculationType.FIND_CONST_OBJ_IN);
        
        // Create Star Charts menu
        JMenu tmpSubMenu;
		menu = ChapGUI.getStarChartsMenu();
        createMenuItem(menu,"Set mV Filter", CalculationType.SET_mV_FILTER);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Draw ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"All Stars in the Catalog", CalculationType.DRAW_ALL_STARS);
		createMenuItem(tmpSubMenu,"All Stars in a Constellation", CalculationType.DRAW_ALL_STARS_IN_CONST);
		createMenuItem(tmpSubMenu,"Brightest Star in each Constellation", CalculationType.DRAW_ALL_CONST);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Locate ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"An Object in the Catalog", CalculationType.FIND_OBJ);
		createMenuItem(tmpSubMenu,"Brightest Object in the Catalog", CalculationType.FIND_BRIGHTEST_OBJ);
		tmpSubMenu.addSeparator();
		createMenuItem(tmpSubMenu,"An Equatorial Coordinate", CalculationType.FIND_EQ_LOC);
		createMenuItem(tmpSubMenu,"A Horizon Coordinate", CalculationType.FIND_HORIZ_LOC);
	}
		
	/** Define a class for what a  menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of calculation to do
		private CalculationType eCalcType;   // what type of calculation to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of calculation is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {
			super(title);
			eCalcType = calcType;
		}
		
		/*=======================================================
		 * Define accessor methods for the menu item fields
		 *======================================================*/
		
		/**
		 * Gets the type of calculation to perform.
		 * 
		 * @return		the type of calculation to perform
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}
	
	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 *----------------------------------------------------------------------------*/

	/**
	 * Creates a menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of calculation to perform
	 * @return					ChapMnuItm created
	 */     
	private ChapMnuItm createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getChapMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());
		
		return tmp;
	}

}
