package jll.celcalc.chap5;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap5.ChapMenuItems.ChapMnuItm;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */
class MenusListener implements ActionListener {

	private static CoordSystemActions cs_actions = new CoordSystemActions();
	private static StarCatalogActions starcat_actions = new StarCatalogActions();
	private static StarChartActions starchart_actions = new StarChartActions();
	
	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}

		// Handle Coord Systems, Star Catalogs, and Star Charts menus
		else if (action.equals(ChapGUI.getChapMenuCommand())) {
			ChapMnuItm objMenuItem = (ChapMnuItm) e.getSource();		// the menu item the user clicked on

			switch (objMenuItem.getCalcType()) {
			// ************* Coord Systems menu
			case EQ_TO_HORIZ:
				cs_actions.calcEQ2Horiz();
				break;
			case HORIZ_TO_EQ:
				cs_actions.calcHoriz2EQ();
				break;
			case CALC_PRECESSION:
				cs_actions.calcPrecession();
				break;
			case STAR_RISE_SET:
				cs_actions.calcStarRiseSet();
				break;
				// ************* Star Catalogs menu
			case LOAD_CATALOG:
				starcat_actions.loadCatalog();
				break;
			case SHOW_CATALOG_INFO:
				starcat_actions.showCatalogInfo();
				break;
			case LIST_ALL_OBJS_IN_CAT:
				starcat_actions.listAllObjsInCatalog();
				break;
			case LIST_ALL_CONST:
				starcat_actions.listAllConstellations();
				break;
			case LIST_ALL_OBJS_IN_CONST:
				starcat_actions.listAllObjsInConstellation();
				break;
			case FIND_CONST_OBJ_IN:
				starcat_actions.findConstellationForRA_Decl();
				break;
				// ************* Star Charts menu
			case SET_mV_FILTER:
				starchart_actions.changemVFilter();
				break;
			case DRAW_ALL_STARS:
				starchart_actions.drawAllStars();
				break;
			case DRAW_ALL_STARS_IN_CONST:
				starchart_actions.drawAllStarsInConst();
				break;
			case DRAW_ALL_CONST:
				starchart_actions.drawBrightestStarInAllConst();
				break;
			case FIND_OBJ:
				starchart_actions.locateStarCatalogObj();
				break;
			case FIND_BRIGHTEST_OBJ:
				starchart_actions.locateBrightestObjInCat();
				break;
			case FIND_EQ_LOC:
				starchart_actions.locateRADecl();
				break;
			case FIND_HORIZ_LOC:
				starchart_actions.locateAltAz();
				break;
			default:
				ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
						"from the 'Coord Systems,' 'Star Catalogs,' or 'Star Charts' menu and try again.", "No Menu Item Selected");
				break;
			}
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			ChapGUI.clearTextAreas();
			prt.clearTextArea();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
					"be used in subsequent calculations. (Note that the observer location and time are irrelevant for creating " +
					"star charts of equatorial coordinates because equatorial coordinates are independent of an " +
					"observer's location.) You may find it convenient to enter a default latitude, longitude, and time zone " +
					"in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
					"longitude, and time zone are already filled in with default values when the program starts. By default, " +
					"the date and time will be set to the local date and time at which the program is started.");
			prt.println();
			prt.print("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
					"various calculations are performed. This checkbox only applies to the 'Coord Systems' menu. Select the " +
					"item you want from the 'Coord Systems' menu, enter the required data, and the results will be displayed " +
					"immediately underneath the 'File' label and, if intermediate results are desired, in the scrollable text " +
					"output area (i.e., where these instructions are being displayed). Note that the 'Coord Systems' menu " +
					"provides an entry to adjust an equatorial coordinate for precession. This entry makes a precession " +
					"correction for ");
			prt.setBoldFont(true);
			prt.print("**only**");
			prt.setBoldFont(false);
			prt.println(" the RA/Decl entered. It does not apply a precession correction to the coordinates in " +
					"a star catalog or to the coordinates when a star chart is plotted.");
			prt.println();
			prt.println("The 'Star Catalogs' menu allows you to load a star catalog as was done in the program for " +
					"Chapter 1. You may list the objects in the most recently loaded catalog as well as view basic information " +
					"about the catalog and the constellations. The catalog objects that will be listed are limited to those " +
					"objects that are at least as bright as the magnitude indicated by the 'mV Filter' data field near " +
					"the top right of the application window. For example, if the mV filter is set to 6.5, then " +
					"objects dimmer than magnitude 6.5 will be excluded from a catalog listing. Similarly, when " +
					"plotting catalog objects, those objects that are dimmer than the mV filter will be " +
					"excluded from the star chart that gets created. If a star catalog does not provide the visual " +
					"magnitude for objects in the catalog, the mV filter will be ignored.");
			prt.println();
			prt.println("The first menu entry under the 'Star Charts' menu allows the mV filter to be set. The remaining items in " +
					"this menu allow star charts to be created and displayed. The 'Equatorial Coords Charts' checkbox determines " +
					"what type of star chart to create. If checked, the star chart will be a rectangular plot of right " +
					"ascension and declination (i.e., equatorial coordinates) while if the checkbox " +
					"is not checked, a circular plot of horizon coordinates will be generated. Resizing the application window will " +
					"destroy any star chart that has been created. If this happens, after resizing the window " +
					"to the size you want, simply go through the menu items to redraw the star chart that you want. It is " +
					"typically better to resize the application window to the size you want before generating a star chart.");
			prt.println();
			prt.println("The 'White Bkg for Charts' checkbox allows you to produce charts with a white background (checkbox is checked) " +
					"or a black background (checkbox is not checked). The remaining checkbox, ('Label Stars') indicates whether " +
					"the name of an object should be displayed on the star charts. The menu items under 'Star Charts' provide the " +
					"ability to plot all of the objects in the currently loaded star catalog, only those in a particular " +
					"constellation, or just the brightest star in a constellation. These are provided as aids to help you find " +
					"a constellatiion or to locate an object in a constellation.");
			prt.println();
			prt.println("The 'StarCharts->Locate ...->An Equatorial Coordinate' is particularly useful for finding an object on a " +
					"star chart, even if the currently loaded star catalog does not contain the object. For example, if you know " +
					"the equatorial coordinates for the planet Saturn (perhaps from an astronomy magazine), you can first draw a " +
					"star chart from the currently loaded catalog, then select 'StarCharts->Locate ...->An Equatorial Coordinate' " +
					"to enter Saturn's equatorial coordinates and see where the planet is in relation to the other objects in the " +
					"star chart.");
			prt.println();
			prt.println("Two cautions are worth noting regarding displaying the names of stars on a star chart. First," +
					"the star's name will appear below where the star is plotted. If a star is near one of the chart's sides," +
					"the label may be only partially displayed. Second, you should plot star labels sparingly because plotting " +
					"too many labels will clutter the star chart. For example, check the 'Label Stars' checkbox and then select the " +
					"'Star Charts->Draw ...->Brightest Star in Each Constellation' menu entry to see how cluttered a chart can become!");
			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
