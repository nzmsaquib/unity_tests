package jll.celcalc.chap5;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Coord Systems menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class CoordSystemActions {

	/**
	 * Convert equatorial coordinates to horizon coordinates
	 */
	protected void calcEQ2Horiz() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		String result;
		double LCT, UT, GST, LST, HA;
		ASTInt dateAdjustObj = new ASTInt();
		int dateAdjust;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		if (ASTQuery.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", 
				"Enter Declination (xxxd yym zz.zzs)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTTime raObj = ASTTime.isValidTime(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (!raObj.isValidTimeObj()) {
			ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (!declObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl");
			return;
		}

		ChapGUI.clearTextAreas();

		result = ASTTime.timeToStr(raObj, ASTMisc.HMSFORMAT) + " RA, " + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to Horizon Coordinates", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		LCT = observer.getObsTime().getDecTime();
		ChapGUI.printlnCond("1.  Convert LCT to decimal format. LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		ChapGUI.printCond("2.  Convert LCT to UT. UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours");

		// adjust the date, if needed, since the LCTtoUT conversion could have changed the date
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		if (dateAdjust < 0) ChapGUI.printlnCond(" (previous day " + ASTDate.dateToStr(adjustedDate) + ")");
		else if (dateAdjust > 0) ChapGUI.printlnCond(" (next day " + ASTDate.dateToStr(adjustedDate) + ")");
		else ChapGUI.printlnCond();

		ChapGUI.printlnCond();

		GST = ASTTime.UTtoGST(UT, adjustedDate);
		ChapGUI.printlnCond("3.  Convert UT to GST. GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		LST = ASTTime.GSTtoLST(GST, observer.getObsLon());
		ChapGUI.printlnCond("4.  Convert GST to LST. LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		HA = ASTCoord.RAtoHA(raObj.getDecTime(), LST);
		ChapGUI.printlnCond("5.  Convert Right Ascension to Hour Angle. H = " + ASTTime.timeToStr(HA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ASTCoord horizCoord = ASTCoord.HADecltoHorizon(HA, declObj.getDecAngle(), observer.getObsLat());
		ChapGUI.printlnCond("6.  Convert Hour Angle, Declination to horizon coordinates.");
		ChapGUI.printlnCond("    Altitude = " + ASTAngle.angleToStr(horizCoord.getAltAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Azimuth = " + ASTAngle.angleToStr(horizCoord.getAzAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("7.  Convert horizon coordinates to DMS format.");
		ChapGUI.printlnCond("    Altitude = " + ASTAngle.angleToStr(horizCoord.getAltAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond("    Azimuth = " + ASTAngle.angleToStr(horizCoord.getAzAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = result + " = " + ASTAngle.angleToStr(horizCoord.getAltAngle(), ASTMisc.DMSFORMAT) + " Alt, " +
				ASTAngle.angleToStr(horizCoord.getAzAngle(), ASTMisc.DMSFORMAT) + " Az";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert horizon coordinates to equatorial coordinates
	 */
	protected void calcHoriz2EQ() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		String result;
		double LCT, UT, GST, LST, JD, RA;
		ASTInt dateAdjustObj = new ASTInt();
		int dateAdjust;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		if (ASTQuery.showQueryForm("Enter Altitude (xxxd yym zz.zzs)", 
				"Enter Azimuth (xxxd yym zz.zzs)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTAngle altObj = ASTAngle.isValidAngle(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (!altObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Altitude entered is invalid - try again.", "Invalid Altitude");
			return;
		}
		ASTAngle azObj = ASTAngle.isValidAngle(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (!azObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth");
			return;
		}

		ChapGUI.clearTextAreas();

		result = ASTAngle.angleToStr(altObj, ASTMisc.DMSFORMAT) + " Altitude, " + 
				ASTAngle.angleToStr(azObj, ASTMisc.DMSFORMAT) + " Azimuth";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to Horizon Coordinates", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		LCT = observer.getObsTime().getDecTime();
		ChapGUI.printlnCond("1.  Convert LCT to decimal format. LCT = " + 
				ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		UT = ASTTime.LCTtoUT(LCT,ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		ChapGUI.printCond("2.  Convert LCT to UT. UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours");
		// adjust the date, if needed, by converting date to JD, adding adjustment, and converting back to a date
		ASTDate adjustedDate = observer.getObsDate();
		if (dateAdjust != 0) {
			JD = ASTDate.dateToJD(adjustedDate) + dateAdjust;
			adjustedDate = ASTDate.JDtoDate(JD);
		}
		if (dateAdjust < 0) ChapGUI.printlnCond(" (previous day " + ASTDate.dateToStr(adjustedDate) + ")");
		else if (dateAdjust > 0) ChapGUI.printlnCond(" (next day " + ASTDate.dateToStr(adjustedDate) + ")");
		else ChapGUI.printlnCond();
		ChapGUI.printlnCond();

		GST = ASTTime.UTtoGST(UT, adjustedDate);
		ChapGUI.printlnCond("3.  Convert UT to GST. GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		LST = ASTTime.GSTtoLST(GST, observer.getObsLon());
		ChapGUI.printlnCond("4.  Convert GST to LST. LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ASTCoord eqCoord = ASTCoord.HorizontoHADecl(altObj.getDecAngle(), azObj.getDecAngle(), observer.getObsLat());
		ChapGUI.printlnCond("5.  Convert horizon coordinates to Hour Angle and Declination.");
		ChapGUI.printlnCond("    Hour Angle = " + ASTTime.timeToStr(eqCoord.getHAAngle(), ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    Decl = " + ASTAngle.angleToStr(eqCoord.getDeclAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		RA = ASTCoord.HAtoRA(eqCoord.getHAAngle().getDecTime(), LST);
		ChapGUI.printlnCond("6.  Convert Hour Angle to Right Ascension. RA = " + 
				ASTTime.timeToStr(RA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("7.  Convert equatorial coordinates to HMS + DMS format.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond("    Decl = " + ASTAngle.angleToStr(eqCoord.getDeclAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = result + " = " + ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT) + " RA, " +
				ASTAngle.angleToStr(eqCoord.getDeclAngle(), ASTMisc.DMSFORMAT) + " Decl";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Precess equatorial coordinates from one epoch to another
	 */
	protected void calcPrecession() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String result;
		double EpochFrom, EpochTo;

		// Get the equatorial coordinates to convert
		if (ASTQuery.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", 
				"Enter Declination (xxxd yym zz.zzs)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTTime raObj = ASTTime.isValidTime(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (!raObj.isValidTimeObj()) {
			ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(ASTQuery.getData2(), ASTMisc.HIDE_ERRORS);
		if (!declObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl");
			return;
		}

		// Get the epochs to convert to and from
		if (ASTQuery.showQueryForm("Epoch to Convert From (ex: 1950.0)", 
				"Epoch to Convert To (ex: 2000.0)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTReal rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("The Epoch to convert from is invalid - try again.", "Invalid Epoch");
			return;
		}
		EpochFrom = rTmp.getRealValue();
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("The Epoch to convert to is invalid - try again.", "Invalid Epoch ");
			return;
		}
		EpochTo = rTmp.getRealValue();

		ChapGUI.clearTextAreas();

		result = ASTTime.timeToStr(raObj, ASTMisc.HMSFORMAT) + " RA, " + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Precess " + result + " from Epoch " + EpochFrom + " to Epoch " + EpochTo, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ASTCoord newCoord = ASTCoord.PrecessEqCoord(raObj.getDecTime(), declObj.getDecAngle(), EpochFrom, EpochTo);

		ChapGUI.printlnCond("Thus, " + result + " (" + EpochFrom + ") =");
		ChapGUI.printlnCond("      " + ASTTime.timeToStr(newCoord.getRAAngle().getDecTime(), ASTMisc.HMSFORMAT) + " RA, " +
				ASTAngle.angleToStr(newCoord.getDeclAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Decl (" + EpochTo + ")");

		result = result + "(" + EpochFrom + ") = " + 
				ASTTime.timeToStr(newCoord.getRAAngle().getDecTime(), ASTMisc.HMSFORMAT) + " RA, " +
				ASTAngle.angleToStr(newCoord.getDeclAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Decl (" + EpochTo + ")";

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert rising and setting times for a given eq coord.
	 */
	protected void calcStarRiseSet() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		String result;
		boolean riseSet = true;
		double Ar, R, S, H1 = 0.0, H2 = 0.0;
		double LSTr = 0.0, LSTs = 0.0, LCTr = 0.0, LCTs = 0.0, tmp;
		ASTInt dateAdjustObj = new ASTInt();

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		if (ASTQuery.showQueryForm("Enter Object Right Ascension (hh:mm:ss.ss)", 
				"Enter Object Declination (xxxd yym zz.zzs)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTTime raObj = ASTTime.isValidTime(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (!raObj.isValidTimeObj()) {
			ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(ASTQuery.getData2(), ASTMisc.HIDE_ERRORS);
		if (!declObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl");
			return;
		}

		ChapGUI.clearTextAreas();

		result = ASTTime.timeToStr(raObj, ASTMisc.HMSFORMAT) + " RA, " + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Find when location " + result + " will rise and set", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Convert RA to decimal format. RA = " + ASTTime.timeToStr(raObj, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2.  Convert Decl to decimal format. Decl = " + 
				ASTAngle.angleToStr(declObj, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3.  Convert observer's latitude to decimal format. Lat = " +
				ASTAngle.angleToStr(observer.getObsLat(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Ar = ASTMath.SIN_D(declObj.getDecAngle()) / ASTMath.COS_D(observer.getObsLat());
		ChapGUI.printlnCond("4.  Compute Ar = sin(Decl)/cos(lat) = sin(" + ASTAngle.angleToStr(declObj, ASTMisc.DECFORMAT) +
				")/cos(" + ASTAngle.angleToStr(observer.getObsLat(), ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("               = " + String.format(ASTStyle.genFloatFormat,Ar));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5.  if ABS(Ar) > 1, the location doesn't rise or set.");
		if (Math.abs(Ar) > 1) {
			ChapGUI.printlnCond("    Location does not rise or set. No need to go further");
			riseSet = false;
		} else ChapGUI.printlnCond("    Location may rise or set, so continue.");
		ChapGUI.printlnCond();

		if (riseSet) {
			R = ASTMath.INVCOS_D(Ar);
			ChapGUI.printlnCond("6.  Compute R = inv cos(Ar) = inv cos(" + String.format(ASTStyle.genFloatFormat,Ar) + ")");
			ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat,R) + " degrees");
			ChapGUI.printlnCond();

			S = 360.0 - R;
			ChapGUI.printlnCond("7.  Compute S = 360 - R = 360 - " + String.format(ASTStyle.genFloatFormat,R));
			ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat,S) + " degrees");
			ChapGUI.printlnCond();

			H1 = ASTMath.TAN_D(observer.getObsLat()) * ASTMath.TAN_D(declObj.getDecAngle());
			ChapGUI.printlnCond("8.  Compute H1 = tan(Lat)*tan(Decl) = tan(" + 
					ASTAngle.angleToStr(observer.getObsLat(), ASTMisc.DECFORMAT) +
					")*tan(" + ASTAngle.angleToStr(declObj, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("               = " + String.format(ASTStyle.genFloatFormat, H1));
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("9.  If ABS(H1) > 1, the location doesn't rise or set.");
			if (Math.abs(H1) > 1) {
				ChapGUI.printlnCond("    Location does not rise or set. No need to go further");
				riseSet = false;
			} else ChapGUI.printlnCond("    Location will rise and set, so continue.");
			ChapGUI.printlnCond();
		}

		if (riseSet) {
			H2 = ASTMath.INVCOS_D(-H1) / 15.0;
			ChapGUI.printlnCond("10. Calculate H2 = inv cos(-H1)/15 = inv cos[-(" + 
					String.format(ASTStyle.genFloatFormat,H1) + ")]/15");
			ChapGUI.printlnCond("                 = " + String.format(ASTStyle.genFloatFormat,H2) + " hours");
			ChapGUI.printlnCond();

			LSTr = 24 + raObj.getDecTime() - H2;
			ChapGUI.printlnCond("11. Let LSTr = 24 + RA - H2 = 24 + " + ASTTime.timeToStr(raObj, ASTMisc.DECFORMAT) + " - " +
					String.format(ASTStyle.genFloatFormat,H2));
			ChapGUI.printlnCond("             = " + ASTTime.timeToStr(LSTr, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond("    This is the LST for when the location rises above the observer's horizon.");
			ChapGUI.printlnCond();

			if (LSTr > 24) LSTr = LSTr - 24.0;
			ChapGUI.printlnCond("12. If LSTr > 24, subtract 24. LSTr = " + ASTTime.timeToStr(LSTr, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond();

			LSTs = raObj.getDecTime() + H2;
			ChapGUI.printlnCond("13. Let LSTs = RA + H2 = " + ASTTime.timeToStr(raObj, ASTMisc.DECFORMAT) + " + " +
					String.format(ASTStyle.genFloatFormat,H2));
			ChapGUI.printlnCond("             = " + ASTTime.timeToStr(LSTs, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond("    This is the LST for when the location sets below the observer's horizon.");
			ChapGUI.printlnCond();

			if (LSTs > 24) LSTs = LSTs - 24.0;
			ChapGUI.printlnCond("14. If LSTs > 24, subtract 24. LSTs = " + 
					ASTTime.timeToStr(LSTs, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("15. Convert LSTr and LSTs to LCTr and LCTs. These are the LCT rise and set times.");
			tmp = ASTTime.LSTtoGST(LSTr, observer.getObsLon());
			ChapGUI.printCond("    GSTr = " + ASTTime.timeToStr(tmp, ASTMisc.DECFORMAT) + ", ");
			tmp = ASTTime.GSTtoUT(tmp, observer.getObsDate());
			ChapGUI.printCond("UTr = " + ASTTime.timeToStr(tmp, ASTMisc.DECFORMAT) + ", ");
			LCTr = ASTTime.UTtoLCT(tmp, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
			ChapGUI.printlnCond("LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.DECFORMAT) + " hours");

			tmp = ASTTime.LSTtoGST(LSTs, observer.getObsLon());
			ChapGUI.printCond("    GSTs = " + ASTTime.timeToStr(tmp, ASTMisc.DECFORMAT) + ", ");
			tmp = ASTTime.GSTtoUT(tmp, observer.getObsDate());
			ChapGUI.printCond("UTs = " + ASTTime.timeToStr(tmp, ASTMisc.DECFORMAT) + ", ");
			LCTs = ASTTime.UTtoLCT(tmp, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
			ChapGUI.printlnCond("LCTs = " + ASTTime.timeToStr(LCTs, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("16. Convert LCTr and LCTs to HMS format.");
			ChapGUI.printCond("    LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + 
					", LCTs = " + ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT));
			ChapGUI.printlnCond();
		}

		result = "Location " + result;
		if (riseSet) result = result + " Rises at " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + " LCT, Sets at " +
				ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT) + " LCT";
		else result = result + " does not rise or set";

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

}
