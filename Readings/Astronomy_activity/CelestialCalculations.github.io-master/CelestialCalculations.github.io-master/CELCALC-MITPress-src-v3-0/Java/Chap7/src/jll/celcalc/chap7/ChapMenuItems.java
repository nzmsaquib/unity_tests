package jll.celcalc.chap7;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
* This class extends JMenuItem to add some additional fields, which
* simplifies the code for adding items to the menu bar and determining
* what actions to take as various menu items are selected. 
* <p>
* Copyright (c) 2018
* 
* @author J. L. Lawrence
* @version 3.0, 2018
*/

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {
	
	/** Define values for the eCalcType field in a ChapMenuItem class.
	 * These indicate what menu actions to take. */
	protected enum CalculationType {
		
		// Lunar Info menu
		/** Calculate the Moon's location */ MOON_LOCATION,
		/** Calculate Moonrise/Moonset */ MOON_RISE_SET,
		/** Calculate lunar distance and angular diameter */ MOON_DIST_AND_ANG_DIAMETER,
		/** Calculate Moon's phases and age */ MOON_PHASES_AND_AGE,
		/** Calculate how much of the Moon is illuminated */ MOON_PERCENT_ILLUMINATED,
		/** Set a termination criteria for solving Kepler's equation */ TERM_CRITERIA,
		// Orbital Elements menu
		/** Load Moon's orbital elements from a disk file */ LOAD_ORBITAL_ELEMENTS,
		/** Display the Moon's orbital elements */ SHOW_ORBITAL_ELEMENTS	
	}
	
	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Solar Info and Orbital Elements menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;

		// Create Lunar Info menu items
		menu = ChapGUI.getLunarInfoMenu();
		createMenuItem(menu,"Moon's Position", CalculationType.MOON_LOCATION);
		menu.addSeparator();
		createMenuItem(menu,"Moonrise/Moonset", CalculationType.MOON_RISE_SET);
		menu.addSeparator();
		createMenuItem(menu,"Lunar Distance and Angular Diameter", CalculationType.MOON_DIST_AND_ANG_DIAMETER);
		createMenuItem(menu,"Phase of the Moon", CalculationType.MOON_PHASES_AND_AGE);
		createMenuItem(menu,"Percent of Moon Illuminated", CalculationType.MOON_PERCENT_ILLUMINATED);
		menu.addSeparator();
		createMenuItem(menu,"Set Termination Criteria", CalculationType.TERM_CRITERIA);

		// Create Orbital Elements menu
		menu = ChapGUI.getOrbitalElementsMenu();
		createMenuItem(menu,"Load Orbital Elements", CalculationType.LOAD_ORBITAL_ELEMENTS);
		createMenuItem(menu,"Display Moon's Orbital Elements", CalculationType.SHOW_ORBITAL_ELEMENTS);
	}
		
	/** Define a class for what a  menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of calculation to do
		private CalculationType eCalcType;   // what type of calculation to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of calculation is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {
			super(title);
			eCalcType = calcType;
		}
		
		/*=========================================================
		 * Define accessor methods for the menu item fields
		 *========================================================*/
		
		/**
		 * Gets the type of calculation to perform.
		 * 
		 * @return		the type of calculation to perform
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}
	
	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 -----------------------------------------------------------------------------*/

	/**
	 * Creates a menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of calculation to perform
	 * @return					ChapMnuItm created
	 */     
	private ChapMnuItm createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getChapMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());
		
		return tmp;
	}

}
