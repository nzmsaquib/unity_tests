package jll.celcalc.chap7;

import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;

/**
 * <b>Handles the Orbital Elements menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class OrbitalElementsActions {

	/**
	 * Load orbital elements from disk
	 */
	protected void loadOrbitalElements() {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		ASTPrt prt = ChapGUI.getPrtInstance();
		String fullFilename = "";

		ChapGUI.clearTextAreas();

		if (ChapGUI.checkOEDBLoaded(ASTMisc.HIDE_ERRORS)) {
			if (!ASTMsg.pleaseConfirm("Orbital Elements have already been loaded. Are you\n" +
					"sure you want to load a new set of orbital elements?", "Clear Orbital Elements Data")) {
				prt.println("The currently loaded orbital elements were not cleared ...");
				return;
			}
		}

		fullFilename = ASTOrbits.getOEDBFileToOpen();
		if ((fullFilename == null) || (fullFilename.length() <= 0)) {
			prt.println("The currently loaded orbital elements were not cleared ...");
			return;
		}

		if (orbElements.loadOEDB(fullFilename)) {
			prt.println("New orbital elements have been successfully loaded ...");
			ChapGUI.setEpoch(orbElements.getOEEpochDate());
		} else {
			prt.println("The currently loaded orbital elements were cleared, but");
			prt.println("new orbital elements could not be loaded from the selected file.");
			prt.println("Please try again ...");
		}

		prt.resetCursor();
	}

	/**
	 * Shows the Moon's orbital elements and other data from whatever
	 * orbital elements database is currently loaded.
	 */
	protected void showMoonsOrbitalElements() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		
		if (!ChapGUI.checkOEDBLoaded()) return;

		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();

		prt.clearTextArea();
		orbElements.displayObjOrbElements(orbElements.getOEDBMoonIndex());
		prt.resetCursor();
	}

}
