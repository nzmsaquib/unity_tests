package jll.celcalc.chap7;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Lunar Info menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class LunarInfoActions {

	//	Set a default termination criteria (in radians) for solving Kepler's equation
	private static double termCriteria = 0.000002;

	/**
	 * Calculate the Moon's distance and angular diameter.
	 */
	protected void calcDistAndAngDiameter() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		int idxMoon;
		double[] eclCoord;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double Ecc, dF, dDist, dAngDiameter;
		double Vmoon;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		// Get Moon index after validating that orbital elements are loaded
		idxMoon = orbElements.getOEDBMoonIndex();

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Moon's Distance and Angular Diameter for the Current Date", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;
		Ecc = orbElements.getOEObjEccentricity(idxMoon);

		// Technically, we should be using the UT for the observer rather than UT=0, but
		// the difference is so small that it isn't worth converting LCT to UT
		eclCoord = orbElements.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Vmoon = eclCoord[ASTOrbits.MOON_TRUEANOM];

		ChapGUI.printlnCond("1.  Compute the Moon's true anomaly at 0 hours UT for the given date.");
		ChapGUI.printlnCond("    Vmoon = " + ASTAngle.angleToStr(Vmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dF = (1 + Ecc * ASTMath.COS_D(Vmoon)) / (1 - Ecc * Ecc);
		ChapGUI.printlnCond("2.  Compute F = [1 + eccentricity*cos(Vmoon)] / [1-eccentricity^2]");
		ChapGUI.printlnCond("              = [1 + " + String.format(ASTStyle.genFloatFormat,Ecc) +
				"*cos(" + ASTAngle.angleToStr(Vmoon, ASTMisc.DECFORMAT) +
				")] / [1 - " + String.format(ASTStyle.genFloatFormat,Ecc) + "^2]");
		ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat,dF));
		ChapGUI.printlnCond();

		dDist = orbElements.getOEObjSemiMajAxisKM(idxMoon) / dF;
		dDist = ASTMath.Round(dDist, 2);
		ChapGUI.printlnCond("3.  Compute the distance to the Moon.");
		ChapGUI.printlnCond("    Dist = a0/F where a0 is the length of the Moon's semi-major axis in km");
		ChapGUI.printlnCond("    Dist = (" + ASTStr.insertCommas(orbElements.getOEObjSemiMajAxisKM(idxMoon)) + 
				")/" + String.format(ASTStyle.genFloatFormat,dF));
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(dDist) + " km");
		ChapGUI.printlnCond();

		dAngDiameter = orbElements.getOEObjAngDiamDeg(idxMoon) * dF;
		ChapGUI.printlnCond("4.  Compute the Moon's angular diameter.");
		ChapGUI.printlnCond("    Theta_Moon = Theta_0*F where Theta_0 is the Moon's angular diameter in degrees");
		ChapGUI.printlnCond("    when the Moon is distance a0 away.");
		ChapGUI.printlnCond("    Theta_Moon = " + ASTAngle.angleToStr(orbElements.getOEObjAngDiamDeg(idxMoon), ASTMisc.DECFORMAT) + "*" +
				String.format(ASTStyle.genFloatFormat,dF) + " = " + ASTAngle.angleToStr(dAngDiameter, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5.  Convert distance to miles and angular diameter to DMS format.");
		ChapGUI.printlnCond("    Dist = " + ASTStr.insertCommas(dDist) + " km = " + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist), 2)) + " miles");
		ChapGUI.printlnCond("    Theta_Moon = " + ASTAngle.angleToStr(dAngDiameter, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("On " + ASTDate.dateToStr(observer.getObsDate()) + ", the Moon is/was " + ASTStr.insertCommas(dDist) + " km away");
		ChapGUI.printlnCond("and its angular diameter is/was " + ASTAngle.angleToStr(dAngDiameter, ASTMisc.DMSFORMAT));

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Moon's Distance: " + ASTStr.insertCommas(dDist) + " km (" + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist), 2)) +
				" miles), Angular Diameter: " + ASTAngle.angleToStr(dAngDiameter, ASTMisc.DMSFORMAT));
	}

	/**
	 * Calculate percentage of the Moon's surface
	 * that is illuminated as seen from Earth.
	 */
	protected void calcMoonPercentIllum() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double[] eclCoordSun, eclCoordMoon;
		double Lsun;
		double Bmoon, Lmoon, Mmoon, d, dPA, dKpcnt;
		int iKpcnt;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate the Percentage Illumination for the Moon as seen from Earth", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		// How to find the Sun's true anomaly
		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;

		eclCoordSun = orbElements.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Lsun = eclCoordSun[ASTOrbits.SUN_ECLLON];
		ChapGUI.printlnCond("1.  Calculate the Sun's ecliptic longitude for the date in question.");
		ChapGUI.printlnCond("    Lsun = " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eclCoordMoon = orbElements.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Bmoon = eclCoordMoon[ASTOrbits.MOON_ECLLAT];
		Lmoon = eclCoordMoon[ASTOrbits.MOON_ECLLON];
		Mmoon = eclCoordMoon[ASTOrbits.MOON_MEANANOM];

		ChapGUI.printlnCond("2.  Calculate the Moon's mean anomaly and ecliptic coordinates for the date in question.");
		ChapGUI.printlnCond("       Mm = " + ASTAngle.angleToStr(Mmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Bmoon = " + ASTAngle.angleToStr(Bmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Lmoon = " + ASTAngle.angleToStr(Lmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		d = ASTMath.COS_D(Lmoon - Lsun) * ASTMath.COS_D(Bmoon);
		d = ASTMath.INVCOS_D(d);
		ChapGUI.printlnCond("3.  Calculate the Moon's age in degrees.");
		ChapGUI.printlnCond("    d = inv cos[cos(Lmoon - Lsun)*cos(Bmoon)]");
		ChapGUI.printlnCond("      = inv cos[cos(" + ASTAngle.angleToStr(Lmoon, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + ")*cos(" +
				ASTAngle.angleToStr(Bmoon, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("      = " + ASTAngle.angleToStr(d, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dPA = (1 - 0.0549 * ASTMath.SIN_D(Mmoon)) / (1 - 0.0167 * ASTMath.SIN_D(Mmoon));
		dPA = 180.0 - d - 0.1468 * dPA * ASTMath.SIN_D(d);
		ChapGUI.printlnCond("4.  Calculate the Moon's phase angle.");
		ChapGUI.printlnCond("    PA = 180 - d - 0.1468*[(1-0.0549*sin(Mm)) / (1-0.0167*sin(Mm))]*sin(d)");
		ChapGUI.printlnCond("       = 180-" + ASTAngle.angleToStr(d, ASTMisc.DECFORMAT) + 
				"-0.1468*[(1-0.0549*sin(" + ASTAngle.angleToStr(Mmoon, ASTMisc.DECFORMAT) +
				"))/(1-0.0167*sin(" + ASTAngle.angleToStr(Mmoon, ASTMisc.DECFORMAT) + 
				"))]*sin(" + ASTAngle.angleToStr(d, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(dPA, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dKpcnt = 50 * (1 + ASTMath.COS_D(dPA));
		ChapGUI.printlnCond("5.  Calculate the percent illumination.");
		ChapGUI.printlnCond("    K% = 100*[(1 - cos(PA))/2] = 0*[(1 - cos(" + ASTAngle.angleToStr(dPA, ASTMisc.DECFORMAT) + "))/2]");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dKpcnt));
		ChapGUI.printlnCond();
		iKpcnt = (int) Math.round(dKpcnt);

		ChapGUI.printlnCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(), 
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
				", the Moon is " + iKpcnt + "% illuminated as seen from Earth");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("On " + ASTDate.dateToStr(observer.getObsDate().getMonth(),
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
				", the Moon is " + iKpcnt + "% illuminated.");
	}

	/**
	 * Calculate the Moon's phase and age
	 */
	protected void calcMoonPhase() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double[] eclCoordSun, eclCoordMoon;
		double Lsun;
		double L_t, Age, dF;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate the Moon's Phase and Age", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		// How to find the Sun's true anomaly
		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;

		// Technically, we should be using the UT for the observer rather than UT=0, but
		// the difference is so small that it isn't worth converting LCT to UT
		eclCoordSun = orbElements.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Lsun = eclCoordSun[ASTOrbits.SUN_ECLLON];
		ChapGUI.printlnCond("1.  Calculate the Sun's ecliptic longitude for the date in question.");
		ChapGUI.printlnCond("    Lsun = " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eclCoordMoon = orbElements.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		L_t = eclCoordMoon[ASTOrbits.MOON_TRUELON];
		ChapGUI.printlnCond("2.  Calculate the Moon's true ecliptic longitude for the date in question.");
		ChapGUI.printlnCond("    L_t = " + ASTAngle.angleToStr(L_t, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Age = L_t - Lsun;
		ChapGUI.printlnCond("3.  Calculate the Moon's age in degrees.");
		ChapGUI.printlnCond("    Age = L_t - Lsun = " + ASTAngle.angleToStr(L_t, ASTMisc.DECFORMAT) + " - " + 
				ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " = " + ASTAngle.angleToStr(Age, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Age = ASTMath.xMOD(Age, 360);
		ChapGUI.printlnCond("4.  If necessary, adjust Age to be in the range [0, 360]");
		ChapGUI.printlnCond("    Age = " + ASTAngle.angleToStr(Age, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Note: The Moon's age in days is Age/12.1907 = " + 
				String.format(ASTStyle.genFloatFormat,Age / 12.1907) + " days");
		ChapGUI.printlnCond();

		dF = (1 - ASTMath.COS_D(Age)) / 2.0;
		dF = ASTMath.Round(dF, 2);
		Age = ASTMath.Round(Age, 2);
		ChapGUI.printlnCond("5.  Calculate the Moon's phase.");
		ChapGUI.printlnCond("    F = (1 - cos(Age))/2 = (1 - cos(" + ASTAngle.angleToStr(Age, ASTMisc.DECFORMAT) +
				"))/2 = " + String.format(ASTStyle.genFloatFormat,dF));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear()) + " for the current observer");
		ChapGUI.printlnCond("the Moon's age is " + String.format(ASTStyle.genFloatFormat,Age) + 
				" degrees (F = " + String.format(ASTStyle.genFloatFormat,dF) +
				"), which is closest to a " + ageToStr(Age) + " Moon.");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("On " + ASTDate.dateToStr(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear()) + ", Age = " + String.format(ASTStyle.genFloatFormat,Age) + 
				" deg, F = " + String.format(ASTStyle.genFloatFormat,dF) +
				" (closest to a " + ageToStr(Age) + " Moon)");	
	}

	/**
	 * Calculate the position of the Moon, using the currently
	 * loaded orbital elements and the current observer position.
	 */
	protected void calcMoonPosition() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		int idxMoon;
		double[] eclCoord; 				// entries defined by calcSunEclipticCoord
		String result;
		double LCT, UT, LST, GST, TT, JD, JDe;
		ASTInt dateAdjustObj = new ASTInt();
		int dateAdjust;
		double De, Msun, Lsun;
		double L_uncor, L_p, L_t, Omega, Omega_p, Mm, Ae, Ev, Ca, Vmoon, V;
		double x, y, dT, inclin, Lmoon, Bmoon;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		ASTCoord eqCoord, horizonCoord;
		ASTPrt prt = ChapGUI.getPrtInstance();

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		// Get Sun index after validating that orbital elements are loaded
		idxMoon = orbElements.getOEDBMoonIndex();

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Moon's Position for the Current Observer", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;
		inclin = orbElements.getOEObjInclination(idxMoon);

		// Do all the time-related calculations
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();

		// adjust the date, if needed, since the LCTtoUT conversion could have
		// changed the date
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST, observer.getObsLon());

		ChapGUI.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.");
		ChapGUI.printlnCond("    LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(observer.getObsDate()) + ")");
		ChapGUI.printCond("     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0) ChapGUI.printCond("previous day ");
		else if (dateAdjust > 0) ChapGUI.printCond("next day ");
		ChapGUI.printlnCond(ASTDate.dateToStr(adjustedDate) + ")");
		ChapGUI.printlnCond("    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		TT = UT + (63.8 / 3600.0);
		ChapGUI.printlnCond("2.  Compute TT = UT + 63.8s = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " + 63.8s = " +
				ASTTime.timeToStr(TT, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		JDe = orbElements.getOEEpochJD();
		ChapGUI.printlnCond("3.  Compute the Julian day number for the standard epoch.");
		ChapGUI.printlnCond("    Epoch: " + String.format(ASTStyle.epochFormat,orbElements.getOEEpochDate()));
		ChapGUI.printlnCond("    JDe = " + String.format(ASTStyle.JDFormat,JDe));
		ChapGUI.printlnCond();

		JD = ASTDate.dateToJD(adjustedDate.getMonth(), adjustedDate.getdDay() + (TT / 24.0), adjustedDate.getYear());
		ChapGUI.printlnCond("4.  Compute the Julian day number for the desired date, being sure to use the");
		ChapGUI.printlnCond("    Greenwich date and TT from steps 1 and 2, and including the fractional part of the day.");
		ChapGUI.printlnCond("    JD = " + String.format(ASTStyle.JDFormat,JD));
		ChapGUI.printlnCond();

		De = JD - JDe;
		ChapGUI.printlnCond("5.  Compute the total number of elapsed days, including fractional days,");
		ChapGUI.printlnCond("    since the standard epoch.");
		ChapGUI.printlnCond("    De = JD - JDe = " + String.format(ASTStyle.JDFormat,JD) + " - " + String.format(ASTStyle.JDFormat,JDe));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,De) + " days");
		ChapGUI.printlnCond();

		// Use UT to get the Sun's position, not TT
		eclCoord = orbElements.calcSunEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),	adjustedDate.getYear(), 
				UT, solveTrueAnomaly, termCriteria);
		Lsun = eclCoord[ASTOrbits.SUN_ECLLON];
		Msun = eclCoord[ASTOrbits.SUN_MEANANOM];
		ChapGUI.printlnCond("6.  Calculate the Sun's ecliptic longitude and mean anomaly for the UT date and UT time.");
		ChapGUI.printlnCond("    Lsun = " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Msun = " + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		L_uncor = 13.176339686 * De + orbElements.getOEObjLonAtEpoch(idxMoon);
		ChapGUI.printlnCond("7.  Calculate the Moon's uncorrected mean ecliptic longitude.");
		ChapGUI.printlnCond("    L_uncor = 13.176339686 * De + L_0 = 13.176339686 * " + String.format(ASTStyle.genFloatFormat,De) +
				" + " + ASTAngle.angleToStr(orbElements.getOEObjLonAtEpoch(idxMoon), ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("            = " + ASTAngle.angleToStr(L_uncor, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("8.  If necessary, use the MOD function to adjust L_uncor to the range [0, 360].");
		ChapGUI.printCond("    L_uncor = L_uncor MOD 360 = " + ASTAngle.angleToStr(L_uncor, ASTMisc.DECFORMAT) + " MOD 360 = ");
		L_uncor = ASTMath.xMOD(L_uncor, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(L_uncor, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Omega = orbElements.getOEObjLonAscNode(idxMoon) - 0.0529539 * De;
		ChapGUI.printlnCond("9.  Calculate the Moon's uncorrected mean longitude of the ascending node.");
		ChapGUI.printlnCond("    Omega = Omega_0 - 0.0529539 * De = " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAscNode(idxMoon), ASTMisc.DECFORMAT) + " - 0.0529539 * " +
				String.format(ASTStyle.genFloatFormat,De));
		ChapGUI.printlnCond("          = " + ASTAngle.angleToStr(Omega, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. If necessary, adjust Omega to be in the range [0, 360].");
		ChapGUI.printCond("    Omega = Omega MOD 360 = " + ASTAngle.angleToStr(Omega, ASTMisc.DECFORMAT) + " MOD 360 = ");
		Omega = ASTMath.xMOD(Omega, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(Omega, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Mm = L_uncor - 0.1114041 * De - orbElements.getOEObjLonAtPeri(idxMoon);
		ChapGUI.printlnCond("11. Compute the Moon's uncorrected mean anomaly.");
		ChapGUI.printlnCond("    Mm = L_uncor - 0.1114041*De - W_0 = " + ASTAngle.angleToStr(L_uncor, ASTMisc.DECFORMAT) + "-0.1114041*(" +
				String.format(ASTStyle.genFloatFormat,De) + ")-" + ASTAngle.angleToStr(orbElements.getOEObjLonAtPeri(idxMoon), ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Mm, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("12. Adjust Mm if necessary to be in the range [0, 360].");
		ChapGUI.printCond("    Mm = Mm MOD 360 = " + ASTAngle.angleToStr(Mm, ASTMisc.DECFORMAT) + " MOD 360 = ");
		Mm = ASTMath.xMOD(Mm, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(Mm, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Ae = 0.1858 * ASTMath.SIN_D(Msun);
		ChapGUI.printlnCond("13. Calculate the annual equation correction.");
		ChapGUI.printlnCond("    Ae = 0.1858 * sin(Msun) = 0.1858 * sin(" + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + ") = " +
				ASTAngle.angleToStr(Ae, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Ev = 1.2739 * ASTMath.SIN_D(2 * (L_uncor - Lsun) - Mm);
		ChapGUI.printlnCond("14. Calculate the evection correction.");
		ChapGUI.printlnCond("    Ev = 1.2739 * sin[2 * (L_uncor - Lsun) - Mm]");
		ChapGUI.printlnCond("       = 1.2739 * sin[2 * (" + ASTAngle.angleToStr(L_uncor, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + ") - " + ASTAngle.angleToStr(Mm, ASTMisc.DECFORMAT) + "]");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Ev, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Ca = Mm + Ev - Ae - 0.37 * ASTMath.SIN_D(Msun);
		ChapGUI.printlnCond("15. Calculate the mean anomaly correction.");
		ChapGUI.printlnCond("    Ca = Mm + Ev - Ae - 0.37 * sin(Msun)");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Mm, ASTMisc.DECFORMAT) + " + " + ASTAngle.angleToStr(Ev, ASTMisc.DECFORMAT) +
				" - " + ASTAngle.angleToStr(Ae, ASTMisc.DECFORMAT) + " - 0.37 * sin(" + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Ca, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Vmoon = 6.2886 * ASTMath.SIN_D(Ca) + 0.214 * ASTMath.SIN_D(2 * Ca);
		ChapGUI.printlnCond("16. Calculate the Moon's true anomaly.");
		ChapGUI.printlnCond("    Vmoon = 6.2886 * sin(Ca) + 0.214 * sin(2*Ca)");
		ChapGUI.printlnCond("          = 6.2886 * sin(" + ASTAngle.angleToStr(Ca, ASTMisc.DECFORMAT) + ") + 0.214 * sin(2*" + 
				ASTAngle.angleToStr(Ca, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("          = " + ASTAngle.angleToStr(Vmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		L_p = L_uncor + Ev + Vmoon - Ae;
		ChapGUI.printlnCond("17. Calculate the corrected ecliptic longitude.");
		ChapGUI.printlnCond("    L_p = L_uncor + Ev + Vmoon - Ae = " + ASTAngle.angleToStr(L_uncor, ASTMisc.DECFORMAT) + "+" +
				ASTAngle.angleToStr(Ev, ASTMisc.DECFORMAT) + "+(" + ASTAngle.angleToStr(Vmoon, ASTMisc.DECFORMAT) + ")-(" + 
				ASTAngle.angleToStr(Ae, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("        = " + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		V = 0.6583 * ASTMath.SIN_D(2 * (L_p - Lsun));
		ChapGUI.printlnCond("18. Compute the variation correction.");
		ChapGUI.printlnCond("    V = 0.6583 * sin[2 * (L_p - Lsun)] = 0.6583 * sin[2 * (" +
				ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " - " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("      = " + ASTAngle.angleToStr(V, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		L_t = L_p + V;
		ChapGUI.printlnCond("19. Calculate the Moon's true ecliptic longitude.");
		ChapGUI.printlnCond("    L_t = L_p + V = " + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " + " +
				ASTAngle.angleToStr(V, ASTMisc.DECFORMAT) + " = " + ASTAngle.angleToStr(L_t, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Omega_p = Omega - 0.16 * ASTMath.SIN_D(Msun);
		ChapGUI.printlnCond("20. Compute a corrected ecliptic longitude of the ascending node.");
		ChapGUI.printlnCond("    Omega_p = Omega - 0.16 * sin(Msun) = " + ASTAngle.angleToStr(Omega, ASTMisc.DECFORMAT) + " - 0.16 * sin(" +
				ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("            = " + ASTAngle.angleToStr(Omega_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		y = ASTMath.SIN_D(L_t - Omega_p) * ASTMath.COS_D(inclin);
		ChapGUI.printlnCond("21. Calculate y = sin(L_t - Omega_p) * cos(inclination)");
		ChapGUI.printlnCond("                = sin(" + ASTAngle.angleToStr(L_t, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(Omega_p, ASTMisc.DECFORMAT) + ") * cos( " + ASTAngle.angleToStr(inclin, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("                = " + String.format(ASTStyle.genFloatFormat,y));
		ChapGUI.printlnCond();

		x = ASTMath.COS_D(L_t - Omega_p);
		ChapGUI.printlnCond("22. Calculate x = cos(L_t - Omega_p) = cos(" + ASTAngle.angleToStr(L_t, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(Omega_p, ASTMisc.DECFORMAT) + ") = " + String.format(ASTStyle.genFloatFormat,x));
		ChapGUI.printlnCond();

		dT = ASTMath.INVTAN_D(y / x);
		ChapGUI.printlnCond("23. Compute T = inv tan(y/x) = inv tan(" + String.format(ASTStyle.genFloatFormat,y) + " / " +
				String.format(ASTStyle.genFloatFormat,x) + ") = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		x = ASTMath.quadAdjust(y, x);
		ChapGUI.printlnCond("24. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.");
		ChapGUI.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " + " + 
				ASTAngle.angleToStr(x, ASTMisc.DECFORMAT) + " = ");
		dT = dT + x;
		ChapGUI.printlnCond(ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Lmoon = Omega_p + dT;
		ChapGUI.printlnCond("25. Calculate the Moon's ecliptic longitude.");
		ChapGUI.printlnCond("    Lmoon = Omega_p + T = " + ASTAngle.angleToStr(Omega_p, ASTMisc.DECFORMAT) + " + " + 
				ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) +
				" = " + ASTAngle.angleToStr(Lmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		if (Lmoon > 360.0) Lmoon = Lmoon - 360.0;
		ChapGUI.printlnCond("26. If Lmoon > 360, then subtract 360.");
		ChapGUI.printlnCond("    Lmoon = " + ASTAngle.angleToStr(Lmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Bmoon = ASTMath.SIN_D(L_t - Omega_p) * ASTMath.SIN_D(inclin);
		Bmoon = ASTMath.INVSIN_D(Bmoon);
		ChapGUI.printlnCond("27. Calculate the Moon's ecliptic latitude.");
		ChapGUI.printlnCond("    Bmoon = inv sin[sin(L_t - Omega_p) * sin(inclination)]");
		ChapGUI.printlnCond("          = inv sin[sin(" + ASTAngle.angleToStr(L_t, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(Omega_p, ASTMisc.DECFORMAT) + ") * sin(" + ASTAngle.angleToStr(inclin, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("          = " + ASTAngle.angleToStr(Bmoon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eqCoord = ASTCoord.EclipticToEquatorial(Bmoon, Lmoon, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("28. Convert the Moon's ecliptic coordinates (Bmoon, Lmoon) to equatorial coordinates.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(eqCoord.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) + " hours, Decl = " +
				ASTAngle.angleToStr(eqCoord.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		horizonCoord = ASTCoord.RADecltoHorizon(eqCoord.getRAAngle().getDecTime(), eqCoord.getDeclAngle().getDecAngle(),
				observer.getObsLat(), LST);
		ChapGUI.printlnCond("29. Convert the Moon's equatorial coordinates to horizon coordinates.");
		ChapGUI.printlnCond("    Alt = " + ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) + ", Az = " +
				ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Alt, " +
				ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Az";
		ChapGUI.printlnCond("Thus, for this observer location and date/time, the Moon");
		ChapGUI.printlnCond("is at " + result + ".");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Moon's Location is " + result);
	}

	/**
	 * Calculate the times of Moonrise and Moonset.
	 */
	protected void calcMoonRiseSet() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		double[] eclCoord;				// values filled in by calcMoonEclipticCoord
		int dateAdjust;
		double LCTr, LCTs, GST, UT;
		double L_t1, Omega_p1, Ca1;
		double Bmoon1, Lmoon1, Bmoon2, Lmoon2;
		ASTCoord eqCoord1, eqCoord2;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double[] LSTTimes;
		double ST1r, ST1s, ST2r, ST2s, Tr, Ts;
		boolean riseSet = true;
		boolean crossedDate = false;
		String result;
		ASTInt dateAdjustObj = new ASTInt();

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate Moonrise and Moonset for the Current Observer", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		//  How to find the Sun's true anomaly
		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;

		eclCoord = orbElements.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Bmoon1 = eclCoord[ASTOrbits.MOON_ECLLAT];
		Lmoon1 = eclCoord[ASTOrbits.MOON_ECLLON];
		L_t1 = eclCoord[ASTOrbits.MOON_TRUELON];
		Omega_p1 = eclCoord[ASTOrbits.MOON_CORRLON];
		Ca1 = eclCoord[ASTOrbits.MOON_MEANANOM_CORR];
		ChapGUI.printlnCond("1.  Calculate the Moon's ecliptic location at midnight for the date in question,");
		ChapGUI.printlnCond("    as well as the true ecliptic longitude (L_t1), corrected ecliptic longitude of");
		ChapGUI.printlnCond("    the ascending node(Omega_p1), and mean anomaly correction (Ca1)");
		ChapGUI.printlnCond("    For UT=0 hours on the date " +
				ASTDate.dateToStr(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(), observer.getObsDate().getYear()));
		ChapGUI.printlnCond("    The Moon's coordinates are Bmoon1 = " + ASTAngle.angleToStr(Bmoon1, ASTMisc.DECFORMAT) +
				" degrees, Lmoon1 = " + ASTAngle.angleToStr(Lmoon1, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    The Moon's true ecliptic longitude L_t1 = " + ASTAngle.angleToStr(L_t1, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    The corrected longitude of the ascending node Omega_p1 = " + ASTAngle.angleToStr(Omega_p1, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    The mean anomaly correction Ca1 = " + ASTAngle.angleToStr(Ca1, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eqCoord1 = ASTCoord.EclipticToEquatorial(Bmoon1, Lmoon1,orbElements.getOEEpochDate());
		ChapGUI.printlnCond("2.  Convert the Moon's ecliptic coordinates to equatorial coordinates.");
		ChapGUI.printlnCond("    RA1 = " + ASTTime.timeToStr(eqCoord1.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) + " hours, Decl1 = " +
				ASTAngle.angleToStr(eqCoord1.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord1.getRAAngle().getDecTime(), eqCoord1.getDeclAngle().getDecAngle(),observer.getObsLat());
		riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0);
		ST1r = LSTTimes[ASTOrbits.RISE_TIME];
		ST1s = LSTTimes[ASTOrbits.SET_TIME];
		ChapGUI.printlnCond("3.  Using the equatorial coordinates from step 2, compute the");
		ChapGUI.printlnCond("    LST rising and setting times.");
		if (!riseSet) {
			ChapGUI.printlnCond("    The Moon does not rise or set for this observer.");
			return;
		}
		ChapGUI.printlnCond("    ST1r = " + ASTTime.timeToStr(ST1r, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    ST1s = " + ASTTime.timeToStr(ST1s, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		Bmoon2 = Bmoon1 + 0.05 * ASTMath.COS_D(L_t1 - Omega_p1) * 12.0;
		ChapGUI.printlnCond("4.  Use L_t1, Omega_p1, and Bmoon1 to compute the Moon's ecliptic latitude 12 hours later.");
		ChapGUI.printlnCond("    Bmoon2 = Bmoon1 + 0.05*cos(L_t1 - Omega_p1)*12");
		ChapGUI.printlnCond("           = " + ASTAngle.angleToStr(Bmoon1, ASTMisc.DECFORMAT) +
				" + 0.05*cos(" + ASTAngle.angleToStr(L_t1, ASTMisc.DECFORMAT) + " - " + ASTAngle.angleToStr(Omega_p1, ASTMisc.DECFORMAT) + ")*12");
		ChapGUI.printlnCond("           = " + ASTAngle.angleToStr(Bmoon2, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Lmoon2 = Lmoon1 + (0.55 + 0.06 * ASTMath.COS_D(Ca1)) * 12.0;
		ChapGUI.printlnCond("5.  Use Ca1 and Lmoon1 to compute the Moon's ecliptic longitude 12 hours later.");
		ChapGUI.printlnCond("    Lmoon2 = Lmoon1 + [0.55 + 0.06*cos(Ca1)]*12");
		ChapGUI.printlnCond("           = " + ASTAngle.angleToStr(Lmoon1, ASTMisc.DECFORMAT) +
				" + [0.55 + 0.06*cos(" + ASTAngle.angleToStr(Ca1, ASTMisc.DECFORMAT) + ")]*12");
		ChapGUI.printlnCond("           = " + ASTAngle.angleToStr(Lmoon2, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		if (Lmoon2 > 360.0) Lmoon2 = Lmoon2 - 360.0;
		ChapGUI.printlnCond("6.  If Lmoon2 > 360.0, subtract 360 degrees.");
		ChapGUI.printlnCond("    Lmoon2 = " + ASTAngle.angleToStr(Lmoon2, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eqCoord2 = ASTCoord.EclipticToEquatorial(Bmoon2, Lmoon2, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("7.  Convert the ecliptic coordinates from the previous step to equatorial coordinates.");
		ChapGUI.printlnCond("    RA2 = " + ASTTime.timeToStr(eqCoord2.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) + " hours, Decl2 = " +
				ASTAngle.angleToStr(eqCoord2.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord2.getRAAngle().getDecTime(), eqCoord2.getDeclAngle().getDecAngle(), observer.getObsLat());
		riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0);
		ST2r = LSTTimes[ASTOrbits.RISE_TIME];
		ST2s = LSTTimes[ASTOrbits.SET_TIME];

		ChapGUI.printlnCond("8.  Using the equatorial coordinates from step 7, compute the");
		ChapGUI.printlnCond("    second set of LST rising and setting times.");
		if (!riseSet) {
			ChapGUI.printlnCond("    The Moon does not rise or set for this observer.");
			return;
		}
		ChapGUI.printlnCond("    ST2r = " + ASTTime.timeToStr(ST2r, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    ST2s = " + ASTTime.timeToStr(ST2s, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		Tr = (12.03 * ST1r) / (12.03 + ST1r - ST2r);
		ChapGUI.printlnCond("9.  Interpolate the two sets of LST rising times.");
		ChapGUI.printlnCond("    Tr = (12.03 * ST1r) / (12.03 + ST1r - ST2r)");
		ChapGUI.printlnCond("       = (12.03*" + ASTTime.timeToStr(ST1r, ASTMisc.DECFORMAT) + ")/(12.03+" + ASTTime.timeToStr(ST1r, ASTMisc.DECFORMAT) +
				"-" + ASTTime.timeToStr(ST2r, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + ASTTime.timeToStr(Tr, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		Ts = (12.03 * ST1s) / (12.03 + ST1s - ST2s);
		ChapGUI.printlnCond("10. Interpolate the two sets of LST setting times.");
		ChapGUI.printlnCond("    Ts = (12.03 * ST1s) / (12.03 + ST1s - ST2s)");
		ChapGUI.printlnCond("       = (12.03*" + ASTTime.timeToStr(ST1s, ASTMisc.DECFORMAT) + ")/(12.03+" + ASTTime.timeToStr(ST1s, ASTMisc.DECFORMAT) +
				"-" + ASTTime.timeToStr(ST2s, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + ASTTime.timeToStr(Ts, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		GST = ASTTime.LSTtoGST(Tr, observer.getObsLon());
		UT = ASTTime.GSTtoUT(GST, observer.getObsDate());
		LCTr = ASTTime.UTtoLCT(UT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		crossedDate = (dateAdjust != 0);

		GST = ASTTime.LSTtoGST(Ts, observer.getObsLon());
		UT = ASTTime.GSTtoUT(GST, observer.getObsDate());
		LCTs = ASTTime.UTtoLCT(UT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		crossedDate = crossedDate || (dateAdjust != 0);

		ChapGUI.printlnCond("11. Convert the LST rising/setting times to their corresponding LCT times.");
		ChapGUI.printlnCond("    LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.DECFORMAT) + " hours, LCTs = " +
				ASTTime.timeToStr(LCTs, ASTMisc.DECFORMAT) + " hours");
		if (crossedDate) {
			ChapGUI.printlnCond("    WARNING: Converting LST to LCT crossed a date boundary, so the results are likely");
			ChapGUI.printlnCond("             inaccurate. Try the date before and the date after the given date,");
			ChapGUI.printlnCond("             then average the results to get a more accurate estimate.");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("12. Convert the LCT times to HMS format.");
		ChapGUI.printlnCond("    LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + ", LCTs = " + ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear()) + " for the current observer");
		ChapGUI.printlnCond("the Moon will rise at " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + " LCT and set at " +
				ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT) + " LCT");


		prt.setProportionalFont();
		prt.resetCursor();

		result = "Moonrise: " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + " LCT, Moonset: " + ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT) + " LCT";
		if (crossedDate) result = result + " (Moonset on day before Moonrise-Try new dates)";
		else if (LCTr > LCTs) result = result + " (next day)";
		ChapGUI.setResults(result);
	}

	/**
	 * Set the termination criteria for solving Kepler's equation
	 */
	protected void setTerminationCriteria() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double dTerm = termCriteria;

		ChapGUI.clearTextAreas();;
		prt.setBoldFont(true);
		prt.println("Set Termination Criteria for use in solving Kepler's Equation", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		if (ASTQuery.showQueryForm("Enter Termination Criteria in radians\n(ex: 0.000002)") != ASTQuery.QUERY_OK) {
			prt.println("Termination criteria was not changed from " + termCriteria + " radians");
			return;
		}

		// Validate the termination criteria
		ASTReal rTmpObj = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (rTmpObj.isValidRealObj()) dTerm = rTmpObj.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria");
			prt.println("Termination criteria was not changed from " + termCriteria + " radians");
			return;
		}

		prt.println("Prior termination criteria was: " + termCriteria + " radians");
		if (dTerm < ASTKepler.KeplerMinCriteria) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too small, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		if (dTerm > 1) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too large, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		termCriteria = dTerm;

		prt.println("Termination criteria is now set to " + termCriteria + " radians");
		prt.resetCursor();
	}

	/*-----------------------------------------------------------------------------------
	 * Some common helper routines used only in this class
	 *----------------------------------------------------------------------------------)*/

	/**
	 * Convert the Moon's age into a printable string
	 * 
	 * @param Age			Moon's age in degrees
	 * @return				New Moon, First Quarter, etc. as a string
	 */
	private static String ageToStr(double Age) {
		if (Age < 45 - 0.5 * 45) return "New";
		else if (Age < 90 - 0.5 * (90 - 45)) return "Waxing Crescent";
		else if (Age < 135 - 0.5 * (135 - 90)) return "First Quarter";
		else if (Age < 180 - 0.5 * (180 - 135)) return "Waxing Gibbous";
		else if (Age < 225 - 0.5 * (225 - 180)) return "Full";
		else if (Age < 270 - 0.5 * (270 - 225)) return "Waning Gibbous";
		else if (Age < 315 - 0.5 * (315 - 270)) return "Last Quarter";
		else if (Age < 360 - 0.5 * (360 - 315)) return "Waning Crescent";
		else return "New";
	}
}
