package jll.celcalc.chap7;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap7.ChapMenuItems.ChapMnuItm;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */
class MenusListener implements ActionListener {
	
	private LunarInfoActions li_actions = new LunarInfoActions();
	private OrbitalElementsActions oe_actions = new OrbitalElementsActions();
	
	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}

		// Handle Lunar Info and Orbital Elements menus
		else if (action.equals(ChapGUI.getChapMenuCommand())) {
			ChapMnuItm objMenuItem = (ChapMnuItm) e.getSource();		// the menu item the user clicked on

			switch (objMenuItem.getCalcType()) {

			// ************* Lunar Info menu
			case MOON_LOCATION:
				li_actions.calcMoonPosition();
				break;
			case MOON_RISE_SET:
				li_actions.calcMoonRiseSet();
				break;
			case MOON_DIST_AND_ANG_DIAMETER:
				li_actions.calcDistAndAngDiameter();
				break;
			case MOON_PHASES_AND_AGE:
                li_actions.calcMoonPhase();
				break;
			case MOON_PERCENT_ILLUMINATED:
                li_actions.calcMoonPercentIllum();
				break;
			case TERM_CRITERIA:
				li_actions.setTerminationCriteria();
				break;
				// ************* Orbital Elements menu
			case LOAD_ORBITAL_ELEMENTS:
				oe_actions.loadOrbitalElements();
				break;
			case SHOW_ORBITAL_ELEMENTS:
				oe_actions.showMoonsOrbitalElements();
				break;
			default:
				ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
						"from the 'Lunar Info' or 'Orbital Elements' menu and try again.", "No Menu Item Selected");
				break;
			}
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			ChapGUI.clearTextAreas();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
					"be used in subsequent calculations. You may find it convenient to enter an initial latitude, longitude, " +
					"and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
					"longitude, and time zone are already filled in with default values when the program starts. When this program " +
					"begins, the date and time will default to the local date and time at which the program is started.");
			prt.println();
			prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
					"various calculations are performed. Also, select the appropriate radio button to choose what method to " +
					"use when it is necessary to determine the Sun's true anomaly. The radio button 'EQ of Center' will solve " +
					"the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " +
					"'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " +
					"Newton/Raphson method. The menu entry 'Lunar Info->Set Termination Critera' allows you to enter the " +
					"termination criteria (in radians) for when to stop iterating to solve Kepler's equation. This radio button " +
					"only affects how the Sun's true anomaly is computed. Finding the Moon's true anomaly is accomplished by " +
					"solving the equation of the center regardless of which of these radio buttons is set.");
			prt.println();
			prt.println("The menu 'Lunar Info' performs various calculations related to the Moon, such as determining its " +
					"location for the current information in the 'Observer Location and Time' data area. The menu " +
					"'Orbital Elements' allow you to load data files that contain the Sun and Moon's orbital elements referenced to " +
					"a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " +
					"starts up. The menu entry 'Orbital Elements->Load Orbital Elements' allows you to load and use orbital elements " +
					"referenced to some other epoch.");

			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
