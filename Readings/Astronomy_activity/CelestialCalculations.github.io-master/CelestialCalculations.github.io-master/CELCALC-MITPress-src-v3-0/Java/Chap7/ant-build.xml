<?xml version="1.0"?>

<!-- This ant file builds the application, packages as an executable jar file,  and creates the javadocs. 

	 IMPORTANT NOTE: The javadocs executable must be on the system path because Eclipse is not smart
	 enough to find it just by knowing where the Java JDK is installed. -->

<project name="BuildApp" default="main" basedir="." xmlns:if="ant:if" xmlns:unless="ant:unless">
	<!-- Set up target names required for this build. It would be nice if ant provided an easy
		 way to convert a package name to a directory, but doing that requires external jars or
		 reading/writing to files. So, srcfordocs.dir is 'hardcoded' to match package.name. The
		 property srcfordocs.dir is used in creating javadocs so that only package.name is included.
		 Otherwise, extraneous documentation would be generated for other included packages, such
		 as org.eclipse.wb.swing. -->
	<property name="chapter" value="7"/>
	<property name="class.name" value="RunChap${chapter}"/>
	<property name="package.name" value="jll.celcalc.chap${chapter}"/>
	<property name="srcfordocs.dir" value="src/jll/celcalc/chap${chapter}"/>
	<property name="lib.name" value="ASTUtils"/>
	
	<!-- Information for the manifest -->
	<property name="app.publisher" value="MIT Press" />
	<property name="app.copyright" value="(c) 2018, All rights reserved"/>
	<property name="book" value="Celestial Calculations" />
	<property name="app.author" value="J. L. Lawrence, PhD" />
	<property name="app.title" value="${class.name}" />
	<property name="app.version" value="3.0-Java" />
	
	<!-- set up default directories -->
	<property name="src.dir" location="src" />
	<property name="build.dir" location="bin" />
	<property name="destination.dir" location="../jars" />
	<property name="docs.dir" location="doc" />
	<property name="target.jar" value="${destination.dir}/${class.name}.jar"/>
	
	<!-- See if there is a resources directory -->
	<available file="${src.dir}/resources" type="dir" property="resources.exist"/>
	
	<!-- Get current timestamp -->
	<tstamp>
		<format property="TODAY_US" pattern="MMMM dd, yyyy HH:mm:ss" locale="en,US"/>
	</tstamp>
	
	<!-- Delete all artifacts from any previous build -->
	<target name="clean">
		<delete dir="${build.dir}" />
		<delete dir="${docs.dir}" />
		<delete file="${target.jar}" />
	</target>

	<!-- Make sure all directories we need for the build exist -->
	<target name="makedirs">
		<mkdir dir="${build.dir}" />
		<mkdir dir="${docs.dir}" />
		<mkdir dir="${destination.dir}" />
	</target>

	<!-- Compile the source code -->
	<target name="compile" depends="clean, makedirs">
		<!-- must remember to copy the resources, if they exist -->
		<copy todir="${build.dir}/resources" if:true="${resources.exist}">
			<fileset dir="${src.dir}/resources"/>
		</copy>
		
		<javac includeantruntime="false" srcdir="${src.dir}" destdir="${build.dir}">
			<classpath>
				<pathelement location="${destination.dir}/${lib.name}.jar" />
			</classpath>
		</javac>
	</target>
	
	<!-- Package output as a jar file -->
	<target name="makejar">
		<jar filesetmanifest="mergewithoutmain" destfile="${target.jar}" basedir="${build.dir}">
			<manifest>
				<attribute name="Build-Date-and-Time" value="${TODAY_US}"/>
				<attribute name="Ant-Version" value="${ant.version}"/>
				<attribute name="Java-Version" value="${java.version}"/>
				<attribute name="Main-Class" value="${package.name}.${class.name}"/>
				<attribute name="Resource-Class-Path" value="./ ${lib.name}.jar" />					
				<attribute name="Publisher" value="${app.publisher}" />
				<attribute name="Copyright" value="${app.copyright}"/>
				<attribute name="Publication" value="${book}" />
				<attribute name="Author" value="${app.author}" />
				<attribute name="Package" value="${package.name}"/>
				<attribute name="Application" value="${app.title}" />
				<attribute name="Application-Version" value="${app.version}" />
			</manifest>
			<zipfileset src="${destination.dir}/${lib.name}.jar" excludes="META-INF/*.SF"/>
		</jar>
	</target>
	
	<!-- Create Javadocs -->
	<target name="makedocs" depends="compile">
		<javadoc packagenames="src" sourcepath="${src.dir}" destdir="${docs.dir}" access="package">
			<!-- Define which files should get included -->
			<fileset dir="${srcfordocs.dir}">
				<include name="**/*.java" />
			</fileset>
			<classpath>
				<pathelement location="${destination.dir}/${lib.name}.jar" />
			</classpath>
		</javadoc>
	</target>

	<target name="main" depends="compile, makedocs, makejar">
		<description>Main target</description>
	</target>

</project>
