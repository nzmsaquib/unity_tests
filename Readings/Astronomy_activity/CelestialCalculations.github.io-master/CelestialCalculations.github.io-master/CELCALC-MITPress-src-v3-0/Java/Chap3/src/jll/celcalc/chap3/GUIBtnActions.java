package jll.celcalc.chap3;

import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Calculate button</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class GUIBtnActions {

	/*=================================================================
	 * Routines to do the actual work for the Time Conversions menu
	 *================================================================*/

	/**
	 * Convert a calendar date to a Julian day number
	 */
	protected void calcCalendar2JD() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String result;
		int iYear, iMonth, iY, iM;
		double dDay, dT, dTemp, dA, dB, JD;
		boolean bGregorian;

		prt.clearTextArea();

		// The "true" flag in isValidDate indicates that we will allow fractional days in the date
		// as well as 0 (so that we can find the JD for 1/0/2000)
		ASTDate dateObj = ASTDate.isValidDate(strInput,ASTMisc.SHOW_ERRORS,true);
		if (!dateObj.isValidDateObj()) return;
		iYear = dateObj.getYear();
		iMonth = dateObj.getMonth();
		dDay = dateObj.getdDay();

		result = "Convert " + strInput;
		if (dateObj.getYear() < 0) result = result + " BC";
		else result = result + " AD";

		prt.setBoldFont(true);
		ChapGUI.printlnCond(result + " to a Julian day number", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1. If the month is greater than 2, set y = year and m = month");
		ChapGUI.printlnCond("   else set y = year - 1 and m = month + 12");
		ChapGUI.printCond("   Since month = " + iMonth + ", ");
		if (iMonth > 2) {
			iY = iYear;
			iM = iMonth;
			ChapGUI.printlnCond("which is greater than 2, set y = " + iYear);
			ChapGUI.printlnCond("   and m = " + iM);
		} else {
			iY = iYear - 1;
			iM = iMonth + 12;
			ChapGUI.printlnCond("which is not greater than 2, set y = year - 1 = " + iY);
			ChapGUI.printlnCond("   and m = month + 12 = " + iM);
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2. If the year is less than 0, set T = 0.75 else set T = 0");
		ChapGUI.printCond("   Since year = " + iYear + " is ");
		if (iYear < 0) {
			dT=0.75;
			ChapGUI.printlnCond("less than 0, set T = 0.75");
		} else {
			dT=0.0;
			ChapGUI.printlnCond("not less than 0, set T = 0");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3. Determine if the date is in the Gregorian Calendar");
		ChapGUI.printlnCond("   The date is Gregorian if it is later than Oct. 15, 1582.");
		ChapGUI.printlnCond("   (Compare YYYY.MMDDdd against 1582.1015. If YYYY.MMDDdd is");
		ChapGUI.printlnCond("   greater than or equal to 1582.1015, the date is Gregorian.)");

		dTemp = (double) iYear + (((double) iMonth)/100.0) + (dDay/10000.0);
		bGregorian = (dTemp >= 1582.1015);
		ChapGUI.printCond("   Since YYYY.MMDDdd = " + String.format("%04.06f",dTemp) + ", the date is ");
		if (!bGregorian) ChapGUI.printCond("not ");
		ChapGUI.printlnCond("Gregorian.");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4. If the date is Gregorian then compute");
		ChapGUI.printlnCond("   A = FIX(y / 100) and B = 2 - A + FIX(A / 4).");
		ChapGUI.printlnCond("   If the date is not Gregorian, set A = 0 and B = 0");
		ChapGUI.printCond("   Since the date is ");
		if (bGregorian) {
			dA = ASTMath.Trunc(((double) iY)/100.0);
			dB = 2.0 - dA + ASTMath.Trunc(dA/4.0);
			ChapGUI.printlnCond("Gregorian, A = FIX(" + iY + " / 100) = " + dA);
			ChapGUI.printlnCond("   and B = 2 - " + dA + " + FIX(" + dA + " / 4) = " + dB);
		}else {
			dA = 0.0;
			dB = 0.0;
			ChapGUI.printlnCond("not Gregorian, A = 0 and B = 0");
		}
		ChapGUI.printlnCond();

		JD = dB + ASTMath.Trunc(365.25*iY - dT) + ASTMath.Trunc(30.6001*(iM + 1)) + dDay + 1720994.5;
		ChapGUI.printlnCond("5. Finally, JD = B + FIX(365.25*y - T) + FIX[30.6001*(m+1)] + day + 1720994.5");
		ChapGUI.printlnCond("               = " + dB + " + FIX(365.25*" + iY + "-" + dT +
				") + FIX[30.6001*(" + iM + "+1)]" + " + " + dDay + " + 1720994.5");
		ChapGUI.printlnCond("               = " + String.format(ASTStyle.JDFormat,JD));
		ChapGUI.printlnCond();	  

		result = strInput + " = " + String.format(ASTStyle.JDFormat,JD) + " JD";
		ChapGUI.printCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate the day of the week
	 */
	protected void calcDayOfWeek() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		int N;
		double JD, dA, dB;
		String result;

		prt.clearTextArea();

		ASTDate dateObj = ASTDate.isValidDate(strInput);
		if (!dateObj.isValidDateObj()) return;

		result = ASTDate.dateToStr(dateObj);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Determine what Day of the Week " + result + " falls on", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		JD = ASTDate.dateToJD(dateObj);
		ChapGUI.printlnCond("1. Convert " + result + " to a Julian day number at 00:00:00 UT");
		ChapGUI.printlnCond("   " + result + " = " + String.format(ASTStyle.JDFormat,JD) + " Julian day number at 00:00:00 UT");
		ChapGUI.printlnCond();

		dA = (JD + 1.5) / 7.0;
		ChapGUI.printlnCond("2. Calculate A = (JD + 1.5) / 7 = (" + String.format(ASTStyle.JDFormat,JD) +
				" + 1.5) / 7 = " + String.format(ASTStyle.JDFormat,dA));
		ChapGUI.printlnCond();

		dB = 7.0 * ASTMath.Frac(dA);
		ChapGUI.printlnCond("3. Let B = 7 * FRAC(A) = 7 * FRAC(" + String.format(ASTStyle.JDFormat,dA) +
				") = " + String.format("%.6f",dB));
		ChapGUI.printlnCond();

		N = (int) (Math.round(dB));
		ChapGUI.printlnCond("4. Finally, let N = ROUND(B) to get the day number for the day");
		ChapGUI.printlnCond("   of the week (0=Sunday, 1=Monday, etc.)");
		ChapGUI.printlnCond("   N = ROUND(B) = ROUND(" + String.format("%.6f",dB) + ") = " + N);
		ChapGUI.printlnCond();

		if ((N < 0) || (N > 6)) {
			ASTMsg.criticalErrMsg("Internal error - N is out of range");
			return;
		}

		result = result + " falls on " + ASTDate.DaysOfWeek[N];
		ChapGUI.printCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate how many days into a year a specific date is
	 */
	protected void calcDaysIntoYear() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		int T, N;
		String result;
		int iYear, iMonth, iDay;

		prt.clearTextArea();

		ASTDate dateObj = ASTDate.isValidDate(strInput);
		if (!dateObj.isValidDateObj()) return;
		iMonth = dateObj.getMonth();
		iDay = dateObj.getiDay();
		iYear = dateObj.getYear();
		result = ASTDate.dateToStr(dateObj);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Determine the number of days into the year for " + result, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1. If this is a leap year, set T = 1 else set T = 2");
		ChapGUI.printCond("   " + result + " is ");
		if (ASTDate.isLeapYear(iYear)) T = 1;
		else {
			T = 2;
			ChapGUI.printCond("not ");
		}
		ChapGUI.printlnCond("a leap year, so T = " + T);
		ChapGUI.printlnCond();

		N = ASTMath.Trunc(275.0 * iMonth / 9.0) - T * ASTMath.Trunc((iMonth + 9) / 12.0) + iDay - 30;
		ChapGUI.printlnCond("2. The number of days into the year is given by the formula");
		ChapGUI.printlnCond("   N = FIX(275 * MONTH / 9) - T * FIX[(MONTH + 9) / 12] + DAY - 30");
		ChapGUI.printlnCond("     = FIX(275 * " + iMonth + " / 9) - " + T + " * FIX[(" + iMonth +
				" + 9) / 12] + " + iDay + " - 30 = " + N);
		ChapGUI.printlnCond();

		result = result + " is " + N + " days into the year";
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate date when given the number of days into a year
	 */
	protected void calcDaysIntoYear2Date() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strDOY = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strYear = ASTStr.removeWhitespace(ChapGUI.getData2());
		int iMonth, iDay, iYear, N, iA, iB, iC, iE;
		String result;

		prt.clearTextArea();
		ASTInt intObj = ASTInt.isValidInt(strDOY,ASTMisc.HIDE_ERRORS);
		if (!intObj.isValidIntObj()) {
			ASTMsg.errMsg("The number of days into the year is invalid - try again", "Days into Year");
			return;
		}
		N = intObj.getIntValue();
		if ((N < 0) || (N > 365)) {
			ASTMsg.errMsg("The number of days into the year is out of range - try again", "Days into Year");
			return;
		}
		intObj = ASTInt.isValidInt(strYear,ASTMisc.HIDE_ERRORS);
		if (!intObj.isValidIntObj()) {
		    ASTMsg.errMsg("The Year entered is invalid - try again", "Invalid Year");
			return;
		}
		iYear = intObj.getIntValue();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Determine the date that is " + N + " days into the year " + iYear,ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1. If Year is a leap year, set A = 1523, otherwise set A = 1889");
		ChapGUI.printCond("   Since " + iYear + " is ");
		if (ASTDate.isLeapYear(iYear)) iA = 1523;
		else {
			iA = 1889;
			ChapGUI.printCond("not ");
		}
		ChapGUI.printlnCond("a leap year, A = " + iA);
		ChapGUI.printlnCond();

		iB = ASTMath.Trunc((N + iA - 122.1) / 365.25);
		ChapGUI.printlnCond("2. B = FIX[(N + A - 122.1) / 365.25] = FIX[(" + N + " + " + iA + " - 122.1) / 365.25]");
		ChapGUI.printlnCond("     = " + iB);
		ChapGUI.printlnCond();

		iC = N + iA - ASTMath.Trunc(365.25 * iB);
		ChapGUI.printlnCond("3. C = N + A - FIX(365.25 * B) = " + N + " + " + iA + " - FIX(365.25 * " + iB + ")");
		ChapGUI.printlnCond("     = " + iC);
		ChapGUI.printlnCond();

		iE = ASTMath.Trunc(iC / 30.6001);
		ChapGUI.printlnCond("4. E = FIX(C / 30.6001) = FIX(" + iC + " / 30.6001) = " + iE);
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5. If E < 13.5, then Month = E - 1, otherwise, Month = E - 13");
		if (iE < 13.5) iMonth = iE - 1;
		else iMonth = iE - 13;
		ChapGUI.printlnCond("   Thus, Month = " + iMonth);
		ChapGUI.printlnCond();

		iDay = iC - ASTMath.Trunc(30.6001 * iE);
		ChapGUI.printlnCond("6. Finally, Day = C - FIX(30.6001 * E) = " + iC + " - FIX(30.6001 * " + iE + ") = " + iDay);
		ChapGUI.printlnCond();

		result = N + " days into the year " + iYear + " is the date " + ASTDate.dateToStr(iMonth, iDay, iYear);
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * 	Convert a Julian day number to a calendar date
	 */
	protected void calcJD2Calendar() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		double JD, JD1, fJD1;
		long iJD1, iA, iB, iC, iD, iE, iG;
		int iMonth, iYear;
		double dDay;
		String result;

		prt.clearTextArea();

		ASTReal dTempObj = ASTReal.isValidReal(strInput,ASTMisc.HIDE_ERRORS);
		if (!dTempObj.isValidRealObj()) {
			ASTMsg.errMsg("Julian day number entered is invalid. Try again.", "Invalid JD");
			return;
		}

		JD = dTempObj.getRealValue();
		result = String.format(ASTStyle.JDFormat,JD);
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " JD to a Calendar Date", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Add 0.5 to the Julian day number");
		JD1 = JD + 0.5;
		ChapGUI.printlnCond("    JD1 = JD + 0.5 = " + JD + " + 0.5 = " + JD1);
		ChapGUI.printlnCond();

		iJD1 = ASTMath.Trunc(JD1);
		fJD1 = ASTMath.Frac(JD1);
		ChapGUI.printlnCond("2.  Set I = FIX(JD1) and F = FRAC(JD1)");
		ChapGUI.printlnCond("    Thus, I = FIX(" + JD1 + ") = " + iJD1);
		ChapGUI.printlnCond("    and F = FRAC(" + JD1 + ") = " + String.format("%.5f",fJD1));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3.  If I is larger than 2299160 set A = FIX[(I - 1867216.25) / 36524.25]");
		ChapGUI.printlnCond("    and set B = I + 1 + A - FIX(A / 4). Otherwise, set B = I.");
		ChapGUI.printlnCond();
		ChapGUI.printCond("    Since I is ");
		if (iJD1 > 2299160) {
			iA = ASTMath.Trunc((iJD1 - 1867216.25) / 36524.25);
			iB = iJD1 + 1 + iA - ASTMath.Trunc(iA / 4.0);
			ChapGUI.printlnCond(" greater than 2299160,");
			ChapGUI.printlnCond("    A = FIX[(" + iJD1 + " - 1867216.25) / 36524.25] = " + iA);
			ChapGUI.printlnCond("    and B = " + iJD1 + " + 1 + " + iA + " - FIX(" + iA + " / 4) = " + iB);
		} else {
			iB = iJD1;
			ChapGUI.printlnCond(" not greater than 2299160, B = " + iB);
		}
		ChapGUI.printlnCond();

		iC = iB + 1524;
		ChapGUI.printlnCond("4.  Set C = B + 1524 = " + iB + " + 1524 = " + iC);
		ChapGUI.printlnCond();

		iD = ASTMath.Trunc((iC - 122.1) / 365.25);
		ChapGUI.printlnCond("5.  Set D = FIX[(C - 122.1) / 365.25] = FIX[(" + iC + " - 122.1) / 365.25]");
		ChapGUI.printlnCond("          = " + iD);
		ChapGUI.printlnCond();

		iE = ASTMath.Trunc(365.25 * iD);
		ChapGUI.printlnCond("6.  Set E = FIX(365.25 * D) = FIX(365.25 * " + iD + ") = " + iE);
		ChapGUI.printlnCond();

		iG = ASTMath.Trunc((iC - iE) / 30.6001);
		ChapGUI.printlnCond("7.  Set G = FIX[(C - E) / 30.6001] = FIX[(" + iC + " - " + iE + ") / 30.6001]");
		ChapGUI.printlnCond("          = " + iG);
		ChapGUI.printlnCond();

		dDay = iC - iE + fJD1 - ASTMath.Trunc(30.6001 * iG);
		ChapGUI.printlnCond("8.  The day is given by Day = C - E + F - FIX(30.6001 * G)");
		ChapGUI.printlnCond("                            = " + iC + " - " + iE + " + " + String.format("%5f",fJD1) +
				" - FIX(30.6001 * " + iG + ")");
		ChapGUI.printlnCond("                            = " + dDay);
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  The month is given by month = G - 1 if G is less than 13.5");
		ChapGUI.printlnCond("    and month = G - 13 if G is greater than 13.5");
		ChapGUI.printCond("    Thus, since G is ");
		if (iG > 13.5) {
			iMonth = (int) (iG - 13);
			ChapGUI.printlnCond("greater than 13.5, month = G - 13 = " + iG + " - 13 = " + iMonth); 
		} else {
			iMonth = (int) (iG - 1);
			ChapGUI.printlnCond("less than 13.5, month = G - 1 = " + iG + " - 1 = " + iMonth);  
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. Finally, the year is given by year = D - 4716 if month is more than 2.5");
		ChapGUI.printlnCond("    and year = D - 4715 if month is less than 2.5");
		ChapGUI.printCond("    Thus, since month is ");
		if (iMonth > 2.5) {
			iYear = (int) (iD - 4716);
			ChapGUI.printlnCond("more than 2.5, year = D - 4716 = " + iD + " - 4716 = " + iYear);
		} else {
			iYear = (int) (iD - 4715);
			ChapGUI.printlnCond("less than 2.5, year = D - 4715 = " + iD + " - 4715 = " + iYear);
		}
		ChapGUI.printlnCond();

		result = result + " JD = " + ASTDate.dateToStr(iMonth,dDay,iYear);
		ChapGUI.printCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * See if a year is a leap year
	 */
	protected void calcLeapYear() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		ASTInt iObj;
		String sfYear, result;
		int iYear;
		boolean bPass, bLeapYear = false;

		prt.clearTextArea();

		iObj = ASTInt.isValidInt(strInput);
		if (!iObj.isValidIntObj()) return;

		iYear = iObj.getIntValue();
		if (iYear < 1582) {
			ASTMsg.errMsg("Year must be > 1581. Try again", "Invalid Year");
			ChapGUI.setResults("");
			return;
		}

		sfYear = String.format("%d",iYear);
		result = "The year " + sfYear + " is ";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Determine if " + sfYear + " is a leap year", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("A year is a leap year if two conditions are satisfied:");
		ChapGUI.printlnCond("  (a) the year must be evenly divisible by 4");
		ChapGUI.printlnCond("  (b) if the year is a century year, it must also be evenly divisible by 400");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("1. Check condition 'a'");
		bPass = ASTMath.isEvenlyDivisible(iYear, 4);
		ChapGUI.printlnCond("   year / 4 = " + sfYear + " / 4 = " + iYear / 4);
		ChapGUI.printCond("   So, " + sfYear + " is ");
		if (!bPass) ChapGUI.printCond("not ");
		ChapGUI.printlnCond("evenly divisible by 4.");
		if (!bPass) ChapGUI.printlnCond("   No need to check any further.");

		if (bPass) {
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("2. Check condition 'b'. A year is a century year if it is evenly divisible by 100.");
			bPass = ASTMath.isEvenlyDivisible(iYear, 100);

			ChapGUI.printlnCond("   year / 100 = " + sfYear + " / 100 = " + iYear / 100);
			ChapGUI.printCond("   So, " + sfYear + " is ");
			if (!bPass) {
				ChapGUI.printCond("not ");
				bLeapYear = true;
			}
			ChapGUI.printlnCond("evenly divisible by 100.");
			if (!bPass) ChapGUI.printlnCond("   No need to check any further since year is not a century year.");
		}

		if (bPass) {
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("3. Since " + sfYear + " is a century year, check if it is evenly divisible by 400.");
			bPass = ASTMath.isEvenlyDivisible(iYear, 400);
			ChapGUI.printlnCond("   year / 400 = " + sfYear + " / 400 = " + iYear / 400);
			ChapGUI.printCond("   So, " + sfYear + " is ");
			if (bPass) bLeapYear = true;
			else {
				ChapGUI.printCond("not ");
				bLeapYear = false;
			}
			ChapGUI.printlnCond("evenly divisible by 400.");
		}

		if (!bLeapYear) result = result + "not ";
		result = result + "a leap year";
		ChapGUI.printlnCond();
		ChapGUI.printlnCond(result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/*=================================================================
	 * Routines to do the actual work for the Local vs Greenwich menu
	 *================================================================*/

	/**
	 * Convert GST to LST
	 */
	protected void calcGST2LST() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strData1 = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strLon = ASTStr.removeWhitespace(ChapGUI.getLongitudeData());
		double dLongitude, GST, LST, adjustment;
		String result, strtmp;

		prt.clearTextArea();

		ASTTime timeObj = ASTTime.isValidTime(strData1);
		if (!timeObj.isValidTimeObj()) return;
		ASTLatLon dLonObj = ASTLatLon.isValidLon(strLon);
		if (!dLonObj.isLonValid()) return;

		dLongitude = dLonObj.getLon();
		strtmp = ASTTime.timeToStr(timeObj, ASTMisc.HMSFORMAT);
		result = strtmp + " GST at Longitude " + strLon;

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to LST", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		GST = timeObj.getDecTime();
		ChapGUI.printlnCond("1. Convert the GST entered to decimal format. GST = " + 
				String.format(ASTStyle.decHoursFormat,GST));
		ChapGUI.printlnCond();

		adjustment = dLongitude / 15.0;
		ChapGUI.printlnCond("2. Calculate an adjustment for longitude.");
		ChapGUI.printlnCond("   ADJUST = (Longitude) / 15 = (" + dLonObj.getLon() + ") / 15");
		ChapGUI.printlnCond("          = " + String.format("%.6f",adjustment));
		ChapGUI.printlnCond();

		LST = GST + adjustment;
		ChapGUI.printlnCond("3. LST = GST + ADJUST = " + String.format(ASTStyle.decHoursFormat,GST) + " + (" +
				String.format("%.6f",adjustment) + ") = " + String.format(ASTStyle.decHoursFormat,LST));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4. If LST is negative, add 24. If LST is greater than 24, subtract 24.");
		ChapGUI.printlnCond("   Otherwise, make no adjustment.");
		ChapGUI.printCond("   Thus, LST = ");
		if (LST < 0) {
			LST = LST + 24;
			ChapGUI.printCond(" LST + 24 = ");
		}
		if (LST > 24) {
			LST = LST - 24;
			ChapGUI.printCond(" LST - 24 = ");
		}
		ChapGUI.printlnCond(String.format(ASTStyle.decHoursFormat,LST));
		ChapGUI.printlnCond();

		strtmp = ASTTime.timeToStr(LST, ASTMisc.HMSFORMAT);
		ChapGUI.printlnCond("5. Finally, convert LST to hours, minutes, seconds");
		ChapGUI.printlnCond("   So, LST = " + strtmp);
		ChapGUI.printlnCond();

		result = result + " = " + strtmp + " LST";
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert GST to UT
	 */
	protected void calcGST2UT() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strData1 = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strData2 = ASTStr.removeWhitespace(ChapGUI.getData2());
		int days;
		double JD, JD0, dT, dR, dB, T0, UT, GST, dA;
		String result, strtmp;
		int iYear;

		prt.clearTextArea();

		ASTTime timeObj = ASTTime.isValidTime(strData1);
		if (!timeObj.isValidTimeObj()) return;
		ASTDate dateObj = ASTDate.isValidDate(strData2);
		if (!dateObj.isValidDateObj()) return;

		iYear = dateObj.getYear();

		strtmp = ASTTime.timeToStr(timeObj, ASTMisc.HMSFORMAT);
		result = strtmp + " GST on " + ASTDate.dateToStr(dateObj);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to UT", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		JD = ASTDate.dateToJD(dateObj);
		ChapGUI.printlnCond("1.  Convert given date to a Julian day number");
		ChapGUI.printlnCond("    Thus, " + ASTDate.dateToStr(dateObj) + " = " + 
				String.format(ASTStyle.JDFormat,JD) + " JD");
		ChapGUI.printlnCond();

		JD0 = ASTDate.dateToJD(1, 0, iYear);
		ChapGUI.printlnCond("2.  Calculate the Julian day number for January 0.0 of the given year");
		ChapGUI.printlnCond("    Thus, JD0 = " + ASTDate.dateToStr(1, 0.0, iYear) + " = " 
				+ String.format(ASTStyle.JDFormat,JD0));
		ChapGUI.printlnCond();

		days = (int) (JD - JD0);
		ChapGUI.printlnCond("3.  Subtract step 2 from step 1 to get number of days into the year");
		ChapGUI.printlnCond("    This gives Days = " + days);
		ChapGUI.printlnCond();

		dT = (JD0 - 2415020.0) / 36525.0;
		ChapGUI.printlnCond("4.  Let T = [(JD0 - 2415020.0) / 36525] = [(" + 
				String.format(ASTStyle.JDFormat,JD0) + " - 2415020.0) / 36525]");
		ChapGUI.printlnCond("          = " + String.format("%.9f",dT));
		ChapGUI.printlnCond();

		dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581;
		ChapGUI.printlnCond("5.  Let R = 6.6460656 + T * 2400.051262 + T * T * 0.00002581");
		ChapGUI.printlnCond("          = 6.6460656+" + String.format("%.9f",dT) + "*2400.05162" +
				"+" + String.format("%.9f",dT) + "*" + String.format("%.9f",dT) + "*0.00002581");
		ChapGUI.printlnCond("          = " + String.format("%.9f",dR));
		ChapGUI.printlnCond();

		dB = 24.0 - dR + 24.0 * (iYear - 1900);
		ChapGUI.printlnCond("6.  Let B = 24 - R + 24*(year - 1900) = 24 - " + String.format("%.9f",dR) +
				" + 24*(" + iYear + " - 1900)");
		ChapGUI.printlnCond("          = " + String.format("%.9f",dB));
		ChapGUI.printlnCond();

		T0 = 0.0657098 * (days) - dB;
		ChapGUI.printlnCond("7.  Let T0 = 0.0657098*days - B = 0.0657098*" + days + " - " + String.format("%.9f",dB));
		ChapGUI.printlnCond("           = " + String.format("%.9f",T0));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("8.  If the result of step 7 is negative, add 24 hours. If the result of step 7");
		ChapGUI.printlnCond("    is greater than 24, subtract 24 hours. Otherwise, make no adjustment.");
		ChapGUI.printCond("    Thus, T0 = ");
		if (T0 < 0) {
			T0 = T0 + 24;
			ChapGUI.printCond(" T0 + 24 = ");
		}
		ChapGUI.printlnCond(String.format("%.9f",T0));
		ChapGUI.printlnCond();

		GST = timeObj.getDecTime();
		ChapGUI.printlnCond("9.  Convert the GST entered to decimal if HMS was entered. Thus, GST = " + 
				String.format(ASTStyle.decHoursFormat,GST));
		ChapGUI.printlnCond();

		dA = GST - T0;
		ChapGUI.printCond("10. Let A = GST - T0 = " + String.format(ASTStyle.decHoursFormat,GST) +
				" - " + String.format("%.9f",T0));
		ChapGUI.printlnCond(" = " + String.format(ASTStyle.decHoursFormat,dA));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. If A is negative, add 24. Otherwise, make no adjustment.");
		ChapGUI.printCond("    Thus, A = ");
		if(dA < 0) {
			dA = dA + 24;
			ChapGUI.printCond(" A + 24 = ");
		}
		ChapGUI.printlnCond(String.format("%.9f",dA));
		ChapGUI.printlnCond();

		UT = dA * 0.99727;
		ChapGUI.printlnCond("12. UT = A * 0.997270 = " + String.format("%.9f",dA) +
				" * 0.997270 = " + String.format(ASTStyle.decHoursFormat,UT));
		ChapGUI.printlnCond();

		strtmp = ASTTime.timeToStr(UT, ASTMisc.HMSFORMAT);
		ChapGUI.printlnCond("13. Finally, convert UT to hours, minutes, seconds");
		ChapGUI.printlnCond("    So, UT = " + strtmp);
		ChapGUI.printlnCond();

		result = result + " = " + strtmp + " UT";
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert LCT to UT
	 */
	protected void calcLCT2UT() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strLon = ASTStr.removeWhitespace(ChapGUI.getLongitudeData());
		ASTLatLon dLonObj;
		double LCT, UT, decTime, dAdjustment, dT;
		double dLongitude = 0.0;
		String result, saveTZone = "", dayChange = "", strtmp = "";
		ASTLatLon.TimeZoneType radbtn = ChapGUI.getSelectedRBStatus();

		prt.clearTextArea();

		ASTTime timeObj = ASTTime.isValidHMSTime(strInput);
		if (!timeObj.isValidTimeObj()) return;
		result = ASTTime.timeToStr(timeObj,ASTMisc.HMSFORMAT) + " LCT";

		// If necessary, validate the longitude
		if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE) {
			dLonObj = ASTLatLon.isValidLon(strLon);
			if (!dLonObj.isLonValid()) return;
			dLongitude = dLonObj.getLon();
			strLon = ASTLatLon.lonToStr(dLonObj, ASTMisc.DECFORMAT);			
		}

		if (ChapGUI.getDSTStatus()) saveTZone = "(DST, ";
		else saveTZone = "(";

		if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE) {
			saveTZone = saveTZone + "at Longitude " + strLon + ")";
		} else {
			saveTZone = saveTZone + "in " + radbtn.toString() + " time zone)";
		}

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " " + saveTZone + " to UT", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		LCT = timeObj.getDecTime();
		ChapGUI.printlnCond("1. Convert LCT to decimal format. LCT = " +
				String.format(ASTStyle.decHoursFormat,LCT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2. If necessary, adjust for daylight saving time.");
		if (ChapGUI.getDSTStatus()) {
			decTime = LCT - 1;
			ChapGUI.printlnCond("   Since this is on daylight saving time, subtract 1 hour.") ;
		} else {
			decTime = LCT;
			ChapGUI.printlnCond("   No adjustment is necessary since this is not daylight saving time.");
		}
		ChapGUI.printlnCond("   This gives T = " + String.format(ASTStyle.decHoursFormat,decTime));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3. Calculate a time zone adjustment");
		dAdjustment = timeZoneAdjustment(true, radbtn, dLongitude);
		ChapGUI.printlnCond();

		dT = decTime - dAdjustment;
		ChapGUI.printlnCond("4. Subtract the time zone adjustment in step 3 from the");
		ChapGUI.printlnCond("   result of step 2 giving");
		ChapGUI.printlnCond("   UT = " + String.format(ASTStyle.decHoursFormat,decTime) +
				" - (" + String.format(ASTStyle.decHoursFormat,dAdjustment) +
				") = " + String.format(ASTStyle.decHoursFormat,dT));
		ChapGUI.printlnCond();

		UT = dT;
		ChapGUI.printlnCond("5. If the result of step 4 is negative, add 24. If the result of step 4");
		ChapGUI.printlnCond("   is greater than 24, subtract 24. Otherwise, make no further adjustments.");
		ChapGUI.printCond("   Thus, UT = " + String.format(ASTStyle.decHoursFormat,UT));
		if (dT > 24) {
			UT = dT - 24.0;
			dayChange = " (next day)";
			ChapGUI.printCond(" - 24 = " + String.format(ASTStyle.decHoursFormat,UT) + dayChange);
		}
		if (dT < 0) {
			UT = dT + 24.0;
			dayChange = " (previous day)";
			ChapGUI.printCond(" + 24 = " + String.format(ASTStyle.decHoursFormat,UT) + dayChange);

		}
		ChapGUI.printlnCond();
		ChapGUI.printlnCond();

		strtmp = ASTTime.timeToStr(UT, ASTMisc.HMSFORMAT);
		ChapGUI.printlnCond("6. Finally, convert to hours, minutes, seconds format, which gives");
		ChapGUI.printlnCond("   UT = " + strtmp);
		ChapGUI.printlnCond();

		result = result + " " + saveTZone + " = " + strtmp + " UT" + dayChange;
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert LST to GST
	 */
	protected void calcLST2GST() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strData1 = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strLon = ASTStr.removeWhitespace(ChapGUI.getLongitudeData());
		double dLongitude, GST, LST, adjustment;
		String result, strtmp;

		prt.clearTextArea();

		ASTTime timeObj = ASTTime.isValidTime(strData1);
		if (!timeObj.isValidTimeObj()) return;
		ASTLatLon dLonObj = ASTLatLon.isValidLon(strLon);
		if (!dLonObj.isLonValid()) return;
		dLongitude = dLonObj.getLon();
		strtmp = ASTTime.timeToStr(timeObj, ASTMisc.HMSFORMAT);
		result = strtmp + " LST at Longitude " + strLon;

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to GST", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		LST = timeObj.getDecTime();
		ChapGUI.printlnCond("1. Convert the LST entered to decimal format. LST = " + 
				String.format(ASTStyle.decHoursFormat,LST));
		ChapGUI.printlnCond();

		adjustment = dLongitude / 15.0;
		ChapGUI.printlnCond("2. Calculate an adjustment for longitude.");
		ChapGUI.printlnCond("   ADJUST = (Longitude) / 15 = (" + dLonObj.getLon() + ") / 15");
		ChapGUI.printlnCond("          = " + String.format("%.6f",adjustment));
		ChapGUI.printlnCond();

		GST = LST - adjustment;
		ChapGUI.printlnCond("3. GST = LST - ADJUST = " + String.format(ASTStyle.decHoursFormat,LST) + " - (" +
				String.format("%.6f",adjustment) + ") = " + String.format(ASTStyle.decHoursFormat,GST));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4. If GST is negative, add 24. If GST is greater than 24, subtract 24.");
		ChapGUI.printlnCond("   Otherwise, make no adjustment.");
		ChapGUI.printCond("   Thus, GST = ");
		if (GST < 0) {
			GST = GST + 24;
			ChapGUI.printCond(" GST + 24 = ");
		}
		if (GST > 24) {
			GST = GST - 24;
			ChapGUI.printCond(" GST - 24 = ");
		}
		ChapGUI.printlnCond(String.format(ASTStyle.decHoursFormat,GST));
		ChapGUI.printlnCond();

		strtmp = ASTTime.timeToStr(GST, ASTMisc.HMSFORMAT);
		ChapGUI.printlnCond("5. Finally, convert GST to hours, minutes, seconds");
		ChapGUI.printlnCond("   So, GST = " + strtmp);
		ChapGUI.printlnCond();

		result = result  +  " = "  +  strtmp  +  " GST";
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();		
	}

	/**
	 * Converts UT to GST
	 */
	protected void calcUT2GST() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strData1 = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strData2 = ASTStr.removeWhitespace(ChapGUI.getData2());
		double days, JD, JD0, dT, dR, dB, T0, UT, GST;
		String result, strtmp;
		int iYear;

		prt.clearTextArea();

		ASTTime timeObj = ASTTime.isValidTime(strData1);
		if (!timeObj.isValidTimeObj()) return;
		ASTDate dateObj = ASTDate.isValidDate(strData2);
		if (!dateObj.isValidDateObj()) return;

		iYear = dateObj.getYear();

		result = ASTTime.timeToStr(timeObj, ASTMisc.HMSFORMAT)+ " UT on " + ASTDate.dateToStr(dateObj);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to GST", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		JD = ASTDate.dateToJD(dateObj);
		ChapGUI.printlnCond("1.  Convert given date to a Julian day number");
		ChapGUI.printlnCond("    Thus, " + ASTDate.dateToStr(dateObj) + " = " + String.format(ASTStyle.JDFormat,JD)  + " JD");
		ChapGUI.printlnCond();

		JD0 = ASTDate.dateToJD(1, 0, iYear);
		ChapGUI.printlnCond("2.  Calculate the Julian day number for January 0.0 of the given year");
		ChapGUI.printlnCond("    Thus, JD0 = " + ASTDate.dateToStr(1, 0.0, iYear) + " = " + String.format(ASTStyle.JDFormat,JD0));
		ChapGUI.printlnCond();

		days = JD - JD0;
		ChapGUI.printlnCond("3.  Subtract step 2 from step 1 to get number of days into the year");
		ChapGUI.printlnCond("    This gives Days = " + days);
		ChapGUI.printlnCond();

		dT = (JD0 - 2415020.0) / 36525.0;
		ChapGUI.printlnCond("4.  Let T = [(JD0 - 2415020.0) / 36525] = [(" + 
				String.format(ASTStyle.JDFormat,JD0) + " - 2415020.0) / 36525]");
		ChapGUI.printlnCond("          = " + String.format("%.9f",dT));
		ChapGUI.printlnCond();

		dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581;
		ChapGUI.printlnCond("5.  Let R = 6.6460656 + T * 2400.051262 + T * T * 0.00002581");
		ChapGUI.printlnCond("          = 6.6460656+" + String.format("%.9f",dT) + "*2400.05162" +
				"+" + String.format("%.9f",dT) + "*" + String.format("%.9f",dT) + "*0.00002581");
		ChapGUI.printlnCond("          = " + String.format("%.9f",dR));
		ChapGUI.printlnCond();

		dB = 24.0 - dR + 24.0 * (iYear - 1900);
		ChapGUI.printlnCond("6.  Let B = 24 - R + 24*(year - 1900) = 24 - " + String.format("%.9f",dR) +
				" + 24*(" + iYear + " - 1900)");
		ChapGUI.printlnCond("          = " + String.format("%.9f",dB));
		ChapGUI.printlnCond();

		T0 = 0.0657098 * (days) - dB;
		ChapGUI.printlnCond("7.  Let T0 = 0.0657098*days - B = 0.0657098*" + days + " - " + String.format("%.9f",dB));
		ChapGUI.printlnCond("           = " + String.format("%.9f",T0));
		ChapGUI.printlnCond();

		UT = timeObj.getDecTime();
		ChapGUI.printlnCond("8.  Convert the UT entered to decimal if HMS was entered. Thus, UT = " +
				String.format(ASTStyle.decHoursFormat,UT));
		ChapGUI.printlnCond();

		GST = T0 + UT * 1.002738;
		ChapGUI.printlnCond("9.  GST = T0 + UT * 1.002738 = " + String.format("%.9f",T0) + " + " + 
				String.format(ASTStyle.decHoursFormat,UT) +	" * 1.002738 = " + 
				String.format(ASTStyle.decHoursFormat,GST));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. If GST is negative, add 24. If GST is greater than 24, subtract 24.");
		ChapGUI.printlnCond("    Otherwise, make no adjustment.");
		ChapGUI.printCond("    Thus, GST = ");
		if (GST < 0) {
			GST = GST + 24;
			ChapGUI.printCond(" GST + 24 = ");
		}
		if (GST > 24) {
			GST = GST - 24;
			ChapGUI.printCond(" GST - 24 = ");
		}
		ChapGUI.printlnCond(String.format(ASTStyle.decHoursFormat,GST));
		ChapGUI.printlnCond();

		strtmp = ASTTime.timeToStr(GST, ASTMisc.HMSFORMAT);
		ChapGUI.printlnCond("11. Finally, convert GST to hours, minutes, seconds");
		ChapGUI.printlnCond("    So, GST = " + strtmp);
		ChapGUI.printlnCond();

		result = result + " = " + strtmp + " GST";
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert UT to LCT
	 */
	protected void calcUT2LCT() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String strLon = ASTStr.removeWhitespace(ChapGUI.getLongitudeData());
		double LCT, UT, dAdjustment, dT;
		double dLongitude = 0.0;
		String result, saveTZone = "", dayChange="", strtmp = "";
		ASTLatLon.TimeZoneType radbtn = ChapGUI.getSelectedRBStatus();

		prt.clearTextArea();

		ASTTime timeObj = ASTTime.isValidTime(strInput);
		if (!timeObj.isValidTimeObj()) return;
		result = ASTTime.timeToStr(timeObj, ASTMisc.HMSFORMAT) + " UT";

		// If necessary, validate the longitude
		if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE) {
			ASTLatLon dLonObj = ASTLatLon.isValidLon(strLon);
			if (!dLonObj.isLonValid()) return;
			dLongitude = dLonObj.getLon();
			strLon = ASTLatLon.lonToStr(dLonObj, ASTMisc.DECFORMAT);
		}

		if (ChapGUI.getDSTStatus()) saveTZone = "(DST, ";
		else saveTZone = "(";

		if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE) saveTZone = saveTZone + "at Longitude " + strLon + ")";
		else saveTZone = saveTZone + "in " + radbtn.toString() + " time zone)";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to LCT " + saveTZone, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		UT = timeObj.getDecTime();
		ChapGUI.printlnCond("1. Convert UT to decimal format. UT = " + String.format(ASTStyle.decHoursFormat,UT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2. Calculate a time zone adjustment");
		dAdjustment = timeZoneAdjustment(true, radbtn, dLongitude);
		ChapGUI.printlnCond();

		dT = UT + dAdjustment;
		ChapGUI.printlnCond("3. Add the time zone adjustment from step 2 to the");
		ChapGUI.printlnCond("   result of step 1 giving");
		ChapGUI.printlnCond("   LCT = " + String.format(ASTStyle.decHoursFormat,UT) + " + (" + 
				String.format(ASTStyle.decHoursFormat,dAdjustment) + ") = " + 
				String.format(ASTStyle.decHoursFormat,dT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4. If the result of step 3 is negative, add 24. If the result of step 3");
		ChapGUI.printlnCond("   is greater than 24, subtract 24. Otherwise, make no further adjustments.");
		ChapGUI.printCond("   Thus, LCT = " + String.format(ASTStyle.decHoursFormat,dT));
		LCT = dT;
		if (dT > 24) {
			LCT = dT - 24.0;
			dayChange = " (next day)";
			ChapGUI.printCond(" - 24 = " + String.format(ASTStyle.decHoursFormat,LCT) + dayChange);
		}
		if (dT < 0) {
			LCT = dT + 24.0;
			dayChange = " (previous day)";
			ChapGUI.printCond(" + 24 = " + String.format(ASTStyle.decHoursFormat,LCT) + dayChange);
		}
		ChapGUI.printlnCond();
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5. If necessary, adjust for daylight saving time.");
		if (ChapGUI.getDSTStatus()) {
			LCT = LCT + 1;
			ChapGUI.printlnCond("   Since this is on daylight saving time, add 1 hour.");
		} else ChapGUI.printlnCond("   No adjustment is necessary since this is not daylight saving time.");
		ChapGUI.printlnCond("   This gives LCT = " + String.format(ASTStyle.decHoursFormat,LCT));
		ChapGUI.printlnCond();

		strtmp = ASTTime.timeToStr(LCT, ASTMisc.HMSFORMAT);
		ChapGUI.printlnCond("6. Finally, convert to hours, minutes, seconds format, which gives");
		ChapGUI.printlnCond("   LCT = " + strtmp);
		ChapGUI.printlnCond();

		result = result + " = " + strtmp + " LCT " + dayChange + " " + saveTZone;
		ChapGUI.printlnCond("Therefore, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();	
	}

	/*-----------------------------------------------------------------
	 * Some common helper routines used only in this class
	 -----------------------------------------------------------------*/

	/**
	 * Determine a time zone adjustment.
	 * 
	 * @param flag			if true, print out intermediate results
	 * @param radbtn		radio button setting from the GUI
	 * @param longitude		longitude value from the GUI
	 * @return				a time zone adjustment
	 */
	private static double timeZoneAdjustment(boolean flag,ASTLatLon.TimeZoneType radbtn,double longitude) {
		double adjustment;

		if (radbtn == ASTLatLon.TimeZoneType.LONGITUDE) {
			adjustment = ASTMath.Round((longitude / 15.0),0);
			if (flag) {
				ChapGUI.printlnCond("   For a given longitude, the time zone adjustment factor is");
				ChapGUI.printlnCond("   determined by the formula");
				ChapGUI.printlnCond("   Adjustment = ROUND(Longitude / 15)");
				ChapGUI.printlnCond("              = ROUND(" + String.format(ASTStyle.latlonFormat,longitude) + " / 15)");
				ChapGUI.printlnCond("              = ROUND(" + String.format(ASTStyle.latlonFormat,longitude / 15.0) +
						") = " + String.format(ASTStyle.decHoursFormat,adjustment));
			}
		} else {
			adjustment = radbtn.getAdjust();
			if (flag) ChapGUI.printlnCond("   For the " + radbtn.toString() +
					" time zone, the time zone adjustment is " + String.format(ASTStyle.decHoursFormat,adjustment));
		}

		return adjustment;
	}

}
