package jll.celcalc.chap3;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
 * This class extends JMenuItem to add some additional fields, which
 * simplifies the code for adding items to the menu bar and determining
 * what actions to take as various menu items are selected. 
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {

	/** Define values for the eCalcType field in a ChapMenuItem class. These indicate what time-related conversion to do. */
	protected enum CalculationType {
		/** no calculation is currently selected */ NO_CALC_SELECTED,
		// Time Conversions
		/** determine if a Gregorian year is a leap year */ LEAP_YEARS, 
		/** convert a calendar date to a Julian day number */ CALENDAR_2_JD,
		/** convert a Julian day number to a calendar date */ JD_2_CALENDAR,
		/** determine what day of the week a particular date falls */ DAY_OF_WEEK,
		/** determine the number of days into the year that a given date falls */ DATE_2_DAYS_INTO_YEAR, 
		/** given a number of days into the year, determine the date */ DAYS_INTO_YEAR_2_DATE,
		// Local vs Greenwich
		/** convert LCT to UT */ LCT_2_UT,
		/** convert UT to LCT */ UT_2_LCT, 
		/** convert UT to GST */ UT_2_GST,
		/** convert GST to UT */ GST_2_UT,
		/** convert GST to LST */ GST_2_LST, 
		/** convert LST to GST */ LST_2_GST
	}

	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Time Conversions and Local vs Greenwich menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;

		// Time Conversions menu
		menu = ChapGUI.getTimeConvMenu();
		createMenuItem(menu,"Leap Years",CalculationType.LEAP_YEARS);
		menu.addSeparator();
		createMenuItem(menu,"Calendar Date to Julian Day Number", CalculationType.CALENDAR_2_JD);
		createMenuItem(menu,"Julian Day Number to Calendar Date", CalculationType.JD_2_CALENDAR);
		menu.addSeparator();
		createMenuItem(menu,"Day of Week", CalculationType.DAY_OF_WEEK);
		createMenuItem(menu,"Date to Days into Year", CalculationType.DATE_2_DAYS_INTO_YEAR);
		createMenuItem(menu,"Days into Year to Date", CalculationType.DAYS_INTO_YEAR_2_DATE);

		// Local vs Greenwich menu
		menu = ChapGUI.getLocalGreenwichMenu();
		createMenuItem(menu,"LCT to UT", CalculationType.LCT_2_UT);
		createMenuItem(menu,"UT to LCT", CalculationType.UT_2_LCT);
		menu.addSeparator();
		createMenuItem(menu,"UT to GST", CalculationType.UT_2_GST);
		createMenuItem(menu,"GST to UT", CalculationType.GST_2_UT);
		menu.addSeparator();
		createMenuItem(menu,"GST to LST", CalculationType.GST_2_LST);
		createMenuItem(menu,"LST to GST", CalculationType.LST_2_GST);
	}

	/** Define a class for what a time conversions menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of time conversion to do
		private CalculationType eCalcType;   // what type of conversion to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of conversion is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {

			super(title);
			eCalcType = calcType;
		}
		
		/*=================================================================
		 * Define accessor methods for the menu item fields
		 *================================================================*/
		
		/**
		 * Gets the type of conversion to perform.
		 * 
		 * @return		the type of conversion to perform
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}

	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 -----------------------------------------------------------------------------*/

	/**
	 * Creates a time conversion menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of conversion to perform
	 */     
	private void createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getTimeConversionsCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());	
	}

}
