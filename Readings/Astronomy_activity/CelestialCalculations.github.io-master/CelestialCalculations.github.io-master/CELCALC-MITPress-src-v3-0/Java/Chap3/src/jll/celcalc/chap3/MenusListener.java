package jll.celcalc.chap3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap3.ChapMenuItems.CalculationType;
import jll.celcalc.chap3.ChapMenuItems.ChapMnuItm;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class MenusListener implements ActionListener {
	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}

		// Handle Time Conversions and Local to Greenwich menus
		else if (action.equals(ChapGUI.getTimeConversionsCommand())) {
			ChapMnuItm objMenuItem = (ChapMnuItm) e.getSource();		// the menu item the user clicked on

			ChapGUI.setCalcToDo(objMenuItem.getCalcType());
			prt.clearTextArea();
			
			switch (objMenuItem.getCalcType()) {
			//**** Time Conversions
			case LEAP_YEARS:
				ChapGUI.setDataLabels("Year (in form YYYY)");
				ChapGUI.setResults("Determine if a year is a leap year");
				break;
			case CALENDAR_2_JD:
				ChapGUI.setDataLabels("Date (in form MM/DD.dd/YYYY)");
				ChapGUI.setResults("Convert Calendar Date to a Julian Day Number");
				break;
			case JD_2_CALENDAR:
				ChapGUI.setDataLabels("Julian Day Number (ex: 2455928.50)");
				ChapGUI.setResults("Convert Julian Day Number to a Calendar Date");
				break;
			case DAY_OF_WEEK:
				ChapGUI.setDataLabels("Date (in form MM/DD/YYYY)");
				ChapGUI.setResults("Calculate the day of the week for a given date");
				break;
			case DATE_2_DAYS_INTO_YEAR:
				ChapGUI.setDataLabels("Date (in form MM/DD/YYYY)");
				ChapGUI.setResults("Convert a Calendar Date to Days into the Year");
				break;
			case DAYS_INTO_YEAR_2_DATE:
				ChapGUI.setDataLabels("Days into Year (e.g., 15)", "Year");
				ChapGUI.setResults("Convert Days into the Year to a Calendar Date");
				break;
				//**** Local vs Greenwich
			case LCT_2_UT:
				ChapGUI.setDataLabels("Enter LCT time (hh:mm:ss.ss)", "(Set Time Zone or Longitude above)");
				ChapGUI.setResults("Convert Local Civil Time to Universal Time");
				break;
			case UT_2_LCT:
				ChapGUI.setDataLabels("Enter UT time (hh:mm:ss.ss)", "(Set Time Zone or Longitude above)");
				ChapGUI.setResults("Convert Universal Time to Local Civil Time");
				break;
			case UT_2_GST:
				ChapGUI.setDataLabels("Enter UT time (hh:mm:ss.ss)", "Date (in form MM/DD/YYYY)");
				ChapGUI.setResults("Convert Universal Time to Greenwich Sidereal Time");
				break;
			case GST_2_UT:
				ChapGUI.setDataLabels("Enter GST time (hh:mm:ss.ss)", "Date (in form MM/DD/YYYY)");
				ChapGUI.setResults("Convert Greenwich Sidereal Time to Universal Time");
				break;
			case GST_2_LST:
				ChapGUI.setLonRadBtn();
				ChapGUI.setDataLabels("Enter GST time (hh:mm:ss.ss)", "(Enter Longitude in the Time Zone box above)");
				ChapGUI.setResults("Convert Greenwich Sidereal Time to Local Sidereal Time");
				break;
			case LST_2_GST:
				ChapGUI.setLonRadBtn();
				ChapGUI.setDataLabels("Enter LST time (hh:mm:ss.ss)", "(Enter Longitude in the Time Zone box above)");
				ChapGUI.setResults("Convert Local Sidereal Time to Greenwich Sidereal Time");
				break;
			default:
				ASTMsg.criticalErrMsg("Invalid menu item " + action);
				ChapGUI.setCalcToDo(CalculationType.NO_CALC_SELECTED);
				break;
			}
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			prt.clearTextArea();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " +
					"intermediate results as the various calculations are performed. Also check the 'Daylight Saving Time' " +
					"checkbox if the times that will be entered are on Daylight Saving Time.");
			prt.println();

			prt.println("Second, select the appropriate time zone (EST, CST, etc.), or select the 'Longitude' radio " +
					"button if a longitude will be entered rather than a time zone. If a longitude is to be entered, " +
					"enter the longtiude in the text box beside the 'Longitude' radio button. Longitudes may be entered " +
					"in decimal format (e.g., 96.5E, 30.45W) or DMS format (e.g., 96d 30m 18.5sW). The N, S, E, W direction " +
					"must be capitalized. That is, 96.5e and 30.45w are both errors because the direction is not capitalized.");
			prt.println();

			prt.print("Third, after establishing the desired checkbox and radio button settings, select the desired " +
					"calculation to perform from the 'Time Conversions' or the 'Local vs. Greenwich' menus. Once a " +
					"menu item is selected, you will be asked to enter one or two lines of data in the text boxes beneath the " +
					"Time Zone radio buttons. The values to be entered depend upon what menu item was selected. For " +
					"converting between GST and LST, the longitude must be entered in the text box beside the 'Longitude' " +
					"radio button ");
			prt.setBoldFont(true);
			prt.print("*and not*");
			prt.setBoldFont(false);
			prt.println(" in one of the text boxes below the Time Zone radio buttons. Note that time is " +
					"entered in 24 hour format. Thus, 3:30:20 PM would be entered as 15:30:20.");
			prt.println();

			prt.print("Finally, after entering all required data, click the 'Calculate' button at the bottom " +
					"right of the data input area. The selected computation (e.g., LCT to UT) will be done. " +
					"If you selected the 'Show Intermedicate Calculations' checkbox, the steps required to find " +
					"a solution will be shown in the scrollable text output area (i.e., where these instructions " +
					"are being displayed). The final result will be shown just below the data input text boxes.");

			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
