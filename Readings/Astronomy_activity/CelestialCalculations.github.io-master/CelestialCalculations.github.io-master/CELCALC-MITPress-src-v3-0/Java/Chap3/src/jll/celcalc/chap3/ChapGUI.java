package jll.celcalc.chap3;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jll.celcalc.ASTUtils.ASTAboutBox;
import jll.celcalc.ASTUtils.ASTBookInfo;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.chap3.ChapMenuItems.CalculationType;

/**
 * <b>Implements the main GUI</b>
 * <p>
 * The GUI was created with, and is maintained by, the Eclipse WindowBuilder.
 * All methods and data are declared static because it only makes sense to have
 * one main GUI per application, and making them static avoids the problem of
 * having to pass a reference to the GUI instance in other classes that need
 * to reference the main GUI.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 * <p>
 * Note that the Eclipse Window Builder has trouble with complex, dynamic GUIs so that
 * one should first build the GUI, then modify the code by hand to use
 * the definitions in ASTUtils.ASTStyle. The Window Builder start/stop hiding tags can be used to
 * surround code that the Window Builder parser has trouble handling.
 * The start hiding tag is <code>{@literal //$hide>>$}</code> while the stop
 * hiding tag is <code>{@literal //$hide<<$}</code>. For example,
 * <p>
 * <code>
 * {@literal //$hide>>$}
 * 		code to be hidden
 * {@literal //$hide<<$}
 * </code>
 */

@SuppressWarnings("serial")
class ChapGUI extends JFrame {
	private static final String WINDOW_TITLE = "Chapter 3 - Time Conversions";

	// Save the scrollable text pane area for output
	private static JTextPane textPane = new JTextPane();
	private static ASTPrt prt = null;				// create instance (actually done below) for printing to the output textPane

	// Create an About Box
	private static ASTAboutBox aboutBox = new ASTAboutBox(WINDOW_TITLE);

	// Create menu items, which will be added to in the ChapMenuItems class
	private static JMenu mnTimeConversions = null;
	private static JMenu mnLocalGreenwich = null;

	// Each menu item selection sets this variable to know what to do when the calculate button is pressed
	private static CalculationType calculationToDo = CalculationType.NO_CALC_SELECTED;	

	// Create listeners for the various GUI components
	private static ActionListener listenerMenus = new MenusListener();
	private static ActionListener listenerGUIBtns = new GUIButtonsListener();
	private static ActionListener listenerRadioBtns = new RadioBtnsListener();

	// Various GUI components that user will interact with
	private static JCheckBox chkboxShowInterimCalcs = new JCheckBox("Show Intermediate Calculations");
	private static JCheckBox chkboxDST = new JCheckBox("Daylight Saving Time");
	private static JTextField txtboxData1 = new JTextField("1234");		// text field for user input
	private static JLabel lblData1 = new JLabel("Data 1");				// label for 1st data field
	private static JTextField txtboxData2 = new JTextField("1234");		// text field for user input
	private static JLabel lblData2 = new JLabel("Data 2");				// label for 2nd data field
	private static JLabel lblResults = new JLabel("Results");			// label for displaying results

	// define radio buttons for selecting Time Zone
	private static JRadioButton radbtnPST;
	private static JRadioButton radbtnMST;
	private static JRadioButton radbtnCST;
	private static JRadioButton radbtnEST;
	private static JRadioButton radbtnLon;
	private static JTextField txtboxLon = new JTextField("");		// text field when enter longitude rather than time zone

	/*===========================================================================================
	 * The string text collected in these next items is done to separate the strings displayed
	 * in the various GUI components (such as buttons and menus) from the semantics of what 
	 * clicking on a component means. This is important because otherwise, a change to the text
	 * displayed in the GUI, such as for a button label, via Eclipse WindowBuilder may well 
	 * break other code, particularly the action listeners. Through this technique, the 
	 * WindowBuilder can be used to change the label on a GUI element (e.g., button) without
	 * breaking any other code. The command that a listener will receive is the same regardless
	 * of what the actual label on the GUI component says. So, for example, the text "Exit"
	 * be changed in the menu to "Quit" without breaking the menu's listener because the
	 * listener will receive the command exitCommand regardless of what text is actually
	 * displayed in the menu. Get methods below (e.g., getExitCommand) return the appropriate
	 * command for the listeners to use.
	 *==========================================================================================*/	
	/* define menu item commands */
	private static final String exitCommand = "exit";
	private static final String instructionsCommand = "instruct";
	private static final String aboutCommand = "about";
	private static final String timeConversionsCommand = "timeconversions";

	/* define button controls */
	private static final String calculateCommand = "calculate";

	/* define radio button commands */
	private static final String radbtnPSTCommand = "PST";
	private static final String radbtnMSTCommand = "MST";
	private static final String radbtnCSTCommand = "CST";
	private static final String radbtnESTCommand = "EST";
	private static final String radbtnLonCommand = "Longitude";
	private JPanel panelChkBoxesGrouping;

	/**
	 * Create the GUI frame.
	 */
	protected ChapGUI() {
		JPanel contentPane = new JPanel();
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChapGUI.class.getResource("/resources/BlueMarble.png")));
		setTitle(WINDOW_TITLE);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 920, 800);
		contentPane.setDoubleBuffered(false);
		contentPane.setFocusable(false);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		/*===================== Create Menus =====================*/
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(ASTStyle.MENU_FONT);
		setJMenuBar(menuBar);

		// File menu
		JMenu mnFile = new JMenu(" File ");
		mnFile.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setFont(ASTStyle.MENU_FONT);
		mntmExit.setActionCommand(exitCommand);
		mntmExit.addActionListener(listenerMenus);
		mnFile.add(mntmExit);

		// Time Conversions menu
		mnTimeConversions = new JMenu(" Time Conversions ");
		mnTimeConversions.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnTimeConversions);

		// Local vs Greenwich conversions menu
		mnLocalGreenwich = new JMenu(" Local vs Greenwich ");
		mnLocalGreenwich.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnLocalGreenwich);

		// This much simpler code will populate the previous 2 menus via the separate ChapMenuItems class,
		// but at the price of not being able to see the menu via WindowBuilder
		// No need to store the menu items created because the results of the "new" is never referenced elsewhere
		new ChapMenuItems();

		// Help menu
		JMenu mnHelp = new JMenu(" Help ");
		mnHelp.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnHelp);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setFont(ASTStyle.MENU_FONT);
		mntmInstructions.setActionCommand(instructionsCommand);
		mntmInstructions.addActionListener(listenerMenus);
		mnHelp.add(mntmInstructions);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setFont(ASTStyle.MENU_FONT);
		mntmAbout.addActionListener(listenerMenus);
		mntmAbout.setActionCommand(aboutCommand);
		mnHelp.add(mntmAbout);
		/*===================== End of Menus =====================*/		

		setContentPane(contentPane);

		JPanel panelBookTitle = new JPanel();
		panelBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		panelBookTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelBookTitle.setFocusable(false);
		panelBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.setDoubleBuffered(false);

		JScrollPane scrollPaneOutput = new JScrollPane();
		scrollPaneOutput.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setVerifyInputWhenFocusTarget(false);
		scrollPaneOutput.setRequestFocusEnabled(false);
		scrollPaneOutput.setFocusable(false);
		scrollPaneOutput.setFocusTraversalKeysEnabled(false);

		panelChkBoxesGrouping = new JPanel();

		JPanel panelTimeZoneGroupings = new JPanel();
		txtboxData1.setHorizontalAlignment(SwingConstants.RIGHT);

		txtboxData1.setFont(ASTStyle.TEXT_FONT);
		txtboxData1.setColumns(10);
		txtboxData2.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxData2.setFont(ASTStyle.TEXT_FONT);
		txtboxData2.setColumns(10);

		lblData1.setFont(ASTStyle.TEXT_FONT);
		lblData1.setHorizontalAlignment(SwingConstants.LEFT);
		lblData2.setFont(ASTStyle.TEXT_FONT);
		lblData2.setHorizontalAlignment(SwingConstants.LEFT);

		lblResults.setFont(ASTStyle.TEXT_BOLDFONT);

		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.setFont(ASTStyle.BTN_FONT);
		btnCalculate.addActionListener(listenerGUIBtns);
		btnCalculate.setActionCommand(calculateCommand);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBookTitle, GroupLayout.DEFAULT_SIZE, 807, Short.MAX_VALUE)
				.addComponent(panelChkBoxesGrouping, GroupLayout.DEFAULT_SIZE, 807, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addContainerGap()
						.addComponent(panelTimeZoneGroupings, GroupLayout.DEFAULT_SIZE, 783, Short.MAX_VALUE)
						.addContainerGap())
						.addGroup(gl_contentPane.createSequentialGroup()
								.addContainerGap()
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPane.createSequentialGroup()
												.addComponent(txtboxData1, GroupLayout.PREFERRED_SIZE, 281, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(lblData1, GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE))
												.addGroup(gl_contentPane.createSequentialGroup()
														.addComponent(txtboxData2, GroupLayout.PREFERRED_SIZE, 281, GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(ComponentPlacement.RELATED)
														.addComponent(lblData2, GroupLayout.DEFAULT_SIZE, 482, Short.MAX_VALUE)))
														.addGap(27))
														.addGroup(gl_contentPane.createSequentialGroup()
																.addContainerGap()
																.addComponent(lblResults, GroupLayout.DEFAULT_SIZE, 468, Short.MAX_VALUE)
																.addGap(228)
																.addComponent(btnCalculate)
																.addContainerGap())
																.addGroup(gl_contentPane.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 783, Short.MAX_VALUE)
																		.addContainerGap())
				);
		gl_contentPane.setVerticalGroup(
				gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addComponent(panelBookTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addGap(2)
						.addComponent(panelChkBoxesGrouping, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panelTimeZoneGroupings, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtboxData1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblData1))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
										.addComponent(txtboxData2, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblData2))
										.addPreferredGap(ComponentPlacement.RELATED)
										.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnCalculate)
												.addComponent(lblResults))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 447, Short.MAX_VALUE)
												.addContainerGap())
				);
		panelTimeZoneGroupings.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel panelTimeZones = new JPanel();
		panelTimeZones.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));

		JLabel lblNewLabel = new JLabel("Time Zone");
		lblNewLabel.setFont(ASTStyle.TEXT_BOLDFONT);

		radbtnPST = new JRadioButton("PST");
		radbtnPST.setFocusable(false);
		radbtnPST.setFocusPainted(false);
		radbtnPST.setFont(ASTStyle.RADBTN_FONT);
		radbtnPST.setActionCommand(radbtnPSTCommand);
		radbtnPST.addActionListener(listenerRadioBtns);

		radbtnMST = new JRadioButton("MST");
		radbtnMST.setFocusable(false);
		radbtnMST.setFocusPainted(false);
		radbtnMST.setFont(ASTStyle.RADBTN_FONT);
		radbtnMST.setActionCommand(radbtnMSTCommand);
		radbtnMST.addActionListener(listenerRadioBtns);

		radbtnCST = new JRadioButton("CST");
		radbtnCST.setFocusable(false);
		radbtnCST.setFont(ASTStyle.RADBTN_FONT);
		radbtnCST.setActionCommand(radbtnCSTCommand);
		radbtnCST.addActionListener(listenerRadioBtns);

		radbtnEST = new JRadioButton("EST");
		radbtnEST.setFocusable(false);
		radbtnEST.setFont(ASTStyle.RADBTN_FONT);
		radbtnEST.setActionCommand(radbtnESTCommand);
		radbtnEST.addActionListener(listenerRadioBtns);

		radbtnLon = new JRadioButton("Longitude");
		radbtnLon.setFocusable(false);
		radbtnLon.setFont(ASTStyle.RADBTN_FONT);
		radbtnLon.setActionCommand(radbtnLonCommand);
		radbtnLon.addActionListener(listenerRadioBtns);

		txtboxLon = new JTextField();
		txtboxLon.setFocusable(false);
		txtboxLon.setFocusTraversalKeysEnabled(false);
		txtboxLon.setHorizontalAlignment(SwingConstants.LEFT);
		txtboxLon.setFont(ASTStyle.TEXT_FONT);
		txtboxLon.setText("180d 30m 25.56sW");
		txtboxLon.setColumns(10);

		JLabel lblLonExample = new JLabel("(ex: 96.5E, 30.45W)");
		lblLonExample.setFont(ASTStyle.TEXT_FONT);
		GroupLayout gl_panelTimeZones = new GroupLayout(panelTimeZones);
		gl_panelTimeZones.setHorizontalGroup(
				gl_panelTimeZones.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelTimeZones.createSequentialGroup()
						.addGap(14)
						.addComponent(radbtnPST, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(radbtnMST, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
						.addComponent(radbtnCST, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
						.addGap(4)
						.addComponent(radbtnEST, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
						.addComponent(radbtnLon, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
						.addGap(3)
						.addComponent(txtboxLon, GroupLayout.PREFERRED_SIZE, 149, GroupLayout.PREFERRED_SIZE)
						.addGap(12)
						.addComponent(lblLonExample))
						.addGroup(Alignment.TRAILING, gl_panelTimeZones.createSequentialGroup()
								.addContainerGap(304, Short.MAX_VALUE)
								.addComponent(lblNewLabel)
								.addGap(300))
				);
		gl_panelTimeZones.setVerticalGroup(
				gl_panelTimeZones.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelTimeZones.createSequentialGroup()
						.addGap(5)
						.addComponent(lblNewLabel)
						.addGap(4)
						.addGroup(gl_panelTimeZones.createParallelGroup(Alignment.LEADING)
								.addComponent(radbtnPST)
								.addComponent(radbtnMST)
								.addComponent(radbtnCST)
								.addComponent(radbtnEST)
								.addComponent(radbtnLon)
								.addGroup(gl_panelTimeZones.createSequentialGroup()
										.addGap(3)
										.addComponent(txtboxLon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addGroup(gl_panelTimeZones.createSequentialGroup()
												.addGap(6)
												.addComponent(lblLonExample)))
												.addContainerGap())
				);
		panelTimeZones.setLayout(gl_panelTimeZones);
		panelTimeZoneGroupings.add(panelTimeZones);

		chkboxShowInterimCalcs.setSelected(false);
		chkboxShowInterimCalcs.setHorizontalAlignment(SwingConstants.CENTER);
		chkboxShowInterimCalcs.setAlignmentY(Component.TOP_ALIGNMENT);
		chkboxShowInterimCalcs.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkboxShowInterimCalcs.setFocusable(false);
		chkboxShowInterimCalcs.setFocusTraversalKeysEnabled(false);
		chkboxShowInterimCalcs.setFocusPainted(false);
		chkboxShowInterimCalcs.setFont(ASTStyle.CBOX_FONT);
		chkboxShowInterimCalcs.setSelected(false);

		chkboxDST.setSelected(false);
		chkboxDST.setHorizontalAlignment(SwingConstants.CENTER);
		chkboxDST.setAlignmentY(Component.TOP_ALIGNMENT);
		chkboxDST.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkboxDST.setFocusable(false);
		chkboxDST.setFocusTraversalKeysEnabled(false);
		chkboxDST.setFocusPainted(false);
		chkboxDST.setFont(ASTStyle.CBOX_FONT);
		chkboxDST.setSelected(false);

		JLabel lblCBoxSpacer = new JLabel("    ");
		lblCBoxSpacer.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelChkBoxesGrouping.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelChkBoxesGrouping.add(chkboxShowInterimCalcs);
		panelChkBoxesGrouping.add(lblCBoxSpacer);
		panelChkBoxesGrouping.add(chkboxDST);

		panelBookTitle.setLayout(new BorderLayout(0, 0));		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setVerifyInputWhenFocusTarget(false);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookTitle.setFont(ASTStyle.BOOK_TITLE_BOLDFONT);
		lblBookTitle.setFocusable(false);
		lblBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.add(lblBookTitle, BorderLayout.CENTER);
		contentPane.setLayout(gl_contentPane);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		// Define the actual text area and add it to scrollPaneOutput
		textPane.setFont(ASTStyle.OUT_TEXT_FONT);
		textPane.setFocusable(false);
		textPane.setFocusTraversalKeysEnabled(false);
		textPane.setFocusCycleRoot(false);
		textPane.setEditable(false);
		scrollPaneOutput.setViewportView(textPane);

		// Initialize the rest of the GUI
		ASTStyle.setASTStyle();
		aboutBox.pack();
		aboutBox.setParentFrame(this);		

		txtboxData1.setText("");
		lblData1.setText("");
		txtboxData2.setText("");
		lblData2.setText("");
		lblResults.setText("");
		txtboxLon.setText("");
		setLonRadBtn();
		calculationToDo = CalculationType.NO_CALC_SELECTED;

		// Set parent frame for static methods that need to center a pane/frame/etc. on the GUI
		ASTQuery.setParentFrame(this);
		ASTMsg.setParentFrame(this);

		textPane.setText("     "); // must initialize with at least a few blanks
		prt = new ASTPrt(textPane);
		
		// Explicitly indicate the order in which we want the GUI components traversed
		Vector<Component> order = new Vector<Component>(3);
		order.add(btnCalculate); order.add(txtboxData1); order.add(txtboxData2);
		ASTStyle.GUIFocusTraversalPolicy GUIPolicy = new ASTStyle.GUIFocusTraversalPolicy(order);
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setFocusTraversalPolicy(GUIPolicy);
	}

	/*=============================================================================
	 * The methods below return references to various GUI components such as the
	 * scrollable output text area. 
	 *============================================================================*/

	/**
	 * Gets the Local vs Greenwich menu.
	 * 
	 * @return		the Local vs Greenwich menu
	 */
	protected static JMenu getLocalGreenwichMenu() {
		return mnLocalGreenwich;
	}

	/**
	 * Gets the listener for the menu items
	 * 
	 * @return		the action listener for handling the menus
	 */
	protected static ActionListener getMenuListener() {
		return listenerMenus;
	}

	/**
	 * Gets the ASTPrt instance for this application's scrollable text pane area.
	 * 
	 * @return		the ASTPrt instance for this application
	 */
	protected static ASTPrt getPrtInstance() {
		return prt;
	}

	/**
	 * Gets the scrollable text pane area for this GUI.
	 * 
	 * @return		the JTextPane for this GUI
	 */
	protected static JTextPane getTextPane() {
		return textPane;
	}

	/**
	 * Gets the Time Conversions menu.
	 * 
	 * @return		the Time Conversions menu
	 */
	protected static JMenu getTimeConvMenu() {
		return mnTimeConversions;
	}

	/*=============================================================================
	 * The methods below are 'getters' and 'setters' for various items in the GUI.
	 *============================================================================*/

	/**
	 * Clears all of the text areas in the GUI, except Longitude
	 */
	protected static void clearAllTextAreas() {
		setDataLabels("","");
		prt.clearTextArea();	
		setResults("");
		// Must also clear the input data areas
		txtboxData1.setText("");
		txtboxData2.setText("");
	}

	/**
	 * Gets the string data associated with the 1st input area
	 * 
	 * @return		string from the 1st input area in the GUI
	 */
	protected static String getData1() {
		return txtboxData1.getText();
	}

	/**
	 * Gets the string data associated with the 2nd input area
	 * 
	 * @return		string from the 2nd input area in the GUI
	 */
	protected static String getData2() {
		return txtboxData2.getText();
	}

	/**
	 * Gets the string the user entered for the Longitude
	 * 
	 * @return		string for the longitude input area in the GUI
	 */
	protected static String getLongitudeData() {
		return txtboxLon.getText();
	}

	/**
	 * Gets the current status of the DST check box
	 * 
	 * @return		true if the DST check box is checked, else false
	 */
	protected static boolean getDSTStatus() {
		return chkboxDST.isSelected();
	}

	/**
	 * Gets the calculation to perform based upon the most recent menu selection.
	 * 
	 * @return			calculation to do
	 */
	protected static CalculationType getCalcToDo() {
		return calculationToDo;
	}

	/**
	 * Saves the calculation to perform based upon the just selected menu item
	 * 
	 * @param calcToDo		which calculation to perform
	 */
	protected static void setCalcToDo(CalculationType calcToDo) {
		calculationToDo = calcToDo;
	}

	/**
	 * Sets the data labels in the GUI
	 * 
	 * @param s1	string for the 1st data label in the GUI
	 */
	protected static void setDataLabels(String s1) {
		setDataLabels(s1,"");
	}

	/**
	 * Sets the data labels in the GUI
	 * 
	 * @param s1	string for the 1st data label in the GUI
	 * @param s2	string for the 2nd data label in the GUI
	 */
	protected static void setDataLabels(String s1,String s2) {
		lblData1.setText(s1);
		lblData2.setText(s2);
	}

	/**
	 * Sets the results label in the GUI
	 * 
	 * @param result	string for the results label in the GUI
	 */
	protected static void setResults(String result) {
		lblResults.setText(result);
	}

	/*-----------------------------------------------------------------------
	 * Handle the radio buttons. Make sure only 1 is selected at a time
	 -----------------------------------------------------------------------*/

	/**
	 * Clears all of the GUI radio buttons (i.e., sets them to unchecked). This
	 * routine is useful since Swing does not automatically manage radio buttons.
	 */
	private static void clearAllRadioBtns() {
		radbtnPST.setSelected(false);
		radbtnPST.setFocusPainted(false);
		radbtnPST.setFocusable(false);
		
		radbtnMST.setSelected(false);
		radbtnMST.setFocusPainted(false);
		radbtnMST.setFocusable(false);
		
		radbtnCST.setSelected(false);
		radbtnCST.setFocusPainted(false);
		radbtnCST.setFocusable(false);
		
		radbtnEST.setSelected(false);
		radbtnEST.setFocusPainted(false);
		radbtnEST.setFocusable(false);
		
		radbtnLon.setSelected(false);
		radbtnLon.setFocusPainted(false);
		radbtnLon.setFocusable(false);
		
		
		txtboxLon.setFocusable(false);
		txtboxLon.setFocusTraversalKeysEnabled(false);
		txtboxLon.requestFocus(false);
	}

	/**
	 * Determine what time zone radio button is selected
	 * 
	 * @return		a time zone type for whatever is the
	 *				currently selected time zone radio button
	 */
	protected static ASTLatLon.TimeZoneType getSelectedRBStatus() {
		if (radbtnPST.isSelected()) return ASTLatLon.TimeZoneType.PST;
		else if (radbtnMST.isSelected()) return ASTLatLon.TimeZoneType.MST;
		else if (radbtnCST.isSelected()) return ASTLatLon.TimeZoneType.CST;
		else if (radbtnEST.isSelected()) return ASTLatLon.TimeZoneType.EST;
		else return ASTLatLon.TimeZoneType.LONGITUDE;
	}

	/**
	 * Gets the command that represents the PST radio button.
	 * 
	 * @return			PST radio button command
	 */
	protected static String getPSTCommand() {
		return radbtnPSTCommand;
	}

	/**
	 * Gets the command that represents the MST radio button.
	 * 
	 * @return			MST radio button command
	 */
	protected static String getMSTCommand() {
		return radbtnMSTCommand;
	}

	/**
	 * Gets the command that represents the CST radio button.
	 * 
	 * @return			CST radio button command
	 */
	protected static String getCSTCommand() {
		return radbtnCSTCommand;
	}

	/**
	 * Gets the command that represents the EST radio button.
	 * 
	 * @return			EST radio button command
	 */
	protected static String getESTCommand() {
		return radbtnESTCommand;
	}

	/**
	 * Gets the command that represents the Longitude radio button.
	 * 
	 * @return			Longitude radio button command
	 */
	protected static String getLongitudeCommand() {
		return radbtnLonCommand;
	}

	/**
	 * Sets the PST radio button to true
	 */
	protected static void setPSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnPST.setSelected(true);
	}

	/**
	 * Sets the MST radio button to true
	 */
	protected static void setMSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnMST.setSelected(true);
	}

	/**
	 * Sets the CST radio button to true
	 */
	protected static void setCSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnCST.setSelected(true);
	}

	/**
	 * Sets the EST radio button to true
	 */
	protected static void setESTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnEST.setSelected(true);
	}

	/**
	 * Sets the Longitude radio button to true
	 */
	protected static void setLonRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		txtboxLon.setText("");
		txtboxLon.setFocusable(true);
		txtboxLon.setFocusTraversalKeysEnabled(true);
		txtboxLon.requestFocus(true);
		radbtnLon.setSelected(true);
	}

	/*===============================================================================
	 * The methods below return the command associated with a specific menu or
	 * GUI button. This is done to separate the actual string in the GUI that
	 * represents an action (e.g., "Exit") from the semantics of the action that is
	 * to occur.
	 *==============================================================================*/
	
	/**
	 * Gets the command that represents the Exit menu item.
	 * 
	 * @return			Exit command
	 */
	protected static String getExitCommand() {
		return exitCommand;
	}

	/**
	 * Gets the command that represents the time conversion menu items.
	 * 
	 * @return			Time Conversions command
	 */
	protected static String getTimeConversionsCommand() {
		return 	timeConversionsCommand;
	}

	/**
	 * Gets the command that represents the Instructions menu item.
	 * 
	 * @return			Instructions command
	 */
	protected static String getInstructionsCommand() {
		return instructionsCommand;
	}

	/**
	 * Gets the command that represents the About menu item.
	 * 
	 * @return			About command
	 */
	protected static String getAboutCommand() {
		return aboutCommand;
	}

	/*=========================
	 * AboutBox methods
	 *========================*/

	/**
	 * Shows the About Box
	 */
	protected static void showAboutBox() {
		aboutBox.showAboutBox();
	}

	/*======================================================================
	 * The methods below conditionally print to the scrollable text area
	 *=====================================================================*/

	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.print(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 */
	protected static void printlnCond() {
		if (chkboxShowInterimCalcs.isSelected()) prt.println();
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printlnCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 * @param centerTxt		true if the text is to be centered
	 */
	protected static void printlnCond(String txt, boolean centerTxt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt,centerTxt);
	}
}
