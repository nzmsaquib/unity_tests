package jll.celcalc.chap3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;

/**
 * <b>Implements an action listener for GUI buttons.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class GUIButtonsListener implements ActionListener {

	private static GUIBtnActions ba = new GUIBtnActions();

	/**
	 * Handles clicks on individual GUI buttons.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */	
	public void actionPerformed(ActionEvent e) {

		switch (ChapGUI.getCalcToDo()) {
		//**** Time Conversions
		case LEAP_YEARS:
			ba.calcLeapYear();
			break;
		case CALENDAR_2_JD:
			ba.calcCalendar2JD();
			break;
		case JD_2_CALENDAR:
			ba.calcJD2Calendar();
			break;
		case DAY_OF_WEEK:
			ba.calcDayOfWeek();
			break;
		case DATE_2_DAYS_INTO_YEAR:
			ba.calcDaysIntoYear();
			break;
		case DAYS_INTO_YEAR_2_DATE:
			ba.calcDaysIntoYear2Date();
			break;

			//**** Local vs Greenwich
		case LCT_2_UT:
			ba.calcLCT2UT();
			break;
		case UT_2_LCT:
			ba.calcUT2LCT();
			break;
		case UT_2_GST:
			ba.calcUT2GST();
			break;
		case GST_2_UT:
			ba.calcGST2UT();
			break;
		case GST_2_LST:
			ba.calcGST2LST();
			break;
		case LST_2_GST:
			ba.calcLST2GST();
			break;
		default:
			ASTMsg.errMsg("No menu item has been selected. Choose a menu item from\n" +
					"'Time Conversions' or 'Local vs Greenwich' and  try again.", "No Menu Item Selected");		
			break;
		}
	}
}
