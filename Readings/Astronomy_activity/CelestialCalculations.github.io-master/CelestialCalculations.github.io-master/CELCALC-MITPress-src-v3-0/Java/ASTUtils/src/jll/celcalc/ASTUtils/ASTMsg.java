package jll.celcalc.ASTUtils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * <b>Methods to display messages to the user.</b> 
 * <p>
 * These methods are static so that a class instance does not have to be 
 * created prior to usage. These methods simplify translating the code to other 
 * languages, and they help enforce consistency in usage and style.
 * <p>
 * These message methods display a message in a dialog box that is centered
 * relative to a parent frame that was set by the <i><code>setParentFrame(JFrame)</code></i>
 * method. The methods in this class also allow a calling routine to explicitly define a 
 * parent frame on which to center the message dialog boxes.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTMsg {
	// Save a reference to the application's parent so that we can center messages w.r.t. the parent frame
	// without having to pass in the parent frame each time.
	private static JFrame parentFrame = null;

	/**
	 * Saves a reference to the caller's parent frame so error messages can be centered on the 
	 * parent application's window.
	 * 
	 * @param parent		parent frame for the main application
	 */
	public static void setParentFrame(JFrame parent) {
		parentFrame = parent;
	}

	/*================================================================================
	 * Define some general methods for displaying various types of error messages.
	 * Messages are centered on the parent frame that is passed in. If no parent
	 * frame is passed in, parentFrame is used instead, which should be set to the
	 * main application frame.
	 *===============================================================================*/

	/**
	 * Asks the user if they wish to abort, and centers the dialog on the 
	 * frame passed in
	 * 
	 * This method is overloaded.
	 * 
	 * @param msg		text message to display as a prompt
	 * @param frame		frame on which to center the message dialog
	 * @return			true if user clicked on "Yes", else false 
	 */
	public static boolean abortMsg(String msg,JFrame frame) {
		return pleaseConfirm(msg +"\n"+"Abort processing?","",frame);
	}
	/**
	 * Asks the user if they wish to abort, and centers the dialog on the 
	 * frame passed in
	 * 
	 * This method is overloaded.
	 * 
	 * @param msg		text message to display as a prompt
	 * @return			true if user clicked on "Yes", else false 
	 */
	public static boolean abortMsg(String msg) {
		return abortMsg(msg,parentFrame);
	}	

	/**
	 * Display a critical error message centered on parentFrame
	 * 
	 * This method is overloaded.
	 * 
	 * @param msg		text of the message to display
	 * @param abort		whether to abort after message is displayed. Default is no.
	 */
	public static void criticalErrMsg(String msg,boolean abort) {
		JOptionPane.showMessageDialog(parentFrame,"Severe Error: "+msg,"Severe Error",JOptionPane.ERROR_MESSAGE);
		if (abort==ASTMisc.ABORT_PROG) System.exit(-1);
	}
	/**
	 * Display a critical error message centered on parentFrame
	 * 
	 * This method is overloaded.
	 * 
	 * @param msg		text of the message to display
	 */
	public static void criticalErrMsg(String msg) {
		criticalErrMsg(msg,false);
	}

	/**
	 * Conditionally displays an error message, but only if errFlag is SHOW_ERRORS.
	 * 
	 * This method is overloaded to allow errFlag to be optional. The default
	 * is to show errors
	 * 
	 * @param msg		text of the error message to be displayed
	 * @param title		text for the dialog window's title
	 * @param flag		display error message only if flag is ASTMisc.SHOW_ERRORS
	 */
	public static void errMsg(String msg,String title,boolean flag) {
		if (flag == ASTMisc.SHOW_ERRORS) errMsg(msg,title);
	}
	/**
	 * Conditionally displays an error message, but only if errFlag is SHOW_ERRORS.
	 * 
	 * This method is overloaded to allow errFlag to be optional. The default
	 * is to show errors
	 * 
	 * @param msg		text of the error message to be displayed
	 * @param title		text for the dialog window's title
	 */
	public static void errMsg(String msg,String title) {
		errMsg(msg,title,parentFrame);
	}
	/**
	 * Conditionally displays an error message, but only if errFlag is SHOW_ERRORS.
	 * 
	 * This method is overloaded to allow errFlag to be optional. The default
	 * is to show errors
	 * 
	 * @param msg		text of the error message to be displayed
	 * @param title		text for the dialog window's title
	 * @param frame		frame on which to center the message dialog
	 */
	public static void errMsg(String msg,String title,JFrame frame) {
		JOptionPane.showMessageDialog(frame,msg,title,JOptionPane.WARNING_MESSAGE);
	}
	/**
	 * Conditionally displays an error message, but only if errFlag is SHOW_ERRORS.
	 * 
	 * This method is overloaded to allow errFlag to be optional. The default
	 * is to show errors
	 * 
	 * @param msg		text of the error message to be displayed
	 * @param title		text for the dialog window's title
	 * @param panel		panel on which to center the message dialog
	 */
	public static void errMsg(String msg,String title,JPanel panel) {
		JOptionPane.showMessageDialog(panel,msg,title,JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Display a simple informational message.
	 * 
	 * This method is overloaded.
	 *
	 * @param msg		text message to display
	 */
	public static void infoMsg(String msg) {
		infoMsg(msg," ");
	}
	/**
	 * Display a simple informational message.
	 * 
	 * This method is overloaded.
	 *
	 * @param msg		text message to display
	 * @param title		title for the message window
	 */
	public static void infoMsg(String msg,String title) {
		JOptionPane.showMessageDialog(parentFrame, msg,title,JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Asks the user to confirm something (such as whether to exit), and centers the dialog
	 * on the frame passed in. If no frame is passed in, parentFrame is used.
	 * 
	 * This method is overloaded.
	 * 
	 * @param msg		text of the message to display
	 * @param title		text for the dialog window's title
	 * @param frame		frame on which to center the message dialog. If not given, use parentFrame
	 * @return			true if user clicked on "Yes", else false
	 */
	public static boolean pleaseConfirm(String msg,String title,JFrame frame) {
		if (frame == null) return pleaseConfirm(msg,title);
		else return (JOptionPane.showConfirmDialog(frame,msg,title,JOptionPane.YES_NO_OPTION) == JOptionPane.OK_OPTION);
	}
	/**
	 * Asks the user to confirm something (such as whether to exit), and centers the dialog
	 * on the frame passed in. If no frame is passed in, parentFrame is used.
	 * 
	 * This method is overloaded.
	 * 
	 * @param msg		text of the message to display
	 * @param title		text for the dialog window's title
	 * @return			true if user clicked on "Yes", else false
	 */
	public static boolean pleaseConfirm(String msg,String title) {
		return pleaseConfirm(msg,title,parentFrame);
	}

}
