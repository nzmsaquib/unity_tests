package jll.celcalc.ASTUtils;

/**
 * <b>This implements a simple class for handling vectors with
 * 3 elements.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTVect {

	// Define a vector as x, y, z elements plus its length
	private double Rx;
	private double Ry;
	private double Rz;
	private double Rmag;

	/**
	 * Constructors for creating vector objects.
	 * 
	 * This method is overloaded.
	 */
	public ASTVect() {
		this.Rx = 0.0;
		this.Ry = 0.0;
		this.Rz = 0.0;
		this.Rmag = 0.0;
	}
	/**
	 * Constructors for creating vector objects.
	 * 
	 * This method is overloaded.
	 * 
	 * @param x			x value for the vector
	 * @param y			y value for the vector
	 * @param z			z value for the vector
	 */
	public ASTVect(double x,double y,double z) {
		this.Rx = x;;
		this.Ry = y;
		this.Rz = z;
		this.Rmag = vectLen(x,y,z);
	}
	
	/*====================================================================
	 * Define 'get' and 'set' accessors for the object's fields
	 *===================================================================*/
	
	/**
	 * Get the x value of the vector.
	 * 
	 * @return		x value
	 */
	public double x() {
		return this.Rx;
	}
	/**
	 * Get the y value of the vector.
	 * 
	 * @return		y value
	 */
	public double y() {
		return this.Ry;
	}
	/**
	 * Get the z value of the vector.
	 * 
	 * @return		z value
	 */
	public double z() {
		return this.Rz;
	}
	/**
	 * Get the vector's length.
	 * 
	 * @return		length of the vector
	 */
	public double len() {
		return this.Rmag;
	}
	/**
	 * Set the components of a vector.
	 * 
	 * @param x		x value to set
	 * @param y		y value to set
	 * @param z		z value to set
	 */
	public void setVect(double x,double y,double z) {
		this.Rx = x;
		this.Ry = y;
		this.Rz = z;
		this.Rmag = vectLen(x,y,z);
	}

	/**
	 * Calculate the length/magnitude of a vector
	 * 
	 * @param x			vector's x value
	 * @param y			vector's y value
	 * @param z			vector's z value
	 * @return			the vector's length
	 */
	public static double vectLen(double x,double y,double z) {
		return Math.sqrt(x*x + y*y + z*z);
	}

}

