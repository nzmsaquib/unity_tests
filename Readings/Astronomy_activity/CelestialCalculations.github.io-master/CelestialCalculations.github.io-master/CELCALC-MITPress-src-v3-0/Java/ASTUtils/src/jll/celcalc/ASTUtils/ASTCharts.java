package jll.celcalc.ASTUtils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

/**
 * <b>Provides a class for creating star charts.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
public class ASTCharts extends JComponent {

	/**-------------------------------------- Graphics Notes --------------------------------------------
	 * This class uses the Java Graphics2D package, which requires that a component's 'paint' method
	 * (or paintComponent method) be overridden and all graphics commands performed inside the overridden
	 * method. This approach is not particularly convenient for our application because the 'paint'
	 * method would be needlessly complex when drawing star charts. So, the strategy this class
	 * takes is to draw to a buffered image and then have the overridden paint method merely copy the
	 * buffered image to the screen. This greatly simplifies the overridden method and allows draw
	 * commands to be performed (sort of!) outside the overridden method.
	 * 
	 * Java provides multiple methods for manipulating graphics via matrix transformations and various
	 * methods such as transform and translate. Note that however the transformations are accomplished,
	 * they affect all graphical objects, including text that is drawn via Java's drawString method. Thus,
	 * if one does a rotation to a line or rectangle, text will be rotated as well. For our purposes, rotating
	 * the text is undesirable and we must implement our own drawString method to account for this.
	 * 
	 * Since Java Graphics2D assumes that the coordinate (0,0) is the top left point on the drawing surface,
	 * it is convenient to do a transformation to move the point (0,0) to be the bottom left point on the
	 * drawing surface. Although this can be done with built-in Graphics2D methods for transformations, it
	 * is easier in our case to do the simple transformations we need through affine transformation matrices.
	 * This makes it easier to translate the code to other languages that may not have all the methods Graphics2D
	 * provides, but also makes it easier to undo how various transformations impact graphical text objects.
	 * 
     * The method setWorldCoordSysOrigin below moves the coordinate system origin from the screen's
     * upper left corner to the bottom left corner. It does so with an affine transformation
     * matrix that is equivalent to a translate(0,yMax) transformation followed by a scale(1,-1)
     * transformation. Both transformations are required because while the translate transformation
     * moves the coordinate system origin, it does **not** change the direction of the positive
     * y-axis. The scale transformation Is required to make the y-axis increase from bottom to
     * top in the coordinate system. Changing the direction of the y-axis could be done with
     * a rotation transformation of 90 degrees, but the equivalent scale(1,-1) is done instead
     * to avoid the need to use sine/cosine calculations to do the transformation.	 * 
	 * 
	 * It is also a simple matter to do a transformation that maps the range for a coordinate, such as right
	 * ascension and declination, to the width/height of the drawing surface. However, this requires a scaling
	 * transformation that will stretch and distort text and make it more difficult to draw circles that will
	 * appear on the screen as circles. Hence, excepting the aforementioned scale(1,-1) transformation, we will
	 * avoid scaling transformations so that we will not to have to worry about how graphics objects, such as text,
	 * are stretched as a result of a scaling transformation.
	 * 
	 * Note that our drawing surface will be the same area in the GUI that is used for displaying text, which is
	 * given to this class by the GUI when a chart object is created.
	 *--------------------------------------- Graphics Notes --------------------------------------------*/

	// Unicode symbol for degrees
	private static final String DEGREE  = "\u00b0";

	// imgBuffer will be our drawing surface. It is overlaid on top of the drawingCanvas component. The
	// actual size of our drawing surface will be computed later and will be 0,0 to canvasWidth, canvasHeight.
	private BufferedImage imgBuffer = null;
	private JComponent drawingCanvas = null;
	private int canvasWidth = 100, canvasHeight = 100;	// This is computed later. For now, just initialize it.
	private Graphics2D gc = null; 						// a graphics context for drawing on our surface

	// Types of star charts that can be drawn
	/** Draw equatorial coordinates chart */
	public static final boolean EQ_CHART = true;
	/** Draw horizon coordinates chart */
	public static final boolean HORIZ_CHART = !EQ_CHART;
	/** Draw white background for chart */
	public static final boolean WHITE_BKG = true;
	/** Draw black background for chart */
	public static final boolean BLACK_BKG = !WHITE_BKG;

	// Define default pens and drawing backgrounds. Some graphics packages distinguish
	// between a pen and a brush, but Java Graphics2D does not. Graphics2D does
	// the equivalent of a brush through the BasicStroke class, which allows a pen thickness
	// to be set. We will not need to change a pen's thickness, so we'll not create any brushes.
	private static final Color whitePen = Color.white;
	private static final Color blackPen = Color.black;
	private static final Color whiteBKG = Color.white;
	private static final Color blackBKG = Color.black;

	// Define the actual colors to use
	private Color currentPen = blackPen;
	private Color currentBKG = whiteBKG;

	// Define font to be used for any text displayed on a chart
	private static final Font canvasFont = new Font("Arial", Font.BOLD, 14);

	// We need scaling factors to allow converting an RA/Decl pair to
	// the x/y coordinates in the clipping rectangle.
	private double xScale = 1.0, yScale = 1.0;

	// For a horizon coordinates chart, plot a circle centered in the display
	// area. The circle will be of size horizonRadius, where the radius is adjusted
	// based on the actual canvas width and height and adjusted to allow compass directions
	// to be displayed outside the plotting circle.
	private double horizonRadius, centerX, centerY;

	/**
	 * This constructor creates a Star Chart instance and saves the component that
	 * the calling routine sent to be used as the drawing canvas.
	 * 
	 * @param canvas	area in GUI to use for a drawing canvas
	 */
	public ASTCharts(JComponent canvas) {
		// Since we must create an ASTCharts object instance to use this class
		// and that can only be done via a new operation, there's no need to
		// check drawingCanvas for null anywhere in the rest of this class

		if (canvas == null)
			ASTMsg.criticalErrMsg("The drawing canvas for star charts\n" + "cannot be null. Aborting program ...",
					ASTMisc.ABORT_PROG);

		this.drawingCanvas = canvas;
		canvas.add(this);
	}

	/**
	 * This method renders the buffered image to the screen in the screen area
	 * given when an instance of this class was created.
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// Skip everything if this component is not currently visible
		if (!this.isVisible()) return;
		if (imgBuffer != null) g.drawImage(imgBuffer, 0, 0, null);
	}

	/**
	 * This method labels an object located at Alt, Az.
	 * 
	 * @param txt			label to display
	 * @param Alt			altitude for the object
	 * @param Az			azimuth for the object
	 * @param penColor		color to use for the text
	 * 
	 * Note: This method does not attempt to draw
	 * a label if the coordinates are below the
	 * observer's horizon.
	 */
	public void drawLabelAltAz(String txt,double Alt, double Az, Color penColor) {
		Point2D pt = AltAz2XY(Alt,Az);
		Rectangle2D bounds = getStrBounds(txt,gc);
		double x,y;
		
		// if object is below the horizon, no need to try to label it
		if (Alt < 0.0) return;
		
		y = pt.getY() - (bounds.getHeight()/2.0) - 4;	// pad a little to make it look better
		x = pt.getX() - (bounds.getWidth()/2.0);
		drawString(txt, x, y, penColor);
	}
	
	/**
	 * This method labels an object located at RA, Decl.
	 * 
	 * @param txt			label to display
	 * @param RA			right ascension for the object
	 * @param Decl			declination for the object
	 * @param penColor		color to use for the text
	 */
	public void drawLabelRADecl(String txt, double RA, double Decl, Color penColor) {
		Point2D pt = RADecl2XY(RA,Decl);
		Rectangle2D bounds = getStrBounds(txt,gc);
		double x,y;

		y = pt.getY() - (bounds.getHeight()/2.0) - 4;	// pad a little to make it look better
		x = pt.getX() + (bounds.getWidth()/2.0);      // add because x decreases going left to right
		drawString(txt, x, y, penColor);
	}

	/**
	 * Initialize a drawing canvas area for a star chart.
	 * 
	 * @param d					size of the drawing canvas area
	 * @param bkgWhite			WHITE_BKG if user wants a white background, else
	 * 							use a black background
	 * @param eqChart			EQ_CHART if doing an equatorial coordinates chart,
	 * 							otherwise do a horizon coordinates chart
	 * @return					graphics context for drawing on the canvas
	 * 
	 * Routines outside this class shouldn't be drawing to the canvas,
	 * but a graphics context is returned in case they want to. Be
	 * forewarned that transformations are applied in this class that
	 * will affect any drawing to the canvas done outside this class.
	 */
	public Graphics2D initDrawingCanvas(Dimension d, boolean bkgWhite, boolean eqChart) {
		// If there's an old buffer and graphics context hanging around, get rid
		// of them before we create a new one
		this.releaseChart();
		imgBuffer = new BufferedImage((int) d.getWidth(), (int) d.getHeight(), BufferedImage.TYPE_INT_RGB);
		gc = imgBuffer.createGraphics();

		// Use anti-aliasing to make lines look better
		gc.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		// Remove the old star chart from the GUI before adding this new one.
		// This is needed to avoid continuously adding defunct charts to the GUI
		// and thus eating up memory.
		drawingCanvas.remove(this);
		this.setSize(d);
		drawingCanvas.add(this);

		// set background color and pen color
		if (bkgWhite) {
			currentBKG = whiteBKG;
			currentPen = blackPen;
		} else {
			currentBKG = blackBKG;
			currentPen = whitePen;
		}

		gc.setColor(currentBKG);
		gc.fillRect(0, 0, (int) d.getWidth(), (int) d.getHeight());
		gc.setFont(canvasFont);
		gc.setColor(currentPen);
		this.setVisible(true);

		setWorldCoordSysOrigin();

		if (eqChart) initEQCanvas();
		else initHorizonCanvas();

		return gc;
	}

	/**
	 * Plot an object, given its horizon coordinates, on a
	 * circular plotting surface.
	 * 
	 * This method is overloaded.
	 *
	 * Note: This method does not attempt to plot any
	 * object that is below the observer's horizon.
	 * 
	 * @param Alt			altitude to plot
	 * @param Az			azimuth to plot
	 * @param mV			visual magnitude of the object
	 */
	public void plotAltAz(double Alt,double Az, double mV) {
		Point2D pt;

		// If object is below the horizon, no need to try to plot it
		if (Alt < 0.0) return;

		pt = AltAz2XY(Alt,Az);
		drawFilledCircle(pt.getX(),pt.getY(),mVtoRadius(mV));
	}
	/**
	 * Plot an object, given its horizon coordinates, on a
	 * circular plotting surface.
	 * 
	 * This method is overloaded.
	 *
	 * Note: This method does not attempt to plot any
	 * object that is below the observer's horizon.
	 * 
	 * @param Alt				altitude to plot
	 * @param Az				azimuth to plot
	 * @param mV				visual magnitude of the object
	 * @param penColor			color to use for plotting
	 */
	public void plotAltAz(double Alt,double Az, double mV, Color penColor) {
		Color saveColor = gc.getColor();

		// If object is below the horizon, no need to try to plot it
		if (Alt < 0.0) return;
		gc.setColor(penColor);
		plotAltAz(Alt,Az,mV);
		gc.setColor(saveColor);
	}

	/**
	 * Plot an object, given its equatorial coordinates, on
	 * a rectangular plotting surface.
	 * 
	 * This method is overloaded.
	 * 
	 * @param RA				right ascension to plot
	 * @param Decl				declination to plot
	 * @param mV				visual magnitude of the object
	 * @param penColor			color to use for plotting
	 */
	public void plotRADecl(double RA, double Decl, double mV, Color penColor) {
		Color saveColor = gc.getColor();

		gc.setColor(penColor);
		plotRADecl(RA,Decl,mV);
		gc.setColor(saveColor);		
	}
	/**
	 * Plot an object, given its equatorial coordinates, on
	 * a rectangular plotting surface.
	 * 
	 * This method is overloaded.
	 * 
	 * @param RA				right ascension to plot
	 * @param Decl				declination to plot
	 * @param mV				visual magnitude of the object
	 */
	public void plotRADecl(double RA, double Decl, double mV) {
		drawFilledCircle(RA2X(RA),Decl2Y(Decl),mVtoRadius(mV));
	}

	/**
	 * When a star chart is no longer to be displayed, this method releases all
	 * resources associated with the star chart to avoid memory leaks.
	 */
	public void releaseChart() {
		if (imgBuffer != null)
			imgBuffer.flush();
		if (gc != null)
			gc.dispose();
		gc = null;
		this.setVisible(false);
	}

	/**
	 * Refresh a star chart. This method is provided to allow an external caller
	 * to request the current state of the chart be displayed to show the user
	 * progress as the star chart is being created.
	 */
	public void refreshChart() {
		this.repaint();
	}

	/*--------------------------------------------------------------------------------------------------
	 * Private routines used only in this class
	 *-------------------------------------------------------------------------------------------------*/

	/**
	 * Convert Alt and Az to the appropriate x,y coordinates for
	 * plotting. This assumes that the x,y coordinates are for
	 * a circular plotting surface.
	 * 
	 * @param Alt, Az		horizon coordinates to convert
	 * @return				point structure containing the converted
	 * 						coordinates.
	 */
	private Point2D AltAz2XY(double Alt, double Az) {
		Point2D pt = new Point2D.Double(0.0, 0.0);
		double x,y;

		x = ASTMath.COS_D(Alt) * ASTMath.SIN_D(Az);
		y = ASTMath.COS_D(Alt) * ASTMath.COS_D(Az);

		pt.setLocation(x * horizonRadius + centerX, y * horizonRadius + centerY);
		return pt;
	}

	/**
	 * Convert RA and Decl to the appropriate x,y coordinates for
	 * plotting. This assumes that the x,y coordinates are for
	 * a rectangular plotting surface.
	 * 
	 * @param RA			RA to convert to an coordinate
	 * @param Decl			Decl to convert to a y coordinate
	 * @return				x, y, or a point structure depending upon which
	 * 						of the overloaded methods were invoked.
	 */
	private double Decl2Y(double Decl) {
		return Decl*yScale;
	}
	private double RA2X(double RA) {
		return RA*xScale;
	}
	private Point2D RADecl2XY(double RA, double Decl) {
		Point2D pt = new Point2D.Double(0.0, 0.0);

		pt.setLocation(RA2X(RA),Decl2Y(Decl));
		return pt;
	}

	/**
	 * Draw a circle in the current or user-specified color.
	 * 
	 * @param x,y			coordinates for the center of the circle
	 * @param radius		radius of the circle to draw
	 * @param penColor		color to use for outlined circle
	 */
	private void drawCircle(double x, double y, double radius) {
		// Java Graphics2D draws ellipses by doing a bounding box
		// described by left, top, width, height coordinates
		Ellipse2D circle = new Ellipse2D.Double(x - radius, y - radius, 2 * radius, 2 * radius);
		gc.draw(circle);
	}
	@SuppressWarnings("unused")
	private void drawCircle(double x, double y, double radius, Color penColor) {
		Color saveColor = gc.getColor();

		gc.setColor(penColor);
		drawCircle(x, y, radius);
		gc.setColor(saveColor);
	}

	/**
	 * Draw a filled circle in the current or user-specified color.
	 * 
	 * @param x,y			coordinates for the center of the circle
	 * @param radius		radius of the circle to draw
	 * @param penColor		color to use for outlined circle
	 */
	private void drawFilledCircle(double x, double y, double radius) {
		// Java Graphics2D draws ellipses by doing a bounding box
		// described by left, top, width, height coordinates
		Ellipse2D circle = new Ellipse2D.Double(x - radius, y - radius, 2 * radius, 2 * radius);
		gc.fill(circle);
	}
	@SuppressWarnings("unused")
	private void drawFilledCircle(double x, double y, double radius, Color penColor) {
		Color saveColor = gc.getColor();

		gc.setColor(penColor);
		drawFilledCircle(x, y, radius);
		gc.setColor(saveColor);
	}

	/**
	 * This method displays a string in graphics mode. Java Graphics2D has a
	 * drawString method, but that method is affected by any transformations
	 * that are done on the graphics surface (see setWorldCoordSysOrigin). The
	 * method implemented here does the equivalent of the Java drawString
	 * method, but is not impacted by prior transformations done in
	 * setWorldCoordSysOrigin. The string will be displayed such that the top
	 * left corner of the string will be at position x,y.
	 * 
	 * @param txt			text string to display
	 * @param x, y			coordinates for where the string is to be displayed relative
	 * 						to the current transformations in the graphics object
	 * @param fnt			font to use for displaying the string. If not specified, use
	 * 						the current font.
	 * @param bColor		pen color to use to display the text. If not specified, use
	 * 						the current pen color
	 */
	@SuppressWarnings("unused")
	private void drawString(String txt, double x, double y, Font fnt, Color bColor) {
		Color saveColor = gc.getColor();
		Font saveFont = gc.getFont();

		gc.setFont(fnt);
		gc.setColor(bColor);
		drawString(txt, x, y);
		gc.setColor(saveColor);
		gc.setFont(saveFont);
	}
	private void drawString(String txt, double x, double y, Color bColor) {
		Color saveColor = gc.getColor();

		gc.setColor(bColor);
		drawString(txt, x, y);
		gc.setColor(saveColor);
	}
	private void drawString(String txt, double x, double y) {
		// The strategy for implementing this method consists of 5 steps:

		// 1. Save the current transformation so that it can be restored
		// when we're all done and create an identity transformation.
		AffineTransform saveXform = gc.getTransform();
		AffineTransform xform = new AffineTransform();
		xform.setToIdentity();

		// 2. Figure out where (x,y) would be if there were no transformations
		// being applied and call that point (xp,yp). (xp,yp) is the equivalent
		// of (x,y) in untransformed space.
		Point2D xy = new Point2D.Double(x, y);
		Point2D xpyp = new Point2D.Double();
		saveXform.transform(xy, xpyp);

		// 3. Figure out the height of the input string
		double yHeight = this.getStrBounds(txt, gc).getHeight();

		// 4. Set to untransformed space and draw the string
		gc.setTransform(xform);
		gc.drawString(txt, (int) xpyp.getX(), (int) (xpyp.getY() + yHeight));

		// 5. Restore original transformation
		gc.setTransform(saveXform);
	}

	/**
	 * Calculate a bounding box for a string of text.
	 * 
	 * @param str		string of text
	 * @param g			graphics context
	 * @return 			a bounding rectangle for the input string
	 */
	private Rectangle2D getStrBounds(String str, Graphics2D g) {
		FontRenderContext frc = g.getFontRenderContext();
		GlyphVector gv = g.getFont().createGlyphVector(frc, str);

		return gv.getPixelBounds(null, 0, 0);
	}

	/**
	 * Initialize the drawing canvas for a star chart based on equatorial
	 * coordinates. The drawing area will be a rectangle with the RA on the
	 * y-axis and the Decl plotted on the x-axis. RA decreases from 24h on the
	 * left of the display to 0h on the right. Decl increases from -180 degrees
	 * at the bottom to 180 degrees at the top. The size of the plotting
	 * rectangle is adjusted to allow RA/Decl marks and labels on the plotting
	 * rectangle boundary.
	 */
	private void initEQCanvas() {
		Rectangle2D.Double clippingRect = new Rectangle2D.Double();
		int xPad = 4, yPad = 3;
		int xTicLen = 5, yTicLen = 5;
		int x,y;
		String st;

		// Figure out how much to adjust the plotting rectangle to allow
		// tic marks and RA/Decl labels outside the plotting rectangle.
		Rectangle2D RAFontSize = this.getStrBounds("00h", gc);
		Rectangle2D DeclFontSize = this.getStrBounds("+180"+DEGREE,gc);
		int RAFontWidth = (int) RAFontSize.getWidth();
		int RAFontHeight = (int) RAFontSize.getHeight();
		int DeclFontWidth = (int) DeclFontSize.getWidth();
		int DeclFontHeight = (int) DeclFontSize.getHeight();

		// Initially set the plotting rectangle relative to the current origin, which
		// is left bottom. This will be adjusted later to be an actual clipping rectangle.
		clippingRect.x = DeclFontWidth + xPad;
		clippingRect.width = canvasWidth - clippingRect.x;
		clippingRect.y = RAFontHeight + yPad + yTicLen;
		clippingRect.height = canvasHeight - clippingRect.y;

		xScale = clippingRect.width/24.0;       // RA goes from 0 to 24h
		yScale = clippingRect.height/360.0;		// Decl goes from -180 to 180 deg

		gc.setStroke(new BasicStroke(2));
		gc.draw(clippingRect);
		gc.setStroke(new BasicStroke(1));

		// At this point, the coordinate system origin (0,0) is the bottom left
		// of the plotting surface. We need to move the origin so that it is in
		// the plotting rectangle just drawn and set the x/y limits to be fully
		// within the plotting rectangle. Also, because we're plotting RA on the
		// x-axis and Decl on the y-axis, it would be useful to do a scaling
		// transformation to map the x range to be [24.0, 0.0] and y to be
		// [-180.0, 180.0] to match RA and Decl ranges. However, a scaling
		// transformation would also scale text for the RA/Decl labels and the
		// circles used to plot stars. So, we'll settle for just (1) translating
		// the current coordinate system origin to be what is presently the point
		// (clippingRect.X, clippingRect.Y), (2) making the x-axis range from 
		// clippingRect.Width to 0, and (3) making the y-axis range from -clippingRect.height/2
		// to clippingRect.Height/2. We'll do the entire transformation with a single
		// affine transformation matrix that first makes the x-axis range from clippingRect.Width
		// to 0 and then translates so that the origin (0,0) is at the left and middle of the
		// plotting rectangle. Doing this then allows RA/Decl coordinates to be very
		// easily mapped to the plotting rectangle via a simple scaling.
		AffineTransform xform = new AffineTransform(-1.0, 0.0, 0.0,
				1.0, clippingRect.x + clippingRect.width, clippingRect.y + clippingRect.height/2.0);
		gc.transform(xform);

		// Now adjust the clipping rectangle to be the proper size. We'll apply
		// the clipping area later because we want to draw the tic marks and
		// labels outside the clipping area
		x = (int) clippingRect.width;
		y = (int) clippingRect.height;
		clippingRect.x = 0;
		clippingRect.width = x;
		clippingRect.y = -Math.round(y/2.0);
		clippingRect.height = y;

		// Draw the RA tic marks and labels on the x-axis
		y = (int) Math.round(Decl2Y(-180.0));
		for (int i=1; i < 24; i++) {
			x = (int) RA2X(i);
			gc.drawLine(x, y, x, y - yTicLen);
			if ((i%2) == 0) {
				st = String.format("%02d",i) + "h";
				x = x + RAFontWidth/2;
				drawString(st, x, y - yTicLen);
			}
		}

		// Now draw the Decl tic marks and labels on the y-axis
		x = (int) Math.round(RA2X(24.0));
		for (int i=-180; i < 170; i += 10) {
			y = (int) Math.round(Decl2Y(i));
			gc.drawLine(x, y, x + xTicLen, y);
			if ((i % 20) == 0) {
				if (i == 0) st = "0" + DEGREE;
				else st = String.format("%+3d",i) + DEGREE;
				DeclFontSize = this.getStrBounds(st,gc);
				DeclFontWidth = (int) DeclFontSize.getWidth();
				y = y + DeclFontHeight/2;
				drawString(st, x + xTicLen + DeclFontWidth, y);
			}
		}

		// Finally, set the clipping rectangle as a clipping region
		gc.clip(clippingRect);

		this.refreshChart();			// refresh so user can see the RA/Decl plotting area
	}

	/**
	 * Initialize the drawing canvas for a star chart, drawn as a circular
	 * chart, of horizon coordinates.
	 * 
	 * Using the current size of the drawing canvas surface, determine the
	 * diameter of a circle to use in plotting the star chart objects. The
	 * diameter will be the canvas width or height, whichever is smaller. This
	 * diameter will be adjusted so that compass directions can be displayed
	 * outside the circle.
	 */
	private void initHorizonCanvas() {
		double WFontHeight, WFontWidth;
		double diameter;
		int padX = 4, padY = 4; // how much spacing to add around the horizon circle

		// Determine where the center of the plotting circle will be
		centerX = canvasWidth / 2.0;
		centerY = canvasHeight / 2.0;

		// Calculate size of adjustment needed for the compass directions
		// based on the size of the font that will be used. We'll use
		// the character W as representative of the font's width/height
		WFontHeight = this.getStrBounds("W", gc).getHeight();
		WFontWidth = this.getStrBounds("W", gc).getWidth();

		// Figure out a proper circle to represent the observer's horizon,
		// which is adjusted for the compass direction strings
		if (canvasWidth > canvasHeight) diameter = canvasHeight - 2 * (WFontHeight + 1.5 * padY);
		else diameter = canvasWidth - 2 * (WFontWidth + 1.5 * padX);
		horizonRadius = diameter / 2.0;

		// Draw the compass directions
		drawString("N", centerX - 0.25 * WFontWidth, centerY + horizonRadius + WFontHeight + padY);
		drawString("S", centerX - 0.25 * WFontWidth, centerY - horizonRadius - padY);
		drawString("W", centerX - horizonRadius - WFontWidth - padX, centerY + WFontHeight / 2.0);
		drawString("E", centerX + horizonRadius + padX, centerY + WFontHeight / 2.0);

		// Draw a circle representing the plotting surface
		drawCircle(centerX, centerY, horizonRadius);

		// Set up a clipping region
		Ellipse2D circle = new Ellipse2D.Double(centerX - horizonRadius, centerY - horizonRadius,
				diameter, diameter);
		gc.clip(circle);

		this.refreshChart();		// refresh so user can see the horizon circle
	}

	/**
	 * Determine how big a "dot" to plot for an object based
	 * upon its mV.
	 * 
	 * @param mV			visual magnitude of the object
	 * @return				radius of a circle to plot for the object
	 * 						where a larger radius represents a brighter object.
	 */
	private static int mVtoRadius(double mV) {
		int rdot;

		// Brightness increases/decreases by 1 magnitude every 2.512 units
		rdot = (int) (ASTMisc.mV0RADIUS - (mV/2.512));
		if (rdot < ASTMisc.mVMIN_RADIUS) rdot = ASTMisc.mVMIN_RADIUS;
		else if (rdot > ASTMisc.mVMAX_RADIUS) rdot = ASTMisc.mVMAX_RADIUS;

		return rdot;
	}

	/**
	 * This method does the transformation necessary to move the coordinate
	 * system origin (0,0) as Java Graphics2D provides it from being the top
	 * left point on the drawing surface to the bottom left of the drawing
	 * surface, and changes the y-axis orientation so that y values increase
	 * going from the origin upwards as in a normal Cartesian coordinate system.
	 * This method also sets the variables canvasWidth and canvasHeight for the
	 * drawing surface. Hence, the net result of this method is that the
	 * coordinate system origin (0,0) is moved to (0, -canvasHeight) with (0,0)
	 * being the bottom left point and (canvasWidth,canvasHeight) being the top
	 * right point on the drawing surface.
	 */
	private void setWorldCoordSysOrigin() {
		AffineTransform xform = new AffineTransform();

		// Remove any prior transformations
		xform.setToIdentity();
		gc.setTransform(xform);

		canvasWidth = this.getWidth();
		canvasHeight = this.getHeight();

		// Move origin from top left corner to bottom left corner
		// and change direction of the y-axis. This is equivalent
		// to translate(0,canvasHeight) followed by scale(1,-1)
		xform.setTransform(1.0, 0.0, 0.0, -1.0, 0.0, canvasHeight);
		gc.setTransform(xform);
	}
}
