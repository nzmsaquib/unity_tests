package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.filechooser.FileNameExtensionFilter;

import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTFileIO;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;

/**
 * <b>Define orbital elements</b>
 * <p>
 * This class provides the ability to load orbital elements
 * from a data file and to store them in an accessible
 * structure. This is done so that changing from one epoch
 * to another only requires that the orbital elements
 * be loaded from a different data file. The default
 * data file is J2000.dat under the data files directory.
 * 
 * An orbital elements data file **MUST** be in the format
 * specifically designed for this book. The format can be
 * gleaned by opening the J2000.dat data file and examining
 * its contents. The format is straightforward and is
 * documented in comments within the data file itself.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTOrbits {

	// The following two globals in this class are used to pass error results back from
	// the readElement method. This is needed because Java functions can only return a
	// single value (or an object) and input parameters to a function cannot be modified.
	private static boolean globalReadError = false;
	private static boolean globalDataError = false;
	
	// The following are indices into a double[] result returned by calcSunEclipticCoordinates
	// for calculating Solar data.
	/** Solar ecliptic latitude */
	public static final int SUN_ECLLAT = 0;
	/** Solar ecliptic longitude */
	public static final int SUN_ECLLON = 1;
	/** Solar mean anomaly */
	public static final int SUN_MEANANOM = 2;
	/** Solar true anomaly */
	public static final int SUN_TRUEANOM = 3;
	
	// The following are indices into a double[] result returned by calcMoonEclipticCoordinates
	// for calculating Lunar data.
	/** Lunar ecliptic latitude */
	public static final int MOON_ECLLAT = 0;
	/** Lunar ecliptic longitude */
	public static final int MOON_ECLLON = 1;
	/** Lunar true ecliptic longitude */
	public static final int MOON_TRUELON = 2;
	/** Corrected lon of the asc. node */
	public static final int MOON_CORRLON = 3;		
	/** Mean anomaly correction for the Moon */
	public static final int MOON_MEANANOM_CORR = 4;
	/** Lunar mean anomaly */
	public static final int MOON_MEANANOM = 5;
	/** Lunar true anomaly */
	public static final int MOON_TRUEANOM = 6;
	
	// The following are indices into a double[] result returned from calcObjEclipticCoord
	// for calculating data about the planets or other Solar System objects
	/** object's ecliptic latitude */
	public static final int OBJ_ECLLAT = 0;
	/** object's ecliptic longitude */
	public static final int OBJ_ECLLON = 1;
	/** object's heliocentric latitude */
	public static final int OBJ_HELIOLAT = 2;
	/** object's radius vector length */
	public static final int OBJ_RADIUSVECT = 3;
	/** Earth's heliocentric latitude */
	public static final int EARTH_HELIOLAT = 4;
	/** Earth's radius vector length */
	public static final int EARTH_RADIUSVECT = 5;
		
	// These indices indicate the results of the calcRiseSetTimes method
	/** whether object rise/set (1.0 = yes, 0.0 = no) */
	public static final int RISE_SET_FLAG = 0;
	/** LST for rising time */
	public static final int RISE_TIME = 1;
	/** LST for setting time */
	public static final int SET_TIME = 2;

	private static final double NA_VALUE = -1.0;		// value to use when something is N/A as an orbital element

	// Define the class instance variables.
	private double epochDate;							// epoch as a date, such as 2000.0 or 1980.0
	private double epochJD;								// epoch as a Julian day number

	/** Define the Earth's standard gravitational parameter. This is only
	 * used in the satellites chapter for a bit of extra accuracy when dealing with
	 * satellites. For other chapters, the value is taken from the orbital
	 * elements data file. */
	public static final double muEarth = 398600.4418;
	
	/** Define radius of the Earth. We could read this from a data file, but this is easier. */
	public static final double EarthRadius = 6378.135;

	// The Earth, Sun, and Moon are mandatory and require special handling,
	// so save their index into the orbital elements db
	private int idxSun = -1;
	private int idxMoon = -1;
	private int idxEarth = -1;

	// Default orbital elements data file
	private static final String dataFilename = "J2000.dat";
	private static final String fullFilename = "/resources/"+dataFilename;
	private static final FileNameExtensionFilter fileExtFilter = new FileNameExtensionFilter("Orbital Elements Data (ex: J2000.dat)","dat","txt");

	// We'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
	private ASTPrt prt = null;

	private boolean orbitalElementsLoaded = false;

	/*-----------------------------------------------------------------
	 * Define a private class and database for the orbital elements.
	 *----------------------------------------------------------------*/
	private static class OrbElemObj {
		private String sName;				// the name of the object to which this orbital element data applies
		private boolean bInferior;			// true if this is an object whose orbit is between the Earth and Sun
		private double dPeriod;				// orbital period in tropical years
		private double dMass;				// object's mass relative to the Earth
		private double dRadius;				// object's radius in km
		private double dDay;				// length of day relative to Earth
		private double dEccentricity;		// orbital eccentricity
		private double dSemiMajAxisAU;		// length of the semi-major axis in AUs
		private double dSemiMajAxisKM;		// length of the semi-major axis in km (Sun and Moon only)
		private double dAngDiamArcSec;		// angular diameter in arcseconds
		private double dAngDiamDeg;			// angular diameter in degrees (Sun and Moon only)
		private double dmV;					// visual magnitude
		private double dGravParm;			// gravitational parameter in km^3/s^2
		private double dInclination;		// orbital inclination in degrees
		private double dLonAtEpoch;			// longitude at the epoch in degrees
		private double dLonAtPeri;			// longitude at perihelion in degrees
		private double dLonAscNode;			// longitude of the ascending node
	}

	// Database of the currently loaded orbital elements
	private static List<OrbElemObj> orbElementsDB = new ArrayList<OrbElemObj>();

	/**
	 * Initialize from the default orbital elements data file, if
	 * it exists. if it doesn't, require the user to find it.
	 * This method should only be called once and then methods
	 * below should be used to load a new orbital elements data
	 * file under the calling application's control.
	 * 
	 * @param prtInstance		instance of an output area
	 */
	public ASTOrbits(ASTPrt prtInstance) {
		initOrbitalElements(prtInstance);
	}

	/*===================================================================
	 * Define 'get' accessors for the instance data for this class and
	 * for the elements in the orbital elements database.
	 * 
	 * Warning: No check is made to be sure idx is valid. Also, note that
	 * if an item is N/A (e.g., angular diameter of the Earth), the
	 * value NA_VALUE is returned.
	 *==================================================================*/
	/**
	 * Get number of orbital elements in the database.
	 * 
	 * @return			number of objects in the OE database
	 */
	public int getNumOEDBObjs() {
		return orbElementsDB.size();
	}
	/**
	 * Get the epoch for the data in the OE database.
	 * 
	 * @return		epoch for the data
	 */
	public double getOEEpochDate() {
		return epochDate;
	}
	/**
	 * Get the epoch for the data in the OE database
	 * as a Julian date.
	 * 
	 * @return		Julian date for the epoch in the OE database
	 */
	public double getOEEpochJD() {
		return epochJD;
	}
	/**
	 * Gets the index for where the Sun is in the OE db.
	 * 
	 * @return		index for the Sun
	 */
	public int getOEDBSunIndex() {
		return idxSun;
	}
	/**
	 * Gets the index for where the Moon is in the OE db.
	 * 
	 * @return		index for the Moon
	 */
	public int getOEDBMoonIndex() {
		return idxMoon;
	}
	/**
	 * Gets the index for where the Earth is in the OE db.
	 * 
	 * @return		index for the Earth
	 */
	public int getOEDBEarthIndex() {
		return idxEarth;
	}
	/**
	 * Gets an object's name.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object name
	 */
	public String getOEObjName(int idx) {
		return orbElementsDB.get(idx).sName;
	}
	/**
	 * Gets whether an object is an inferior object.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			true if an inferior object
	 */
	public boolean getOEObjInferior(int idx) {
		return orbElementsDB.get(idx).bInferior;
	}
	/**
	 * Gets an object's orbital period.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's orbital period
	 */
	public double getOEObjPeriod(int idx) {
		return orbElementsDB.get(idx).dPeriod;
	}
	/**
	 * Gets an object's mass.
	 *
	 * @param idx		where object is in the OE db
	 * @return			object's mass
	 */
	public double getOEObjMass(int idx) {
		return orbElementsDB.get(idx).dMass;
	}
	/**
	 * Gets an object's radius.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			radius of the object
	 */
	public double getOEObjRadius(int idx) {
		return orbElementsDB.get(idx).dRadius;
	}
	/**
	 * Gets length of an object's day relative to Earth.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			length of object's day
	 */
	public double getOEObjDay(int idx) {
		return orbElementsDB.get(idx).dDay;
	}
	/**
	 * Gets an object's orbital eccentricity.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's orbital eccentricity
	 */
	public double getOEObjEccentricity(int idx) {
		return orbElementsDB.get(idx).dEccentricity;
	}
	/**
	 * Gets length of an object's semi-major axis in AUs.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			length of semi-major axis
	 */
	public double getOEObjSemiMajAxisAU(int idx) {
		return orbElementsDB.get(idx).dSemiMajAxisAU;
	}
	/**
	 * Gets length of an object's semi-major axis in km.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			length of semi-major axis
	 */
	public double getOEObjSemiMajAxisKM(int idx) {
		return orbElementsDB.get(idx).dSemiMajAxisKM;
	}
	/**
	 * Gets an object's angular diameter in arcseconds.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's angular diameter
	 */
	public double getOEObjAngDiamArcSec(int idx) {
		return orbElementsDB.get(idx).dAngDiamArcSec;
	}
	/**
	 * Gets an object's angular diameter in degrees.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's angular diameter
	 */
	public double getOEObjAngDiamDeg(int idx) {
		return orbElementsDB.get(idx).dAngDiamDeg;
	}
	/**
	 * Gets an object's visual magnitude.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's visual magnitude
	 */
	public double getOEObjmV(int idx) {
		return orbElementsDB.get(idx).dmV;
	}
	/**
	 * Gets an object's gravitational parameter.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's gravitational parameter
	 */
	public double getOEObjGravParm(int idx) {
		return orbElementsDB.get(idx).dGravParm;
	}
	/**
	 * Gets an object's orbital inclination.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's orbital inclination
	 */
	public double getOEObjInclination(int idx) {
		return orbElementsDB.get(idx).dInclination;
	}
	/**
	 * Gets an object's longitude at the epoch.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's longitude at the epoch
	 */
	public double getOEObjLonAtEpoch(int idx) {
		return orbElementsDB.get(idx).dLonAtEpoch;
	}
	/**
	 * Gets an objects longitude at perihelion.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's longitude at perihelion
	 */
	public double getOEObjLonAtPeri(int idx) {
		return orbElementsDB.get(idx).dLonAtPeri;
	}
	/**
	 * Gets an object's longitude of the ascending node.
	 * 
	 * @param idx		where object is in the OE db
	 * @return			object's longitude of the ascending node
	 */
	public double getOEObjLonAscNode(int idx) {
		return orbElementsDB.get(idx).dLonAscNode;
	}
	
	/**
	 * Calculate the Earth's radius vector length (i.e., distance to the Sun)
	 * at the stated time and date.
	 * 
	 * @param month					month at which Earth's distance is desired
	 * @param day					day at which Earth's distance is desired
	 * @param year					year at which Earth's distance is desired
	 * @param UT					time (UT) at which distance is desired
	 * @param solvetrueAnomaly		how to compute the true anomaly
	 * @param termCriteria			termination criteria if Kepler's equation
	 * 								is solved
	 * @return						the Earth's	radius vector length (R_e)
	 */
	public double calcEarthRVLength(int month, int day, int year, double UT, ASTKepler.TrueAnomalyType solvetrueAnomaly,double termCriteria) {
		double[] tmpKepler;
		double R_e = 1.0;
		ASTDate obsDate = new ASTDate();
		double JD, JDe, De;
		double Ecc_e, M_e, v_e, Ec_e, Ea_e;
		
		if (!orbitalElementsLoaded) {
			ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements");
			return R_e;
		}

		obsDate.setMonth(month);
		obsDate.setYear(year);
		obsDate.setiDay(day);
		Ecc_e = getOEObjEccentricity(idxEarth);

		JDe = getOEEpochJD();
		JD = ASTDate.dateToJD(obsDate.getMonth(), obsDate.getdDay() + (UT / 24.0), obsDate.getYear());
		De = JD - JDe;

		// Calculate the Earth's mean and true anomalies
		M_e = (360 * De) / (365.242191 * getOEObjPeriod(idxEarth)) + getOEObjLonAtEpoch(idxEarth) - getOEObjLonAtPeri(idxEarth);
		M_e = ASTMath.xMOD(M_e, 360.0);

		// Find the Earth's true anomaly from equation of center or Kepler's equation
		if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER) {
			Ec_e = (360.0 / Math.PI) * Ecc_e * ASTMath.SIN_D(M_e);
			v_e = M_e + Ec_e;
		} else {
			if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
				tmpKepler = ASTKepler.calcSimpleKepler(M_e, Ecc_e, termCriteria);
				Ea_e = tmpKepler[0];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(M_e, Ecc_e, termCriteria);
				Ea_e = tmpKepler[0];
			}

			v_e = (1 + Ecc_e) / (1 - Ecc_e);
			v_e = Math.sqrt(v_e) * ASTMath.TAN_D(Ea_e / 2.0);
			v_e = 2.0 * ASTMath.INVTAN_D(v_e);
		}

		// Calculate the Earth's radius vector length (R_e)
		R_e = getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e);
		R_e = R_e / (1 + Ecc_e * ASTMath.COS_D(v_e));

		return R_e;
	}

	/**
	 * Calculate the Moon's ecliptic coordinates using the
	 * currently loaded orbital elements.
	 * 
	 * @param month					month at which Moon's position is desired
	 * @param day					day at which Moon's position is desired
	 * @param year					year at which Moon's position is desired
	 * @param UT					time (UT) at which position is desired
	 * @param solvetrueAnomaly		how to compute the true anomaly
	 * @param termCriteria          termination criteria if Kepler's equation
	 * 								is solved
	 * @return						a double[7] array with Moon's ecliptic
	 * 								latitude (Bmoon), ecliptic longitude (Lmoon),
	 * 								true ecliptic longitude (Ltrue), corrected
	 * 								longitude of the ascending node (Omega_p),
	 * 								mean anomaly correction (Ca), mean anomaly (Mm),
	 * 								and true anomaly (Vmoon)
	 */
	public double[] calcMoonEclipticCoord(int month, int day, int year, double UT, ASTKepler.TrueAnomalyType solvetrueAnomaly,double termCriteria) {
		double[] result = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		double[] tmpResult;
		double Ltrue = 0.0, Omega_p = 0.0, Ca = 0.0, Mm = 0.0, Vmoon = 0.0;
		ASTDate obsDate = new ASTDate();
		double TT, JDe, JD, De;
		double inclin, L_uncor, L_p, Omega, Ev, Ae, V;
		double Lmoon = 0.0, Bmoon = 0.0;
		double x, y, dT;
		double Msun, Lsun;
		
		if (!orbitalElementsLoaded) {
			ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements");
			return result;
		}

		obsDate.setMonth(month);
		obsDate.setYear(year);
		obsDate.setiDay(day);
		inclin = getOEObjInclination(idxMoon);

		// Adjust the time at which the Moon's position is required
		TT = UT + (63.8 / 3600.0);
		JDe = getOEEpochJD();
		JD = ASTDate.dateToJD(month, day + (TT / 24.0), year);
		De = JD - JDe;
	
		// Get the Sun's position
		tmpResult = calcSunEclipticCoord(month, day, year, UT, solvetrueAnomaly, termCriteria);
		Lsun = tmpResult[ASTOrbits.SUN_ECLLON];
		Msun = tmpResult[ASTOrbits.SUN_MEANANOM];
	
		// Compute various corrections
		L_uncor = 13.176339686 * De + getOEObjLonAtEpoch(idxMoon);
		L_uncor = ASTMath.xMOD(L_uncor, 360.0);
		Omega = getOEObjLonAscNode(idxMoon) - 0.0529539 * De;
		Omega = ASTMath.xMOD(Omega, 360.0);
		Mm = L_uncor - 0.1114041 * De - getOEObjLonAtPeri(idxMoon);
		Mm = ASTMath.xMOD(Mm, 360.0);
		Ae = 0.1858 * ASTMath.SIN_D(Msun);
		Ev = 1.2739 * ASTMath.SIN_D(2 * (L_uncor - Lsun) - Mm);
		Ca = Mm + Ev - Ae - 0.37 * ASTMath.SIN_D(Msun);

		// Calculate Moon's true anomaly
		Vmoon = 6.2886 * ASTMath.SIN_D(Ca) + 0.214 * ASTMath.SIN_D(2 * Ca);

		L_p = L_uncor + Ev + Vmoon - Ae;
		V = 0.6583 * ASTMath.SIN_D(2 * (L_p - Lsun));
		Ltrue = L_p + V;
		Omega_p = Omega - 0.16 * ASTMath.SIN_D(Msun);
		y = ASTMath.SIN_D(Ltrue - Omega_p) * ASTMath.COS_D(inclin);
		x = ASTMath.COS_D(Ltrue - Omega_p);
		dT = ASTMath.INVTAN_D(y / x);
		x = ASTMath.quadAdjust(y, x);
		dT = dT + x;

		Lmoon = Omega_p + dT;
		if (Lmoon > 360) Lmoon = Lmoon - 360;
		Bmoon = ASTMath.SIN_D(Ltrue - Omega_p) * ASTMath.SIN_D(inclin);
		Bmoon = ASTMath.INVSIN_D(Bmoon);
		
		result[MOON_ECLLAT] = Bmoon;
		result[MOON_ECLLON] = Lmoon;
		result[MOON_TRUELON] = Ltrue;
		result[MOON_CORRLON] = Omega_p;
		result[MOON_MEANANOM_CORR] = Ca;
		result[MOON_MEANANOM] = Mm;
		result[MOON_TRUEANOM] = Vmoon;

		return result;
	}

	/**
	 * Calculate the distance (in AUs) from an object to the Earth.
	 * 
	 * @param Re			Earth's radius vector in AUs
	 * @param Le			Earth's heliocentric longitude
	 * @param Rp			object's radius vector in AUs
	 * @param Lp			object's heliocentric longitude
	 * @return				distance from Earth to object in AUs.
	 */
	public static double calcObjDistToEarth(double Re, double Le, double Rp, double Lp) {
		return Math.sqrt(Re * Re + Rp * Rp - 2 * Re * Rp * ASTMath.COS_D(Lp - Le));
	}
	
	/**
	 * Calculate an object's ecliptic coordinates using the
	 * currently loaded orbital elements. The object can be anything
	 * in the orbital elements file except the Sun, Moon, or Earth.
	 * 
	 * @param month					month at which object's position is desired
	 * @param day					day at which object's position is desired
	 * @param year					year at which object's position is desired
	 * @param UT					time (UT) at which position is desired
	 * @param idx					index into the orbital elements database
	 * 								for the object whose position is desired
	 * @param solvetrueAnomaly		how to compute the true anomaly for the Sun,
	 * 								Earth, and object
	 * @param termCriteria			termination criteria if Kepler's equation
	 * 								is solved
	 * @return						a double[6] array with the object's ecliptic latitude (Lat_p),
	 * 								object's ecliptic longitude (Lon_p), object's heliocentric
	 * 								latitude (L_p), object's radius vector length (R_p),
	 * 								Earth's heliocentric latitude (L_e), and Earth's
	 * 								radius vector length (R_e)
	 * 
	 * Caution: No check is made to be sure idx is valid.
	 */
	public double[] calcObjEclipticCoord(int month, int day, int year, double UT, int idx, 
			ASTKepler.TrueAnomalyType solvetrueAnomaly,double termCriteria) {
		double[] result = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		double[] tmpKepler;
		double L_p, R_p, L_e, R_e;
		ASTDate obsDate = new ASTDate();
		double JD, JDe, De;
		double Ecc_p, inclin_p, M_p, v_p, Ec_p, Ea_p, H_p, Lp_p, Lon_p, Lat_p;
		double Ecc_e, inclin_e, M_e, v_e, Ec_e, Ea_e, H_e;
		double x, y, dT;
		
		if (!orbitalElementsLoaded) {
			ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements");
			return result;
		}

		obsDate.setMonth(month);
		obsDate.setYear(year);
		obsDate.setiDay(day);
		Ecc_p = getOEObjEccentricity(idx);
		inclin_p = getOEObjInclination(idx);
		Ecc_e = getOEObjEccentricity(idxEarth);
		inclin_e = getOEObjInclination(idxEarth);

		JDe = getOEEpochJD();
		JD = ASTDate.dateToJD(obsDate.getMonth(), obsDate.getdDay() + (UT / 24.0), obsDate.getYear());
		De = JD - JDe;

		// Calculate the object's mean and true anomalies
		M_p = (360 * De) / (365.242191 * getOEObjPeriod(idx)) + getOEObjLonAtEpoch(idx) - getOEObjLonAtPeri(idx);
		M_p = ASTMath.xMOD(M_p, 360.0);

		// Find the true anomaly from equation of center or Kepler's equation
		if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER) {
			Ec_p = (360.0 / Math.PI) * Ecc_p * ASTMath.SIN_D(M_p);
			v_p = M_p + Ec_p;
		} else {
			if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
				tmpKepler = ASTKepler.calcSimpleKepler(M_p, Ecc_p, termCriteria);
				Ea_p = tmpKepler[0];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(M_p, Ecc_p, termCriteria);
				Ea_p = tmpKepler[0];
			}
			v_p = (1 + Ecc_p) / (1 - Ecc_p);
			v_p = Math.sqrt(v_p) * ASTMath.TAN_D(Ea_p / 2.0);
			v_p = 2.0 * ASTMath.INVTAN_D(v_p);
		}

		// Calculate the object's heliocentric ecliptic coordinates (L_p, H_p)
		// and radius vector length (R_p)
		L_p = v_p + getOEObjLonAtPeri(idx);
		L_p = ASTMath.xMOD(L_p, 360.0);
		H_p = ASTMath.SIN_D(L_p - getOEObjLonAscNode(idx)) * ASTMath.SIN_D(inclin_p);
		H_p = ASTMath.INVSIN_D(H_p);
		H_p = ASTMath.xMOD(H_p, 360.0);

		R_p = getOEObjSemiMajAxisAU(idx) * (1 - Ecc_p * Ecc_p);
		R_p = R_p / (1 + Ecc_p * ASTMath.COS_D(v_p));

		// Repeat the steps just done for the object for the Earth

		// Calculate the Earth's mean and true anomalies
		M_e = (360 * De) / (365.242191 * getOEObjPeriod(idxEarth)) + getOEObjLonAtEpoch(idxEarth) - getOEObjLonAtPeri(idxEarth);
		M_e = ASTMath.xMOD(M_e, 360.0);

		// Find the Earth's true anomaly from equation of center or Kepler's equation
		if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER) {
			Ec_e = (360.0 / Math.PI) * Ecc_e * ASTMath.SIN_D(M_e);
			v_e = M_e + Ec_e;
		} else {
			if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
				tmpKepler = ASTKepler.calcSimpleKepler(M_e, Ecc_e, termCriteria);
				Ea_e = tmpKepler[0];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(M_e, Ecc_e, termCriteria);
				Ea_e = tmpKepler[0];
			}

			v_e = (1 + Ecc_e) / (1 - Ecc_e);
			v_e = Math.sqrt(v_e) * ASTMath.TAN_D(Ea_e / 2.0);
			v_e = 2.0 * ASTMath.INVTAN_D(v_e);
		}

		// Calculate the Earth's heliocentric ecliptic coordinates (L_e, H_e) and radius vector length (R_e)
		L_e = v_e + getOEObjLonAtPeri(idxEarth);
		L_e = ASTMath.xMOD(L_e, 360.0);
		H_e = ASTMath.SIN_D(L_e - getOEObjLonAscNode(idxEarth)) * ASTMath.SIN_D(inclin_e);
		H_e = ASTMath.INVSIN_D(H_e);
		H_e = ASTMath.xMOD(H_e, 360.0);

		R_e = getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e);
		R_e = R_e / (1 + Ecc_e * ASTMath.COS_D(v_e));

		// Given the heliocentric location and radius vector length for both the Earth
		// and the object, project the object's location onto the ecliptic plane
		// with respect to the Earth
		y = ASTMath.SIN_D(L_p - getOEObjLonAscNode(idx)) * ASTMath.COS_D(inclin_p);
		x = ASTMath.COS_D(L_p - getOEObjLonAscNode(idx));
		dT = ASTMath.INVTAN_D(y / x);
		x = ASTMath.quadAdjust(y, x);
		dT = dT + x;

		Lp_p = getOEObjLonAscNode(idx) + dT;
		Lp_p = ASTMath.xMOD(Lp_p, 360.0);

		// See if this is an inferior or superior object and compute accordingly
		if (getOEObjInferior(idx)) {
			y = R_p * ASTMath.COS_D(H_p) * ASTMath.SIN_D(L_e - Lp_p);
			x = R_e - R_p * ASTMath.COS_D(H_p) * ASTMath.COS_D(L_e - Lp_p);
			dT = ASTMath.INVTAN_D(y / x);
			x = ASTMath.quadAdjust(y, x);
			dT = dT + x;

			Lon_p = 180 + L_e + dT;
			Lon_p = ASTMath.xMOD(Lon_p, 360.0);
		} else {
			y = R_e * ASTMath.SIN_D(Lp_p - L_e);
			x = R_p * ASTMath.COS_D(H_p) - R_e * ASTMath.COS_D(L_e - Lp_p);
			dT = ASTMath.INVTAN_D(y / x);
			x = ASTMath.quadAdjust(y, x);
			dT = dT + x;

			Lon_p = Lp_p + dT;
			Lon_p = ASTMath.xMOD(Lon_p, 360.0);
		}
	
		Lat_p = R_p * ASTMath.COS_D(H_p) * ASTMath.TAN_D(H_p) * ASTMath.SIN_D(Lon_p - Lp_p);
		Lat_p = Lat_p / (R_e * ASTMath.SIN_D(Lp_p - L_e));
		Lat_p = ASTMath.INVTAN_D(Lat_p);
		
		result[OBJ_ECLLAT] = Lat_p;
		result[OBJ_ECLLON] = Lon_p;
		result[OBJ_HELIOLAT] = L_p;
		result[OBJ_RADIUSVECT] = R_p;
		result[EARTH_HELIOLAT] = L_e;
		result[EARTH_RADIUSVECT] = R_e;
	
		return result;
	}
	
	/**
	 * Calculate the LST rising and setting times for a given
	 * equatorial coordinate.
	 * 
	 * @param RA				right ascension
	 * @param Decl				declination
	 * @param Lat				observer's latitude
	 * @return					a double[3] array with flag indicating
	 * 							whether the object rise or sets (1.0 = rise/set,
	 * 							-1.0 = doesn't rise/set), LST rising time, and
	 * 							LST setting time
	 * 
	 * Note: Rising and setting azimuth are calculated although the setting
	 * azimuth isn't needed.
	 */
	@SuppressWarnings("unused")
	public static double[] calcRiseSetTimes(double RA, double Decl, double Lat) {
		double[] result = {0.0, 0.0, 0.0};
		double Ar, R, S, H1, H2;
		double TRUE = 1.0;
		double FALSE = -1.0;
		
		result[RISE_SET_FLAG] = TRUE;
		result[RISE_TIME] = 0.0;				// LST Rise time
		result[SET_TIME] = 0.0;					// LST set time

		Ar = ASTMath.SIN_D(Decl) / ASTMath.COS_D(Lat);
		if (Math.abs(Ar) > 1) {
			result[RISE_SET_FLAG] = FALSE;
			return result;
		}
	
		R = ASTMath.INVCOS_D(Ar);				// rising azimuth
		S = 360.0 - R;							// setting azimuth
		H1 = ASTMath.TAN_D(Lat) * ASTMath.TAN_D(Decl);
		if (Math.abs(H1) > 1) {
			result[RISE_SET_FLAG] = FALSE;
			return result;
		}
	
		H2 = ASTMath.INVCOS_D(-H1) / 15.0;
		result[RISE_TIME] = 24 + RA - H2;
		if (result[RISE_TIME] > 24) result[RISE_TIME] = result[RISE_TIME] - 24.0;
		result[SET_TIME] = RA + H2;
		if (result[SET_TIME] > 24) result[SET_TIME] = result[SET_TIME] - 24.0;
		
		return result;
	}

	/**
	 * Calculate the Sun's ecliptic coordinates using the
	 * currently loaded orbital elements.
	 * 
	 * @param month					month at which Sun's position is desired
	 * @param day					day at which Sun's position is desired
	 * @param year					year at which Sun's position is desired
	 * @param UT					time (UT) at which position is desired
	 * @param solvetrueAnomaly		how to compute the true anomaly
	 * @param termCriteria          termination criteria if Kepler's equation
	 * 								is solved
	 * @return						a double[4] array with Sun's ecliptic latitude,
	 * 								ecliptic longitude, mean anomaly, and true
	 * 								anomaly as calculated by this method
	 */
	public double[] calcSunEclipticCoord(int month,int day,int year,double UT,ASTKepler.TrueAnomalyType solvetrueAnomaly,double termCriteria) {
		double[] result = {0.0, 0.0, 0.0, 0.0};
		double[] tmpKepler;
		double Msun = 0.0, Vsun = 0.0;
		double Ecc, Lsun, JDe, JD, De, Ec, Ea;
		ASTDate obsDate = new ASTDate();

		if (!orbitalElementsLoaded) {
			ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements");
			return result;
		}

		Ecc = getOEObjEccentricity(idxSun);
		obsDate.setMonth(month);
		obsDate.setYear(year);
		obsDate.setiDay(day);

		JDe = getOEEpochJD();
		JD = ASTDate.dateToJD(month, day + (UT / 24.0), year);
		De = JD - JDe;

		Msun = ((360.0 * De) / 365.242191) + getOEObjLonAtEpoch(idxSun) - getOEObjLonAtPeri(idxSun);
		Msun = ASTMath.xMOD(Msun, 360.0);

		// Find the true anomaly from equation of center or Kepler's equation
		if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER) {
			Ec = (360.0 / Math.PI) * Ecc * ASTMath.SIN_D(Msun);
			Vsun = Ec + Msun;
		} else {
			if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
				tmpKepler = ASTKepler.calcSimpleKepler(Msun, Ecc, termCriteria);
				Ea = tmpKepler[0];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(Msun, Ecc, termCriteria);
				Ea = tmpKepler[0];
			}
			Vsun = (1 + Ecc) / (1 - Ecc);
			Vsun = Math.sqrt(Vsun) * ASTMath.TAN_D(Ea / 2.0);
			Vsun = 2.0 * ASTMath.INVTAN_D(Vsun);
		}

		Vsun = ASTMath.xMOD(Vsun, 360.0);
		Lsun = Vsun + getOEObjLonAtPeri(idxSun);
		if (Lsun > 360.0) Lsun = Lsun - 360.0;

		result[SUN_ECLLAT] = 0.0;
		result[SUN_ECLLON] = Lsun;
		result[SUN_MEANANOM] = Msun;
		result[SUN_TRUEANOM] = Vsun;
		return result;
	}

	/**
	 * Displays a list in the user's scrollable output
	 * text area of all the orbital elements for all
	 * of the objects in the database.
	 */
	public void displayAllOrbitalElements() {
		int i;

		if (!isOrbElementsDBLoaded()) {
			ASTMsg.errMsg("No Orbital Elements have been loaded", "No Orbital Elements");
			return;
		}
		prt.setBoldFont(true);
		prt.println("Currently Loaded Orbital Elements", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();
		prt.println("Orbital Elements are for Epoch " + String.format(ASTStyle.epochFormat,epochDate) +
				" (Julian Day Number " + String.format(ASTStyle.JDFormat,epochJD) + ")");
		prt.println();

		for (i=0; i < getNumOEDBObjs(); i++) {        
			prt.println(String.format("%80s", "=").replace(' ', '='));
			displayObjOrbElements(i);
			prt.println();
		}
	}

	/**
	 * Display the orbital elements for an object.
	 * 
	 * @param idx		index into OE db for the desired object
	 */
	public void displayObjOrbElements(int idx) {
		String st = "Orbital Elements and Other Data for ";

		if (!isOrbElementsDBLoaded()) return;
		if ((idx < 0) || (idx > getNumOEDBObjs())) return;

		if ((idx == getOEDBEarthIndex()) || (idx == getOEDBMoonIndex()) || (idx == getOEDBSunIndex())) st = st + "the ";

		prt.setBoldFont(true);
		prt.println(st + orbElementsDB.get(idx).sName + " (Epoch: " + getOEEpochDate() + ")", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		if ((idx != getOEDBEarthIndex()) && (idx != getOEDBMoonIndex()) && (idx != getOEDBSunIndex())) {
			prt.print("Object's orbit is ");
			if (!getOEObjInferior(idx)) prt.print("not ");
			prt.println("between the Earth and the Sun");
			prt.println();
		}

		prt.println("Mass: " + ASTStr.insertCommas(getOEObjMass(idx)) + " times the mass of the Earth");
		prt.println("Gravitational Parameter: " + ASTStr.insertCommas(getOEObjGravParm(idx)) + " km^3 per s^2");
		prt.println("Radius: " + ASTStr.insertCommas(getOEObjRadius(idx)) + " km");
		prt.println("Day: " + getOEObjDay(idx) + " Earth days");
		if (idx == getOEDBMoonIndex()) prt.println("Visual Magnitude (Full Moon): " + getOEObjmV(idx) + " when 1 AU from the Earth");
		else if (idx != getOEDBEarthIndex()) prt.println("Visual Magnitude: " + getOEObjmV(idx) + " when 1 AU from the Earth");

		if ((idx != getOEDBEarthIndex()) && (idx != getOEDBMoonIndex())) {
			prt.println("Angular Diameter: " + ASTStr.insertCommas(getOEObjAngDiamArcSec(idx)) +
					" arc seconds when length of the semi-major axis away");
		}
		if ((idx == getOEDBSunIndex()) || (idx == getOEDBMoonIndex())) {
			prt.println("Angular Diameter: " + getOEObjAngDiamDeg(idx) +
					" degrees when length of the semi-major axis away");
		}

		prt.println();

		if ((idx != getOEDBSunIndex()) && (idx != getOEDBMoonIndex())) {
			prt.println("Orbital Period: " + getOEObjPeriod(idx) + " tropical years");
		}
		prt.println("Orbital Inclination: " + getOEObjInclination(idx) + " degrees");
		prt.println("Orbital Eccentricity: " + getOEObjEccentricity(idx));
		prt.print("Length of Orbital Semi-Major Axis: " + ASTStr.insertCommas(getOEObjSemiMajAxisAU(idx)) + " AUs");
		if ((idx == getOEDBSunIndex()) || (idx == getOEDBMoonIndex())) {
			prt.print(" (which is approximately " + ASTStr.insertCommas(getOEObjSemiMajAxisKM(idx)) + " km)");
		}
		prt.println();
		prt.println("Longitude at the Epoch: " + getOEObjLonAtEpoch(idx) + " degrees");
		prt.print("Longitude at ");
		if (idx == getOEDBMoonIndex()) prt.print("Perigee");
		else prt.print("Perihelion");
		prt.println(": " + getOEObjLonAtPeri(idx) + " degrees");
		if (idx != getOEDBSunIndex()) prt.println("Longitude of the Ascending Node: " + 
				getOEObjLonAscNode(idx) + " degrees");
	}

	/**
	 * Searches the orbital elements database and returns an index into the
	 * database for the requested object. The search performed is **not**
	 * case sensitive, but an exact match, ignoring white space, is required.
	 * 
	 * @param name			name of the object to find
	 * @return				if successful, returns an index into the orbital elements database
	 * 						for the object requested. if unsuccessful, -1 is returned. Note that
	 * 						this assumes 0-based indexing!
	 */
	public int findOrbElementObjIndex(String name) {

		if (!isOrbElementsDBLoaded()) return -1;

		for (int i = 0; i < getNumOEDBObjs(); i++) {
			if (name.compareToIgnoreCase(getOEObjName(i)) == 0) return i;
		}
		return -1;
	}

	/**
	 * Puts up a browser window and gets an orbital elements filename.
	 * 
	 * @return					full name (file and path) of the orbital elements file
	 * 							to open or null if no file is selected or
	 * 							file doesn't exist. This routine does **not**
	 * 							check to see if the file is a valid orbital elements data, 
	 * 							but it **does** check to see that the file exists.
	 */
	public static String getOEDBFileToOpen() {
		String fullFilename = null;
		String[] fileToRead = new String[2];

		fileToRead = ASTFileIO.getFileToRead("Select Orbital Elements file to Load ...","Load Orbital Elements",fileExtFilter);
		fullFilename = fileToRead[ASTFileIO.FULLPATHNAME_IDX];

		// See if the file exists
		if (!ASTFileIO.doesFileExist(fullFilename)) {
			ASTMsg.errMsg("The Orbital Elements data file specified does not exist", "Orbital Elements File Does Not Exist");
			return null;
		}

		return fullFilename;
	}
	
	/**
	 * Determine an object's orbit type based on orbital
	 * inclination and eccentricity.
	 * 
	 * @param e			orbital eccentricity
	 * @param inclin	orbital inclination in degrees
	 * @return			orbit type or -1 for an error
	 * 
	 * Note: orbit types are as defined in the Satellites chapter.
	 */
	public static int getSatOrbitType(double e,double inclin) {	
		if (e < 0.0) return -1;			// negative eccentricity is impossible
		if (e >= 1) return -1;			// we don't handle parabolic or hyperbolic orbits

		if (ASTMath.isClose(e, 0.0)) {							// circular orbits
			if (ASTMath.isClose(inclin, 0.0) || 
					ASTMath.isClose(inclin, 180.0)) return 1;   // circular orbit in the equatorial plane
			else return 3;                						// circular orbit inclined w.r.t. equatorial plane
		}
		if (e > 0.0) {             								// non-circular orbits
			if (ASTMath.isClose(inclin, 0.0) || 
					ASTMath.isClose(inclin, 180.0)) return 2;   // elliptical orbit in the equatorial plane	
			else return 4;                						// elliptical orbit inclined w.r.t. equatorial plane
		}
		return -1;					// An error condition!
	}


	/**
	 * Determine whether there is currently an orbital
	 * elements database loaded.
	 * 
	 * @return				true if a db is loaded, else false
	 */
	public boolean isOrbElementsDBLoaded() {
		if ((idxSun < 0) || (idxMoon < 0) || (idxEarth < 0)) return false;
		return orbitalElementsLoaded;
	}

	/**
	 * Loads orbital elements database from disk.
	 * 
	 * @param filename			Full filename (including path) to load
	 * @return					true if successful, else false.
	 */
	public boolean loadOEDB(String filename) {

		clearOrbitalElementObjects();

		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			if (!readOrbitalElementsData(br)) {
				br.close();
				ASTMsg.errMsg("An Orbital Elements data file is required but could\n" + 
						"not be processed. Try another data file.", "Invalid Orbital Elements Data File");
				br.close();
				return false;
			} else br.close();

		} catch (IOException e) {
			ASTMsg.criticalErrMsg("The selected Orbital Elements data file could not be loaded");
			e.printStackTrace();
			return false;
		}

		orbitalElementsLoaded = true;
		idxEarth = findOrbElementObjIndex("Earth");
		idxMoon = findOrbElementObjIndex("Moon");
		idxSun = findOrbElementObjIndex("Sun");

		return orbitalElementsLoaded;
	}

	/*------------------------------------------------
	 * Private methods used only in this class.
	 *-----------------------------------------------*/

	/**
	 * Clears all the currently loaded orbital elements.
	 */
	private void clearOrbitalElementObjects() {
		idxSun = -1;
		idxMoon = -1;
		idxEarth = -1;

		// Now delete all of the orbital element objects.
		// In some languages, we'd need to loop through the db
		// and delete objects individually.		
		orbElementsDB = new ArrayList<OrbElemObj>();

		orbitalElementsLoaded = false;
	}

	/**
	 * Does a one-time initialization by reading in the default
	 * orbital elements data file.
	 * 
	 * @param prtInstance		instance of a scrollable output area
	 */
	public void initOrbitalElements(ASTPrt prtInstance) {
		String[] fileToRead = new String[2];
		InputStream stream = null;

		orbitalElementsLoaded = false;
		globalReadError = false;
		globalDataError = false;

		if (prtInstance == null) {
			ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ASTMisc.ABORT_PROG);
		}

		prt = prtInstance;

		// See if the file exists where it is expected. If not, ask the user to find it.
		if (ASTOrbits.class.getResource(fullFilename) == null) {
			ASTMsg.errMsg("The Orbital Elements data file (" + dataFilename + ")\n" +
					"is missing. Please click 'OK' and then manually\n" +
					"find an orbital elements data file.", "Missing Orbital Elements Data File");
			fileToRead = ASTFileIO.getFileToRead("Load Orbital Elements Data ...","Orbital Elements Data",fileExtFilter);
			if ((fileToRead[ASTFileIO.FILENAME_IDX] == null) || (fileToRead[ASTFileIO.FILENAME_IDX].length() <= 0)) {
				ASTMsg.criticalErrMsg("An Orbital Elements data file is required but was\n" +
						"not found. Aborting program ...",ASTMisc.ABORT_PROG);
			} else {
				try {
					stream = new FileInputStream(fileToRead[ASTFileIO.FULLPATHNAME_IDX]);
				} catch (IOException e) {
					ASTMsg.criticalErrMsg("The file specified is not an Orbital Elements data\n" +
							"file and could not be processed. Aborting program ...", ASTMisc.ABORT_PROG);
				}
			}
		} else stream = ASTOrbits.class.getResourceAsStream(fullFilename);

		// Now that we've found an orbital elements data file, read in the data.
		// Note that we're using try-with-resources so  we don't have to explicitly close the stream when we're done
		try (BufferedReader br = new BufferedReader(new InputStreamReader(stream,StandardCharsets.UTF_8))) {
			if (!readOrbitalElementsData(br)) {
				br.close();
				ASTMsg.criticalErrMsg("The file specified is not an Orbital Elements data\n" +
						"file and could not be processed. Aborting program ...", ASTMisc.ABORT_PROG);					
			}

			// Close the file. We don't actually have to do this, but it's good practice.
			br.close();
			orbitalElementsLoaded = true;
			idxEarth = findOrbElementObjIndex("Earth");
			idxMoon = findOrbElementObjIndex("Moon");
			idxSun = findOrbElementObjIndex("Sun");
		} catch (IOException e) {
			ASTMsg.criticalErrMsg("The file specified is not an Orbital Elements data\n" +
					"file and could not be processed. Aborting program ...", ASTMisc.ABORT_PROG);
		}
	}

	/**
	 * This method reads the already opened orbital data file, looks for a
	 * specific tag, and returns its value. This only handles tags whose
	 * value is a real number.
	 * 
	 * @param br				buffered stream to read from
	 * @param tag				tag to look for
	 * @return					the real data value (or NA_VALUE) for the tag
	 * 
	 * This method also sets the global flag globalReadError if a read error
	 * occurs (e.g., tag not found) and the flag globalDataError if a
	 * data error occurs (e.g., data format is invalid). This rather clunky
	 * way of doing things is because Java functions can only return a single
	 * value (or object) and cannot modify input parameters. Since these
	 * globals are restricted to this class and only to a couple of methods,
	 * the potential liability of this technique is limited.
	 */
	private double readElement(BufferedReader br,String tag) {
		String strIn;
		ASTReal rTmp;
		double dResult = NA_VALUE;

		strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, tag));
		if (strIn == null) {
			globalReadError = true;
			return NA_VALUE;
		}
		if (strIn.length() <= 0) return NA_VALUE;

		rTmp = ASTReal.isValidReal(strIn,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) dResult = rTmp.getRealValue();
		else globalDataError = true;

		return dResult;
	}

	/**
	 * Reads an orbital elements data file to extract and validate its data.
	 * The data tags **must** be in the order specified by this code.
	 * 
	 * @param br			buffered stream to read from
	 * @return				true if successfully processed the orbital elements data.
	 */
	private boolean readOrbitalElementsData(BufferedReader br) {
		String strIn, str2, objName;
		ASTReal rTmp = new ASTReal();
		boolean readError = false, dataError = false;
		int i = -1;
		OrbElemObj obj = null;

		clearOrbitalElementObjects();

		// Validate that this is an orbital elements data file
		strIn=ASTFileIO.readTillStr(br,"<OrbitalElements>",ASTMisc.HIDE_ERRORS);
		if ((strIn == null) || (strIn.length() <= 0)) return false;

		// Get the epoch before reading in the individual orbital elements
		strIn=ASTFileIO.getSimpleTaggedValue(br,"<Epoch>");
		if (strIn == null) strIn = "";
		rTmp = ASTReal.isValidReal(strIn,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) epochDate = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Epoch Date at string [" + strIn + "]", "Invalid Epoch");
			return false;
		}

		strIn = ASTFileIO.getSimpleTaggedValue(br, "<JD>");
		if (strIn == null) strIn = "";
		rTmp = ASTReal.isValidReal(strIn,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) epochJD = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Julian Day Number at string [" + strIn + "]", "Invalid JD");
			return false;
		}

		// Look for the beginning of the data section with the orbital elements,
		// then cycle through and extract each object and its orbital elements.
		strIn=ASTFileIO.readTillStr(br,"<Data>",ASTMisc.HIDE_ERRORS);
		if ((strIn == null) || (strIn.length() <= 0)) return false;

		while ((strIn = ASTFileIO.getNonBlankLine(br)) != null) {
			readError = false;
			dataError = false;

			strIn = ASTStr.removeWhitespace(strIn);
			str2 = strIn.toLowerCase(Locale.US);
			if (str2.indexOf("</data>") >= 0) break;				// found end of data section

			// Convert the current tag to be an object's name
			strIn = ASTStr.replaceStr(strIn, "<", "");
			objName = ASTStr.replaceStr(strIn, ">", "");
			i = i + 1;

			obj = new OrbElemObj();
			obj.sName = objName;

			if (objName.compareToIgnoreCase("Sun") == 0) idxSun = i;
			else if (objName.compareToIgnoreCase("Moon") == 0) idxMoon = i;
			else if (objName.compareToIgnoreCase("Earth") == 0) idxEarth = i;

			strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, "<Inferior>"));
			readError = readError || ((strIn == null) || (strIn.length() <= 0));
			obj.bInferior = (strIn.compareToIgnoreCase("true") == 0);
			strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, "<Period>"));
			readError = readError || ((strIn == null) || (strIn.length() <= 0));
			if ((i == idxSun) || (i == idxMoon)) obj.dPeriod = NA_VALUE;
			else {
				rTmp = ASTReal.isValidReal(strIn,ASTMisc.HIDE_ERRORS);
				obj.dPeriod = rTmp.getRealValue();
				dataError = dataError || (!rTmp.isValidRealObj());
			}
			obj.dMass = readElement(br, "<Mass>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dRadius = readElement(br, "<Radius>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dDay = readElement(br, "<Day>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dEccentricity = readElement(br, "<Eccentricity>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dSemiMajAxisAU = readElement(br, "<SemiMajorAxisAU>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;

			if ((i == idxSun) || (i == idxMoon)) {
				obj.dSemiMajAxisKM = readElement(br, "<SemiMajorAxisKM>");
				readError = readError || globalReadError; dataError = dataError || globalDataError;
			} else {				// N/A for all the rest of the objects
				obj.dAngDiamArcSec = NA_VALUE;
				// Throw away; value is irrelevant
				strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, "<SemiMajorAxisKM>"));
				readError = readError || ((strIn == null) || (strIn.length() <= 0));    	
			}
			if ((i == idxEarth) || (i == idxMoon)) {
				obj.dAngDiamArcSec = NA_VALUE;
				strIn = ASTFileIO.getSimpleTaggedValue(br, "<AngDiamArcSec>");
				readError = readError || ((strIn == null) || (strIn.length() <= 0));    	
			} else {
				obj.dAngDiamArcSec = readElement(br, "<AngDiamArcSec>");
				readError = readError || globalReadError; dataError = dataError || globalDataError;
			}     
			if ((i == idxSun) || (i == idxMoon)) {
				obj.dAngDiamDeg = readElement(br, "<AngDiamDeg>");
				readError = readError || globalReadError; dataError = dataError || globalDataError;
			} else {
				obj.dAngDiamDeg = NA_VALUE;
				strIn = ASTFileIO.getSimpleTaggedValue(br, "<AngDiamDeg>");
				readError = readError || ((strIn == null) || (strIn.length() <= 0));  
			}
			if (i == idxEarth) {
				obj.dmV = NA_VALUE;
				strIn = ASTFileIO.getSimpleTaggedValue(br, "<VisualMagnitude>");
				readError = readError || ((strIn == null) || (strIn.length() <= 0));  
			} else {
				obj.dmV = readElement(br, "<VisualMagnitude>");
				readError = readError || globalReadError; dataError = dataError || globalDataError;
			}

			obj.dGravParm = readElement(br, "<GravParm>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dInclination = readElement(br, "<Inclination>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dLonAtEpoch = readElement(br, "<LonAtEpoch>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;
			obj.dLonAtPeri = readElement(br, "<LonAtPeri>");
			readError = readError || globalReadError; dataError = dataError || globalDataError;

			if (i == idxSun) {
				obj.dLonAscNode = NA_VALUE;
				strIn = ASTFileIO.getSimpleTaggedValue(br, "<LonAscNode>");
				readError = readError || ((strIn == null) || (strIn.length() <= 0)); 
			} else {
				obj.dLonAscNode = readElement(br, "<LonAscNode>");
				readError = readError || globalReadError; dataError = dataError || globalDataError;
			}

			if (readError || dataError) {
				ASTMsg.errMsg("The Orbital Elements data file has one or more\n" +
						"errors for the object [" + objName + "] Try another file.", "Orbital Elements Error");
				return false;
			}

			orbElementsDB.add(obj);

			// Throw away the end tag
			ASTFileIO.readTillStr(br, "</" + objName + ">");

		} // while loop

		if ((idxSun < 0) || (idxMoon < 0) || (idxEarth < 0)) {
			ASTMsg.errMsg("Orbital Elements for the Sun, Moon, and Earth\n" +
					"are required but one or more are missing.", "Missing Orbital Elements");
			return false;
		}

		orbitalElementsLoaded = true;
		return true;
	}

}
