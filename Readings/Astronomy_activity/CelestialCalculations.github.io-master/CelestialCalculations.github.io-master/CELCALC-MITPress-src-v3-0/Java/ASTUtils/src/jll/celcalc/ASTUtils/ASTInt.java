package jll.celcalc.ASTUtils;

/**
 * <b>Provides a class for creating integer objects and validating
 * that a string has a valid integer value.</b>
 * <p>
 * This class is useful because Java passes all parameters to a method by value. Hence,
 * in Java one cannot have a statement such as
 * <p>
 * <code>bValid = isValidReal(String inputStr, double dResult)</code>
 * <p>
 * where the <i><code>isValidReal</code></i> method returns a validity (true/false) and
 * modifies <code>dResult</code> to have the value parsed from <code>inputStr</code>.
 * Passing objects rather than primitives such as double avoids this problem.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTInt {
	private boolean bValid;					// true if the object represents a valid integer
	private int iValue;						// the object's value

	/**
	 * Class constructor.
	 * <p>
	 * Since much of the usage of this class involves validating that a string has a valid integer,
	 * assume a new object is invalid until it is proven otherwise.
	 */
	public ASTInt() {
		bValid = false;
		iValue = 0;
	}

	/*===================================================================
	 * Define 'get' and 'set' accessors for the object's fields
	 *==================================================================*/

	/**
	 * Returns whether the object is a valid integer
	 * 
	 * @return			true if a valid integer
	 */
	public boolean isValidIntObj() {
		return this.bValid;
	}

	/**
	 * Gets the integer value in the object
	 * 
	 * @return		integer value contained in the object
	 */
	public int getIntValue() {
		return this.iValue;
	}
	
	/**
	 * Sets the integer value in the object
	 * 
	 * @param N			value to set
	 */
	public void setIntValue(int N) {
		this.bValid = true;
		this.iValue = N;
	}
	
	/**
	 * Sets whether the object is a valid integer
	 * 
	 * @param flag		whether the object is a valid integer
	 */
	public void setValidIntObj(boolean flag) {
		this.bValid = flag;
		this.iValue = 0;
	}

	/*===============================================================================
	 * Define some general methods for validating that a string has a valid integer
	 * in it.
	 *==============================================================================*/

	/**
	 * Check to see if a valid integer was entered, but don't
	 * display any error messages unless flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow the flag for
	 * whether to display error messages to be optional.
	 * The default is to show error messages.
	 * 
	 * @param inputStr		string to be validated
	 * @return				an ASTInt object containing the result
	 */
	public static ASTInt isValidInt(String inputStr) {
		return isValidInt(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Check to see if a valid integer was entered, but don't
	 * display any error messages unless flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow the flag for
	 * whether to display error messages to be optional.
	 * The default is to show error messages.
	 * 
	 * @param inputStr		string to be validated
	 * @param flag			whether to display error messages 
	 * @return				an ASTInt object containing the result
	 */
	public static ASTInt isValidInt(String inputStr,boolean flag) {
		ASTInt resultObj = new ASTInt();

		resultObj.bValid = false;

		inputStr = ASTStr.removeWhitespace(inputStr);
		try
		{
			resultObj.iValue = Integer.parseInt(inputStr);
			resultObj.bValid = true;
		}
		catch(Exception e)
		{
			ASTMsg.errMsg("Either nothing was entered or what was entered\n" +
					"is invalid. Please enter a valid integer.","Invalid Integer",flag);
		}

		return resultObj;
	}
}
