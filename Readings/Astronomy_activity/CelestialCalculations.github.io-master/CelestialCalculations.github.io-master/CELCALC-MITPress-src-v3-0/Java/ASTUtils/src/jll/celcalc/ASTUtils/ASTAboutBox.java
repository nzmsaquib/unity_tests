package jll.celcalc.ASTUtils;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

/**
 * <b>Provides an "About Box" for the programs</b>
 * <p>
 * This About Box GUI was created by, and is maintained by, the 
 * Eclipse WindowBuilder. Some 'hand editing' was done to use the 
 * styles in ASTStyle to enforce a common GUI style across all programs.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
public class ASTAboutBox extends JDialog {

	// save the parent frame from the routine that creates the About Box so that the
	// About Box can be centered on the parent
	private JFrame parentFrame = null;
	
	/**
	 * Creates an About Box instance.
	 * 
	 * @param chapter	which chapter this about box is for
	 */
	public ASTAboutBox(String chapter) {
		setType(Type.POPUP);
		setResizable(false);
		getContentPane().setBackground(UIManager.getColor("CheckBoxMenuItem.background"));
				
		setTitle("About ...");
		setBounds(100, 100, 510, 339);
		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setFont(ASTStyle.ABOUTBOOKTITLE_BOLDFONT);
		lblBookTitle.setOpaque(true);
		lblBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panelChapterInfo = new JPanel();
		panelChapterInfo.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		JLabel labelChapterTitle = new JLabel(chapter);
		labelChapterTitle.setVerifyInputWhenFocusTarget(false);
		labelChapterTitle.setHorizontalTextPosition(SwingConstants.CENTER);
		labelChapterTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelChapterTitle.setFont(ASTStyle.ABOUTCHAPTITLE_BOLDFONT);
		labelChapterTitle.setAlignmentY(0.0f);
		
		JLabel labelVersion = new JLabel("(S/W Version "+ASTBookInfo.CODE_VER+")");
		labelVersion.setHorizontalTextPosition(SwingConstants.CENTER);
		labelVersion.setHorizontalAlignment(SwingConstants.CENTER);
		labelVersion.setFont(ASTStyle.ABOUTVERSION_FONT);
		labelVersion.setAlignmentX(0.5f);
		GroupLayout gl_panelChapterInfo = new GroupLayout(panelChapterInfo);
		gl_panelChapterInfo.setHorizontalGroup(
			gl_panelChapterInfo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelChapterInfo.createSequentialGroup()
					.addContainerGap(75, Short.MAX_VALUE)
					.addComponent(labelVersion)
					.addGap(65))
				.addComponent(labelChapterTitle, GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
		);
		gl_panelChapterInfo.setVerticalGroup(
			gl_panelChapterInfo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelChapterInfo.createSequentialGroup()
					.addContainerGap()
					.addComponent(labelChapterTitle)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(labelVersion)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panelChapterInfo.setLayout(gl_panelChapterInfo);
		
		JPanel panelBookInfo = new JPanel();
		
		JLabel labelEdition = new JLabel(ASTBookInfo.BOOK_EDITION);
		labelEdition.setHorizontalTextPosition(SwingConstants.CENTER);
		labelEdition.setHorizontalAlignment(SwingConstants.CENTER);
		labelEdition.setFont(ASTStyle.ABOUTEDITION_BOLDFONT);
		labelEdition.setAlignmentX(0.5f);
		
		JLabel labelCopyright = new JLabel(ASTBookInfo.BOOK_COPYRIGHT);
		labelCopyright.setHorizontalTextPosition(SwingConstants.CENTER);
		labelCopyright.setHorizontalAlignment(SwingConstants.CENTER);
		labelCopyright.setFont(ASTStyle.ABOUTCOPYRIGHT_FONT);
		labelCopyright.setAlignmentX(0.5f);
		
		JLabel labelAuthor = new JLabel(ASTBookInfo.BOOK_AUTHOR);
		labelAuthor.setHorizontalTextPosition(SwingConstants.CENTER);
		labelAuthor.setHorizontalAlignment(SwingConstants.CENTER);
		labelAuthor.setFont(ASTStyle.ABOUTAUTHOR_FONT);
		labelAuthor.setAlignmentX(0.5f);
		GroupLayout gl_panelBookInfo = new GroupLayout(panelBookInfo);
		gl_panelBookInfo.setHorizontalGroup(
			gl_panelBookInfo.createParallelGroup(Alignment.LEADING)
				.addGap(0, 264, Short.MAX_VALUE)
				.addGroup(gl_panelBookInfo.createSequentialGroup()
					.addGroup(gl_panelBookInfo.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelBookInfo.createSequentialGroup()
							.addGap(125)
							.addComponent(labelEdition))
						.addGroup(gl_panelBookInfo.createSequentialGroup()
							.addGap(72)
							.addComponent(labelCopyright))
						.addGroup(gl_panelBookInfo.createSequentialGroup()
							.addGap(67)
							.addComponent(labelAuthor)))
					.addContainerGap(74, Short.MAX_VALUE))
		);
		gl_panelBookInfo.setVerticalGroup(
			gl_panelBookInfo.createParallelGroup(Alignment.LEADING)
				.addGap(0, 90, Short.MAX_VALUE)
				.addGroup(gl_panelBookInfo.createSequentialGroup()
					.addGap(6)
					.addComponent(labelEdition)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(labelCopyright)
					.addGap(13)
					.addComponent(labelAuthor)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panelBookInfo.setLayout(gl_panelBookInfo);
		
		JLabel labelImage = new JLabel("");
		labelImage.setIcon(new ImageIcon(ASTAboutBox.class.getResource("/resources/PillarsOfCreation-EagleNebula.png")));
		labelImage.setHorizontalTextPosition(SwingConstants.CENTER);
		labelImage.setAlignmentX(Component.CENTER_ALIGNMENT);
		labelImage.setHorizontalAlignment(SwingConstants.CENTER);
		labelImage.setFocusable(false);
		labelImage.setFocusTraversalKeysEnabled(false);
		
		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		textPane.setText("'Pillars of Creation' in the Eagle Nebula taken in 2014 through the Hubble Space Telescope. (Image courtesy of NASA/ESA/Hubble Heritage Team)");
		textPane.setBackground(SystemColor.menu);
		textPane.setAlignmentY(0.0f);
		textPane.setAlignmentX(0.0f);
		
		JButton okButton = new JButton("OK");
		
		okButton.setFont(ASTStyle.ABOUTBTN_FONT);			
		okButton.setActionCommand("OK");
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		getRootPane().setDefaultButton(okButton);
	
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(lblBookTitle, GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(labelImage, GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(panelBookInfo, GroupLayout.DEFAULT_SIZE, 299, Short.MAX_VALUE)
						.addComponent(panelChapterInfo, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE))
					.addContainerGap())
				.addComponent(textPane, GroupLayout.DEFAULT_SIZE, 489, Short.MAX_VALUE)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(217)
					.addComponent(okButton, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(217, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblBookTitle, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panelBookInfo, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(panelChapterInfo, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE))
						.addComponent(labelImage, 0, 0, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textPane, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
					.addComponent(okButton)
					.addGap(10))
		);
		getContentPane().setLayout(groupLayout);
		
		ASTStyle.setASTStyle();

	}
	
	/**
	 * Saves a reference to the caller's parent frame so the About Box can be centered on the 
	 * parent application's window.
	 * 
	 * @param parent		parent frame for the main application
	 */
	public void setParentFrame(JFrame parent) {
		parentFrame = parent;
	}
	
	/**
	 * Shows the About Box
	 */
	public void showAboutBox() {
		this.setLocationRelativeTo(parentFrame);
		this.setVisible(true);
	}
	
}
