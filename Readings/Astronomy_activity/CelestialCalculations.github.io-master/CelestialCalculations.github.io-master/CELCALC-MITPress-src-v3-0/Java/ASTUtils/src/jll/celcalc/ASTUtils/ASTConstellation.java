package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <b>Provides useful information about the 88 modern
 * constellations, including their boundaries, a method for
 * determining what constellation a given RA/Decl falls within,
 * and other useful methods for accessing constellation information.</b>
 * <p>
 * All data and methods are declared shared because it doesn't make
 * sense to have multiple constellation databases in an application.
 * Unless otherwise noted, RA is given in hours, Decl is given in
 * degrees, and both coordinates are w.r.t. Epoch 2000.0.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTConstellation {
	// define the class instance variables that will contain the data for an individual constellation
	private String sName;					// the constellation's name
	private String sAbbrevName;				// the constellation's international 3-character abbreviation
	private String sMeaning;				// what the constellation's name means (e.g., Andromeda means 'The Chained Maiden')
	private double dCenterRA;				// right ascension for the center of the constellation
	private double dCenterDecl;				// declination for the center of the constellation.
	private String sBrightestStar;			// name of the brightest star in the constellation
	private double dStarRA;					// right ascension for the brightest star in the constellation
	private double dStarDecl;				// declination for the brightest star in the constellation

	private static final int NUM_CONSTELLATIONS = 88;			// # of modern constellations

	// we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
	private static ASTPrt prt = null;		

	// Name of the data file containing the constellation data, its location,
	// and defaults in case the user must find it.
	private static final String dataFilename = "ConstellationData.dat";
	private static final String fullFilename = "/resources/"+dataFilename;
	private static final FileNameExtensionFilter fileExtFilter = new FileNameExtensionFilter("Constellations Data (ConstellationsData.dat)","dat");

	// avoid initializing more than once
	private static boolean constDBReadIn = false;

	// The only access to the constellations database (constDB) that is allowed external
	// to this class is to get elements within the database, which is provided through 
	// 'get' accessors defined below. The constellation database is an array of the
	// class instance variables.
	private static ASTConstellation[] constDB = new ASTConstellation[NUM_CONSTELLATIONS];

	/*==================================================================================
	 * Define 'get' accessors. There is no need for 'set' accessors because only the
	 * methods in this class set the class instance variables. Note that the
	 * constellations are stored as an array, so a 'get' must specify which
	 * constellation is desired	from that array.
	 *=================================================================================*/

	/**
	 * Gets the number of constellations in the database
	 * 
	 * @return			number of constellations in the database
	 */
	public static int getNumConstellations() {
		return NUM_CONSTELLATIONS;
	}

	/**
	 * Gets a constellation's name
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the constellation's name, or null if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static String getConstName(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return null;
		else return constDB[idx].sName;
	}

	/**
	 * Gets a constellation's international 3-character abbreviated name
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the constellation's abbreviated name, or null if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static String getConstAbbrevName(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return null;
		else return constDB[idx].sAbbrevName;
	}

	/**
	 * Gets a constellation's 'meaning' (e.g., Andromeda means 'The Chained Maiden')
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the constellation's meaning field, or null if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static String getConstMeaning(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return null;
		else return constDB[idx].sMeaning;
	}

	/**
	 * Gets the right ascension (Epoch J2000.0) for the constellation's center
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the RA for constellation's center, or UNKNOWN_RA_DECL if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static double getConstCenterRA(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return ASTMisc.UNKNOWN_RA_DECL;
		else return constDB[idx].dCenterRA;
	}

	/**
	 * Gets the declination (Epoch J2000.0) for the constellation's center
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the Decl for constellation's center, or UNKNOWN_RA_DECL if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static double getConstCenterDecl(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return ASTMisc.UNKNOWN_RA_DECL;
		else return constDB[idx].dCenterDecl;
	}

	/**
	 * Gets the name of the brightest star in the constellation
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			name of the brightest star, or null if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static String getConstBrightestStarName(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return null;
		else return constDB[idx].sBrightestStar;
	}

	/**
	 * Gets the right ascension (Epoch J2000.0) for the constellation's brightest star
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the RA for the constellation's brightest star, or UNKNOWN_RA_DECL if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static double getConstBrightestStarRA(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return ASTMisc.UNKNOWN_RA_DECL;
		else return constDB[idx].dStarRA;
	}

	/**
	 * Gets the declination (Epoch J2000.0) for the constellation's brightest star
	 * 
	 * @param idx		which constellation is being referenced
	 * @return			the Decl for the constellation's brightest star, or UNKNOWN_RA_DECL if idx is invalid. 0-based
	 * 					indexing is assumed!
	 */
	public static double getConstBrightestStarDecl(int idx) {
		if ((idx < 0) || (idx > (NUM_CONSTELLATIONS - 1))) return ASTMisc.UNKNOWN_RA_DECL;
		else return constDB[idx].dStarDecl;
	}

	/*-----------------------------------------------------------------------------------------
	 * Define an internal class and database for boundaries data, which define the boundaries
	 * for a constellation. The boundaries database can't be part of constDB because boundaries
	 * are not sorted in the same order as the constellations database. The boundaries database
	 * is sorted in a very specific way to make it easier to find what constellation a given
	 * RA/Decl falls within. Note that all RA/Decl coordinates in the boundaries database are
	 * defined w.r.t. the <b>1875.0 Epoch</b>, not J2000.0.
	 *-----------------------------------------------------------------------------------------*/
	private static class BoundaryData {		// must be static so that an ASTConstellation instance is not required
		private double dRAlower;			// Lower RA boundary for a constellation
		private double dRAupper;			// Upper RA boundary for this constellation
		private double dDecl;				// Declination boundary for this constellation
		private int idx;					// Index into the constDB for this boundary
	}

	// database of constellation boundaries
	private static List<BoundaryData> boundariesDB = new ArrayList<BoundaryData>();
	
	/*--------------------------------------------------------------------------
	 * Define some other data items that are used only in this class.
	 *-------------------------------------------------------------------------*/
	
	// Various constants used in the precession and findConstellation methods
	private static final double CDR = 0.17453292519943e-01;
	private static final double CONVH = 0.2617993878;
	private static final double CONVD = 0.1745329251994e-01;
	private static final double PI4 = 6.28318530717948;
	private static final double E75 = 1875.0;
	private static final int RA_IDX = 0;
	private static final int DECL_IDX = 1;

	/*============================================================================
	 * Public methods for getting/displaying/manipulating constellation info
	 *===========================================================================*/
	
	/**
	 * Gets whether the constellations database has been loaded.
	 * 
	 * @return		true if constellations have been loaded.
	 */
	public static boolean areConstellationsLoaded() {
		return constDBReadIn;
	}

	/**
	 * Displays a list of all the constellations.
	 */
	public static void displayAllConstellations() {
		checkInitConstellationsDone();

		prt.println(String.format("%-20s %5s %-30s %-25s","Constellation Name","Abrv","   Meaning","Brightest Star"));
		prt.println(String.format("%90s", "=").replace(' ', '='));
		for (int i = 0; i < getNumConstellations(); i++) {
			prt.println(String.format("%-20s %5s %-30s %-25s",getConstName(i),getConstAbbrevName(i),
					getConstMeaning(i),getConstBrightestStarName(i)));
		}
	}

	/**
	 * Displays data for a constellation.
	 * 
	 * @param idx		index into constDB for the constellation to display. 0-based
	 * 					indexing is assumed!
	 */
	public static void displayConstellation(int idx) {
		int n;

		checkInitConstellationsDone();

		if ((idx < 0) || (idx > (ASTConstellation.NUM_CONSTELLATIONS - 1))) {
			prt.println("Constellation does not exist");
			return;
		}

		prt.setBoldFont(true);
		prt.println(getConstName(idx) + " (" + getConstAbbrevName(idx) +")",ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.println("The Constellation's name means '" + getConstMeaning(idx) + "'");
		prt.print("Its center is located at " + ASTTime.timeToStr(getConstCenterRA(idx),ASTMisc.HMSFORMAT) + " RA, ");
		prt.println(ASTAngle.angleToStr(getConstCenterDecl(idx),ASTMisc.DMSFORMAT) + " Decl (Epoch 2000.0)");
		prt.println();

		prt.print("Its Brightest Star (" + getConstBrightestStarName(idx)+") ");
		prt.print("is located at " + ASTTime.timeToStr(getConstBrightestStarRA(idx),ASTMisc.HMSFORMAT) + " RA, ");
		prt.println(ASTAngle.angleToStr(getConstBrightestStarDecl(idx), ASTMisc.DMSFORMAT) + " Decl (Epoch 2000.0)");
		prt.println();

		prt.println("The Constellation's boundaries (for Epoch 1875.0) are:");
		prt.setFixedWidthFont();

		prt.println(String.format("%-12s %-12s %-16s","   Lower RA","   Upper RA","       Decl"));
		prt.println(String.format("%43s", "=").replace(' ', '='));

		for (int i = 0; i < ASTConstellation.boundariesDB.size(); i++) {
			n = ASTConstellation.boundariesDB.get(i).idx;
			if (n == idx) {
				prt.println(String.format("%12s %12s %16s",ASTTime.timeToStr(boundariesDB.get(i).dRAlower,ASTMisc.HMSFORMAT),
						ASTTime.timeToStr(boundariesDB.get(i).dRAupper,ASTMisc.HMSFORMAT),
						ASTAngle.angleToStr(boundariesDB.get(i).dDecl,ASTMisc.DMSFORMAT)));
			}
		}

		prt.setProportionalFont();

	}
	
	/**
	 * Searches the constellations database and returns the index into the database for
	 * the requested constellation by its abbreviated name.
	 * <p>
	 * The search performed is <b>not</b> case sensitive, but an exact match, ignoring white space,
	 * is required.
	 * 
	 * @param targ			Abbreviated name of the constellation to find
	 * @return				If successful, returns an index into the constellations database
	 * 						for the requested constellation. If unsuccessful, -1 is returned.
	 * 						0-based	indexing is assumed!
	 */
	public static int findConstellationByAbbrvName(String targ) {
		checkInitConstellationsDone();

		String targStr = ASTStr.removeWhitespace(targ);

		for (int i = 0; i < NUM_CONSTELLATIONS; i++) {
			if (targStr.equalsIgnoreCase(getConstAbbrevName(i))) return i;
		}

		return -1;			// not found
	}

	/**
	 * Searches the constellations database and returns an index into the database for all
	 * constellations that contain the target substring in their 'meaning' field.
	 * <p>
	 * The search performed is <b>not</b> case sensitive.
	 * 
	 * @param targ			target substring to search for
	 * @return				If successful, returns a list of indices into the constellations database
	 * 						for the requested constellations. If unsuccessful, the list is empty.
	 * 						0-based	indexing is assumed for the indices!
	 */
	public static List<Integer> findConstellationsByMeaning(String targ) {
		checkInitConstellationsDone();

		List<Integer> result = new ArrayList<Integer>();

		String tmpStr;
		String targStr = ASTStr.removeWhitespace(targ.toLowerCase(Locale.US));

		for (int i = 0; i < NUM_CONSTELLATIONS; i++) {
			tmpStr = ASTStr.removeWhitespace(getConstMeaning(i).toLowerCase(Locale.US));
			if (tmpStr.indexOf(targStr) >= 0) result.add(i);
		}

		return result;
	}

	/**
	 * Searches the constellations database and returns the index into the database for
	 * the requested constellation by its name.
	 * <p>
	 * The search performed is <b>not</b> case sensitive, but an exact match, ignoring white space,
	 * is required.
	 * 
	 * @param targ			Constellation name to find
	 * @return				If successful, returns an index into the constellations database
	 * 						for the requested constellation. If unsuccessful, -1 is returned.
	 * 						0-based	indexing is assumed!
	 */
	public static int findConstellationByName(String targ) {
		checkInitConstellationsDone();

		String targStr = targ.trim();

		for (int i = 0; i < NUM_CONSTELLATIONS; i++) {
			if (targStr.equalsIgnoreCase(getConstName(i))) return i;
		}

		return -1;			// not found
	}	

	/**
	 * Find the constellation that a particular RA/Decl falls within.
	 * <p>
	 * The code for this method was converted from the C and Fortran
	 * code found in the VizieR archives, http://cdsarc.u-strasbg.fr/viz-bin/Cat?VI/42. The
	 * code is translated somewhat literally, taking into account that array indexing in Java
	 * is 0 based whereas Fortran is not.
	 * 
	 * @param RAIn				right ascension in decimal hours (actually, hour angle)
	 * @param DeclIn			declination in decimal degrees
	 * @param EpochIn			epoch in which the RAIn/DeclIn are given
	 * @return					index into the constellations database for the constellation in which the
	 * 							RA/Decl falls, or -1 if there is a problem
	 */
	public static int findConstellationFromCoord(double RAIn, double DeclIn,double EpochIn) {
		/*
		 * The author of the C program from which this translation came used a macro to adjust arrays
		 * (e.g., translate X[i] to x[i-1]) to take the indexing base into account. The C author also
		 * used macros to define the trig functions (e.g., #define DCOS	cos). Neither of these 
		 * conventions are preserved in the translation below. A major alteration to the original algorithm is that
		 * the boundariesDB (boundaries database) is memory resident rather than constantly
		 * opening and reading a datafile containing the constellation boundaries as
		 * the original program did.
		 */
		double ARAD,DRAD,A,D,RAH,RA,DEC,RAL,RAU,DECL,DECD;
		double[] precResult = new double[2];			// RA is index RA_IDX, Decl is index DECL_IDX
		int idx;
		int iConstOut = -1;
		
		checkInitConstellationsDone();
		
		RAH = RAIn;
		DECD = DeclIn;

		// Convert to radians and then precess the position to the 1875.0 Epoch
		ARAD = CONVH * RAH;
		DRAD = CONVD * DECD;
		precResult = HGTPrecession(ARAD,DRAD,EpochIn,E75);
		A = precResult[RA_IDX];
		D = precResult[DECL_IDX];
		if (A <  0.0) A=A+PI4;
		if (A >= PI4) A=A-PI4;

		// Convert radians back to degrees
		RA= A/CONVH;
		DEC=D/CONVD;

		// Now find the constellation such that the Declination entered is higher than
		// the lower boundary of the constellation when the upper and lower
		// Right Ascensions for the constellation bound the entered
		// Right Ascension
		for (int i = 0; i < boundariesDB.size(); i++) {
			RAL = boundariesDB.get(i).dRAlower;
			RAU = boundariesDB.get(i).dRAupper;
			DECL = boundariesDB.get(i).dDecl;

			if (DECL >  DEC) continue;
			if (RAU <= RA) continue;
			if (RAL >  RA) continue;

			// If the constellation has been found, save the result and continue
			// to the next boundariesDB entry. Otherwise continue the search by
			// returning to RAU
			if ((RA >= RAL) && (RA <  RAU) &&  (DECL <= DEC)) {
				idx = boundariesDB.get(i).idx;
				iConstOut = idx;
			}
			else if (RAU <  RA) continue;
			else ASTMsg.errMsg("Constellation not found for RA/DECL" + RAIn + "/" + DeclIn, "Constellation not found");

			break;
		}

		return iConstOut;
	}

	/**
	 * Does a one-time initialization by reading in the constellation data file.
	 * 
	 * @param prtInstance		instance for performing output to the application's scrollable text output area
	 * @throws IOException 		Error in trying to read the constellations data file.
	 */
	public static void initConstellations(ASTPrt prtInstance) throws IOException {
		if (constDBReadIn) return;			// we've already read in the constellations data file

		if (prtInstance == null) ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ASTMisc.ABORT_PROG);

		prt = prtInstance;

		InputStream stream = null;
		String[] fileToRead = new String[2];
		String str;

		// See if the file exists where it is expected. If not, ask the user to find it.
		if (ASTConstellation.class.getResource(fullFilename) == null) {
			ASTMsg.errMsg("The constellations data file ("+dataFilename+") is missing.\n" +
					"Please click 'OK' and then manually find the constellations data file", "Missing Constellations Data File");

			fileToRead = ASTFileIO.getFileToRead("Load Constellations Data ...","Constellation Data",fileExtFilter);
			if ((fileToRead[ASTFileIO.FILENAME_IDX] == null) || (fileToRead[ASTFileIO.FILENAME_IDX].length() <= 0)) {
				ASTMsg.criticalErrMsg("Could not read the "+dataFilename+" file ... Aborting ...",ASTMisc.ABORT_PROG);
			} else stream = new FileInputStream(fileToRead[ASTFileIO.FULLPATHNAME_IDX]);
		} else stream = ASTConstellation.class.getResourceAsStream(fullFilename);

		// Validate that the file is indeed a constellations data file
		// Note that we're using try-with-resources so we don't have to explicitly close the stream when we're done
		try (BufferedReader br = new BufferedReader(new InputStreamReader(stream,StandardCharsets.UTF_8))) {
			str=ASTFileIO.readTillStr(br,"<Constellations>",ASTMisc.HIDE_ERRORS);
			if (str.length() > 0) {
				// Now that we've found the constellations data file, read in the data.
				// Note that we must read the basic data and then the sorted data because
				// that's how the data file is structured.					
				if (!readBasicConstData(br)) {
					ASTMsg.criticalErrMsg("The Constellations Data File is required but could\n" +
							"not be processed. Aborting program ...", ASTMisc.ABORT_PROG);				
					return;
				}

				// Although full initialization has not been completed, enough has been done
				// to have constellations in place to now read in the constellation boundaries.
				// We **must** set the flag here because reading in the sorted boundaries data assumes
				// that the basic constellations have already been successfully read in.
				constDBReadIn = true;				
				if (!readSortedBoundariesData(br)) {
					ASTMsg.criticalErrMsg("The Constellations Data File is required but could\n" +
							"not be processed. Aborting program ...", ASTMisc.ABORT_PROG);		
					return;
				}
			}
		} catch (IOException e) {
			ASTMsg.criticalErrMsg("The file specified is not a Constellations data\n" +
					"file and could not be processed. Aborting program ...", ASTMisc.ABORT_PROG);
		}

	}

	/*----------------------------------------------------------------------------------
	 * Private methods used only in this class
	 *---------------------------------------------------------------------------------*/

	/**
	 * Checks to see if this class has been properly initialized. This must
	 * be done to be sure none of the shared procedures (e.g., displayAllConstellations)
	 * is invoked before the class has been initialized. This method doesn't
	 * return anything. It displays an error message and aborts the running
	 * program if there is a problem.
	 */
	private static void checkInitConstellationsDone() {
		if (!constDBReadIn) ASTMsg.criticalErrMsg("The constellations database has not been loaded ... Aborting program ...",ASTMisc.ABORT_PROG);
	}

	/**
	 * This method performs a precession correction to convert an RA/Decl from one Epoch to another.
	 * <p>
	 * The algorithm used here in the Herget Precession algorithm published on page 9
	 * of PUBL. CINCINNATI OBS. NO. 24. The code below was converted from the C and Fortran
	 * code found in the VizieR archives, http://cdsarc.u-strasbg.fr/viz-bin/Cat?VI/42. The
	 * code is translated almost literally, taking into account that array indexing in Java
	 * is 0 based whereas Fortran is not. The author of the C program from which this
	 * translation came used a macro to adjust arrays (e.g., translate X[i] to x[i-1])
	 * to take the indexing base into account. The C author also used macros to define
	 * the trig functions (e.g., #define DCOS	cos) which are not preserved
	 * in the translation below.
	 * 
	 * @param RAIn			Right Ascension in radians for EpochIn
	 * @param DeclIn		Declination in radians for EpochOut
	 * @param EpochIn		Epoch that RAIn/DeclIn is give in
	 * @param EpochOut		Epoch to convert to
	 * @return				returns a double[] array with the precessed RA in item RA_IDX and the
	 * 						precessed Decl in item DECL_IDX of the array
	 */
	private static double[] HGTPrecession(double RAIn,double DeclIn,double EpochIn,double EpochOut) {
		double[] result = new double[2];			// RA is index 0, Decl is index 1
		double EP1=0.0, EP2=0.0;
		double[] x1 = new double[3];
		double[] x2 = new double[3];
		double[][] r = new double[3][3];
		double T,ST,A,B,C,CSR,SINA,SINB,SINC,COSA,COSB,COSC;
		int i,j;

		// Compute input direction cosines
		A=Math.cos(DeclIn);
		x1[0]=A*Math.cos(RAIn);
		x1[1]=A*Math.sin(RAIn);
		x1[2]=Math.sin(DeclIn);

		// Set up rotation matrix (R)
		// Don't compare Epochs directly since equality comparison for real numbers is problematic due to roundoff errors
		if ((Math.abs(EP1 - EpochIn) < ASTMath.EPS) &&  (Math.abs(EP2 - EpochOut) < ASTMath.EPS)) ;  // do nothing
		else {
			EP1 = EpochIn;
			EP2 = EpochOut;
			CSR=CDR/3600.0;
			T=0.001*(EP2-EP1);
			ST=0.001*(EP1-1900.0);
			A=CSR*T*(23042.53+ST*(139.75+0.06*ST)+T*(30.23-0.27*ST+18.0*T));
			B=CSR*T*T*(79.27+0.66*ST+0.32*T)+A;
			C=CSR*T*(20046.85-ST*(85.33+0.37*ST)+T*(-42.67-0.37*ST-41.8*T));
			SINA=Math.sin(A);
			SINB=Math.sin(B);
			SINC=Math.sin(C);
			COSA=Math.cos(A);
			COSB=Math.cos(B);
			COSC=Math.cos(C);
			r[0][0]=COSA*COSB*COSC-SINA*SINB;
			r[0][1]=-COSA*SINB-SINA*COSB*COSC;
			r[0][2]=-COSB*SINC;
			r[1][0]=SINA*COSB+COSA*SINB*COSC;
			r[1][1]=COSA*COSB-SINA*SINB*COSC;
			r[1][2]=-SINB*SINC;
			r[2][0]=COSA*SINC;
			r[2][1]=-SINA*SINC;
			r[2][2]=COSC;
		}

		// Perform the rotation to get the direction cosines at EpochOut
		for (i=0; i<3; i++) {
			x2[i]=0.0;
			for (j=0; j<3; j++) x2[i] += r[i][j]*x1[j];
		}

		result[0] = Math.atan2(x2[1],x2[0]);		// precessed RA
		if (result[0] <  0) result[0] = 6.28318530717948 + result[0];
		result[1] = Math.asin(x2[2]);				// precessed Decl

		return result;
	}

	/**
	 * Reads the constellations data file to extract basic info about the constellations
	 * 
	 * @param br			file to read from
	 * @return				false if an error occurs
	 */
	private static boolean readBasicConstData(BufferedReader br) {
		String str,str2;
		String[] parts;
		ASTReal tmpReal = new ASTReal();

		// count will be used to count the # of constellations actually read. If it differs from
		// NUM_CONSTELLATIONS, we have an error and must abort. i will be used as the constDB index
		// and must start at 0 since Java arrays are 0-based.
		int count = 0;
		int i = -1;

		str = ASTFileIO.readTillStr(br,"<Data>");
		if (str == null) return false;

		try {
			while ((str = br.readLine()) != null) {
				str2 = ASTStr.removeWhitespace(str.toLowerCase(Locale.US));
				if (str2.indexOf("</data>") >= 0) {						// must use Lower Case since previous statement set it to LC!
					if (count == NUM_CONSTELLATIONS) return true;
					else {
						ASTMsg.criticalErrMsg("The constellations data file ("+dataFilename+") may be corrupted. It\n"+
								"contains "+count+" Constellations but should contain "+NUM_CONSTELLATIONS+". Thus, we must abort ...",ASTMisc.ABORT_PROG);
					}
				}

				count += 1;
				i += 1;
				if (count > NUM_CONSTELLATIONS) continue;		// Make sure we don't try to store too much data in constDB

				constDB[i] = new ASTConstellation();

				parts = str.split("[\\,]", -1);				// Must use -1 so that we get all fields, including null ones
				if (parts.length != 8) {
					ASTMsg.criticalErrMsg("Error while reading the constellation basic data\n" + "at string ["+str+"]");
					return false;
				}

				constDB[i].sAbbrevName = parts[0].trim();
				constDB[i].sName = parts[1].trim();
				constDB[i].sMeaning = parts[7].trim();
				constDB[i].sBrightestStar = parts[4].trim();				

				// Get the RA/Decl coordinates for the center of the constellation
				tmpReal = ASTReal.isValidReal(parts[2],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) constDB[i].dCenterRA = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid RA for the constellation center\n" + "at string ["+str+"]");
					return false;
				}

				tmpReal = ASTReal.isValidReal(parts[3],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) constDB[i].dCenterDecl = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid Decl for the constellation center \n" +	"at string ["+str+"]");
					return false;
				}

				// Get the RA/Decl coordinates for the brightest star in the constellation
				tmpReal = ASTReal.isValidReal(parts[5],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) constDB[i].dStarRA = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid RA for the Brightest Star\n" +	"at string ["+str+"]");
					return false;
				}

				tmpReal = ASTReal.isValidReal(parts[6],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) constDB[i].dStarDecl = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid Decl for the Brightest Star\n" + "at string ["+str+"]");
					return false;
				}

			} // for loop
		} catch(Exception e) {
			ASTMsg.criticalErrMsg("Error reading basic constellation data from the file "+dataFilename);
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Reads the sorted database of constellations and puts it into the boundariesDB structure.
	 * 
	 * @param br			file to read from
	 * @return				false if an error occurs
	 */
	private static boolean readSortedBoundariesData(BufferedReader br) {
		String str,targ;
		String[] parts;
		ASTReal tmpReal = new ASTReal();		

		// Look for the start of the constellation boundaries data
		str = ASTFileIO.readTillStr(br,"<SortedBoundaries>");
		if (str == null) return false;
		str = ASTFileIO.readTillStr(br,"<Data>");
		if (str == null) return false;	

		try {
			while ((str = br.readLine()) != null) {
				if (str.indexOf("</Data>") >= 0) return true;

				// Now parse the input string
				parts = str.split("[\\,]", -1);				// Must use -1 so that we get all fields, including null ones
				if (parts.length != 4) {
					ASTMsg.criticalErrMsg("Error in the constellation boundaries data\nat string ["+str+"]");
					return false;
				}

				BoundaryData obj = new BoundaryData();

				// Get the constellation boundaries (RA/Decl)
				tmpReal = ASTReal.isValidReal(parts[0],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) obj.dRAlower = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid Lower RA in the constellation boundaries data\nat string ["+str+"]");
					return false;
				}
				tmpReal=ASTReal.isValidReal(parts[1],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) obj.dRAupper = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid Upper RA in the constellation boundaries data\nat string ["+str+"]");
					return false;
				}
				tmpReal=ASTReal.isValidReal(parts[2],ASTMisc.HIDE_ERRORS);
				if (tmpReal.isValidRealObj()) obj.dDecl = tmpReal.getRealValue();
				else {
					ASTMsg.criticalErrMsg("Invalid Declination in the constellation boundaries data\nat string ["+str+"]");
					return false;
				}

				// Find and store the index into the constDB for this boundary
				targ = parts[3].trim();
				obj.idx = -1;

				// Find the index into the ConstDB for this boundary.		
				obj.idx = findConstellationByAbbrvName(targ);
				if (obj.idx < 0) ASTMsg.criticalErrMsg("Could not match boundary with a Constellation\nat string ["+str+"]");

				boundariesDB.add(obj);
			}			
		} catch (IOException e) {
			ASTMsg.criticalErrMsg("Error while reading the constellation boundaries data.");
			return false;
		}
		return true;
	}
}
