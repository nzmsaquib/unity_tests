package jll.celcalc.ASTUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>Provides a class for a date object and related methods.</b>
 * <p>
 * Methods provided include validating strings in the form
 * mm/dd/yyyy where all fields are required and yyyy could
 * be negative. The validate methods ensure that mm is in [1,12]
 * and that the day is in an appropriate range for the mm
 * including checking for leap years for Gregorian dates).
 * 
 * Note that different languages have date-related routines, but
 * they are not used here because (a) we need to allow
 * dates to be negative and (b) implementing these routines
 * rather than using a language's native date routines makes
 * it easier to translate to a different programming language.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTDate {
	// Class instance variables
	private boolean bValid;				// true if object holds a valid date
	private int iMonth;
	private int iDay;					// provide both int and double day to support Julian day number calculations
	private double dDay;
	private int iYear;

	/** days of the week */
	public static final String[] DaysOfWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
		
	/**
	 * Constructor to create a date object.
	 * 
	 * Since much of the usage of this class involves validating
	 * that a string has a valid date, assume a new object is invalid
	 * until it is proven otherwise.
	 */
	public ASTDate() {
		bValid = false;
		iMonth = 1;
		iDay = 1;
		dDay = 1.0;
		iYear = 0;
	}
	
	/*==================================================================
	 * Define 'get' and 'set' accessors for the object's fields.
	 *=================================================================*/
	
	/**
	 * Returns whether the object is a valid date
	 * 
	 * @return			true if a valid date, else false
	 */
	public boolean isValidDateObj() {
		return this.bValid;
	}

	/**
	 * Returns the month contained in the date object
	 * 
	 * @return		the month contained in the date object
	 */
	public int getMonth() {
		return this.iMonth;
	}
	
	// Unfortunately, we can't overload getDay because getiDay and getdDay differ
	// only in their return type. Hence, we must have two separate methods.
	/**
	 * Returns the day contained in the date object
	 * 
	 * @return		the day contained in the date object as an integer
	 */
	public int getiDay() {
		return this.iDay;
	}
	
	/**
	 * Returns the day contained in the date object
	 * 
	 * @return		the day contained in the date object as a double
	 */
	public double getdDay() {
		return this.dDay;
	}
	
	/**
	 * Returns the year contained in the date object
	 * 
	 * @return		the year contained in the date object
	 */
	public int getYear() {
		return this.iYear;
	}
	
	/**
	 * Sets the month in the date object.
	 * 
	 * @param month		month for this object
	 */
	public void setMonth(int month) {
		this.iMonth = month;
	}

	/**
	 * Sets the integer day in the date object
	 * 
	 * @param day		day for this object (as an integer)
	 */
	public void setiDay(int day) {
		this.iDay = day;
		this.dDay = (double) day;
	}

	/**
	 * Sets the double day in the date object
	 * 
	 * @param day		day for this object (as a double)
	 */
	public void setdDay(double day) {
		this.dDay = day;
		this.iDay = (int) day;
	}

	/**
	 * Sets the year in the date object
	 * 
	 * @param year			year for this object
	 */
	public void setYear(int year) {
		this.iYear = year;
	}
	
	/**
	 * Define a method for returning today's current date. This is easy
	 * to do, but is provided as a method to ease portability to other languages.
	 * 
	 * @return			current date in form MM/dd/yyyy
	 */
	public static String getCurrentDate() {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date dateobj = new Date();
		
		return df.format(dateobj);
	}

	
	/*==================================================================================
	 * Define some general methods for validating that a string contains a valid date.
	 *=================================================================================*/

	/**
	 * Check a string to see if contains a valid Date. Do not display any error
	 * message unless flag is SHOW_ERRORS to let the calling routine display
	 * its own message.
	 * 
	 * This method is overloaded to allow flexibility in whether error messages
	 * are displayed (default is to show errors) and whether the day must be
	 * an integer or can include a fractional part of the day.
	 * 
	 * The custom code here is needed rather than simply using Java's <code>SimpleDateFormat</code> because
	 * we want to allow for the possibility that yyyy could be negative. Note that range checking
	 * is done to ensure that mm and dd are in the proper ranges. A check is also made to ensure
	 * that dd = 29 only for leap years, but this only works for years in the Gregorian calendar
	 * (i.e., years greater than 1581).
	 * 
	 * @param inputStr			string to be validated
	 * @return					an ASTDate object with the result
	 */
	public static ASTDate isValidDate(String inputStr) {
		return isValidDate(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Check a string to see if contains a valid Date. Do not display any error
	 * message unless flag is SHOW_ERRORS to let the calling routine display
	 * its own message.
	 * 
	 * This method is overloaded to allow flexibility in whether error messages
	 * are displayed (default is to show errors) and whether the day must be
	 * an integer or can include a fractional part of the day.
	 * 
	 * The custom code here is needed rather than simply using Java's <code>SimpleDateFormat</code> because
	 * we want to allow for the possibility that yyyy could be negative. Note that range checking
	 * is done to ensure that mm and dd are in the proper ranges. A check is also made to ensure
	 * that dd = 29 only for leap years, but this only works for years in the Gregorian calendar
	 * (i.e., years greater than 1581).
	 * 
	 * @param inputStr			string to be validated
	 * @param flag				whether to display error messages 
	 * @return					an ASTDate object with the result
	 */
	public static ASTDate isValidDate(String inputStr, boolean flag) {
		return isValidDate(inputStr,flag,false);
	}
	/**
	 * Check a string to see if contains a valid Date. Do not display any error
	 * message unless flag is SHOW_ERRORS to let the calling routine display
	 * its own message.
	 * 
	 * This method is overloaded to allow flexibility in whether error messages
	 * are displayed (default is to show errors) and whether the day must be
	 * an integer or can include a fractional part of the day.
	 * 
	 * The custom code here is needed rather than simply using Java's <code>SimpleDateFormat</code> because
	 * we want to allow for the possibility that yyyy could be negative. Note that range checking
	 * is done to ensure that mm and dd are in the proper ranges. A check is also made to ensure
	 * that dd = 29 only for leap years, but this only works for years in the Gregorian calendar
	 * (i.e., years greater than 1581).
	 * 
	 * @param inputStr			string to be validated
	 * @param flag				whether to display error messages 
	 * @param allowFrac			if true, allow fractional part of
	 * 							a day to be in the input string
	 * 							and allow day to be 0
	 * @return					an ASTDate object with the result
	 */
	public static ASTDate isValidDate(String inputStr,boolean flag,boolean allowFrac) {
		ASTDate dateObj = new ASTDate();
		ASTInt iTempInt = new ASTInt();
		ASTReal dTempReal = new ASTReal();
		int iMaxDay = 28;						// Max day for February
		int iMinDay = 1;						// minimum day (could be 0 if allowFrac is true)
		
		dateObj.bValid = false;
		if (allowFrac) iMinDay = 0;				// allow 0 so routines can convert a '0 day' to a JD
		
		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter a valid Date in the form mm/dd/yyyy","Invalid Date format",flag);
			return dateObj;
		}
		
		String[] parts = inputStr.split("\\/");

		if (parts.length != 3) {							// Must have exactly 3 fields
			ASTMsg.errMsg("The Date entered is not in a valid format. Please enter\n" +
					"a valid Date in the form mm/dd/yyyy","Invalid Date format",flag);
			return dateObj;
		}
		
		// Get the year
		iTempInt = ASTInt.isValidInt(parts[2],ASTMisc.HIDE_ERRORS);
		if (iTempInt.isValidIntObj()) dateObj.iYear = iTempInt.getIntValue();
		else {
			ASTMsg.errMsg("The Year entered is not valid. Please enter\n" +
					"a valid Date in the form mm/dd/yyyy","Invalid Date (Year)",flag);
			return dateObj;
		}
		
		// Get month
		iTempInt = ASTInt.isValidInt(parts[0], ASTMisc.HIDE_ERRORS);
		if (iTempInt.isValidIntObj()) dateObj.iMonth = iTempInt.getIntValue();
		else {
			ASTMsg.errMsg("The Month entered is invalid. Please enter\n" +
					"a valid Date in the form mm/dd/yyyy","Invalid Date (Month)",flag);
			return dateObj;
		}
		if ((dateObj.iMonth < 1) || (dateObj.iMonth > 12)) {
			ASTMsg.errMsg("The Month entered is out of range. Please enter\n" +
					"a valid Date in the form mm/dd/yyyy","Invalid Date (Month)",flag);
			return dateObj;
		}
		
		// Get day and validate it according to the month
		if (allowFrac) { 			// allow user to enter fractional days or 0
			dTempReal = ASTReal.isValidReal(parts[1],ASTMisc.HIDE_ERRORS);
			if (dTempReal.isValidRealObj()) dateObj.dDay = dTempReal.getRealValue();
			else {
				ASTMsg.errMsg("The Day entered is not a valid value. Please enter\n" +
						"a valid Date in the form mm/dd.dd/yyyy","Invalid Date (Day)",flag);
				return dateObj;
			}
			dateObj.iDay = ASTMath.Trunc(dateObj.dDay);			
		} else {
			iTempInt = ASTInt.isValidInt(parts[1], ASTMisc.HIDE_ERRORS);
			if (iTempInt.isValidIntObj()) dateObj.iDay = iTempInt.getIntValue();
			else {
				ASTMsg.errMsg("The Day entered is not a valid value. Please enter\n" +
						"a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag);
				return dateObj;
			}
			dateObj.dDay = (double) dateObj.iDay;	
		}
		
		if ((dateObj.iDay < iMinDay) || (dateObj.iDay > 31)) {
			ASTMsg.errMsg("The Day entered is out of range. Please enter\n" +
					"a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag);
			return dateObj;
		}
		// Check February
		if (dateObj.iMonth == 2) {
			if (isLeapYear(dateObj.iYear)) iMaxDay = 29;
			if (dateObj.iDay > iMaxDay) {
				ASTMsg.errMsg("The Day entered for February is out of range. Please enter\n" +
						"a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag);
				return dateObj;
			}
		}
		// Check April, June, Sept, Nov
		if ((dateObj.iMonth == 4) || (dateObj.iMonth == 6) || (dateObj.iMonth == 9) || (dateObj.iMonth == 11)) {
			if (dateObj.iDay > 30) {
				ASTMsg.errMsg("The Day entered is out of range. Please enter\n" +
						"a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag);
				return dateObj;
			}
		}

		dateObj.bValid = true;
		
		return dateObj;
	}
	
	/**
	 * Convert a date to a string.
	 * 
	 * This method is overloaded to allow the date to be
	 * passed in as an object or by month, day, year. Also
	 * allows day to be an integer or a decimal value to
	 * handle fractional days.
	 * 
	 * @param dateObj			date to be converted as an object
	 * @return					date as a string
	 */
	public static String dateToStr(ASTDate dateObj) {
		return dateToStr(dateObj.iMonth, dateObj.iDay, dateObj.iYear);
	}
	/**
	 * Convert a date to a string.
	 * 
	 * This method is overloaded to allow the date to be
	 * passed in as an object or by month, day, year. Also
	 * allows day to be an integer or a decimal value to
	 * handle fractional days.
	 * 
	 * @param month				month as an integer
	 * @param day				day as a double
	 * @param year				year as an integer
	 * @return					date as a string
	 */
	public static String dateToStr(int month,double day,int year) {
		return String.format("%02d/%05.2f/%d", month,day,year);
	}
	/**
	 * Convert a date to a string.
	 * 
	 * This method is overloaded to allow the date to be
	 * passed in as an object or by month, day, year. Also
	 * allows day to be an integer or a decimal value to
	 * handle fractional days.
	 * 
	 * @param month				month as an integer
	 * @param day				day as an integer
	 * @param year				year as an integer
	 * @return					date as a string
	 */
	public static String dateToStr(int month,int day, int year) {
		return String.format("%02d/%02d/%d", month,day,year);
	}
	
	/*==================================================================================
	 * These methods perform various date related calculations, such as determining
	 * if a year is a leap year and converting a date to a Julian day number.
	 *=================================================================================*/

	/**
	 * Converts a calendar date to a Julian day number.
	 * 
	 * This method is overloaded to allow the date to be
	 * passed as an object or by month, day, year.
	 * 
	 * @param dateObj		date to be converted as an object
	 * @return				the Julian day number for the given date
	 */
	public static double dateToJD(ASTDate dateObj) {
		return dateToJD(dateObj.iMonth,dateObj.dDay,dateObj.iYear);
	}
	/**
	 * Converts a calendar date to a Julian day number.
	 * 
	 * This method is overloaded to allow the date to be
	 * passed as an object or by month, day, year.
	 * 
	 * @param month			month to be converted
	 * @param day			day, including fractional part of
	 * 						the day, to be converted
	 * @param year			year to be converted
	 * @return				the Julian day number for the given date
	 */
	public static double dateToJD(int month, double day, int year) {
		double dT,dTemp,dA,dB,JD, dY,dM;

		if (month > 2) {
			dY = (double) year;
			dM = (double) month;
		} else {
			dY = (double) (year - 1);
			dM = (double) (month + 12);
		}

		if (year < 0) dT = 0.75;
		else dT = 0.0;

		dTemp = (double) (year) + (double) (month)/100.0 + day / 10000.0;
		if (dTemp >= 1582.1015) {
			dA = ASTMath.Trunc(dY/100.0);
			dB = 2.0 - dA + ASTMath.Trunc(dA/4.0);
		} else {
			dA = 0.0;
			dB = 0.0;
		}

		JD = dB + ASTMath.Trunc(365.25*dY-dT) + ASTMath.Trunc(30.6001*(dM+1.0)) + day + 1720994.5;

		return JD;
	}

	/**
	 * Calculate how many days into a year a specific date is
	 * 
	 * @param iMonth		month for the date in question
	 * @param iDay			day for the date in question
	 * @param iYear			year for the date in question
	 * @return				# of days into the year iYear
	 */
	public static int daysIntoYear(int iMonth, int iDay, int iYear) {
		int T,N;

		if (isLeapYear(iYear)) T = 1;
		else T = 2;

		N = ASTMath.Trunc(275.0 * iMonth / 9.0) - T * ASTMath.Trunc((iMonth + 9) / 12.0) + iDay - 30;
		return N;
	}

	/**
	 * Calculate date when given the number of days into a year
	 * 
	 * @param iYear                     year under consideration
	 * @param N                         number of days into iYear
	 * @return							the calendar date that is iDays into the year. No
	 * 									check is made to be sure N is in range.
	 */
	public static ASTDate daysIntoYear2Date(int iYear, int N) {
		int iMonth, iDay, iA, iB, iC, iE;
		ASTDate calendarDate = new ASTDate();

      if (isLeapYear(iYear)) iA = 1523;
      else iA = 1889;

      iB = ASTMath.Trunc((N + iA - 122.1) / 365.25);
      iC = N + iA - ASTMath.Trunc(365.25 * iB);
      iE = ASTMath.Trunc(iC / 30.6001);

      if (iE < 13.5) iMonth = iE - 1;
      else iMonth = iE - 13;

      iDay = iC - ASTMath.Trunc(30.6001 * iE);

      calendarDate.setYear(iYear);
      calendarDate.setMonth(iMonth);
      calendarDate.setiDay(iDay);

      return calendarDate;
	}
	
	/**
	 * Checks to see if a year is a leap year.
	 * <p>
	 * By definition, a leap year must be in the Gregorian calendar (i.e., greater than 1581).
	 * This method will return false if the year is not a leap year or if the
	 * year is less than 1582.
	 * 
	 * @param year			year to check
	 * @return				true if year is a leap year
	 */
	public static boolean isLeapYear(int year) {
		if (year < 1582) return false;
		return ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));
	}

	/**
	 * Convert a Julian day number to a calendar date.
	 * 
	 * @param JD            Julian day number to convert
	 * @return				date as a date object
	 */
	public static ASTDate JDtoDate(double JD) {
		ASTDate dateObj = new ASTDate();
		double JD1, fJD1;
		long iJD1, iA, iB, iC, iD, iE, iG;
		int iMonth, iYear;
		double dDay;

      JD1 = JD + 0.5;
      iJD1 = ASTMath.Trunc(JD1);
      fJD1 = ASTMath.Frac(JD1);
      if (iJD1 > 2299160) {
          iA = ASTMath.Trunc((iJD1 - 1867216.25) / 36524.25);
          iB = iJD1 + 1 + iA - ASTMath.Trunc((double) (iA) / 4.0);
      } else iB = iJD1;

      iC = iB + 1524;
      iD = ASTMath.Trunc((iC - 122.1) / 365.25);
      iE = ASTMath.Trunc(365.25 * iD);
      iG = ASTMath.Trunc((iC - iE) / 30.6001);
      dDay = iC - iE + fJD1 - ASTMath.Trunc(30.6001 * iG);

      if (iG > 13.5) iMonth = (int) (iG - 13);
      else iMonth = (int) (iG - 1);
      if (iMonth > 2.5) iYear = (int) (iD - 4716);
      else iYear = (int) (iD - 4715);

      dateObj.bValid = true;
      dateObj.iMonth = iMonth;
      dateObj.dDay = dDay;
      dateObj.iDay = ASTMath.Trunc(dDay);
      dateObj.iYear = iYear;

      return dateObj;
	}
	
}
