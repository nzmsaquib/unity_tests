package jll.celcalc.ASTUtils;

/**
 * <b>Provides a class for angle objects and related methods.</b>
 * <p>
 * This class is primarily used for determining whether a string has a valid
 * angle. Methods provided include validation routines in which an angle can
 * be expressed in DMS format (###d ##m ##.##s) or decimal format. When
 * expressed in DMS format, the d, m, and s values are optional as long as one
 * is provided. They must be given in the correct order (i.e., 5m 30d is
 * invalid because degrees must be specified before minutes). The angle object
 * maintains the object's value in both DMS and decimal format so that either
 * format can be readily obtained directly from the object.
 * <p>
 * Note that to handle situations where the angle is negative but the integer
 * degrees is 0 (e.g., -0d 13m 14.45s), the bNeg field is used to indicate whether
 * the DMS format of the value is negative. The decimal value of the angle will be
 * negative or positive as required and kept in synch with the bNeg field. Also
 * note that due to roundoff errors, it is possible that minutes and seconds can
 * exceed the value 60.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTAngle {
	// Class instance variables. Save object value in both decimal and d,m,s formats
	private boolean bValid;					// true if the object is a valid angle
	private double dDecValue;				// decimal value for the angle
	private boolean bNeg;					// true if the angle is negative
	private int iDegrees;
	private int iMinutes;
	private double dSeconds;

	/**
	 * Class constructor for creating an angle object.
	 * <p>
	 * Since much of the usage of this class involves validating that a string has a valid angle,
	 * assume a new object is invalid until it is proven otherwise.
	 * <p>
	 * This method is overloaded to allow passing in an initial value or taking only the defaults.
	 */
	public ASTAngle() {
		bValid = false;
		dDecValue = 0.0;
		bNeg = false;
		iDegrees = 0;
		iMinutes = 0;
		dSeconds = 0.0;
	}
	/**
	 * Class constructor for creating an angle object.
	 * <p>
	 * Since much of the usage of this class involves validating that a string has a valid angle,
	 * assume a new object is invalid until it is proven otherwise.
	 * <p>
	 * This method is overloaded to allow passing in an initial value or taking only the defaults.
	 * 
	 * @param decValue					angle value to give the object
	 */
	public ASTAngle(double decValue) {
		setDecAngle(decValue);
	}

	/*===================================================================
	 * Define 'get' accessors for the object's fields
	 *==================================================================*/

	/**
	 * Returns whether the object is a valid angle.
	 * 
	 * @return			true if a valid angle
	 */
	public boolean isValidAngleObj() {
		return this.bValid;
	}

	/**
	 * Gets the decimal value of the angle
	 * 
	 * @return		decimal value of the angle (in degrees)
	 */
	public double getDecAngle() {
		return this.dDecValue;
	}

	/**
	 * Determines whether the angle is negative.
	 * <p>
	 * This is needed because an angle such as -0d 15m 30s is
	 * valid, but some variable is required to indicate that the
	 * value is negative.
	 * 
	 * @return			true if the angle is negative
	 */
	public boolean isNegAngle() {
		return this.bNeg;
	}

	/**
	 * Gets the 'degrees' portion of the angle
	 * 
	 * @return			the degrees portion of the angle
	 */
	public int getDegrees() {
		return this.iDegrees;
	}

	/**
	 * Gets the 'minutes' portion of the angle
	 * 
	 * @return			the minutes portion of the angle
	 */
	public int getMinutes() {
		return this.iMinutes;
	}

	/**
	 * Gets the 'seconds' portion of the angle
	 * 
	 * @return			the seconds portion of the angle
	 */
	public double getSeconds() {
		return this.dSeconds;
	}

	/********************************************************************
	 * Define 'set' accessors for the object's fields. We actually
	 * will only need one.
	 * 
	 * @param decValue			value to assign to the object
	 ********************************************************************/
	public void setDecAngle(double decValue) {
		double tmp = Math.abs(decValue);

		bValid = true;
		bNeg = (decValue < 0.0);
		dDecValue = decValue;
		iDegrees = ASTMath.Trunc(tmp);
		iMinutes = ASTMath.Trunc(ASTMath.Frac(tmp) * 60.0);
		dSeconds = 60.0 * ASTMath.Frac(60 * ASTMath.Frac(tmp));
	}

	/*==============================================================================
	 * Define some general methods for validating that a string has a valid angle
	 * expressed in either decimal format or DMS format.
	 *=============================================================================*/

	/**
	 * Validate a string that may be in either DMS or decimal
	 * format. Do not display any error messages unless flag
	 * is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to make the flag
	 * input parameter optional. The default is to
	 * show error messages.
	 * 
	 * @param inputStr		string to be validated
	 * @return				an ASTAngle object containing the angle extracted from the string.
	 * 						The returned value for iDegrees will always be non-negative with bNeg set to true
	 * 						if the value is negative. This is because we must allow for the
	 * 						possibility of the integer degrees being -0.
	 */
	public static ASTAngle isValidAngle(String inputStr) {
		return isValidAngle(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Validate a string that may be in either DMS or decimal
	 * format. Do not display any error messages unless flag
	 * is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to make the flag
	 * input parameter optional. The default is to
	 * show error messages.
	 * 
	 * @param inputStr		string to be validated
	 * @param flag			flag indicating whether to display error message
	 * @return				an ASTAngle object containing the angle extracted from the string.
	 * 						The returned value for iDegrees will always be non-negative with bNeg set to true
	 * 						if the value is negative. This is because we must allow for the
	 * 						possibility of the integer degrees being -0.
	 */
	public static ASTAngle isValidAngle(String inputStr,boolean flag) {
		ASTAngle angleObj = new ASTAngle();

		angleObj.bValid = false;

		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter an angle as a valid decimal or DMS (###d ##m ##.##s) value","Invalid Angle Format",flag);
			return angleObj;
		}

		// if 'd', 'm', or 's' is in the string, it is in DMS format
		if ((inputStr.indexOf((int) 'd') >= 0) || (inputStr.indexOf((int) 'm') >= 0) || (inputStr.indexOf((int) 's') >= 0))
			angleObj = isValidDMSAngle(inputStr,flag);
		else angleObj = isValidDecAngle(inputStr,flag);

		return angleObj;	
	}

	/**
	 * Validate that a string contains a valid decimal format value
	 * Don't display any error messages unless flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow the flag input
	 * parameter to be optional. The default is to show errors.
	 * 
	 * @param inputStr		string to be validated
	 * @return				an ASTAngle object containing the angle extracted from the string.
	 * 						The returned value for iDegrees will always be non-negative with bNeg set to true
	 * 						if the value is negative. This is because we must allow for the
	 * 						possibility of the integer degrees being -0.
	 */
	public static ASTAngle isValidDecAngle(String inputStr) {
		return isValidDecAngle(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Validate that a string contains a valid decimal format value
	 * Don't display any error messages unless flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow the flag input
	 * parameter to be optional. The default is to show errors.
	 * 
	 * @param inputStr		string to be validated
	 * @param flag			whether to display error messages
	 * @return				an ASTAngle object containing the angle extracted from the string.
	 * 						The returned value for iDegrees will always be non-negative with bNeg set to true
	 * 						if the value is negative. This is because we must allow for the
	 * 						possibility of the integer degrees being -0.
	 */
	public static ASTAngle isValidDecAngle(String inputStr,boolean flag) {
		ASTAngle angleObj = new ASTAngle();
		ASTReal dTempObj = new ASTReal();

		angleObj.bValid = false;				// assume invalid till we parse and prove otherwise

		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter an angle as a valid decimal value", "Invalid Angle Format", flag);
			return angleObj;
		}

		dTempObj = ASTReal.isValidReal(inputStr,ASTMisc.HIDE_ERRORS);
		if (dTempObj.isValidRealObj()) angleObj = DecToDMS(dTempObj.getRealValue());
		else ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
				"Please enter an angle as a valid decimal value", "Invalid Angle Format", flag);

		return angleObj;
	}

	/**
	 * Validate that a string contains a valid DMS format value.
	 * Don't display any error messages unless flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow the flag input
	 * parameter to be optional. The default is to show errors.
	 * 
	 * @param inputStr		string to be validated
	 * @return				an ASTAngle object containing the angle extracted from the string.
	 * 						The returned value for iDegrees will always be non-negative with bNeg set to true
	 * 						if the value is negative. This is because we must allow for the
	 * 						possibility of the integer degrees being -0.
	 */
	public static ASTAngle isValidDMSAngle(String inputStr) {
		return isValidDMSAngle(inputStr,ASTMisc.SHOW_ERRORS);	
	}
	/**
	 * Validate that a string contains a valid DMS format value.
	 * Don't display any error messages unless flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow the flag input
	 * parameter to be optional. The default is to show errors.
	 * 
	 * @param inputStr		string to be validated
	 * @param flag			whether to display error messages
	 * @return				an ASTAngle object containing the angle extracted from the string.
	 * 						The returned value for iDegrees will always be non-negative with bNeg set to true
	 * 						if the value is negative. This is because we must allow for the
	 * 						possibility of the integer degrees being -0.
	 */
	public static ASTAngle isValidDMSAngle(String inputStr,boolean flag) {
		ASTAngle angleObj = new ASTAngle();
		ASTInt iTempObj = new ASTInt();
		ASTReal dTempObj = new ASTReal();
		int posD,posM,posS;
		String temp;
		boolean bValidData;

		angleObj.bValid = false;		// assume invalid till we parse and prove otherwise

		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag);
			return angleObj;
		}

		inputStr = inputStr.toLowerCase();
		angleObj.bNeg = (inputStr.charAt(0) == '-');		// account for -0d situation

		// Figure out where the d, m, and s values are located
		posD = inputStr.indexOf((int) 'd');
		posM = inputStr.indexOf((int) 'm');
		posS = inputStr.indexOf((int) 's');

		// First, be sure that the delimiters d, m, and s appear no more than once in the input string
		if ((ASTStr.countChars(inputStr,'d') > 1) || (ASTStr.countChars(inputStr,'m') > 1) || (ASTStr.countChars(inputStr,'s') > 1)) {
			ASTMsg.errMsg("Only one value for Degrees, Minutes, and Seconds can be entered.\n" +
					"Please enter an angle as a valid DMS (###d ##m ##.##s) value","Invalid Angle Format",flag);
			return angleObj;
		}

		// Now be sure they're in the correct order (i.e., dms) if they exist
		bValidData = true;
		if (posD >= 0) {				// if true, the user specified degrees
			if (posM >= 0) bValidData = (posM > posD) && bValidData;
			if (posS >= 0) bValidData = (posS > posD) && bValidData;
		}
		if (posM >= 0) {			// if true, the user specified minutes
			if (posS >= 0) bValidData = (posS > posM) && bValidData;
		}	
		if (!bValidData) {
			ASTMsg.errMsg("The DMS values must be entered in the correct order.\n" +
					"Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag);
			return angleObj;
		}

		// At this point everything is in the correct order and appears no more than once.
		// Get the degrees, which is the first in the string if the user entered degrees.
		if (posD >= 0) {
			temp = inputStr.substring(0,posD);
			// Now throw away the degrees. No need to worry if the result is null since that would mean
			// that the user didn't enter minutes or seconds. This will be correctly caught below with posM and/or posS.
			inputStr = inputStr.substring(posD+1,inputStr.length()); 	// throw away the degrees

			iTempObj = ASTInt.isValidInt(temp,ASTMisc.HIDE_ERRORS);
			if (iTempObj.isValidIntObj()) angleObj.iDegrees = Math.abs(iTempObj.getIntValue());		// angleObj.bNeg already contains the sign
			else {
				ASTMsg.errMsg("The value for Degrees that was entered is invalid.\n" +
						"Please enter a valid DMS (###d ##m ##.##s) value","Invalid Angle Format (Degrees)",flag);
				return angleObj;	
			}	
		}

		// Get minutes if they were entered, and they must be after the degrees, which we've already checked
		if (posM >= 0) {
			// Have to update the value of posM since inputStr may have changed in the previous step for getting degrees
			posM = inputStr.indexOf((int) 'm');		
			temp = inputStr.substring(0,posM);
			inputStr = inputStr.substring(posM+1,inputStr.length());

			iTempObj = ASTInt.isValidInt(temp,ASTMisc.HIDE_ERRORS);
			if (iTempObj.isValidIntObj()) angleObj.iMinutes = iTempObj.getIntValue();
			else {
				ASTMsg.errMsg("The value for Minutes that was entered is invalid.\n" +
						"Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format (Minutes)",flag);
				return angleObj;
			}
		}

		if ((angleObj.iMinutes < 0) || (angleObj.iMinutes > 59)) {
			ASTMsg.errMsg("The value entered for Minutes is out of range.\n" +
					"Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format (Minutes)",flag);
			return angleObj;
		}

		// Finally, get seconds if they were entered.
		if (posS >= 0) {
			posS = inputStr.indexOf((int) 's');
			temp = inputStr.substring(0,posS);

			dTempObj = ASTReal.isValidReal(temp,ASTMisc.HIDE_ERRORS);
			if (dTempObj.isValidRealObj()) angleObj.dSeconds = dTempObj.getRealValue();
			else {
				ASTMsg.errMsg("The value for Seconds that was entered is invalid.\n" +
						"Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format (Seconds)", flag);
				return angleObj;
			}
		}

		if (!((angleObj.dSeconds >= 0.0) && (angleObj.dSeconds < 60.0))) {
			ASTMsg.errMsg("The value entered for Seconds is out of range.\n" +
					"Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format (Seconds)", flag);
			return angleObj;
		}
		
	    // Handle the pathological case where the user didn't specify d, m, or s. Default to seconds
	    if ((posD < 0) && (posM < 0) && (posS < 0)) {
	        dTempObj = ASTReal.isValidReal(inputStr,ASTMisc.HIDE_ERRORS);
	        if (dTempObj.isValidRealObj()) angleObj.dSeconds = dTempObj.getRealValue();
	        else {
	        	ASTMsg.errMsg("The value for Seconds that was entered is invalid.\n" +
	                           "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
	                           "Invalid Angle Format (Seconds)", flag);
	            return angleObj;
	        }
	    }
		

		angleObj.dDecValue = DMStoDec(angleObj.bNeg,angleObj.iDegrees,angleObj.iMinutes,angleObj.dSeconds);
		angleObj.bValid = true;
		return angleObj;
	}

	/*==============================================================================
	 * Define some general methods for converting an angle to a string format
	 *=============================================================================*/

	/**
	 * Convert an angle object to DMS or decimal string format
	 * depending upon the formatFlag.
	 * <p>
	 * This method is overloaded to allow flexibility in
	 * how the angle is passed: as an object, as a decimal
	 * value, or by neg, d, m, s.
	 *
	 * @param angleObj			angle object to be converted
	 * @param formatFlag		either DMSFORMAT or DECFORMAT
	 * @return					angle as a string
	 */
	public static String angleToStr(ASTAngle angleObj,boolean formatFlag) {
		if (formatFlag == ASTMisc.DMSFORMAT) return DMStoStr(angleObj);
		else return String.format(ASTStyle.decDegFormat,angleObj.dDecValue);	
	}
	/**
	 * Convert an angle object to DMS or decimal string format
	 * depending upon the formatFlag.
	 * <p>
	 * This method is overloaded to allow flexibility in
	 * how the angle is passed: as an object, as a decimal
	 * value, or by neg, d, m, s.
	 *
	 * @param angle				angle as a decimal value
	 * @param formatFlag		either DMSFORMAT or DECFORMAT
	 * @return					angle as a string
	 */
	public static String angleToStr(double angle,boolean formatFlag) {
		return angleToStr(DecToDMS(angle),formatFlag);
	}
	/**
	 * Convert an angle object to DMS or decimal string format
	 * depending upon the formatFlag.
	 * <p>
	 * This method is overloaded to allow flexibility in
	 * how the angle is passed: as an object, as a decimal
	 * value, or by neg, d, m, s.
	 *
	 * @param neg				true if angle is negative
	 * @param d					degrees
	 * @param m					minutes
	 * @param s					seconds
	 * @param formatFlag		either DMSFORMAT or DECFORMAT
	 * @return					angle as a string
	 */
	public static String angleToStr(boolean neg,int d,int m,double s,boolean formatFlag) {
		if (formatFlag == ASTMisc.DMSFORMAT) return DMStoStr(neg,d,m,s);
		else return String.format(ASTStyle.decDegFormat,DMStoDec(neg,d,m,s));	
	}

	/*======================================================================
	 * Convert an angle to/from DMS and to/from decimal
	 *=====================================================================*/

	/**
	 * Convert a decimal angle to DMS format
	 * 
	 * @param decValue			decimal angle to be converted
	 * @return					an angle object
	 */
	public static ASTAngle DecToDMS(double decValue) {
		ASTAngle angle = new ASTAngle();
		double tmp = Math.abs(decValue);

		angle.bValid = true;
		angle.dDecValue = decValue;
		angle.bNeg = (decValue < 0.0);
		angle.iDegrees = ASTMath.Trunc(tmp);
		angle.iMinutes = ASTMath.Trunc(ASTMath.Frac(tmp) * 60.0);
		angle.dSeconds = 60.0 * ASTMath.Frac(60 * ASTMath.Frac(tmp));

		return angle;
	}

	/**
	 * Convert individual DMS values to a decimal value.
	 * <p>
	 * The neg flag is required because the integer
	 * degrees could be 0 and we may want -0d angles.
	 * 
	 * @param neg			true if the angle is negative
	 * @param d				degrees
	 * @param m				minutes
	 * @param s				seconds
	 * @return				DMS value converted to a decimal value
	 */
	public static double DMStoDec(boolean neg,int d,int m,double s) {
		int sign = 1;

		if (neg) sign = -1;	
		return sign*(d + (m/60.0) + (s/3600.0));
	}

	/**------------------------------------------------------------------------------
	 * Define some private methods used only in this class
	 *------------------------------------------------------------------------------*/

	/**
	 * Convert DMS format to a string.
	 * <p>
	 * This function is overloaded to allow the angle
	 * to be passed as an object or by neg, d, m, s.
	 * <p>
	 * @param angleObj		angle as an object
	 * @param neg			true if the angle is negative
	 * @param d				degrees
	 * @param m				minutes
	 * @param s				seconds
	 * @return 				angle as a DMS string (xxd yym zz.zzs)
	 */
	private static String DMStoStr(ASTAngle angleObj) {
		return DMStoStr(angleObj.bNeg,angleObj.iDegrees,angleObj.iMinutes,angleObj.dSeconds);
	}
	private static String DMStoStr(boolean neg,int d,int m,double s) {
		String result,str;
		int n;
		double x;

		// Due to round off and truncation errors, build up the result starting with s, then m, then d and worry
		// about m, and s values being out of bounds. (Allow d to be > 360 degrees) So, convert s to a string,
		// convert the string back to a number, and adjust both s and m if required. Similarly, handle m and do d last.
		str = String.format("%05.2f",s);
		x = Double.parseDouble(str);
		if (x >= 60.0) {
			str = "00.00";
			m += 1;
		}
		result = str + "s";

		str = String.format("%02d",m);
		n = Integer.parseInt(str);
		if (n >= 60) {
			str = "00";
			n = Math.abs(d) + 1;
		} else n = Math.abs(d);
		result = str + "m " + result;

		result = String.format("%d",n) + "d " + result;
		if (neg) result = "-" + result;

		return result;	
	}

}
