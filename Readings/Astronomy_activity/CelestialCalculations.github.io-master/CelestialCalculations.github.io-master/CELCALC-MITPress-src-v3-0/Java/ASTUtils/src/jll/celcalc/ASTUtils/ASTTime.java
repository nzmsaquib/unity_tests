package jll.celcalc.ASTUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>Provides a class for time objects and time-related methods,
 * including validating strings in the form h:m:s.s where h is
 * required but m and s.s are optional.</b>
 * <p>
 * The code allows for the possibility that h is negative as well
 * as situations where the time is negative but the integer hours is 0
 * (e.g., -0:13:14.45). Time objects maintain the time
 * in both HMS and decimal format so that either format can be
 * readily obtained directly from the object.
 * <p>
 * Different programming languages have time-related routines,
 * but they are not used here because (a) we need to allow
 * time to be negative and (b) implementing the time-related routines
 * needed makes it easier to translate to a different programming
 * language.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTTime {
	// Class instance variables
	private boolean bValid;				// true if the object contains a valid time
	private boolean bNeg;				// true if the time is negative
	private double dDecValue;			// decimal value for the time
	private int iHours;
	private int iMinutes;
	private double dSeconds;

	/**
	 * Creates and initializes a new time object.
	 * <p>
	 * Since much of the usage of this class involves validating
	 * that a string has a valid time, assume a new object is
	 * invalid until it is proven otherwise.
	 * <p>
	 * This method is overloaded to allow an initial value to be specified
	 */
	public ASTTime() {
		bValid = false;
		bNeg = false;
		dDecValue = 0.0;
		iHours = 0;
		iMinutes = 0;
		dSeconds = 0.0;
	}
	/**
	 * Creates and initializes a new time object.
	 * <p>
	 * Since much of the usage of this class involves validating
	 * that a string has a valid time, assume a new object is
	 * invalid until it is proven otherwise.
	 * <p>
	 * This method is overloaded to allow an initial value to be specified
	 * 
	 * @param decValue		initial value for the time object
	 */
	public ASTTime(double decValue) {
		setDecTime(decValue);
	}

	/*=======================================================
	 * Define 'get' accessors for the object's fields
	 *======================================================*/
	
	/**
	 * Returns whether the object is a valid time
	 * 
	 * @return			true if a valid time
	 */
	public boolean isValidTimeObj() {
		return this.bValid;
	}

	/**
	 * Returns whether the time is negative
	 * 
	 * @return			true if time is negative
	 */
	public boolean isNegTime() {
		return this.bNeg;
	}

	/**
	 * Gets the time as a decimal value
	 * 
	 * @return			time as a decimal value
	 */
	public double getDecTime() {
		return this.dDecValue;
	}

	/**
	 * Gets the 'hours' portion of the time
	 * 
	 * @return			'hours' component of the time
	 */
	public int getHours() {
		return this.iHours;
	}

	/**
	 * Gets the 'minutes' portion of the time
	 * 
	 * @return			'minutes' component of the time
	 */
	public int getMinutes() {
		return this.iMinutes;
	}

	/**
	 * Gets the 'seconds' portion of the time
	 * 
	 * @return			'seconds' component of the time
	 */
	public double getSeconds() {
		return this.dSeconds;
	}	

	/*======================================================
	 * Define 'set' accessors for a time object's fields
	 *=====================================================*/
	
	/**
	 * Set the time object's value
	 * 
	 * @param decValue			decimal value to which object is to be set
	 */
	public void setDecTime(double decValue) {
		double tmp;

		tmp = Math.abs(decValue);

		this.bValid = true;
		this.bNeg = (decValue < 0.0);
		this.dDecValue = decValue;
		this.iHours = ASTMath.Trunc(tmp);
		this.iMinutes = ASTMath.Trunc(ASTMath.Frac(tmp) * 60.0);
		this.dSeconds = 60.0 * ASTMath.Frac(60 * ASTMath.Frac(tmp));		
	}
	
	/**
	 * Define a method for returning the current time. This is easy
	 * to do, but is provided as a method to ease portability to other languages.
	 * 
	 * @return		current system time
	 */
	public static String getCurrentTime() {
		DateFormat tf = new SimpleDateFormat("HH:mm:ss");
		Date timeobj = new Date();
	
		return tf.format(timeobj);
	}

	/*===================================================================================
	 * Define some general methods for validating that a string contains a valid time
	 *==================================================================================*/

	/**
	 * Checks a string to see if it contains a valid decimal
	 * format value. Don't display any error messages unless
	 * flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow flag to be an
	 * optional parameter. The default is to display error
	 * messages.
	 * 
	 * @param inputStr			string to validate
	 * @return					an ASTTime object containing the result
	 */
	public static ASTTime isValidDecTime(String inputStr) {
		return isValidDecTime(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Checks a string to see if it contains a valid decimal
	 * format value. Don't display any error messages unless
	 * flag is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow flag to be an
	 * optional parameter. The default is to display error
	 * messages.
	 * 
	 * @param inputStr			string to validate
	 * @param flag				whether to display error messages
	 * @return					an ASTTime object containing the result
	 */
	public static ASTTime isValidDecTime(String inputStr,boolean flag) {
		ASTTime timeObj = new ASTTime();
		ASTReal dTempObj = new ASTReal();

		timeObj.bValid = false;			// assume invalid till we parse and prove otherwise

		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter time as a valid decimal value", "Invalid Time Format", flag);
			return timeObj;
		}

		dTempObj = ASTReal.isValidReal(inputStr,ASTMisc.HIDE_ERRORS);

		if (dTempObj.isValidRealObj()) {
			timeObj = DecToHMS(dTempObj.getRealValue());
			if (timeObj.getHours() > 23) {
				ASTMsg.errMsg("The Hours value entered is not a valid value.\n" +
					"Please enter a valid decimal value", "Invalid Time Format (Hours)", flag);
				timeObj.bValid = false;
			} else timeObj.bValid = true;
		} else ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
				"Please enter time as a valid decimal value", "Invalid Time Format", flag);

		return timeObj;
	}

	/**
	 * Checks a string to see if contains a valid HMS value.
	 * The string format is h:m:s.s where h is required but m
	 * and s.s are optional. Do not display any error message
	 * if flag is HIDE_ERRORS to let the calling routine display
	 * its own messages.
	 * <p>
	 * The custom code here is needed because we want to allow for
	 * the possibility that h could be negative, and to allow both
	 * minutes and seconds to be optional. Also note that to
	 * handle a situation where the time is negative but h is 0
	 * (e.g., -0:13:15.45), the bNeg field is required.
	 * <p>
	 * This method is overloaded to allow flag to be an
	 * optional parameter. The default is to display error
	 * messages.
	 * 
	 * @param inputStr		string to validate
	 * @return				an ASTTime object containing the results. The returned 'hours' value in the ASTTime object
	 * 						will always be non-negative with bNeg set to true
	 * 						if the time is negative. This is because we must allow for the
	 * 						possibility of the hours being -0.
	 */
	public static ASTTime isValidHMSTime(String inputStr) {
		return isValidHMSTime(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Checks a string to see if contains a valid HMS value.
	 * The string format is h:m:s.s where h is required but m
	 * and s.s are optional. Do not display any error message
	 * if flag is HIDE_ERRORS to let the calling routine display
	 * its own messages.
	 * <p>
	 * The custom code here is needed because we want to allow for
	 * the possibility that h could be negative, and to allow both
	 * minutes and seconds to be optional. Also note that to
	 * handle a situation where the time is negative but h is 0
	 * (e.g., -0:13:15.45), the bNeg field is required.
	 * <p>
	 * This method is overloaded to allow flag to be an
	 * optional parameter. The default is to display error
	 * messages.
	 * 
	 * @param inputStr		string to validate
	 * @param flag			whether to display error messages 
	 * @return				an ASTTime object containing the results. The returned 'hours' value in the ASTTime object
	 * 						will always be non-negative with bNeg set to true
	 * 						if the time is negative. This is because we must allow for the
	 * 						possibility of the hours being -0.
	 */
	public static ASTTime isValidHMSTime(String inputStr,boolean flag) {
		ASTTime timeObj = new ASTTime();	
		ASTInt iTempObj = new ASTInt();
		ASTReal dTempObj = new ASTReal();

		timeObj.bValid = false;		// assume invalid till we parse and prove otherwise

		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS format",flag);
			return timeObj;
		}

		timeObj.bNeg = (inputStr.charAt(0) == '-');		// account for negative time 

		String[] parts = inputStr.split(":");
		if (parts.length > 3) {
			ASTMsg.errMsg("The HMS value entered is not in a valid format.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS format",flag);
			return timeObj;
		}

		// Get hours, which must have been entered if we get this far
		iTempObj = ASTInt.isValidInt(parts[0],ASTMisc.HIDE_ERRORS);
		if (iTempObj.isValidIntObj()) timeObj.iHours = Math.abs(iTempObj.getIntValue());			// timeObj.bNeg will contain the sign
		else {
			ASTMsg.errMsg("The Hours value entered is not a valid value.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Hours)",flag);
			return timeObj;
		}
		if (timeObj.iHours > 23) {
			ASTMsg.errMsg("The Hours value entered is not a valid value.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Hours)",flag);
			return timeObj;
		}

		// Get minutes, if they were entered
		if (parts.length == 1) {				// if true, neither m or s were entered, but still need to set dec value
			timeObj.dDecValue = HMStoDec(timeObj.bNeg,timeObj.iHours,0,0.0);
			timeObj.bValid = true;
			return timeObj;
		}

		iTempObj = ASTInt.isValidInt(parts[1], ASTMisc.HIDE_ERRORS);
		if (iTempObj.isValidIntObj()) timeObj.iMinutes = iTempObj.getIntValue();
		else {
			ASTMsg.errMsg("The Minutes value entered is not a valid value.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Minutes)",flag);
			return timeObj;
		}
		if ((timeObj.iMinutes > 59) || (timeObj.iMinutes < 0)) {
			ASTMsg.errMsg("The Minutes value entered is not a valid value.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Minutes)",flag);
			return timeObj;
		}

		// Get seconds, if they were entered
		if (parts.length == 2) {				// if true, s was not entered
			timeObj.dDecValue = HMStoDec(timeObj.bNeg,timeObj.iHours,timeObj.iMinutes,0.0);
			timeObj.bValid = true;
			return timeObj;
		}

		dTempObj = ASTReal.isValidReal(parts[2],ASTMisc.HIDE_ERRORS);
		if (dTempObj.isValidRealObj()) timeObj.dSeconds = dTempObj.getRealValue();
		else {
			ASTMsg.errMsg("The Seconds value entered is not a valid value.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Seconds)",flag);
			return timeObj;
		}
		if ((timeObj.dSeconds >= 60.0) || (timeObj.iMinutes < 0.0)) {
			ASTMsg.errMsg("The Seconds value entered is not a valid value.\n" +
					"Please enter a valid time in the form hh:mm:ss.ss","Invalid HMS (Seconds)",flag);
			return timeObj;
		}

		timeObj.dDecValue = HMStoDec(timeObj.bNeg, timeObj.iHours, timeObj.iMinutes, timeObj.dSeconds);
		timeObj.bValid = true;
		return timeObj;
	}

	/**
	 * Validate a string that may be in either HMS or decimal
	 * format. Do not display any error messages unless flag
	 * is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow flag to be an
	 * optional parameter. The default is to display error
	 * messages.
	 * 
	 * @param inputStr			string to be validated
	 * @return					an ASTTime object containing the results
	 */
	public static ASTTime isValidTime(String inputStr) {
		return isValidTime(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Validate a string that may be in either HMS or decimal
	 * format. Do not display any error messages unless flag
	 * is SHOW_ERRORS.
	 * <p>
	 * This method is overloaded to allow flag to be an
	 * optional parameter. The default is to display error
	 * messages.
	 * 
	 * @param inputStr			string to be validated
	 * @param flag				whether to display error messages
	 * @return					an ASTTime object containing the results
	 */
	public static ASTTime isValidTime(String inputStr,boolean flag) {
		ASTTime resultObj = new ASTTime();

		resultObj.bValid = false;

		inputStr = ASTStr.removeWhitespace(inputStr);
		if (inputStr.length() <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter time as a valid decimal or HMS (hh:mm:ss.ss) value", "Invalid Time Format",flag);
			return resultObj;
		}

		if (inputStr.indexOf((int) ':') >= 0) resultObj = isValidHMSTime(inputStr,flag);
		else resultObj = isValidDecTime(inputStr,flag);

		return resultObj;	
	}

	/*=======================================================
	 * Define some methods to convert time to a string
	 *======================================================*/

	/**
	 * Convert time to an HMS or decimal string format depending upon the formatFlag,
	 * which can be HMSFORMAT or DECFORMAT. This function is overloaded to allow passing
	 * time as an object, as a decimal value, or by its individual neg,h,m,s values.
	 * 
	 * This method is overloaded.
	 * 
	 * @param decTime			decimal value to convert
	 * @param formatFlag		what format to return
	 * @return					decimal time converted to a string
	 */
	public static String timeToStr(double decTime,boolean formatFlag) {
		return timeToStr(DecToHMS(decTime),formatFlag);
	}
	/**
	 * Convert time to an HMS or decimal string format depending upon the formatFlag,
	 * which can be HMSFORMAT or DECFORMAT. This function is overloaded to allow passing
	 * time as an object, as a decimal value, or by its individual neg,h,m,s values.
	 * 
	 * This method is overloaded.
	 * 
	 * @param timeObj			time object to be converted
	 * @param formatFlag		what format to return
	 * @return					decimal time converted to a string
	 */
	public static String timeToStr(ASTTime timeObj,boolean formatFlag) {
		if (formatFlag == ASTMisc.HMSFORMAT) return HMStoStr(timeObj);
		else return String.format(ASTStyle.decHoursFormat,timeObj.dDecValue);
	}
	/**
	 * Convert time to an HMS or decimal string format depending upon the formatFlag,
	 * which can be HMSFORMAT or DECFORMAT. This function is overloaded to allow passing
	 * time as an object, as a decimal value, or by its individual neg,h,m,s values.
	 * 
	 * This method is overloaded.
	 * 
	 * @param neg				true if the time is negative
	 * @param h					integer hours
	 * @param m					integer minutes
	 * @param s					decimal seconds
	 * @param formatFlag		what format to return
	 * @return					decimal time converted to a string
	 */
	public static String timeToStr(boolean neg,int h,int m,double s,boolean formatFlag) {
		if (formatFlag == ASTMisc.HMSFORMAT) return HMStoStr(neg,h,m,s);
		else return String.format(ASTStyle.decHoursFormat,HMStoDec(neg,h,m,s));
	}

	/*================================================================
	 * Methods to convert time to/from HMS and to/from decimal
	 *===============================================================*/

	/**
	 * Converts a decimal value into an ASTTime object
	 * 
	 * @param decValue		decimal value to be converted
	 * @return				an ASTTime object
	 */
	public static ASTTime DecToHMS(double decValue) {
		ASTTime timeObj = new ASTTime();
		double tmp = Math.abs(decValue);

		timeObj.bValid = true;
		timeObj.dDecValue = decValue;
		timeObj.bNeg = (decValue < 0.0);
		timeObj.iHours = ASTMath.Trunc(tmp);
		timeObj.iMinutes = ASTMath.Trunc(ASTMath.Frac(tmp) * 60.0);
		timeObj.dSeconds = 60.0 * ASTMath.Frac(60.0 * ASTMath.Frac(tmp));

		return timeObj;
	}

	/**
	 * Convert HMS values to decimal
	 * 
	 * @param neg			true if the time is negative
	 * @param h				integer hours (always non-negative)
	 * @param m				integer minutes
	 * @param s				decimal seconds
	 * @return				time converted to a decimal value
	 */
	public static double HMStoDec(boolean neg,int h,int m,double s) {
		double result = h + (m/60.0) + (s/3600.0);
		if (neg) result = -result;

		return result;
	}	

	/*==================================================
	 * These methods perform various time conversions
	 *=================================================*/
	
	/**
	 * Adjust the date by some number of days
	 * 
	 * @param dateObj           original date
	 * @param days              number of days by which to adjust the date
	 * @return					adjusted date
	 */
	public static ASTDate adjustDatebyDays(ASTDate dateObj, double days) {
		double JD;

		if (days == 0) return dateObj;

		JD = ASTDate.dateToJD(dateObj) + days;
		return ASTDate.JDtoDate(JD);
	}

	/**
	 * Convert GST to LST
	 * 
	 * @param GST			GST time to convert
	 * @param dLongitude	observer's longitude
	 * @return				LST time for the given GST and longitude
	 */
	public static double GSTtoLST(double GST, double dLongitude) {
		double LST;

		LST = GST + (dLongitude / 15.0);

		if (LST < 0.0) LST = LST + 24.0;
		if (LST > 24.0) LST = LST - 24.0;

		return LST;
	}

	/**
	 * Convert GST to UT
	 * 
	 * @param GST			GST time to convert
	 * @param dateObj		date for which UT is desired
	 * @return				UT time for the given GST and date
	 */
	public static double GSTtoUT(double GST, ASTDate dateObj) {
		double UT;
		double JD, JD0, dT, dR, dB, dA, T0;
		int days;

		// must use iDay rather than dDay because dDay may contain time of day
		// which will throw the results off
		JD = ASTDate.dateToJD(dateObj.getMonth(), (double) (dateObj.getiDay()), dateObj.getYear());
		JD0 = ASTDate.dateToJD(1, 0, dateObj.getYear());

		days = (int) (JD - JD0);
		dT = (JD0 - 2415020.0) / 36525.0;
		dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581;
		dB = 24.0 - dR + 24.0 * (dateObj.getYear() - 1900);
		T0 = 0.0657098 * (days) - dB;
		if (T0 < 0) T0 = T0 + 24;

		dA = GST - T0;
		if (dA < 0) dA = dA + 24;
		UT = dA * 0.99727;
		return UT;
	}

	/**
	 * Convert LCT to UT
	 * 
	 * @param LCT				time to convert
	 * @param DST				true if on daylight saving time
	 * @param timeZone			observer's time zone
	 * @param lon				observer's longitude
	 * @param dateAdjustObj		amount by which to adjust date
	 * @return					UT and also dateAdjustObj
	 * 
	 * Note: Must pass dateAdjustObj as an object because Java won't allow
	 * input parameters to be modified.
	 */
	public static double LCTtoUT(double LCT, boolean DST, ASTLatLon.TimeZoneType timeZone, double lon, ASTInt dateAdjustObj) {
		double UT = LCT;

		dateAdjustObj.setIntValue(0);		// amount to adjust date by if the conversion results in next or prev day

		// Adjust for daylight saving time
		if (DST) UT = LCT - 1;

		UT = UT - ASTLatLon.timeZoneAdjustment(timeZone, lon);
		if (UT > 24) {
			UT = UT - 24.0;
			dateAdjustObj.setIntValue(1);
		} else if (UT < 0) {
			UT = UT + 24.0;
			dateAdjustObj.setIntValue(-1);
		}

		return UT;	
	}

	/**
	 * Convert LST to GST
	 * 
	 * @param LST			LST time to convert
	 * @param dLongitude	observer's longitude
	 * @return				the LST converted to GST.
	 */
	public static double LSTtoGST(double LST, double dLongitude) {
		double GST;

		GST = LST - (dLongitude / 15.0);
		if (GST < 0) GST = GST + 24;
		if (GST > 24) GST = GST - 24;

		return GST;
	}

	/**
	 * Convert UT to GST
	 * 
	 * @param UT			UT time to convert
	 * @param dateObj		date as an object
	 * @return				GST time for the given UT and date
	 */
	public static double UTtoGST(double UT, ASTDate dateObj) {
		double GST, JD, JD0, dDays, dT, dR, dB, T0;

		// must use iDay rather than dDay because dDay may contain time of day
		// which will throw the results off
		JD = ASTDate.dateToJD(dateObj.getMonth(), (double) (dateObj.getiDay()), dateObj.getYear());
		JD0 = ASTDate.dateToJD(1, 0.0, dateObj.getYear()) ;			// JD for Jan 0.0
		dDays = JD - JD0;										// # days since Jan 0.0
		dT = (JD0 - 2415020.0) / 36525.0;
		dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581;
		dB = 24.0 - dR + 24.0 * (dateObj.getYear() - 1900);
		T0 = 0.0657098 * (dDays) - dB;
		GST = T0 + UT * 1.002738;

		if (GST < 0.0) GST = GST + 24.0;
		if (GST > 24.0) GST = GST - 24.0;

		return GST;
	}

	/**
	 * Convert UT to LCT
	 * 
	 * @param UT			time to convert
	 * @param DST			true if on daylight saving time
	 * @param timeZone		observer's time zone
	 * @param lon			observer's longitude
	 * @param dateAdjustObj	amount by which to adjust date
	 * @return				LCT and also dateAdjustObj
	 * 
	 * Note: Must pass dateAdjustObj as an object because Java won't allow
	 * input parameters to be modified.
	 */
	public static double UTtoLCT(double UT, boolean DST, ASTLatLon.TimeZoneType timeZone, double lon, ASTInt dateAdjustObj) {
		double LCT;

		dateAdjustObj.setIntValue(0);				// amount to adjust date by if the conversion results in next or prev day

		LCT = UT + ASTLatLon.timeZoneAdjustment(timeZone, lon);
		if (LCT > 24) {				// next day
			LCT = LCT - 24.0;
			dateAdjustObj.setIntValue(1);
		}
		if (LCT < 0) {				// previous day
			LCT = LCT + 24.0;
			dateAdjustObj.setIntValue(-1);
		}

		if (DST) LCT = LCT + 1;

		return LCT;
	}

	/*-------------------------------------------------------------
	 * Private routines used only in this class
	 *------------------------------------------------------------*/

	/**
	 * Convert time to an HMS-formatted string
	 * <p>
	 * This function is overloaded to allow time to be
	 * passed as an object or as neg, h, m, s.
	 * <p>
	 * @param timeObj		time object to convert
	 * @param neg			true if time is negative
	 * @param h				integer hours
	 * @param m				integer minutes
	 * @param s				decimal seconds
	 * @return				time object converted to a string
	 */
	private static String HMStoStr(ASTTime timeObj) {
		return HMStoStr(timeObj.bNeg,timeObj.iHours,timeObj.iMinutes,timeObj.dSeconds);
	}	
	private static String HMStoStr(boolean neg,int h,int m,double s) {
		String result,str;
		int n;
		double x;

		// Due to round off and truncation errors, build up the result starting with s, then m, then h and worry
		// about h, m, and s values being out of bounds. So, convert s to a string, convert the string back to a
		// number, and adjust both s and m if required. Similarly, handle m and do h last.
		str = String.format("%05.2f",s);
		x = Double.parseDouble(str);
		if (x >= 60.0) {
			str = "00.00";
			m += 1;
		}
		result = str;

		str = String.format("%02d", m);
		n = Integer.parseInt(str);
		if (n >= 60) {
			str = "00";
			n = Math.abs(h) + 1;
		} else n = Math.abs(h);
		result = str + ":" + result;

		result = String.format("%d",n) + ":" + result;
		if (neg) result = "-" + result;
		return result;		
	}

}
