package jll.celcalc.ASTUtils;

/**
 * <b>This class implements a Lat/Lon object and related methods,
//' such as time zones and validating lat/lon strings.</b>
 *<p>
 * In the validation routines, lat/lon can usually be entered in either
 * decimal (e.g., 95.63W) or DMS format (e.g., 9d 15m 33sN).
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTLatLon {
	// Class instance variables. Lat/lon objects hold both the DMS and decimal version of the lat/lon.
	// POS_DIRECTION is for North latitudes and East Longitudes
	
	// Latitude fields
	private boolean bLatValid;				// true if the object contains a valid latitude
	private boolean bLatDir;				// North latitude if bLatDir = POS_DIRECTION, else South Latitude
	private ASTAngle latAngle;				// actual value for the latitude
	// Longitude fields
	private boolean bLonValid;				// true if the object contains a valid longitude
	private boolean bLonDir;				// East longitude if bLonDir = POS_DIRECTION, else West Longitude	
	private ASTAngle lonAngle;				// actual value for the longitude
	
	/**
	 * The numeric value of the time zone type is the time zone adjustment. These are
	 * negative because West longitudes are negative.
	 */
	public enum TimeZoneType {
		/** use longitude rather than a time zone */ LONGITUDE(0,"Longitude"),
		/** use Eastern Standard Time zone */ EST(-5,"EST"),
		/** use Central Standard Time zone */ CST(-6,"CST"),
		/** use Mountain Standard Time zone */ MST(-7,"MST"),
		/** use Pacific Standard Time zone */ PST(-8,"PST");

		private int adjust;			// adjustment to add/subtract due to the time zone or specified longitude
		private String str;			// string representation of the time zone/longitude
		private TimeZoneType(int n,String s) {
			adjust = n;
			str = s;
		}

		/**
		 * Gets the adjustment factor to use for this enumerated type.
		 * 
		 * @return		adjustment factor
		 */
		public int getAdjust() {
			return this.adjust;
		}

		/**
		 * Gets a printable string for this enumerated type.
		 * 
		 * @return		printable string for this enumerated type.
		 */
		public String toString() {
			return this.str;
		}
	}

  // These flags are used just to make the code more readable
  private static final boolean DO_LATITUDE = true;
  private static final boolean DO_LONGITUDE = !DO_LATITUDE;

	/**
	 * Class constructor.
	 * <p>
	 * Since much of the usage of this class involves validating that a string has a valid lat
	 * and/or lon, assume a new object is invalid until it is proven otherwise.
	 */
	public ASTLatLon() {
		// latitude fields
		bLatValid = false;
		bLatDir = ASTMisc.POS_DIRECTION;		// assume North latitude by default
		latAngle = new ASTAngle();
		// longitude fields
		bLonValid = false;
		bLonDir = ASTMisc.POS_DIRECTION;		// assume East longitude by default	
		lonAngle = new ASTAngle();
	}
	
	/*================================================================
	 * Define 'get/set' accessors for the object's fields
	 *===============================================================*/

	// Latitude fields

	/**
	 * Returns whether the object has a valid latitude
	 * 
	 * @return			true if object has a valid latitude
	 */
	public boolean isLatValid() {
		return this.bLatValid;
	}

	/**
	 * Returns whether the object is a North latitude
	 * 
	 * @return			POS_DIRECTION if North latitude, NEG_DIRECTION if South Latitude
	 */
	public boolean isNorthLat() {
		return this.bLatDir;
	}

	/**
	 * Gets the latitude value as an angle object
	 * 
	 * @return			latitude as an ASTAngle object
	 */
	public ASTAngle getLatAngle() {
		return this.latAngle;
	}

	/**
	 * Gets the latitude value
	 * 
	 * @return			latitude as a real number
	 */
	public double getLat() {
		return this.latAngle.getDecAngle();
	}
	
	// Longitude fields
	/**
	 * Returns whether the object has a valid longitude
	 * 
	 * @return			true if object has a valid longitude
	 */
	public boolean isLonValid() {
		return this.bLonValid;
	}

	/**
	 * Returns whether the object is a West longitude
	 * 
	 * @return			POS_DIRECTION if East longitude, NEG_DIRECTION if West longitude
	 */
	public boolean isEastLon() {
		return this.bLonDir;
	}

	/**
	 * Gets the longitude value as an angle object
	 * 
	 * @return			longitude as an ASTAngle object
	 */
	public ASTAngle getLonAngle() {
		return this.lonAngle;
	}

	/**
	 * Gets the longitude value
	 * 
	 * @return			longitude as a real number
	 */
	public double getLon() {
		return this.lonAngle.getDecAngle();
	}
	
	/**
	 * Sets the latitude value. This method assumes
	 * that the latitude is valid and does **not** check it
	 * 
	 * @param lat		latitude as a real number
	 */	
	public void setLat(double lat) {
		this.bLatValid = true;
		if (lat >= 0) this.bLatDir = ASTMisc.POS_DIRECTION;
		else this.bLatDir = ASTMisc.NEG_DIRECTION;
		this.latAngle.setDecAngle(lat);
	}
	
	/**
	 * Sets the longitude value. This method assumes
	 * that the longitude is valid and does **not** check it
	 * 
	 * @param lon			longitude as a real number
	 */
	
	public void setLon(double lon) {
		this.bLonValid = true;
		if (lon >= 0) this.bLonDir = ASTMisc.POS_DIRECTION;
		else this.bLonDir = ASTMisc.NEG_DIRECTION;
		this.lonAngle.setDecAngle(lon);
	}
	
	/*====================================================================================
	 * Define some general methods for validating that a string contains a valid lat/lon
	 * and for converting a lat/lon to a displayable string
	 *===================================================================================*/

	/**
	 * Convert a lat/lon object to a string
	 * 
	 * @param latlonObj				object to convert to a string
	 * @param formatFlag			flag for how to format the
	 * 								result (DMSFORMAT or DECFORMAT)
	 * @return						a printable lat/lon string
	 */
	public static String lat_lonToStr(ASTLatLon latlonObj,boolean formatFlag) {
		return latToStr(latlonObj,formatFlag) + ", " + lonToStr(latlonObj,formatFlag);
	}
	
	/**
	 * Convert a string to a time zone type
	 * 
	 * @param tzStr			string to convert
	 * @return				tzStr converted to a time zone type
	 */
	public static TimeZoneType strToTimeZone(String tzStr) {
		if (tzStr.equalsIgnoreCase("PST")) return TimeZoneType.PST;
		else if (tzStr.equalsIgnoreCase("MST")) return TimeZoneType.MST;
		else if (tzStr.equalsIgnoreCase("CST")) return TimeZoneType.CST;
		else if (tzStr.equalsIgnoreCase("EST")) return TimeZoneType.EST;			
		return TimeZoneType.LONGITUDE;
	}
	
	/**
	 * Calculate a time zone adjustment
	 * 
	 * @param tZone     time zone
	 * @param lon		longitude
	 * @return			a time zone adjustment
	 */
	public static double timeZoneAdjustment(TimeZoneType tZone, double lon) {
		if (tZone == TimeZoneType.LONGITUDE) return ASTMath.Round((lon/15.0),0);
		return tZone.getAdjust();
	}
	
	/*================================== Latitude methods =============================================*/

	/**
	 * Checks a string to see if it contains a valid latitude value. The string format can be
	 * either DMS (###d ##m ##.##s[N or S]) or decimal (###.######[N or S]). If N/S is omitted,
	 * the latitude is assumed to be N. Specifying a sign in addition to, or in lieu of, N or S
	 * is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling routine
	 * display its own message.
	 * 
	 * This routine returns the validated values in the latlonObj (latitude only is set). The 'degrees'
	 * field in the latitude angle will always be non-negative with bLatDir (returned by isNorthLat)
	 * being POS_DIRECTION if the latitude is a North latitude. The real value in the angle object
	 * (use getLat()) will be negative if the value is for a South latitude, and bLatDir will
	 * be set to NEG_DIRECTION.
	 * 
	 * This method is overloaded to allow multiple ways to pass in parameters
	 * 
	 * @param inputStr              string to be checked for a valid latitude
	 * @param flag                  whether to display error messages
	 * @return						a latlonObj object with the results (only latitude filled in)
	 */
	public static ASTLatLon isValidLat(String inputStr,boolean flag) {
		return validateLatOrLon(inputStr,DO_LATITUDE,flag);
	}
	/**
	 * Checks a string to see if it contains a valid latitude value. The string format can be
	 * either DMS (###d ##m ##.##s[N or S]) or decimal (###.######[N or S]). If N/S is omitted,
	 * the latitude is assumed to be N. Specifying a sign in addition to, or in lieu of, N or S
	 * is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling routine
	 * display its own message.
	 * 
	 * This routine returns the validated values in the latlonObj (latitude only is set). The 'degrees'
	 * field in the latitude angle will always be non-negative with bLatDir (returned by isNorthLat)
	 * being POS_DIRECTION if the latitude is a North latitude. The real value in the angle object
	 * (use getLat()) will be negative if the value is for a South latitude, and bLatDir will
	 * be set to NEG_DIRECTION.
	 * 
	 * This method is overloaded to allow multiple ways to pass in parameters
	 * 
	 * @param inputStr              string to be checked for a valid latitude
	 * @return						a latlonObj object with the results (only latitude filled in)
	 */
	public static ASTLatLon isValidLat(String inputStr) {
		return isValidLat(inputStr,ASTMisc.SHOW_ERRORS);
	}
	
	/**
	 * Converts latitude to a printable string as determined by
	 * formatFlag. This method is overloaded to allow the latitude
	 * to be passed as an object or by D, M, and S.
	 * 
	 * This method is overloaded.
	 * 
	 * @param latlonObj             object to be converted to a string
	 * @param formatFlag            what format is desired (DMSFORMAT or DECFORMAT)
	 * @return						a printable string for the latitude
	 */
	public static String latToStr(ASTLatLon latlonObj,boolean formatFlag) {
		if (formatFlag == ASTMisc.DMSFORMAT) return latOrLonToDMSStr(DO_LATITUDE,latlonObj.bLatDir,
				latlonObj.latAngle.getDegrees(),latlonObj.latAngle.getMinutes(), latlonObj.latAngle.getSeconds());
		else return latOrLonToDecStr(DO_LATITUDE, latlonObj.getLat());
	}
	/**
	 * Converts latitude to a printable string as determined by
	 * formatFlag. This method is overloaded to allow the latitude
	 * to be passed as an object or by D, M, and S.
	 * 
	 * This method is overloaded.
	 * 
	 * @param pos                   true if the latitude is positive
	 * @param d                     latitude degrees
	 * @param m                     latitude minutes
	 * @param s                     latitude seconds
	 * @param formatFlag            what format is desired (DMSFORMAT or DECFORMAT)
	 * @return						a printable string for the latitude
	 */
	public static String latToStr(boolean pos,int d, int m, double s, boolean formatFlag) {
		if (formatFlag == ASTMisc.DMSFORMAT) return latOrLonToDMSStr(DO_LATITUDE, pos, d, m, s);
		// Note: have to do not pos because DMStoDEC uses true to indicate negative value		
		else return latOrLonToDecStr(DO_LATITUDE, ASTAngle.DMStoDec(!pos, d, m, s));
	}
	
	/*=============================== Longitude methods ================================================*/

	/**
	 * Checks a string to see if it contains a valid longitude value. The string format can be
	 * either DMS (###d ##m ##.##s[W or E]) or decimal (###.######[W or E]). If W/E is omitted,
	 * the longitude is assumed to be W. Specifying a sign in addition to, or in lieu of, W or D
	 * is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling
	 * routine display its own message.
	 * 
	 * This routine returns the validated values in the latlonObj (longitude only is set). The 'degrees'
	 * field in the longitude angle will always be non-negative with bLonDir (returned by isEastLon)
	 * being POS_DIRECTION if the longitude is an East longitude. The real value in the angle object
	 * (use getLon()) will be negative if the value is for a West longitude, and bLonDir will
	 * be set to NEG_DIRECTION.
	 * 
	 * This method is overloaded to allow multiple ways to pass in parameters
	 * 
	 * @param inputStr              string to be checked for a valid longitude
	 * @param flag                  whether to display error messages
	 * @return						a latlonObj object with the results (only longitude filled in)
	 */
	public static ASTLatLon isValidLon(String inputStr,boolean flag) {
		return validateLatOrLon(inputStr,DO_LONGITUDE,flag);
	}
	/**
	 * Checks a string to see if it contains a valid longitude value. The string format can be
	 * either DMS (###d ##m ##.##s[W or E]) or decimal (###.######[W or E]). If W/E is omitted,
	 * the longitude is assumed to be W. Specifying a sign in addition to, or in lieu of, W or D
	 * is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling
	 * routine display its own message.
	 * 
	 * This routine returns the validated values in the latlonObj (longitude only is set). The 'degrees'
	 * field in the longitude angle will always be non-negative with bLonDir (returned by isEastLon)
	 * being POS_DIRECTION if the longitude is an East longitude. The real value in the angle object
	 * (use getLon()) will be negative if the value is for a West longitude, and bLonDir will
	 * be set to NEG_DIRECTION.
	 * 
	 * This method is overloaded to allow multiple ways to pass in parameters
	 * 
	 * @param inputStr              string to be checked for a valid longitude
	 * @return						a latlonObj object with the results (only longitude filled in)
	 */	
	public static ASTLatLon isValidLon(String inputStr) {
		return isValidLon(inputStr,ASTMisc.SHOW_ERRORS);
	}

	/**
	 * Converts longitude to a printable string as determined by
	 * formatFlag. This method is overloaded to allow the longitude
	 * to be passed as an object or by D, M, and S.
	 * 
	 * This method is overloaded.
	 * 
	 * @param latlonObj             object to be converted to a string
	 * @param formatFlag            what format is desired (DMSFORMAT or DECFORMAT)
	 * @return						a printable string for the longitude
	 */
	public static String lonToStr(ASTLatLon latlonObj,boolean formatFlag) {
		if (formatFlag == ASTMisc.DMSFORMAT) return latOrLonToDMSStr(DO_LONGITUDE,latlonObj.bLonDir,
				latlonObj.lonAngle.getDegrees(),latlonObj.lonAngle.getMinutes(), latlonObj.lonAngle.getSeconds());
		else return latOrLonToDecStr(DO_LONGITUDE, latlonObj.getLon());
	}
	/**
	 * Converts longitude to a printable string as determined by
	 * formatFlag. This method is overloaded to allow the longitude
	 * to be passed as an object or by D, M, and S.
	 * 
	 * This method is overloaded.
	 * 
	 * @param pos                   true if the longitude is positive
	 * @param d                     longitude degrees
	 * @param m                     longitude minutes
	 * @param s                     longitude seconds
	 * @param formatFlag            what format is desired (DMSFORMAT or DECFORMAT)
	 * @return						a printable string for the longitude
	 */
	public static String lonToStr(boolean pos,int d, int m, double s, boolean formatFlag) {
		if (formatFlag == ASTMisc.DMSFORMAT) return latOrLonToDMSStr(DO_LONGITUDE, pos, d, m, s);
		// Note: have to do not pos because DMStoDEC uses true to indicate negative value
		else return latOrLonToDecStr(DO_LONGITUDE, ASTAngle.DMStoDec(!pos, d, m, s));
	}
	
	/*---------------------------------------------------------------------
	 * Methods only used internally to this class
	 *--------------------------------------------------------------------*/
	
	/**
	 * Checks a string to see if it contains a valid latitude or longitude value. The
	 * string format can be either DMS (###d ##m ##.##s[N/S or W/E]) or decimal
	 * (###.######[N/S or W/E]). If the direction is omitted, a positive direction
	 * (i.e., N or E) is assumed. Specifying a sign in addition to, or in lieu of,
	 * N/S or W/E is invalid. Do not display any error message if flag is HIDE_ERRORS to
	 * let the calling routine display its own message. This routine is only used internally
	 * by isValidLat and isValidLon.
	 * 
	 * @param inputStr		string to validate
	 * @param latlonFlag	either DO_LATITUDE or DO_LONGITUDE
	 * @param flag			either HIDE_ERRORS or SHOW_ERRORS
	 * @return				ASTLatLon object containing the results, but the results will be
	 * 						<b>only</b> the latitude or longitude. The returned object will 
	 * 						contain both decimal and DMS formats in the ASTLatLon angle object.
	 */
	private static ASTLatLon validateLatOrLon(String inputStr,boolean latlonFlag,boolean flag) {
		int iLen;
		String strDir, strLatLon;
		char cPos, cNeg;
		boolean bDirection = ASTMisc.POS_DIRECTION;
		ASTAngle angleObj = new ASTAngle();
		ASTLatLon returnObj = new ASTLatLon();

		if (latlonFlag == DO_LATITUDE) {
			strLatLon = "Latitude";
			cPos = 'N';
			cNeg = 'S';
			returnObj.bLatValid = false;
		} else {
			strLatLon = "Longitude";
			cPos = 'E';
			cNeg = 'W';
			returnObj.bLonValid = false;
		}

		strDir = "["+cPos+","+cNeg+"]";

		inputStr = ASTStr.removeWhitespace(inputStr);
		iLen = inputStr.length();
		if (iLen <= 0) {
			ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
					"Please enter a valid "+strLatLon+" in the form ###d ##m ##.##s"+strDir+"\n" +
					"or the form ###.######"+strDir+".","Invalid "+strLatLon+" format",flag);
			return returnObj;
		}

		// See if the user specified a sign.
		if (inputStr.indexOf((int) '-') >= 0) {
			ASTMsg.errMsg("Do not enter a sign [-,+]. Use either '"+cPos+"' or '"+cNeg+"' for "+strLatLon+".",
					"Invalid "+strLatLon+" format",flag);
			return returnObj;
		}
		if (inputStr.indexOf((int) '+') >= 0) {
			ASTMsg.errMsg("Do not enter a sign [-,+]. Use either '"+cPos+"' or '"+cNeg+"' for "+strLatLon+".",
					"Invalid "+strLatLon+" format",flag);
			return returnObj;
		}

		// Figure out whether this is positive or negative direction. Note that we could have ##.##sS to contend with
		// since the user could express DMS format with S to indicate direction. Thus, we can't
		// just convert to upper case and check for 'S'.
		if (inputStr.charAt(iLen-1) == cNeg) {
			bDirection = ASTMisc.NEG_DIRECTION;
			inputStr = inputStr.substring(0, iLen-1);		// Get rid of the negative direction
			iLen = iLen - 1;
		} else if (inputStr.charAt(iLen-1) == cPos) {
			bDirection = ASTMisc.POS_DIRECTION;
			inputStr = inputStr.substring(0, iLen-1);		// Get rid of the positive direction
			iLen = iLen - 1;
		}
		
		// Let the Angle class do the work to validate that inputStr has a valid angle
		angleObj = ASTAngle.isValidAngle(inputStr,ASTMisc.HIDE_ERRORS);
		if (!angleObj.isValidAngleObj()) {
			ASTMsg.errMsg("The " + strLatLon + " entered is invalid. Please enter a\n" +
					"valid " + strLatLon+" in the form ###d ##m ##.##s"+strDir+"\n" +
					"or the form ###.######"+strDir+".","Invalid "+strLatLon+" format",flag);
			return returnObj;
		}

		if (latlonFlag == DO_LATITUDE) {
			if (Math.abs(angleObj.getDecAngle()) > 90.0) {
				ASTMsg.errMsg("The latitude value is out of range.", "Invalid Latitude",flag);
				return returnObj;	
			}
			returnObj.bLatDir= bDirection;
			returnObj.bLatValid = true;
			
			// Because the 'N/S' was stripped off, the angle is always positive. So, we must
			// check to see if it has to be converted to a negative angle.
			if (!returnObj.bLatDir) angleObj = ASTAngle.DecToDMS(-angleObj.getDecAngle());
			
			returnObj.latAngle = angleObj;
		} else {
			if (Math.abs(angleObj.getDecAngle()) > 180.0) {
				ASTMsg.errMsg("The longitude value is out of range.", "Invalid Longitude",flag);
				return returnObj;	
			}
			returnObj.bLonDir = bDirection;
			returnObj.bLonValid = true;
			
			// Because the 'W/E' was stripped off, the angle is always positive. So, we must
			// check to see if it has to be converted to a negative angle.
			if (!returnObj.bLonDir) angleObj = ASTAngle.DecToDMS(-angleObj.getDecAngle());
			
			returnObj.lonAngle = angleObj;
		}

		return returnObj;
	}

	/**
	 * Converts lat or lon decimal value to a displayable string.
	 * 
	 * @param bLat		DO_LATITUDE or DO_LONGITUDE
	 * @param LatOrLon	decimal latitude or longitude
	 * @return			latitude or longitude as a printable string
	 */
	private static String latOrLonToDecStr(boolean bLat,double LatOrLon) {
		String result;

		result = String.format(ASTStyle.latlonFormat,Math.abs(LatOrLon));
		if (bLat == DO_LATITUDE) {
			if (LatOrLon >= 0.0) result = result + "N";
			else result = result + "S";
		} else {
			if (LatOrLon >= 0.0) result = result + "E";
			else result = result + "W";
		}

		return result;
	}

	/**
	 * Converts lat or lon DMS values to a displayable DMS string.
	 * 
	 * @param bLat		DO_LATITUDE or DO_LONGITUDE
	 * @param pos		true if a positive latitude/longitude
	 * @param d			degrees
	 * @param m			minutes
	 * @param s			seconds
	 * @return			latitude or longitude as a printable string
	 */
	private static String latOrLonToDMSStr(boolean bLat,boolean pos,int d,int m,double s) {
		String result;

		result = String.format("%d",Math.abs(d)) + "d " + String.format("%2dm %5.2fs", m,s);
		if (bLat == DO_LATITUDE) {
			if (pos == ASTMisc.POS_DIRECTION) result = result + "N";
			else result = result + "S";
		} else {
			if (pos == ASTMisc.POS_DIRECTION) result = result + "E";
			else result=result + "W";
		}
		return result;
	}
}
