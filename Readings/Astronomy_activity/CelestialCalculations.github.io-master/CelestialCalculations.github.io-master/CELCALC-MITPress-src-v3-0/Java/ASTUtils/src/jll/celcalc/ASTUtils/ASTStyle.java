package jll.celcalc.ASTUtils;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.util.Vector;

import javax.swing.UIManager;

/**
 * <b>GUI Style Settings</b>
 * <p>
 * Defines the fonts and other miscellaneous items that are used for the 
 * programs throughout the book. Style settings are collected here to ensure
 * uniformity across all the programs. With JavaFX (rather than Swing), most
 * of these items would normally go into a style sheet.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */
public class ASTStyle {
	/*===================================================================
	 *				Style for the Book Panel
	 *==================================================================*/
	
	/** Font for the Book Title in the Book Title panel of a GUI */
	public static final Font BOOK_TITLE_BOLDFONT = new Font("Tahoma",Font.BOLD,16);
	/** Color to use for the Book Title in the Book Title panel of a GUI */
	public static final Color BOOK_TITLE_COLOR = Color.WHITE;
	/** Color to use for book title background */
	public static final Color BOOK_TITLE_BKG = new Color(25, 25, 112);
	
	/*=============================================================
	 * Items for the GUI areas with which a user will interact
	 *============================================================*/
	
	/** Font to use for menus */
	public static final Font MENU_FONT = new Font("Segoe UI Semibold",Font.PLAIN,16);
	/** Font for the buttons a user will click in the GUI */
	public static final Font BTN_FONT = new Font("Tahoma",Font.PLAIN,14);
	/** Plain font for labels and other text to show the user in the GUI */
	public static final Font TEXT_FONT = new Font("Tahoma",Font.PLAIN,14);
	/** Bold font for labels and other test to show the user in the GUI */
	public static final Font TEXT_BOLDFONT = new Font("Tahoma",Font.BOLD,14);
	/** Font for text in the GUI checkboxes */
	public static final Font CBOX_FONT = new Font("Tahoma",Font.PLAIN,16);
	/** Small font for text in the GUI checkboxes */
	public static final Font CBOX_FONT_SMALL = new Font("Tahoma",Font.PLAIN,14);
	/** Font for text in the GUI radio buttons */
	public static final Font RADBTN_FONT = new Font("Tahoma",Font.PLAIN,16);
	/** Small font for text in the GUI radio buttons */
	public static final Font RADBTN_FONT_SMALL = new Font("Tahoma",Font.PLAIN,14);
	/** Font for the text written to a scrollable text area when a proportional font is needed */
	public static final Font OUT_TEXT_FONT = new Font("Tahoma",Font.PLAIN,16);
	/** Font for the scrollable text area when a fixed width font is needed */
	public static final Font FW_OUT_TEXT_FONT = new Font("Monospaced",Font.PLAIN,16);
	
	/*===================================================
	 * Fonts used in the error messages
	 *==================================================*/
	
	/** Font for the error message text. */
	public static final Font ERRMSG_BOLDFONT = new Font("Tahoma",Font.BOLD,14);
	/** Font for the 'OK' button in the error message window. */
	public static final Font ERRMSG_BUTTON_FONT = new Font("Tahoma",Font.PLAIN,14);
	
	/*==================================================
	 * Fonts used in the About Box
	 *=================================================*/
	
	/** Font to use for the About box buttons */
	public static final Font ABOUTBTN_FONT = new Font("Tahoma",Font.PLAIN,14);
	/** Font to use for the book Edition in the About box. */
	public static final Font ABOUTEDITION_BOLDFONT = new Font("Tahoma", Font.BOLD, 14);
	/** Font to use for the Copyright section of the About box */
	public static final Font ABOUTCOPYRIGHT_FONT = new Font("Tahoma",Font.PLAIN,14);
	/** Font to use for the Author section of the About box */
	public static final Font ABOUTAUTHOR_FONT = new Font("Tahoma",Font.PLAIN,14);
	/** Font to use for the Chapter Title section of the About box */
	public static final Font ABOUTCHAPTITLE_BOLDFONT = new Font("Tahoma",Font.BOLD,14);
	/** Font to use for the Version number in the About box */
	public static final Font ABOUTVERSION_FONT = new Font("Tahoma",Font.PLAIN,14);
	/** Font to use for the Book Title in the About box */
	public static final Font ABOUTBOOKTITLE_BOLDFONT = new Font("Tahoma", Font.BOLD, 16);

	/*====================================================================
	 * Define format strings for various items to provide consistency
	 * across programs
	 *===================================================================*/
	
	/** Format for visual magnitude */
	public static final String mVFormat = "%.2f";
	/** decimal degrees */
	public static final String decDegFormat = "%.6f";
	/** decimal hours */
	public static final String decHoursFormat = "%.6f";
	/** Temperature (C and F) */
	public static final String temperatureFormat = "%.2f";
	/** Julian day number format */
	public static final String JDFormat = "%9.5f";
	/** latitude/longitude format */
	public static final String latlonFormat = "%.6f";	
	/** Format for displaying the Epoch */
	public static final String epochFormat = "%.1f";	
	/** General format for real numbers */
	public static final String genFloatFormat = "%.6f";
	/** Extra digits for general format for real numbers */
	public static final String genFloatFormat8 = "%.8f";
	
	/*======================================================================
	 * Define some general methods useful for defining the user interface
	 *=====================================================================*/
	
	/**
	 * Sets the look and feel to be whatever is the standard for the underlying OS.
	 */
	public static void setASTStyle() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			// do nothing - system will then default to the Java "metal" look and feel
		}

		// Establish various fonts
		UIManager.put("OptionPane.messageFont",ERRMSG_BOLDFONT);  // set font for error messages
		UIManager.put("Button.font",ERRMSG_BUTTON_FONT);

		// Set as many of the JFileChooser dialog fonts as we can - still need to invoke
		// setContainerFonts to get all of them.
		UIManager.put("FileChooser.listFont",TEXT_FONT);
	}

	/**
	 * Sets the fonts used for an arbitrary container.
	 * <p>
	 * This method is specifically intended for the JFileChooser dialog because the UIManager
	 * doesn't set all of its fonts. However, note that this is a generic method that will
	 * set the fonts for any container, not just a JFileChooser dialog. An example
	 * invocation is
	 * <p>
	 * <code>
	 * setContainerFonts(fc.getComponents(),ASTStyle.TEXT_FONT); 
	 * </code>
	 * <p>
	 * where fc is a JFileChooser instance.
	 * 
	 * @param comp		components in the container to be set
	 * @param font		font to use
	 */
	public static void setContainerFonts(Component[] comp,Font font) {
		for(int x = 0; x < comp.length; x++) {
			if(comp[x] instanceof Container) setContainerFonts(((Container)comp[x]).getComponents(),font);
			try{
				comp[x].setFont(font);
			} catch(Exception e) {
				// do nothing - accept whatever the defaults are
			}
		}
	}
	
	/**
	 * Create a focus traversal policy for the GUI components that should be
	 * traversed when the tab key is pressed. Swing provides a default
	 * traversal policy, but it doesn't generally do the components in the
	 * order we want. So, this code allows us to explicitly specify what order we
	 * want the components traversed.
	 */

	public static class GUIFocusTraversalPolicy extends FocusTraversalPolicy {
		/** order in which to traverse the GUI components */
		Vector<Component> order;

		/**
		 * Set the GUI focus traversal order.
		 * 
		 * @param order		order in which to traverse the GUI components
		 */
		public GUIFocusTraversalPolicy(Vector<Component> order) {
			this.order = new Vector<Component>(order.size());
			this.order.addAll(order);
		}
		
		/**
		 * Gets the next component in the traversal tree after the one passed in.
		 * 
		 * @param focusCycleRoot		root container
		 * @param aComponent			current component
		 * @return						next component after aComponent
		 */
		public Component getComponentAfter(Container focusCycleRoot, Component aComponent) {
			int idx = (order.indexOf(aComponent) + 1) % order.size();
			return order.get(idx);
		}
		
		/**
		 * Gets the prior component in the traversal tree before the one passed in.
		 * 
		 * @param focusCycleRoot		root container
		 * @param aComponent			current component
		 * @return						component before aComponent
		 */
		public Component getComponentBefore(Container focusCycleRoot, Component aComponent)	{
			int idx = order.indexOf(aComponent) - 1;
			if (idx < 0) {
				idx = order.size() - 1;
			}
			return order.get(idx);
		}
		
		/**
		 * Gets the default GUI component, which is the first one in the list.
		 * 
		 * @param focusCycleRoot		root container
		 * @return						default GUI component
		 */
		public Component getDefaultComponent(Container focusCycleRoot) {
			return order.get(0);
		}

		/**
		 * Get the last GUI component in the list.
		 * 
		 * @param focusCycleRoot		root container
		 * @return						last GUI component
		 */
		public Component getLastComponent(Container focusCycleRoot) {
			return order.lastElement();
		}
		
		/**
		 * Get the first GUI component in the list.
		 * 
		 * @param focusCycleRoot		root container
		 * @return						1st GUI component
		 */		public Component getFirstComponent(Container focusCycleRoot) {
			return order.get(0);
		}
	}

}
