package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.filechooser.FileNameExtensionFilter;

import jll.celcalc.ASTUtils.ASTFileIO;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTStr;

/**
 * <b>Define launch sites </b>
 * <p>
 * This class provides the ability to load launch sites
 * from a data file and to store them in an accessible
 * structure. The sample data file Launch-Sites.dat
 * is under the data files directory.
 * 
 * A launch site data file **MUST** be in the format
 * specifically designed for this book. The format can be
 * gleaned by opening the sample Launch-Sites.dat data file and examining
 * its contents. The format is straightforward and is
 * documented in comments within the data file itself.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTSites {

	// Note: Launch site data bases are likely fairly small, so they aren't sorted by this class.
	// This means that any searches must be brute force through all the sites.

	// file types typically associated with Launch data files in this program
	private static final FileNameExtensionFilter fileExtFilter = new FileNameExtensionFilter("Launch Sites","dat","txt");

	private static boolean SitesLoaded = false;
	private static String SitesFilename = "";							// The currently loaded Sites data file

	/*-----------------------------------------------------------------
	 * Define a private class and database for the Launch sites
	 *----------------------------------------------------------------*/

	private static class LaunchSite {
		private String sName;				// Launch site name
		private double dLat;				// Launch site latitude
	}

	// This array will hold Launch Site data
	private static List<LaunchSite> SitesDB = new ArrayList<LaunchSite>();  

	/*========================================================
	 * Define 'getter' methods to return information about
	 * the Launch Sites database.
	 *=======================================================*/

	/**
	 * Gets the filename for the currently loaded Launch Sites database.
	 * 
	 * @return			filename for the Launch Sites db
	 */
	public static String getSitesFilename() {
		return SitesFilename;
	}

	/**
	 * Gets a filter for filtering Launch Sites data files by extension.
	 * This isn't really an instance variable, but this is a
	 * good place to put the method.
	 * 
	 * @return			file extensions filter
	 */
	public static FileNameExtensionFilter getFileExtFilter() {
		return fileExtFilter;
	}

	/**
	 * Returns launch site latitude
	 * 
	 * @param idx			index into Launch Sites database
	 * @return				latitude object. The latitude
	 * 						(for valid idx values)
	 * 						is valid because it is checked during
	 * 						loading Launch Sites data from disk.
	 */
	public static ASTLatLon getSiteLat(int idx) {
		// Intentionally parse bad data in case there is a problem
		ASTLatLon result = ASTLatLon.isValidLat("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumSites())) return result;
		else {
			result = new ASTLatLon();
			result.setLat(SitesDB.get(idx).dLat);
		}
		
		return result;
	}
	
	/**
	 * Returns launch site name
	 * 
	 * @param idx			index into Launch Sites database
	 * @return				site name. If idx is out of range
	 * 						or any other error occurs,
	 * 						null is returned.
	 */
	public static String getSiteName(int idx) {
		if ((idx < 0) || (idx >= getNumSites())) return null;
		else return SitesDB.get(idx).sName;
	}


	/**
	 * Gets the number of Launch Sites in the currently loaded Sites database.
	 * 
	 * @return			number of sites in the Launch Sites db
	 */
	public static int getNumSites() {
		return SitesDB.size();
	}

	/*============================================================
	 * Public methods for manipulating the Launch Sites database
	 *===========================================================*/

	/**
	 * Check to see if a Launch Sites database has been loaded.
	 * 
	 * @return			true if Launch Sites db has been loaded
	 */
	public static boolean areSitesLoaded() {
		return SitesLoaded;
	}

	/**
	 * Clear all the currently loaded Launch Sites data
	 */
	public static void clearLaunchSites() {
		// Delete all of the Launch Sites data. In some languages, we'd need
		//  to loop through the database and and delete objects individually.
		SitesDB = new ArrayList<LaunchSite>();
		SitesFilename = "";
		SitesLoaded = false;
	}

	/**
	 * Puts up a browser window and gets a Launch Sites data filename.
	 * 
	 * @return					full name (file and path) of the Sites data file
	 * 							to open or null if no file is selected or
	 * 							file doesn't exist. This routine does **not**
	 * 							check to see if the file is a valid Sites data file, 
	 * 							but it **does** check to see that the file exists.
	 */
	public static String getSitesFileToOpen() {
		String fullFilename = null;
		String[] fileToRead = new String[2];

		fileToRead = ASTFileIO.getFileToRead("Select Launch Sites Data File to Load ...",
				"Load Launch Sites Data",ASTSites.getFileExtFilter());
		fullFilename = fileToRead[ASTFileIO.FULLPATHNAME_IDX];

		// See if the file exists
		if (!ASTFileIO.doesFileExist(fullFilename)) {
			ASTMsg.errMsg("The Laumch Sites data file specified does not exist", "Launch Sites Data File Does Not Exist");
			return null;
		}

		return fullFilename;
	}

	/**
	 * Search the currently loaded sites database and return an index into the
	 * database for the requested site. The search performed is **not**
	 * case sensitive, and a partial match is counted as successful.
	 * 
	 * @param name			name of the site to find
	 * @return				If successful, returns an index into the
	 * 						currently loaded sites database for the
	 * 						object requested. If unsuccessful, -1 is 
	 * 						returned. Note that this assumes 0-based indexing!
	 */
	public static int findSiteName(String name) {
		String targ, strTmp;
		
		if (!areSitesLoaded()) return -1;

		targ = ASTStr.removeWhitespace(name).toLowerCase(Locale.US);
		for (int i = 0; i < getNumSites(); i++) {
			strTmp = ASTStr.removeWhitespace(SitesDB.get(i).sName).toLowerCase(Locale.US);
			if (strTmp.indexOf(targ) != -1) return i;
		}

		return -1;
	}

	/**
	 * Loads Launch Sites database from disk.
	 * 
	 * @param filename			Full filename (including path) to load
	 * @return					true if successful, else false.
	 */
	public static boolean loadSitesDB(String filename) {
		int count = 0;
		LaunchSite obj;
		double Lat = 0.0;
		ASTLatLon latObj;
		String name;

		clearLaunchSites();

		// Note that we're using try-with-resources so that we don't have to explicitly close the stream when we're done
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String strIn, str2;

			strIn=ASTFileIO.readTillStr(br,"<LaunchSites>",ASTMisc.HIDE_ERRORS);
			if (strIn == null) {
				ASTMsg.errMsg("The specified file does not appear to be a\n" +
						"valid Launch Sites data file - try again.", "Invalid Launch Sites Data File");
				br.close();
				return false;
			}
			// Look for the beginning of the data section, then cycle through
			// and extract each Launch Site data line.
			strIn = ASTFileIO.readTillStr(br,"<Data>");
			if (strIn == null) {
				ASTMsg.errMsg("The specified file does not appear to be a\n" +
						"valid Launch Sites data file - try again.", "Invalid Launch Sites Data File");
				br.close();
				return false;
			}

			while ((strIn = br.readLine()) != null) {
				if (strIn.length() <= 0) continue;					// ignore blank lines
				count = count + 1;

				strIn = strIn.trim();
				str2 = ASTStr.removeWhitespace(strIn.toLowerCase(Locale.US));
				if (str2.indexOf("</data>") >= 0) break;				// found end of data section

				// See if this is a <Site> tag
				if (str2.compareToIgnoreCase("<Site>") != 0) {
					ASTMsg.errMsg("Expected <Site> tag but found '" + strIn + "' instead", "Invalid Tag");
					br.close();
					return false;
				}				  

				strIn = ASTFileIO.getSimpleTaggedValue(br, "<Name>");
				if ((strIn == null) || (strIn.length() <= 0)) {
					ASTMsg.errMsg("Expected <Name> tag but found '" + strIn + "' instead", "Missing <Name> tag");
					br.close();
					return false;
				}
				name = strIn.trim();

				strIn = ASTFileIO.getSimpleTaggedValue(br, "<Lat>");
				if ((strIn == null) || (strIn.length() <= 0)) {
					ASTMsg.errMsg("Expected <Lat> tag but found '" + strIn + "' instead", "Missing <Lat> tag");
					br.close();
					return false;
				}
				// Convert data to a string so that we can validate it is
				// a valid latitude.
				latObj = ASTLatLon.isValidLat(strIn,ASTMisc.HIDE_ERRORS);
				if (latObj.isLatValid()) Lat = latObj.getLat();
				else {
					ASTMsg.errMsg("Invalid latitude for site " + (count - 1) + 
							"\nat string [" + strIn + "]", "Invalid site latitude");
					br.close();
					return false;
				}
				obj = new LaunchSite();
				obj.sName = name;
				obj.dLat = Lat;
				SitesDB.add(obj);

				// Read and throw away the ending </Site> tag
				strIn = ASTFileIO.getNonBlankLine(br);
				if (strIn == null) break;
			}

			br.close();				// don't really have to do this ...
		} catch (IOException e) {
			return false;
		}

		SitesFilename = filename;
		SitesLoaded = true;
		return SitesLoaded;
	}

}
