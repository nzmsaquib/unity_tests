package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class implements an observer class so that an
 * object can be created that represents a default observer
 * location and time.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTObserver {

	private boolean bValid;									// true if this is a valid obs object
	private ASTLatLon obsLatLon = new ASTLatLon();			// Observer's lat/lon location
	private ASTLatLon.TimeZoneType tZone;					// time zone as an enumerated type
	private ASTDate obsDate = new ASTDate();				// default date
	private ASTTime obsTime = new ASTTime();				// default obs time

	// Location for the optional data file with the default observer location
	private static final String dataFullFilename = "..\\Ast Data Files\\DefaultObsLoc.dat";

	/**
	 * Initialize from the default observer data file, if it
	 * exists. if it doesn't, do some reasonable default values.
	 */
	public ASTObserver() {
		String latStr = "0N";
		String lonStr = "0E";
		String zoneStr = "LON";
		String dateStr = ASTDate.getCurrentDate();
		String timeStr = ASTTime.getCurrentTime();
		
		this.bValid = true;
		
		// If default observer location data file exists, read it in.
		// Otherwise, use the defaults set above		
		if (ASTFileIO.doesFileExist(dataFullFilename)) {
			// Note that we're using try-with-resources so that we don't have to explicitly close the stream when we're done
			try (BufferedReader br = new BufferedReader(new FileReader(dataFullFilename))) {
				String strIn;
				
				// Validate that this is an observer location data file. if it is,
				// continue processing. Otherwise, use defaults set up above.
				strIn=ASTFileIO.readTillStr(br,"<ObserverLocation>",ASTMisc.HIDE_ERRORS);
				if (strIn != null) {
					strIn = ASTFileIO.getSimpleTaggedValue(br, "<Latitude>");
					if (strIn != null) latStr = strIn;
					strIn = ASTFileIO.getSimpleTaggedValue(br, "<Longitude>");
					if (strIn != null) lonStr = strIn;
					strIn = ASTFileIO.getSimpleTaggedValue(br, "<TimeZone>");
					if (strIn != null) zoneStr = strIn;	
				}

				br.close();				// don't really have to do this ...
			} catch (IOException e) {
				// do nothing because the file is optional
			}
		}
		
		if (isValidObsLoc(this, latStr, lonStr, zoneStr, dateStr, timeStr)) {
			// do nothing - we just want the observer location object updated
			// which happens as a side effect of isValidObsLoc
		}
	}

	/*==================================================================
	 * Define 'get' accessors for the object's fields
	 *=================================================================*/

	/**
	 * Returns whether the object is a valid observation object
	 * 
	 * @return				true if object is valid observation object
	 */
	public boolean isValidObsObj() {
		return this.bValid;
	}

	// Location fields
	/**
	 * Returns observer location
	 * 
	 * @return				ASTLatLon object
	 */
	public ASTLatLon getObsLocation() {
		return this.obsLatLon;
	}

	/**
	 * Returns observer latitude
	 * 
	 * @return				decimal latitude value
	 */
	public double getObsLat() {
		return this.obsLatLon.getLat();
	}

	/**
	 * Returns observer longitude
	 * 
	 * @return				decimal longitude value
	 */
	public double getObsLon() {
		return this.obsLatLon.getLon();
	}

	// Time zone
	/**
	 * Returns the observer's time zone
	 * 
	 * @return			ASTLatLon.TimeZoneType
	 */
	public ASTLatLon.TimeZoneType getObsTimeZone() {
		return this.tZone;
	}

	// Time/Data fields
	/**
	 * Returns observer's date
	 * 
	 * @return				ASTDate object
	 */
	public ASTDate getObsDate() {
		return this.obsDate;
	}

	/**
	 * Returns observer's time
	 * 
	 * @return				ASTTime object
	 */
	public ASTTime getObsTime() {
		return this.obsTime;
	}

	/**
	 * Sets the observer objects date and time
	 * 
	 * @param iMonth		observer month
	 * @param iDay			observer day
	 * @param iYear			observer year
	 * @param dTime			observer time
	 */
	public void setObsDateTime(int iMonth, int iDay, int iYear,double dTime) {
		// In an observer, the time is not included in the dDay data field
		this.obsDate.setMonth(iMonth);
		this.obsDate.setiDay(iDay);
		this.obsDate.setYear(iYear);
		this.obsDate.setdDay(iDay);
		this.obsTime.setDecTime(dTime);
	}

	/**
	 * Checks a collection of data strings to see if the data they contain
	 * specify a valid observer location, date, and time.
	 * 
	 * @param obsLoc				observer object to update
	 * @param latStr				string with a latitude
	 * @param lonStr				string with a longitude
	 * @param tzStr					string with a time zone (PST, MST, CST,
	 * 								EST, or LON)
	 * @param dateStr				string with a date
	 * @param timeStr				string with a time
	 * @return						true if the strings represent a valid location. Also, if
	 * 								valid, obsLoc is updated. if not valid, obsLoc is undefined.
	 */
	public static boolean isValidObsLoc(ASTObserver obsLoc,String latStr, String lonStr, String tzStr,
			String dateStr, String timeStr) {

		obsLoc.bValid = false;

		ASTLatLon tmpLatorLon = ASTLatLon.isValidLat(latStr,ASTMisc.HIDE_ERRORS);
		if (!tmpLatorLon.isLatValid()) {
			ASTMsg.errMsg("The observer latitude specified is invalid - try again", "Invalid Latitude");
			return false;
		}
		obsLoc.obsLatLon.setLat(tmpLatorLon.getLat());
		tmpLatorLon = ASTLatLon.isValidLon(lonStr, ASTMisc.HIDE_ERRORS);
		if (!tmpLatorLon.isLonValid()) {
			ASTMsg.errMsg("The observer longitude specified is invalid - try again", "Invalid Longitude");
			return false;
		}
		obsLoc.obsLatLon.setLon(tmpLatorLon.getLon());

		obsLoc.tZone = ASTLatLon.strToTimeZone(tzStr);
		
		ASTDate tmpDate = ASTDate.isValidDate(dateStr,ASTMisc.HIDE_ERRORS);
		if (!tmpDate.isValidDateObj()) {
			ASTMsg.errMsg("The observer date specified is invalid - try again", "Invalid Date");
			return false;
		}
		
		ASTTime tmpTime = ASTTime.isValidTime(timeStr,ASTMisc.HIDE_ERRORS);
		if (!tmpTime.isValidTimeObj()) {
			ASTMsg.errMsg("The observer time specified is invalid - try again", "Invalid Time");
			return false;
		}

		obsLoc.setObsDateTime(tmpDate.getMonth(), tmpDate.getiDay(), tmpDate.getYear(), tmpTime.getDecTime());

		obsLoc.bValid = true;
		return true;
	}

}
