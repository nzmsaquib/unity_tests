package jll.celcalc.ASTUtils;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * <b>This class provides a GUI to allow a user to enter
 * 1 to 3 lines of data.'</b>
 * <p>
 * These methods are static so that a class instance does not have to be 
 * created prior to usage. These methods simplify translating the code to other 
 * languages, and they help enforce consistency in usage and style.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTQuery {

	// Define query return values so invoker won't have to know it is a JOptionPane implementation
	/** value indicating user clicked on OK */
	public static final int QUERY_OK = JOptionPane.OK_OPTION;
	/** value indicating user clicked on CANCEL */
	public static final int QUERY_CANCEL = JOptionPane.CANCEL_OPTION;

	// save the parent pane from the routine that creates the query form so that
	// it can be centered on the parent
	private static JFrame parentFrame = null;

	// Create a text input fields for the GUI
	private static JTextField txtData1 = new JTextField();
	private static JTextField txtData2 = new JTextField();
	private static JTextField txtData3 = new JTextField();

	/**
	 * Saves a reference to the caller's parent frame so queries can be centered on the 
	 * parent application's window.
	 * 
	 * @param parent		parent frame for the main application
	 */
	public static void setParentFrame(JFrame parent) {
		parentFrame = parent;
	}

	/**
	 * Display the query form and center it on the parent.
	 * This method is overloaded to allow 1, 2, or 3 data items
	 * to be entered.
	 * 
	 * This method is overloaded.
	 * 
	 * @param sLabel1		1st label to display
	 * @return				either OK_OPTION or CANCEL_OPTION
	 */
	public static int showQueryForm(String sLabel1) {
		txtData1.setText("");
		Object[] message = {sLabel1,txtData1};

		// The 1st null below is an optional icon. The 2nd null is the initial value. Setting
		// the initial value to null is required so that the 1st data field (i.e., txtData1)
		// will have focus when the dialog comes up.
		return JOptionPane.showOptionDialog(parentFrame, message, "Enter Data...", JOptionPane.OK_CANCEL_OPTION, 
				JOptionPane.QUESTION_MESSAGE, null, new Object[] {" OK "," Cancel "}, null);  
	}
	/**
	 * Display the query form and center it on the parent.
	 * This method is overloaded to allow 1, 2, or 3 data items
	 * to be entered.
	 * 
	 * This method is overloaded.
	 * 
	 * @param sLabel1		1st label to display
	 * @param sLabel2		2nd label to display
	 * @return				either OK_OPTION or CANCEL_OPTION
	 */
	public static int showQueryForm(String sLabel1,String sLabel2) {
		txtData1.setText("");
		txtData2.setText("");
		Object[] message={sLabel1, txtData1, sLabel2,txtData2};

		return JOptionPane.showOptionDialog(parentFrame, message, "Enter Data...", JOptionPane.OK_CANCEL_OPTION, 
				JOptionPane.QUESTION_MESSAGE, null, new Object[] {" OK "," Cancel "}, null);        
	}
	/**
	 * Display the query form and center it on the parent.
	 * This method is overloaded to allow 1, 2, or 3 data items
	 * to be entered.
	 * 
	 * This method is overloaded.
	 * 
	 * @param sLabel1		1st label to display
	 * @param sLabel2		2nd label to display
	 * @param sLabel3		3rd label to display
	 * @param sTitle		title to display
	 * @return				either OK_OPTION or CANCEL_OPTION
	 */
	public static int showQueryForm(String sLabel1,String sLabel2,String sLabel3,String sTitle) {
		txtData1.setText("");
		txtData2.setText("");
		txtData3.setText("");
		Object[] message={sLabel1, txtData1, sLabel2,txtData2, sLabel3,txtData3};

		return JOptionPane.showOptionDialog(parentFrame, message, sTitle, JOptionPane.OK_CANCEL_OPTION, 
				JOptionPane.QUESTION_MESSAGE, null, new Object[] {" OK "," Cancel "}, null);        
	}

	/**
	 * Gets first data field from the query form.
	 * 
	 * @return		1st text data field
	 */
	public static String getData1() {
		return txtData1.getText();
	}
	/**
	 * Gets second data field from the query form.
	 * 
	 * @return		2nd text data field
	 */
	public static String getData2() {
		return txtData2.getText();
	}
	/**
	 * Gets third data field from the query form.
	 * 
	 * @return		3rd text data field
	 */
	public static String getData3() {
		return txtData3.getText();
	}
}
