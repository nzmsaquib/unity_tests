package jll.celcalc.ASTUtils;

/**
 * <b>Miscellaneous constants</b> 
 * <p>
 * Defines miscellaneous constants that are used throughout the
 * programs. They are collected here to ensure uniformity across
 * all the programs. 
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTMisc {
	
	/*=====================================================================
	 * Define various flags that indicate what to do in several of the
	 * utility methods.
	 *====================================================================*/
	
	/**
	 * Whether to abort the program if an error is detected
	 */
	public static final boolean ABORT_PROG = true;
	
	/**
	 * Show errors when doing validations.
	 */
	public static final boolean SHOW_ERRORS = true;

	/**
	 * Don't show errors when doing validations. The invoking routine will display its own messages.
	 */
	public static final boolean HIDE_ERRORS = !SHOW_ERRORS;

	/**
	 * Display time in HMS format
	 */
	public static final boolean HMSFORMAT = true;

	/**
	 * Display angles in DMS format
	 */
	public static final boolean DMSFORMAT = HMSFORMAT;

	/**
	 * Display time/angles in decimal format
	 */
	public static final boolean DECFORMAT = !HMSFORMAT;

	/**
	 * Direction is positive (e.g., North, East)
	 */
	public static final boolean POS_DIRECTION = true;

	/**
	 * Direction is negative (e.g., South, West)
	 */
	public static final boolean NEG_DIRECTION = !POS_DIRECTION;

	/**
	 * Sort lists in ascending order
	 */
	public static final boolean ASCENDING_ORDER = true;

	/**
	 * Sort lists in descending order
	 */
	public static final boolean DESCENDING_ORDER = !ASCENDING_ORDER;
	
	/*============================================================
	 * Define various constants related to astronomy
	 *===========================================================*/
	
	/**
	 * Value indicating that something is undefined
	 */
	public static final double AST_UNDEFINED = 99999999.999999;
	/**
	 * Default Epoch to use when none is specified.
	 */
	public static final double DEFAULT_EPOCH = 2000.0;
	/**
	 * Value to use when RA or Decl is unknown. Any object with this RA or Decl value <b>must</b> be ignored!
	 */
	public static final double UNKNOWN_RA_DECL = AST_UNDEFINED;
	/**
	 * Value to use when the visual magnitude is unknown
	 */
	public static final double UNKNOWN_mV = AST_UNDEFINED;
	/**
	 * Approximate visual magnitude limit for objects visible with the naked eye
	 */
	public static final double mV_NAKED_EYE = 6.5;
	/**
	 * Approximate visual magnitude limit for objects visible with binoculars
	 */
	public static final double mV_BINOCULARS = 10.0;	
	/**
	 * Approximate visual magnitude limit for objects visible with a 10-inch telescope
	 */
	public static final double mV_10INCH = 14.7;
	
	/*=======================================================================
	 * Define various constants related to creating star charts. These
	 * values are likely to need to be adjusted (or computed dynamically)
	 * to match a particular computer's graphics resolution.
	 *======================================================================*/
	
	// Define radius of a dot to represent a magnitude 0 object and the max/min size of dots.
	/** radius when vis magnitude is 0 */
	public static final int mV0RADIUS = 5;
	/** max radius for any vis magnitude */
	public static final int mVMAX_RADIUS = 12;
	/** min radius for any vis magnitude */
	public static final int mVMIN_RADIUS = 2;
	
	// Since star catalogs can be quite large, it may take a long time to plot all objects.
	// The MAX_OBJS_TO_PLOT constant is the recommended maximum number of objects to plot
	// before asking the user if they want to continue. However, if there are OBJS_LIMIT or
	// less, this limit is ignored since the objects will still plot in a reasonable amount of time.
	// The proper value of these constants depends upon a particular computer's CPU speed
	// and its graphics card speed.
	/** max number of objects to plot */
	public static final int MAX_OBJS_TO_PLOT = 50000;
	/** max # of objects before worrying about needing to pause */
	public static final int OBJS_LIMIT = 75000;
	
}
