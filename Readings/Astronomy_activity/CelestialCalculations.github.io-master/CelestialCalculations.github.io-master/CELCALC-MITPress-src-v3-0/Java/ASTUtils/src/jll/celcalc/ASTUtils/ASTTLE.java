package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <b>Define Two Line Elements</b>
 * <p>
 * This class provides the ability to load TLEs
 * from a data file and to store them in an accessible
 * structure. Only one TLE data file can
 * be loaded at a time.
 * 
 * A TLE data file **MUST** be in the format
 * specifically designed for this book. The format can be
 * gleaned by opening the sample TLEs.dat data file and examining
 * its contents. The format is straightforward and is
 * documented in comments within the data file itself.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTTLE {
	
	// Note: TLE data bases are likely fairly small, so they aren't sorted by this class.
	// This means that any searches must be brute force through all the elements.
	
	// We'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
	private static ASTPrt prt = null;
	
	// file types typically associated with TLE data files in this program
	private static final FileNameExtensionFilter fileExtFilter = new FileNameExtensionFilter("TLE Data","dat","tle","txt");

	private static boolean TLEsLoaded = false;
	private static String TLEsFilename = "";							// The currently loaded TLE data file
	
	// This padding is used in case a TLE line is too short when extracting data
	private static final String padding = String.format("%60s"," ").replace(' ',' ');
	
	/**
	 * Constructor for this class. Simply save an ASTPrt instance.
	 * 
	 * @param prtInstance		instance to save
	 */
	public ASTTLE(ASTPrt prtInstance) {
		if (prtInstance == null) {
			ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ASTMisc.ABORT_PROG);
		}

		prt = prtInstance;
	}
	
	/*------------------------------------------------------------
	 * Define a private class and database for the TLEs
	 *-----------------------------------------------------------*/
	
	private static class RawTLE {
		// We could also parse the data lines and store results,
		// but for our purposes we can just calculate them when
		// needed.
		private boolean bNamed;				// true if set has a name
		private String sName;				// optional name for 2 TLE lines
		private String sLine1;
		private String sLine2;
	}

	// This array will hold lines of TLE data
	private static List<RawTLE> TLEdb = new ArrayList<RawTLE>();
	
	/*=========================================================
	 * Define 'getter' methods to return information about
	 * the TLE database. Many of the methods return an
	 * object so that the caller can check the validity of
	 * the results.
	 *========================================================*/
	
	/**
	 * Gets the argument of perigee from data line 2
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLEArgofPeri(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Argument of perigee is on the 2nd data line, columns 35-42
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(34, 42).trim();
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the catalog ID from data line 1
	 * 
	 * @param idx			index into the TLE database
	 * @return				String with result
	 */
	public static String getTLECatID(int idx) {
		String result = null;
		String strTmp;
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Cat ID is on the 1st data line, columns 3-7
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(2, 7).trim();
		return strTmp;
	}
		
	/**
	 * Gets the orbital eccentricity from data line 2
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLEEccentricity(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Eccentricity is on the 2nd data line, columns 27-33
		// with a leading decimal point assumed.
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = "0."+ strTmp.substring(26, 33).trim();
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the epoch date from the TLE.
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTDate with the result
	 * 						or null if an error occurs
	 */
	public static ASTDate getTLEEpochDate(int idx) {
		ASTDate result = null;
		ASTInt iTmp;
		ASTReal rTmp;
		int iYear, daysIntoYear;
		double dT;
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		iTmp = getTLEEpochYear(idx);
		if (iTmp.isValidIntObj()) iYear = iTmp.getIntValue();
		else return result;
		
		if (iYear < 57) iYear = iYear + 2000;
		else iYear = iYear + 1900;
		
		rTmp = getTLEEpochDay(idx);
		if (rTmp.isValidRealObj()) dT = rTmp.getRealValue();
		else return result;
		
		daysIntoYear = ASTMath.Trunc(dT);
		return ASTDate.daysIntoYear2Date(iYear,daysIntoYear);
	}
	
	/**
	 * Gets the Epoch day from data line 1
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLEEpochDay(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Epoch day is on the 1st data line, columns 21-32
		strTmp = getTLELine1(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(20, 32);
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the epoch UT from the TLE.
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with the result
	 */
	public static ASTReal getTLEEpochUT(int idx) {
		ASTReal result;
		ASTReal rTmp;
		double dT;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
		
		rTmp = getTLEEpochDay(idx);
		if (rTmp.isValidRealObj()) dT = rTmp.getRealValue();
		else return result;

		dT = dT - ASTMath.Trunc(dT);		// strip off the day
		dT = ASTMath.Round(dT, 8) * 24.0;	// convert to hours
		result.setRealValue(dT);
		return result;
		
	}
	
	/**
	 * Gets the Epoch year from data line 1
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTInt with result
	 */
	public static ASTInt getTLEEpochYear(int idx) {
		ASTInt result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTInt.isValidInt("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Epoch year is on the 1st data line, columns 19-20
		strTmp = getTLELine1(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(18, 20);
		return ASTInt.isValidInt(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the orbital inclination from data line 2
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLEInclination(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Inclination is on the 2nd data line, columns 9-16
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(8, 16);
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the filename for the currently loaded TLE database.
	 * 
	 * @return			filename for the current TLE db
	 */
	public static String getTLEFilename() {
		return TLEsFilename;
	}
	
	/**
	 * Gets a filter for filtering TLE data files by extension.
	 * This isn't really an instance variable, but this is a
	 * good place to put the method.
	 * 
	 * @return			file extensions filter
	 */
	public static FileNameExtensionFilter getFileExtFilter() {
		return fileExtFilter;
	}
	
	/**
	 * Returns 1st TLE data line
	 * 
	 * @param idx			index into TLE database
	 * @return				line1 or null if doesn't exist
	 */
	public static String getTLELine1(int idx) {
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return null;
		return TLEdb.get(idx).sLine1;
	}
	
	/**
	 * Returns 2nd TLE data line
	 * 
	 * @param idx			index into TLE database
	 * @return				line2 or null if doesn't exist
	 */
	public static String getTLELine2(int idx) {
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return null;
		return TLEdb.get(idx).sLine2;
	}
	
	/**
	 * Gets the Mean Anomaly from data line 2
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLEMeanAnomaly(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Mean anomaly is on the 2nd data line, columns 44-51
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(43, 51);
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the Mean Motion from data line 2
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLEMeanMotion(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// Mean motion is on the 2nd data line, columns 53-63
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(52, 63);
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/**
	 * Gets the number of TLEs in the currently loaded TLE database.
	 * 
	 * @return			number of TLEs in the db
	 */
	public static int getNumTLEDBObjs() {
		return TLEdb.size();
	}
	
	/**
	 * Gets the orbital RAAN from data line 2
	 * 
	 * @param idx			index into the TLE database
	 * @return				ASTReal with result
	 */
	public static ASTReal getTLERAAN(int idx) {
		ASTReal result;
		String strTmp;
		
		// Intentionally parse an invalid string to set the
		// return result as invalid
		result = ASTReal.isValidReal("xyz",ASTMisc.HIDE_ERRORS);
		
		if ((idx < 0) || (idx >= getNumTLEDBObjs())) return result;
	
		// RAAN is on the 2nd data line, columns 18-25
		strTmp = getTLELine2(idx);
		if (strTmp == null) return result;
		
		// Pad to avoid possibility of a substring error.
		strTmp = strTmp + padding;
		strTmp = strTmp.substring(17, 25);
		return ASTReal.isValidReal(strTmp,ASTMisc.HIDE_ERRORS);
	}
	
	/*=======================================================
	 * Public methods for manipulating the TLE database
	 *======================================================*/

	/**
	 * Check to see if a TLE database has been loaded.
	 * 
	 * @return			true if TLE db has been loaded
	 */
	public static boolean areTLEsLoaded() {
		return TLEsLoaded;
	}
	
	/**
	 * Clear all the currently loaded TLE data
	 */
	public static void clearTLEData() {
		// Delete all of the TLE data. In some languages, we'd need
		//  to loop through the database and and delete objects individually.
		TLEdb = new ArrayList<RawTLE>();
		TLEsFilename = "";
		TLEsLoaded = false;
	}

	/**
	 * Decode a TLE data set to show what its elements are
	 * 
	 * @param idx		index into TLE db for set to display,
	 * 					assuming 0-based indexing!
	 * @param txt		text to display as printout heading
	 */
	public static void decodeTLE(int idx, String txt) {
		String strTmp;

		prt.setBoldFont(true);
		prt.println(txt, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();
		displayTLEHeader();
		displayTLEItem(idx);

		// Decode TLE data line 1
		strTmp = TLEdb.get(idx).sLine1 + padding;
		prt.setBoldFont(true);
		prt.println("TLE Data Line 1");
		prt.setBoldFont(false);
		prt.println("Line ID (Always 1)    (Col 1)    : " + strTmp.substring(0, 1));
		prt.println("Catlog Number         (Col 3-7)  : " + strTmp.substring(2, 7));
		prt.println("Classification        (Col 8)    : " + strTmp.substring(7, 8));
		prt.println("Launch Year           (Col 10-11): " + strTmp.substring(9, 11));
		prt.println("Launch Number         (Col 12-14): " + strTmp.substring(11, 14));
		prt.println("Piece of the Launch   (Col 15-17): " + strTmp.substring(14, 17));
		prt.println("Epoch Year            (Col 19-20): " + strTmp.substring(18, 20));
		prt.println("Epoch Day of Year     (Col 21-32): " + strTmp.substring(20, 32));
		prt.println("1st Deriv Mean Motion (Col 34-43): " + strTmp.substring(33, 43));
		prt.println("2nd Deriv Mean Motion (Col 45-52): " + strTmp.substring(44, 52));
		prt.println("Drag Term             (Col 54-61): " + strTmp.substring(53, 61));
		prt.println("Ephemeris Type        (Col 63)   : " + strTmp.substring(62, 63));
		prt.println("Element Number        (Col 65-68): " + strTmp.substring(64, 68));
		prt.println("Checksum              (Col 69)   : " + strTmp.substring(68, 69));
		prt.println();

		strTmp = TLEdb.get(idx).sLine2 + padding;
		prt.setBoldFont(true);
		prt.println("TLE Data Line 2");
		prt.setBoldFont(false);
		prt.println("Line ID (Always 2)    (Col 1)    : " + strTmp.substring(0, 1));
		prt.println("Catlog Number         (Col 3-7)  : " + strTmp.substring(2, 7));
		prt.println("Inclination (degrees) (Col 9-16) : " + strTmp.substring(8, 16));
		prt.println("RAAN (degrees)        (Col 18-25): " + strTmp.substring(17, 25));
		prt.println("Eccentricity          (Col 27-33): " + strTmp.substring(26, 33));
		prt.println("Arg of Perigee (deg)  (Col 35-42): " + strTmp.substring(34, 42));
		prt.println("Mean Anomaly (deg)    (Col 44-51): " + strTmp.substring(43, 51));
		prt.println("Mean Motion (rev/day) (Col 53-63): " + strTmp.substring(52, 63));
		prt.println("Num of Orbits         (Col 64-68): " + strTmp.substring(63, 68));
		prt.println("Checksum              (Col 69)   : " + strTmp.substring(68, 69));
		prt.println();

		prt.setProportionalFont();
	}
	
	/**
	 * Display a header for the TLE data
	 */
	public static void displayTLEHeader() {
		prt.println("TLE            1         2         3         4         5         6");
		prt.println("Set   123456789012345678901234567890123456789012345678901234567890123456789");
		prt.println(String.format("%76s", "=").replace(' ','='));
	}
	
	/**
	 * Display a single TLE data set
	 * 
	 * @param idx			which data set to display
	 */
	public static void displayTLEItem(int idx) {
		if (TLEdb.get(idx).bNamed) prt.println("      " + TLEdb.get(idx).sName);
		prt.print(String.format("%-5d ", idx + 1));
		prt.println(TLEdb.get(idx).sLine1);
		prt.println("      " + TLEdb.get(idx).sLine2);
	}

	/**
	 * Search the currently loaded TLE database and return an index into the
	 * database for the requested catalog ID. An exact match is required. Note
	 * that there may be several items with the same name, so this routine
	 * must be invoked until all are found. Also note that the catalog ID is
	 * an integer, but we'll do a string search rather than converting the
	 * raw TLE data to an integer. The catalog ID is characters 3-7 on both
	 * TLE line 1 and line 2.
	 * 
	 * @param catID			catalog ID of the object to find
	 * @param iStart		where to start searching in the db
	 * @return 				If successful, returns an index into the currently
	 * 						loaded TLE database for the object requested. If 
	 * 						unsuccessful, -1 is returned. Note that this assumes
	 * 0-based indexing!
	 */
	public static int findTLECatID(String catID,int iStart) {
		int dbSize = TLEdb.size();
		String targ, strTmp;

		if (iStart >= dbSize) return -1;

		targ = ASTStr.removeWhitespace(catID);
		for (int i = iStart; i < dbSize; i++) {
			// We can use either Line1 or Line2 - no need to check both
			// as they had better be the same catID! Since Java starts
			// indexing at 0, we want the substring that starts at
			// 2 and goes for 5 characters
			strTmp = TLEdb.get(i).sLine1.substring(2, 7);
			if (targ.compareToIgnoreCase(strTmp) == 0) return i;
		}

		return -1;
	}

	/**
	 * Search the currently loaded TLE database and return an index into the
	 * database for the requested object. The search performed is **not**
	 * case sensitive, and a partial match is counted as successful. Note
	 * that there may be several items with the same name, so this routine
	 * must be invoked until all are found.
	 * 
	 * @param name			name of the object to find
	 * @param iStart		where to start searching in the db
	 * @return				If successful, returns an index into the currently
	 * 						loaded TLE database for the object requested. If 
	 * 						unsuccessful, -1 is returned. Note that this assumes
	 * 						0-based indexing!
	 */
	public static int findTLEName(String name, int iStart) {
		int dbSize = TLEdb.size();
		String targ, strTmp;

		if (iStart >= dbSize) return -1;

		targ = ASTStr.removeWhitespace(name).toLowerCase(Locale.US);
		for (int i = iStart; i < dbSize; i++) {
			if (TLEdb.get(i).bNamed) { 
				strTmp = ASTStr.removeWhitespace(TLEdb.get(i).sName).toLowerCase(Locale.US);
				if (strTmp.indexOf(targ) != -1) return i;
			}
		}

		return -1;
	}
	
	/**
	 * Validate the Keplerian elements and epoch from a TLE data set.
	 * 
	 * @param idx		index into the TLE database
	 * @return			returns true if the Keplerian elements are
	 * 					valid, else false. The calling routine
	 * 					must call the appropriate get* methods
	 * 					to actually get the elements.
	 */
	public static boolean getKeplerianElementsFromTLE(int idx) {
		ASTInt iTmp;
		ASTReal rTmp;
	
      if ((idx < 0) || (idx >= getNumTLEDBObjs())) {
          ASTMsg.errMsg("No TLE Data Set number " + idx + " exists", "Invalid Data Set Number");
          return false;
      }

      // Validate epoch year and epoch day
      iTmp = getTLEEpochYear(idx);
      if (!iTmp.isValidIntObj()) {
          ASTMsg.errMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year");
          return false;
      }
      rTmp = getTLEEpochDay(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid Epoch Day of Year in TLE Data Set", "Invalid Epoch Day of Year");
          return false;
      }
      
      // Validate Keplerian elements
      rTmp = getTLEInclination(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid Inclination in TLE Data Set", "Invalid Inclination");
          return false;
      }
      rTmp = getTLERAAN(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid RAAN in TLE Data Set", "Invalid RAAN");
          return false;
      }
      rTmp = getTLEEccentricity(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid Eccentricity in TLE Data Set", "Invalid Eccentricity");
          return false;
      }
      rTmp = getTLEArgofPeri(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid Argument of Perigee in TLE Data Set", "Invalid Arg of Perigee");
          return false;
      }
      rTmp = getTLEMeanAnomaly(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid Mean Anomaly in TLE Data Set", "Invalid Mean Anomaly");
          return false;
      }
      rTmp = getTLEMeanMotion(idx);
      if (!rTmp.isValidRealObj()) {
          ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion");
          return false;
      }

      return true;
  }
	
	/**
	 * Puts up a browser window and gets a TLE data filename.
	 * 
	 * @return					full name (file and path) of the TLE data file
	 * 							to open or null if no file is selected or
	 * 							file doesn't exist. This routine does **not**
	 * 							check to see if the file is a valid TLE data file, 
	 * 							but it **does** check to see that the file exists.
	 */
	public static String getTLEFileToOpen() {
		String fullFilename = null;
		String[] fileToRead = new String[2];
		
		fileToRead = ASTFileIO.getFileToRead("Select TLE Data File to Load ...","Load TLE Data",ASTTLE.getFileExtFilter());
		fullFilename = fileToRead[ASTFileIO.FULLPATHNAME_IDX];
		
		// See if the file exists
		if (!ASTFileIO.doesFileExist(fullFilename)) {
			 ASTMsg.errMsg("The TLE data file specified does not exist", "TLE Data File Does Not Exist");
			 return null;
		}
				
		return fullFilename;
	}
	
	/**
	 * Loads TLEs database from disk.
	 * 
	 * @param filename			Full filename (including path) to load
	 * @return					true if successful, else false.
	 */
	  public static boolean loadTLEDB(String filename) {
		  int count = 0;
		  String name, line1, line2;
		  boolean named;
		  char c = ' ';
		  RawTLE obj;

		  clearTLEData();

		  // Note that we're using try-with-resources so that we don't have to explicitly close the stream when we're done
		  try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			  String strIn, str2;

			  strIn=ASTFileIO.readTillStr(br,"<TLEs>",ASTMisc.HIDE_ERRORS);
			  if (strIn == null) {
				  ASTMsg.errMsg("The specified file does not appear to\n" +
						  "be a valid TLE data file - try again.", "Invalid TLE Data File");
				  br.close();
				  return false;
			  }
			  // Look for the beginning of the data section, then cycle through
			  // and extract each TLE data line.
			  strIn = ASTFileIO.readTillStr(br,"<Data>");
			  if (strIn == null) {
				  ASTMsg.errMsg("The specified file does not appear to\n" +
						  "be a valid TLE data file - try again.", "Invalid TLE Data File");
				  br.close();
				  return false;
			  }

			  while ((strIn = br.readLine()) != null) {
				  if (strIn.length() <= 0) continue;					// ignore blank lines
				  name = "";
				  named = false;
				  count = count + 1;

				  strIn = strIn.trim();
				  str2 = ASTStr.removeWhitespace(strIn.toLowerCase(Locale.US));
				  if (str2.indexOf("</data>") >= 0) break;				// found end of data section
				  
				  // See if this line is an object name
				  c = strIn.charAt(0);
				  if ((c != '1') && (c != '2')) {
					  name = strIn;
					  named = true;
					  strIn = ASTFileIO.getNonBlankLine(br);
					  if (strIn == null) break;
					  strIn = strIn.trim();
				  }

				  // if there's any error, may as well quit because we'll get out of synch
				  // with the fact that line 1 must precede a line 2
				  c = strIn.charAt(0);
				  if (c != '1') {
					  prt.println("Error for TLE data set " + count + ", line 1, near the string");
					  prt.println(strIn);
					  break;
				  } else line1 = strIn;

				  strIn = ASTFileIO.getNonBlankLine(br);
				  if (strIn == null) break;
				  strIn = strIn.trim();
				  c = strIn.charAt(0);
				  if (c != '2') {
					  prt.println("Error for TLE data set " + count + ", line 2, near the string");
					  prt.println(strIn);
					  break;
				  } else line2 = strIn;

				  obj = new RawTLE();
				  obj.sName = name;
				  obj.bNamed = named;
				  obj.sLine1 = line1;
				  obj.sLine2 = line2;
				  TLEdb.add(obj);
			  }

			  br.close();				// don't really have to do this ...
		  } catch (IOException e) {
			  return false;
		  }

		  TLEsFilename = filename;
		  TLEsLoaded = true;
		  return TLEsLoaded;
	  }

}
