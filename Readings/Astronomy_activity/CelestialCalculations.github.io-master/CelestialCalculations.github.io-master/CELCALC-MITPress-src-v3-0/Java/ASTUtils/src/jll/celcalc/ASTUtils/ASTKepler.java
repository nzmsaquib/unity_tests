package jll.celcalc.ASTUtils;

import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTStyle;

/**
 * <b>Solve Kepler's Equation</b>
 * <p>
 * 
 * This class solves Kepler's equation via a simple iteration
 * scheme and by the Newton/Raphson method.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTKepler {

	// Default max iterations and minimum termination criteria for Kepler's equation
	/** max iterations to perform in solving Kepler's equation */
	public static final int KeplerMaxIterations = 2500;
	/** termination criteria for solving Kepler's equation */
	public static final double KeplerMinCriteria = 0.00000001;
	
	// Define possible ways to determine the true anomaly. This ASTKepler class
	// provides algorithms for solving Kepler's equation. Solving the equation of
	// the center is specific to the Sun and Moon.

	/** enumeration of ways to solve for the true anomaly */
	public enum TrueAnomalyType {
		/** Solve equation of the center */ SOLVE_EQ_OF_CENTER,
		/** Solve Kepler's equation by simple iteration */ SOLVE_SIMPLE_ITERATION_KEPLER,
		/** Solver Kepler's equation by Newton's method */ SOLVE_NEWTON_METHOD_KEPLER
	}

	/**
	 * Solve Kepler's equation via a simple iteration method.
	 * 
	 * This method is overloaded.
	 * 
	 * Note that if prt is null, no interim results are displayed.
	 * 
	 * @param dMA			mean anomaly in degrees
	 * @param dEcc			orbital eccentricity
	 * @param dTerm			termination criteria in radians
	 * @return				the eccentric anomaly in degrees in element 0
	 * 						and the number of iterations required to solve
	 * 						Kepler's equation in element 1 (as a double)
	 */
	public static double[] calcSimpleKepler(double dMA, double dEcc, double dTerm) {
		return calcSimpleKepler(null,dMA, dEcc, dTerm);
	}
	/**
	 * Solve Kepler's equation via a simple iteration method.
	 * 
	 * This method is overloaded.
	 * 
	 * Note that if prt is null, no interim results are displayed.
	 * 
	 * @param prt			object that is the caller's text output area
	 * @param dMA			mean anomaly in degrees
	 * @param dEcc			orbital eccentricity
	 * @param dTerm			termination criteria in radians
	 * @return				the eccentric anomaly in degrees in element 0
	 * 						and the number of iterations required to solve
	 * 						Kepler's equation in element 1 (as a double)
	 */
	public static double[] calcSimpleKepler(ASTPrt prt, double dMA, double dEcc, double dTerm) {
		double[] result = {0.0, 0.0};
		int iter;
		double dEA, dEANext, dMARad, delta;

		// Validate the termination criteria
		if (dTerm < KeplerMinCriteria) dTerm = KeplerMinCriteria;

		printlncond(prt, "    Use Simple Iteration to solve Kepler's equation");
		printlncond(prt, "    Iterate until difference is less than " + dTerm +
				" radians (" + String.format(ASTStyle.genFloatFormat, ASTMath.rad2deg(dTerm) * 3600.0) + " arcseconds)");
		printlncond(prt);

		dMARad = ASTMath.deg2rad(dMA);
		dEA = dMARad;
		printlncond(prt, "      E0 = " + String.format(ASTStyle.genFloatFormat, dEA));

		for (iter = 1; iter < KeplerMaxIterations; iter++) {
			// Note that the sine function here uses radians, not degrees!
			dEANext = dMARad + dEcc * Math.sin(dEA);
			delta = Math.abs(dEANext - dEA);
			printlncond(prt, "      E" + iter + " = " + String.format(ASTStyle.genFloatFormat,dEANext) + 
					", Delta = " + String.format(ASTStyle.genFloatFormat, delta));
			dEA = dEANext;
			if (delta <= dTerm) break;
		}

		if (iter >= KeplerMaxIterations) printlncond(prt, "WARNING: Did not converge, so stopped after " + 
				KeplerMaxIterations + " iterations.");
		printlncond(prt);

		dEA = ASTMath.rad2deg(dEA);
		
		result[0] = dEA;
		result[1] = (double) iter;
		return result;
	}

	/**
	 * Solve Kepler's equation via the Newton/Raphson method.
	 * 
	 * This method is overloaded.
	 * 
	 * Note that if prt is null, no interim results are displayed.
	 * 
	 * @param dMA			mean anomaly in degrees
	 * @param dEcc			orbital eccentricity
	 * @param dTerm			termination criteria in radians
	 * @return				the eccentric anomaly in degrees in element 0
	 * 						and the number of iterations required to solve
	 * 						Kepler's equation in element 1 (as a double)
	 */
	public static double[] calcNewtonKepler(double dMA, double dEcc, double dTerm) {
		return calcNewtonKepler(null, dMA, dEcc, dTerm);
	}
	/**
	 * Solve Kepler's equation via the Newton/Raphson method.
	 * 
	 * This method is overloaded.
	 * 
	 * Note that if prt is null, no interim results are displayed.
	 * 
	 * @param prt			object that is the caller's text output area
	 * @param dMA			mean anomaly in degrees
	 * @param dEcc			orbital eccentricity
	 * @param dTerm			termination criteria in radians
	 * @return				the eccentric anomaly in degrees in element 0
	 * 						and the number of iterations required to solve
	 * 						Kepler's equation in element 1 (as a double)
	 */
	public static double[] calcNewtonKepler(ASTPrt prt, double dMA, double dEcc, double dTerm) {
		double[] result = {0.0, 0.0};
		int iter;
		double dEA, dEANext, dMARad, delta;

		// Validate the termination criteria
		if (dTerm < KeplerMinCriteria) dTerm = KeplerMinCriteria;

		printlncond(prt, "    Use the Newton/Raphson method to solve Kepler's equation");
		printlncond(prt, "    Iterate until difference is less than " + dTerm + " radians (" +
				String.format(ASTStyle.genFloatFormat,ASTMath.rad2deg(dTerm) * 3600.0) + " arcseconds)");
		printlncond(prt);

		dMARad = ASTMath.deg2rad(dMA);
		if (dEcc < 0.75) dEA = dMARad;
		else dEA = Math.PI;
		printlncond(prt, "      E0 = " + String.format(ASTStyle.genFloatFormat,dEA));

		for (iter = 1; iter < KeplerMaxIterations; iter++) {
			// Note that the sine & cosine functions here uses radians, not degrees!
			dEANext = dEA - (dEA - dEcc * Math.sin(dEA) - dMARad) / (1 - dEcc * Math.cos(dEA));
			delta = Math.abs(dEANext - dEA);
			printlncond(prt, "      E" + iter + " = " + String.format(ASTStyle.genFloatFormat,dEANext) + ", Delta = " +
					String.format(ASTStyle.genFloatFormat,delta));
			dEA = dEANext;
			if (delta <= dTerm) break;
		}

		if (iter >= KeplerMaxIterations) printlncond(prt, "WARNING: Did not converge, so stopped after " + 
				KeplerMaxIterations + " iterations.");
		printlncond(prt);

		dEA = ASTMath.rad2deg(dEA);
		
		result[0] = dEA;
		result[1] = (double) iter;
		return result;
	}

	/*------------------------------------------------------------------
	 * Some common helper routines used only in this class
	 *-----------------------------------------------------------------*/

	/**
	 * Provide some routines that allow printing text messages to
	 * a calling routine's scrollable text output area. These methods
	 * are provided to avoid tying this Kepler class to a particular
	 * chapter's GUI, but at the cost of the calling routine having to pass
	 * a prt reference.
	 * 
	 * @param prt				object that is the caller's output area
	 * @param txt				text to be displayed in the output area
	 */
	private static void printlncond(ASTPrt prt) {
		if (prt != null) prt.println();
	}
	private static void printlncond(ASTPrt prt, String txt) {
		if (prt != null) prt.println(txt);
	}

}
