package jll.celcalc.ASTUtils;

/**
 * <b>Provides a class for creating real objects and validating that a string has a real value.</b>
 * <p>
 * This class is useful because Java passes all parameters to a method by value. Hence,
 * in Java one cannot have a statement such as
 * <p>
 * <code>bValid = isValidReal(String inputStr, double dResult)</code>
 * <p>
 * where the <i><code>isValidReal</code></i> method returns a validity (true/false) and
 * modifies <code>dResult</code> to have the value parsed from <code>inputStr</code>.
 * Passing objects rather than primitives such as double avoids this problem.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTReal {
	private boolean bValid;					// true if the object represents a valid real number
	private double dValue;					// the object's value

	/**
	 * Class constructor.
	 * <p>
	 * Since much of the usage of this class involves validating that a string has a valid
	 * real number, assume a new object is invalid until it is proven otherwise.
	 */
	public ASTReal() {
		bValid = false;
		dValue = 0.0;
	}
	
	/**
	 * Class constructor.
	 * <p>
	 * Explicitly give an object a value.
	 * 
	 * @param r			value for the new real object
	 */
	public ASTReal(double r) {
		bValid = true;
		dValue = r;
	}

	/*========================================================
	 * Define 'get/set' accessors for the object's fields
	 *=======================================================*/

	/**
	 * Returns whether the object is a valid real number
	 * 
	 * @return			true if a valid real number
	 */
	public boolean isValidRealObj() {
		return this.bValid;
	}

	/**
	 * Gets the real value in the object
	 * 
	 * @return		real value contained in the object
	 */
	public double getRealValue() {
		return this.dValue;
	}
	
	/**
	 * Sets the real value in the object
	 * 
	 * @param r		value to which the object is to be set
	 */
	public void setRealValue(double r) {
		this.bValid = true;
		this.dValue = r;
	}

	/*=============================================================================
	 * Define some general methods for validating that a string has a valid real
	 * number in it.
	 *============================================================================*/

	/**
	 * Checks to see if a valid number was entered, but don't display
	 * any error messages unless flag is <i><code>ASTMisc.SHOW_ERRORS</code></i>.
	 * to let the calling routine display its own error messages.
	 * <p>
	 * This function is overloaded to allow flag to be an optional
	 * parameter. The default is to display error messages.
	 * <p>
	 * @param inputStr		string to be validated
	 * @return				an ASTReal object containing the result
	 */
	public static ASTReal isValidReal(String inputStr) {
		return isValidReal(inputStr,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Checks to see if a valid number was entered, but don't display
	 * any error messages unless flag is <i><code>ASTMisc.SHOW_ERRORS</code></i>.
	 * to let the calling routine display its own error messages.
	 * <p>
	 * This function is overloaded to allow flag to be an optional
	 * parameter. The default is to display error messages.
	 * <p>
	 * @param inputStr		string to be validated
	 * @param flag			ASTMisc.SHOW_ERRORS if an error message should be displayed 
	 * @return				an ASTReal object containing the result
	 */
	public static ASTReal isValidReal(String inputStr,boolean flag) {
		ASTReal resultObj = new ASTReal();

		resultObj.bValid = false;

		inputStr = ASTStr.removeWhitespace(inputStr);
		try
		{
			resultObj.dValue = Double.parseDouble(inputStr);
			resultObj.bValid = true;
		}
		catch(Exception e)
		{
			ASTMsg.errMsg("Either nothing was entered or what was entered\n" +
					"is invalid. Please enter a valid number.","Invalid Number",flag);
		}

		return resultObj;
	}

}
