package jll.celcalc.ASTUtils;

/**
 * <b>Provides several string and character manipulation utilities.</b>
 * <p>
 * These methods are provided to simplify translating the code
 * to other languages.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTStr {

	/**
	 * Abbreviates a string on the right, if necessary.
	 * 
	 * @param strIn			input string
	 * @param maxlen		maximum length not counting ellipses
	 * @return				returns rightmost maxlen characters of the input string
	 *						along with ellipses if necessary
	 */
	public static String abbrevRight(String strIn,int maxlen) {
		String str;
		if (strIn.length() >= maxlen) str = "... " + strIn.substring(strIn.length() - maxlen);
		else str = strIn;
		return str;
	}

	/**
	 * Compares two strings that may contain alphanumeric characters so that a string such
	 * as 'A9' is "less" than the string 'A11'. If the strings are strictly numeric, a numeric
	 * comparison is done. Alphabetic comparisons are not case sensitive.
	 * <p>
	 * Note that this method only supports integers (e.g., numbers such as 5.9 vs 3.01 may not
	 * compare as expected) and that the maximum size integer in the string is limited to that
	 * which can be represented by a Java int data type.
	 * 
	 * @param str1			First string to compare.
	 * @param str2			Second string to compare.
	 * @return				-1 if str1 is "less than" str2, 0 if str1 is "equal" to str2, and +n
	 * 						if str1 is "greater" than str2.
	 */
	public static int compareAlphaNumeric(String str1,String str2) {
		int idx1 = 0, idx2 = 0;
		int n1,n2;
		int len1 = str1.length();
		int len2 = str2.length();
		int result = 0;
		String s1,s2;

		// The strategy is to recursively extract a numeric or alphabetic substring
		// from both strings and compare them. We have to do this until we reach
		// the end of a string or until two substrings are not equal

		while ((idx1 < len1) && (idx2 < len2)) {
			s1 = extractAlphaNumSubstr(str1,idx1);
			idx1 += s1.length();

			s2 = extractAlphaNumSubstr(str2,idx2);
			idx2 += s2.length();

			if (Character.isDigit(s1.charAt(0)) && Character.isDigit(s2.charAt(0)))  { // Compare as numbers
				n1 = Integer.parseInt(s1);
				n2 = Integer.parseInt(s2);
				result = Integer.compare(n1,n2);
			} else result = s1.compareToIgnoreCase(s2);

			if (result != 0) {						// We only need to continue comparison if they're still equal
				if (result < 0) return -1;
				else return 1;
			}

		}

		// If we get here, either the strings are truly equal, or one is longer than the other.
		if (len1 < len2) return -1;
		else if (len1 > len2) return 1;
		else return 0;
	}

	/**
	 * Counts all occurrences of a specified character in a string.
	 * 
	 * @param str		input string
	 * @param ch		character to search for
	 * @return			a count of the number of times ch appears in str
	 */
	public static int countChars(String str, char ch) {
		int count = 0;

		if ((str == null) || (str.length() <= 0)) return 0;

		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == ch) ++count;
		}
		return count;
	}

	/**
	 * Extract a substring from the input string that is all numeric or all alphabetic.
	 * 
	 * @param str			the input string
	 * @param start			where in str to begin extraction (0-based indexing!)
	 * @return				a purely numeric or a purely alphabetic string
	 */
	public static String extractAlphaNumSubstr(String str,int start) {
		String result = "";
		char c;
		int i;

		int n = str.length();
		if (start >= n) return "";		// already reached end of string

		c = str.charAt(start);
		if (Character.isDigit(c)) {		// accumulate digits
			result = result + c;
			for (i = start+1; i < n; i++) {
				c=str.charAt(i);
				if (! Character.isDigit(c)) break;
				result = result + c;
			}
		} else {						// accumulate alphabetic characters
			result = result + c;
			for (i=start+1; i < n; i++) {
				c=str.charAt(i);
				if (Character.isDigit(c)) break;
				result = result + c;
			}	
		}
		return result;
	}

	/**
	 * Convert a number to a string and insert commas.
	 * This makes large numbers easier to read.
	 * 
	 * @param x			number to convert
	 * @return			a string with commas in
	 * 					it (e.g., 1,245,980.1234)
	 */
	public static String insertCommas(double x) {
		// Since we don't know the number of decimal digits a priori,
		// we'll insert commas into the integer part and then add on
		// the fractional part. The variable f will have a leading
		// 0, which we strip off via the substring command.
		String st;
		double x1 = Math.abs(x);
		double f = ASTMath.Frac(x1);
		
		// Don't use Trunc if the number is very large
		if (x1 > 100000000.0) st = String.format("%,.1f",x1);
		else st = String.format("%,d",ASTMath.Trunc(x1)) + Double.toString(f).substring(1);
		if (x < 0.0) st = "-" + st;
		
		return st;
	}

	/**
	 * Removes all whitespace (spaces, tabs, ctrl-lf, etc.) from a string, including
	 * embedded whitespace.
	 * 
	 * @param str		input string
	 * @return			string with all whitespace removed
	 */
	public static String removeWhitespace(String str) {
		if ((str == null) || (str.length() <= 0)) return "";
		return str.replaceAll("\\s+","");
	}

	/**
	 * Replaces all occurrences of strTarget in strIn with strReplace.
	 * 
	 * @param strIn					input string to be modified
	 * @param strTarget				target string to be searched for in strIn
	 * @param strReplace			replacement string
	 * @return						string with the requested replacement
	 */
	public static String replaceStr(String strIn,String strTarget,String strReplace) {
		return strIn.replaceAll(strTarget,strReplace);
	}

}
