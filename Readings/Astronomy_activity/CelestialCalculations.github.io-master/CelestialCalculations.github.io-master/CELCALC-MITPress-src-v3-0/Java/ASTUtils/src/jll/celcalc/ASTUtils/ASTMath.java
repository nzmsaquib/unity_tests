package jll.celcalc.ASTUtils;

import java.math.BigDecimal;

/**
 * <b>Provides several math-related utilities.</b>
 * <p>
 * These methods are provided to be sure they perform the
 * calculations as required by the programs and to simplify
 * translating the code to another language. For example, the
 * Visual Basic function Truncate already does what the Trunc
 * function defined below does and hence Trunc is unnecessary
 * for Visual Basic. But, the Trunc function is defined here
 * because other languages, such as Java, do not have a
 * built-in Truncate function.
 * <p>
 * All methods are static so that an instance of this
 * class does not have to be created before using these methods.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTMath {
	/**
	 * Define a value to use when comparing small real numbers to see if they are the same.
	 * Comparing reals for strict equality will often fail due to roundoff errors, so just
	 * check for "close enough" in those cases.
	 */
	public static final double EPS = 0.00000005;

	/*=====================================================
	 * Define some general math routines
	 *====================================================*/

	/**
	 * Find the cube root of a number.
	 * 
	 * This method is provided to make it easier to convert to
	 * other languages. The Java Math class has a built in
	 * cube root method
	 * 
	 * @param x			value whose cube root is desired
	 * @return			cube root of x
	 */
	public static double cuberoot(double x) {
		return Math.cbrt(x);
	}

	/**
	 * Determine if the number x is 'close' to the number y
	 * 
	 * @param x			first number to compare
	 * @param y			2nd number to compare
	 * @return			true if x is 'close' to y
	 */
	public static boolean isClose(double x, double y) {
		return Math.abs(x - y) <= EPS;
	}

	/**
	 * Determines whether n is evenly divisible by m.
	 * That is, return true if (n MOD m) is 0, otherwise
	 * return false.
	 * 
	 * @param n			number to check
	 * @param m			divisor
	 * @return			true if n is evenly divisible by m, else false
	 */
	public static boolean isEvenlyDivisible(int n, int m) {
		return (xMOD(n,m) == 0);
	}

	/**
	 * Returns the fractional part of a number.
	 * <p>
	 * <code>
	 * Frac(1.5) = 0.5, Frac(-1.5) = 0.5
	 * </code>
	 * 
	 * @param x			value whose decimal part is desired
	 * @return			the decimal part of the value passed
	 */
	public static double Frac(double x) {
		// The mathematically correct way to get the fractional part of a number is
		//		return Math.abs(x) - (int) Math.abs(x);
		// However, due to roundoff errors, we use strings so that results match what a
		// user would see if the intermediate steps are printed.
		String temp = String.format("%.8f",x);
		String[] parts = temp.split("\\.");
		return Double.parseDouble("0." + parts[1]);
	}

	/**
	 * Round a number to the specified number of digits.
	 * 
	 * @param x				number to round
	 * @param d				number of digits
	 * @return				x rounded to d digits
	 * 
	 * Note: Java does not have a built in function to round
	 * to a specified number of digits. The Java round method
	 * rounds a number to the nearest integer and returns an integer.
	 * 
	 * Caution: This method does not work well if x is large enough
	 * to be displayed in scientific notation, but it's good
	 * enough for our purposes.
	 */
	public static double Round(double x, int d) {
		BigDecimal y = new BigDecimal(String.valueOf(x)).setScale(d, BigDecimal.ROUND_HALF_UP);
		double result = x;
		String s;
		s = y.toPlainString();

		try
		{
			result = Double.parseDouble(s);
		}
		catch(Exception e) {}
		return result;
	}

	/**
	 * Gets the integer part of a number.
	 * <p>
	 * <code>
	 * Trunc(1.5) = 1, Trunc(-1.5) = -1, etc.
	 * </code>
	 * <p>
	 * Some languages have built in functions that perform
	 * this operation, such as Visual Basic's Truncate function.
	 * Also, some languages have a FIX function, which is <b>NOT</b>
	 * the same as the function defined here.
	 * 
	 * @param x			value whose integer part is desired
	 * @return			the integer part of the value passed
	 * 
	 * Caution: This method does not work well if x is large enough
	 * to be displayed in scientific notation, but it's good
	 * enough for our purposes.
	 */
	public static int Trunc(double x) {
		// The mathematically correct way to truncate a number is to just
		// convert it to an integer and account for the sign. That is,
		//		int i = (int) Math.abs(x);
		//		if (x < 0.0) i=i*-1;
		//		return i;
		// However, due to roundoff errors, we use strings so that results match what a
		// user would see if the intermediate steps are printed.
		String temp = String.format("%.8f",x);
		String[] parts = temp.split("\\.");
		return Integer.parseInt(parts[0]);
	}

	/**
	 * Returns x modulo div. For example:
	 * xMOD(270,180) = 90, xMOD(-270.87,180) = 89.13,
	 * xMOD(-400,360) = 320, xMOD(-100,8) = 4.
	 * 
	 * Some languages have built in functions that perform
	 * this operation, such as Visual Basic's MOD operator.
	 * However, they may not handle negative numbers the way
	 * required for this book. (Visual Basic will return a
	 * negative number if x is negative whereas this function
	 * always returns a positive number.)
	 * 
	 * This method is overloaded.
	 * 
	 * @param x			x value in x MOD divisor
	 * @param div		divisor
	 * @return			x modulo div
	 */
	public static int xMOD(int x, int div) {
		if (x < 0) return (x % div) + div;
		else return (x % div);
	}
	/**
	 * Returns x modulo div. For example:
	 * xMOD(270,180) = 90, xMOD(-270.87,180) = 89.13,
	 * xMOD(-400,360) = 320, xMOD(-100,8) = 4.
	 * 
	 * Some languages have built in functions that perform
	 * this operation, such as Visual Basic's MOD operator.
	 * However, they may not handle negative numbers the way
	 * required for this book. (Visual Basic will return a
	 * negative number if x is negative whereas this function
	 * always returns a positive number.)
	 * 
	 * This method is overloaded.
	 * 
	 * @param x			x value in x MOD divisor
	 * @param div		divisor
	 * @return			x modulo div
	 */
	public static double xMOD(double x, double div) {
		if (x < 0) return (x % div) + div;
		else return (x % div);
	}

	/*================================================================================
	 * Define some useful trigonometric routines to use degrees rather than radians.
	 *===============================================================================*/

	/**
	 * Converts degrees to radians
	 * 
	 * @param deg		angle in degrees
	 * @return			angle in radians
	 */
	public static double deg2rad(double deg) {
		return Math.toRadians(deg);
		//	return Math.PI*deg/180.0;
	}	
	/**
	 * Converts radians to degrees
	 * 
	 * @param	rad			angle in radians
	 * @return				angle in degrees
	 */
	public static double rad2deg(double rad) {
		return Math.toDegrees(rad);
		//		return 180.0*rad/Math.PI;
	}

	/*================= Various trig functions to accept degrees rather than radians ===========*/

	/**
	 * Compute the sine of an angle
	 * 
	 * @param deg		angle in degrees
	 * @return			sine of the angle
	 */
	public static double SIN_D(double deg) {
		return Math.sin(deg2rad(deg));
	}

	/**
	 * Compute the cosine of an angle
	 * 
	 * @param deg		angle in degrees
	 * @return			cosine of the angle
	 */
	public static double COS_D(double deg) {
		return Math.cos(deg2rad(deg));
	}

	/**
	 * Compute the tangent of an angle
	 * 
	 * @param deg		angle in degrees
	 * @return			tangent of the angle
	 */
	public static double TAN_D(double deg) {
		return Math.tan(deg2rad(deg));
	}

	/**
	 * Computes the inverse sine of a value
	 * 
	 * @param x			value whose inverse sine is desired
	 * @return			angle (in degrees) whose sine is x
	 */
	public static double INVSIN_D(double x) {
		return rad2deg(Math.asin(x));
	}

	/**
	 * Computes the inverse cosine of a value
	 * 
	 * @param x			value whose inverse cosine is desired
	 * @return			angle (in degrees) whose cosine is x
	 */
	public static double INVCOS_D(double x) {
		return rad2deg(Math.acos(x));
	}

	/**
	 * Computes the arctangent of a value
	 * 
	 * @param x			value whose arctangent is desired
	 * @return			angle (in degrees) whose tangent is x
	 */
	public static double INVTAN_D(double x) {
		return rad2deg(Math.atan(x));
	}

	/**
	 * Computes the arctangent of y/x, corrected for the quadrant the resulting angle is in.
	 * 
	 * @param y			y coordinate
	 * @param x			x coordinate
	 * @return			angle (in degrees) whose tangent is y/x
	 */
	public static double INVTAN2_D(double y,double x) {
		return INVTAN_D(y/x) + quadAdjust(y,x);
	}

	/**
	 * Compute a quadrant adjustment for the inv tan
	 * function given x and y.
	 * 
	 * @param y				y coordinate
	 * @param x				x coordinate
	 * @return				quadrant adjustment
	 */
	public static double quadAdjust(double y, double x) {
		double dQA = 0.0;

		if ((y > 0.0) && (x < 0.0)) dQA = 180.0;
		if ((y < 0.0) && (x < 0.0)) dQA = 180.0;
		if ((y < 0.0) && (x > 0.0)) dQA = 360.0;

		return dQA;
	}

	/*==============================================================
	 * Define some simple distance conversion routines
	 *=============================================================*/

	/**
	 * Convert kilometers to miles
	 * 
	 * @param km        kilometers to convert
	 * @return			miles
	 */
	public static double KM2Miles(double km) {
		return km * 0.62137119;
	}

	/**
	 * Convert miles to kilometers
	 * 
	 * @param miles         miles to convert
	 * @return				kilometers
	 */
	public static double Miles2KM(double miles) {
		return miles * 1.609344;
	}

	/**
	 * Convert AUs to kilometers
	 * 
	 * @param AU         AUs to convert
	 * @return			 kilometers
	 */
	public static double AU2KM(double AU) {
		return AU * 149597870.691;
	}

	/**
	 * Convert kilometers to AUs
	 * 
	 * @param KM         kms to convert
	 * @return			 AUs
	 */
	public static double KM2AU(double KM) {
		return KM / 149597870.691;
	}

	/*=============================================================================
	 * Define a couple of rotations that are needed for the satellites chapter.
	 *============================================================================*/

	/**
	 * Rotate about the x-axis. This is the book's
	 * 'f' family of functions.
	 * 
	 * @param theta         angle, in degrees, to rotate
	 * @param V             vector to rotate
	 * @return				the input vector rotated by theta degrees
	 * 						about the x-axis
	 */
	public static ASTVect RotateX(double theta,ASTVect V) {
		ASTVect F = new ASTVect();
		double x1,y1,z1;

		x1 = V.x();
		y1 = V.y() * ASTMath.COS_D(theta) + V.z() * ASTMath.SIN_D(theta);
		z1 = V.z() * ASTMath.COS_D(theta) - V.y() * ASTMath.SIN_D(theta);

		F.setVect(x1, y1, z1);
		return F;
	}

	/**
	 * Rotate about the z-axis. This is the book's
	 * 'g' family of functions.
	 * 
	 * @param theta         angle, in degrees, to rotate
	 * @param V             vector to rotate
	 * @return				the input vector rotated by theta degrees
	 * 						about the z-axis
	 */
	public static ASTVect RotateZ(double theta, ASTVect V) {
		ASTVect G = new ASTVect();
		double x1,y1,z1;

		x1 = V.x() * ASTMath.COS_D(theta) + V.y() * ASTMath.SIN_D(theta);
		y1 = V.y() * ASTMath.COS_D(theta) - V.x() * ASTMath.SIN_D(theta);
		z1 = V.z();

		G.setVect(x1, y1, z1);
		return G;
	}

}
