package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <b>Provides the GUI to let a user choose a filename
 * and path for loading a data file, typically a star catalog
 * but also other files (e.g., constellations data file)
 * if the file is not where it was expected to be.</b> 
 * <p>
 * The methods in this class are declared static so that we can preserve the
 * file chooser directory path between invocations. This does mean that the
 * method initFileIO must be invoked at the beginning of an application to
 * properly set things up. No harm is done if initFileIO is
 * not called because the system defaults will still work.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTFileIO {
	private static boolean setupCompleted = false;			// avoid initializing things multiple times
	
	// Save a single instance of the file chooser so that the directory path
	// is preserved between invocations, as well as filters.
	private static final JFileChooser fc = new JFileChooser();

	// Save reference to main application frame so we can center the file chooser on the parent window
	private static JFrame parentFrame = null;

	// define which entries are the filename and full pathname fields returned
	// by the methods that ask the user for a file to open/save
	/** index for the filename in file open/save */
	public static final int FILENAME_IDX = 0;
	/** index for the full pathname in file open/save */
	public static final int FULLPATHNAME_IDX = 1;

	/**
	 * Determines whether a specified file already exists. This method
	 * does not create the file if it does not exist.
	 * 
	 * @param fullFilename		full pathname and filename to be checked
	 * @return					true if file exists, else false
	 */
	public static boolean doesFileExist(String fullFilename) {
		if ((fullFilename == null) || (fullFilename.length() <= 0)) return false;
		
		File f = new File(fullFilename);
		
		return f.exists();
	}
	
	/**
	 * Gets the file to open for reading.
	 * 
	 * @param title		title to display for the dialog
	 * @param btn		label to put on the "load" button
	 * @param filter	filter to use for displaying files by extension type
	 * @return			String array containing filename and full pathname. If the user
	 * 					cancels, the String array returned will contain nulls.
	 */
	public static String[] getFileToRead(String title,String btn,FileNameExtensionFilter filter) {
		String[] result = new String[2];
		
		result[0] = "";
		result[1] = "";
		
		fc.setDialogTitle(title);
		fc.setApproveButtonText(btn);
		fc.setFileFilter(filter);

		if (fc.showOpenDialog(parentFrame) == JFileChooser.APPROVE_OPTION) {
			result[FILENAME_IDX] = fc.getSelectedFile().getName();						// filename
			result[FULLPATHNAME_IDX] = fc.getSelectedFile().getAbsolutePath();			// full pathname
		}

		return result;
	}
	
	/**
	 * Reads the input file to get a non-blank line.
	 * 
	 * @param br			file to read from
	 * @return				a non-blank line or null if reached end of file
	 */
	public static String getNonBlankLine(BufferedReader br) {
		String strIn;
		
		try {
			strIn = br.readLine().trim();
			if (strIn.length() > 0) return strIn;
		} catch (IOException e) {
			return null;
		}	
		
		// We only get here if EOF was reached
		return null;
	}
		
	// Note: the following description of the method uses {@literal} blocks
	// so that Javadoc won't try to interpret the <Epoch>, </Epoch>, and <Data> tags.
	/**
	 * Reads the input file looking for a tag, then returns the value
	 * found for that tag.
	 * <p>
	 * For example, a tag might be {@literal <Epoch>} and could exist in the
	 * file in multiple ways: (1) {@literal <Epoch>}, next line has value, next line has {@literal </Epoch>}, 
	 * (2) {@literal <Epoch>value</Epoch>} on a single line, (3) {@literal <Epoch>, next line has value</Epoch>},
	 * and so on. This method is only intended to be used for XML-style tags that have simple
	 * values and not for tags (such as {@literal <Data>}) that will likely have multiple lines of input.
	 * If used for a multi-line tag, only the first line will be extracted as the tag value.
	 * 
	 * @param br				file to read from.
	 * @param tag				Simple "XML-like" tag to search for
	 * @return					string containing the value field for the given tag, a null if the
	 * 							tag does not exist, or "" if the tag exists but has no value field
	 */
	public static String getSimpleTaggedValue(BufferedReader br,String tag) {
		String strIn;
		String endTag = tag.replaceFirst("<","</");
		
		strIn = readTillStr(br,tag, ASTMisc.HIDE_ERRORS);							// readTillStr will worry about upper vs lowercase
		if ((strIn == null) || (strIn.length() <= 0)) return null;
		
		strIn = strIn.replaceFirst(tag,"").trim();				// remove the tag from the line and see what's left
		if ((strIn == null) || (strIn.length() <= 0)) {			// value must be on the next line
			try {
				strIn = br.readLine().trim();
			} catch (IOException e) {
				return null;
			}			
		}
		
		// Replace the endTag, if it's there, with a null string. Whatever is left over
		// is the result we need to return.
		strIn = strIn.replaceAll(endTag,"").trim();
		
		return strIn;
	}
	
	/**
	 * Does a one time initialization of this class, which primarily involves setting up
	 * a file chooser.
	 * 
	 * @param parent		parent frame for the main GUI so that we can center the chooser on the parent
	 */
	public static void initFileIO(JFrame parent) {
		if (setupCompleted) return;
		setupCompleted = true;
		
		parentFrame = parent;
		ASTStyle.setContainerFonts(fc.getComponents(),ASTStyle.TEXT_FONT);
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setAcceptAllFileFilterUsed(true);
		fc.setCurrentDirectory(new File(System.getProperty("user.dir")));
	}
	
	/**
	 * Reads the input file until the expected string is found or an error occurs.
	 * <p>
	 * The string to search for is <b>not</b> case sensitive.
	 * 
	 * This method is overloaded.
	 * 
	 * @param br			file to read from
	 * @param target		string to search for
	 * @return				target string if it was found else a null string
	 */
	public static String readTillStr(BufferedReader br,String target) {
		return readTillStr(br,target,ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Reads the input file until the expected string is found or an error occurs.
	 * <p>
	 * The string to search for is <b>not</b> case sensitive.
	 * 
	 * This method is overloaded.
	 * 
	 * @param br			file to read from
	 * @param target		string to search for
	 * @param showErrors	true if error messages should be displayed 
	 * @return				target string if it was found else a null string
	 */
	public static String readTillStr(BufferedReader br,String target,boolean showErrors) {
		String strIn, str2In,targ;
		
		targ = ASTStr.removeWhitespace(target.toLowerCase(Locale.US));
	
		try {
			while ((strIn = br.readLine()) != null) {
				str2In = ASTStr.removeWhitespace(strIn.toLowerCase(Locale.US));
				if (str2In.indexOf(targ) >= 0) return strIn;	
			}
		} catch (IOException e) {
			if (showErrors)	ASTMsg.criticalErrMsg("Reached end of file but did not find '" + target+"'");
			return null;
		}
		
		if (showErrors) ASTMsg.criticalErrMsg("Expected but did not find '" + target+"'");
		return null;

	}
}
