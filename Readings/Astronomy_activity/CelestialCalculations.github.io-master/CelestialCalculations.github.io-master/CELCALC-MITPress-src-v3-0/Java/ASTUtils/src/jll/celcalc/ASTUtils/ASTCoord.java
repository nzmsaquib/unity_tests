package jll.celcalc.ASTUtils;

/**
 * This class implements a class for performing coordinate
 * system conversions and a coordinate object.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTCoord {

	// A generic coordinate is simply two or three angles. When an equatorial coordinate, the 1st angle
	// is actually in hours rather than degrees. See below for how the class instance variables are
    // to be used depending upon the coordinate system (e.g, equatorial, horizon) under consideration
	private ASTTime time1;
	private ASTAngle angle1;
	private ASTAngle angle2;
	private ASTAngle angle3;

	/**
	 * Create an instance with the given angles
	 * 
	 * This method is overloaded.
	 */
	public ASTCoord() {
		this.time1 = new ASTTime(0.0);
		this.angle1 = new ASTAngle(0.0);
		this.angle2 = new ASTAngle(0.0);
		this.angle3 = new ASTAngle(0.0);
	}
	/**
	 * Create an instance with the given angles
	 * 
	 * This method is overloaded.
	 * 
	 * @param ang1          value for 1st angle
	 * @param ang2          value for 2nd angle
	 */
	public ASTCoord(double ang1, double ang2) {
		this.time1 = new ASTTime(ang1 / 15.0);
		this.angle1 = new ASTAngle(ang1);
		this.angle2 = new ASTAngle(ang2);
		this.angle3 = new ASTAngle(0.0);
	}
	/**
	 * Create an instance with the given angles
	 * 
	 * This method is overloaded.
	 * 
	 * @param ang1          value for 1st angle
	 * @param ang2          value for 2nd angle
	 * @param ang3          value for 3rd angle
	 */
	public ASTCoord(double ang1, double ang2, double ang3) {
		this.time1 = new ASTTime(ang1 / 15.0);
		this.angle1 = new ASTAngle(ang1);
		this.angle2 = new ASTAngle(ang2);
		this.angle3 = new ASTAngle(ang3);
	}

	/*===============================================================
	 * Define 'get' and 'set' accessors for the object's fields
	 *==============================================================*/
	/**
	 * Get the first angle of this coordinate.
	 * 
	 * @return		first angle
	 */
	public ASTAngle getAngle1() {
		return this.angle1;
	}
	/**
	 * Get the 2nd angle of this coordinate.
	 * 
	 * @return		2nd angle
	 */
	public ASTAngle getAngle2() {
		return this.angle2;
	}
	/**
	 * Get the 3rd angle of this coordinate.
	 * 
	 * @return		3rd angle
	 */
	public ASTAngle getAngle3() {
		return this.angle3;
	}
	// We'll use angle 1 for altitude, angle 2 for azimuth when this is a horizon/topocentric coordinate
	/**
	 * Get the altitude angle for this coordinate.
	 * 
	 * @return		altitude angle
	 */
	public ASTAngle getAltAngle() {
		return this.angle1;
	}
	/**
	 * Get the azimuth angle for this coordinate.
	 * 
	 * @return		azimuth angle
	 */
	public ASTAngle getAzAngle() {
		return this.angle2;
	}
	// We'll use angle 1 for latitude, angle 2 for longitude when this
	// is an ecliptic or terrestrial coordinate
	/**
	 * Get the latitude angle for this coordinate.
	 * 
	 * @return	latitude angle
	 */
	public ASTAngle getLatAngle() {
		return this.angle1;
	}
	/**
	 * Get the longitude angle for this coordinate.
	 * 
	 * @return	longitude angle
	 */
	public ASTAngle getLonAngle() {
		return this.angle2;
	}
	// We'll use time1 for hour angle and RA (in hours), angle 2 for decl when this is an equatorial coordinate
	/**
	 * Get the right ascension angle for this coordinate.
	 * 
	 * @return		right ascension
	 */
	public ASTTime getRAAngle() {
		return this.time1;
	}
	/**
	 * Get the hour angle for this coordinate.
	 * 
	 * @return		hour angle
	 */
	public ASTTime getHAAngle() {
		return this.time1;
	}
	/**
	 * Get the declination for this coordinate.
	 * 
	 * @return		declination
	 */
	public ASTAngle getDeclAngle() {
		return this.angle2;
	}
	// For cartesian coordinates, use the order x, y, z
	/**
	 * Get the x Cartesian coordinate.
	 * 
	 * @return		x coordinate
	 */
	public ASTAngle getCartX() {
		return this.angle1;
	}
	/**
	 * Get the y Cartesian coordinate.
	 * 
	 * @return		y coordinate
	 */
	public ASTAngle getCartY() {
		return this.angle2;
	}
	/**
	 * Get the z Cartesian coordinate.
	 * 
	 * @return		z coordinate
	 */
	public ASTAngle getCartZ() {
		return this.angle3;
	}
	// For spherical coordinates, use the order r, alpha, phi
	/**
	 * Get the radius for a spherical coordinate.
	 * 
	 * @return		radius
	 */
	public ASTAngle getSpherR() {
		return this.angle1;
	}
	/**
	 * Get the alpha angle for a spherical coordinate.
	 * 
	 * @return		alpha angle
	 */
	public ASTAngle getSpherAlpha() {
		return this.angle2;
	}
	/**
	 * Get the phi angle for a spherical coordinate.
	 * 
	 * @return		phi angle
	 */
	public ASTAngle getSpherPhi() {
		return this.angle3;
	}

	/**
	 * Set angle 1 for this coordinate.
	 * 
	 * @param angle		angle to which angle 1 is to be set
	 */
	public void setAngle1(double angle) {
		this.angle1.setDecAngle(angle);
	}
	/**
	 * Set angle 2 for this coordinate.
	 * 
	 * @param angle		angle to which angle 2 is to be set
	 */
	public void setAngle2(double angle) {
		this.angle2.setDecAngle(angle);
	}
	/**
	 * Set angle 3 for this coordinate.
	 * 
	 * @param angle		angle to which angle 3 is to be set
	 */
	public void setAngle3(double angle) {
		this.angle3.setDecAngle(angle);
	}
	/**
	 * Set the time (hour angle) for this object.
	 * 
	 * @param tme	hour angle for this object
	 */
	public void setTime1(double tme) {
		this.time1.setDecTime(tme);
	}

	/**
	 * Calculate the obliquity of the ecliptic for a given epoch
	 * 
	 * @param Epoch             epoch for which obliquity is desired
	 * @return					obliquity of the ecliptic calculated for the
	 * 							stated epoch.
	 */
	public static double calcEclipticObliquity(double Epoch) {
		double JD, dT, dDE, dE;
		int iEpoch = ASTMath.Trunc(Epoch);

		JD = ASTDate.dateToJD(1, 0.0, iEpoch);
		dT = (JD - 2451545.0) / 36525.0;
		dDE = 46.815 * dT + 0.0006 * dT * dT - 0.00181 * dT * dT * dT;
		dE = 23.439292 - (dDE / 3600.0);
		return dE;
	}

	/**
	 * Convert Cartesian coordinates to spherical coordinates.
	 * 
	 * @param x					x Cartesian coordinate
	 * @param y					y Cartesian coordinate
	 * @param z					z Cartesian coordinate
	 * @return					spherical coordinates with angle1 as r,
	 * 							angle2 as alpha, and angle3 as phi
	 */
	public static ASTCoord CartesianToSpherical(double x, double y, double z) {
		double r, alpha, phi;
		ASTCoord spherCoord = new ASTCoord(0.0, 0.0, 0.0);

		r = Math.sqrt(x*x + y*y + z*z);
		alpha = ASTMath.INVTAN_D(y / x) + ASTMath.quadAdjust(y, x);
		phi = ASTMath.INVCOS_D(z / r);

		spherCoord.angle1.setDecAngle(r);
		spherCoord.angle2.setDecAngle(alpha);
		spherCoord.angle3.setDecAngle(phi);

		return spherCoord;
	}

	/**
	 * Convert ecliptic coordinates to equatorial coordinates.
	 * 
	 * @param EclLat					ecliptic latitude
	 * @param EclLon            		ecliptic longitude
	 * @param Epoch                     epoch coordinates are in
	 * @return							equatorial coordinates with angle1 as the RA
	 * 									and angle2 as the Decl
	 */
	public static ASTCoord EclipticToEquatorial(double EclLat, double EclLon, double Epoch) {
		double dE, dT, dY, dX, dR, dQA, RA, Decl;
		ASTCoord eqCoord = new ASTCoord(0.0, 0.0);

		dE = calcEclipticObliquity(Epoch);
		dT = ASTMath.SIN_D(EclLat) * ASTMath.COS_D(dE) + ASTMath.COS_D(EclLat) * ASTMath.SIN_D(dE) * ASTMath.SIN_D(EclLon);
		Decl = ASTMath.INVSIN_D(dT);
		dY = ASTMath.SIN_D(EclLon) * ASTMath.COS_D(dE) - ASTMath.TAN_D(EclLat) * ASTMath.SIN_D(dE);
		dX = ASTMath.COS_D(EclLon);
		dR = ASTMath.INVTAN_D(dY / dX);
		dQA = ASTMath.quadAdjust(dY, dX);
		RA = dR + dQA;
		RA = RA / 15.0;

		eqCoord.time1.setDecTime(RA);
		eqCoord.angle2.setDecAngle(Decl);

		return eqCoord;
	}

	/**
	 * Convert an equatorial coordinate expressed an hour angle
	 * and declination to a horizon coordinate.
	 * 
	 * @param HA            hour angle (in hours)
	 * @param Decl          declination in degrees
	 * @param Lat           observer latitude
	 * @return				a coord object in which the 1st angle is the
	 * 						altitude, 2nd angle is the azimuth.
	 */
	public static ASTCoord HADecltoHorizon(double HA, double Decl, double Lat) {
		ASTCoord horizCoord = new ASTCoord(0.0, 0.0);
		double dHA, dT, Alt, Az, dT1, dT2;

		dHA = HA * 15.0;			// Convert HA to degrees
		dT = ASTMath.SIN_D(Decl) * ASTMath.SIN_D(Lat) + ASTMath.COS_D(Decl) * ASTMath.COS_D(Lat) * ASTMath.COS_D(dHA);
		Alt = ASTMath.INVSIN_D(dT);
		dT1 = ASTMath.SIN_D(Decl) - ASTMath.SIN_D(Lat) * ASTMath.SIN_D(Alt);
		dT2 = dT1 / (ASTMath.COS_D(Lat) * ASTMath.COS_D(Alt));
		Az = ASTMath.INVCOS_D(dT2);

		dT1 = ASTMath.SIN_D(dHA);
		if (dT1 > 0) Az = 360.0 - Az;

		horizCoord.angle1.setDecAngle(Alt);
		horizCoord.angle2.setDecAngle(Az);

		return horizCoord;
	}

	/**
	 * Convert Hour Angle to Right Ascension
	 * 
	 * @param HA            hour angle (in hours)
	 * @param LST           LST for the observer
	 * @return				right ascension
	 */
	public static double HAtoRA(double HA, double LST) {
		double RA;

		RA = LST - HA;
		if (RA < 0.0) RA = RA + 24.0;

		return RA;
	}

	/**
	 * Convert a horizon coordinate to an equatorial coordinate
	 * expressed as an hour angle (in hours) and declination.
	 * 
	 * @param Alt           altitude
	 * @param Az            azimuth
	 * @param Lat           observer latitude
	 * @return				a coord object in which the 1st angle is the hour
	 * 						angle (in hours), 2nd angle is the declination
	 */
	public static ASTCoord HorizontoHADecl(double Alt, double Az, double Lat) {
		ASTCoord eqCoord = new ASTCoord(0.0, 0.0);
		double dTmp, Decl, HA;

		dTmp = ASTMath.SIN_D(Alt) * ASTMath.SIN_D(Lat) + ASTMath.COS_D(Alt) * ASTMath.COS_D(Lat) * ASTMath.COS_D(Az);
		Decl = ASTMath.INVSIN_D(dTmp);

		dTmp = ASTMath.SIN_D(Alt) - ASTMath.SIN_D(Lat) * ASTMath.SIN_D(Decl);
		HA = dTmp / (ASTMath.COS_D(Lat) * ASTMath.COS_D(Decl));
		HA = ASTMath.INVCOS_D(HA);

		dTmp = ASTMath.SIN_D(Az);
		if (dTmp > 0) HA = 360 - HA;
		HA = HA / 15.0;

		eqCoord.time1.setDecTime(HA);
		eqCoord.angle2.setDecAngle(Decl);

		return eqCoord;
	}

	/**
	 * Convert a horizon coordinate to right ascension and declination.
	 * 
	 * @param Alt           altitude in degrees
	 * @param Az            azimuth in degrees
	 * @param Lat           observer latitude
	 * @param LST           sidereal time for observer
	 * @return				a coord object in which the 1st angle is the right
	 * 						ascension (in hours) and the 2nd angle is the declination.
	 */
	public static ASTCoord HorizontoRADecl(double Alt, double Az, double Lat, double LST) {
		ASTCoord eqCoord;
		double RA;

		eqCoord = HorizontoHADecl(Alt, Az, Lat);
		RA = HAtoRA(eqCoord.time1.getDecTime(), LST);
		eqCoord.time1.setDecTime(RA);

		return eqCoord;
	}

	/**
	 * Convert satellite Keplerian elements to topocentric coordinates.
	 * 
	 * This method is overloaded.
	 *  
	 * Note: iStp is used so that output printed out by this method
	 * can be merged in with the caller's computational steps.
	 * 
	 * @param obs			observer object w/date and LCT
	 * @param h_sea			observer's height above sea level (meters)
	 * @param DST			true if on Daylight Saving Time
	 * @param epochDate		calendar date for the epoch
	 * @param epochUT		UT time for the epoch
	 * @param inclin		orbital inclination
	 * @param ecc			orbital eccentricity
	 * @param axis			orbital semi-major axis
	 * @param RAAN			orbital RAAN
	 * @param argofperi		orbital argument of perigee
	 * @param M0			orbital mean anomaly
	 * @param MMotion		orbital mean motion
	 * @param solveKepler	what method to use to solve Kepler's equation
	 * @param termCriteria	termination criteria for stopping iteration to
	 * 						solve Kepler's equation
	 * @return				horizon coordinates. The calling
	 * 						routine should check the altitude's value
	 * 						because it is set to AST_UNDEFINED if an error
	 * 						occurs.
	 */
	public static ASTCoord Keplerian2Topo(ASTObserver obs, double h_sea, boolean DST, ASTDate epochDate, double epochUT,
			double inclin, double ecc, double axis, double RAAN, double argofperi,
			double M0, double MMotion,ASTKepler.TrueAnomalyType solveKepler,double termCriteria) {
		return Keplerian2Topo(1,obs,h_sea,DST,epochDate,epochUT,inclin,ecc,axis,RAAN,
				argofperi,M0,MMotion,solveKepler,termCriteria,null);
	}
	/**
	 * Convert satellite Keplerian elements to topocentric coordinates.
	 * 
	 * This method is overloaded.
	 *  
	 * Note: iStp is used so that output printed out by this method
	 * can be merged in with the caller's computational steps.
	 * 
	 * @param iStp			starting step for printing results
	 * @param obs			observer object w/date and LCT
	 * @param h_sea			observer's height above sea level (meters)
	 * @param DST			true if on Daylight Saving Time
	 * @param epochDate		calendar date for the epoch
	 * @param epochUT		UT time for the epoch
	 * @param inclin		orbital inclination
	 * @param ecc			orbital eccentricity
	 * @param axis			orbital semi-major axis
	 * @param RAAN			orbital RAAN
	 * @param argofperi		orbital argument of perigee
	 * @param M0			orbital mean anomaly
	 * @param MMotion		orbital mean motion
	 * @param solveKepler	what method to use to solve Kepler's equation
	 * @param termCriteria	termination criteria for stopping iteration to
	 * 						solve Kepler's equation
	 * @param prt			window for displaying intermediate
	 * 						results. If null, no intermediate
	 * 						results are displayed.
	 * @return				horizon coordinates. The calling
	 * 						routine should check the altitude's value
	 * 						because it is set to AST_UNDEFINED if an error
	 * 						occurs.
	 */
	public static ASTCoord Keplerian2Topo(int iStp, ASTObserver obs, double h_sea, boolean DST, ASTDate epochDate, double epochUT,
			double inclin, double ecc, double axis, double RAAN, double argofperi, double M0,
			double MMotion,ASTKepler.TrueAnomalyType solveKepler,double termCriteria, ASTPrt prt) {
		ASTCoord topoCoord = new ASTCoord(0.0, 0.0);
		double LCT, UT, GST, LST, LSTd, JD, JDe, deltaT, Mt;
		ASTVect[] result;
		int dateAdjust, i = iStp;
		ASTInt dateAdjustObj = new ASTInt();
		ASTVect R = new ASTVect(0.0,0.0,0.0);
		ASTVect V = new ASTVect(0.0,0.0,0.0);
		ASTVect R_obs = new ASTVect(0.0,0.0,0.0);
		ASTVect Rp = new ASTVect(0.0,0.0,0.0);
		ASTVect Rpp = new ASTVect(0.0,0.0,0.0);
		double x,y,z,rp_e,r_eq,r_dist, obsLat;

		// Do all the time-related calculations
		LCT = obs.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, DST, obs.getObsTimeZone(), obs.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(obs.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST,obs.getObsLon());

		printlnCond(prt,i + ".  Convert the observer LCT to UT, GST, and LST and adjust date if needed.");
		i = i + 1;
		printlnCond(prt,"    LCT = " + ASTTime.timeToStr(obs.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(obs.getObsDate()));
		printCond(prt,"     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0) printCond(prt,"previous day ");
		else if (dateAdjust > 0) printCond(prt,"next day ");
		printlnCond(prt,ASTDate.dateToStr(adjustedDate) + ")");
		printlnCond(prt,"    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		printlnCond(prt,"    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		printlnCond(prt);

		JDe = ASTDate.dateToJD(epochDate.getMonth(), epochDate.getdDay() + (epochUT / 24.0), epochDate.getYear());
		printlnCond(prt,i + ".  Compute the Julian day number for the epoch at which the");
		i = i + 1;
		printlnCond(prt,"    Keplerian elements were captured.");
		printlnCond(prt,"    JDe = " + String.format(ASTStyle.JDFormat,JDe));
		printlnCond(prt);

		JD = ASTDate.dateToJD(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear());
		printlnCond(prt,i + ".  Compute the Julian day number for the desired date, being sure to use the");
		i = i + 1;
		printlnCond(prt,"    Greenwich date and UT from step 3, and including the fractional part of the day.");
		printlnCond(prt,"    JD = " + String.format(ASTStyle.JDFormat,JD));
		printlnCond(prt);

		deltaT = JD - JDe;
		printlnCond(prt,i + ".  Compute the total number of elapsed days, including fractional days,");
		i = i + 1;
		printlnCond(prt,"    since the epoch.");
		printlnCond(prt,"    deltaT = JD - JDe = " + String.format(ASTStyle.JDFormat,JD) +
				" - " + String.format(ASTStyle.JDFormat,JDe));
		printlnCond(prt,"           = " + String.format(ASTStyle.genFloatFormat8,deltaT) + " days");
		printlnCond(prt);

		Mt = M0 + 360 * deltaT * MMotion;
		printlnCond(prt,i + ".  Propagate the mean anomaly by deltaT days");
		i = i + 1;
		printlnCond(prt,"    Mt = M0 + 360*deltaT*(Mean Motion) = " + 
				String.format(ASTStyle.genFloatFormat8,M0) + " + 360*" +
				String.format(ASTStyle.genFloatFormat8,deltaT) + "*(" + 
				String.format(ASTStyle.genFloatFormat8,MMotion) + ")");
		printlnCond(prt,"       = " + String.format(ASTStyle.genFloatFormat8,Mt) + " degrees");
		printlnCond(prt);

		printlnCond(prt,i + ".  If necessary, adjust Mt to be in the range [0,360]");
		i = i + 1;
		printCond(prt,"    Mt = Mt MOD 360 = " + String.format(ASTStyle.genFloatFormat8,Mt) + " MOD 360 = ");
		Mt = ASTMath.xMOD(Mt, 360.0);
		printlnCond(prt,String.format(ASTStyle.genFloatFormat8,Mt) + " degrees");
		printlnCond(prt);

		printlnCond(prt,i  + ".  Using Mt in place of M0, convert the Keplerian elements to a state vector");
		i = i + 1;
		result = Keplerian2State(inclin, ecc, axis, RAAN, argofperi, Mt, solveKepler, termCriteria, prt);
		if (result[2].x() < 0) {				// conversion to a state vector failed
			printlnCond(prt,"***An error occurred in converting the Keplerian elements to horizon coordinates");
			topoCoord.angle1.setDecAngle(ASTMisc.AST_UNDEFINED);
			topoCoord.angle2.setDecAngle(ASTMisc.AST_UNDEFINED);
			return topoCoord;
		} else {
			R = result[0];
			V = result[1];
		}
		printlnCond(prt,"    Resulting State Vector:");
		printlnCond(prt,"        [" + ASTStr.insertCommas(ASTMath.Round(R.x(), 3)) + ", " + 
				ASTStr.insertCommas(ASTMath.Round(R.y(), 3)) +
				", " + ASTStr.insertCommas(ASTMath.Round(R.z(), 3)) + 
				"] (positional vector)");
		printlnCond(prt,"        [" + ASTStr.insertCommas(ASTMath.Round(V.x(), 3)) + ", " + 
				ASTStr.insertCommas(ASTMath.Round(V.y(), 3)) +
				", " + ASTStr.insertCommas(ASTMath.Round(V.z(), 3)) + "] (velocity vector)");
		printlnCond(prt,"    State Vector gives a velocity of " + ASTMath.Round(V.len(), 3) + " km/s at a distance");
		printlnCond(prt,"    of " + ASTStr.insertCommas(ASTMath.Round(R.len(), 3)) + " km (" + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(R.len()), 3)) +
				" miles) above the center of the Earth.");
		printlnCond(prt);

		printlnCond(prt,i + ". Convert the positional vector (i.e., ECI coordinates) to topocentric coordinates.");
		i = i + 1;
		LSTd = LST * 15.0;
		obsLat = obs.getObsLat();
		// Convert obs location to ECI
		rp_e = ASTOrbits.EarthRadius + (h_sea / 1000);
		r_eq = rp_e * ASTMath.COS_D(obsLat);
		R_obs.setVect(r_eq * ASTMath.COS_D(LSTd), r_eq * ASTMath.SIN_D(LSTd), rp_e * ASTMath.SIN_D(obsLat));
		// Compute range vector
		Rp.setVect(R.x() - R_obs.x(), R.y() - R_obs.y(), R.z() - R_obs.z());
		// Rotate range vector
		x = Rp.x()*ASTMath.SIN_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.SIN_D(obsLat)*ASTMath.SIN_D(LSTd) - Rp.z()*ASTMath.COS_D(obsLat);
		y = -Rp.x()*ASTMath.SIN_D(LSTd) + Rp.y()*ASTMath.COS_D(LSTd);
		z = Rp.x()*ASTMath.COS_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.COS_D(obsLat)*ASTMath.SIN_D(LSTd) + Rp.z()*ASTMath.SIN_D(obsLat);
		Rpp.setVect(x, y, z);
		// Compute topo Alt, Az
		r_dist = Rpp.len();
		topoCoord.setAngle2(ASTMath.INVTAN2_D(-Rpp.y(), Rpp.x()));			// Azimuth
		topoCoord.setAngle1(ASTMath.INVSIN_D(Rpp.z() / r_dist));			// Altitude

		printlnCond(prt,"    Alt_topo = " + ASTAngle.angleToStr(topoCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) + 
				", Az_topo = " + ASTAngle.angleToStr(topoCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT));
		printlnCond(prt);

		return topoCoord;
	}

	/**
	 * Compute a precession correction
	 * 
	 * @param RAIn              Right Ascension in hours for EpochIn
	 * @param DeclIn    		Declination in degrees for EpochOut
	 * @param EpochIn   		Epoch that RAIn/DeclIn is given in
	 * @param EpochTo     		Epoch to convert to
	 * @return					a coordinate object with the input coordinates
	 * 							precessed to EpochOut
	 */
	public static ASTCoord PrecessEqCoord(double RAIn, double DeclIn, double EpochIn, double EpochTo) {
		double RAdeg, dT, dM, dNd, dNt, dDiff;
		double deltaRA, deltaDecl;
		ASTCoord eqCoord = new ASTCoord(0.0, 0.0);

		// Convert RA to degrees
		RAdeg = RAIn * 15.0;

		dT = (EpochTo - 1900.0) / 100.0;
		dM = 3.07234 + 0.00186 * dT;
		dNd = 20.0468 - 0.0085 * dT;
		dNt = dNd / 15.0;

		dDiff = (EpochTo - EpochIn);
		deltaRA = (dM + dNt * ASTMath.SIN_D(RAdeg) * ASTMath.TAN_D(DeclIn)) * dDiff;
		deltaDecl = dNd * ASTMath.COS_D(RAdeg) * dDiff;
		deltaRA = deltaRA / 3600.0;
		deltaDecl = deltaDecl / 3600.0;

		eqCoord.getRAAngle().setDecTime(RAIn + deltaRA);
		eqCoord.getDeclAngle().setDecAngle(DeclIn + deltaDecl);

		return eqCoord;
	}

	/**
	 * Convert an equatorial coordinate expressed as right ascension
	 * and declination to a horizon coordinate.
	 * 
	 * @param RA            right ascension (in hours)
	 * @param Decl          declination in degrees
	 * @param Lat           observer latitude
	 * @param LST           sidereal time for observer
	 * @return				a coord object in which the 1st angle is the altitude,
	 * 						2nd angle is the azimuth.
	 */
	public static ASTCoord RADecltoHorizon(double RA, double Decl, double Lat, double LST) {
		return HADecltoHorizon(RAtoHA(RA, LST), Decl, Lat);
	}

	/**
	 * Convert Right Ascension to Hour Angle
	 * 
	 * @param RA            right ascension (in hours)
	 * @param LST           LST for the observer
	 * @return				hour angle
	 */
	public static double RAtoHA(double RA, double LST) {
		double HA;

		HA = LST - RA;
		if (HA < 0.0) HA = HA + 24.0;
		return HA;
	}

	/**
	 * Convert spherical coordinates to Cartesian coordinates.
	 * 
	 * @param r						radius for the spherical coordinate
	 * @param alpha					alpha angle for the spherical coordinate
	 * @param phi					phi angle for the spherical coordinate
	 * @return						Cartesian coordinates with angle1 as x,
	 * 								angle2 as y, and angle3 as z
	 */
	public static ASTCoord SphericalToCartesian(double r, double alpha, double phi) {
		double x, y, z;
		ASTCoord cartCoord = new ASTCoord(0.0, 0.0, 0.0);

		x = r * ASTMath.COS_D(alpha) * ASTMath.SIN_D(phi);
		y = r * ASTMath.SIN_D(alpha) * ASTMath.SIN_D(phi);
		z = r * ASTMath.COS_D(phi);

		cartCoord.angle1.setDecAngle(x);
		cartCoord.angle2.setDecAngle(y);
		cartCoord.angle3.setDecAngle(z);

		return cartCoord;
	}

	/*------------------------------------------------------------------------------
	 * Some common helper routines used only in this class
	 *-----------------------------------------------------------------------------*/

	/**
	 * Convert Keplerian elements to a state vector
	 * 
	 * @param inclin		orbital inclination
	 * @param ecc			orbital eccentricity
	 * @param axis			length of the semi-major axis
	 * @param RAAN			RAAN
	 * @param argofperi		argument of perigee
	 * @param M0			mean anomaly
	 * @param solveKepler	which method to use to solve Kepler's equation
	 * @param termCriteria	when to stop iterating for Kepler's equation
	 * @param prt			area in which to display errors, if they occur
	 * @return				returns 3 ASTVect objects. The 1st is the position
	 * 						vector R, the 2nd is the velocity vector V, and
	 * 						the third represents true (1.0) if successful and false (-1) if
	 * 						not. This is necessary because Java does not allow modifying
	 * 						input parameters and can only return an object, a primitive,
	 * 						or an array of like objects.
	 */
	private static ASTVect[] Keplerian2State(double inclin, double ecc, double axis, double RAAN,
			double argofperi, double M0, ASTKepler.TrueAnomalyType solveKepler,
			double termCriteria, ASTPrt prt) {
		double RAANp, argofperip;
		double[] tmpKepler;
		ASTVect[] result = new ASTVect[3];
		ASTVect R = new ASTVect (0.0,0.0,0.0);
		ASTVect V = new ASTVect(0.0,0.0,0.0);
		ASTVect status = new ASTVect(-1.0,-1.0,-1.0);		// assume false
		double rho, EA, dv, dLen, x, y, z, A;
		int Otype;

		// compute semi-latus rectum
		rho = axis * (1.0 - ecc*ecc);

		// initialize as all false values
		result[0] = status;
		result[1] = status;
		result[2] = status;

		// Solve Kepler's equation for the eccentric anomaly and use that to get true anomaly
		if (solveKepler == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
			tmpKepler = ASTKepler.calcSimpleKepler(prt, M0, ecc, termCriteria);
			EA = tmpKepler[0];
		} else {
			tmpKepler = ASTKepler.calcNewtonKepler(prt, M0, ecc, termCriteria);
			EA = tmpKepler[0];
		}
		dv = Math.sqrt((1 + ecc) / (1 - ecc)) * ASTMath.TAN_D(EA / 2.0);
		dv = 2.0 * ASTMath.INVTAN_D(dv);

		// Compute the positional vector
		dLen = rho / (1 + ecc * ASTMath.COS_D(dv));
		x = dLen * ASTMath.COS_D(dv);
		y = dLen * ASTMath.SIN_D(dv);
		z = 0.0;
		R.setVect(x, y, z);

		A = Math.sqrt(ASTOrbits.muEarth / rho);

		// Compute the velocity vector
		x = -A * ASTMath.SIN_D(dv);
		y = A * (ecc + ASTMath.COS_D(dv));
		z = 0.0;
		V.setVect(x, y, z);

		// Make adjustments based on orbit type
		Otype = ASTOrbits.getSatOrbitType(ecc, inclin);
		if (Otype < 0) {
			printlnCond(prt,"Orbit must be elliptical. The computed eccentricity\nand inclination are not supported.");
			return result;
		}

		if ((Otype == 1) || (Otype == 2)) RAANp = 0.0;
		else RAANp = RAAN;
		if ((Otype == 1) || (Otype == 3)) argofperip = 0.0;
		else argofperip = argofperi;

		// Do rotations to xlate R from perifocal to ECI coordinates
		R = ASTMath.RotateZ(-argofperip, R);
		R = ASTMath.RotateX(-inclin, R);
		R = ASTMath.RotateZ(-RAANp, R);

		// Now do the same for the velocity vector
		V = ASTMath.RotateZ(-argofperip, V);
		V = ASTMath.RotateX(-inclin, V);
		V = ASTMath.RotateZ(-RAANp, V);

		status.setVect(1.0, 1.0, 1.0);		// true result
		result[0] = R;
		result[1] = V;
		result[2] = status;
		return result;
	}

	/**
	 * Provide some routines that allow printing text messages to
	 * a calling routine's scrollable text output area. These methods
	 * are provided to avoid tying this class to a particular
	 * chapter's GUI, but at the cost of the calling routine having to pass
	 * a prt reference.
	 * 
	 * @param prt				object that is the caller's output area
	 * @param txt				text to be displayed in the output area
	 * @param centerTxt			if true, center the text
	 */
	private static void printCond(ASTPrt prt, String txt) {
		if (prt != null) prt.print(txt);
	}
	private static void printlnCond(ASTPrt prt) {
		if (prt != null) prt.println();
	}
	private static void printlnCond(ASTPrt prt, String txt) {
		if (prt != null) prt.println(txt);
	}

}
