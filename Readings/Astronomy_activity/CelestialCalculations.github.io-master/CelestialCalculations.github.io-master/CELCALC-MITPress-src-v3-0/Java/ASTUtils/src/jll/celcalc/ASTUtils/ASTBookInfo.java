package jll.celcalc.ASTUtils;

/**
 * <b>Book information</b>
 * <p>
 * Defines information about the current version of the
 * the software, the book name, author, etc.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */
public class ASTBookInfo {
	/** code version */
	public static final String CODE_VER = "Java-3.0";
	/** Book title */
	public static final String BOOK_TITLE = "Celestial Calculations";
	/** Book edition */
//	public static final String BOOK_EDITION = "First Edition";
	public static final String BOOK_EDITION = " ";
	/** Book author */
	public static final String BOOK_AUTHOR = "J. L. Lawrence, PhD";
	/** Book copyright date */
	public static final String BOOK_COPYRIGHT = "Copyright (c) 2018";
}
