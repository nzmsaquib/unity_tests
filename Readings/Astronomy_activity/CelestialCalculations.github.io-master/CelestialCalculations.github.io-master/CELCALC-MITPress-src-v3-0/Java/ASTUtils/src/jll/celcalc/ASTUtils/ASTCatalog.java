package jll.celcalc.ASTUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <b>Provides the ability to load and view star catalogs.</b>
 * <p>
 * The catalog data files **MUST** be in the format specifically
 * designed for this book. The format can be gleaned by opening
 * any of the star catalog data files and examining them. The
 * format is straightforward.
 * 
 * The data files provided with this book were created from
 * publicly available sources, particularly those data files
 * maintained in the NASA HEASARC archives, which are at the URL
 * http://heasarc.gsfc.nasa.gov/docs/archive.html as of the
 * time this book was written.
 * 
 * Because the catalogs can be so large, static methods and
 * data are defined in this class so that the class can easily
 * enforce having only one catalog loaded at a time. The calling
 * routine must create a class instance so that things get
 * initialized properly.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTCatalog implements Comparable<ASTCatalog> {
	// Define the class instance variables, the set of which serve as a 'header' that
	// provides basic information about the catalog.
	private static String cat_type;				// type of catalog
	private static String sDate;				// most recent date that the catalog was updated
	private static String sSource;				// source from which the catalog came
	private static String sDescription;			// brief overall description of the objects in the catalog
	private static int nObjects = 0;			// how many objects are contained in the catalog
	private static int nConstellations = 0;		// how many constellations the objects in the catalog cover
	private static boolean bEpochKnown;			// true if the Epoch on which this catalog is based is known
	private static double Epoch;				// the Epoch, if known, to which object coordinates are referenced.
	private static String sAuthor;				// the author(s) that created the catalog
	private static boolean bmVProvided;			// true if the catalog provides visual magnitudes for the objects in the catalog
	private static double dmVLimit;				// The limiting visual magnitude that was used to filter the catalog to reduce its size.

	/*====================================================
	 * 'getters' for the catalog instance variables. No
	 * 'setters' are provided because only methods in
	 * this class set the catalog instance variables.
	 *====================================================*/

	/**
	 * Gets a filter for filtering star catalog files by extension.
	 * This isn't really an instance variable, but this is a
	 * good place to put the method.
	 * 
	 * @return			file extensions filter
	 */
	public static FileNameExtensionFilter getFileExtFilter() {
		return fileExtFilter;
	}

	/**
	 * Gets the author(s) who created the catalog
	 * 
	 * @return		author(s) who created the catalog
	 */
	public static String getCatAuthor() {
		return sAuthor;
	}

	/**
	 * Gets the most recent date that the catalog was updated.
	 * 
	 * @return			date of most recent catalog update
	 */
	public static String getCatDate() {
		return sDate;
	}

	/**
	 * Gets an amplifying description of this catalog
	 *  
	 * @return			brief catalog description
	 */
	public static String getCatDescription() {
		return sDescription;
	}

	/**
	 * Gets the Epoch to which catalog objects are referenced
	 * 
	 * @return		catalog epoch. Note that this value is meaningless
	 * 				if the catalog epoch is unknown
	 */
	public static double getCatEpoch() {
		return Epoch;
	}

	/**
	 * Gets whether the catalog Epoch is known
	 * 
	 * @return		true if the catalog Epoch is known
	 */
	public static boolean getCatEpochKnown() {
		return bEpochKnown;
	}

	/**
	 * Gets the visual magnitude limit used to filter the catalog. The catalog will contain only
	 * objects that are at least as bright as the limit.
	 * 
	 * @return			visual magnitude limit (maximum brightness) for the objects in the catalog.
	 * 					This value is set regardless of whether the catalog actually contains any
	 * 					visual magnitude for its objects.
	 */
	public static double getCatmVLimit() {
		return dmVLimit;
	}

	/**
	 * Gets whether the catalog provides visual magnitude for the objects
	 * in the catalog.
	 * 
	 * @return			true if the catalog provides the visual magnitude
	 */
	public static boolean getCatmVProvided() {
		return bmVProvided;
	}

	/**
	 * Gets the catalog type.
	 * 
	 * @return			catalog type
	 */
	public static String getCatType() {
		return cat_type;
	}

	/**
	 * Gets the number of constellations that the catalog covers
	 * 
	 * @return		number of constellations covered by the catalog
	 */
	public static int getCatNumConst() {
		return nConstellations;
	}

	/**
	 * Gets how many space objects are in the catalog.
	 * 
	 * @return			number of objects in the catalog
	 */
	public static int getCatNumObjs() {
		return nObjects;
	}

	/**
	 * Gets the source from which this catalog came
	 * 
	 * @return			catalog source
	 */
	public static String getCatSource() {
		return sSource;
	}

	/**------------------------------------------------------------------------------------
	 * Define a class and database for the space objects in the currently loaded catalog.															
	 *------------------------------------------------------------------------------------*/
	private static class SpaceObj {			// must be static so that we don't have to have an ASTCatalog instance
		private String sName;				// the object's primary name
		private String sAltName;			// object's alternate name
		private double dRA;					// right ascension (in decimal hours) for the object w.r.t. the catalog's Epoch
		private double dDecl;				// declination (in decimal degrees) for the object w.r.t. the catalog's Epoch
		private boolean bmVKnown;			// true if the catalog provides a visual magnitude for the object
		private double dmV;					// object's visual magnitude, if known
		private String sComment;			// optional comment contained in the catalog about the object
		private int idx;					// index into the constellations database for the constellation that this object lies within
	}

	/**
	 * Create a new SpaceObj instance with the values passed in.
	 * 
	 * @param Name				name of the object to add
	 * @param AltName			alternate name for the object
	 * @param RA				the object's right ascension (in hours) w.r.t. the catalog Epoch
	 * @param Decl				the object's declination (in degrees) w.r.t. the catalog Epoch
	 * @param mVKnown			true if the object's visual magnitude is known
	 * @param mV				the object's visual magnitude (ignored if the mV is unknown)
	 * @param Comment			optional comment about the object
	 * @param i					index into the Constellations database for the constellation in
	 * 							which this object falls
	 */
	private static void addSpaceObjToCatalog(String Name,String AltName,double RA,double Decl,boolean mVKnown,double mV,
			String Comment,int i) {

		SpaceObj obj = new SpaceObj();

		obj.sName = Name;
		obj.sAltName = AltName;
		obj.dRA = RA;
		obj.dDecl = Decl;
		obj.bmVKnown = mVKnown;
		if (mVKnown) obj.dmV = mV;
		else obj.dmV = ASTMisc.UNKNOWN_mV;
		obj.sComment = Comment;
		obj.idx = i;

		StarCatalog.add(obj);
		nObjects += 1;
	}

	/*==================================================================
	 * Public methods for getting star catalog object data.
	 * 
	 * Note that the space objects are stored in a list, so a 'get/set'
	 * must specify which space object is desired in the list.
	 *=================================================================*/

	/**
	 * Gets an object's alternate name
	 * 
	 * @param idx		which object (0-based indexing) from the space objects database is desired
	 * @return			object's alternate name or null if the object does not exist
	 */
	public static String getCatObjAltName(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return "";
		else return StarCatalog.get(idx).sAltName;
	}

	/**
	 * Gets a comment about the object
	 * 
	 * @param idx		which object (0-based indexing) from the space objects database is desired
	 * @return			comment, if there is one
	 */
	public static String getCatObjComment(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return "";
		else return StarCatalog.get(idx).sComment;
	}

	/**
	 * Gets an index into the constellations database for the constellation
	 * in which this object lies within.
	 * 
	 * @param idx		which object (0-based indexing) from the space objects database is desired
	 * @return			index into constellation database or -1 if an error
	 */
	public static int getCatObjConstIdx(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return -1;
		else return StarCatalog.get(idx).idx;
	}

	/**
	 * Gets an object's Decl w.r.t. the catalog Epoch
	 * 
	 * @param idx		which object (0-based indexing) in the space objects database
	 * @return			object's Decl or UNKNOWN_RA_DECL if the object does not exist
	 */
	public static double getCatObjDecl(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return ASTMisc.UNKNOWN_RA_DECL;
		else return StarCatalog.get(idx).dDecl;
	}

	/**
	 * Gets an object's visual magnitude
	 * 
	 * @param idx		which object (0-based indexing) in the space objects database
	 * @return			object's visual magnitude or UNKNOWN_mV if the object does not
	 * 					exist. This return value if valid only if the object's visual
	 * 					magnitude is known
	 */
	public static double getCatObjmV(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return ASTMisc.UNKNOWN_mV;
		else return StarCatalog.get(idx).dmV;
	}

	/**
	 * Gets whether the object's visual magnitude is known
	 * 
	 * @param idx		which object (0-based indexing) in the space objects database
	 * @return			false if the visual magnitude is unknown or if the object does not exist
	 */
	public static boolean getCatObjmVKnown(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return false;
		else return StarCatalog.get(idx).bmVKnown;
	}

	/**
	 * Gets an object's name
	 * 
	 * @param idx		which object (0-based indexing) from the space objects database is desired
	 * @return			object's name or null if the object does not exist
	 */
	public static String getCatObjName(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return "";
		else return StarCatalog.get(idx).sName;
	}

	/**
	 * Gets an object's RA w.r.t. the catalog Epoch
	 * 
	 * @param idx		which object (0-based indexing) in the space objects database
	 * @return			object's RA or UNKNOWN_RA_DECL if the object does not exist
	 */
	public static double getCatObjRA(int idx) {
		if ((idx < 0) || (idx >= nObjects)) return ASTMisc.UNKNOWN_RA_DECL;
		else return StarCatalog.get(idx).dRA;
	}

	// Database of the objects in the currently loaded star catalog
	private static List<SpaceObj> StarCatalog = null;

	/**
	 * Define the ways a star catalog can be sorted and a method for converting the
	 * 'enum' type to a printable string.
	 */
	public enum CatalogSortField {
		/** by constellation name */ CONSTELLATION("Constellation"),
		/** by constellation name and then by object names */	CONST_AND_OBJNAME("Constellation and Object Name"),
		/** by object name */ OBJNAME("Object's Name"),
		/** by an an object's alternate name */ OBJ_ALTNAME("Object's Alternate Name"),
		/** by an object's Right Ascension */ RA("Object's Right Ascension"),
		/** by an object's Declination */ DECL("Object's Declination"),
		/** by an object's visual magnitude */ VISUAL_MAGNITUDE("Object's Visual Magnitude");

		private String str;			// printable string for this enumerated type
		private CatalogSortField(String s) {
			str = s;
		}

		/**
		 * Gets a printable string for an enumerated type item
		 * 
		 * @return		printable string for an enumerated type
		 */
		public String toStr() {
			return this.str;
		}
	}

	/**-------------------------------------------------------------------------
	 * Define some other data items that are used only in this class.
	 *-------------------------------------------------------------------------*/
	// we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
	private static ASTPrt prt = null;

	// Since the catalogs can be quite large, it may take a long time to display all objects. The MAX_OBJS_TO_PRT constant
	// is the maximum number of objects to display before asking the user if they want to continue. However, if there
	// are OBJS_LIMIT or less, this limit is ignored since the objects will still print in a reasonable amount of time.
	private static final int MAX_OBJS_TO_PRT = 1000;
	private static final int OBJS_LIMIT = 2500;

	// file types typically associated with star catalogs in this program
	private static final FileNameExtensionFilter fileExtFilter = new FileNameExtensionFilter("Star Catalog","dat","cat","txt");

	// avoid initializing more than once
	private static boolean classInitialized = false;		// keep track of whether the class has been initialized
	private static boolean catalogLoaded = false;			// keep track of whether a catalog is loaded

	// As per the format requirements, objects in the catalog data files are to be sorted
	// in ascending order by constellation and then in ascending order by object name
	// within the constellations. However, once a catalog is loaded, the user may want
	// the catalog sorted in some other order. The boolean saveCurrentSortOrder is needed
	// because we need a way to pass the desired sorting order to the various comparators defined below.
	// These two "save" flags are also used to avoid sorting when we don't need to.
	private static boolean saveCurrentSortOrder = ASTMisc.ASCENDING_ORDER;
	private static CatalogSortField saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME;	

	/*===================================================================
	 * Public methods for manipulating a catalog
	 *==================================================================*/

	/**
	 * Clears all the currently loaded catalog data including all space objects in the catalog
	 */
	public static void clearCatalogAndSpaceObjects() {
		// Clear and recreate the various objects that need to be recreated.
		// In other languages, we'd have to do a dispose operation here to
		// get rid of all the old objects.

		// First, clear the catalog header information
		cat_type = "";
		sDate = "";
		sSource = "";
		sDescription = "";
		nObjects = 0;
		nConstellations = 0;
		Epoch = ASTMisc.DEFAULT_EPOCH;
		bEpochKnown = true;
		bmVProvided = false;
		dmVLimit = ASTMisc.UNKNOWN_mV;
		sAuthor = "";

		saveCurrentSortOrder = ASTMisc.ASCENDING_ORDER;
		saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME;

		// Now delete all of the space objects in the star catalog
		// In some languages, we'd need to loop through the star catalog
		// and delete objects individually.		
		StarCatalog = new ArrayList<SpaceObj>();

		catalogLoaded = false;
	}

	/**
	 * Displays all space objects in the currently loaded catalog
	 * by however the catalog is currently sorted.
	 * 
	 * This method is overloaded so that passing mVFilter is
	 * optional. If not specified, all objects are displayed.
	 */
	public static void displayAllCatalogObjects() {
		displayAllCatalogObjects(ASTMisc.UNKNOWN_mV);
	}
	/**
	 * Displays all space objects in the currently loaded catalog
	 * by however the catalog is currently sorted.
	 * 
	 * This method is overloaded so that passing mVFilter is
	 * optional. If not specified, all objects are displayed.
	 * 
	 * @param mVFilter			filter for displaying only those objects
	 * 							that are at least as bright as mVFilter
	 */
	public static void displayAllCatalogObjects(double mVFilter) {
		String str;
		ASTInt n = new ASTInt();
		int lineCount = 0;
		int prtLimit;
		boolean showObj;

		if (!isInitAndLoaded()) return;

		// See if we need to bother about pausing during display
		prtLimit = OBJS_LIMIT + 10;			// this really means to ignore pausing during display
		if (getCatNumObjs() > OBJS_LIMIT) {
			if (ASTMsg.abortMsg("Displaying "+getCatNumObjs()+" objects may take a long time.")) return;

			if (ASTQuery.showQueryForm("Enter Max # of Objects to Display at a Time\n"+
					"(Recommend no More Than " + MAX_OBJS_TO_PRT + " Objects at a Time)") != ASTQuery.QUERY_OK) return;
			str = ASTQuery.getData1();
			if ((str == null) || (str.length() <= 0)) return;

			n = ASTInt.isValidInt(str,ASTMisc.HIDE_ERRORS);
			if (n.isValidIntObj()) prtLimit = n.getIntValue();
			else prtLimit = MAX_OBJS_TO_PRT;
		}

		displayObjHeader();

		for (int i = 0; i < getCatNumObjs(); i++) {
			lineCount += 1;
			if (lineCount > prtLimit) {
				if (ASTMsg.abortMsg("Displaying "+i+" of "+getCatNumObjs()+" objects")) return;
				lineCount = 0;
			}

			// See if this object should be filtered out
			if (getCatObjmVKnown(i)) showObj = (getCatObjmV(i) <= mVFilter);
			else showObj = true;				// show object even if we don't know its mV value

			if (showObj) displayObject(i + 1, i);
		}
	}

	/**
	 * Displays all space objects in the currently loaded catalog
	 * by constellation.
	 * 
	 * This method is overloaded so that passing mVFilter is
	 * optional. If not specified, all objects are displayed.
	 * 
	 * @param idx			index into constellations database for the
	 * 						constellation of interest
	 * @param sortOrder		Sort in ascending order if ASCENDING_ORDER, else
	 * 						sort in descending order
	 */
	public static void displayAllObjsByConstellation(int idx,boolean sortOrder) {
		displayAllObjsByConstellation(idx,sortOrder,ASTMisc.UNKNOWN_mV);
	}
	/**
	 * Displays all space objects in the currently loaded catalog
	 * by constellation.
	 * 
	 * This method is overloaded so that passing mVFilter is
	 * optional. If not specified, all objects are displayed.
	 * 
	 * @param idx			index into constellations database for the
	 * 						constellation of interest
	 * @param sortOrder		Sort in ascending order if ASCENDING_ORDER, else
	 * 						sort in descending order
	 * @param mVFilter		filter for displaying only those objects
	 * 						that are at least as bright as mVFilter
	 */
	public static void displayAllObjsByConstellation(int idx ,boolean sortOrder, double mVFilter) {
		int iStart, iEnd, iNumObjs;		// How many objects to print and where they stop/end in the Star Catalog
		int lineCount, i, prtLimit;
		ASTInt n = new ASTInt();
		boolean showObj;

		if (!isInitAndLoaded()) return;

		if ((idx < 0) || (idx > ASTConstellation.getNumConstellations() - 1)) {
			ASTMsg.errMsg("The Constellation requested does not exist", "Invalid Constellation Name");
			return;
		}

		iStart = -1;
		iEnd = -1;
		iNumObjs = 0;
		lineCount = 0;
		
		// We need to be sure the star catalog is already sorted by constellation name
		sortStarCatalog(CatalogSortField.CONST_AND_OBJNAME,sortOrder);			// this will sort only if we need to

		// First, go through the objects and find out how many we need to display
		for (i=0; i < getCatNumObjs(); i++) {
			if (getCatObjConstIdx(i) == idx) {
				iNumObjs += 1;
				iEnd = i;
				if (iStart < 0) iStart = i;
			} else	if (iStart >= 0) break;			// We reached the end of the objects in this constellation.
		}
		
		// See if we need to bother about pausing during display
		prtLimit = OBJS_LIMIT + 10;			// this really means to ignore pausing during display
		if (iNumObjs > OBJS_LIMIT) {
			if (ASTMsg.abortMsg("Displaying "+iNumObjs+" objects may take a long time.")) return;

			if (ASTQuery.showQueryForm("Enter Max # of Objects to Display at a Time\n"+
					"(Recommend no More Than " + MAX_OBJS_TO_PRT + " Objects at a Time)") != ASTQuery.QUERY_OK) return;
			n = ASTInt.isValidInt(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (n.isValidIntObj()) prtLimit = n.getIntValue();
			else prtLimit = MAX_OBJS_TO_PRT;
		}

		prt.println("The currently loaded catalog has "+iNumObjs+" objects in the constellation " +
				ASTConstellation.getConstName(idx) + " ("+ASTConstellation.getConstAbbrevName(idx)+")");
		displayObjHeader();
		if (iNumObjs <= 0) return;
		
		for (i = iStart ; i <= iEnd; i++) {
			lineCount += 1;
			if (lineCount > prtLimit) {
				if (ASTMsg.abortMsg("Displaying "+(i - iStart + 1)+" of "+iNumObjs+" objects")) return;
				lineCount = 0;
			}

			// See if this object should be filtered out
			if (getCatObjmVKnown(i)) showObj = (getCatObjmV(i) <= mVFilter);
			else showObj = true;				// show object even if we don't know its mV value

			if (showObj) displayObject(i - iStart + 1, i);
		}
	}

	/**
	 * Displays all space objects in the currently loaded catalog by
	 * range, however the catalog is currently sorted.
	 * 
	 * This method is overloaded so that passing mVFilter is
	 * optional. If not specified, all objects are displayed.
	 *
	 * @param nStart		the range start (assuming 0-based numbering so that 0 is the 1st item)
	 * @param nEnd			the end of the range (assuming 0-based numbering)
	 */
	public static void displayAllObjsByRange(int nStart,int nEnd) {
		displayAllObjsByRange(nStart,nEnd,ASTMisc.UNKNOWN_mV);
	}
	/**
	 * Displays all space objects in the currently loaded catalog by
	 * range, however the catalog is currently sorted.
	 * 
	 * This method is overloaded so that passing mVFilter is
	 * optional. If not specified, all objects are displayed.
	 *
	 * @param nStart		the range start (assuming 0-based numbering so that 0 is the 1st item)
	 * @param nEnd			the end of the range (assuming 0-based numbering)
	 * @param mVFilter		filter for displaying only those objects that are at least as bright as mVFilter
	 */
	public static void displayAllObjsByRange(int nStart, int nEnd, double mVFilter) {
		int iStart, iEnd, iNumObjs, lineCount, prtLimit;
		ASTInt n = new ASTInt();
		boolean showObj;
		String str;

		if (!isInitAndLoaded()) return;

		// First, be sure that nStart <= nEnd and that both are in range
		if (nStart > nEnd) {
			iStart = nEnd;
			iEnd = nStart;
		} else {
			iStart = nStart;
			iEnd = nEnd;
		}
		if (iStart < 0) iStart = 0;
		if (nEnd > getCatNumObjs() - 1) iEnd = getCatNumObjs() - 1;

		lineCount = 0;
		iNumObjs = iEnd - iStart + 1;			// number of objects to display

		// See if we need to bother about pausing during display
		prtLimit = OBJS_LIMIT + 10;			// this really means to ignore pausing during display
		if (iNumObjs > OBJS_LIMIT) {
			if (ASTMsg.abortMsg("Displaying "+iNumObjs+" objects may take a long time.")) return;

			if (ASTQuery.showQueryForm("Enter Max # of Objects to Display at a Time\n"+
					"(Recommend no More Than " + MAX_OBJS_TO_PRT + " Objects at a Time)") != ASTQuery.QUERY_OK) return;
			str = ASTQuery.getData1();
			if ((str == null) || (str.length() <= 0)) return;

			n = ASTInt.isValidInt(str,ASTMisc.HIDE_ERRORS);
			if (n.isValidIntObj()) prtLimit = n.getIntValue();
			else prtLimit = MAX_OBJS_TO_PRT;
		}

		prt.println("The currently loaded catalog has "+iNumObjs+" objects in the requested index range");
		displayObjHeader();

		for (int i = iStart ; i <= iEnd; i++) {
			lineCount += 1;
			if (lineCount > prtLimit) {
				if (ASTMsg.abortMsg("Displaying "+(i - iStart + 1)+" of "+iNumObjs+" objects")) return;
				lineCount = 0;
			}

			// See if this object should be filtered out
			if (getCatObjmVKnown(i)) showObj = (getCatObjmV(i) <= mVFilter);
			else showObj = true;				// show object even if we don't know its mV value

			if (showObj) displayObject(i + 1, i);
		}
	}

	/**
	 * Displays the catalog header information
	 */
	public static void displayCatalogInfo() {
		String str;

		if (!isInitAndLoaded()) return;

		prt.println("Catalog Type: " + getCatType(),ASTPrt.CENTERTXT);
		prt.println(getCatDescription(),ASTPrt.CENTERTXT);
		if (getCatmVProvided()) str = String.format(ASTStyle.mVFormat,getCatmVLimit());
		else str = "***Visual Magnitude not provided***";		
		prt.println("Limiting Visual Magnitude for Catalog: "+str,ASTPrt.CENTERTXT);
		if (getCatEpochKnown()) str = String.format(ASTStyle.epochFormat,getCatEpoch());
		else str = "***Epoch not provided***";
		prt.println("Catalog Epoch: " + str,ASTPrt.CENTERTXT);
		prt.println("Catalog Author(s): " + getCatAuthor(),ASTPrt.CENTERTXT);
		prt.println();
		prt.println("Catalog Date: " + getCatDate());
		prt.println("Original Source: " + getCatSource());
		prt.println();
		prt.println(String.format("Catalog covers %d Constellations and has a total of %d Objects",
				getCatNumConst(),getCatNumObjs()));
	}

	/**
	 * Displays all information in the catalog about the i-th space
	 * object entry in the currently loaded and sorted catalog
	 * 
	 * @param i				Which object to display (assumes 0-based indexing).
	 */
	public static void displayFullObjInfo(int i) {
		String strRA, strDecl;

		if (!isInitAndLoaded()) return;

		if ((i < 0) || (i >= getCatNumObjs())) {
			prt.println("The object specified does not exist in the currently loaded catalog");
			return;
		}

		int idx = getCatObjConstIdx(i);
		strRA = ASTTime.timeToStr(getCatObjRA(i),ASTMisc.HMSFORMAT);
		strDecl = ASTAngle.angleToStr(getCatObjDecl(i),ASTMisc.DMSFORMAT);

		prt.println("The object named '" + getCatObjName(i)+"' is in the constellation " +
				ASTConstellation.getConstName(idx)+" ("+ASTConstellation.getConstAbbrevName(idx)+")");
		prt.println("It's alternate name (if any) in this catalog is: "+getCatObjAltName(i));
		prt.println("The object is located at "+strRA + " RA, " + strDecl + " Decl");

		if (getCatObjmVKnown(i)) prt.println("It's visual magnitude is "+String.format(ASTStyle.mVFormat,getCatObjmV(i)));
		else prt.println("It's visual magnitude is not given in the catalog");
		prt.println("Additional information (if any) in the catalog about this object: "+getCatObjComment(i));
	}

	/**
	 * Finds an object in the current catalog given its alternate name.
	 * Searches are not case sensitive.
	 * 
	 * @param searchStr		The name to search for. An exact match, ignoring whitespace and case, is required.
	 * @return				index into the StarCatalog list if successful, else -1. 0-based indexing is assumed!
	 */
	public static int findObjByAltName(String searchStr) {
		String targ = ASTStr.removeWhitespace(searchStr);

		for (int i = 0; i < nObjects; i++) {
			if (targ.equalsIgnoreCase(ASTStr.removeWhitespace(getCatObjAltName(i)))) return i;
		}

		return -1;			// not found
	}

	/**
	 * Searches the currently loaded star catalog and returns an index into
	 * the database for all objects that contain the target substring
	 * in their 'comments' field. Searches are not case sensitive.
	 * 
	 * @param targ			target substring to search for
	 * @return				dynamic array of integers that will contain the indices into 
	 * 						the star catalog for the requested objects. If unsuccessful, the 
	 * 						list is empty. 0-based indexing	is assumed for the indices in 
	 * 						the list that gets generated.
	 */
	public static List<Integer> findObjsByComments(String targ) {
		String targStr, tmpStr;
		List<Integer> result = new ArrayList<Integer>();

		targStr = ASTStr.removeWhitespace(targ.toLowerCase(Locale.US));

		for (int i = 0; i < nObjects; i++) {
			tmpStr = ASTStr.removeWhitespace(getCatObjComment(i).toLowerCase(Locale.US));
			if (tmpStr.indexOf(targStr) >= 0) result.add(i);
		}

		return result;
	}

	/**
	 * Finds an object in the current catalog given its name.
	 * Searches are not case sensitive.
	 * 
	 * @param searchStr		The name to search for. An exact match, ignoring whitespace and case, is required.
	 * @return				index into the StarCatalog list if successful, else -1. 0-based indexing is assumed!
	 */
	public static int findObjByName(String searchStr) {

		String targ = ASTStr.removeWhitespace(searchStr);

		for (int i = 0; i < nObjects; i++) {
			if (targ.equalsIgnoreCase(ASTStr.removeWhitespace(getCatObjName(i)))) return i;
		}

		return -1;			/// not found
	}
	
	/**
	 * Puts up a browser window and gets a star catalog filename.
	 * 
	 * @return					full name (file and path) of the catalog file
	 * 							to open or null if no file is selected or
	 * 							file doesn't exist. This routine does **not**
	 * 							check to see if the file is a valid catalog, 
	 * 							but it **does** check to see that the file exists.
	 */
	public static String getCatFileToOpen() {
		String fullFilename = null;
		String[] fileToRead = new String[2];
		
		fileToRead = ASTFileIO.getFileToRead("Select Catalog to Load ...","Load Catalog",ASTCatalog.getFileExtFilter());
		fullFilename = fileToRead[ASTFileIO.FULLPATHNAME_IDX];
		
		// See if the file exists
		if (!ASTFileIO.doesFileExist(fullFilename)) {
			 ASTMsg.errMsg("The Star Catalog file specified does not exist", "Catalog Does Not Exist");
			 return null;
		}
				
		return fullFilename;
	}

	/**
	 * Checks to see if a star catalog has been loaded
	 * 
	 * @return		true if a star catalog has been loaded.
	 */
	public static boolean isCatalogLoaded() {
		return catalogLoaded && classInitialized;
	}

	/**
	 * Does a one-time initialization required for loading or using Star Catalogs
	 * 
	 * @param prtInstance	instance for performing output to the application's scrollable text output area
	 */
	public static void initStarCatalogs(ASTPrt prtInstance) {
		if (classInitialized) return;	// we've already initialized, so just return

		clearCatalogAndSpaceObjects();
		classInitialized = true;

		if (prtInstance == null) ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ASTMisc.ABORT_PROG);

		prt = prtInstance;
	}

	/**
	 * Loads a catalog from disk. The catalog file must have
	 * all the fields even If they have no value for them and
	 * the catalog must be in the format designed for this book.
	 * 
	 * @param filename			The full filename (including path) to load.
	 * @return					true if successful, else false
	 */
	public static boolean loadFormattedStarCatalog(String filename) {
		String[] parts;
		String Name, ConstAbbrvName, AltName, Comment;
		boolean skipLine, mVGiven;
		double RA, Decl, mV;
		int ConstIdx;
		ASTReal rTmp = new ASTReal();
		boolean errorOccurred = false;

		clearCatalogAndSpaceObjects();

		// Note that we're using try-with-resources so that we don't have to explicitly close the stream when we're done
		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String strIn,str2;

			strIn=ASTFileIO.readTillStr(br,"<Catalog>",ASTMisc.HIDE_ERRORS);
			if (strIn == null) {
				ASTMsg.errMsg("The data file specified is not a Star Catalog\ndata file. Try again ...", "Invalid Star Catalog");
				return false;
			}

			// Get the basic catalog header information. Each tag must be
			// present or else it is an error. A check could be made after
			// each invocation of getSimpleTaggedValue, but that would
			// complicate the code and make it messier. So, we set the
			// error flag errorOccurred after each getSimpleTaggedValue
			// call and only check once at the end. A null result from
			// getSimpleTaggedValue means a tag was missing while a "" result 
			// means the tag was there but no value was given for the tag
			cat_type = ASTFileIO.getSimpleTaggedValue(br, "<CatalogType>");
			errorOccurred = (cat_type == null);

			sDescription = ASTFileIO.getSimpleTaggedValue(br,"<Description>");
			errorOccurred = errorOccurred || (sDescription == null);

			dmVLimit = ASTMisc.UNKNOWN_mV;
			strIn=ASTFileIO.getSimpleTaggedValue(br,"<VisualMagnitudeLimit>");
			errorOccurred = errorOccurred || (strIn == null);
			if (errorOccurred) bmVProvided = false;			// tag may have been there w/o a value so take defaults
			else {
				rTmp = ASTReal.isValidReal(strIn,ASTMisc.HIDE_ERRORS);
				bmVProvided = rTmp.isValidRealObj();
				if (bmVProvided) dmVLimit = rTmp.getRealValue();
			}

			Epoch = ASTMisc.DEFAULT_EPOCH;
			strIn=ASTFileIO.getSimpleTaggedValue(br,"<Epoch>");
			errorOccurred = errorOccurred || (strIn == null);
			if (errorOccurred) bEpochKnown = false;
			else {
				rTmp = ASTReal.isValidReal(strIn,ASTMisc.HIDE_ERRORS);
				bEpochKnown = rTmp.isValidRealObj();
				if (bEpochKnown) Epoch = rTmp.getRealValue();
			}

			sAuthor=ASTFileIO.getSimpleTaggedValue(br,"<Author>");
			errorOccurred = errorOccurred || (sAuthor == null);
			sDate=ASTFileIO.getSimpleTaggedValue(br,"<Date>");
			errorOccurred = errorOccurred || (sDate == null);
			sSource=ASTFileIO.getSimpleTaggedValue(br,"<Source>");
			errorOccurred = errorOccurred || (sSource == null);

			// Now get the data about the space objects
			strIn = ASTFileIO.readTillStr(br,"<Data>");
			errorOccurred = errorOccurred || (strIn == null);

			while ((strIn = br.readLine()) != null) {
				if (strIn.length() <= 0) continue;					// ignore blank lines

				str2 = ASTStr.removeWhitespace(strIn.toLowerCase(Locale.US));
				if (str2.indexOf("</data>") >= 0) break;				// found end of data section

				skipLine = false;
				RA = ASTMisc.UNKNOWN_RA_DECL;
				Decl = ASTMisc.UNKNOWN_RA_DECL;

				parts = strIn.split("[\\,]", -1);				// Must use -1 so that we get all fields, including null ones

				// Get the various fields, but skip if RA or Decl is missing
				if (parts.length != 7) continue;

				Name = parts[0].trim();
				ConstAbbrvName = parts[4].trim();
				AltName = parts[5].trim();
				Comment = parts[6].trim();

				// Get RA, Decl, and mV. Skip if RA or Decl is missing
				rTmp = ASTReal.isValidReal(parts[1],ASTMisc.HIDE_ERRORS);
				if (rTmp.isValidRealObj()) RA = rTmp.getRealValue();
				else skipLine = true;

				rTmp = ASTReal.isValidReal(parts[2],ASTMisc.HIDE_ERRORS);
				if (rTmp.isValidRealObj()) Decl = rTmp.getRealValue();
				else skipLine = true;

				rTmp = ASTReal.isValidReal(parts[3],ASTMisc.HIDE_ERRORS);
				if (rTmp.isValidRealObj()) {
					mV = rTmp.getRealValue();
					mVGiven = true;
				} else {
					mVGiven = false;
					mV = ASTMisc.UNKNOWN_mV;
				}

				if (skipLine) continue;

				ConstIdx = ASTConstellation.findConstellationByAbbrvName(ConstAbbrvName);
                if (ConstIdx < 0) continue;			// skip object if constellation not found				
				addSpaceObjToCatalog(Name,AltName,RA,Decl,mVGiven,mV,Comment,ConstIdx);
			}

			br.close();				// don't really have to do this ...

		} catch (IOException e) {
			ASTMsg.criticalErrMsg("The selected catalog could not be loaded");
			e.printStackTrace();
			errorOccurred = true;
		}

		if (errorOccurred) clearCatalogAndSpaceObjects();		// if an error occurred, then reset back to having no catalog
		else {
			nObjects = StarCatalog.size();
			nConstellations = countConstellations();
			catalogLoaded = true;
			saveCurrentSortOrder = ASTMisc.ASCENDING_ORDER;
			saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME;		
		}

		// Note that errorOccurred is the opposite of what we need to return
		return !errorOccurred;
	}

	/**
	 * Sorts the catalog of space objects.
	 * 
	 * @param sortField				What field to sort on.
	 * @param sortOrder				Sort in ascending order if ASCENDING_ORDER,
	 * 								else sort in descending order
	 */
	public static void sortStarCatalog(CatalogSortField sortField,boolean sortOrder) {
		
		if (!isInitAndLoaded() || ((sortOrder == saveCurrentSortOrder) && (sortField == saveCurrentSortField))) return; // nothing to do
		
		saveCurrentSortOrder = sortOrder;
		saveCurrentSortField = sortField;

		switch(sortField) {
		case CONSTELLATION:
			Collections.sort(StarCatalog,constComparator);
			break;
		case CONST_AND_OBJNAME:
			Collections.sort(StarCatalog,constAndObjNameComparator);
			break;				
		case OBJNAME:
			Collections.sort(StarCatalog,spaceObjNameComparator);
			break;
		case OBJ_ALTNAME:
			Collections.sort(StarCatalog,spaceObjAltNameComparator);
			break;
		case RA:
			Collections.sort(StarCatalog,spaceObjRAComparator);
			break;
		case DECL:
			Collections.sort(StarCatalog,spaceObjDeclComparator);
			break;
		case VISUAL_MAGNITUDE:
			Collections.sort(StarCatalog,spaceObjmVComparator);
			break;
		}
	}
	
	/**----------------------------------------------------------------------------------------
	 * Private methods used only in this class.
	 *----------------------------------------------------------------------------------------*/

	/**
	 * Counts the number of unique constellations in the currently
	 * loaded catalog. The count is put into the catalog data structure.
	 * The objects in the catalog **MUST** be grouped by constellation
	 * before this method is invoked. All properly formatted catalogs
	 * are stored on disk sorted by constellation, so they should be OK
	 * when loaded. How the objects are sorted does not matter as long
	 * as they are grouped by constellation.
	 * 
	 * Note that the StarCatalog database has an index that points
	 * into the constellations database, so we can compare the indices
	 * without needing to actually find out the constellation name
	 * and do a string compare.
	 * 
	 * @return			a count of the number of unique constellations in the
	 * 					currently loaded star catalog.
	 */
	private static int countConstellations() {
		int count = 0;
		int idx = -1;

		for (int i = 0; i < StarCatalog.size(); i++) {
			if (getCatObjConstIdx(i) != idx) {
				count += 1;
				idx = getCatObjConstIdx(i);
			}
		}

		nConstellations = count;
		return count;
	}

	/**
	 * Displays catalog information for one specific object from the catalog.
	 * 
	 * @param i			Since this method is intended to be invoked from inside a loop,
	 * 					the parameter i is a counter from the calling method's loop.
	 * @param idx		index into the Star Catalog for the object to display
	 */
	private static void displayObject(int i,int idx) {
		String sTmp;
		String sConstAbbrvName = " ";
		int iConstName;
		String strRA, strDecl;

		if (!isInitAndLoaded()) return;
		if ((idx < 0) || (idx >= getCatNumObjs())) return;

		strRA = ASTTime.timeToStr(getCatObjRA(idx),ASTMisc.HMSFORMAT);
		strDecl = ASTAngle.angleToStr(getCatObjDecl(idx),ASTMisc.DMSFORMAT);

		if (getCatObjmVKnown(idx)) sTmp = String.format("%4.1f",getCatObjmV(idx));
		else sTmp = "  ? ";

		iConstName = getCatObjConstIdx(idx);
		if ((iConstName >= 0) && (iConstName < ASTConstellation.getNumConstellations())) sConstAbbrvName = ASTConstellation.getConstAbbrevName(iConstName);
		else sConstAbbrvName = "ERR";

		// This format string must match the one in displayObjHeader
		prt.println(String.format("%5d %-18s %12s %16s %4s   %-12s %-5s",i,getCatObjName(idx),strRA,strDecl,sTmp,
				getCatObjAltName(idx),sConstAbbrvName));
	}

	/**
	 * Displays a header for the objects that will be displayed after the header.
	 * This method must be kept in synch with displayObject so that the header
	 * and the columns that are printed are consistent.
	 */
	private static void displayObjHeader() {
		if (!isInitAndLoaded()) return;
		prt.println(String.format("      %-18s %-12s %-16s %4s   %-12s %5s","Obj Name","      RA",
				"        Decl","mV ","Alt Name","Const"));
	}

	/**
	 * Checks to see if this class has been properly initialized
	 * and a catalog has been loaded.
	 */
	private static boolean isInitAndLoaded() {
		if (!classInitialized) ASTMsg.criticalErrMsg("Catalogs have not been initialized ... Catalog data\n" +
				"cannot be displayed in the scrollable text area. Aborting program ...", ASTMisc.ABORT_PROG);
		return isCatalogLoaded();
	}
	
	/**----------------------------------------------------------------------------
	 * Comparators for sorting the objects in a catalog
	 *----------------------------------------------------------------------------*/
	
	/**
	 * Defines the 'natural' ordering for a catalog.
	 * <p>
	 * This method doesn't do anything because it doesn't make sense to sort the catalog, 
	 * only the objects within the catalog. This method should really never be called by 
	 * any of the code, but it is required because we are implementing a Comparator interface.
	 */
	@Override
	public int compareTo(ASTCatalog obj) {
		return 0;
	}
	
	/**
	 * Comparator for sorting catalog objects by the Constellation
	 * they are in, and then by the object's name
	 */
	public static Comparator<SpaceObj> constAndObjNameComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			String s1,s2;
			
			s1 = ASTConstellation.getConstName(obj1.idx);
			s2 = ASTConstellation.getConstName(obj2.idx);
			
			int returnValue = s1.compareToIgnoreCase(s2);

			// If returnValue is 0, then the two objects are in the same constellation. So sort them
			// by their object name. We need to do a special alphanumeric comparison so that, for example,
			// A9 sorts before A11
			if (returnValue == 0) returnValue = ASTStr.compareAlphaNumeric(obj1.sName, obj2.sName);

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}
	};
	
	/**
	 * Comparator for sorting catalog objects by the constellation they are in.
	 */
	public static Comparator<SpaceObj> constComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			int returnValue;
			String s1,s2;

			s1 = ASTConstellation.getConstName(obj1.idx);
			s2 = ASTConstellation.getConstName(obj2.idx);

			returnValue = s1.compareToIgnoreCase(s2);

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}
	};

	/**
	 * Comparator for sorting catalog objects by an object's alternate name
	 */
	public static Comparator<SpaceObj> spaceObjAltNameComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			int returnValue;

			// We need to do a special alphanumeric comparison so that, for example,
			// A9 sorts before A11
			returnValue = ASTStr.compareAlphaNumeric(obj1.sAltName, obj2.sAltName);

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}
	};
	
	/**
	 * Comparator for sorting catalog objects by Decl
	 */
	public static Comparator<SpaceObj> spaceObjDeclComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			int returnValue;

			if (obj1.dDecl > obj2.dDecl) returnValue = 1;
			else if (obj1.dDecl < obj2.dDecl) returnValue = -1;
			else returnValue = 0;

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}		
	};

	/**
	 * Comparator for sorting catalog objects by visual magnitude
	 */
	public static Comparator<SpaceObj> spaceObjmVComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			int returnValue;

			if (obj1.dmV > obj2.dmV) returnValue = 1;
			else if (obj1.dmV < obj2.dmV) returnValue = -1;
			else returnValue = 0;

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}
	};

	/**
	 * Comparator for sorting catalog objects by an object's name
	 */
	public static Comparator<SpaceObj> spaceObjNameComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			int returnValue;

			returnValue = ASTStr.compareAlphaNumeric(obj1.sName, obj2.sName);

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}
	};
	
	/**
	 * Comparator for sorting catalog objects by RA
	 */
	public static Comparator<SpaceObj> spaceObjRAComparator = new Comparator<SpaceObj>() {
		@Override
		public int compare(SpaceObj obj1, SpaceObj obj2) {
			int returnValue;

			if (obj1.dRA > obj2.dRA) returnValue = 1;
			else if (obj1.dRA < obj2.dRA) returnValue = -1;
			else returnValue= 0;

			if (saveCurrentSortOrder == ASTMisc.ASCENDING_ORDER) return returnValue;
			else return -1*returnValue;
		}
	};
}
