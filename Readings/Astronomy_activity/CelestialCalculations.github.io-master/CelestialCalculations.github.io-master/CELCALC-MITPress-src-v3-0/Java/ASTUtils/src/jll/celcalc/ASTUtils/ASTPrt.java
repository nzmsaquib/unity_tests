package jll.celcalc.ASTUtils;

import java.awt.Color;

import javax.swing.JTextPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 * <b>Output text methods</b>
 * <p>
 * This class implements methods that print to a scrollable text
 * area that the invoking routine sets up. The methods are collected
 * into a class to standardize behavior across the programs.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

public class ASTPrt {

	private static StyleContext saveDefaultOutTxtBoxSC;						// save the style context
	private static AttributeSet saveDefaultOutBoxAttribSet;					// save the default output text area attribute set
	private static SimpleAttributeSet attribs = new SimpleAttributeSet(); 	// attributes to use when writing the text

	// Whether to replace attributes for the entire document (true) or just from this point forward (false)
	private static final boolean REPLACE_ATTRIBS = true;

	/**
	 * Boolean indicating that text is to be centered in the print and println methods.
	 */
	public static final boolean CENTERTXT = true;

	/*
	 * Save a reference to the invoking routine's scrollable text area so that 
	 * we don't have to constantly get it and so that the calling routines
	 * don't have to pass in a reference to the scrollable text are. This
	 * is saved when the calling routine creates a class instance.
	 */
	private JTextPane outputTxtBox = null;

	/**
	 * Constructor saves a reference to the caller's output text area.
	 * 
	 * @param outTextPane	JTextPane object created by calling routine for text output
	 */
	public ASTPrt(JTextPane outTextPane) {
		// Since we must create an object instance and we must set outputTxtBox, there's no need
		// to check outputTxtBox for null in the rest of this class
		if (outTextPane == null) {
			ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...");
			System.exit(-1);
		}
		outputTxtBox = outTextPane;
		saveDefaultOutTxtBoxSC = StyleContext.getDefaultStyleContext();
		saveDefaultOutBoxAttribSet = saveDefaultOutTxtBoxSC.addAttribute(SimpleAttributeSet.EMPTY,StyleConstants.Foreground, Color.black);
		clearTextArea();
		resetCursor();
	}

	/*=================================================================================
	 * Define various methods to write text to the caller's scrollable text box area
	 *================================================================================*/

	/**
	 * Clears the caller's text output area
	 */
	public void clearTextArea() {
		outputTxtBox.setText("");
		resetCursor();
		outputTxtBox.setCharacterAttributes(saveDefaultOutBoxAttribSet,REPLACE_ATTRIBS);
		outputTxtBox.setParagraphAttributes(saveDefaultOutBoxAttribSet,REPLACE_ATTRIBS);
		StyleConstants.setAlignment(attribs,StyleConstants.ALIGN_LEFT);
		outputTxtBox.setParagraphAttributes(attribs,REPLACE_ATTRIBS);
		setProportionalFont();
		setBoldFont(false);
		setItalicFont(false);
	}

	/**
	 * Resets the cursor to the beginning of the scrollable text area.
	 * <p>
	 * This method is intended to be used at the end of a lengthy series
	 * of prints so that the scrollable list presented to the user will be
	 * at the beginning of the list, not the end.
	 */
	public void resetCursor() {
		outputTxtBox.setCaretPosition(0);
	}

	/**
	 * Sets whether the output font is to be bold or plain.
	 * 
	 * @param boldFont		if true, set output font to be bold.
	 */
	public void setBoldFont(boolean boldFont) {
		setCursorAtEnd();

		StyleConstants.setBold(attribs,boldFont);
		outputTxtBox.setCharacterAttributes(attribs,!REPLACE_ATTRIBS);
	}

	/**
	 * Sets the output font to be a fixed width font.
	 */
	public void setFixedWidthFont() {
		// Position the cursor to the end of the text so that effect is only from that point forward.
		setCursorAtEnd();

		StyleConstants.setFontFamily(attribs,ASTStyle.FW_OUT_TEXT_FONT.getFamily());
		StyleConstants.setFontSize(attribs,ASTStyle.FW_OUT_TEXT_FONT.getSize());
		outputTxtBox.setCharacterAttributes(attribs,!REPLACE_ATTRIBS);
	}

	/**
	 * Sets whether the output font is to be italicized.
	 * 
	 * @param italicFont	if true, set output font to be italicized.
	 */
	public void setItalicFont(boolean italicFont) {
		setCursorAtEnd();

		StyleConstants.setItalic(attribs,italicFont);
		outputTxtBox.setCharacterAttributes(attribs,!REPLACE_ATTRIBS);
	}

	/**
	 * Outputs text to the caller's scrollable output area and optionally centers it.
	 * 
	 * This method is overloaded.
	 * 
	 * @param txt			text to output
	 * @param centerText	center the text if this flag is <i><code>CENTERTXT</code></i>
	 */
	public void print(String txt,boolean centerText) {
		int len = outputTxtBox.getText().length();
		setCursorAtEnd();

		if (centerText) { 				// Set attributes to center the text and apply it
			StyleConstants.setAlignment(attribs,StyleConstants.ALIGN_CENTER);
			outputTxtBox.setParagraphAttributes(attribs,!REPLACE_ATTRIBS);
		} else {						// Left justify text
			StyleConstants.setAlignment(attribs,StyleConstants.ALIGN_LEFT);
			outputTxtBox.setParagraphAttributes(attribs,!REPLACE_ATTRIBS);
		}		

		try {
			outputTxtBox.getDocument().insertString(len,txt,attribs);	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * Outputs text to the caller's scrollable output area and optionally centers it.
	 * 
	 * This method is overloaded.
	 * 
	 * @param txt			text to output
	 */
	public void print(String txt) {
		print(txt,!CENTERTXT);	
	}

	/**
	 * Outputs text and a newline to the caller's scrollable output area,
	 * and optionally centers it
	 * 
	 * This method is overloaded.
	 * 
	 * @param txt			text to output
	 * @param centerText	center the text if this flag is <i><code>CENTERTXT</code></i>
	 */
	public  void println(String txt,boolean centerText) {
		print(txt+"\n",centerText);
	}
	/**
	 * Outputs text and a newline to the caller's scrollable output area,
	 * and optionally centers it
	 * 
	 * This method is overloaded.
	 * 
	 * @param txt			text to output
	 */
	public void println(String txt) {
		println(txt,!CENTERTXT);
	}
	/**
	 * Outputs text and a newline to the caller's scrollable output area,
	 * and optionally centers it
	 * 
	 * This method is overloaded.
	 */
	public void println() {
		println("",!CENTERTXT);
	}

	/**
	 * Sets the output font to be a proportional font.
	 */
	public void setProportionalFont() {
		setCursorAtEnd();

		StyleConstants.setFontFamily(attribs,ASTStyle.OUT_TEXT_FONT.getFamily());
		StyleConstants.setFontSize(attribs,ASTStyle.OUT_TEXT_FONT.getSize());
		outputTxtBox.setCharacterAttributes(attribs,!REPLACE_ATTRIBS);
	}

	/*-------------------------------------------------------
	 * Private methods used only in this class
	 *------------------------------------------------------*/

	/**
	 * Position the cursor at the end of the document
	 */
	private void setCursorAtEnd() {
		int len = outputTxtBox.getText().length();
		outputTxtBox.setCaretPosition(len);
	}
}
