package jll.celcalc.chap1;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import jll.celcalc.ASTUtils.ASTAboutBox;
import jll.celcalc.ASTUtils.ASTBookInfo;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;

/**
 * <b>Implements the main GUI</b>
 * <p>
 * The GUI was created with, and is maintained by, the Eclipse WindowBuilder.
 * All methods and most data are declared static because it only makes sense to have
 * one main GUI per application, and making them static avoids the problem of
 * having to pass a reference to the GUI instance in other classes that need
 * to reference the main GUI.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
class ChapGUI extends JFrame {
	private static final String WINDOW_TITLE = "Chapter 1 - Introduction";

	// Save the scrollable text pane area for output
	private static JTextPane textPane = new JTextPane();
	private static ASTPrt prt = null;				// create instance (actually done below) for printing to the output textPane

	// Create an About Box
	private static ASTAboutBox aboutBox = new ASTAboutBox(WINDOW_TITLE);
	
	// Maximum length of a filename to display. Actual filename can be longer, but only a limited
	// number of characters can be displayed in the GUI.
	private static final int MAX_FNAME_DISPLAY = 100;

	// Create listeners for the various GUI components
	private static ActionListener listenerMenus = new MenusListener();

	// Info about a star catalog
	private static JLabel lblEpoch = new JLabel("Epoch: ");
	private static JLabel lblFilename = new JLabel("Filename: ");
	private static JLabel lblCatalogType=new JLabel("Catalog Type: ");

	// Checkbox for setting the sorting order
	private static JCheckBox chkboxSortOrder = new JCheckBox("Sort in Ascending Order");

	/*===========================================================================================
	 * The string text collected in these next items is done to separate the strings displayed
	 * in the various GUI components (such as buttons and menus) from the semantics of what 
	 * clicking on a component means. This is important because otherwise, a change to the text
	 * displayed in the GUI, such as for a button label, via Eclipse WindowBuilder may well 
	 * break other code, particularly the action listeners. Through this technique, the 
	 * WindowBuilder can be used to change the label on a GUI element (e.g., button) without
	 * breaking any other code. The command that a listener will receive is the same regardless
	 * of what the actual label on the GUI component says. So, for example, the text "Exit"
	 * be changed in the menu to "Quit" without breaking the menu's listener because the
	 * listener will receive the command exitCommand regardless of what text is actually
	 * displayed in the menu. Get methods below (e.g., getExitCommand) return the appropriate
	 * command for the listeners to use.
	 *==========================================================================================*/	
	/* define menu item commands */
	private static final String exitCommand = "exit";
	private static final String instructionsCommand = "instruct";
	private static final String aboutCommand = "about";

	// Constellations menu
	private static final String listConstByNameCommand = "list_const_by_name";
	private static final String listConstByAbbrevCommand = "list_const_by_abbrev_name";
	private static final String listConstByMeaningCommand = "list_const_by_meaning";
	private static final String listAllConstCommand = "list_all_const";
	private static final String findConstCommand = "find_const";

	// Star Catalogs menu
	private static final String clearCatalogCommand = "clear_catalog";
	private static final String loadCatalogCommand = "load_catalog";
	private static final String showCatalogInfoCommand = "show_catalog";

	// Space Objects menu
	private static final String listObjByNameCommand = "list_obj_by_name";
	private static final String listObjByAltNameCommand = "list_obj_by_alt_name";
	private static final String listObjByCommentsCommand = "list_obj_by_comments";
	private static final String listAllObjsInCatCommand="list_all_objs_in_cat";
	private static final String listAllObjsInRangeCommand = "list_all_objs_in_range";
	private static final String listAllObjsInConstCommand = "list_all_objs_in_const";
	private static final String sortCatByConstellationCommand = "sort_cat_by_const";
	private static final String sortCatByConstAndObjNameCommand = "sort_cat_by_const_and_name";
	private static final String sortCatByObjNameCommand = "sort_cat_by_name";
	private static final String sortCatByObjAltNameCommand = "sort_cat_by_alt_name";
	private static final String sortCatByObjRACommand = "sort_cat_by_RA";
	private static final String sortCatByObjDeclCommand = "sort_cat_by_Decl";
	private static final String sortCatByObjmVCommand = "sort_cat_by_mV";

	/**
	 * Create the GUI frame.
	 */
	protected ChapGUI() {
		JPanel contentPane = new JPanel();
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChapGUI.class.getResource("/resources/BlueMarble.png")));
		setTitle(WINDOW_TITLE);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 980, 740);
		contentPane.setDoubleBuffered(false);
		contentPane.setFocusable(false);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		/*========================== Create Menus =============================*/
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(ASTStyle.MENU_FONT);
		setJMenuBar(menuBar);

		// File menu
		JMenu mnFile = new JMenu(" File ");
		mnFile.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setFont(ASTStyle.MENU_FONT);
		mntmExit.setActionCommand(exitCommand);
		mntmExit.addActionListener(listenerMenus);
		mnFile.add(mntmExit);

		// Constellations menu
		JMenu mnConstellations = new JMenu(" Constellations ");
		mnConstellations.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnConstellations);

		JMenu mnListConstBy = new JMenu("List a Constellation by ...");
		mnListConstBy.setFont(ASTStyle.MENU_FONT);
		mnConstellations.add(mnListConstBy);
		JMenuItem mntmByName = new JMenuItem("Name");
		mntmByName.setFont(ASTStyle.MENU_FONT);
		mntmByName.setActionCommand(listConstByNameCommand);
		mntmByName.addActionListener(listenerMenus);
		mnListConstBy.add(mntmByName);

		JMenuItem mntmByAbbrvName = new JMenuItem("Abbreviated Name");
		mntmByAbbrvName.setFont(ASTStyle.MENU_FONT);
		mntmByAbbrvName.setActionCommand(listConstByAbbrevCommand);
		mntmByAbbrvName.addActionListener(listenerMenus);
		mnListConstBy.add(mntmByAbbrvName);

		JMenuItem mntmByMeaning = new JMenuItem("Meaning");
		mntmByMeaning.setFont(ASTStyle.MENU_FONT);
		mntmByMeaning.setActionCommand(listConstByMeaningCommand);
		mntmByMeaning.addActionListener(listenerMenus);
		mnListConstBy.add(mntmByMeaning);

		mnConstellations.add(new JSeparator());

		JMenuItem mntmListAllConstellations = new JMenuItem("List All Constellations");
		mntmListAllConstellations.setFont(ASTStyle.MENU_FONT);
		mntmListAllConstellations.setActionCommand(listAllConstCommand);
		mntmListAllConstellations.addActionListener(listenerMenus);
		mnConstellations.add(mntmListAllConstellations);

		mnConstellations.add(new JSeparator());

		JMenuItem mntmFindConstellation = new JMenuItem("Find Constellation an Object is In");
		mntmFindConstellation.setFont(ASTStyle.MENU_FONT);
		mntmFindConstellation.setActionCommand(findConstCommand);
		mntmFindConstellation.addActionListener(listenerMenus);
		mnConstellations.add(mntmFindConstellation);

		// Star Catalogs menu
		JMenu mnASTCatalogs = new JMenu(" Star Catalogs ");
		mnASTCatalogs.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnASTCatalogs);

		JMenuItem mntmClearCatalogData = new JMenuItem("Clear Catalog");
		mntmClearCatalogData.setFont(ASTStyle.MENU_FONT);
		mntmClearCatalogData.setActionCommand(clearCatalogCommand);
		mntmClearCatalogData.addActionListener(listenerMenus);
		mnASTCatalogs.add(mntmClearCatalogData);

		mnASTCatalogs.add(new JSeparator());

		JMenuItem mntmLoadCatalog = new JMenuItem("Load a Star Catalog");
		mntmLoadCatalog.setFont(ASTStyle.MENU_FONT);
		mntmLoadCatalog.setActionCommand(loadCatalogCommand);
		mntmLoadCatalog.addActionListener(listenerMenus);
		mnASTCatalogs.add(mntmLoadCatalog);

		mnASTCatalogs.add(new JSeparator());

		JMenuItem mntmShowBasicCatalogInfo = new JMenuItem("Show Catalog Information");
		mntmShowBasicCatalogInfo.setFont(ASTStyle.MENU_FONT);
		mntmShowBasicCatalogInfo.setActionCommand(showCatalogInfoCommand);
		mntmShowBasicCatalogInfo.addActionListener(listenerMenus);
		mnASTCatalogs.add(mntmShowBasicCatalogInfo);

		// Space Objects menu
		JMenu mnSpaceObjects = new JMenu(" Space Objects ");
		mnSpaceObjects.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnSpaceObjects);

		JMenu mnListObjBy = new JMenu("List a Catalog Object by ...");
		mnListObjBy.setFont(ASTStyle.MENU_FONT);
		mnSpaceObjects.add(mnListObjBy);

		JMenuItem mntmListObjByName = new JMenuItem("Name");
		mntmListObjByName.setFont(ASTStyle.MENU_FONT);
		mntmListObjByName.setActionCommand(listObjByNameCommand);
		mntmListObjByName.addActionListener(listenerMenus);			
		mnListObjBy.add(mntmListObjByName);

		JMenuItem mntmListObjByAltName = new JMenuItem("Alternate Name");
		mntmListObjByAltName.setFont(ASTStyle.MENU_FONT);
		mntmListObjByAltName.setActionCommand(listObjByAltNameCommand);
		mntmListObjByAltName.addActionListener(listenerMenus);			
		mnListObjBy.add(mntmListObjByAltName);

		JMenuItem mntmListObjByComments = new JMenuItem("Catalog Comments");
		mntmListObjByComments.setFont(ASTStyle.MENU_FONT);
		mntmListObjByComments.setActionCommand(listObjByCommentsCommand);
		mntmListObjByComments.addActionListener(listenerMenus);			
		mnListObjBy.add(mntmListObjByComments);

		mnSpaceObjects.add(new JSeparator());

		JMenu mnListAllCatObjsBy = new JMenu("List All Space Objects ...");
		mnListAllCatObjsBy.setFont(ASTStyle.MENU_FONT);
		mnSpaceObjects.add(mnListAllCatObjsBy);

		JMenuItem mntmListAllObjsInCatalog = new JMenuItem("In the Entire Catalog");
		mntmListAllObjsInCatalog.setFont(ASTStyle.MENU_FONT);
		mntmListAllObjsInCatalog.setActionCommand(listAllObjsInCatCommand);
		mntmListAllObjsInCatalog.addActionListener(listenerMenus);	
		mnListAllCatObjsBy.add(mntmListAllObjsInCatalog);

		JMenuItem mntmListAllObjsInRange = new JMenuItem("In a Range");
		mntmListAllObjsInRange.setFont(ASTStyle.MENU_FONT);
		mntmListAllObjsInRange.setActionCommand(listAllObjsInRangeCommand);
		mntmListAllObjsInRange.addActionListener(listenerMenus);	
		mnListAllCatObjsBy.add(mntmListAllObjsInRange);

		JMenuItem mntmListAllObjsInConst = new JMenuItem("In a Constellation");
		mntmListAllObjsInConst.setFont(ASTStyle.MENU_FONT);
		mntmListAllObjsInConst.setActionCommand(listAllObjsInConstCommand);
		mntmListAllObjsInConst.addActionListener(listenerMenus);	
		mnListAllCatObjsBy.add(mntmListAllObjsInConst);

		mnSpaceObjects.add(new JSeparator());

		JMenu mnSortCatalog = new JMenu("Sort Catalog by ...");
		mnSortCatalog.setFont(ASTStyle.MENU_FONT);
		mnSpaceObjects.add(mnSortCatalog);

		JMenuItem mntmSortCatByConst = new JMenuItem("Constellation");
		mntmSortCatByConst.setFont(ASTStyle.MENU_FONT);
		mntmSortCatByConst.setActionCommand(sortCatByConstellationCommand);
		mntmSortCatByConst.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatByConst);

		JMenuItem mntmSortCatByConstAndObjName = new JMenuItem("Constellation and Obj Name");
		mntmSortCatByConstAndObjName.setFont(ASTStyle.MENU_FONT);
		mntmSortCatByConstAndObjName.setActionCommand(sortCatByConstAndObjNameCommand);
		mntmSortCatByConstAndObjName.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatByConstAndObjName);

		JMenuItem mntmSortCatByName = new JMenuItem("Object's Name");
		mntmSortCatByName.setFont(ASTStyle.MENU_FONT);
		mntmSortCatByName.setActionCommand(sortCatByObjNameCommand);
		mntmSortCatByName.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatByName);

		JMenuItem mntmSortCatByAltName = new JMenuItem("Object's Alternate Name");
		mntmSortCatByAltName.setFont(ASTStyle.MENU_FONT);
		mntmSortCatByAltName.setActionCommand(sortCatByObjAltNameCommand);
		mntmSortCatByAltName.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatByAltName);

		JMenuItem mntmSortCatByRA = new JMenuItem("Object's Right Ascension");
		mntmSortCatByRA.setFont(ASTStyle.MENU_FONT);
		mntmSortCatByRA.setActionCommand(sortCatByObjRACommand);
		mntmSortCatByRA.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatByRA);

		JMenuItem mntmSortCatByDecl = new JMenuItem("Object's Declination");
		mntmSortCatByDecl.setFont(ASTStyle.MENU_FONT);
		mntmSortCatByDecl.setActionCommand(sortCatByObjDeclCommand);
		mntmSortCatByDecl.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatByDecl);

		JMenuItem mntmSortCatBymV = new JMenuItem("Object's Visual Magnitude");
		mntmSortCatBymV.setFont(ASTStyle.MENU_FONT);
		mntmSortCatBymV.setActionCommand(sortCatByObjmVCommand);
		mntmSortCatBymV.addActionListener(listenerMenus);			
		mnSortCatalog.add(mntmSortCatBymV);

		// Help menu
		JMenu mnHelp = new JMenu(" Help ");
		mnHelp.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnHelp);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setFont(ASTStyle.MENU_FONT);
		mntmInstructions.setActionCommand(instructionsCommand);
		mntmInstructions.addActionListener(listenerMenus);
		mnHelp.add(mntmInstructions);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setFont(ASTStyle.MENU_FONT);
		mntmAbout.addActionListener(listenerMenus);
		mntmAbout.setActionCommand(aboutCommand);
		mnHelp.add(mntmAbout);
		/*============================== End of Menus ================================*/		

		setContentPane(contentPane);

		JPanel panelBookTitle = new JPanel();
		panelBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		panelBookTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelBookTitle.setFocusable(false);
		panelBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.setDoubleBuffered(false);

		JScrollPane scrollPaneOutput = new JScrollPane();
		scrollPaneOutput.setFocusTraversalKeysEnabled(false);
		scrollPaneOutput.setVerifyInputWhenFocusTarget(false);
		scrollPaneOutput.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setRequestFocusEnabled(false);
		scrollPaneOutput.setFocusable(false);

		/*============= Add a checkbox for determining the sort order ============*/
		chkboxSortOrder.setFocusPainted(false);
		chkboxSortOrder.setFont(ASTStyle.CBOX_FONT);
		chkboxSortOrder.setHorizontalAlignment(SwingConstants.CENTER);
		chkboxSortOrder.setSelected(true);

		/*==================== Set up Catalog Type, Epoch, and Filename labels =====*/
		lblCatalogType.setHorizontalAlignment(SwingConstants.LEFT);
		lblCatalogType.setFont(ASTStyle.TEXT_BOLDFONT);
		lblCatalogType.setAlignmentY(Component.TOP_ALIGNMENT);
		lblFilename.setHorizontalTextPosition(SwingConstants.LEFT);
		lblFilename.setHorizontalAlignment(SwingConstants.LEFT);

		lblFilename.setFont(ASTStyle.TEXT_BOLDFONT);		
		lblFilename.setAlignmentY(Component.TOP_ALIGNMENT);
		lblFilename.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblEpoch.setHorizontalTextPosition(SwingConstants.LEFT);
		lblEpoch.setHorizontalAlignment(SwingConstants.LEFT);

		lblEpoch.setAlignmentY(Component.TOP_ALIGNMENT);
		lblEpoch.setFont(ASTStyle.TEXT_BOLDFONT);
		lblEpoch.setAlignmentX(Component.RIGHT_ALIGNMENT);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBookTitle, GroupLayout.DEFAULT_SIZE, 982, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 958, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(292)
					.addComponent(chkboxSortOrder, GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
					.addGap(291))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblFilename, GroupLayout.DEFAULT_SIZE, 958, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblCatalogType, GroupLayout.DEFAULT_SIZE, 679, Short.MAX_VALUE)
							.addGap(162)
							.addComponent(lblEpoch, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panelBookTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(chkboxSortOrder, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCatalogType)
						.addComponent(lblEpoch))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFilename)
					.addGap(24)
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
					.addContainerGap())
		);

		panelBookTitle.setLayout(new BorderLayout(0, 0));		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setVerifyInputWhenFocusTarget(false);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookTitle.setFont(ASTStyle.BOOK_TITLE_BOLDFONT);
		lblBookTitle.setFocusable(false);
		lblBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.add(lblBookTitle, BorderLayout.CENTER);
		contentPane.setLayout(gl_contentPane);
		textPane.setRequestFocusEnabled(false);
		textPane.setVerifyInputWhenFocusTarget(false);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		// Define the actual text area and add it to scrollPaneOutput
		textPane.setFont(ASTStyle.OUT_TEXT_FONT);
		textPane.setFocusable(false);
		textPane.setFocusTraversalKeysEnabled(false);
		textPane.setFocusCycleRoot(false);
		textPane.setEditable(false);
		scrollPaneOutput.setViewportView(textPane);

		// Initialize the rest of the GUI
		ASTStyle.setASTStyle();
		aboutBox.pack();
		aboutBox.setParentFrame(this);	
		
		// Set parent frame for static methods that need to center a pane/frame/etc. on the GUI
		ASTQuery.setParentFrame(this);
		ASTMsg.setParentFrame(this);
		
		setEpoch(ASTMisc.DEFAULT_EPOCH);
		setCatalogType("");
		setFilename("");		
		chkboxSortOrder.setSelected(true);
		textPane.setText("     "); // must initialize with at least a few blanks
		prt = new ASTPrt(textPane);
	}

	/*============================================================================
	 * The methods below return references to various GUI components such as the
	 * scrollable output text area. 
	 *===========================================================================*/

	/**
	 * Gets the ASTPrt instance for this application's scrollable text pane area.
	 * 
	 * @return 			the ASTPrt instance for this application
	 */
	protected static ASTPrt getPrtInstance() {
		return prt;
	}

	/**
	 * Gets the current checkbox setting for the sort order.
	 * 
	 * @return boolean		ASCENDING_ORDER or DESCENDING_ORDER.
	 */
	protected static boolean getSortOrderChkbox() {
		if (chkboxSortOrder.isSelected()) return ASTMisc.ASCENDING_ORDER;
		else return ASTMisc.DESCENDING_ORDER;
	}

	/**
	 * Sets the Catalog Type label in the GUI.
	 * 
	 * @param cat_type		the Catalog Type to be displayed in the GUI
	 */
	protected static void setCatalogType(String cat_type) {
		lblCatalogType.setText("Catalog Type: " + cat_type);
	}

	/**
	 * Sets the Epoch label in the GUI.
	 * 
	 * @param epoch		the epoch to be displayed in the GUI
	 */
	protected static void setEpoch(double epoch) {
		lblEpoch.setText("Epoch: " + String.format(ASTStyle.epochFormat,epoch));
	}

	/**
	 * Sets the filename label to be displayed in the GUI.
	 * 
	 * @param fname		the filename (with pathname) to be displayed in the GUI
	 */
	protected static void setFilename(String fname) {
		lblFilename.setText("File: " + ASTStr.abbrevRight(fname,MAX_FNAME_DISPLAY));
	}

	/*==============================================================================
	 * The methods below return the command associated with a specific menu or
	 * GUI button. This is done to separate the actual string in the GUI that
	 * represents an action (e.g., "Exit") from the semantics of the action that is
	 * to occur.
	 *=============================================================================*/

	// File menu ******************************************************************
	/**
	 * Gets the command that represents the Exit menu item.
	 * 
	 * @return			Exit command
	 */
	protected static String getExitCommand() {
		return exitCommand;
	}

	// Constellations menu *********************************************************
	/**
	 * Gets the command that represents the List Constellation by Name menu item.
	 * 
	 * @return			List Constellation by Name command
	 */
	protected static String getListConstByNameCommand() {
		return listConstByNameCommand;
	}

	/**
	 * Gets the command that represents the List Constellation by Abbreviated Name menu item.
	 * 
	 * @return			List Constellation by Abbreviated Name command
	 */
	protected static String getListConstByAbbrevNameCommand() {
		return listConstByAbbrevCommand;
	}

	/**
	 * Gets the command that represents the List List Constellation by Meaning menu item.
	 * 
	 * @return			List Constellation by Meaning command
	 */
	protected static String getListConstByMeaningCommand() {
		return listConstByMeaningCommand;
	}

	/**
	 * Gets the command that represents the List All Constellations menu item.
	 * 
	 * @return			List All Constellations command
	 */
	protected static String getListAllConstellationsCommand() {
		return listAllConstCommand;
	}

	/**
	 * Gets the command that represents the Find Constellation menu item.
	 * 
	 * @return			Find Constellation command
	 */
	protected static String getFindConstellationCommand() {
		return findConstCommand;
	}	

	// Star Catalogs menu ***************************************************
	/**
	 * Gets the command that represents the clear catalog menu item.
	 * 
	 * @return			Clear Catalog command
	 */
	protected static String getClearCatalogCommand() {
		return clearCatalogCommand;
	}

	/**
	 * Gets the command that represents the load catalog menu item.
	 * 
	 * @return			Load Catalog command
	 */
	protected static String getLoadCatalogCommand() {
		return loadCatalogCommand;
	}

	/**
	 * Gets the command that represents the show catalog info menu item.
	 * 
	 * @return			Show Catalog Info command
	 */
	protected static String getShowCatalogInfoCommand() {
		return showCatalogInfoCommand;
	}

	// Space Objects menu ****************************************************
	/**
	 * Gets the command that represents the List Object by Name menu item.
	 * 
	 * @return			List Object by Name command
	 */
	protected static String getListObjByNameCommand() {
		return listObjByNameCommand;
	}	

	/**
	 * Gets the command that represents the List Object by Alternate Name menu item.
	 * 
	 * @return			List Object by Alternate Name command
	 */
	protected static String getListObjByAltNameCommand() {
		return listObjByAltNameCommand;
	}

	/**
	 * Gets the command that represents the List Object by Comments menu item.
	 * 
	 * @return			List Object by Comments command
	 */
	protected static String getListObjByCommentsCommand() {
		return listObjByCommentsCommand;
	}

	/**
	 * Gets the command that represents the List the Entire Catalog menu item.
	 * 
	 * @return			List Entire Catalog command
	 */
	protected static String getListAllObjsInCatalogCommand() {
		return listAllObjsInCatCommand;
	}

	/**
	 * Gets the command that represents the List a Range of Catalog Objects menu item.
	 * 
	 * @return			List a Range of Catalog Objects command
	 */
	protected static String getListAllObjsInRangeCommand() {
		return listAllObjsInRangeCommand;
	}

	/**
	 * Gets the command that represents the List all Catalog Objects in a Constellation menu item.
	 * 
	 * @return			List all Catalog Objects in a Constellation command
	 */
	protected static String getListAllObjsInConstCommand() {
		return listAllObjsInConstCommand;
	}

	/**
	 * Gets the command that represents the Sort Catalog by Constellation menu item.
	 * 
	 * @return			Sort Catalog by Constellation command
	 */
	protected static String getSortCatByConstellationCommand() {
		return sortCatByConstellationCommand;
	}

	/**
	 * Gets the command that represents the Sort Catalog by Constellation and Object Name menu item.
	 * 
	 * @return			Sort Catalog by Constellation and Object Name command
	 */
	protected static String getSortCatByConstAndObjNameCommand() {
		return sortCatByConstAndObjNameCommand;
	}

	/**
	 * Gets the command that represents the Sort Catalog by Object Name menu item.
	 * 
	 * @return			Sort Catalog by Object Name command
	 */
	protected static String getSortCatByObjNameCommand() {
		return sortCatByObjNameCommand;
	}	

	/**
	 * Gets the command that represents the Sort Catalog by Object Alt Name menu item.
	 * 
	 * @return			Sort Catalog by Obj Alt Name command
	 */
	protected static String getSortCatByObjAltNameCommand() {
		return sortCatByObjAltNameCommand;
	}

	/**
	 * Gets the command that represents the Sort Catalog by Object RA menu item.
	 * 
	 * @return			Sort Catalog by Object RA command
	 */
	protected static String getSortCatByObjRACommand() {
		return sortCatByObjRACommand;
	}

	/**
	 * Gets the command that represents the Sort Catalog by Object Decl menu item.
	 * 
	 * @return			Sort Catalog by Object Decl command
	 */
	protected static String getSortCatByObjDeclCommand() {
		return sortCatByObjDeclCommand;
	}

	/**
	 * Gets the command that represents the Sort Catalog by Object mV menu item.
	 * 
	 * @return			Sort Catalog by Object mV command
	 */
	protected static String getSortCatByObjmVCommand() {
		return sortCatByObjmVCommand;
	}

	// Help menu *********************************************************
	/**
	 * Gets the command that represents the Instructions menu item.
	 * 
	 * @return			Instructions command
	 */
	protected static String getInstructionsCommand() {
		return instructionsCommand;
	}

	/**
	 * Gets the command that represents the About menu item.
	 * 
	 * @return			About command
	 */
	protected static String getAboutCommand() {
		return aboutCommand;
	}

	/*============================
	 * AboutBox methods
	 *===========================*/

	/**
	 * Shows the About Box.
	 */
	protected static void showAboutBox() {
		aboutBox.showAboutBox();
	}	
}
