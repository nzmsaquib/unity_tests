package jll.celcalc.chap1;

import java.util.List;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCatalog;
import jll.celcalc.ASTUtils.ASTConstellation;
import jll.celcalc.ASTUtils.ASTFileIO;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Performs various actions based on what menu item was selected.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class MenuActions {

	/*=========================================
	 * Handle Constellations menu items
	 *========================================*/

	/**
	 * Finds the constellation that a given RA/Decl falls within.
	 */
	protected void findConstellationForRA_Decl() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTTime raObj = new ASTTime();
		ASTAngle declObj = new ASTAngle();
		String str;
		int idx;

		// Get RA & Decl
		if (ASTQuery.showQueryForm("Enter Right Ascension (hh:mm:ss.ss) for Epoch 2000.0",
				"Enter Declination (xxxd yym zz.zzs) for Epoch 2000.0") != ASTQuery.QUERY_OK) return;

		str=ASTQuery.getData1(); // RA
		if ((str == null) || (str.length() <= 0)) return;
		raObj = ASTTime.isValidTime(str,ASTMisc.HIDE_ERRORS);
		if (!raObj.isValidTimeObj()) {
			ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA");
			return;
		}

		str=ASTQuery.getData2(); // Decl
		if ((str == null) || (str.length() <= 0)) return;
		declObj = ASTAngle.isValidAngle(str,ASTMisc.HIDE_ERRORS);
		if (!declObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl");
			return;
		}

		prt.clearTextArea();

		idx = ASTConstellation.findConstellationFromCoord(raObj.getDecTime(),declObj.getDecAngle(),ASTMisc.DEFAULT_EPOCH);

		if (idx < 0) ASTMsg.criticalErrMsg("Could not determine a constellation for the data entered.");
		else {
			prt.println("The location "+ASTTime.timeToStr(raObj, ASTMisc.HMSFORMAT) + " RA, " +
					ASTAngle.angleToStr(declObj,ASTMisc.DMSFORMAT) + " Decl " +
					"is in the "+ASTConstellation.getConstName(idx) + " (" +
					ASTConstellation.getConstAbbrevName(idx)+") constellation");	
		}
		prt.resetCursor();
	}

	/**
	 * Displays a list of all of the constellations.
	 */
	protected void listAllConstellations() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		prt.clearTextArea();
		prt.setFixedWidthFont();
		ASTConstellation.displayAllConstellations();
		prt.setProportionalFont();
		prt.resetCursor();
	}
	
	/**
	 * Displays detailed information about a constellation when
	 * given its abbreviated name.
	 */
	protected void listConstByAbbrevName() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (ASTQuery.showQueryForm("Enter Constellation's 3 Character\nAbbreviated Name") != ASTQuery.QUERY_OK) return;

		String sAbbrevName = ASTQuery.getData1();
		if ((sAbbrevName == null) || (sAbbrevName.length() <= 0)) return;

		prt.clearTextArea();
		int idx = ASTConstellation.findConstellationByAbbrvName(sAbbrevName.trim());
		if (idx < 0) {
			prt.println("No Constellation whose abbreviated name is '" + sAbbrevName + "' was found");
		} else ASTConstellation.displayConstellation(idx);

		prt.resetCursor();
	}

	/**
	 * Displays detailed information about the constellations that contain a target substring
	 * in their "meaning" field.
	 */
	protected void listConstsByMeaning() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int n;

		if (ASTQuery.showQueryForm("Enter Substring to Search for in the\n"+
				"Constellation's 'Meaning' field") != ASTQuery.QUERY_OK) return;

		String targ = ASTQuery.getData1();
		if ((targ == null) || (targ.length() <= 0)) return;

		prt.clearTextArea();

		List<Integer> iResult = ASTConstellation.findConstellationsByMeaning(targ.trim());

		n=iResult.size();

		if (n <= 0) prt.println("No Constellation(s) found with a 'Meaning' field containing the substring '"+targ+"'");
		else {
			// There is at least one constellation to display
			if (n == 1) prt.println("One Constellation has the substring '"+targ+"' in it's 'Meaning' field");
			else prt.println(n + " Constellations have the substring '"+targ+"' in their 'Meaning' field");

			for (int i=0; i < n; i++) {
				prt.println();
				prt.setFixedWidthFont();
				prt.println(String.format("%80s", "*").replace(' ', '*'));
				prt.setProportionalFont();
				prt.println("Constellation # " + (i+1));
				ASTConstellation.displayConstellation(iResult.get(i));
			}
		}

		prt.resetCursor();
	}

	/**
	 * Displays detailed information about a constellation when given its name.
	 */
	protected void listConstByName() {
		ASTPrt prt = ChapGUI.getPrtInstance();


		if (ASTQuery.showQueryForm("Enter Constellation Name to Display") != ASTQuery.QUERY_OK) return;

		String sConstName = ASTQuery.getData1();
		if ((sConstName == null) || (sConstName.length() <= 0)) return;

		prt.clearTextArea();
		int idx = ASTConstellation.findConstellationByName(sConstName.trim());
		if (idx < 0) {
			prt.println("There is no Constellation with the name '"+sConstName+"'");
		} else ASTConstellation.displayConstellation(idx);

		prt.resetCursor();
	}

	/*=========================================
	 * Handle Star Catalogs menu items
	 *========================================*/

	/**
	 * Clears all catalog data currently loaded.
	 */
	protected void clearCatalog() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded, so there is nothing to clear.", "No Catalog Loaded");
			return;
		}

		prt.clearTextArea();

		if (ASTMsg.pleaseConfirm("Are you sure you want to clear all\ncurrently loaded catalog data?","Clear Catalog Data")) {
			ASTCatalog.clearCatalogAndSpaceObjects();
			ChapGUI.setFilename("");
			ChapGUI.setCatalogType("");
			ChapGUI.setEpoch(ASTMisc.DEFAULT_EPOCH);
			prt.println("All currently loaded catalog data was cleared ...");
		}
		else prt.println("Catalog data was not cleared ...");
		prt.resetCursor();
	}

	/**
	 * Loads a star catalog from disk.
	 */
	protected void loadCatalog() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String fullFilename;
		String[] fileToRead = new String[2];

		fileToRead = ASTFileIO.getFileToRead("Select Catalog to Load ...","Load Catalog",ASTCatalog.getFileExtFilter());
		fullFilename = fileToRead[ASTFileIO.FULLPATHNAME_IDX];

		if ((fullFilename == null) || (fullFilename.length() <= 0)) return;

		ChapGUI.setFilename(fullFilename);

		prt.clearTextArea();

		if (ASTCatalog.loadFormattedStarCatalog(fullFilename)) {
			prt.println(String.format("Read in %d different Constellations with a total of %d Objects",
					ASTCatalog.getCatNumConst(),ASTCatalog.getCatNumObjs()));
			ChapGUI.setCatalogType(ASTCatalog.getCatType());
			ChapGUI.setEpoch(ASTCatalog.getCatEpoch());
		} else {			
			ASTMsg.errMsg("Could not load the catalog data from "+fullFilename, "Catalog Load Failed");
			ChapGUI.setFilename("");
			ChapGUI.setCatalogType("");
			ChapGUI.setEpoch(ASTMisc.DEFAULT_EPOCH);
		}

		prt.resetCursor();
	}

	/**
	 * Shows the catalog information from the currently loaded catalog.
	 */
	protected void showCatalogInfo() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTCatalog.isCatalogLoaded()) ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
		else {
			prt.clearTextArea();
			ASTCatalog.displayCatalogInfo();
			prt.resetCursor();
		}
	}	

	/*=====================================
	 * Handle Space Objects menu items
	 *====================================*/
	
	/**
	 * Shows all space objects within a given constellation.
	 */
	protected void listAllObjsByConst() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx;

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}
		
		if (ASTQuery.showQueryForm("Enter Constellation's 3 Character Abbreviated Name") != ASTQuery.QUERY_OK) return;
		
		String constAbbrevName = ASTQuery.getData1();
		if ((constAbbrevName == null) || (constAbbrevName.length() <= 0)) return;
		
		prt.clearTextArea();

		idx = ASTConstellation.findConstellationByAbbrvName(constAbbrevName.trim());
		if (idx < 0) prt.println("No Constellation whose abbreviated name is '" + constAbbrevName + "' was found");
		else {
			prt.setFixedWidthFont();	
			ASTCatalog.displayAllObjsByConstellation(idx,ChapGUI.getSortOrderChkbox());		
			prt.setProportionalFont();
			prt.resetCursor();
		}
	}
	
	/**
	 * Shows all space objects within a user specified index range.
	 */
	protected void listAllObjsByRange() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int iMaxNum = 100;					// default to 1st 100 entries
		int iStart, iEnd;
		ASTInt n = new ASTInt();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}
		
		if (ASTQuery.showQueryForm("Enter index for 1st object to list\n(ex: 1 for 1st object in the catalog)",
				"Enter number of objects to list\n(ex: 10 for total of 10 objects)") != ASTQuery.QUERY_OK) return;
		
		prt.clearTextArea();
		
		n = ASTInt.isValidInt(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (n.isValidIntObj()) iStart = n.getIntValue() - 1;			// 0 base indexing assumed!
		else iStart = 0;
		
		n = ASTInt.isValidInt(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (n.isValidIntObj()) iEnd = iStart + n.getIntValue() - 1;
		else iEnd = iStart + iMaxNum - 1;

		prt.setFixedWidthFont();	
		ASTCatalog.displayAllObjsByRange(iStart,iEnd);		
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Shows all catalog information, including space objects,
	 * in the currently loaded catalog.
	 */
	protected void listAllObjsInCatalog() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}

		prt.clearTextArea();
		ASTCatalog.displayCatalogInfo();
		prt.setFixedWidthFont();
		prt.println(String.format("%80s", "*").replace(' ', '*'));		
		ASTCatalog.displayAllCatalogObjects();		
		prt.setProportionalFont();
		prt.resetCursor();
	}
	
	/**
	 * Displays all catalog information about an object given its alternate name.
	 */
	protected void listObjByAltName() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}

		if (ASTQuery.showQueryForm("Enter the Object's Alternate Name") != ASTQuery.QUERY_OK) return;
		
		String searchStr = ASTQuery.getData1();
		if ((searchStr == null) || (searchStr.length() <= 0)) return;

		prt.clearTextArea();
		int idx = ASTCatalog.findObjByAltName(searchStr.trim());
		if (idx < 0) prt.println("No object whose alternate name is '" + searchStr + "' was found in the catalog");
		else ASTCatalog.displayFullObjInfo(idx);
		prt.resetCursor();
	}

	/**
	 * Displays detailed information about catalog objects that
	 * contain a target substring in their "comments" field.
	 */
	protected void listObjByComments() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int n;

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}
		
		if (ASTQuery.showQueryForm("Enter Substring to Search for in the Object's 'Comments' Field") != ASTQuery.QUERY_OK) return;
		
		String searchStr = ASTQuery.getData1();
		if ((searchStr == null) || (searchStr.length() <= 0)) return;

		prt.clearTextArea();
		
		List<Integer> iResult = ASTCatalog.findObjsByComments(searchStr.trim());

		n = iResult.size();

		if (n <= 0) prt.println("No Objects in the current Catalog have the substring '"+searchStr+"' in their 'Comments' field");
		else {
			if (n == 1) prt.println("One Object has the substring '"+searchStr+"' in it's 'Comments' field");
			else prt.println(n + " Objects have the substring '"+searchStr+"' in their 'Comments' field");

			for (int i=0; i < n; i++) {
				prt.println();
				prt.setFixedWidthFont();
				prt.println(String.format("%80s", "*").replace(' ', '*'));
				prt.setProportionalFont();
				prt.println("Object # " + (i+1));
				ASTCatalog.displayFullObjInfo(iResult.get(i));
			}
		}
		prt.resetCursor();
	}

	/**
	 * Displays all catalog information about an object given its name.
	 */
	protected void listObjByName() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}

		if (ASTQuery.showQueryForm("Enter the Object's Name") != ASTQuery.QUERY_OK) return;
		
		String searchStr = ASTQuery.getData1();
		if ((searchStr == null) || (searchStr.length() <= 0)) return;

		prt.clearTextArea();
		int idx = ASTCatalog.findObjByName(searchStr.trim());
		if (idx < 0) prt.println("No object named '" + searchStr + "' was found in the catalog");
		else ASTCatalog.displayFullObjInfo(idx);

		prt.resetCursor();	
	}

	/**
	 * Sorts the currently loaded catalog in ascending or descending
	 * order depending upon the sort checkbox in the GUI.
	 * 
	 * @param sortField				which field to sort on.
	 */
	protected void sortCatalog(ASTCatalog.CatalogSortField sortField) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		boolean sortOrder = ChapGUI.getSortOrderChkbox();
		
		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}
		
		prt.clearTextArea();
		prt.print("You have elected to sort the catalog in ");
		if (sortOrder == ASTMisc.ASCENDING_ORDER) prt.print("ascending");
		else prt.print("descending");
		prt.println(" order by " + sortField.toStr()+" ... ");	
		prt.println();
		
		ASTCatalog.sortStarCatalog(sortField, sortOrder);
		prt.println("The catalog has now been sorted ...");
		prt.resetCursor();
	}
}
