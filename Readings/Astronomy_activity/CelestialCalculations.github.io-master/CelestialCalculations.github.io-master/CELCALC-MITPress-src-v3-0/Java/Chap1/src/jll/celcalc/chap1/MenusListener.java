package jll.celcalc.chap1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTCatalog;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class MenusListener implements ActionListener {

	// Create an object with methods to handle all but File and Help menus
	private static MenuActions ma = new MenuActions();

	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		} 

		// Handle Constellations menu
		else if (action.equals(ChapGUI.getListConstByNameCommand())) ma.listConstByName();
		else if (action.equals(ChapGUI.getListConstByAbbrevNameCommand()))	ma.listConstByAbbrevName();
		else if (action.equals(ChapGUI.getListConstByMeaningCommand())) ma.listConstsByMeaning();
		else if (action.equals(ChapGUI.getListAllConstellationsCommand())) ma.listAllConstellations();
		else if (action.equals(ChapGUI.getFindConstellationCommand())) ma.findConstellationForRA_Decl();

		// Handle Star Catalogs menu
		else if (action.equals(ChapGUI.getClearCatalogCommand())) ma.clearCatalog();
		else if (action.equals(ChapGUI.getLoadCatalogCommand())) ma.loadCatalog();
		else if (action.equals(ChapGUI.getShowCatalogInfoCommand())) ma.showCatalogInfo();

		// Handle Space Objects menu
		// List Object by ...
		else if (action.equals(ChapGUI.getListObjByNameCommand())) ma.listObjByName();
		else if (action.equals(ChapGUI.getListObjByAltNameCommand())) ma.listObjByAltName();
		else if (action.equals(ChapGUI.getListObjByCommentsCommand()))	ma.listObjByComments();
		// List all Objects by ...
		else if (action.equals(ChapGUI.getListAllObjsInCatalogCommand())) ma.listAllObjsInCatalog();
		else if (action.equals(ChapGUI.getListAllObjsInRangeCommand())) ma.listAllObjsByRange();
		else if (action.equals(ChapGUI.getListAllObjsInConstCommand())) ma.listAllObjsByConst();
		// Sort catalog by ...
		else if (action.equals(ChapGUI.getSortCatByConstellationCommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.CONSTELLATION);
		else if (action.equals(ChapGUI.getSortCatByConstAndObjNameCommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.CONST_AND_OBJNAME);
		else if (action.equals(ChapGUI.getSortCatByObjNameCommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.OBJNAME);
		else if (action.equals(ChapGUI.getSortCatByObjAltNameCommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.OBJ_ALTNAME);
		else if (action.equals(ChapGUI.getSortCatByObjRACommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.RA);
		else if (action.equals(ChapGUI.getSortCatByObjDeclCommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.DECL);
		else if (action.equals(ChapGUI.getSortCatByObjmVCommand())) ma.sortCatalog(ASTCatalog.CatalogSortField.VISUAL_MAGNITUDE);

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			prt.clearTextArea();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("This program allows you to examine constellations and objects in star catalogs in various ways." +
					" If you want the lists of objects to be displayed in ascending order, check the 'Sort in Ascending Order' " +
					" checkbox. If you want to sort in descending order, be sure the checkbox is unchecked.");
			prt.println();

			prt.println("The 'Constellations' menu allows you to display information about the 88 modern constellations. You can " +
					"display a list of all the constellations and basic information about them, or use the " +
					"'List a Constellation by ...' menu item to list individual constellations with more detailed information. " +
					"The 'Find Constellation an Object is In' menu item allows you to enter the Right Ascension (in hours) and " +
					"Declination (in degrees) for an object and determine what constellation the object lies within.");
			prt.println();

			prt.print("The 'Star Catalogs' menu item allows you to load a star catalog and see basic information about the " +
					"catalog, such as where it came from, how many objects are in the catalog, and what constellations " +
					"those objects are in. The catalogs ");
			prt.setBoldFont(true);
			prt.print("*MUST*");
			prt.setBoldFont(false);
			prt.println(" be in the format specifically designed for the programs in this book. Look at the source code for this " +
					"program, the README.txt file, or any of the star catalog data files to understand the format of the " +
					"star catalog data files. The star catalog data files are ordinary text files in a 'pseudo-XML' format. " +
					"You can download publically available data from some astronomy/space organization to create your own " +
					"star catalog files as long as the data is plain ASCII data in exactly the format this book requires.");
			prt.println();

			prt.print("The 'Space Objects' menu allows you to look at objects within an already loaded catalog. You can display " +
					"individual objects by entering their name or alternate name. It is important to note that the name/alternate name " +
					"entered must match a name in the");
			prt.setBoldFont(true);
			prt.print(" *currently loaded* ");
			prt.setBoldFont(false);
			prt.println("catalog because different catalogs (e.g., Messier versus Henry Draper) use different names " +
					"for the same object. The 'List all Space Objects ...' menu entry allows you to " +
					"look at all, or a subset of, the objects in the catalog. For example, if you wish to explore " +
					"the objects within a particular constellation, choose the 'In a Constellation' sub-menu to see " +
					"all objects within the currently loaded catalog that are in a given constellation. Be aware that " +
					"some of the catalogs are quite large and may take considerable time to display all of their objects. " +
					"The 'Sort Catalog by ...' menu item allows you to sort the catalog objects in various ways before " +
					"displaying them.");
			prt.println();

			prt.print("Several menu options, such as those under 'List a Catalog Object by ...', allow you to enter a string, " +
					"such as an object name, to search for in the currently loaded catalog. When searches are performed for " +
					"a string that you enter, the search is not case sensitive. Thus, a search for 'm39' and 'M39' will yield " +
					"the same results. Also, spaces are generally not important so that a search for 'M 39' will yield the " +
					"same result as a search for 'M39'.");
			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
