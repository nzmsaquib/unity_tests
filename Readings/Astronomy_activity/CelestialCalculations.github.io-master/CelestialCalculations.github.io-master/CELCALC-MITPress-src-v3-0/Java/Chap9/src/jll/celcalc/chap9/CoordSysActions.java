package jll.celcalc.chap9;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;
import jll.celcalc.ASTUtils.ASTVect;

/**
 * <b>Handles the Coord Sys menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class CoordSysActions {

	/**
	 * Convert Cartesian coordinates to spherical
	 */
	protected void calcCartesian2Spherical() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double x,y,z,r,alpha, phi, dT;
		ASTReal rTmp;
		String result;

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);;
		ChapGUI.printlnCond("Convert Cartesian Coordinates to Spherical", ASTPrt.CENTERTXT);;
		prt.setBoldFont(false);;
		prt.println();

		prt.setFixedWidthFont();;

		if (ASTQuery.showQueryForm("x", "y", "z", "Enter Cartesian coordinates ...") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) x = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid x value - try again", "Invalid x");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) y = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid y value - try again", "Invalid y");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData3(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) z = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid z value - try again", "Invalid z");
			return;
		}

		ChapGUI.printlnCond("Convert x= " + x + ", y = " + y + ", z = " + z + " to spherical coordinates");
		ChapGUI.printlnCond();

		r = Math.sqrt(x*x + y*y + z*z);
		ChapGUI.printlnCond("1.  r = sqrt[ x^2 + y^2 + z^2 ]");
		ChapGUI.printlnCond("      = sqrt[(" + String.format(ASTStyle.genFloatFormat8,x) + ")^2 + (" + 
				String.format(ASTStyle.genFloatFormat8,y) + ")^2 + (" + 
				String.format(ASTStyle.genFloatFormat8,z) + ")^2]");
		ChapGUI.printlnCond("      = " + r);
		ChapGUI.printlnCond();

		alpha = ASTMath.INVTAN_D(y / x);
		ChapGUI.printlnCond("2.  alpha = inv tan(y/x) = inv tan(" + y + "/" + x + ")");
		ChapGUI.printlnCond("          = " + String.format(ASTStyle.genFloatFormat8,alpha) + " degrees");
		ChapGUI.printlnCond();

		dT = ASTMath.quadAdjust(y, x);
		ChapGUI.printlnCond("3.  Use the algebraic signs of y and x to determine a quadrant adjustment");
		ChapGUI.printlnCond("    and apply it to alpha.");
		ChapGUI.printCond("    alpha = alpha + Adjustment = " + String.format(ASTStyle.genFloatFormat8,alpha) + " + " +
				ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " = ");
		alpha = alpha + dT;
		ChapGUI.printlnCond(String.format(ASTStyle.genFloatFormat8,alpha) + " degrees");
		ChapGUI.printlnCond();

		phi = ASTMath.INVCOS_D(z / r);
		ChapGUI.printlnCond("4.  phi = inv cos(z/r) = inv cos(" + z + "/" + r + ")");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat8,phi) + " degrees");
		ChapGUI.printlnCond();

		result = "[x=" + String.format(ASTStyle.genFloatFormat,x) + ", y=" + String.format(ASTStyle.genFloatFormat,y) +
				", z=" + String.format(ASTStyle.genFloatFormat,z) + "] = [r=" + 
				String.format(ASTStyle.genFloatFormat,r) + ", alpha=" +
				String.format(ASTStyle.genFloatFormat,alpha) + ", phi=" + 
				String.format(ASTStyle.genFloatFormat,phi) + "]";
		ChapGUI.printlnCond("Thus, [x=" + String.format(ASTStyle.genFloatFormat8,x) + 
				", y=" + String.format(ASTStyle.genFloatFormat8,y) + 
				", z=" + String.format(ASTStyle.genFloatFormat8,z) + "]");
		ChapGUI.printlnCond("    = [r=" + String.format(ASTStyle.genFloatFormat8,r) + 
				", alpha=" + String.format(ASTStyle.genFloatFormat8,alpha) +
				", phi=" + String.format(ASTStyle.genFloatFormat8,phi) + "]");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(result);
	}

	/**
	 * Convert ECI (Cartesian) coordinates to horizon
	 */
	protected void calcECI2Horizon() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		double LCT, UT, LST, GST;
		double x, y, z, RA, Decl;
		ASTReal rTmp;
		int dateAdjust;
		ASTInt dateAdjustObj = new ASTInt();
		ASTCoord spherCoord, horizonCoord;
		String result;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert ECI (Cartesian) Coordinates to Horizon", ASTPrt.CENTERTXT);;
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (ASTQuery.showQueryForm("x", "y", "z", "Enter Cartesian coordinates ...") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) x = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid x value - try again", "Invalid x");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) y = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid y value - try again", "Invalid y");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData3(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) z = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid z value - try again", "Invalid z");
			return;
		}

		// Do all the time-related calculations
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		// adjust the date, if needed, since the LCTtoUT conversion could have changed the date
		dateAdjust = dateAdjustObj.getIntValue();
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST,observer.getObsLon());

		ChapGUI.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.");
		ChapGUI.printlnCond("    LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(observer.getObsDate()) + ")");
		ChapGUI.printCond("     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0) ChapGUI.printCond("previous day ");
		else if (dateAdjust > 0) ChapGUI.printCond("next day ");
		ChapGUI.printlnCond(ASTDate.dateToStr(adjustedDate) + ")");
		ChapGUI.printlnCond("    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		spherCoord = ASTCoord.CartesianToSpherical(x, y, z);
		ChapGUI.printlnCond("2.  Convert ECI (cartesian) coordinates to spherical coordinates");
		ChapGUI.printlnCond("    r = " + String.format(ASTStyle.genFloatFormat8,spherCoord.getSpherR().getDecAngle()));
		ChapGUI.printlnCond("    alpha = " + 
				String.format(ASTStyle.genFloatFormat8,spherCoord.getSpherAlpha().getDecAngle()) + " degrees");
		ChapGUI.printlnCond("    phi = " + 
				String.format(ASTStyle.genFloatFormat8,spherCoord.getSpherPhi().getDecAngle()) + " degrees");
		ChapGUI.printlnCond();
		ChapGUI.printlnCond("    Note: r is the distance above the center of the Earth.");
		ChapGUI.printlnCond();

		Decl = 90.0 - spherCoord.getSpherPhi().getDecAngle();
		ChapGUI.printlnCond("3.  Convert phi to declination.");
		ChapGUI.printlnCond("    Decl = 90 - phi = 90 - " + 
				String.format(ASTStyle.genFloatFormat8,spherCoord.getSpherPhi().getDecAngle()) +
				" = " + String.format(ASTStyle.genFloatFormat8,Decl) + " degrees");
		ChapGUI.printlnCond();

		RA = spherCoord.getSpherAlpha().getDecAngle() / 15.0;
		ChapGUI.printlnCond("4.  Convert alpha to hours.");
		ChapGUI.printlnCond("    RA = alpha/15.0 = " + 
				String.format(ASTStyle.genFloatFormat8,spherCoord.getSpherAlpha().getDecAngle()) +
				"/15.0 = " + String.format(ASTStyle.genFloatFormat8,RA) + " hours");
		ChapGUI.printlnCond();

		horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
		ChapGUI.printlnCond("5.  Convert the equatorial coordinates (RA, Decl) to horizon coordinates");
		ChapGUI.printlnCond("    Alt = " + ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) +
				", Az = " + ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = "[x=" + String.format(ASTStyle.genFloatFormat,x) + 
				", y=" + String.format(ASTStyle.genFloatFormat,y) + 
				", z=" + String.format(ASTStyle.genFloatFormat,z) + "] =  " + 
				ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) +
				" Alt, " + ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT) + 
				" Az, dist=" + String.format(ASTStyle.genFloatFormat,spherCoord.getSpherR().getDecAngle());
		ChapGUI.printlnCond("Thus, for this observer location and date/time,");
		ChapGUI.printlnCond("  " + result);

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(result);
	}

	/**
	 * Convert ECI (Cartesian) to Topocentric coordinates
	 */
	protected void calcECI2Topo() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		double LCT, UT, LST, LSTd, GST;
		double x,y,z;
		ASTVect R = new ASTVect(0.0,0.0,0.0);			// Satellite's position in ECI coordinates
		ASTVect R_obs = new ASTVect(0.0,0.0,0.0);		// Observer's position in ECI coordinates	
		ASTVect Rp = new ASTVect(0.0,0.0,0.0);			// Range vector
		ASTVect Rpp = new ASTVect(0.0,0.0,0.0);			// Rotated range vector	
		double obsLat;
		double h_sea;									// Observer's distance above sea level (meters)
		double Alt, Az;
		double rp_e, r_eq, r_dist;
		int dateAdjust;
		ASTReal rTmp;
		ASTInt dateAdjustObj = new ASTInt();
		String result;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		obsLat = observer.getObsLat();

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert ECI (Cartesian) Coordinates to Topocentric", ASTPrt.CENTERTXT);;
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (ASTQuery.showQueryForm("Enter Observer's Height\nabove sea level (in meters)") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) h_sea = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Observer Height - try again", "Invalid Height");
			return;
		}

		if (ASTQuery.showQueryForm("x", "y", "z", "Enter Cartesian coordinates ...") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) x = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid x value - try again", "Invalid x");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) y = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid y value - try again", "Invalid y");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData3(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) z = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid z value - try again", "Invalid z");
			return;
		}
		R.setVect(x,y,z);

		// Do all the time-related calculations
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		// adjust the date, if needed, since the LCTtoUT conversion could have changed the date
		dateAdjust = dateAdjustObj.getIntValue();
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST,observer.getObsLon());

		ChapGUI.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.");
		ChapGUI.printlnCond("    LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(observer.getObsDate()) + ")");
		ChapGUI.printCond("     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0) ChapGUI.printCond("previous day ");
		else if (dateAdjust > 0) ChapGUI.printCond("next day ");
		ChapGUI.printlnCond(ASTDate.dateToStr(adjustedDate) + ")");
		ChapGUI.printlnCond("    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		LSTd = LST * 15.0;
		ChapGUI.printlnCond("2.  Convert the observer's LST to an angle");
		ChapGUI.printlnCond("    LSTd = LST*15 = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + "*15 = " +
				ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		rp_e = ASTOrbits.EarthRadius + (h_sea / 1000);
		r_eq = rp_e * ASTMath.COS_D(obsLat);
		R_obs.setVect(r_eq * ASTMath.COS_D(LSTd), r_eq * ASTMath.SIN_D(LSTd), rp_e * ASTMath.SIN_D(obsLat));
		ChapGUI.printlnCond("3.  Convert the observer's location to ECI coordinates");
		ChapGUI.printlnCond("    rp_e = r_e + (h_obs/1000) = " + ASTOrbits.EarthRadius + " + (" + h_sea + "/1000)");
		ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat8,rp_e) + " km");
		ChapGUI.printlnCond("    r_eq = rp_e * cos(lat) = " + String.format(ASTStyle.genFloatFormat8,rp_e) + " * cos(" +
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat8,r_eq) + " km");
		ChapGUI.printlnCond("    x_obs = r_eq * cos(LSTd) = " + String.format(ASTStyle.genFloatFormat8,r_eq) + " * cos(" +
				String.format(ASTStyle.genFloatFormat8,LSTd) + ")");
		ChapGUI.printlnCond("          = " + String.format(ASTStyle.genFloatFormat8,R_obs.x()));
		ChapGUI.printlnCond("    y_obs = r_eq * sin(LSTd) = " + String.format(ASTStyle.genFloatFormat8,r_eq) + " * sin(" +
				String.format(ASTStyle.genFloatFormat8,LSTd) + ")");
		ChapGUI.printlnCond("          = " + String.format(ASTStyle.genFloatFormat8,R_obs.y()));
		ChapGUI.printlnCond("    z_obs = r_eq * sin(lat) = " + String.format(ASTStyle.genFloatFormat8,r_eq) + " * sin(" +
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("          = " + String.format(ASTStyle.genFloatFormat8,R_obs.z()));
		ChapGUI.printlnCond();

		Rp.setVect(R.x() - R_obs.x(), R.y() - R_obs.y(), R.z() - R_obs.z());
		ChapGUI.printlnCond("4.  Compute the range vector");
		ChapGUI.printlnCond("    x' = x - x_obs = " + String.format(ASTStyle.genFloatFormat8,R.x()) + " - " + 
				String.format(ASTStyle.genFloatFormat8,R_obs.x()));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,Rp.x()));
		ChapGUI.printlnCond("    y' = y - y_obs = " + String.format(ASTStyle.genFloatFormat8,R.y()) + " - " + 
				String.format(ASTStyle.genFloatFormat8,R_obs.y()));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,Rp.y()));
		ChapGUI.printlnCond("    z' = z - z_obs = " + String.format(ASTStyle.genFloatFormat8,R.z()) + " - " + 
				String.format(ASTStyle.genFloatFormat8,R_obs.z()));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,Rp.z()));
		ChapGUI.printlnCond();

		x = Rp.x()*ASTMath.SIN_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.SIN_D(obsLat)*ASTMath.SIN_D(LSTd) - Rp.z()*ASTMath.COS_D(obsLat);
		y = -Rp.x()*ASTMath.SIN_D(LSTd) + Rp.y()*ASTMath.COS_D(LSTd);
		z = Rp.x()*ASTMath.COS_D(obsLat)*ASTMath.COS_D(LSTd) + Rp.y()*ASTMath.COS_D(obsLat)*ASTMath.SIN_D(LSTd) + Rp.z()*ASTMath.SIN_D(obsLat);
		Rpp.setVect(x, y, z);
		ChapGUI.printlnCond("5.  Rotate the range vector by LSTd and latitude");
		ChapGUI.printlnCond("    x'' = x'*sin(lat)*cos(LSTd) + y'*sin(lat)*sin(LSTd) - z'*cos(lat)");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat8,Rp.x()) + "*sin(" + 
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")*cos(" +
				ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + ") + ");
		ChapGUI.printlnCond("          " + String.format(ASTStyle.genFloatFormat8,Rp.y()) + "*sin(" + 
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")*sin(" +
				ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + ") - ");
		ChapGUI.printlnCond("          " + String.format(ASTStyle.genFloatFormat8,Rp.z()) + "*cos(" + 
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat8,Rpp.x()));
		ChapGUI.printlnCond("    y'' = -[x'*sin(LSTd)] + y'*cos(LSTd)");
		ChapGUI.printlnCond("        = -[" + String.format(ASTStyle.genFloatFormat8,Rp.x()) + "*sin(" + 
				ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + ")] + " +
				String.format(ASTStyle.genFloatFormat8,Rp.y()) + "*cos(" + ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat8,Rpp.y()));
		ChapGUI.printlnCond("    z'' = x'*cos(lat)*cos(LSTd) + y'*cos(lat)*sin(LSTd) + z'*sin(lat)");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat8,Rp.x()) + "*cos(" + 
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")*cos(" +
				ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + ") + ");
		ChapGUI.printlnCond("          " + String.format(ASTStyle.genFloatFormat8,Rp.y()) + "*cos(" + 
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")*sin(" +
				ASTAngle.angleToStr(LSTd, ASTMisc.DECFORMAT) + ") + ");
		ChapGUI.printlnCond("          " + String.format(ASTStyle.genFloatFormat8,Rp.z()) + "*sin(" + 
				ASTAngle.angleToStr(obsLat, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat8,Rpp.z()));
		ChapGUI.printlnCond();

		r_dist = Rpp.len();
		ChapGUI.printlnCond("6.  Compute the rotated range vector's magnitude");
		ChapGUI.printlnCond("    r_dist = " + ASTStr.insertCommas(r_dist) + " km");
		ChapGUI.printlnCond();

		Az = ASTMath.INVTAN2_D(-Rpp.y(), Rpp.x());
		ChapGUI.printlnCond("7.  Compute the topocentric azimuth and adjust if necessary to put in the correct quadrant");
		ChapGUI.printlnCond("    Az_topo = inv tan(-y'' / x'') = inv tan(-" + String.format(ASTStyle.genFloatFormat8,Rp.y()) + "/" +
				String.format(ASTStyle.genFloatFormat8,Rp.x()) + ")");
		ChapGUI.printlnCond("            = " + String.format(ASTStyle.genFloatFormat,ASTMath.INVTAN_D(-Rp.y()/Rp.x())) + " degrees");
		ChapGUI.printlnCond("    After adjusting to the correct quadrant,");
		ChapGUI.printlnCond("    Az_topo = " + String.format(ASTStyle.genFloatFormat,Az) + " degrees");
		ChapGUI.printlnCond();

		Alt = ASTMath.INVSIN_D(Rpp.z() / r_dist);
		ChapGUI.printlnCond("8.  Compute the topocentric altitude");
		ChapGUI.printlnCond("    Alt_topo = inv sin(z'' / r_dist) = inv sin(" + String.format(ASTStyle.genFloatFormat8,Rpp.z()) + "/" +
				String.format(ASTStyle.genFloatFormat8,r_dist) + ")");
		ChapGUI.printlnCond("             = " + String.format(ASTStyle.genFloatFormat,Alt) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  Finally, convert Altitude and Azimuth to DMS format. Thus,");
		ChapGUI.printlnCond("    Alt_topo = " + ASTAngle.angleToStr(Alt,ASTMisc.DMSFORMAT) + ", Az_topo = " + 
				ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = "[x=" + String.format(ASTStyle.genFloatFormat8,R.x()) + ", y=" + String.format(ASTStyle.genFloatFormat8,R.y()) +
				", z=" + String.format(ASTStyle.genFloatFormat8,R.z()) + "] =  " + ASTAngle.angleToStr(Alt, ASTMisc.DMSFORMAT) +
				" Alt_topo, " + ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT) + " Az_topo";
		ChapGUI.printlnCond("Thus, for this observer location and date/time,");
		ChapGUI.printlnCond("  " + result);

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(result);
	}

	/**
	 * Convert Keplerian elements to a state vector
	 */
	protected void calcKeplerian2State() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTPrt prtObj = null;
		double[] tmpKepler;
		double axis, inclin, RAAN, argofperi, M0, ecc;
		double RAANp, argofperip;
		double rho, EA, dv, dLen, x, y, z, A;
		ASTVect R = new ASTVect(0.0,0.0,0.0);
		ASTVect V = new ASTVect(0.0,0.0,0.0);
		int Otype;

		ChapGUI.clearTextAreas();

		if (!VDator.validateKeplerianElements()) return;
		inclin = VDator.getInclination();
		ecc = VDator.getEccentricity();
		axis = VDator.getAxisLength();
		RAAN = VDator.getRAAN();
		argofperi = VDator.getArgOfPeri();
		M0 = VDator.getMeanAnomaly();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert Keplerian Elements to the corresponding State Vector", ASTPrt.CENTERTXT);;
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		// Set up whether to display interim results when solving Kepler's equation
		if (ChapGUI.getShowInterimCalcsStatus()) prtObj = prt;

		ChapGUI.printlnCond("Convert these Keplerian Elements to a State Vector:");
		ChapGUI.printlnCond("  Inclination = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		ChapGUI.printlnCond("  Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
		ChapGUI.printlnCond("  Length of Semi-major axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		ChapGUI.printlnCond("  RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
		ChapGUI.printlnCond("  Argument of Perigee = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
		ChapGUI.printlnCond("  Mean anomaly at the epoch = " + String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
		ChapGUI.printlnCond();

		rho = axis * (1.0 - ecc*ecc);
		ChapGUI.printlnCond("1.  Compute the semi-latus rectum from the semi-major axis and eccentricity");
		ChapGUI.printlnCond("    p = a*(1 - e^2) = " + ASTStr.insertCommas(axis) + 
							"*(1 - " + String.format(ASTStyle.genFloatFormat8,ecc) + "^2)");
		ChapGUI.printlnCond("      = " + ASTStr.insertCommas(rho));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2.  Solve Kepler's equation to get the eccentric anomaly from M0");
		if (ChapGUI.getSimpleIterationRBStatus()) {
			tmpKepler = ASTKepler.calcSimpleKepler(prtObj, M0, ecc, Misc.termCriteria);
			EA = tmpKepler[0];
		} else {
			tmpKepler = ASTKepler.calcNewtonKepler(prtObj, M0, ecc, Misc.termCriteria);
			EA = tmpKepler[0];
		}
		ChapGUI.printlnCond("    E = " + String.format(ASTStyle.genFloatFormat8,EA) + " degrees");
		ChapGUI.printlnCond();

		dv = Math.sqrt((1 + ecc) / (1 - ecc)) * ASTMath.TAN_D(EA / 2.0);
		dv = 2.0 * ASTMath.INVTAN_D(dv);
		ChapGUI.printlnCond("3.  Compute the object's true anomaly");
		ChapGUI.printlnCond("    v = 2 * inv tan[ sqrt[(1 + e)/(1 - e)] * tan(E/2) ]");
		ChapGUI.printlnCond("      = 2 * inv tan[ sqrt[(1+" + ecc + ")/(1-" + ecc + ")] * tan(" +
				String.format(ASTStyle.genFloatFormat8,EA) + "/2) ]");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
		ChapGUI.printlnCond();

		dLen = rho / (1 + ecc * ASTMath.COS_D(dv));
		ChapGUI.printlnCond("4.  Compute the length of the positional vector");
		ChapGUI.printlnCond("    Rlen = p/(1 + e*cos(v)) = " + ASTStr.insertCommas(rho) + 
				"/(1 + " + String.format(ASTStyle.genFloatFormat8,ecc) +
				"*cos(" + String.format(ASTStyle.genFloatFormat8,dv) + "))");
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(dLen));
		ChapGUI.printlnCond();

		x = dLen * ASTMath.COS_D(dv);
		y = dLen * ASTMath.SIN_D(dv);
		z = 0.0;
		R.setVect(x, y, z);
		ChapGUI.printlnCond("5.  Compute the positional vector R' in the perifocal coordinate system");
		ChapGUI.printlnCond("    x' = Rlen*cos(v) = " + ASTStr.insertCommas(dLen) + "*cos(" +
				String.format(ASTStyle.genFloatFormat8,dv) + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(x));
		ChapGUI.printlnCond("    y' = Rlen*sin(v) = " + ASTStr.insertCommas(dLen) + "*sin(" + 
				String.format(ASTStyle.genFloatFormat8,dv) + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(y));
		ChapGUI.printlnCond("    z' = 0.0");
		ChapGUI.printlnCond();

		A = Math.sqrt(ASTOrbits.muEarth / rho);
		ChapGUI.printlnCond("6.  Compute a temporary variable A");
		ChapGUI.printlnCond("    A = sqrt(muEarth/p) = sqrt(" + String.format(ASTStyle.genFloatFormat8,ASTOrbits.muEarth) +
				"/" + String.format(ASTStyle.genFloatFormat8,rho) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,A));
		ChapGUI.printlnCond();

		x = -A * ASTMath.SIN_D(dv);
		y = A * (ecc + ASTMath.COS_D(dv));
		z = 0.0;
		V.setVect(x, y, z);
		ChapGUI.printlnCond("7.  Compute the velocity vector V' in the perifocal coordinate system");
		ChapGUI.printlnCond("    Vx' = -A*sin(v) = -(" + String.format(ASTStyle.genFloatFormat8,A) + ")*sin(" + 
				String.format(ASTStyle.genFloatFormat8,dv) + ")");
		ChapGUI.printlnCond("        = " + ASTStr.insertCommas(x));
		// E in the next statement is e, the base of the natural log
		ChapGUI.printlnCond("    Vy' = A*(e + cos(v)) = (" + String.format(ASTStyle.genFloatFormat8,A) + 
				")*(" + String.format(ASTStyle.genFloatFormat8,Math.E) +
				" + cos(" + String.format(ASTStyle.genFloatFormat8,dv) + "))");
		ChapGUI.printlnCond("        = " + ASTStr.insertCommas(y));
		ChapGUI.printlnCond("    Vz' = 0.0");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("8.  Compute the length of the velocity vector to get the velocity");
		ChapGUI.printlnCond("    Vlen' = " + String.format(ASTStyle.genFloatFormat8,V.len()));
		ChapGUI.printlnCond();

		Otype = ASTOrbits.getSatOrbitType(ecc, inclin);
		if (Otype < 0) {
			ASTMsg.errMsg("Orbit must be elliptical. The computed eccentricity\n" + 
					"and inclination are not supported.",
					"Invalid Orbit type");
			return;
		}
		ChapGUI.printlnCond("9.  Determine the orbit type based on orbital eccentricity and inclination");
		ChapGUI.printlnCond("    Otype = " + Otype);
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. Calculate RAAN' based on the orbit type");
		if ((Otype == 1) || (Otype == 2)) {
			RAANp = 0.0;
			ChapGUI.printlnCond("    For orbit types 1 and 2, RAAN' = 0.0 degrees");
		} else {
			RAANp = RAAN;
			ChapGUI.printlnCond("    For orbit types other than 1 and 2, RAAN' = RAAN");
		}
		ChapGUI.printlnCond("    Thus, RAAN' = " + String.format(ASTStyle.genFloatFormat8,RAANp) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Compute w' based on the orbit type");
		if ((Otype == 1) || (Otype == 3)) {
			ChapGUI.printlnCond("    For orbit types 1 and 3, w' = 0.0 degrees");
			argofperip = 0.0;
		} else {
			argofperip = argofperi;
			ChapGUI.printlnCond("    For orbit types other than 1 and 3, w' = w");
		}
		ChapGUI.printlnCond("    Thus, w' = " + String.format(ASTStyle.genFloatFormat8,argofperip) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("12. Use the 'g' functions to rotate the positional");
		ChapGUI.printlnCond("    vector R' by -w' degrees about the z-axis");
		R = ASTMath.RotateZ(-argofperip, R);
		ChapGUI.printlnCond("    Rotating R' by " + String.format(ASTStyle.genFloatFormat8,-argofperip) + 
				" degrees about the z-axis gives");
		ChapGUI.printlnCond("    x' = " + ASTStr.insertCommas(R.x()));
		ChapGUI.printlnCond("    y' = " + ASTStr.insertCommas(R.y()));
		ChapGUI.printlnCond("    z' = " + ASTStr.insertCommas(R.z()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("13. Use the 'f' functions to rotate the vector from the previous step");
		ChapGUI.printlnCond("    by -inclination degrees about the x-axis");
		R = ASTMath.RotateX(-inclin, R);
		ChapGUI.printlnCond("    Rotating R' by " + String.format(ASTStyle.genFloatFormat8,-inclin) + 
				" degrees about the x-axis gives");
		ChapGUI.printlnCond("    x' = " + ASTStr.insertCommas(R.x()));
		ChapGUI.printlnCond("    y' = " + ASTStr.insertCommas(R.y()));
		ChapGUI.printlnCond("    z' = " + ASTStr.insertCommas(R.z()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("14. Use the 'g' functions to rotate the vector from the previous step");
		ChapGUI.printlnCond("    by -RAANp degrees about the z-axis to get the positional vector");
		R = ASTMath.RotateZ(-RAANp, R);
		ChapGUI.printlnCond("    Rotating R' by " + String.format(ASTStyle.genFloatFormat8,-RAANp) + 
				" degrees about the z-axis gives");
		ChapGUI.printlnCond("    x = " + ASTStr.insertCommas(R.x()));
		ChapGUI.printlnCond("    y = " + ASTStr.insertCommas(R.y()));
		ChapGUI.printlnCond("    z = " + ASTStr.insertCommas(R.z()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("15. Use the 'g' functions to rotate the velocity");
		ChapGUI.printlnCond("    vector V' by -w' degrees about the z-axis");
		V = ASTMath.RotateZ(-argofperip, V);
		ChapGUI.printlnCond("    Rotating V' by " + String.format(ASTStyle.genFloatFormat8,-argofperip) + 
				" degrees about the z-axis gives");
		ChapGUI.printlnCond("    Vx' = " + ASTStr.insertCommas(V.x()));
		ChapGUI.printlnCond("    Vy' = " + ASTStr.insertCommas(V.y()));
		ChapGUI.printlnCond("    Vz' = " + ASTStr.insertCommas(V.z()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("16. Use the 'f' functions to rotate the vector from the previous step");
		ChapGUI.printlnCond("    by -inclination degrees about the x-axis");
		V = ASTMath.RotateX(-inclin, V);
		ChapGUI.printlnCond("    Rotating V' by " + String.format(ASTStyle.genFloatFormat8,-inclin) + 
				" degrees about the x-axis gives");
		ChapGUI.printlnCond("    Vx' = " + ASTStr.insertCommas(V.x()));
		ChapGUI.printlnCond("    Vy' = " + ASTStr.insertCommas(V.y()));
		ChapGUI.printlnCond("    Vz' = " + ASTStr.insertCommas(V.z()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("17. Use the 'g' functions to rotate the vector from the previous step");
		ChapGUI.printlnCond("    by -RAANp degrees about the z-axis to get the velocity vector");
		V = ASTMath.RotateZ(-RAANp, V);
		ChapGUI.printlnCond("    Rotating V' by " + String.format(ASTStyle.genFloatFormat8,-RAANp) + 
				" degrees about the z-axis gives");
		ChapGUI.printlnCond("    Vx = " + ASTStr.insertCommas(V.x()));
		ChapGUI.printlnCond("    Vy = " + ASTStr.insertCommas(V.y()));
		ChapGUI.printlnCond("    Vz = " + ASTStr.insertCommas(V.z()));
		ChapGUI.printlnCond();

		prt.println("Input Keplerian Elements (assuming units are km and km/s):");
		prt.println("  Inclination = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		prt.println("  Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
		prt.println("  Length of Semi-major axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		prt.println("  RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
		prt.println("  Argument of Perigee = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
		prt.println("  Mean anomaly at the epoch = " + String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
		prt.println();

		prt.println("Gives the State Vector:");
		prt.println("  [" + ASTStr.insertCommas(ASTMath.Round(R.x(), 3)) + ", " + ASTStr.insertCommas(ASTMath.Round(R.y(), 3)) +
				", " + ASTStr.insertCommas(ASTMath.Round(R.z(), 3)) + "] (positional vector)");
		prt.println("  [" + ASTStr.insertCommas(ASTMath.Round(V.x(), 3)) + ", " + ASTStr.insertCommas(ASTMath.Round(V.y(), 3)) +
				", " + ASTStr.insertCommas(ASTMath.Round(V.z(), 3)) + "] (velocity vector)");
		prt.println();
		prt.println("State Vector gives a velocity of " + ASTMath.Round(V.len(), 3) + " km/s at a distance");
		prt.println("of " + ASTStr.insertCommas(ASTMath.Round(R.len(), 3)) + " km (" + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(R.len()), 3)) +
				" miles) above the center of the Earth.");
		prt.println();

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("State Vector components are listed in the text area below");
	}

	/**
	 * Convert spherical coordinates to Cartesian
	 */
	protected void calcSpherical2Cartesian() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double x, y, z, r, alpha, phi;
		String result;
		ASTReal rTmp;

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert Spherical Coordinates to Cartesian", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (ASTQuery.showQueryForm("r", "alpha", "phi", "Enter Spherical coordinates ...") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) r = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid r value - try again", "Invalid r");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(), ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) alpha = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid alpha value - try again", "Invalid alpha");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData3(), ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) phi = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid phi value - try again", "Invalid phi");
			return;
		}

		ChapGUI.printlnCond("Convert r= " + r + ", alpha = " + alpha + ", phi = " + phi + " to cartesian coordinates");
		ChapGUI.printlnCond();

		x = r * ASTMath.COS_D(alpha) * ASTMath.SIN_D(phi);
		ChapGUI.printlnCond("1.  x = r * cos(alpha) * sin(phi)");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,r) + " * cos(" + 
				String.format(ASTStyle.genFloatFormat8,alpha) +
				") * sin(" + String.format(ASTStyle.genFloatFormat8,phi) + ")");
		ChapGUI.printlnCond("      = " + x);
		ChapGUI.printlnCond();

		y = r * ASTMath.SIN_D(alpha) * ASTMath.SIN_D(phi);
		ChapGUI.printlnCond("2.  y = r * sin(alpha) * sin(phi)");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,r) + " * sin(" + 
				String.format(ASTStyle.genFloatFormat8,alpha) +
				") * sin(" + String.format(ASTStyle.genFloatFormat8,phi) + ")");
		ChapGUI.printlnCond("      = " + y);
		ChapGUI.printlnCond();

		z = r * ASTMath.COS_D(phi);
		ChapGUI.printlnCond("3.  z = r * cos(phi) = " + r + " * cos(" + String.format(ASTStyle.genFloatFormat8,phi) + ")");
		ChapGUI.printlnCond("        = " + z);
		ChapGUI.printlnCond();

		result = "[r=" + String.format(ASTStyle.genFloatFormat,r) + 
				", alpha=" + String.format(ASTStyle.genFloatFormat,alpha) +
				", phi=" + String.format(ASTStyle.genFloatFormat,phi) + 
				"] = [x=" + String.format(ASTStyle.genFloatFormat,x) +
				", y=" + String.format(ASTStyle.genFloatFormat,y) + 
				", z=" + String.format(ASTStyle.genFloatFormat,z) + "]";
		ChapGUI.printlnCond("Thus, [r=" + String.format(ASTStyle.genFloatFormat8,r) + 
				", alpha=" + String.format(ASTStyle.genFloatFormat8,alpha) +
				", phi=" + String.format(ASTStyle.genFloatFormat8,phi) + "]");
		ChapGUI.printlnCond("    = [x=" + String.format(ASTStyle.genFloatFormat8,x) + 
				", y=" + String.format(ASTStyle.genFloatFormat8,y) +
				", z=" + String.format(ASTStyle.genFloatFormat8,z) + "]");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(result);
	}

	/**
	 * Convert a state vector to Keplerian elements
	 */
	protected void calcState2Keplerian() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTVect R;
		ASTVect V;
		ASTVect H = new ASTVect();
		ASTVect N = new ASTVect();
		ASTVect e = new ASTVect();
		double x, y, z, A, B, rho, axis, inclin, RAAN, argofperi, w_t;
		double dv, L_t, u, EA, MA, M0;
		int Otype;

		// Note: dv is the satellite's true anomaly. It is undefined for circular
		// orbits. For all other orbits, the true anomaly is calculated below.
		// However, it is initialized here to avoid compiler warnings about
		// a potentially uninitialized variable.
		dv = 1.0;

		ChapGUI.clearTextAreas();

		if (!VDator.validateStateVect()) return;
		R = VDator.getPosVect();
		V = VDator.getVelVect();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert a State Vector to its Keplerian Elements", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("Convert the state vector");
		ChapGUI.printlnCond("  [" + String.format(ASTStyle.genFloatFormat8,R.x()) + 
				", " + String.format(ASTStyle.genFloatFormat8,R.y()) +
				", " + String.format(ASTStyle.genFloatFormat8,R.z()) + "] (positional vector)");
		ChapGUI.printlnCond("  [" + String.format(ASTStyle.genFloatFormat8,V.x()) + 
				", " + String.format(ASTStyle.genFloatFormat8,V.y()) +
				", " + String.format(ASTStyle.genFloatFormat8,V.z()) + "] (velocity vector)");
		ChapGUI.printlnCond("to its equivalent Keplerian elements");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("1.  Compute the length of the position vector R");
		ChapGUI.printlnCond("    Rlen = sqrt[x^2 + y^2 + z^2]");
		ChapGUI.printlnCond("         = sqrt[(" + String.format(ASTStyle.genFloatFormat8,R.x()) + 
				")^2 + (" + String.format(ASTStyle.genFloatFormat8,R.y()) +
				")^2 + (" + String.format(ASTStyle.genFloatFormat8,R.z()) + ")^2]");
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(R.len()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2.  Compute the length of the velocity vector V");
		ChapGUI.printlnCond("    Vlen = sqrt[Vx^2 + Vy^2 + Vz^2]");
		ChapGUI.printlnCond("         = sqrt[(" + String.format(ASTStyle.genFloatFormat8,V.x()) + 
				")^2 + (" + String.format(ASTStyle.genFloatFormat8,V.y()) +
				")^2 + (" + String.format(ASTStyle.genFloatFormat8,V.z()) + ")^2]");
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(V.len()));
		ChapGUI.printlnCond();

		x = R.y() * V.z() - R.z() * V.y();
		y = R.z() * V.x() - R.x() * V.z();
		z = R.x() * V.y() - R.y() * V.x();
		H.setVect(x, y, z);
		ChapGUI.printlnCond("3.  Compute the angular momentum vector H=[Hx Hy Hz]");
		ChapGUI.printlnCond("    Hx = yVz - zVy = (" + String.format(ASTStyle.genFloatFormat8,R.y()) +
				")*(" + String.format(ASTStyle.genFloatFormat8,V.z()) + ") - (" +
				String.format(ASTStyle.genFloatFormat8,R.z()) + ")*(" + 
				String.format(ASTStyle.genFloatFormat8,V.y()) + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(H.x()));
		ChapGUI.printlnCond("    Hy = zVx - xVz = (" + String.format(ASTStyle.genFloatFormat8,R.z()) + 
				")*(" + String.format(ASTStyle.genFloatFormat8,V.x()) + ") - (" +
				String.format(ASTStyle.genFloatFormat8,R.x()) + ")*(" + 
				String.format(ASTStyle.genFloatFormat8,V.z()) + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(H.y()));
		ChapGUI.printlnCond("    Hz = xVy - yVx = (" + String.format(ASTStyle.genFloatFormat8,R.x()) +
				")*(" + String.format(ASTStyle.genFloatFormat8,V.y()) + ") - (" +
				String.format(ASTStyle.genFloatFormat8,R.y()) + ")*(" + 
				String.format(ASTStyle.genFloatFormat8,V.x()) + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(H.z()));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4.  Compute the length of the angular momentum vector H");
		ChapGUI.printlnCond("    Hlen = sqrt[Hx^2 + Hy^2 + Hz^2]");
		ChapGUI.printlnCond("         = sqrt[(" + String.format(ASTStyle.genFloatFormat8,H.x()) + 
				")^2 + (" + String.format(ASTStyle.genFloatFormat8,H.y()) +
				")^2 + (" + String.format(ASTStyle.genFloatFormat8,H.z()) + ")^2]");
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(H.len()));
		ChapGUI.printlnCond();

		x = -H.y();
		y = H.x();
		z = 0.0;
		N.setVect(x, y, z);
		ChapGUI.printlnCond("5.  Compute the node vector.");
		ChapGUI.printlnCond("    Nx = -Hy = -(" + H.y() + ") = " + ASTStr.insertCommas(N.x()));
		ChapGUI.printlnCond("    Ny = Hx = " + ASTStr.insertCommas(N.y()));
		ChapGUI.printlnCond("    Nz = 0.0");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  Compute the length of the node vector N");
		ChapGUI.printlnCond("    Nlen = " + ASTStr.insertCommas(N.len()));
		ChapGUI.printlnCond();

		A = V.len()*V.len() - (ASTOrbits.muEarth / R.len());
		B = R.x() * V.x() + R.y() * V.y() + R.z() * V.z();
		ChapGUI.printlnCond("7.  Calculate temporary variables A and B");
		ChapGUI.printlnCond("    A = Vlen^2 - (muEarth/Rlen) = " + ASTStr.insertCommas(V.len()) + "^2 - (" +
				ASTOrbits.muEarth + "/" + ASTStr.insertCommas(R.len()) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,A));
		ChapGUI.printlnCond("    B = xVx + yVy + zVz");
		ChapGUI.printlnCond("      = (" + ASTStr.insertCommas(R.x()) + ")*(" + ASTStr.insertCommas(V.x()) + ") + (" +
				ASTStr.insertCommas(R.y()) + ")*(" + ASTStr.insertCommas(V.y()) + ") + (" + ASTStr.insertCommas(R.z()) +
				")*(" + ASTStr.insertCommas(V.z()) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,B));
		ChapGUI.printlnCond();

		x = (R.x() * A - B * V.x()) / ASTOrbits.muEarth;
		y = (R.y() * A - B * V.y()) / ASTOrbits.muEarth;
		z = (R.z() * A - B * V.z()) / ASTOrbits.muEarth;
		e.setVect(x, y, z);
		ChapGUI.printlnCond("8.  Compute the eccentricity vector");
		ChapGUI.printlnCond("    ex = [xA - BVx]/muEarth");
		ChapGUI.printlnCond("       = [(" + ASTStr.insertCommas(R.x()) + ")*(" + 
				String.format(ASTStyle.genFloatFormat8,A) + ") - (" +
				String.format(ASTStyle.genFloatFormat8,B) + ")*(" + 
				ASTStr.insertCommas(V.x()) + ")]/" + ASTOrbits.muEarth);
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,x));
		ChapGUI.printlnCond("    ey = [yA - BVy]/muEarth");
		ChapGUI.printlnCond("       = [(" + ASTStr.insertCommas(R.y()) + ")*(" + 
				String.format(ASTStyle.genFloatFormat8,A) + ") - (" +
				String.format(ASTStyle.genFloatFormat8,B) + ")*(" + 
				ASTStr.insertCommas(V.y()) + ")]/" + ASTOrbits.muEarth);
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,y));
		ChapGUI.printlnCond("    ez = [zA - BVz]/muEarth");
		ChapGUI.printlnCond("       = [(" + ASTStr.insertCommas(R.z()) + ")*(" + 
				String.format(ASTStyle.genFloatFormat8,A) + ") - (" +
				String.format(ASTStyle.genFloatFormat8,B) + ")*(" + 
				ASTStr.insertCommas(V.z()) + ")]/" + ASTOrbits.muEarth);
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,z));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  Compute the length of the eccentricity vector to get the orbital eccentricity");
		ChapGUI.printlnCond("    e = " + String.format(ASTStyle.genFloatFormat8,e.len()));
		ChapGUI.printlnCond();

		rho = (H.len()*H.len()) / ASTOrbits.muEarth;
		ChapGUI.printlnCond("10. Compute the semi-latus rectum");
		ChapGUI.printlnCond("    p = (Hlen^2)/muEarth = (" + ASTStr.insertCommas(H.len()) + "^2)/" + ASTOrbits.muEarth);
		ChapGUI.printlnCond("      = " + ASTStr.insertCommas(rho));
		ChapGUI.printlnCond();

		axis = rho / (1 - (e.len()*e.len()));
		ChapGUI.printlnCond("11. Use the semi-latus rectum and eccentricity to compute the semi-major axis");
		ChapGUI.printlnCond("    a = p/(1 - e^2) = " + ASTStr.insertCommas(rho) + "/(1 - " + 
				String.format(ASTStyle.genFloatFormat8,e.len()) + "^2)");
		ChapGUI.printlnCond("      = " + ASTStr.insertCommas(axis));
		ChapGUI.printlnCond();

		inclin = ASTMath.INVCOS_D(H.z() / H.len());
		ChapGUI.printlnCond("12. Compute the orbital inclination");
		ChapGUI.printlnCond("    inclin = inv cos(Hz/Hlen) = inv cos(" + ASTStr.insertCommas(H.z()) + 
				"/" + ASTStr.insertCommas(H.len()) + ")");
		ChapGUI.printlnCond("           = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		ChapGUI.printlnCond();

		Otype = ASTOrbits.getSatOrbitType(e.len(), inclin);
		if (Otype < 0) {
			ASTMsg.errMsg("Orbit must be elliptical. The computed eccentricity\n" 
					+ "and inclination are not supported.","Invalid Orbit type");
			return;
		}
		ChapGUI.printlnCond("13. Determine the orbit type based on orbital eccentricity and inclination");
		ChapGUI.printlnCond("    Otype = " + Otype);
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("14. Calculate the right ascension of the ascending node");
		if ((Otype == 1) || (Otype == 2)) {
			RAAN = 0.0;
			ChapGUI.printlnCond("    RAAN = 0.0 degrees for orbit types 1 and 2");
		} else {
			RAAN = ASTMath.INVCOS_D(-H.y() / N.len());
			ChapGUI.printlnCond("    RAAN = inv cos(-Hy/Nlen) = inv cos[-(" + 
					ASTStr.insertCommas(H.y()) + ")/" + ASTStr.insertCommas(N.len()) + "]");
			ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
			ChapGUI.printlnCond("    if Hx < 0, subtract RAAN from 360 to put it into the correct quadrant");
			if (H.x() < 0.0) {
				RAAN = 360.0 - RAAN;
				ChapGUI.printlnCond("    RAAN = 360 - RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
			} else ChapGUI.printlnCond("    No adjustment required.");
		}
		ChapGUI.printlnCond();

		argofperi = 0.0;
		ChapGUI.printlnCond("15. Calculate the argument of perigee");
		if ((Otype == 1) || (Otype == 3)) {
			argofperi = 0.0;
			ChapGUI.printlnCond("    w = 0.0 degrees for orbit types 1 and 3. Skip to step 18.");
		} else ChapGUI.printlnCond("    For orbit types 2 and 4, proceed to step 16.");
		ChapGUI.printlnCond();

		w_t = ASTMath.INVCOS_D(e.x() / e.len());
		if (e.y() < 0.0) w_t = 360.0 - w_t;
		ChapGUI.printlnCond("16. Calculate the argument of perigee");
		if ((Otype == 1) || (Otype == 3)) ChapGUI.printlnCond("    For orbit types 1 and 3, skip to step 18.");
		else if (Otype == 2) {
			ChapGUI.printlnCond("    For orbit type 2, set w = w_t and skip to step 18.");
			argofperi = ASTMath.INVCOS_D(e.x() / e.len());
			ChapGUI.printlnCond("    w = w_t = inv cos(ex/e) = inv cos(" + 
					String.format(ASTStyle.genFloatFormat8,e.x()) + "/" + 
					String.format(ASTStyle.genFloatFormat8,e.len()) + ")");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
			ChapGUI.printlnCond("    if ey < 0, subtract w from 360 to put it into the correct quadrant");
			if (e.y() < 0.0) {
				argofperi = 360.0 - argofperi;
				ChapGUI.printlnCond("    w = 360 - w = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
			} else ChapGUI.printlnCond("    No adjustment required.");
		}else {
			ChapGUI.printlnCond("    For orbit type 4, must proceed to step 17, but");
			ChapGUI.printlnCond("    w_t = " + String.format(ASTStyle.genFloatFormat8,w_t) + " degrees");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("17. Calculate the argument of perigee");
		if (Otype != 4) ChapGUI.printlnCond("   Skip to step 18 for this orbit type. w was calculated in a prior step.");
		else {
			argofperi = ASTMath.INVCOS_D((e.y() * H.x() - e.x() * H.y()) / (e.len() * N.len()));
			ChapGUI.printlnCond("    w = inv cos[(eyHx-exHy)/(eNlen)]");
			ChapGUI.printlnCond("      = inv cos[((" + String.format(ASTStyle.genFloatFormat8,e.y()) +
					")*(" + ASTStr.insertCommas(H.x()) + ") - (" +
					String.format(ASTStyle.genFloatFormat8,e.x()) + ")*(" + 
					ASTStr.insertCommas(H.y()) + "))/");
			ChapGUI.printlnCond("                (" + String.format(ASTStyle.genFloatFormat8,e.len()) +
					")*(" + ASTStr.insertCommas(N.len()) + "))]");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
			ChapGUI.printlnCond("     if ez < 0, subtract w from 360 to put it into the correct quadrant");
			if (e.z() < 0.0) {
				argofperi = 360.0 - argofperi;
				ChapGUI.printlnCond("     w = 360 - w = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
			} else ChapGUI.printlnCond("     No adjustment required.");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("18. Compute the object's true anomaly");
		if (Otype == 1) {
			L_t = ASTMath.INVCOS_D(R.x() / R.len());
			dv = L_t;
			ChapGUI.printlnCond("    For orbit type 1, v = L_t and skip to step 21.");
			ChapGUI.printlnCond("    v = L_t = inv cos(x/Rlen) = inv cos(" + ASTStr.insertCommas(R.x()) + 
					"/" + ASTStr.insertCommas(R.len()) + ")");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,L_t) + " degrees");
			ChapGUI.printlnCond("    if y < 0, subtract v from 360 to put it into the correct quadrant");
			if (R.y() < 0.0) {
				dv = 360.0 - dv;
				ChapGUI.printlnCond("    v = 360 - v = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
			} else ChapGUI.printlnCond("    No adjustment required.");
		} else ChapGUI.printlnCond("    For orbit types other than 1, proceed to next step");
		ChapGUI.printlnCond();

		u = ASTMath.INVCOS_D((R.x() * N.x() + R.y() * N.y()) / (N.len() * R.len()));
		if (R.z() < 0) u = 360.0 - u;
		ChapGUI.printlnCond("19. Compute the object's true anomaly.");
		if (Otype == 1) ChapGUI.printlnCond("    For orbit type 1, proceed to step 21.");
		else if (Otype == 3) {
			dv = ASTMath.INVCOS_D((R.x() * N.x() + R.y() * N.y()) / (N.len() * R.len()));
			ChapGUI.printlnCond("    For orbit type 3, set v = u and skip to step 21.");
			ChapGUI.printlnCond("    u = inv cos[(xNx + yNy)/(Nlen*Rlen)])");
			ChapGUI.printlnCond("      = inv cos[((" + ASTStr.insertCommas(R.x()) + ")*(" +
					ASTStr.insertCommas(N.x()) + ") + (" +
					ASTStr.insertCommas(R.y()) + ")*(" + ASTStr.insertCommas(N.y()) + 
					"))/(" + ASTStr.insertCommas(N.len()) + "*" +
					ASTStr.insertCommas(R.len()) + ")]");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
			ChapGUI.printlnCond("    if z < 0, subtract v from 360 to put it into the correct quadrant");
			if (R.z() < 0.0) {
				dv = 360.0 - dv;
				ChapGUI.printlnCond("    v = 360 - v = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
			} else ChapGUI.printlnCond("    No adjustment required.");
		} else {
			ChapGUI.printlnCond("    For orbit types other than 3, proceed to the next step, but");
			ChapGUI.printlnCond("    u = " + String.format(ASTStyle.genFloatFormat8,u) + " degrees");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("20. Compute the object's true anomaly.");
		if ((Otype == 1) || (Otype == 3)) ChapGUI.printlnCond("    For orbit types 1 and 3, skip to step 21.");
		else {
			dv = ASTMath.INVCOS_D((e.x() * R.x() + e.y() * R.y() + e.z() * R.z()) / (e.len() * R.len()));
			ChapGUI.printlnCond("    v = inv cos[(ex*x + ey*y + ez*z)/(e*Rlen)]");
			ChapGUI.printlnCond("      = inv cos[((" + String.format(ASTStyle.genFloatFormat8,e.x()) +
					")*(" + ASTStr.insertCommas(R.x()) + ") + (" +
					String.format(ASTStyle.genFloatFormat8,e.y()) + ")*(" + 
					ASTStr.insertCommas(R.y()) + ") + (" +
					String.format(ASTStyle.genFloatFormat8,e.z()) + ")*(" + 
					ASTStr.insertCommas(R.z()) + "))/");
			ChapGUI.printlnCond("                (" + String.format(ASTStyle.genFloatFormat8,e.len()) +
					"*" + ASTStr.insertCommas(R.len()) + ")]");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
			ChapGUI.printlnCond("    if B < 0, subtract v from 360 to put it into the correct quadrant");
			if (B < 0.0) {
				dv = 360.0 - dv;
				ChapGUI.printlnCond("    v = 360 - v = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
			} else ChapGUI.printlnCond("    No adjustment required.");
		}
		ChapGUI.printlnCond();

		EA = ASTMath.INVCOS_D((e.len() + ASTMath.COS_D(dv)) / (1 + e.len() * ASTMath.COS_D(dv)));
		ChapGUI.printlnCond("21. Use the true anomaly to calculate the eccentric anomaly");
		ChapGUI.printlnCond("    E = inv cos[(e + cos(v))/(1 + e*cos(v))]");
		ChapGUI.printlnCond("      = inv cos[(" + String.format(ASTStyle.genFloatFormat8,e.len()) + 
				" + cos(" + String.format(ASTStyle.genFloatFormat8,dv) + "))/(1 + " +
				String.format(ASTStyle.genFloatFormat8,e.len()) + "*cos(" + 
				String.format(ASTStyle.genFloatFormat8,dv) + "))]");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,EA) + " degrees");
		ChapGUI.printlnCond("    if v > 180, then subtract E from 360 to put it into the correct quadrant");
		if (dv > 180.0) {
			EA = 360 - EA;
			ChapGUI.printlnCond("    E = 360 - E = " + String.format(ASTStyle.genFloatFormat8,EA) + " degrees");
		} else ChapGUI.printlnCond("    No adjustment necessary");
		ChapGUI.printlnCond();

		MA = ASTMath.deg2rad(EA) - e.len() * ASTMath.SIN_D(EA);
		ChapGUI.printlnCond("22. Use E to compute the mean anomaly at the epoch.");
		ChapGUI.printlnCond("    M = E - e*sin(E) where E **must** be in radians");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,ASTMath.deg2rad(EA)) + " - " + 
				String.format(ASTStyle.genFloatFormat8,e.len()) + "*sin(" +
				String.format(ASTStyle.genFloatFormat8,ASTMath.deg2rad(EA)) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,MA) + " radians");
		ChapGUI.printlnCond();

		M0 = ASTMath.rad2deg(MA);
		ChapGUI.printlnCond("23. Convert M to degrees");
		ChapGUI.printlnCond("    M0 = (180*M)/PI = " + String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
		ChapGUI.printlnCond();

		prt.println("Input State Vector (assuming units are km and km/s):");
		prt.println("  [" + String.format(ASTStyle.genFloatFormat8,R.x()) + ", " + 
				String.format(ASTStyle.genFloatFormat8,R.y()) + ", " + 
				String.format(ASTStyle.genFloatFormat8,R.z()) + "] (positional vector)");
		prt.println("  [" + String.format(ASTStyle.genFloatFormat8,V.x()) + ", " + String.format(ASTStyle.genFloatFormat8,V.y()) +
				", " + String.format(ASTStyle.genFloatFormat8,V.z()) + "] (velocity vector)");
		prt.println();
		prt.println("State Vector gives a velocity of " + ASTMath.Round(V.len(), 3) + " km/s at a distance");
		prt.println("of " + ASTStr.insertCommas(ASTMath.Round(R.len(), 3)) + " km (" + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(R.len()), 3)) +
				" miles) above the center of the Earth.");
		prt.println();
		prt.println("Keplerian Elements are:");
		prt.println("  Inclination = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		prt.println("  Eccentricity = " + String.format(ASTStyle.genFloatFormat8,e.len()));
		prt.println("  Length of Semi-major axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		prt.println("  RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
		prt.println("  Argument of Perigee = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
		prt.println("  Mean anomaly at the epoch = " + String.format(ASTStyle.genFloatFormat8,M0) + " degrees");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Keplerian Elements are listed in the text area below");
	}

}
