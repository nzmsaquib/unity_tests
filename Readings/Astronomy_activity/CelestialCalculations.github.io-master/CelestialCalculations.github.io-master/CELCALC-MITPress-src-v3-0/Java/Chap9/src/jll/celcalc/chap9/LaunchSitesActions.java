package jll.celcalc.chap9;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTSites;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;

/**
 * <b>Handles the Launch Sites menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class LaunchSitesActions {

	/**
	 * Calculate the orbital inclination when launched
	 * from a launch site.
	 * 
	 * @param calctype		INCLINATION_FROM_FILE or
	 * 						INCLINATION_FROM_INPUT
	 */
	protected void calcInclination(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx = 0;
		ASTAngle Az;
		ASTLatLon latObj;
		double inclin, dLat;

		if (calctype == ChapMenuItems.CalculationType.INCLINATION_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter Launch Site latitude",
					"Enter Launch Azimuth") != ASTQuery.QUERY_OK) return;

			latObj = ASTLatLon.isValidLat(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (!latObj.isLatValid()) {
				ASTMsg.errMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude");
				return;
			}
		} else {						// INCLINATION_FROM_FILE 	  
			if (!ASTSites.areSitesLoaded()) {
				ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded");
				return;
			}
			if (ASTQuery.showQueryForm("Enter Name of Launch Site",
					"Enter Launch Azimuth") != ASTQuery.QUERY_OK) return;
			idx = ASTSites.findSiteName(ASTQuery.getData1());
			if (idx < 0) {
				ASTMsg.errMsg("No Launch Site named '" + ASTQuery.getData1() + "' exists\n" + 
						"in the currently loaded launch sites database", "Invalid Site name");
				return;
			}
			latObj = ASTSites.getSiteLat(idx);
		}

		dLat = latObj.getLat();

		Az = ASTAngle.isValidAngle(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (!Az.isValidAngleObj()) {
			ASTMsg.errMsg("Invalid Azimuth - try again", "Invalid Azimuth");
			return;
		}
		if ((Az.getDecAngle() < 0.0) || (Az.getDecAngle() > 360.0)) {
			ASTMsg.errMsg("Invalid Azimuth - try again", "Invalid Azimuth");
			return;
		}

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.INCLINATION_FROM_INPUT) {
			ChapGUI.printlnCond("Calculate the Orbital Inclination when launched from " + 
					ASTLatLon.latToStr(latObj, ASTMisc.DECFORMAT) + " degrees", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Calculate the Orbital Inclination when launched from " + 
					ASTSites.getSiteName(idx), ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.INCLINATION_FROM_INPUT) {
			ChapGUI.printlnCond("The Launch Site latitude entered was " + 
					ASTLatLon.latToStr(latObj, ASTMisc.DECFORMAT) +
					" degrees (" + ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + ")");
			ChapGUI.printlnCond("The Azimuth entered was A = " + Az.getDecAngle() + " degrees (" + 
					ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT) + ")");
		} else {
			ChapGUI.printlnCond("The selected launch site is at latitude " + dLat + " degrees (" + 
					ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + ")");
			ChapGUI.printlnCond("The Azimuth entered was A = " + Az.getDecAngle() + " degrees (" + 
					ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT) + ")");
		}
		ChapGUI.printlnCond();

		inclin = ASTMath.SIN_D(Az.getDecAngle()) * ASTMath.COS_D(dLat);
		inclin = ASTMath.INVCOS_D(inclin);
		ChapGUI.printlnCond("inclination = inv cos[sin(A)*cos(Lat)]");
		ChapGUI.printlnCond("            = inv cos[sin(" + 
				String.format(ASTStyle.genFloatFormat8,Az.getDecAngle()) +
				")*cos(" + String.format(ASTStyle.genFloatFormat8,dLat) + ")]");
		ChapGUI.printlnCond("            = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		ChapGUI.printlnCond();

		if (calctype == ChapMenuItems.CalculationType.INCLINATION_FROM_INPUT) {
			ChapGUI.printlnCond("Launching from " + ASTAngle.angleToStr(dLat, ASTMisc.DMSFORMAT) +
					" at an Azimuth of " + ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT));
		} else {
			ChapGUI.printlnCond("Launching from " + ASTSites.getSiteName(idx) + " at an Azimuth of " + 
					ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT));
		}
		ChapGUI.printlnCond("results in an orbital inclination of " + 
				ASTAngle.angleToStr(inclin, ASTMisc.DMSFORMAT) + " (" +
				ASTAngle.angleToStr(inclin, ASTMisc.DECFORMAT) + " deg)");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Lat = " + ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + 
				", A = " + ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT) +
				" results in inclination = " + ASTAngle.angleToStr(inclin, ASTMisc.DMSFORMAT) + " (" +
				ASTAngle.angleToStr(inclin, ASTMisc.DECFORMAT) + " deg)");

	}

	/**
	 * Calculate the launch azimuth required to achieve
	 * a specific orbital inclination.
	 * 
	 * @param calctype	LAUNCH_AZIMUTH_FROM_FILE or
	 * 					LAUNCH_AZIMUTH_FROM_INPUT
	 */
	protected void calcLaunchAzimuth(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx = 0;
		ASTLatLon latObj;
		double dLat, Az, inclin;
		ASTAngle tmpAngle;

		if (calctype == ChapMenuItems.CalculationType.LAUNCH_AZIMUTH_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter Launch Site latitude",
					"Enter Desired Orbital Inclination") != ASTQuery.QUERY_OK) return;
			latObj = ASTLatLon.isValidLat(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS); 
		} else {
			if (!ASTSites.areSitesLoaded()) {
				ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded");
				return;
			}
			if (ASTQuery.showQueryForm("Enter Name of Launch Site",
					"Enter Desired Orbital Inclination") != ASTQuery.QUERY_OK) return;
			idx = ASTSites.findSiteName(ASTQuery.getData1());
			if (idx < 0) {
				ASTMsg.errMsg("No Launch Site named '" + ASTQuery.getData1() + "' exists\n" + 
						"in the currently loaded launch sites database", "Invalid Site name");
				return;
			}
			latObj= ASTSites.getSiteLat(idx);
		}
		if (latObj.isLatValid()) dLat = latObj.getLat();
		else {
			ASTMsg.errMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude");
			return;
		}
		// If too close to polar, the cos function "blows up"
		if ((dLat < -85.5) || (dLat > 85.5)) {
			ASTMsg.errMsg("Launch site latitude is out of range [-85.5, 85.5] - try again", "Invalid Site Latitude");
			return;
		}

		tmpAngle = ASTAngle.isValidAngle(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (tmpAngle.isValidAngleObj()) inclin = tmpAngle.getDecAngle();
		else {
			ASTMsg.errMsg("Invalid inclination - try again", "Invalid inclination");
			return;
		}
		if ((inclin < -90.0) || (inclin > 90.0)) {
			ASTMsg.errMsg("Invalid Inclination - try again", "Invalid Inclination");
			return;
		}

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.LAUNCH_AZIMUTH_FROM_INPUT) {
			ChapGUI.printlnCond("Calculate the required launch azimuth when launched from " +
					ASTLatLon.latToStr(latObj, ASTMisc.DECFORMAT) +
					" degrees", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Calculate the required launch azimuth when launched from " + 
					ASTSites.getSiteName(idx), ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("The selected launch site is at latitude " + dLat + " degrees (" +
				ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + ")");
		ChapGUI.printlnCond("The desired inclination is " + inclin + " degrees (" +
				ASTAngle.angleToStr(inclin, ASTMisc.DMSFORMAT) + ")");
		ChapGUI.printlnCond();

		if (Math.abs(inclin) < Math.abs(dLat)) {
			ChapGUI.printlnCond("Error: Orbital Inclination must be greater than the launch latitude");
			ChapGUI.setResults("Error: Orbital Inclination must be greater than the launch latitude");
			return;
		}

		Az = ASTMath.COS_D(inclin) / ASTMath.COS_D(dLat);
		Az = ASTMath.INVSIN_D(Az);
		ChapGUI.printlnCond("A = inv sin[cos(inclination)/cos(Lat)]");
		ChapGUI.printlnCond("  = inv sin[cos(" + String.format(ASTStyle.genFloatFormat8,inclin) +
				")/cos(" + String.format(ASTStyle.genFloatFormat8,dLat) + ")]");
		ChapGUI.printlnCond("  = " + String.format(ASTStyle.genFloatFormat8,Az) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("To achieve an orbital inclination of " + 
				ASTAngle.angleToStr(inclin, ASTMisc.DECFORMAT) + " deg (" +
				ASTAngle.angleToStr(inclin, ASTMisc.DMSFORMAT) + ")");
		if (calctype == ChapMenuItems.CalculationType.LAUNCH_AZIMUTH_FROM_INPUT) {
			ChapGUI.printlnCond("when launched from a site at " + dLat + " degrees (" +
					ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + ") requires a");
		} else {
			ChapGUI.printlnCond("when launching from " + ASTSites.getSiteName(idx) + " requires a");
		}
		ChapGUI.printlnCond("launch azimuth of " + ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT) + 
				" (" + ASTAngle.angleToStr(Az, ASTMisc.DECFORMAT) + " deg)");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Lat = " + ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + 
				", inclination = " + ASTAngle.angleToStr(inclin, ASTMisc.DMSFORMAT) + 
				" requires azimuth of " + ASTAngle.angleToStr(Az, ASTMisc.DMSFORMAT) +
				" (" + ASTAngle.angleToStr(Az, ASTMisc.DECFORMAT) + " deg)");
	}

	/**
	 * Calculate the launch velocity that a rocket must provide
	 * when launched from a given launch site.
	 * 
	 * @param calctype	VELOCITY_FROM_FILE or VELOCITY_FROM_INPUT
	 */
	protected void calcLaunchVelocity(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx = 0;
		ASTLatLon latObj;
		ASTReal rTmp;
		double dLat, dVelocity, deltaV;
		String tmpStr;

		if (calctype == ChapMenuItems.CalculationType.VELOCITY_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter Launch Site latitude",
					"Enter Desired Velocity (km/hr)") != ASTQuery.QUERY_OK) return;      
			latObj = ASTLatLon.isValidLat(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		} else {				// VELOCITY_FROM_FILE
			if (!ASTSites.areSitesLoaded()) {
				ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded");
				return;
			}
			if (ASTQuery.showQueryForm("Enter Name of Launch Site",
					"Enter Desired Velocity (km/hr)") != ASTQuery.QUERY_OK) return;
			idx = ASTSites.findSiteName(ASTQuery.getData1());
			if (idx < 0) {
				ASTMsg.errMsg("No Launch Site named '" + ASTQuery.getData1() + "' exists\n" + 
						"in the currently loaded launch sites database", "Invalid Site name");
				return;
			}
			latObj = ASTSites.getSiteLat(idx);
		}
		if (!latObj.isLatValid()) {
			ASTMsg.errMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude");
			return;
		}     
		dLat = latObj.getLat();
		if ((dLat < -85.5) || (dLat > 85.5)) {
			ASTMsg.errMsg("Launch site latitude is out of range [-85.5, 85.5] - try again", "Invalid Site Latitude");
			return;
		}

		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) dVelocity = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Velocity - try again", "Invalid Velocity");
			return;
		}
		if (dVelocity < 0.0) {
			ASTMsg.errMsg("Invalid Velocity - try again", "Invalid Velocity");
			return;
		}

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.VELOCITY_FROM_INPUT) {
			ChapGUI.printlnCond("Calculate the Velocity a rocket must provide when launched from " +
					ASTLatLon.latToStr(latObj, ASTMisc.DECFORMAT) + " degrees", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Calculate the Velocity a rocket must provide when launched from " +
					ASTSites.getSiteName(idx), ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("The selected launch site is at latitude " + dLat + " degrees (" + 
				ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + ")");
		ChapGUI.printlnCond("The required veclocity is " + 
				ASTStr.insertCommas(ASTMath.Round(dVelocity, 2)) + " km/hr (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dVelocity), 2)) + " mph)");
		ChapGUI.printlnCond();

		deltaV = 1669.81 * ASTMath.COS_D(dLat);
		ChapGUI.printlnCond("'Free' velocity = 1669.81*cos(Lat) = 1669.81*cos(" + 
				String.format(ASTStyle.genFloatFormat8,dLat) + ")");
		ChapGUI.printlnCond("                = " + ASTMath.Round(deltaV, 2) + " km/hr");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Rocket must provide difference in velocities");
		ChapGUI.printlnCond("deltaV = Vel - 'free' = " + 
				ASTStr.insertCommas(ASTMath.Round(dVelocity, 2)) + " - " + 
				ASTMath.Round(deltaV, 2));
		deltaV = dVelocity - deltaV;
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(ASTMath.Round(deltaV, 2)) + " km/hr");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("To achieve an orbital velocity of " + 
				ASTStr.insertCommas(ASTMath.Round(dVelocity, 2)) + " km/hr (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dVelocity), 2)) + 
				" mph) when launched");
		if (calctype == ChapMenuItems.CalculationType.VELOCITY_FROM_INPUT) {
			tmpStr = "from " + ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT);
		} else tmpStr = "from " + ASTSites.getSiteName(idx);
		ChapGUI.printlnCond(tmpStr + ", a rocket must supply a");
		ChapGUI.printlnCond("velocity of " + ASTStr.insertCommas(ASTMath.Round(deltaV, 2)) + " km/hr (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(deltaV), 2)) + " mph)");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Launch " + tmpStr + " requires " + 
				ASTStr.insertCommas(ASTMath.Round(deltaV, 2)) +
				" km/hr to achieve " + ASTStr.insertCommas(ASTMath.Round(dVelocity, 2)) + 
				" km/hr orbital velocity");
	}

	/**
	 * Display all of the currently loaded launch sites
	 */
	protected void displayAllLaunchSites() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTLatLon latObj;
		String name = null;

		if (!ASTSites.areSitesLoaded()) {
			ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded");
			return;
		}

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		prt.println("Currently Loaded Launch Sites", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.println("Launch Sites Data Filename: " + ASTSites.getSitesFilename());
		prt.println("Number of Launch Sites: " + ASTSites.getNumSites());
		prt.println();

		prt.setFixedWidthFont();

		for (int i = 0; i < ASTSites.getNumSites(); i++) {
			name = ASTSites.getSiteName(i);
			if (name == null) {
				prt.println("*** Invalid Name at Site " + (i+1));
				continue;
			}

			// We don't have to worry about the latitude's value
			// since getting the name validates that i is in range
			// and latitudes are validated when they are read from disk.
			latObj = ASTSites.getSiteLat(i);
			prt.println("Site " + (i + 1) + ": " + name + " at latitude " + latObj.getLat() +
					" degrees (" + ASTLatLon.latToStr(latObj, ASTMisc.DMSFORMAT) + ")");
		}

		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Load a Launch Sites data file
	 */
	protected void loadLaunchSites() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String fullFilename;

		ChapGUI.clearTextAreas();
		if (ASTSites.areSitesLoaded()) {
			if (ASTMsg.pleaseConfirm("Launch Sites have already been loaded. Are you sure\n" +
					"you want to load a new set of Launch Site Data?","Clear Launch Sites")) {
				ASTSites.clearLaunchSites();
				prt.println("The currently loaded Launch Sites were cleared ...");
			} else {
				prt.println("The currently loaded Launch Sites were not cleared ...");
				return;
			}			
		}

		fullFilename = ASTSites.getSitesFileToOpen();
		if ((fullFilename == null) || (fullFilename.length() <= 0)) return;

		if (ASTSites.loadSitesDB(fullFilename)) {
			prt.println(ASTSites.getNumSites() + " Launch Sites have been successfully loaded ...");
		} else {			
			ASTMsg.errMsg("Could not load Launch Sites data from " + fullFilename, "Launch Sites Data Load Failed");
		}
		prt.resetCursor();
	}

}
