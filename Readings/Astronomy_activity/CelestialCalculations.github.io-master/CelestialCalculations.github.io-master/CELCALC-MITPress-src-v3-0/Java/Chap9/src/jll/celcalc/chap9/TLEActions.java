package jll.celcalc.chap9;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTLE;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the TLE menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class TLEActions {

	/**
	 * Calculate length of the semi-major axis from the mean motion
	 * 
	 * @param calctype		AXIS_FROM_MMOTION_FROM_TLE or
	 * 						AXIS_FROM_MMOTION_FROM_INPUT
	 */
	protected void calcAxisFromMMotion(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx = 0;
		double MMotion = 0.0, axis;
		String strTmp;
		ASTReal rTmp;

		if (calctype == ChapMenuItems.CalculationType.AXIS_FROM_MMOTION_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter Mean Motion (revs/day)") != ASTQuery.QUERY_OK) return;
			rTmp = ASTReal.isValidReal(ASTQuery.getData1());
			if (rTmp.isValidRealObj()) MMotion = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid Mean Motion - try again", "Invalid Mean Motion");
				return;
			}
			if (MMotion < 0.0) {
				ASTMsg.errMsg("Invalid Mean Motion - try again", "Invalid Mean Motion");
				return;
			}
		} else {						// calculationtype.AXIS_FROM_MMOTION_FROM_TLE
			if (!ASTTLE.areTLEsLoaded()) {
				ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded");
				return;
			}
			idx = Misc.getValidTLENum();
			if (idx < 0) return;
		}

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.AXIS_FROM_MMOTION_FROM_INPUT) {
			ChapGUI.printlnCond("Calculate the Length of the Semi-Major Axis from Mean Motion", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Calculate the Length of the Semi-Major Axis from TLE Data Set " + (idx+1), ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.AXIS_FROM_MMOTION_FROM_INPUT) {
			ChapGUI.printlnCond("Mean Motion entered: " + String.format(ASTStyle.genFloatFormat8,MMotion)
					+ " revolutions per day");
		} else {
			if (ChapGUI.getShowInterimCalcsStatus()) {
				ASTTLE.displayTLEHeader();
				ASTTLE.displayTLEItem(idx);
				ChapGUI.printlnCond();
			}

			rTmp = ASTTLE.getTLEMeanMotion(idx);
			if (rTmp.isValidRealObj()) MMotion = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion");
				return;
			}
			ChapGUI.printlnCond("Mean Motion extracted from TLE Data Set: " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + " revolutions per day");
		}

		ChapGUI.printlnCond();

		axis = Misc.MeanMotion2Axis(MMotion, true);

		strTmp = "Mean Motion of " + String.format(ASTStyle.genFloatFormat8,MMotion) + " rev/day gives a = " +
				String.format(ASTStyle.genFloatFormat8,axis) + " km";

		ChapGUI.printlnCond("Thus, " + strTmp);

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(strTmp);
	}

	/**
	 * Calculate the mean motion from the length of the semi-major axis
	 */
	protected void calcMeanMotionFromAxis() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double MMotion, axis = 0.0;
		String result;
		ASTReal rTmp;

		if (ASTQuery.showQueryForm("Enter Length of Semi-Major Axis (in km)") != ASTQuery.QUERY_OK) return;

		rTmp = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Axis - try again", "Invalid Axis");
			return;
		}

		if (axis < 0.0) {
			ASTMsg.errMsg("Invalid Axis - try again", "Invalid Axis");
			return;
		}

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate Mean Motion from Length of the Semi-Major Axis", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("Axis entered: " + String.format(ASTStyle.genFloatFormat8,axis) + " km");
		ChapGUI.printlnCond();

		MMotion = Misc.Axis2MeanMotion(axis, true);
		ChapGUI.printlnCond();

		result = "a = " + String.format(ASTStyle.genFloatFormat8,axis) + " km gives Mean Motion = " +
				String.format(ASTStyle.genFloatFormat8,MMotion) + " rev/day";

		ChapGUI.printlnCond("Thus, " + result);

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(result);
	}

	/**
	 * Decode a TLE data set to show what its elements are. There may 
	 * be multiple data sets to display based on how the selection
	 * of what to display is done.
	 * 
	 * @param decodeBy		DECODE_TLE_BY_NAME, DECODE_TLE_BY_CATID,
	 * 						or DECODE_TLE_BY_SETNUM
	 */
	protected void decodeTLEData(ChapMenuItems.CalculationType decodeBy) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx = 0;
		String obj = "";
		
		if (!ASTTLE.areTLEsLoaded()) {
			ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded");
			return;
		}

		if (decodeBy == ChapMenuItems.CalculationType.DECODE_TLE_BY_CATID) {
			if (ASTQuery.showQueryForm("Enter Object's Catalog ID") != ASTQuery.QUERY_OK) return;
			obj = ASTQuery.getData1();
			obj = obj.trim();
			idx = ASTTLE.findTLECatID(obj, idx);
			if (idx < 0) {
				ASTMsg.errMsg("No Object with the Catalog ID '" + obj + "' was found", "Invalid Catalog ID");
				return;
			}
		} else if (decodeBy == ChapMenuItems.CalculationType.DECODE_TLE_BY_NAME) {
			if (ASTQuery.showQueryForm("Enter Object's Name (or partial name)") != ASTQuery.QUERY_OK) return;
			obj = ASTQuery.getData1();
			obj = obj.trim();
			idx = ASTTLE.findTLEName(obj, idx);
			if (idx < 0) {
				ASTMsg.errMsg("No Object with the name '" + obj + "' was found", "Invalid Object Name");
				return;
			}
		} else {            // DECODE_TLE_BY_SETNUM
			idx = Misc.getValidTLENum();
		}

		ChapGUI.clearTextAreas();

		if (decodeBy == ChapMenuItems.CalculationType.DECODE_TLE_BY_SETNUM) {
			ASTTLE.decodeTLE(idx, "TLE Data for Data Set " + (idx+1));
		} else {
			while (idx >= 0) {
				if (decodeBy == ChapMenuItems.CalculationType.DECODE_TLE_BY_CATID) {
					ASTTLE.decodeTLE(idx, "TLE Data for Catlog ID '" + obj + "', Data Set " + (idx+1));
					idx = ASTTLE.findTLECatID(obj, idx + 1);
				} else{
					ASTTLE.decodeTLE(idx, "TLE Data for Object '" + obj + "', Data Set " + (idx+1));
					idx = ASTTLE.findTLEName(obj, idx + 1);
				}
			}
		}

		prt.resetCursor();
	}

	/**
	 * Display all of the currently loaded TLE data.
	 */
	protected void displayRawTLEData() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTTLE.areTLEsLoaded()) {
			ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded");
			return;
		}

		int TLEdbsize = ASTTLE.getNumTLEDBObjs();

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		prt.println("Currently Loaded TLE Data Sets", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.println("TLE Data Filename: " + ASTTLE.getTLEFilename());
		prt.println("Number of TLE data sets: " + TLEdbsize);
		prt.println();

		prt.setFixedWidthFont();
		ASTTLE.displayTLEHeader();

		for (int i = 0; i < TLEdbsize; i++) {
			ASTTLE.displayTLEItem(i);
		}

		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Load a TLE data file
	 */
	protected void loadTLEData() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String fullFilename;

		ChapGUI.clearTextAreas();
		if (ASTTLE.areTLEsLoaded()) {
			if (ASTMsg.pleaseConfirm("TLEs have already been loaded. Are you sure\n" +
					"you want to load a new set of TLE Data?","Clear TLEs")) {
				ASTTLE.clearTLEData();
				prt.println("The currently loaded TLEs were cleared ...");
			} else {
				prt.println("The currently loaded TLEs were not cleared ...");
				return;
			}			
		}

		fullFilename = ASTTLE.getTLEFileToOpen();
		if ((fullFilename == null) || (fullFilename.length() <= 0)) return;

		if (ASTTLE.loadTLEDB(fullFilename)) {
			prt.println(ASTTLE.getNumTLEDBObjs() + " TLE data sets have been successfuly loaded ...");
		} else {			
			ASTMsg.errMsg("Could not load TLE data from " + fullFilename, "TLE Data Load Failed");
		}
		prt.resetCursor();
	}

	/**
	 * Extract Keplerian elements from a TLE data set
	 */
	protected void extractKeplerianFromTLE() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx;
		ASTInt iTmp;
		ASTReal rTmp;
		double inclin, ecc, axis, RAAN, argofperi, M0, MMotion;
		int daysIntoYear, iYear;
		ASTDate epochDate;
		double epochUT, dT;
		String line1,line2, strTmp;
		
		if (!ASTTLE.areTLEsLoaded()) {
			ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded");
			return;
		}

		idx = Misc.getValidTLENum();
		if (idx < 0) return;

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Extract Keplerian Elements from TLE Data Set " + (idx+1), ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		if (ChapGUI.getShowInterimCalcsStatus()) {
			ASTTLE.displayTLEHeader();
			ASTTLE.displayTLEItem(idx);
			ChapGUI.printlnCond();
		}

		line1 = ASTTLE.getTLELine1(idx);
		line2 = ASTTLE.getTLELine2(idx);

		ChapGUI.printlnCond("1.  Get the epoch year from columns 19-20 of TLE Line 1");
		ChapGUI.printlnCond("    Epoch year from TLE is " + line1.substring(18, 20));
		iTmp = ASTTLE.getTLEEpochYear(idx);
		if (iTmp.isValidIntObj()) iYear = iTmp.getIntValue();
		else {
			ASTMsg.errMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year");
			return;
		}
		ChapGUI.printlnCond("    Years < 57 are in the 2000s. Years > 56 are in the 1900s");
		if (iYear < 57) {
			iYear = iYear + 2000;
			ChapGUI.printlnCond("    Thus, Epoch Year = Year + 2000 = " + iYear);
		} else {
			iYear = iYear + 1900;
			ChapGUI.printlnCond("    Thus, Epoch Year = Year + 1900 = " + iYear);
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2.  Get the epoch day of year from columns 21-32 of TLE Line 1");
		ChapGUI.printlnCond("    Epoch day of year from TLE is " + line1.substring(20, 32));
		rTmp = ASTTLE.getTLEEpochDay(idx);
		if (rTmp.isValidRealObj()) dT = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year");
			return;
		}
		ChapGUI.printlnCond();

		daysIntoYear = ASTMath.Trunc(dT);
		ChapGUI.printlnCond("3.  Extract days into year as first 3 digits from " + line1.substring(20, 32));
		ChapGUI.printlnCond("    days into year = INT(" + line1.substring(20, 23) + ")");
		ChapGUI.printlnCond("                   = " + daysIntoYear);
		ChapGUI.printlnCond();

		epochDate = ASTDate.daysIntoYear2Date(iYear, daysIntoYear);
		ChapGUI.printlnCond("4.  Convert days into year to a calendar date for the year " + iYear);
		ChapGUI.printlnCond("    Epoch date = " + ASTDate.dateToStr(epochDate));
		ChapGUI.printlnCond();

		dT = dT - daysIntoYear;
		ChapGUI.printlnCond("5.  Extract fractional part of the day from " + line1.substring(20, 32));
		ChapGUI.printlnCond("    Frac Day = " + ASTMath.Round(dT, 8));
		ChapGUI.printlnCond();

		epochUT = ASTMath.Round(dT, 8) * 24.0;
		ChapGUI.printlnCond("6.  Convert fractional part of day to UT");
		ChapGUI.printlnCond("    UT = (Frac Day)*24 = " + ASTMath.Round(dT, 8) + "*24");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,epochUT) + 
				" = " + ASTTime.timeToStr(epochUT, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("7.  Get the orbital inclination from columns 9-16 of TLE Line 2");
		ChapGUI.printlnCond("    Inclination = " + line2.substring(8, 16).trim() + " degrees");
		rTmp = ASTTLE.getTLEInclination(idx);
		if (rTmp.isValidRealObj()) inclin = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Inclination in TLE Data Set", "Invalid Inclination");
			return;
		}
		ChapGUI.printlnCond("                = " + ASTAngle.angleToStr(inclin, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("8.  Get the RAAN from columns 18-25 of TLE Line 2");
		ChapGUI.printlnCond("    RAAN = " + line2.substring(17, 25) + " degrees");
		rTmp = ASTTLE.getTLERAAN(idx);
		if (rTmp.isValidRealObj()) RAAN = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid RAAN in TLE Data Set", "Invalid RAAN");
			return;
		}
		ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(RAAN, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  Get the orbital eccentricity from columns 27-33 of TLE Line 2");
		ChapGUI.printlnCond("    (A leading decimal point is assumed)");
		strTmp = "0." + line2.substring(26, 33).trim();
		rTmp = ASTTLE.getTLEEccentricity(idx);
		if (rTmp.isValidRealObj()) {
			ecc = rTmp.getRealValue();
			ChapGUI.printlnCond("    e = " + strTmp);
		} else {
			ASTMsg.errMsg("Invalid Eccentricity in TLE Data Set", "Invalid Eccentricity");
			return;
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. Get the argument of perigee from columns 35-42 of TLE Line 2");
		ChapGUI.printlnCond("    w = " + line2.substring(34, 42).trim() + " degrees");
		rTmp = ASTTLE.getTLEArgofPeri(idx);
		if (rTmp.isValidRealObj()) argofperi = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Argument of Perigee in TLE Data Set", "Invalid Arg of Perigee");
			return;
		}
		ChapGUI.printlnCond("      = " + ASTAngle.angleToStr(argofperi, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Get the mean anomaly from columns 44-51 of TLE Line 2");
		ChapGUI.printlnCond("    M0 = " + line2.substring(43, 51).trim() + " degrees");
		rTmp = ASTTLE.getTLEMeanAnomaly(idx);
		if (rTmp.isValidRealObj()) M0 = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Mean Anomaly in TLE Data Set", "Invalid Mean Anomaly");
			return;
		}
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(M0, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("12. Get the mean motion from columns 53-63 of TLE Line 2");
		ChapGUI.printlnCond("    Mean Motion = " + line2.substring(52, 63).trim() + " rev/day");
		rTmp = ASTTLE.getTLEMeanMotion(idx);
		if (rTmp.isValidRealObj()) MMotion = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion");
			return;
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("13. Convert the mean motion to length of the semi-major axis");
		axis = Misc.MeanMotion2Axis(MMotion, true);

		prt.println("Epoch Date " + ASTDate.dateToStr(epochDate) + " at " + 
				ASTTime.timeToStr(epochUT, ASTMisc.HMSFORMAT) + " UT");
		prt.println("Keplerian Elements are:");
		prt.println("  Inclination = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		prt.println("  Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
		prt.println("  Length of Semi-major axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		prt.println("  RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
		prt.println("  Argument of Perigee = " + String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
		prt.println("  Mean anomaly at the epoch = " + String.format(ASTStyle.genFloatFormat8,M0) + " degrees");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Keplerian Elements extracted from TLE Data Set " + (idx+1) +
				" are listed in the text area below");
	}

	/**
	 * Search the TLE database by catalog ID or name and display the
	 * raw data for that object. Note that the TLE database may have
	 * multiple entries with the same catalog ID/name. An exact match
	 * is required for catalog IDs, but a partial match is all that
	 * is required when done by name.
	 * 
	 * @param searchBy		SEARCH_TLE_BY_NAME or SEARCH_TLE_BY_CATID
	 */
	protected void searchForTLE(ChapMenuItems.CalculationType searchBy) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx = 0;
		String obj = "";

		if (!ASTTLE.areTLEsLoaded()) {
			ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded");
			return;
		}

		if (searchBy == ChapMenuItems.CalculationType.SEARCH_TLE_BY_CATID) {
			if (ASTQuery.showQueryForm("Enter Object's Catalog ID") != ASTQuery.QUERY_OK) return;
			obj = ASTQuery.getData1();
			obj = obj.trim();
			idx = ASTTLE.findTLECatID(obj, idx);
			if (idx < 0) {
				ASTMsg.errMsg("No Object with the catalog ID '" + obj + "' was found", "Invalid Catalog ID");
				return;
			}
		} else {
			if (ASTQuery.showQueryForm("Enter Object's Name (or partial name)") != ASTQuery.QUERY_OK) return;
			obj = ASTQuery.getData1();
			obj = obj.trim();
			idx = ASTTLE.findTLEName(obj, idx);
			if (idx < 0) {
				ASTMsg.errMsg("No Object with the name '" + obj + "' was found", "Invalid Object Name");
				return;
			}
		}

		ChapGUI.clearTextAreas();
		prt.setFixedWidthFont();
		ASTTLE.displayTLEHeader();

		while (idx >= 0) {
			ASTTLE.displayTLEItem(idx);
			if (searchBy == ChapMenuItems.CalculationType.SEARCH_TLE_BY_CATID) {
				idx = ASTTLE.findTLECatID(obj, idx + 1);
			} else idx = ASTTLE.findTLEName(obj, idx + 1);
		}

		prt.setProportionalFont();
		prt.resetCursor();
	}

}
