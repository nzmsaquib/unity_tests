package jll.celcalc.chap9;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <b>Implements an action listener for Radio buttons.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class RadioBtnsListener implements ActionListener {

	/**
	 * Handles clicks on individual radio buttons.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */	
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();

		if (action.equals(ChapGUI.getPSTCommand())) ChapGUI.setPSTRadBtn();
		else if (action.equals(ChapGUI.getMSTCommand())) ChapGUI.setMSTRadBtn();
		else if (action.equals(ChapGUI.getCSTCommand())) ChapGUI.setCSTRadBtn();
		else if (action.equals(ChapGUI.getESTCommand())) ChapGUI.setESTRadBtn();
		else if (action.equals(ChapGUI.getLongitudeCommand())) ChapGUI.setLonRadBtn();
		
		// Kepler's Equation radio buttons
		else if (action.equals(ChapGUI.getSimpleIterationCommand())) ChapGUI.setSimpleIterationRadBtn();
		else if (action.equals(ChapGUI.getNewtonMethodCommand())) ChapGUI.setNewtonMethodRadBtn();

	}
}
