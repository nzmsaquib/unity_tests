package jll.celcalc.chap9;

import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTLE;

/**
 * <b>Provides some miscellaneous helper methods used throughout the
 * Satellites chapter</b>
 * <p>
 * The methods and data here are static so that an object instance
 * does not have to be created before use the methods/data
 * in this class
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class Misc {

	/** Set a default termination criteria (in radians) for solving Kepler's equation */
	protected static double termCriteria = 0.000002;

	/**
	 * Convert length of the semi-major axis to mean motion
	 * 
	 * This method is overloaded.
	 * 
	 * @param axis			length of semi-major axis in km
	 * @return				mean motion in revs/day
	 */
	protected static double Axis2MeanMotion(double axis) {
		return Axis2MeanMotion(axis, false);
	}
	/**
	 * Convert length of the semi-major axis to mean motion
	 * 
	 * This method is overloaded.
	 * 
	 * @param axis			length of semi-major axis in km
	 * @param showInterim	show intermediate results if
	 * 						true and checkbox is true
	 * @return				mean motion in revs/day
	 */
	protected static double Axis2MeanMotion(double axis, boolean showInterim) {
		double N, MMotion;

		N = (1 / (2 * Math.PI * axis)) * Math.sqrt(ASTOrbits.muEarth / axis);
		if (showInterim) {
			ChapGUI.printlnCond("    Convert length of semi-major axis to rev/s");
			ChapGUI.printlnCond("    N = [1/(2*pi*a)]*sqrt(muEarth/a)");
			ChapGUI.printlnCond("      = [1/(2*pi*" + axis + ")]*sqrt(" + 
					ASTStr.insertCommas(ASTOrbits.muEarth) + "/" + axis + ")");
			ChapGUI.printlnCond("      = " + N + " rev/s");
			ChapGUI.printlnCond();
		}

		MMotion = N * 86400.0;
		if (showInterim) {
			ChapGUI.printlnCond("    Convert N from rev/s to rev/day");
			ChapGUI.printlnCond("    Mean Motion = 86,400*N = 86,400*" + N);
			ChapGUI.printlnCond("                = " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + " rev/day");
			ChapGUI.printlnCond();
		}

		return MMotion;
	}
	
	/**
	 * Ask user for a valid TLE data set number
	 * 
	 * @return			valid TLE data set number (0-based indexing!)
	 * 					or -1 if an error occurs
	 */
	protected static int getValidTLENum() {
		int idx = -1;
		ASTInt iTmp;

		if (!ASTTLE.areTLEsLoaded()) {
			ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded");
			return -1;
		}
		if (ASTQuery.showQueryForm("Enter desired TLE Data set\n" +
				"number (1-" + ASTTLE.getNumTLEDBObjs() + ")") != ASTQuery.QUERY_OK) return -1;

		iTmp = ASTInt.isValidInt(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (iTmp.isValidIntObj()) {
			idx = iTmp.getIntValue();
			if ((idx < 1) || (idx > ASTTLE.getNumTLEDBObjs())) {
				ASTMsg.errMsg("No TLE Data Set number " + idx + " exists", "Invalid TLE Data Set Number");
				return -1;
			}
			idx = idx - 1;		// adjust for 0-based indexing!
		} else {
			ASTMsg.errMsg("Invalid TLE data set number - try again", "Invalid TLE Data Set Number");
			return -1;
		}

		return idx;
	}

	/**
	 * Convert mean motion to length of the semi-major axis
	 * 
	 * This method is overloaded.
	 * 
	 * @param MMotion			mean motion in revs/day
	 * @return					length of the semi-major axis
	 */
	protected static double MeanMotion2Axis(double MMotion) {
		return MeanMotion2Axis(MMotion, false);
	}
	/**
	 * Convert mean motion to length of the semi-major axis
	 * 
	 * This method is overloaded.
	 * 
	 * @param MMotion			mean motion in revs/day
	 * @param showInterim		show intermediate results if
	 * 							true and checkbox is true
	 * @return					length of the semi-major axis
	 */
	protected static double MeanMotion2Axis(double MMotion, boolean showInterim) {
		double N, axis;

		N = MMotion / 86400.0;
		if (showInterim) {
			ChapGUI.printlnCond("    Convert mean motion from rev/day to rev/s");
			ChapGUI.printlnCond("    N = (Mean Motion)/86,400 = " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + "/86,400");
			ChapGUI.printlnCond("      = " + N + " rev/s");
			ChapGUI.printlnCond();
		}

		axis = 2.0 * Math.PI * N;
		axis = ASTOrbits.muEarth / (axis*axis);
		axis = ASTMath.cuberoot(axis);
		if (showInterim) {
			ChapGUI.printlnCond("    Calculate length of the semi-major axis");
			ChapGUI.printlnCond("    a = cuberoot[muEarth/((2*pi*N)^2)]");
			ChapGUI.printlnCond("      = cuberoot[" + ASTStr.insertCommas(ASTOrbits.muEarth) + 
					"/(2*pi*" + N + ")^2)]");
			ChapGUI.printlnCond("      = " + axis + " km");
			ChapGUI.printlnCond();
		}

		return axis;
	}

}
