package jll.celcalc.chap9;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStyle;

/**
 * <b>Provides a GUI for entering 6 coordinate elements.</b>
 * <p>
 * This is a modal window to force the user to explicitly click
 * either OK or Cancel before proceeding.
 * <p>
 * This About Box GUI was created by, and is maintained by, the 
 * Eclipse WindowBuilder. Some 'hand editing' was done to use the 
 * styles in ASTStyle to enforce a common GUI style across all programs.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
class Form6Coords extends JDialog {
	
	// save the parent frame from the routine that creates this dialog so that the
	// dialog can be centered on the parent
	private JFrame parentFrame = null;

	private final JPanel contentPanel = new JPanel();
	private JButton okButton;
	private JButton cancelButton;
	private JTextField txtData1 = new JTextField("100.56");
	private JLabel lblLabel1 = new JLabel("Label 1");
	private JTextField txtData2 = new JTextField("100.56");
	private JLabel lblLabel2 = new JLabel("Label 2");
	private JTextField txtData3 = new JTextField("100.56");
	private JLabel lblLabel3 = new JLabel("Label 3");
	private JTextField txtData4 = new JTextField("100.56");
	private JLabel lblLabel4 = new JLabel("Label 4");
	private JTextField txtData5 = new JTextField("100.56");
	private JLabel lblLabel5 = new JLabel("Label 5");
	private JTextField txtData6 = new JTextField("100.56");
	private JLabel lblLabel6 = new JLabel("Label 6");
	
	// showDialog will return dialogResult to indicate whether the user clicked
	// on OK or CANCEL
	private int dialogResult = ASTQuery.QUERY_CANCEL;

	/**
	 * Create the dialog.
	 */
	protected Form6Coords() {
		setModalityType(ModalityType.APPLICATION_MODAL);
		setType(Type.POPUP);
		setTitle("Enter data ...");
		setResizable(false);
		setBounds(100, 100, 390, 288);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		txtData1.setBounds(14, 14, 194, 23);

		txtData1.setHorizontalAlignment(SwingConstants.RIGHT);
		txtData1.setColumns(10);
		txtData1.setFont(ASTStyle.TEXT_FONT);
		lblLabel1.setBounds(226, 17, 138, 17);
		lblLabel1.setFont(ASTStyle.TEXT_BOLDFONT);
		contentPanel.setLayout(null);
		contentPanel.add(txtData1);
		contentPanel.add(lblLabel1);
		txtData2.setHorizontalAlignment(SwingConstants.RIGHT);
		txtData2.setFont(ASTStyle.TEXT_FONT);
		txtData2.setColumns(10);
		txtData2.setBounds(14, 43, 194, 23);
		contentPanel.add(txtData2);
		lblLabel2.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLabel2.setBounds(226, 46, 138, 17);
		contentPanel.add(lblLabel2);
		txtData3.setHorizontalAlignment(SwingConstants.RIGHT);
		txtData3.setFont(ASTStyle.TEXT_FONT);
		txtData3.setColumns(10);
		txtData3.setBounds(14, 74, 194, 23);
		contentPanel.add(txtData3);
		lblLabel3.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLabel3.setBounds(226, 77, 138, 17);
		contentPanel.add(lblLabel3);
		txtData4.setHorizontalAlignment(SwingConstants.RIGHT);
		txtData4.setFont(ASTStyle.TEXT_FONT);
		txtData4.setColumns(10);
		txtData4.setBounds(14, 103, 194, 23);
		contentPanel.add(txtData4);
		txtData5.setHorizontalAlignment(SwingConstants.RIGHT);
		txtData5.setFont(ASTStyle.TEXT_FONT);
		txtData5.setColumns(10);
		txtData5.setBounds(14, 132, 194, 23);
		contentPanel.add(txtData5);
		txtData6.setHorizontalAlignment(SwingConstants.RIGHT);
		txtData6.setFont(ASTStyle.TEXT_FONT);
		txtData6.setColumns(10);
		txtData6.setBounds(14, 163, 194, 23);
		contentPanel.add(txtData6);
		lblLabel6.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLabel6.setBounds(226, 166, 138, 17);
		contentPanel.add(lblLabel6);
		lblLabel5.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLabel5.setBounds(226, 135, 138, 17);
		contentPanel.add(lblLabel5);
		lblLabel4.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLabel4.setBounds(226, 106, 138, 17);
		contentPanel.add(lblLabel4);

		JPanel buttonPane = new JPanel();
		buttonPane.setBounds(0, 199, 384, 51);
		contentPanel.add(buttonPane);
		okButton = new JButton("OK");
		okButton.setActionCommand("OK");
		okButton.setFont(ASTStyle.BTN_FONT);
		getRootPane().setDefaultButton(okButton);
		okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dialogResult = ASTQuery.QUERY_OK;
				setVisible(false);
			}
		});
		
		cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		cancelButton.setFont(ASTStyle.BTN_FONT);
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dialogResult = ASTQuery.QUERY_CANCEL;
				setVisible(false);
			}
		});
		
		GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
		gl_buttonPane.setHorizontalGroup(
			gl_buttonPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_buttonPane.createSequentialGroup()
					.addGap(90)
					.addComponent(okButton)
					.addGap(59)
					.addComponent(cancelButton)
					.addContainerGap(89, Short.MAX_VALUE))
		);
		gl_buttonPane.setVerticalGroup(
			gl_buttonPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_buttonPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_buttonPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(okButton)
						.addComponent(cancelButton))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_buttonPane.linkSize(SwingConstants.HORIZONTAL, new Component[] {okButton, cancelButton});
		buttonPane.setLayout(gl_buttonPane);
	}
	
	/**
	 * Get 1st data item from the query form
	 * 
	 * @return		text in 1st data field
	 */
	protected String getData1() {
		return txtData1.getText();
	}
	/**
	 * Get 2nd data item from the query form
	 * 
	 * @return		text in 2nd data field
	 */
	protected String getData2() {
		return txtData2.getText();
	}
	/**
	 * Get 3rd data item from the query form
	 * 
	 * @return		text in 3rd data field
	 */
	protected String getData3() {
		return txtData3.getText();
	}
	/**
	 * Get 4th data item from the query form
	 * 
	 * @return		text in 4th data field
	 */
	protected String getData4() {
		return txtData4.getText();
	}
	/**
	 * Get 5th data item from the query form
	 * 
	 * @return		text in 5th data field
	 */
	protected String getData5() {
		return txtData5.getText();
	}
	/**
	 * Get 6th data item from the query form
	 * 
	 * @return		text in 6th data field
	 */
	protected String getData6() {
		return txtData6.getText();
	}
	
	/**
	 * Saves a reference to the caller's parent frame so the dialog can be centered on the 
	 * parent application's window.
	 * 
	 * @param parent		parent frame for the main application
	 */
	protected void setParentFrame(JFrame parent) {
		parentFrame = parent;
	}
	
	/**
	 * Shows the dialog
	 * 
	 * @param title		title for the window
	 * @param s1		string to display for the label 1
 	 * @param s2		string to display for the label 2
 	 * @param s3		string to display for the label 3
 	 * @param s4		string to display for the label 4
 	 * @param s5		string to display for the label 5
 	 * @param s6		string to display for the label 6
 	 * @return			whether user clicked on OK or CANCEL		
	 */
	protected int showDialog(String title, String s1, String s2, String s3, String s4, String s5, String s6) {
		// Assume user will cancel - this is in case the user clicks on the "X" to
		// close the window rather than clicking one of the buttons
		dialogResult = ASTQuery.QUERY_CANCEL;
		
		txtData1.setText(""); lblLabel1.setText(s1);
		txtData2.setText(""); lblLabel2.setText(s2);
		txtData3.setText(""); lblLabel3.setText(s3);
		txtData4.setText(""); lblLabel4.setText(s4);
		txtData5.setText(""); lblLabel5.setText(s5);
		txtData6.setText(""); lblLabel6.setText(s6);
		this.setTitle(title);
		this.setLocationRelativeTo(parentFrame);
		this.setVisible(true);
		return dialogResult;
	}
	
}
