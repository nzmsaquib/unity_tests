package jll.celcalc.chap9;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTVect;

/**
 * Allows a user to enter orbital elements and then validate
 * the user's input. Static methods are used so that
 * the user does not have to create an object instance
 * before using the methods in this class.
 * 
 * This method puts up a modal dialog, thus requiring
 * the user to provide input, or cancel, before proceeding.
 * User will either enter Keplerian elements or a state vector.
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class VDator {

	// Define instance variables to hold Keplerian elements and two state vectors.
	private static double inclin = 0.0;							// orbital inclination
	private static double ecc = 0.0;							// orbital eccentricity
	private static double axis = 0.0;							// length of the orbital semi-major axis
	private static double RAAN = 0.0;							// right ascension of the ascending node
	private static double argofperi = 0.0;						// argument of perigee
	private static double M0 = 0.0;								// mean anomaly at the epoch
	private static ASTVect R = new ASTVect(0.0,0.0,0.0);		// positional vector
	private static ASTVect V = new ASTVect(0.0,0.0,0.0);		// velocity vector

	/*=============== Handle Keplerian elements ===============*/

	/**
	 * Display a form to get Keplerian elements and validate the
	 * data values the user enters
	 * 
	 * User will enter inclination, eccentricity, axis,
	 * RAAN, argument of perigee, and M0.
	 * 
	 * @return		true if user enters valid Keplerian
	 * 				elements and clicks on OK. Otherwise,
	 * 				return false. The calling routine
	 * 				must invoke  the appropriate 'get'
	 * 				to get the actual data that the user
	 * 				entered.
	 */
	protected static boolean validateKeplerianElements() {
		Form6Coords form = ChapGUI.getCoordQueryForm();
		ASTAngle angleObj;
		ASTReal rTmp;

		inclin = 0.0;
		ecc = 0.0;
		axis = 0.0;
		RAAN = 0.0;
		argofperi = 0.0;
		M0 = 0.0;

		// Be sure this is a modal window
		form.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		if (form.showDialog("Enter Keplerian Elements ...","inclination","eccentricity",
				"axis","RAAN","Arg of Perigee","M0") != ASTQuery.QUERY_OK) return false;

		angleObj = ASTAngle.isValidAngle(form.getData1(),ASTMisc.HIDE_ERRORS);
		if (angleObj.isValidAngleObj()) inclin = angleObj.getDecAngle();
		else {
			ASTMsg.errMsg("Invalid inclination - try again", "Invalid Inclination");
			return false;
		}

		rTmp = ASTReal.isValidReal(form.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) ecc = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
			return false;
		}
		if ((ecc < 0.0) || (ecc > 1.0)) {
			ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
			return false;
		}

		rTmp = ASTReal.isValidReal(form.getData3(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis");
			return false;
		}

		rTmp = ASTReal.isValidReal(form.getData4(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) RAAN = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid RAAN - try again", "Invalid RAAN");
			return false;
		}

		angleObj = ASTAngle.isValidAngle(form.getData5(),ASTMisc.HIDE_ERRORS);
		if (angleObj.isValidAngleObj()) argofperi = angleObj.getDecAngle();
		else {
			ASTMsg.errMsg("Invalid Argument of Perigee - try again", "Invalid Arg of Peri");
			return false;
		}

		angleObj = ASTAngle.isValidAngle(form.getData6(),ASTMisc.HIDE_ERRORS);
		if (angleObj.isValidAngleObj()) M0 = angleObj.getDecAngle();
		else {
			ASTMsg.errMsg("Invalid Mean Anomaly - try again", "Invalid Mean Anomaly");
			return false;
		}

		return true;
	}

	/*====================================================================
	 * Define 'getters' for the Keplerian elements that the user entered
	 *===================================================================*/
	
	/**
	 * Gets the orbital inclination.
	 * 
	 * @return		orbital inclination
	 */
	protected static double getInclination() {
		return inclin;
	}
	/**
	 * Gets the orbital eccentricity.
	 * 
	 * @return		orbital eccentricity
	 */
	protected static double getEccentricity() {
		return ecc;
	}
	/**
	 * Gets the length of the orbital semi-major axis.
	 * 
	 * @return		length of semi-major axis
	 */
	protected static double getAxisLength() {
		return axis;
	}
	/**
	 * Gets the right ascension of the ascending node.
	 * 
	 * @return		RAAN
	 */
	protected static double getRAAN() {
		return RAAN;
	}
	/**
	 * Gets the argument of perigee.
	 * 
	 * @return		argument of perigee
	 */
	protected static double getArgOfPeri() {
		return argofperi;
	}
	/**
	 * Gets the orbital mean anomaly.
	 * 
	 * @return		mean anomaly
	 */
	protected static double getMeanAnomaly() {
		return M0;
	}

	/*======================= Handle state vectors =======================*/

	/**
	 * Display a form to get a state vector and validate the
	 * data values the user enters.
	 * 
	 * User will enter a state vector consisting of a 
	 * positional vector (R) and a velocity vector (V).
	 * 
	 * @return		true if user enters a valid state
	 * 				vector and clicks on OK. Otherwise,
	 * 				return false. The calling routine
	 * 				must invoke the appropriate 'get'
	 * 				to get the actual data that the user
	 * 				entered.
	 */
	public static boolean validateStateVect() {
		Form6Coords form = ChapGUI.getCoordQueryForm();
		double x,y,z;
		ASTReal rTmp;

		R.setVect(0.0, 0.0, 0.0);
		V.setVect(0.0, 0.0, 0.0);

		// Be sure this is a modal window
		form.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
		if (form.showDialog("Enter State Vector components ...","Rx","Ry","Rz",
				"Vx","Vy","Vz") != ASTQuery.QUERY_OK) return false;

		// Validate the positional (R) vector
		rTmp = ASTReal.isValidReal(form.getData1());
		if (rTmp.isValidRealObj()) x = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid positional Rx value - try again", "Invalid Rx");
			return false;
		}
		rTmp = ASTReal.isValidReal(form.getData2());
		if (rTmp.isValidRealObj()) y = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid positional Ry value - try again", "Invalid Ry");
			return false;
		}
		rTmp = ASTReal.isValidReal(form.getData3());
		if (rTmp.isValidRealObj()) z = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid positional Rz value - try again", "Invalid Rz");
			return false;
		}
		R.setVect(x, y, z);

		// Validate the velocity (V) vector
		rTmp = ASTReal.isValidReal(form.getData4());
		if (rTmp.isValidRealObj()) x = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid positional Vx value - try again", "Invalid Vx");
			return false;
		}
		rTmp = ASTReal.isValidReal(form.getData5());
		if (rTmp.isValidRealObj()) y = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid positional Vy value - try again", "Invalid Vy");
			return false;
		}
		rTmp = ASTReal.isValidReal(form.getData6());
		if (rTmp.isValidRealObj()) z = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid positional Vz value - try again", "Invalid Vz");
			return false;
		}
		V.setVect(x, y, z);

		return true;
	}

	/* Define 'getters' for the state vector components */
	/**
	 * Gets the state vector's positional vector.
	 * 
	 * @return		positional vector
	 */
	protected static ASTVect getPosVect() {
		return R;
	}
	/**
	 * Gets the state vector's velocity vector.
	 * 
	 * @return		velocity vector
	 */
	protected static ASTVect getVelVect() {
		return V;
	}

}
