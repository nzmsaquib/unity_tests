package jll.celcalc.chap9;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
* This class extends JMenuItem to add some additional fields, which
* simplifies the code for adding items to the menu bar and determining
* what actions to take as various menu items are selected. 
* <p>
* Copyright (c) 2018
* 
* @author J. L. Lawrence
* @version 3.0, 2018
*/

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {
	
	/** Define values for the eCalcType field in a ChapMenuItem class. 
	 * These indicate what menu actions to take. */
	
	protected enum CalculationType {
		// Coord Sys menu
		/** Convert cartesian to spherical coordinates */ CART_TO_SPHERICAL,
		/** Convert spherical to cartesian coordinates */ SPHERICAL_TO_CART,
		/** Convert ECI to Horizon coordinates */ ECI_TO_HORIZON,
		/** Convert ECI to Topocentric coordinates */ ECI_TO_TOPO,
		/** Convert state vector to Keplerian elements */ STATEVECTOR_TO_KEPLERIAN,
		/** Convert Keplerian elements to state vector */ KEPLERIAN_TO_STATEVECTOR,
		
		// TLEs menu
		/** Load TLE Data */ LOAD_TLE_DATA,
		/** Display Raw TLE Data */ DISPLAY_RAW_TLE_DATA,
		/** Find TLE by object name */ SEARCH_TLE_BY_NAME,
		/** Find TLE by Catalog ID */ SEARCH_TLE_BY_CATID,
		/** Decode TLE data by Object Name */ DECODE_TLE_BY_NAME,
		/** Decode TLE data by Catalog ID */ DECODE_TLE_BY_CATID,
		/** Decode TLE data by Data Set Number */ DECODE_TLE_BY_SETNUM,
		/** Convert mean motion to semi-major axis via TLE data set */ AXIS_FROM_MMOTION_FROM_TLE,
		/** Convert mean motion to semi-major axis via User Input */ AXIS_FROM_MMOTION_FROM_INPUT,
		/** Convert semi-major axis to mean motion */ MEAN_MOTION_FROM_AXIS,
		/** Extract Keplerian Elements from TLE */ KEPLERIAN_FROM_TLE,
		
		// Launch Sites menu
		/** Load Launch Sites */ LOAD_LAUNCH_SITES,
		/** Display all launch sites */ DISPLAY_ALL_LAUNCH_SITES,
		/** Calculate orbital inclination from sites file */ INCLINATION_FROM_FILE,
		/** Calculate orbital inclination from user input */ INCLINATION_FROM_INPUT,
		/** Calculate launch azimuth from sites file */ LAUNCH_AZIMUTH_FROM_FILE,
		/** Calculate launch azimuth from user input */ LAUNCH_AZIMUTH_FROM_INPUT,
		/** Calculate launch velocity from sites file */ VELOCITY_FROM_FILE,
		/** Calculate launch velocity from user input */ VELOCITY_FROM_INPUT,
		
		// Satellite menu
		/** Locate satellite from TLE data set */ LOCATE_SAT_FROM_TLE,
		/** Locate satellite from user input */ LOCATE_SAT_FROM_INPUT,
		/** Calculate rise/set times from TLE data set */ RISE_SET_FROM_TLE,
		/** Calculate rise/set times from user input */ RISE_SET_FROM_INPUT,
		/** Calculate satellite distance from TLE data set */ SAT_DISTANCE_FROM_TLE,
		/** Calculate satellite distance from user input */ SAT_DISTANCE_FROM_INPUT,
		/** Calculate perigee/apogee from TLE data set */ SAT_PERI_APOGEE_FROM_TLE,
		/** Calculate perigee/apogee from user input */ SAT_PERI_APOGEE_FROM_INPUT,
		/** Calculate orbital period from TLE data set */ SAT_PERIOD_FROM_TLE,
		/** Calculate orbital period from user input */ SAT_PERIOD_FROM_INPUT,
		/** Use TLE data set to calculate axis from orbital period */ AXIS_FROM_PERIOD_FROM_TLE,
		/** Use user input to calculate axis from orbital period */ AXIS_FROM_PERIOD_FROM_INPUT,
		/** Calculate orbital velocity for a circular orbit */ SAT_VELOCITY_CIRCULAR,
		/** Calculate orbital velocity for an elliptical orbit */ SAT_VELOCITY_ELLIPTICAL,
		/** Calculate satellite footprint */ SAT_FOOTPRINT,
		/** Set termination criteria for solving Kepler's equation */ TERM_CRITERIA
	}
		
	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Solar Info and Orbital Elements menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;

		// Create Coord Sys menu items
		menu = ChapGUI.getCoordSysMenu();
		createMenuItem(menu,"Cartesian to Spherical", CalculationType.CART_TO_SPHERICAL);
		createMenuItem(menu,"Spherical to Cartesian", CalculationType.SPHERICAL_TO_CART);
		menu.addSeparator();
		createMenuItem(menu,"ECI to Horizon Coordinates", CalculationType.ECI_TO_HORIZON);
		createMenuItem(menu,"ECI to Topocentric Coordinates", CalculationType.ECI_TO_TOPO);
		menu.addSeparator();
		createMenuItem(menu,"State Vector to Keplerian Elements", CalculationType.STATEVECTOR_TO_KEPLERIAN);
		createMenuItem(menu,"Keplerian Elements to State Vector", CalculationType.KEPLERIAN_TO_STATEVECTOR);

		// Create TLEs menu items
		menu = ChapGUI.getTLEsMenu();		
		JMenu tmpSubMenu;
		createMenuItem(menu,"Load TLE Data", CalculationType.LOAD_TLE_DATA);
		createMenuItem(menu,"Display Raw TLE Data", CalculationType.DISPLAY_RAW_TLE_DATA);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Find TLE ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"By Object Name", CalculationType.SEARCH_TLE_BY_NAME);
		createMenuItem(tmpSubMenu,"By Catalog ID", CalculationType.SEARCH_TLE_BY_CATID);
		tmpSubMenu = new JMenu("Decode TLE Data ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"By Object Name", CalculationType.DECODE_TLE_BY_NAME);
		createMenuItem(tmpSubMenu,"By Catalog ID", CalculationType.DECODE_TLE_BY_CATID);
		createMenuItem(tmpSubMenu,"By Data Set Number", CalculationType.DECODE_TLE_BY_SETNUM);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Mean Motion to Semi-Major Axis ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.AXIS_FROM_MMOTION_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.AXIS_FROM_MMOTION_FROM_INPUT);
		createMenuItem(menu,"Semi-Major Axis to Mean Motion", CalculationType.MEAN_MOTION_FROM_AXIS);
		createMenuItem(menu,"Extract Keplerian Elements from TLE", CalculationType.KEPLERIAN_FROM_TLE);
		
		// Launch Sites menu
		menu = ChapGUI.getLaunchSitesMenu();	
		createMenuItem(menu,"Load Launch Sites", CalculationType.LOAD_LAUNCH_SITES);
		createMenuItem(menu,"Display All Launch Sites", CalculationType.DISPLAY_ALL_LAUNCH_SITES);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Calculate Orbital Inclination ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From Sites File", CalculationType.INCLINATION_FROM_FILE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.INCLINATION_FROM_INPUT);
		tmpSubMenu = new JMenu("Calculate Launch Azimuth ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From Sites File", CalculationType.LAUNCH_AZIMUTH_FROM_FILE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.LAUNCH_AZIMUTH_FROM_INPUT);
		tmpSubMenu = new JMenu("Calculate Launch Velocity ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From Sites File", CalculationType.VELOCITY_FROM_FILE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.VELOCITY_FROM_INPUT);

		// Satellites menu
		menu = ChapGUI.getSatellitesMenu();
		tmpSubMenu = new JMenu("Locate Satellite ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.LOCATE_SAT_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.LOCATE_SAT_FROM_INPUT);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Rise/Set Times ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.RISE_SET_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.RISE_SET_FROM_INPUT);
		tmpSubMenu = new JMenu("Satellite Distance ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.SAT_DISTANCE_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.SAT_DISTANCE_FROM_INPUT);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Perigee/Apogee ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.SAT_PERI_APOGEE_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.SAT_PERI_APOGEE_FROM_INPUT);
		tmpSubMenu = new JMenu("Orbital Period ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.SAT_PERIOD_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.SAT_PERIOD_FROM_INPUT);
		tmpSubMenu = new JMenu("Axis from Orbital Period ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"From TLE Data Set", CalculationType.AXIS_FROM_PERIOD_FROM_TLE);
		createMenuItem(tmpSubMenu,"From User Input", CalculationType.AXIS_FROM_PERIOD_FROM_INPUT);
		tmpSubMenu = new JMenu("Orbital Velocity ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"For Circular Orbit", CalculationType.SAT_VELOCITY_CIRCULAR);
		createMenuItem(tmpSubMenu,"For Elliptical Orbit", CalculationType.SAT_VELOCITY_ELLIPTICAL);
		menu.addSeparator();
		createMenuItem(menu,"Satellite Footprint", CalculationType.SAT_FOOTPRINT);
		menu.addSeparator();
		createMenuItem(menu,"Set Termination Criteria", CalculationType.TERM_CRITERIA);
	}
		
	/** Define a class for what a  menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of calculation to do
		private CalculationType eCalcType;   // what type of calculation to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of calculation is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {
			super(title);
			eCalcType = calcType;
		}
		
		/*========================================================
		 * Define accessor methods for the menu item fields
		 *=======================================================*/
		
		/**
		 * Gets the type of calculation to perform.
		 * 
		 * @return		the type of calculation to perform
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}
	
	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 -----------------------------------------------------------------------------*/

	/**
	 * Creates a menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of calculation to perform
	 * @return					ChapMnuItm created
	 */     
	private ChapMnuItm createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getChapMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());
		
		return tmp;
	}

}

