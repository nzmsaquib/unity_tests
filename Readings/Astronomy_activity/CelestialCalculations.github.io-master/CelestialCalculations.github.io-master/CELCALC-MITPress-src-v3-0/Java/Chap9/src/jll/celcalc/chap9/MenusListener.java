package jll.celcalc.chap9;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap9.ChapMenuItems.ChapMnuItm;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class MenusListener implements ActionListener {

	private static CoordSysActions cs_actions = new CoordSysActions();
	private static TLEActions tle_actions = new TLEActions();
	private static LaunchSitesActions ls_actions = new LaunchSitesActions();
	private static SatelliteActions sat_actions = new SatelliteActions();

	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();
		ChapMenuItems.CalculationType calctype;

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}

		// Handle Coord Systems, TLEs, Launch Sites, and Satellites menus
		else if (action.equals(ChapGUI.getChapMenuCommand())) {
			ChapMnuItm objMenuItem = (ChapMnuItm) e.getSource();		// the menu item the user clicked on
			calctype = objMenuItem.getCalcType();

			switch (calctype) {
			// ********************* Coord Sys menu
			case CART_TO_SPHERICAL:
				cs_actions.calcCartesian2Spherical();
				break;
			case SPHERICAL_TO_CART:
				cs_actions.calcSpherical2Cartesian();
				break;
			case ECI_TO_HORIZON:
				cs_actions.calcECI2Horizon();
				break;
			case ECI_TO_TOPO:
				cs_actions.calcECI2Topo();
				break;
			case STATEVECTOR_TO_KEPLERIAN:
				cs_actions.calcState2Keplerian();
				break;
			case KEPLERIAN_TO_STATEVECTOR:
				cs_actions.calcKeplerian2State();
				break;

				// ********************* TLEs menu
			case LOAD_TLE_DATA:
				tle_actions.loadTLEData();
				break;
			case DISPLAY_RAW_TLE_DATA:
				tle_actions.displayRawTLEData();
				break;
			case SEARCH_TLE_BY_NAME:
				tle_actions.searchForTLE(calctype);
				break;
			case SEARCH_TLE_BY_CATID:
				tle_actions.searchForTLE(calctype);
				break;
			case DECODE_TLE_BY_NAME:
				tle_actions.decodeTLEData(calctype);
				break;
			case DECODE_TLE_BY_CATID:
				tle_actions.decodeTLEData(calctype);
				break;
			case DECODE_TLE_BY_SETNUM:
				tle_actions.decodeTLEData(calctype);
				break;
			case AXIS_FROM_MMOTION_FROM_TLE:
				tle_actions.calcAxisFromMMotion(calctype);
				break;
			case AXIS_FROM_MMOTION_FROM_INPUT:
				tle_actions.calcAxisFromMMotion(calctype);
				break;
			case MEAN_MOTION_FROM_AXIS:
				tle_actions.calcMeanMotionFromAxis();
				break;
			case KEPLERIAN_FROM_TLE:
				tle_actions.extractKeplerianFromTLE();
				break;

				// ********************* Launch Sites menu
			case LOAD_LAUNCH_SITES:
				ls_actions.loadLaunchSites();
				break;
			case DISPLAY_ALL_LAUNCH_SITES:
				ls_actions.displayAllLaunchSites();
				break;
			case INCLINATION_FROM_FILE:
				ls_actions.calcInclination(calctype);
				break;
			case INCLINATION_FROM_INPUT:
				ls_actions.calcInclination(calctype);
				break;
			case LAUNCH_AZIMUTH_FROM_FILE:
				ls_actions.calcLaunchAzimuth(calctype);
				break;
			case LAUNCH_AZIMUTH_FROM_INPUT:
				ls_actions.calcLaunchAzimuth(calctype);
				break;
			case VELOCITY_FROM_FILE:
				ls_actions.calcLaunchVelocity(calctype);
				break;
			case VELOCITY_FROM_INPUT:
				ls_actions.calcLaunchVelocity(calctype);
				break;

				// ********************* Satellites menu
			case LOCATE_SAT_FROM_TLE:
				sat_actions.calcSatLocation(calctype);
				break;
			case LOCATE_SAT_FROM_INPUT:
				sat_actions.calcSatLocation(calctype);
				break;
			case RISE_SET_FROM_TLE:
				sat_actions.calcSatRiseSet(calctype);
				break;
			case RISE_SET_FROM_INPUT:
				sat_actions.calcSatRiseSet(calctype);
				break;
			case SAT_DISTANCE_FROM_TLE:
				sat_actions.calcSatDistance(calctype);
				break;
			case SAT_DISTANCE_FROM_INPUT:
				sat_actions.calcSatDistance(calctype);
				break;
			case SAT_PERI_APOGEE_FROM_TLE:
				sat_actions.calcSatPerigeeApogee(calctype);
				break;
			case SAT_PERI_APOGEE_FROM_INPUT:
				sat_actions.calcSatPerigeeApogee(calctype);
				break;
			case SAT_PERIOD_FROM_TLE:
				sat_actions.calcSatOrbitalPeriod(calctype);
				break;
			case SAT_PERIOD_FROM_INPUT:
				sat_actions.calcSatOrbitalPeriod(calctype);
				break;
			case AXIS_FROM_PERIOD_FROM_TLE:
				sat_actions.calcAxisFromPeriod(calctype);
				break;
			case AXIS_FROM_PERIOD_FROM_INPUT:
				sat_actions.calcAxisFromPeriod(calctype);
				break;
			case SAT_VELOCITY_CIRCULAR:
				sat_actions.calcSatVelocityCircular();
				break;
			case SAT_VELOCITY_ELLIPTICAL:
				sat_actions.calcSatVelocityElliptical();
				break;
			case SAT_FOOTPRINT:
				sat_actions.calcSatFootprint();
				break;
			case TERM_CRITERIA:
				sat_actions.setTerminationCriteria();
				break;

			default:
				ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item from the\n" +
						"'Coord Sys,' 'TLEs,' 'Launch Sites,' or 'Satellites' menu and try again.", "No Menu Item Selected");
				break;
			}
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			ChapGUI.clearTextAreas();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
					"be used in subsequent calculations. You may find it convenient to enter an initial latitude, longitude, " +
					"and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
					"longitude, and time zone are already filled in with default values when the program starts. When this program " +
					"begins, the date and time will default to the local date and time at which the program is started.");
			prt.println();
			prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
					"various calculations are performed. Also, select the appropriate radio button to choose what method to " +
					"use when it is necessary to determine an object's true anomaly. The radio button " +
					"'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " +
					"Newton/Raphson method. The menu entry 'Satellites->Set Termination Critera' allows you to enter the " +
					"termination criteria (in radians) for when to stop iterating to solve Kepler's equation.");
			prt.println();

			prt.println("The menu 'Coord Sys' performs various coordinate system conversions related to satellites, such as converting " +
					"Keplerian elements to/from a state vector. The menu 'TLEs' allows TLE data files to be loaded and examined. There are " +
					"also menu entries that decode a TLE data set and convert between Mean Motion and the length of the semi-major axis. " +
					"The 'Launch Sites' menu provides the ability to load a collection of launch sites and their latitude for performing " +
					"various calculations about launching rockets where the choice of the launch site is important.");
			prt.println();

			prt.println("The 'Satellites' menu uses the algorithms from the 'Coord Sys' and 'TLEs' menus to provide information about satellite " +
					"orbits. Since only a very simple propagator is implemented, the results will not be very accurate if the time at which " +
					"the information is calculated is more than a few hours after the epoch at which the Keplerian elements were captured. The " +
					"'Satellites->Rise/Set Times' produces a table of a satellite's location at various times. In addition to the observer's " +
					"location and LCT, the algorithm asks for the number of hours after the LCT an estimation is desired and a time increment. " +
					"The algorithm then estimates the satellite's location for the observer's LCT, increments the LCT by the time increment " +
					"entered, recomputes the esimated satellite location for that new time, and continues the process until the requsted " +
					"number of hours have been reached. Within the time interval generated by this method, the algorithm estimates when a " +
					"satellite will rise or set. For each 'Satellites' menu entry, the required Keplerian elements can be either entered " +
					"directly from the keyboard or extracted from a TLE data set that the user selects. When time must be entered, " +
					"such as when entering an object's orbital period, you must enter decimal minutes if the period " +
					"is greater than 59 minutes because in HMS format, hours must be less than 24, minutes must be less than 60, and " +
					"seconds must be less than 60.");
			prt.println();

			prt.setBoldFont(true);
			prt.println("**Important Notes**", ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();
			prt.println("In the context of this program, the word 'satellite' is used rather loosely to mean any manmade object that is " +
					"orbiting the Earth (satellite, rocket body, space station, space telescope, etc.), and not as literally " +
					"meaning only a satellite. The various calculations performed by this program will work for any object " +
					"orbiting the Earth whose Keplerian elements are known.");
			prt.println();

			prt.println("This program allows Keplerian elements to be entered into the program in multiple ways. They may be entered " +
					"directly from the keyboard, or they may be read from a TLE data file. When read from a TLE data file, you may generally " +
					"specify which object is of interest in one of up to three different ways: (1) by object name, (2) by catalog ID, or " +
					"(3) by TLE data set. (1) When an object name is specified, a partial match is all that is required and the search " +
					"is not case sensitive. Thus, a search for an object named 'is' or 'IS' will find every object in the currently loaded " +
					"TLE data file that has 'IS' in its name. Searching by object name requires that object names actually exist in the " +
					"TLE data file (names are optional), and such a search may return multiple entries. (2) A catalog ID is a required " +
					"field in the TLE format. The catalog ID is a 5 digit number and all 5 digits must be entered for the search. As with " +
					"searching by object name, searching by catalog ID may return multiple results because the TLE data file could contain " +
					"several entries (but usually at different times) for the same object. (3) Searching by TLE data set number means to retrieve " +
					"a specific TLE data set from the currently loaded TLE data file. For example, if the currently loaded data file has " +
					"14 sets of TLE data lines, then searching for data set number 4 will retrieve the 4th data set in the TLE data file.");

			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
