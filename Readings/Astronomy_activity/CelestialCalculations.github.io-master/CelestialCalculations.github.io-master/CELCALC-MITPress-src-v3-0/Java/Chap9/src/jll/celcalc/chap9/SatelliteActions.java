package jll.celcalc.chap9;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTLE;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Satellites menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class SatelliteActions {

	/**
	 * Calculate a satellite's semi-major axis from its
	 * orbital period
	 * 
	 * @param calctype			AXIS_FROM_PERIOD_FROM_TLE or
	 * 							AXIS_FROM_PERIOD_FROM_INPUT
	 */
	protected void calcAxisFromPeriod(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx;
		double axis, MMotion, period;
		String tmpStr;
		ASTReal rTmp;

		// Although these are calculated below, initialize
		// them to avoid compiler warnings.
		axis = 0.0;
		MMotion = 0.0;
		period = 0.0;

		if (calctype == ChapMenuItems.CalculationType.AXIS_FROM_PERIOD_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter orbital period\n" +
					"(in decimal minutes)") != ASTQuery.QUERY_OK) return;
			rTmp = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) period = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid orbital period - try again", "Invalid Orbital Period");
				return;
			}
			if (period < 0.0) {
				ASTMsg.errMsg("Invalid orbital period - try again", "Invalid Orbital Period");
				return;
			}
			tmpStr = "manually entered data";
		} else {
			idx = Misc.getValidTLENum();
			if (idx < 0) return;

			if (ASTTLE.getKeplerianElementsFromTLE(idx)) {
				// The getKeplerianElementsFromTLE validates that the Keplerian
				// elements are valid, so we don't need to check the 'get' results
				rTmp = ASTTLE.getTLEMeanMotion(idx);
				MMotion = rTmp.getRealValue();
			} else {
				ASTMsg.errMsg("The Keplerian elements for TLE data set " + (idx+1) + 
						"\nappear to be invalid - try another set", "Invalid Keplerian elements");
				return;
			}
			axis = Misc.MeanMotion2Axis(MMotion);
			tmpStr = "TLE data set " + (idx+1);
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Use " + tmpStr + " to calculate the Semi-Major Axis from the Orbital Period", 
				ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.AXIS_FROM_PERIOD_FROM_INPUT) {
			ChapGUI.printlnCond("Orbital Period as entered: " + period + " minutes");
		} else {
			ChapGUI.printlnCond("Mean Motion from TLE data: " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
			ChapGUI.printlnCond();

			period = 1440.0 / MMotion;
			ChapGUI.printlnCond("Convert Mean Motion to orbital period");
			ChapGUI.printlnCond("    tau = 1440/(Mean Motion) = 1440/" + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + ")");
			ChapGUI.printlnCond("        = " + period + " minutes");
		}
		ChapGUI.printlnCond();

		axis = 331.253274 * ASTMath.cuberoot(period*period);
		ChapGUI.printlnCond("Compute the length of the object's semi-major axis");
		ChapGUI.printlnCond("   a = 331.253274*cuberoot(tau^2) = 331.253274*cuberoot(" + period + "^2)");
		ChapGUI.printlnCond("     = " + axis + " km");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Object's orbital semi-major axis is " + ASTStr.insertCommas(ASTMath.Round(axis, 4)) + " km");

		ChapGUI.setResults("a=" + ASTStr.insertCommas(ASTMath.Round(axis, 4)) + " km for tau = " + 
				ASTMath.Round(period, 4) + " minutes");

		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate a satellite's distance from the center of the
	 * Earth at a given time.
	 * 
	 * @param calctype		SAT_DISTANCE_FROM_TLE or
	 * 						SAT_DISTANCE_FROM_INPUT
	 */
	protected void calcSatDistance(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prtObj = null;
		int idx, dateAdjust;
		double ecc, axis, M0, MMotion, Mt;
		double[] tmpKepler;
		ASTInt dateAdjustObj = new ASTInt();
		ASTDate epochDate;
		double epochUT, LCT, UT, GST, LST, JD, JDe, deltaT, EA, dv, dDist;
		ASTTime timeObj;
		ASTKepler.TrueAnomalyType solveKepler;
		ASTReal rTmp;

		// Although these are calculated below, initialize
		// them to avoid compiler warnings.
		idx = 0;
		epochUT = 0.0;
		ecc = 0.0;
		M0 = 0.0;
		MMotion = 0.0;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		// Figure out which method to use for solving Kepler's equation
		solveKepler = ChapGUI.getTrueAnomalyRBStatus();

		// Set up whether to display interim results when solving Kepler's equation
		if (ChapGUI.getShowInterimCalcsStatus()) prtObj = prt;

		if (calctype == ChapMenuItems.CalculationType.SAT_DISTANCE_FROM_INPUT) {
			if (ASTQuery.showQueryForm("eccentricity", "axis", "M0", 
					"Enter Keplerian elements ...") != ASTQuery.QUERY_OK) return;
			rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS); 
			if (rTmp.isValidRealObj()) ecc = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
				return;
			}
			if ((ecc < 0.0) || (ecc > 1.0)) {
				ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
				return;
			}
			rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis");
				return;
			}
			rTmp = ASTReal.isValidReal(ASTQuery.getData3(), ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) M0 = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid Mean Anomaly - try again", "Invalid Mean Anomaly");
				return;
			}

			if (ASTQuery.showQueryForm("Enter Epoch date (mm/dd/yyyy)",
					"Enter UT time for the Epoch") != ASTQuery.QUERY_OK) return;
			epochDate = ASTDate.isValidDate(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (!epochDate.isValidDateObj()) {
				ASTMsg.errMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date");
				return;
			}
			timeObj = ASTTime.isValidTime(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
			if (!timeObj.isValidTimeObj()) {
				ASTMsg.errMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time");
				return;
			}
			epochUT = timeObj.getDecTime();
			MMotion = Misc.Axis2MeanMotion(axis);
		} else {
			idx = Misc.getValidTLENum();
			if (idx < 0) return;
			if (ASTTLE.getKeplerianElementsFromTLE(idx)) {
				// The getKeplerianElementsFromTLE validates that the Keplerian
				// elements are valid, so we don't need to check the 'get' results
				epochDate = ASTTLE.getTLEEpochDate(idx);
				rTmp = ASTTLE.getTLEEpochUT(idx);
				epochUT = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEEccentricity(idx);
				ecc = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanAnomaly(idx);
				M0 = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanMotion(idx);
				MMotion = rTmp.getRealValue();  
			} else {
				ASTMsg.errMsg("The Keplerian elements for TLE data set " + (idx+1) + 
						"\nappear to be invalid - try another set", "Invalid Keplerian elements");
				return;
			}
			axis = Misc.MeanMotion2Axis(MMotion);
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.SAT_DISTANCE_FROM_INPUT) {
			ChapGUI.printlnCond("Use manually entered Keplerian Elements to calculate distance", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Use TLE data set " + (idx + 1) + " to calculate distance", ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.SAT_DISTANCE_FROM_INPUT) {
			ChapGUI.printlnCond("1.  Calculate mean motion from the semi-major axis");
			ChapGUI.printlnCond("    Mean Motion = " + String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
		} else {
			ChapGUI.printlnCond("1.  Calculate length of semi-major axis from mean motion");
			ChapGUI.printlnCond("    axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		}
		ChapGUI.printlnCond();
		ChapGUI.printlnCond("    Resulting data items to use are ...");
		ChapGUI.printlnCond("    Epoch Date: " + ASTDate.dateToStr(epochDate) + " at " +
				ASTTime.timeToStr(epochUT, ASTMisc.HMSFORMAT) + " UT");
		ChapGUI.printlnCond("    Keplerian Elements:");
		ChapGUI.printlnCond("       Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
		ChapGUI.printlnCond("       Length of Semi-major axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		ChapGUI.printlnCond("       Mean anomaly at the epoch = " + 
				String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
		ChapGUI.printlnCond("       Mean Motion = " + String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
		ChapGUI.printlnCond();

		// Do all the time-related calculations
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST,observer.getObsLon());

		ChapGUI.printlnCond("2.  Convert the observer LCT to UT, GST, and LST and adjust date if needed.");
		ChapGUI.printlnCond("    LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(observer.getObsDate()));
		ChapGUI.printCond("     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0) ChapGUI.printCond("previous day ");
		else if (dateAdjust > 0) ChapGUI.printCond("next day ");
		ChapGUI.printlnCond(ASTDate.dateToStr(adjustedDate) + ")");
		ChapGUI.printlnCond("    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		JDe = ASTDate.dateToJD(epochDate.getMonth(), epochDate.getdDay() + (epochUT / 24.0), epochDate.getYear());
		JD = ASTDate.dateToJD(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear());
		deltaT = JD - JDe;
		ChapGUI.printlnCond("3.  Compute the total number of elapsed days, including fractional days,");
		ChapGUI.printlnCond("    since the epoch.");
		ChapGUI.printlnCond("    deltaT = " + String.format(ASTStyle.genFloatFormat8,deltaT) + " days");
		ChapGUI.printlnCond();

		Mt = M0 + 360 * deltaT * MMotion;
		ChapGUI.printlnCond("4.  Propagate the mean anomaly by deltaT days");
		ChapGUI.printlnCond("    Mt = M0 + 360*deltaT*(Mean Motion) = " + 
				String.format(ASTStyle.genFloatFormat8,M0) + " + 360*" +
				String.format(ASTStyle.genFloatFormat8,deltaT) + "*(" + 
				String.format(ASTStyle.genFloatFormat8,MMotion) + ")");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat8,Mt) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5.  if necessary, adjust Mt to be in the range [0,360]");
		ChapGUI.printCond("    Mt = Mt MOD 360 = " + String.format(ASTStyle.genFloatFormat8,Mt) + " MOD 360 = ");
		Mt = ASTMath.xMOD(Mt, 360.0);
		ChapGUI.printlnCond(String.format(ASTStyle.genFloatFormat8,Mt) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  Solve Kepler's equation to get the eccentric anomaly from Mt");
		if (solveKepler == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
			tmpKepler = ASTKepler.calcSimpleKepler(prtObj, M0, ecc, Misc.termCriteria);
			EA = tmpKepler[0];
		} else {
			tmpKepler = ASTKepler.calcNewtonKepler(prtObj, M0, ecc, Misc.termCriteria);
			EA = tmpKepler[0];
		}
		ChapGUI.printlnCond("    E = " + String.format(ASTStyle.genFloatFormat8,EA) + " degrees");
		ChapGUI.printlnCond();

		dv = Math.sqrt((1 + ecc) / (1 - ecc)) * ASTMath.TAN_D(EA / 2.0);
		dv = 2.0 * ASTMath.INVTAN_D(dv);
		ChapGUI.printlnCond("7.  Compute the object's true anomaly");
		ChapGUI.printlnCond("    v = 2 * inv tan[ sqrt[(1 + e)/(1 - e)] * tan(E/2) ]");
		ChapGUI.printlnCond("      = 2 * inv tan[ sqrt[(1+" + ecc + ")/(1-" + ecc + ")] * tan(" +
				String.format(ASTStyle.genFloatFormat8,EA) + "/2) ]");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat8,dv) + " degrees");
		ChapGUI.printlnCond();

		dDist = (axis * (1 - (ecc*ecc))) / (1 + ecc * ASTMath.COS_D(dv));
		ChapGUI.printlnCond("8.  Compute the distance from the center of the Earth");
		ChapGUI.printlnCond("    Dist = a*(1-e^2)/(1 + e*cos(v))");
		ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat8,axis) + 
				"*(1-" + String.format(ASTStyle.genFloatFormat8,ecc) +
				"^2)/(1+" + String.format(ASTStyle.genFloatFormat8,ecc) + 
				"*cos(" + String.format(ASTStyle.genFloatFormat8,dv) + "))");
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(dDist));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("On " + ASTDate.dateToStr(observer.getObsDate()) + 
				", object is/was " + ASTStr.insertCommas(ASTMath.Round(dDist, 3)) + " km");
		ChapGUI.printlnCond("(" + ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist), 3)) +
				" miles) from the center of the Earth");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Distance: " + ASTStr.insertCommas(ASTMath.Round(dDist, 3)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist), 3)) + 
				" miles) from the center of the Earth");
	}

	/**
	 * Calculate the size of a satellite's footprint, what %
	 * of the Earth its footprint covers, and how long it will
	 * be in the footprint.
	 */
	protected void calcSatFootprint() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double dist, V, r_fp, Fpct, Fpt;
		ASTReal rTmp;

		if (ASTQuery.showQueryForm("Distance from center\nof Earth in km",
				"Orbital Velocity (km/s)") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) dist = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid orbital distance - try again", "Invalid Orbital Distance");
			return;
		}
		if (dist < ASTOrbits.EarthRadius) {
			ASTMsg.errMsg("Orbital distance must be > Radius of Earth", "Invalid Orbital Distance");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) V = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid orbital distance - try again", "Invalid Orbital Distance");
			return;
		}
		if (V < 0.0) {
			ASTMsg.errMsg("Invalid orbital velocity - try again", "Invalid Orbital Velocity");
			return;
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate Basic Data about a Satellite's Footprint", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		r_fp = ASTOrbits.EarthRadius * ASTMath.deg2rad(ASTMath.INVCOS_D(ASTOrbits.EarthRadius / dist));
		ChapGUI.printlnCond("Compute the max radius of the satellite's footprint");
		ChapGUI.printlnCond("   r_fp = (pi/180)*r_e*invcos(r_e/r) = (pi/180)*" + ASTOrbits.EarthRadius +
				"*invcos(" + ASTOrbits.EarthRadius + "/" + dist + ")");
		ChapGUI.printlnCond("        = " + r_fp + " km");
		ChapGUI.printlnCond();

		Fpct = 50 * (dist - ASTOrbits.EarthRadius) / dist;
		ChapGUI.printlnCond("Compute the fraction of the Earth's surface that is in the footprint");
		ChapGUI.printlnCond("   F = 50*(r - r_e)/r = 50*(" + dist + " - " + ASTOrbits.EarthRadius + ")/" + dist);
		ChapGUI.printlnCond("     = " + ASTMath.Round(Fpct, 1) + "%");
		ChapGUI.printlnCond();

		Fpt = r_fp / (30 * V);
		ChapGUI.printlnCond("Calculate how long the satellite will be in the footprint");
		ChapGUI.printlnCond("   FPt = r_fp/(30*V) = " + r_fp + "/(30*" + V + ")");
		ChapGUI.printlnCond("       = " + Fpt + " minutes");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("A satellite at a distance of " + 
				ASTStr.insertCommas(ASTMath.Round(dist, 2)) + " km that is");
		ChapGUI.printlnCond("travelling at " + ASTMath.Round(V, 2) + 
				" km/s has a footprint radius of " + ASTMath.Round(r_fp, 2) + " km");
		ChapGUI.printlnCond("and covers " + ASTMath.Round(Fpct, 1) + 
				"% of the Earth's surface. It will remain in the");
		ChapGUI.printlnCond("footprint for " + ASTMath.Round(Fpt, 2) + " minutes");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Footprint radius=" + ASTMath.Round(r_fp, 2) + " km, " + 
				ASTMath.Round(Fpct, 1) + "% coverage, in footprint for " +
				ASTMath.Round(Fpt, 2) + " minutes");
	}

	/**
	 * Calculate a satellite's location.
	 * 
	 * @param calctype		LOCATE_SAT_FROM_TLE or
	 * 						LOCATE_SAT_FROM_INPUT
	 */
	protected void calcSatLocation(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		int idx;
		ASTReal rTmp;
		double inclin, ecc, axis, RAAN, argofperi, M0, MMotion;
		ASTDate epochDate = new ASTDate();
		double epochUT;
		double h_sea;
		ASTTime timeObj;
		ASTCoord topoCoord;
		ASTKepler.TrueAnomalyType solveKepler = ChapGUI.getTrueAnomalyRBStatus();
		boolean DST = ChapGUI.getDSTStatus();

		// Although these are calculated below, initialize
		// them to avoid compiler warnings.
		idx = 0;
		epochUT = 0.0;
		inclin = 0.0;
		ecc = 0.0;
		RAAN = 0.0;
		argofperi = 0.0;
		M0 = 0.0;
		MMotion = 0.0;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;
		
		if (ASTQuery.showQueryForm("Enter Observer's Height\nabove sea level (in meters)") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) h_sea = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Observer Height - try again", "Invalid Height");
			return;
		}

		if (calctype == ChapMenuItems.CalculationType.LOCATE_SAT_FROM_INPUT) {
			if (!VDator.validateKeplerianElements()) return;   
			inclin = VDator.getInclination();
			ecc = VDator.getEccentricity();
			axis = VDator.getAxisLength();
			RAAN = VDator.getRAAN();
			argofperi = VDator.getArgOfPeri();
			M0 = VDator.getMeanAnomaly();

			if (ASTQuery.showQueryForm("Enter Epoch date (mm/dd/yyyy)",
					"Enter UT time for the Epoch") != ASTQuery.QUERY_OK) return;
			epochDate = ASTDate.isValidDate(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (!epochDate.isValidDateObj()) {
				ASTMsg.errMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date");
				return;
			}
			timeObj = ASTTime.isValidTime(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
			if (!timeObj.isValidTimeObj()) {
				ASTMsg.errMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time");
				return;
			}
			epochUT = timeObj.getDecTime();
		} else {
			idx = Misc.getValidTLENum();
			if (idx < 0) return;

			rTmp = ASTTLE.getTLEMeanMotion(idx);
			if (rTmp.isValidRealObj()) MMotion = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion");
				return;
			}
			axis = Misc.MeanMotion2Axis(MMotion);
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.LOCATE_SAT_FROM_INPUT) {
			ChapGUI.printlnCond("Find a satellite's location from manually entered data", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Find a satellite's location from TLE data set " + (idx+1), ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.LOCATE_SAT_FROM_INPUT) {
			ChapGUI.printlnCond("1.  Data entered was");
			ChapGUI.printlnCond("    Epoch Date: " + ASTDate.dateToStr(epochDate) + " at " + 
					ASTTime.timeToStr(epochUT, ASTMisc.HMSFORMAT) + " UT");
			ChapGUI.printlnCond("    Keplerian Elements are:");
			ChapGUI.printlnCond("       Inclination = " + 
					String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
			ChapGUI.printlnCond("       Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
			ChapGUI.printlnCond("       Length of Semi-major axis = " + 
					ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
			ChapGUI.printlnCond("       RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
			ChapGUI.printlnCond("       Argument of Perigee = " + 
					String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
			ChapGUI.printlnCond("       Mean anomaly at the epoch = " + 
					String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
			ChapGUI.printlnCond();
			MMotion = Misc.Axis2MeanMotion(axis);
			ChapGUI.printlnCond("2.  Convert Mean Motion to semi-major axis");
			ChapGUI.printlnCond("    Mean Motion = " + String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
		} else {
			ChapGUI.printlnCond("1.  Extract epoch date and Keplerian Elements from TLE data set " + (idx+1));
			if (ChapGUI.getShowInterimCalcsStatus()) {
				ASTTLE.displayTLEHeader();
				ASTTLE.displayTLEItem(idx);
				ChapGUI.printlnCond();
			}
			if (ASTTLE.getKeplerianElementsFromTLE(idx)) {
				// The getKeplerianElementsFromTLE validates that the Keplerian
				// elements are valid, so we don't need to check the 'get' results
				epochDate = ASTTLE.getTLEEpochDate(idx);
				rTmp = ASTTLE.getTLEEpochUT(idx);
				epochUT = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEInclination(idx);
				inclin = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEEccentricity(idx);
				ecc = rTmp.getRealValue();
				rTmp = ASTTLE.getTLERAAN(idx);
				RAAN = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEArgofPeri(idx);
				argofperi = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanAnomaly(idx);
				M0 = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanMotion(idx);
				MMotion = rTmp.getRealValue();  
			} else {
				ASTMsg.errMsg("The Keplerian elements for TLE data set " + (idx+1) + 
						"\nappear to be invalid - try another set", "Invalid Keplerian elements");
				return;
			}          
			ChapGUI.printlnCond("    Epoch Date: " + ASTDate.dateToStr(epochDate) + " at " + 
					ASTTime.timeToStr(epochUT, ASTMisc.HMSFORMAT) + " UT");
			ChapGUI.printlnCond("    Keplerian Elements are:");
			ChapGUI.printlnCond("       Inclination = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
			ChapGUI.printlnCond("       Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
			ChapGUI.printlnCond("       RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
			ChapGUI.printlnCond("       Argument of Perigee = " + 
					String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
			ChapGUI.printlnCond("       Mean anomaly at the epoch = " + 
					String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
			ChapGUI.printlnCond("       Mean Motion = " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
			ChapGUI.printlnCond();
			axis = Misc.MeanMotion2Axis(MMotion);
			ChapGUI.printlnCond("2.  Convert Mean Motion to semi-major axis");
			ChapGUI.printlnCond("    axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		}
		ChapGUI.printlnCond();

		topoCoord = ASTCoord.Keplerian2Topo(3,observer, h_sea, DST, epochDate, epochUT, inclin, ecc, 
				axis, RAAN, argofperi, M0, MMotion, solveKepler, Misc.termCriteria, prt);

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("For this observer, the satellite is at " +
				ASTAngle.angleToStr(topoCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Alt_topo, " +
				ASTAngle.angleToStr(topoCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Az_topo");
	}

	/**
	 * Calculate a satellite's orbital period.
	 * 
	 * @param calctype		SAT_PERIOD_FROM_TLE or
	 * 						SAT_PERIOD_FROM_INPUT
	 */
	protected void calcSatOrbitalPeriod(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx;
		double axis, MMotion;
		double period;
		String tmpStr;
		ASTReal rTmp;

		// Although these are calculated below, initialize
		// them to avoid compiler warnings.
		idx = 0;
		MMotion = 0.0;

		if (calctype == ChapMenuItems.CalculationType.SAT_PERIOD_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter length of semi-major\n" + 
					"axis (in km)") != ASTQuery.QUERY_OK) return;
			rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis");
				return;
			}
			if (axis < 0.0) {
				ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis");
				return;
			}
			tmpStr = "manually entered data";
		} else {
			idx = Misc.getValidTLENum();
			if (idx < 0) return;
			if (ASTTLE.getKeplerianElementsFromTLE(idx)) {
				// The getKeplerianElementsFromTLE validates that the Keplerian
				// elements are valid, so we don't need to check the 'get' results
				rTmp = ASTTLE.getTLEMeanMotion(idx);
				MMotion = rTmp.getRealValue();  
			} else {
				ASTMsg.errMsg("The Keplerian elements for TLE data set " + (idx+1) + 
						"\nappear to be invalid - try another set", "Invalid Keplerian elements");
				return;
			}
			axis = Misc.MeanMotion2Axis(MMotion);
			tmpStr = "TLE data set " + (idx+1);
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Use " + tmpStr + " to calculate the Orbital Period", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.SAT_PERIOD_FROM_INPUT) {
			ChapGUI.printlnCond("Length of semi-major axis as entered: " + 
					ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		} else {
			ChapGUI.printlnCond("Mean Motion from TLE data: " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
			ChapGUI.printlnCond("Semi-major axis computed from Mean Motion: " + 
					ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		}
		ChapGUI.printlnCond();

		period = 0.00016587 * Math.sqrt(Math.pow(axis,3));
		ChapGUI.printlnCond("Compute the object's orbital period");
		ChapGUI.printlnCond("   tau = 0.00016587*sqrt(a^3) = 0.00016587*sqrt(" + axis + "^3)");
		ChapGUI.printlnCond("       = " + period + " minutes");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Object's orbital period is " + ASTMath.Round(period, 4) + " minutes (" +
				ASTMath.Round(period / 60.0, 4) + " hours)");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("tau=" + ASTMath.Round(period, 4) + " minutes (" +
				ASTMath.Round(period / 60.0, 4) + " hours) for a = " +
				ASTStr.insertCommas(ASTMath.Round(axis, 4)) + " km");
	}

	/**
	 * Calculate a satellite's perigee and apogee distances.
	 * 
	 * @param calctype		SAT_PERI_APOGEE_FROM_TLE or
	 * 						SAT_PERI_APOGEE_FROM_INPUT
	 */
	protected void calcSatPerigeeApogee(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx;
		double ecc, axis, MMotion;
		double apogeeDist, perigeeDist;
		String tmpStr;
		ASTReal rTmp;

		// Although these are calculated below, initialize
		// them to avoid compiler warnings.
		idx = 0;
		ecc = 0.0;
		MMotion = 0.0;

		if (calctype == ChapMenuItems.CalculationType.SAT_PERI_APOGEE_FROM_INPUT) {
			if (ASTQuery.showQueryForm("Enter orbital eccentricity", "Enter length of semi-major\n" +
					"axis (in km)") != ASTQuery.QUERY_OK) return;
			rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) ecc = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
				return;
			}
			if ((ecc < 0.0) || (ecc > 1.0)) {
				ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
				return;
			}
			rTmp = ASTReal.isValidReal(ASTQuery.getData2(), ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
			else {
				ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis");
				return;
			}
			tmpStr = "manually entered data";
		} else {
			idx = Misc.getValidTLENum();
			if (idx < 0) return;

			if (ASTTLE.getKeplerianElementsFromTLE(idx)) {
				// The getKeplerianElementsFromTLE validates that the Keplerian
				// elements are valid, so we don't need to check the 'get' results
				rTmp = ASTTLE.getTLEEccentricity(idx);
				ecc = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanMotion(idx);
				MMotion = rTmp.getRealValue();  
			} else {
				ASTMsg.errMsg("The Keplerian elements for TLE data set " + (idx+1) + 
						"\nappear to be invalid - try another set", "Invalid Keplerian elements");
				return;
			}
			axis = Misc.MeanMotion2Axis(MMotion);
			tmpStr = "TLE data set " + (idx+1);
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Use " + tmpStr + " to calculate Perigee/Apogee distances", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		if (calctype == ChapMenuItems.CalculationType.SAT_PERI_APOGEE_FROM_INPUT) {
			ChapGUI.printlnCond("Eccentricity as entered: " + String.format(ASTStyle.genFloatFormat8,ecc));
			ChapGUI.printlnCond("Length of semi-major axis as entered: " + 
					ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		} else {
			ChapGUI.printlnCond("Eccentricity from TLE data: " + String.format(ASTStyle.genFloatFormat8,ecc));
			ChapGUI.printlnCond("Mean Motion from TLE data: " + 
					String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
			ChapGUI.printlnCond("Semi-major axis computed from Mean Motion: " + 
					ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		}
		ChapGUI.printlnCond();

		perigeeDist = axis * (1 - ecc);
		ChapGUI.printlnCond("Compute the object's distance at perigee.");
		ChapGUI.printlnCond("   r_p = a * (1 - e) = " + axis + " * (1 - " + ecc + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(ASTMath.Round(perigeeDist, 8)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(perigeeDist), 8)) + 
				" miles) from the center of the Earth");
		ChapGUI.printlnCond();

		apogeeDist = axis * (1 + ecc);
		ChapGUI.printlnCond("Compute the object's distance at apogee.");
		ChapGUI.printlnCond("   r_a = a * (1 + e) = " + axis + " * (1 + " + ecc + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(ASTMath.Round(apogeeDist, 8)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(apogeeDist), 8)) + 
				" miles) from the center of the Earth");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("At perigee, object is " + 
				ASTStr.insertCommas(ASTMath.Round(perigeeDist, 2)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(perigeeDist), 2)) + 
				" miles) from the center of the Earth.");
		ChapGUI.printlnCond("At apogee, object is " + 
				ASTStr.insertCommas(ASTMath.Round(apogeeDist, 2)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(apogeeDist), 2)) + 
				" miles) from the center of the Earth");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Perigee Dist: " + ASTStr.insertCommas(ASTMath.Round(perigeeDist, 2)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(perigeeDist), 2)) + " miles), Apogee Dist: " +
				ASTStr.insertCommas(ASTMath.Round(apogeeDist, 2)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(apogeeDist), 2)) + " miles)");
	}

	/**
	 * Calculate a satellite's orbital velocity when in a
	 * circular orbit.
	 */
	protected void calcSatVelocityCircular() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double axis, period, V, V2;
		ASTReal rTmp;

		// For circular orbits, the radius and the semi-major axis are the same
		if (ASTQuery.showQueryForm("Enter orbital radius in km\n" +
				"(i.e., distance above center of Earth)") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid orbital radius - try again", "Invalid Orbital Radius");
			return;
		}
		if (axis < ASTOrbits.EarthRadius) {
			ASTMsg.errMsg("Orbital radius must be > Radius of Earth", "Invalid Orbital Radius");
			return;
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute Orbital Velocity for a Circular Orbit", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		period = 0.00016587 * Math.sqrt(Math.pow(axis, 3));
		ChapGUI.printlnCond("Compute the object's orbital period");
		ChapGUI.printlnCond("   tau = 0.00016587*sqrt(r^3) = 0.00016587*sqrt(" + axis + "^3)");
		ChapGUI.printlnCond("       = " + period + " minutes");
		ChapGUI.printlnCond();

		V = (Math.PI * axis) / (30 * period);
		ChapGUI.printlnCond("Compute the object's orbital velocity");
		ChapGUI.printlnCond("   V = (pi*r)/(30*tau) = (pi*" + axis + ")/(30*" + period + ")");
		ChapGUI.printlnCond("     = " + String.format(ASTStyle.genFloatFormat8,V) + " km/s");
		ChapGUI.printlnCond();

		V2 = 631.348115 * Math.sqrt(1 / axis);
		ChapGUI.printlnCond("Using vis-viva law instead,");
		ChapGUI.printlnCond("   V = 631.348115*sqrt(1/r) = 631.348115*sqrt(1/" + axis + ")");
		ChapGUI.printlnCond("     = " + String.format(ASTStyle.genFloatFormat8,V2) + " km/s");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("An object in a circular orbit that is " + 
				ASTStr.insertCommas(ASTMath.Round(axis, 4)) + " km");
		ChapGUI.printlnCond("above the center of the Earth has an orbital velocity");
		ChapGUI.printlnCond("of " + ASTMath.Round(V, 2) + " km/s (" + 
				ASTMath.Round(ASTMath.KM2Miles(V), 2) + " miles/s, or " +
				ASTStr.insertCommas(ASTMath.Round(3600 * ASTMath.KM2Miles(V), 2)) + " mph)");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("r = " + ASTStr.insertCommas(ASTMath.Round(axis, 2)) + 
				" km, tau = " + ASTMath.Round(period, 2) + " minutes, V = " +
				ASTMath.Round(V, 2) + " km/s (" + ASTMath.Round(ASTMath.KM2Miles(V), 2) + " miles/s)");
	}

	/**
	 * Calculate a satellite's orbital velocity when in an
	 * elliptical orbit.
	 */
	protected void calcSatVelocityElliptical() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double axis, dist, perigeeDist, apogeeDist, ecc, V, Vap, Vperi;
		ASTReal rTmp;

		if (ASTQuery.showQueryForm("eccentricity", "distance", "axis", 
				"Enter orbital data ...") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) ecc = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
			return;
		}
		if ((ecc < 0.0) || (ecc >= 1.0)) {
			ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) dist = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid orbital distance - try again", "Invalid Orbital Distance");
			return;
		}
		if (dist < ASTOrbits.EarthRadius) {
			ASTMsg.errMsg("Orbital distance must be > Radius of Earth", "Invalid Orbital Distance");
			return;
		}
		rTmp = ASTReal.isValidReal(ASTQuery.getData3(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) axis = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis");
			return;
		}
		if (axis < ASTOrbits.EarthRadius) {
			ASTMsg.errMsg("Semi-major axis must be > Radius of Earth", "Invalid Semi-Major Axis");
			return;
		}

		// Be sure that (2/r) > (1/a)
		if ((1 / axis) >= (2 / dist)) {
			ASTMsg.errMsg("Semi-major axis or Dist is incorrect", "Invalid axis or distance");
			return;
		}

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute Orbital Velocity for an Elliptical Orbit", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		V = 631.348115 * Math.sqrt((2 / dist) - (1 / axis));
		ChapGUI.printlnCond("Use the vis-viva law to compute the object's orbital velocity");
		ChapGUI.printlnCond("   V = 631.348115*sqrt[(2/r)-(1/a)] = 631.348115*sqrt[(2/" + dist + ")-(1/" + axis + ")]");
		ChapGUI.printlnCond("     = " + String.format(ASTStyle.genFloatFormat8,V) + " km/s");
		ChapGUI.printlnCond();

		perigeeDist = axis * (1 - ecc);
		ChapGUI.printlnCond("Compute the object's distance at perigee.");
		ChapGUI.printlnCond("   r_p = a * (1 - e) = " + axis + " * (1 - " + ecc + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(ASTMath.Round(perigeeDist, 8)) + " km (" + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(perigeeDist), 8)) + 
				" miles) from the center of the Earth");
		ChapGUI.printlnCond();

		Vperi = 631.348115 * Math.sqrt((2 / perigeeDist) - (1 / axis));
		ChapGUI.printlnCond("Use the vis-viva law to compute the object's orbital velocity at perigee");
		ChapGUI.printlnCond("   Vperigee = 631.348115*sqrt[(2/r_p)-(1/a)] = 631.348115*sqrt[(2/" + 
				perigeeDist + ")-(1/" + axis + ")]");
		ChapGUI.printlnCond("            = " + String.format(ASTStyle.genFloatFormat8,Vperi) + " km/s");
		ChapGUI.printlnCond();

		apogeeDist = axis * (1 + ecc);
		ChapGUI.printlnCond("Compute the object's distance at apogee.");
		ChapGUI.printlnCond("   r_a = a * (1 + e) = " + axis + " * (1 + " + ecc + ")");
		ChapGUI.printlnCond("       = " + ASTStr.insertCommas(ASTMath.Round(apogeeDist, 8)) + " km (" +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(apogeeDist), 8)) + 
				" miles) from the center of the Earth");
		ChapGUI.printlnCond();

		Vap = 631.348115 * Math.sqrt((2 / apogeeDist) - (1 / axis));
		ChapGUI.printlnCond("Use the vis-viva law to compute the object's orbital velocity at apogee");
		ChapGUI.printlnCond("   Vapogee = 631.348115*sqrt[(2/r_a)-(1/a)] = 631.348115*sqrt[(2/" + 
				apogeeDist + ")-(1/" + axis + ")]");
		ChapGUI.printlnCond("           = " + String.format(ASTStyle.genFloatFormat8,Vap) + " km/s");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("An object in an elliptical orbit that is " + 
				ASTStr.insertCommas(ASTMath.Round(dist, 4)) + " km");
		ChapGUI.printlnCond("above the center of the Earth has an orbital velocity");
		ChapGUI.printlnCond("of " + ASTMath.Round(V, 2) + " km/s (" + 
				ASTMath.Round(ASTMath.KM2Miles(V), 2) + " miles/s, or " +
				ASTStr.insertCommas(ASTMath.Round(3600 * ASTMath.KM2Miles(V), 2)) + " mph)");
		ChapGUI.printlnCond();
		ChapGUI.printlnCond("Distance at perigee is " +
				ASTStr.insertCommas(ASTMath.Round(perigeeDist, 4)) + " km and");
		ChapGUI.printlnCond("orbital velocity is " + ASTMath.Round(Vperi, 2) + " km/s (" + 
				ASTMath.Round(ASTMath.KM2Miles(Vperi), 2) + " miles/s, or " +
				ASTStr.insertCommas(ASTMath.Round(3600 * ASTMath.KM2Miles(Vperi), 2)) + " mph)");
		ChapGUI.printlnCond();
		ChapGUI.printlnCond("Distance at apogee is " + 
				ASTStr.insertCommas(ASTMath.Round(apogeeDist, 4)) + " km and");
		ChapGUI.printlnCond("orbital velocity is " + ASTMath.Round(Vap, 2) + " km/s (" + 
				ASTMath.Round(ASTMath.KM2Miles(Vap), 2) + " miles/s, or " +
				ASTStr.insertCommas(ASTMath.Round(3600 * ASTMath.KM2Miles(Vap), 2)) + " mph)");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("V=" + ASTMath.Round(V, 2) + " km/s, Vperigee=" + 
				ASTMath.Round(Vperi, 2) + " km/s, Vapogee=" +
				ASTMath.Round(Vap, 2) + " km/s");
	}

	/**
	 * Calculate a satellite's rise/set times. This method
	 * only provides an estimate. It does so by periodically
	 * calculating the horizon coordinates over some
	 * interval of time. The results are rough estimates
	 * at best, due in part to using such a simple
	 * orbit propagator
	 *
	 * The starting time is assumed to be the observer LCT. As
	 * the LCT is incremented, it is rounded to 2 decimal seconds
	 * so that what is printed as the LCT will actually match
	 * what is used for the calculations. Failure to do this
	 * means that the coordinates calculated by a decimal
	 * LCT value (e.g., 7.5043138888) may differ greatly 
	 * from that calculated using a less precise HMS
	 * value (e.g., 7:30:15.53 vs 7:30:15.52999968).
	 *
	 * @param calctype		LOCATE_SAT_FROM_TLE or
	 *						LOCATE_SAT_FROM_INPUT
	 */
	protected void calcSatRiseSet(ChapMenuItems.CalculationType calctype) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		int i, idx;
		double inclin, ecc, axis, RAAN, argofperi, M0, MMotion;
		ASTDate epochDate = new ASTDate();
		ASTDate tmpDate = new ASTDate();
		int saveDay, saveSign;
		double epochUT, LCT, saveJD, maxHours, incrementT, deltaT, JD, dT;
		ASTTime timeObj = new ASTTime();
		ASTCoord topoCoord = new ASTCoord();
		String tmpStr;
		ASTReal rTmp;
		double h_sea;
		boolean DST = ChapGUI.getDSTStatus();
		ASTKepler.TrueAnomalyType solveKepler = ChapGUI.getTrueAnomalyRBStatus();

		// Although these are calculated below, initialize
		// them to avoid compiler warnings.
		idx = 0;
		epochUT = 0.0;
		ecc = 0.0;
		RAAN = 0.0;
		argofperi = 0.0;
		M0 = 0.0;
		MMotion = 0.0;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;
		
		if (ASTQuery.showQueryForm("Enter Observer's Height\nabove sea level (in meters)") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) h_sea = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Observer Height - try again", "Invalid Height");
			return;
		}

		if (calctype == ChapMenuItems.CalculationType.RISE_SET_FROM_INPUT) {
			if (!VDator.validateKeplerianElements()) return;
			inclin = VDator.getInclination();
			ecc = VDator.getEccentricity();
			axis = VDator.getAxisLength();
			RAAN = VDator.getRAAN();
			argofperi = VDator.getArgOfPeri();
			M0 = VDator.getMeanAnomaly();

			if (ASTQuery.showQueryForm("Enter Epoch date (mm/dd/yyyy)",
					"Enter UT time for the Epoch") != ASTQuery.QUERY_OK) return;
			epochDate = ASTDate.isValidDate(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
			if (!epochDate.isValidDateObj()) {
				ASTMsg.errMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date");
				return;
			}
			timeObj = ASTTime.isValidTime(ASTQuery.getData2(), ASTMisc.HIDE_ERRORS);
			if (!timeObj.isValidTimeObj()) {
				ASTMsg.errMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time");
				return;
			}
			epochUT = timeObj.getDecTime();
			MMotion = Misc.Axis2MeanMotion(axis);
		} else {
			idx = Misc.getValidTLENum();
			if (idx < 0) return;
			if (ASTTLE.getKeplerianElementsFromTLE(idx)) {
				// The getKeplerianElementsFromTLE validates that the Keplerian
				// elements are valid, so we don't need to check the 'get' results
				epochDate = ASTTLE.getTLEEpochDate(idx);
				rTmp = ASTTLE.getTLEEpochUT(idx);
				epochUT = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEInclination(idx);
				inclin = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEEccentricity(idx);
				ecc = rTmp.getRealValue();
				rTmp = ASTTLE.getTLERAAN(idx);
				RAAN = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEArgofPeri(idx);
				argofperi = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanAnomaly(idx);
				M0 = rTmp.getRealValue();
				rTmp = ASTTLE.getTLEMeanMotion(idx);
				MMotion = rTmp.getRealValue();  
			} else {
				ASTMsg.errMsg("The Keplerian elements for TLE data set " + (idx+1) + 
						"\nappear to be invalid - try another set", "Invalid Keplerian elements");
				return;
			}
			axis = Misc.MeanMotion2Axis(MMotion);
		}

		if (ASTQuery.showQueryForm("Maximum Hours from observer LCT\n(enter decimal hours, NOT HMS!)",
				"Increment (decimal hours or HMS)") != ASTQuery.QUERY_OK) return;
		rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) maxHours = rTmp.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Max Hours - try again", "Invalid Max Hours");
			return;
		}
		if (maxHours < 0) {
			ASTMsg.errMsg("Invalid Max Hours - try again", "Invalid Max Hours");
			return;
		}
		timeObj = ASTTime.isValidTime(ASTQuery.getData2(), ASTMisc.HIDE_ERRORS);
		if (!timeObj.isValidTimeObj()) {
			ASTMsg.errMsg("Invalid Time Increment- try again", "Invalid Time Increment");
			return;
		}
		deltaT = timeObj.getDecTime();

		ChapGUI.clearTextAreas();
		prt.setBoldFont(true);
		if (calctype == ChapMenuItems.CalculationType.RISE_SET_FROM_INPUT) {
			ChapGUI.printlnCond("Use manually entered Keplerian Elements to estimate rise/set times", ASTPrt.CENTERTXT);
		} else {
			ChapGUI.printlnCond("Use TLE data set " + (idx+1) + " to estimate rise/set times", ASTPrt.CENTERTXT);
		}
		prt.setBoldFont(false);
		prt.println();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("    Epoch Date: " + ASTDate.dateToStr(epochDate) + " at " + 
				ASTTime.timeToStr(epochUT, ASTMisc.HMSFORMAT) + " UT");
		ChapGUI.printlnCond("    Keplerian Elements are:");
		ChapGUI.printlnCond("       Inclination = " + String.format(ASTStyle.genFloatFormat8,inclin) + " degrees");
		ChapGUI.printlnCond("       Eccentricity = " + String.format(ASTStyle.genFloatFormat8,ecc));
		ChapGUI.printlnCond("       Length of Semi-major axis = " + ASTStr.insertCommas(ASTMath.Round(axis, 8)) + " km");
		ChapGUI.printlnCond("       RAAN = " + String.format(ASTStyle.genFloatFormat8,RAAN) + " degrees");
		ChapGUI.printlnCond("       Argument of Perigee = " + 
				String.format(ASTStyle.genFloatFormat8,argofperi) + " degrees");
		ChapGUI.printlnCond("       Mean anomaly at the epoch = " + String.format(ASTStyle.genFloatFormat8,M0) + " degrees");
		ChapGUI.printlnCond("       Mean Motion = " + String.format(ASTStyle.genFloatFormat8,MMotion) + " revs/day");
		ChapGUI.printlnCond();

		LCT = observer.getObsTime().getDecTime();
		prt.println("Start at " + ASTTime.timeToStr(LCT, ASTMisc.HMSFORMAT) + " on " + 
				ASTDate.dateToStr(observer.getObsDate()));
		prt.println("for a maximum of " + maxHours + " hours, incremented by " + 
				ASTTime.timeToStr(deltaT, ASTMisc.HMSFORMAT) + " (" +
				ASTTime.timeToStr(deltaT, ASTMisc.DECFORMAT) + " hours)");
		prt.println();

		i = 1;
		incrementT = 0.0;
		tmpDate = observer.getObsDate();

		prt.println(String.format("%5s %10s %11s %16s %15s", " ", "Date   ", "LCT Time ",
				"Topo Alt    ", "Topo Az    "));
		prt.println(String.format("%65s", " ").replace(" ", "="));

		// Show location at the start time and save the day to determine when a new day starts,
		// save the sign of the altitude to determine rise/set when the sign changes, and
		// save the JD to interpolate a rise/set time. JD is needed rather than saving the LCT
		// because we may cross a date boundary
		prt.print(String.format("%-5d %10s %11s ", i, ASTDate.dateToStr(tmpDate), 
				ASTTime.timeToStr(LCT, ASTMisc.HMSFORMAT)));

		topoCoord = ASTCoord.Keplerian2Topo(0,observer,h_sea, DST, epochDate, epochUT, inclin, ecc, 
				axis, RAAN, argofperi, M0, MMotion, solveKepler, Misc.termCriteria, null);
		prt.println(String.format("%16s %15s", 
				ASTAngle.angleToStr(topoCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT),
				ASTAngle.angleToStr(topoCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT)));

		saveDay = tmpDate.getiDay();
		// Use LCT even though it really should be UT. This allows us to interpolate the LCT times
		// w/o the need to interpolate UTs and then convert to the LCT
		saveJD = ASTDate.dateToJD(tmpDate.getMonth(), tmpDate.getiDay() + (LCT / 24.0), tmpDate.getYear());
		saveSign = (int) Math.signum(topoCoord.getAltAngle().getDecAngle());

		// Loop until maxHours is reached
		while (incrementT < maxHours) {
			i = i + 1;

			// add deltaT to the time to get a new LCT - use JDs to do so and round
			// to nearest 100ths of a second so that times printed for the user
			// match the actual times used for determining the horizon coordinates
			JD = saveJD + deltaT / 24.0;
			tmpDate = ASTDate.JDtoDate(JD);
			LCT = ASTMath.Frac(tmpDate.getdDay()) * 24.0;
			LCT = ASTMath.Round(LCT, 2);
			// Update the observer object with the new time and potentially new date. This
			// will not affect the values displayed in the GUI. We could create a new observer
			// object and use it, but other methods in this program use the global
			// observer object. So, we will just update the global observer object.
			observer.setObsDateTime(tmpDate.getMonth(), tmpDate.getiDay(), tmpDate.getYear(), LCT);

			if (saveDay == tmpDate.getiDay()) tmpStr = " ";
			else {
				tmpStr = ASTDate.dateToStr(tmpDate);
				saveDay = tmpDate.getiDay();
			}
			prt.print(String.format("%-5d %10s %11s ", i, tmpStr, ASTTime.timeToStr(LCT, ASTMisc.HMSFORMAT)));

			// propagate to the new time
			topoCoord = ASTCoord.Keplerian2Topo(0,observer,h_sea, DST, epochDate, epochUT, inclin, ecc, 
					axis, RAAN, argofperi, M0, MMotion, solveKepler, Misc.termCriteria, null);

			prt.print(String.format("%16s %15s", 
					ASTAngle.angleToStr(topoCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT),
					ASTAngle.angleToStr(topoCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT)));

			// See if the object has transition above or below the horizon
			if (Math.signum(topoCoord.getAltAngle().getDecAngle()) != saveSign) {
				dT = saveJD + (JD - saveJD) / 2.0;  // This is the JD halfway between the previous and current time
				dT = ASTMath.Frac(ASTDate.JDtoDate(dT).getdDay()) * 24.0;  // Convert JD to a date and get the LCT time from it
				prt.print(" " + ASTTime.timeToStr(dT, ASTMisc.HMSFORMAT) + " est. ");
				if (saveSign < 0) prt.print("rise");
				else prt.print("set");
			}
			prt.println();

			saveSign = (int) Math.signum(topoCoord.getAltAngle().getDecAngle());
			saveJD = JD;
			incrementT = incrementT + deltaT;
		}

		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Set the termination criteria for solving Kepler's equation
	 */
	protected void setTerminationCriteria() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double dTerm = Misc.termCriteria;

		ChapGUI.clearTextAreas();;
		prt.setBoldFont(true);
		prt.println("Set Termination Criteria for use in solving Kepler's Equation", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		if (ASTQuery.showQueryForm("Enter Termination Criteria in radians\n(ex: 0.000002)") != ASTQuery.QUERY_OK) {
			prt.println("Termination criteria was not changed from " + Misc.termCriteria + " radians");
			return;
		}

		// Validate the termination criteria
		ASTReal rTmpObj = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (rTmpObj.isValidRealObj()) dTerm = rTmpObj.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria");
			prt.println("Termination criteria was not changed from " + Misc.termCriteria + " radians");
			return;
		}

		prt.println("Prior termination criteria was: " + Misc.termCriteria + " radians");
		if (dTerm < ASTKepler.KeplerMinCriteria) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too small, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		if (dTerm > 1) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too large, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		Misc.termCriteria = dTerm;

		prt.println("Termination criteria is now set to " + Misc.termCriteria + " radians");
		prt.resetCursor();
	}		
}
