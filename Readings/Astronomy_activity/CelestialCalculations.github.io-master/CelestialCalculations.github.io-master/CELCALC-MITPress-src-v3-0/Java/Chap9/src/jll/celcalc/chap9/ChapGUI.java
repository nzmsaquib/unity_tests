package jll.celcalc.chap9;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jll.celcalc.ASTUtils.ASTAboutBox;
import jll.celcalc.ASTUtils.ASTBookInfo;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTSites;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTLE;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Implements the main GUI</b>
 * <p>
 * The GUI was created with, and is maintained by, the Eclipse WindowBuilder.
 * All methods and data are declared static because it only makes sense to have
 * one main GUI per application, and making them static avoids the problem of
 * having to pass a reference to the GUI instance in other classes that need
 * to reference the main GUI.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 * <p>
 * Note that the Eclipse Window Builder has trouble with complex, dynamic GUIs so that
 * one should first build the GUI, then modify the code by hand to use
 * the definitions in ASTUtils.ASTStyle. The Window Builder start/stop hiding tags can be used to
 * surround code that the Window Builder parser has trouble handling.
 * The start hiding tag is <code>{@literal //$hide>>$}</code> while the stop
 * hiding tag is <code>{@literal //$hide<<$}</code>. For example,
 * <p>
 * <code>
 * {@literal //$hide>>$}
 * 		code to be hidden
 * {@literal //$hide<<$}
 * </code>
 */

@SuppressWarnings("serial")
class ChapGUI extends JFrame {
	private static final String WINDOW_TITLE = "Chapter 9 - Satellites";

	// Save the scrollable text pane area for output
	private static JTextPane textPane = new JTextPane();
	private static JScrollPane scrollPaneOutput = new JScrollPane();
	
	private static ASTPrt prt = null;				// create instance (actually done below) for printing to the output textPane
	private static ASTObserver observer = null;		// default observer location for the GUI
	private static ASTTLE TLEdb = null;				// currently loaded TLEs
	private static ASTSites sitesDB = null;			// currently loaded launch sites

	// Create an About Box
	private static ASTAboutBox aboutBox = new ASTAboutBox(WINDOW_TITLE);
	
	// Create a dialog for enter coordinate data
	private static Form6Coords coords6Form = new Form6Coords();

	// Create menu items, which will be added to in the ChapMenuItems class
	private static JMenu mnCoordSys = null;
	private static JMenu mnTLEs = null;
	private static JMenu mnLaunchSites = null;
	private static JMenu mnSatellites = null;

	// Create listeners for the various GUI components
	private static ActionListener listenerMenus = new MenusListener();
	private static ActionListener listenerRadioBtns = new RadioBtnsListener();

	// Various GUI components that user will interact with
	private static JTextField txtboxLat = new JTextField("180d 30m 25.56sW");
	private static JTextField txtboxLon = new JTextField("180d 30m 25.56sW");
	private static JTextField txtboxDate = new JTextField("12/30/2020");
	private static JTextField txtboxLCT = new JTextField("24:55:55");
	private static JCheckBox chkboxDST = new JCheckBox("Daylight Saving Time");
	private static JCheckBox chkboxShowInterimCalcs = new JCheckBox("Show Intermediate Calculations     ");
	private static JLabel lblResults = new JLabel("Result:");

	// define radio buttons for selecting Time Zone
	private static JRadioButton radbtnPST = new JRadioButton("PST");
	private static JRadioButton radbtnMST = new JRadioButton("MST");
	private static JRadioButton radbtnCST = new JRadioButton("CST");
	private static JRadioButton radbtnEST = new JRadioButton("EST");
	private static JRadioButton radbtnLon = new JRadioButton("Use Lon");
	
	// define radio buttons for selecting how to solve Kepler's equation
	private static JRadioButton radbtnSimple = new JRadioButton("Simple   ");
	private static JRadioButton radbtnNewtonMethod = new JRadioButton("Newton");

	/*=============================================================================================
	 * The string text collected in these next items is done to separate the strings displayed
	 * in the various GUI components (such as buttons and menus) from the semantics of what 
	 * clicking on a component means. This is important because otherwise, a change to the text
	 * displayed in the GUI, such as for a button label, via Eclipse WindowBuilder may well 
	 * break other code, particularly the action listeners. Through this technique, the 
	 * WindowBuilder can be used to change the label on a GUI element (e.g., button) without
	 * breaking any other code. The command that a listener will receive is the same regardless
	 * of what the actual label on the GUI component says. So, for example, the text "Exit"
	 * be changed in the menu to "Quit" without breaking the menu's listener because the
	 * listener will receive the command exitCommand regardless of what text is actually
	 * displayed in the menu. Get methods below (e.g., getExitCommand) return the appropriate
	 * command for the listeners to use.
	 *============================================================================================*/	
	/* define menu item commands */
	private static final String exitCommand = "exit";
	private static final String instructionsCommand = "instruct";
	private static final String aboutCommand = "about";
	private static final String chapMenuCommand = "chapmenucommand";

	/* define radio button commands */
	private static final String radbtnPSTCommand = "PST";
	private static final String radbtnMSTCommand = "MST";
	private static final String radbtnCSTCommand = "CST";
	private static final String radbtnESTCommand = "EST";
	private static final String radbtnLonCommand = "Longitude";
	private static final String radbtnSimpleIterationCommand = "simple";
	private static final String radbtnNewtonMethodCommand = "Newton";

	/**
	 * Create the GUI frame.
	 */
	protected ChapGUI() {
		JPanel contentPane = new JPanel();
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChapGUI.class.getResource("/resources/BlueMarble.png")));
		setTitle(WINDOW_TITLE);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 860);
		contentPane.setDoubleBuffered(false);
		contentPane.setFocusable(false);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		/*========================= Create Menus ===========================*/
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(ASTStyle.MENU_FONT);
		setJMenuBar(menuBar);

		// File menu
		JMenu mnFile = new JMenu(" File ");
		mnFile.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setFont(ASTStyle.MENU_FONT);
		mntmExit.setActionCommand(exitCommand);
		mntmExit.addActionListener(listenerMenus);
		mnFile.add(mntmExit);

		// Coord Sys menu
		mnCoordSys = new JMenu(" Coord Sys ");
		mnCoordSys.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnCoordSys);
		
		// TLEs menu
		mnTLEs = new JMenu(" TLEs ");
		mnTLEs.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnTLEs);
		
		// Launch Sites menu
		mnLaunchSites = new JMenu(" Launch Sites ");
		mnLaunchSites.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnLaunchSites);
		
		// Satellites menu
		mnSatellites = new JMenu(" Satellites ");
		mnSatellites.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnSatellites);
	
		// This much simpler code will populate the previous 2 menus via the separate ChapMenuItems class,
		// but at the price of not being able to see the menu via WindowBuilder
		// No need to store the menu items created because the results of the "new" is never referenced elsewhere
		new ChapMenuItems();

		// Help menu
		JMenu mnHelp = new JMenu(" Help ");
		mnHelp.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnHelp);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setFont(ASTStyle.MENU_FONT);
		mntmInstructions.setActionCommand(instructionsCommand);
		mntmInstructions.addActionListener(listenerMenus);
		mnHelp.add(mntmInstructions);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setFont(ASTStyle.MENU_FONT);
		mntmAbout.addActionListener(listenerMenus);
		mntmAbout.setActionCommand(aboutCommand);
		mnHelp.add(mntmAbout);
		/*======================== End of Menus =====================*/		

		setContentPane(contentPane);

		JPanel panelBookTitle = new JPanel();
		panelBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		panelBookTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelBookTitle.setFocusable(false);
		panelBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.setDoubleBuffered(false);

		scrollPaneOutput.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setVerifyInputWhenFocusTarget(false);
		scrollPaneOutput.setRequestFocusEnabled(false);
		scrollPaneOutput.setFocusable(false);
		scrollPaneOutput.setFocusTraversalKeysEnabled(false);

		JPanel panelObsTitle = new JPanel();
		panelObsTitle.setRequestFocusEnabled(false);

		JPanel panelForObsData = new JPanel();

		JPanel panelchkBoxes = new JPanel();
		panelchkBoxes.setFocusable(false);
		panelchkBoxes.setFocusTraversalKeysEnabled(false);
		panelchkBoxes.setRequestFocusEnabled(false);
		lblResults.setFont(ASTStyle.TEXT_BOLDFONT);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBookTitle, GroupLayout.DEFAULT_SIZE, 966, Short.MAX_VALUE)
				.addComponent(panelObsTitle, GroupLayout.DEFAULT_SIZE, 966, Short.MAX_VALUE)
				.addComponent(panelForObsData, GroupLayout.DEFAULT_SIZE, 966, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblResults, GroupLayout.DEFAULT_SIZE, 803, Short.MAX_VALUE)
					.addGap(149))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 938, Short.MAX_VALUE)
					.addContainerGap())
				.addComponent(panelchkBoxes, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panelBookTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelObsTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addComponent(panelForObsData, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addComponent(panelchkBoxes, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(lblResults)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 528, Short.MAX_VALUE)
					.addContainerGap())
		);
		chkboxShowInterimCalcs.setFocusable(false);
		chkboxShowInterimCalcs.setFocusTraversalKeysEnabled(false);
		chkboxShowInterimCalcs.setRequestFocusEnabled(false);

		chkboxShowInterimCalcs.setFont(ASTStyle.CBOX_FONT);
		chkboxShowInterimCalcs.setFocusPainted(false);

		JPanel panelObsData = new JPanel();
		panelObsData.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));

		txtboxLat.setFont(ASTStyle.TEXT_FONT);
		txtboxLat.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxLat.setColumns(10);

		JLabel lblLatitude = new JLabel("Latitude");
		lblLatitude.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLatitude.setHorizontalAlignment(SwingConstants.LEFT);

		txtboxLon.setFont(ASTStyle.TEXT_FONT);
		txtboxLon.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxLon.setColumns(10);

		JLabel lblLongitude = new JLabel("Longitude");
		lblLongitude.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLongitude.setHorizontalAlignment(SwingConstants.LEFT);

		txtboxLCT.setFont(ASTStyle.TEXT_FONT);
		txtboxLCT.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxLCT.setColumns(10);

		JLabel lblLCT = new JLabel("LCT");
		lblLCT.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLCT.setHorizontalAlignment(SwingConstants.LEFT);

		JLabel lblDate = new JLabel("Date");
		lblDate.setFont(ASTStyle.TEXT_BOLDFONT);
		lblDate.setHorizontalAlignment(SwingConstants.LEFT);

		txtboxDate.setFont(ASTStyle.TEXT_FONT);
		txtboxDate.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxDate.setColumns(10);

		radbtnPST.setFont(ASTStyle.RADBTN_FONT);
		radbtnPST.setActionCommand(radbtnPSTCommand);
		radbtnPST.addActionListener(listenerRadioBtns);

		radbtnMST.setFont(ASTStyle.RADBTN_FONT);
		radbtnMST.setActionCommand(radbtnMSTCommand);
		radbtnMST.addActionListener(listenerRadioBtns);

		radbtnCST.setFont(ASTStyle.RADBTN_FONT);
		radbtnCST.setActionCommand(radbtnCSTCommand);
		radbtnCST.addActionListener(listenerRadioBtns);

		radbtnEST.setFont(ASTStyle.RADBTN_FONT);
		radbtnEST.setActionCommand(radbtnESTCommand);
		radbtnEST.addActionListener(listenerRadioBtns);

		radbtnLon.setFont(ASTStyle.RADBTN_FONT);
		radbtnLon.setActionCommand(radbtnLonCommand);
		radbtnLon.addActionListener(listenerRadioBtns);

		chkboxDST.setFocusable(false);
		chkboxDST.setFocusTraversalKeysEnabled(false);
		chkboxDST.setRequestFocusEnabled(false);
		chkboxDST.setFocusPainted(false);
		chkboxDST.setFont(ASTStyle.CBOX_FONT);

		JLabel lblTimeZone = new JLabel("Time Zone");
		lblTimeZone.setFont(ASTStyle.TEXT_BOLDFONT);
		GroupLayout gl_panelObsData = new GroupLayout(panelObsData);
		gl_panelObsData.setHorizontalGroup(
				gl_panelObsData.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelObsData.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addComponent(txtboxLat, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(lblLatitude, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addComponent(txtboxLon, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(lblLongitude, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)))
						.addGap(18)
						.addComponent(lblTimeZone)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addComponent(radbtnPST)
										.addGap(4)
										.addComponent(radbtnMST)
										.addGap(4)
										.addComponent(radbtnCST)
										.addComponent(radbtnEST)
										.addGap(4)
										.addComponent(radbtnLon))
								.addComponent(chkboxDST, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtboxDate, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtboxLCT, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addComponent(lblDate, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblLCT, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
						.addGap(16))
				);
		gl_panelObsData.setVerticalGroup(
				gl_panelObsData.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelObsData.createSequentialGroup()
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panelObsData.createSequentialGroup()
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblDate)
																.addComponent(chkboxDST))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxLCT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblLCT)))
												.addGroup(gl_panelObsData.createSequentialGroup()
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxLat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblLatitude))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxLon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblLongitude))))
										.addGap(6))
								.addGroup(Alignment.TRAILING, gl_panelObsData.createSequentialGroup()
										.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
														.addComponent(radbtnPST)
														.addComponent(lblTimeZone))
												.addComponent(radbtnMST)
												.addComponent(radbtnCST)
												.addComponent(radbtnEST)
												.addComponent(radbtnLon))
										.addGap(7))))
				);
		panelObsData.setLayout(gl_panelObsData);
		panelObsTitle.setLayout(new BorderLayout(0, 0));

		JLabel lblObs = new JLabel("Observer Location and Time");
		lblObs.setFont(ASTStyle.TEXT_BOLDFONT);
		lblObs.setHorizontalAlignment(SwingConstants.CENTER);
		panelObsTitle.add(lblObs);
		
		panelForObsData.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelForObsData.add(panelObsData);

		panelBookTitle.setLayout(new BorderLayout(0, 0));		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setVerifyInputWhenFocusTarget(false);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookTitle.setFont(ASTStyle.BOOK_TITLE_BOLDFONT);
		lblBookTitle.setFocusable(false);
		lblBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.add(lblBookTitle, BorderLayout.CENTER);
		contentPane.setLayout(gl_contentPane);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		// Define the actual text area and add it to scrollPaneOutput
		textPane.setFont(ASTStyle.OUT_TEXT_FONT);
		textPane.setFocusable(false);
		textPane.setFocusTraversalKeysEnabled(false);
		textPane.setFocusCycleRoot(false);
		textPane.setEditable(false);
		scrollPaneOutput.setViewportView(textPane);
		
		JPanel panelEccAnomaly = new JPanel();
		panelEccAnomaly.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		radbtnSimple.setFont(ASTStyle.RADBTN_FONT);
		radbtnSimple.setActionCommand(radbtnSimpleIterationCommand);
		radbtnSimple.addActionListener(listenerRadioBtns);
		radbtnNewtonMethod.setFont(ASTStyle.RADBTN_FONT);
		radbtnNewtonMethod.setActionCommand(radbtnNewtonMethodCommand);
		radbtnNewtonMethod.addActionListener(listenerRadioBtns);
		
		GroupLayout gl_panelEccAnomaly = new GroupLayout(panelEccAnomaly);
		gl_panelEccAnomaly.setHorizontalGroup(
			gl_panelEccAnomaly.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelEccAnomaly.createSequentialGroup()
					.addContainerGap(24, Short.MAX_VALUE)
					.addComponent(radbtnSimple)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(radbtnNewtonMethod)
					.addGap(20))
		);
		gl_panelEccAnomaly.setVerticalGroup(
			gl_panelEccAnomaly.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelEccAnomaly.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panelEccAnomaly.createParallelGroup(Alignment.BASELINE)
						.addComponent(radbtnSimple)
						.addComponent(radbtnNewtonMethod))
					.addContainerGap(13, Short.MAX_VALUE))
		);
		panelEccAnomaly.setLayout(gl_panelEccAnomaly);
		
		panelchkBoxes.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelchkBoxes.add(chkboxShowInterimCalcs);
		panelchkBoxes.add(panelEccAnomaly);
		
		// Set parent frame for static methods that need to center a pane/frame/etc. on the GUI
		ASTQuery.setParentFrame(this);
		ASTMsg.setParentFrame(this);

		textPane.setText("     "); // must initialize with at least a few blanks
		prt = new ASTPrt(textPane);

		// Initialize the rest of the GUI
		ASTStyle.setASTStyle();
		aboutBox.pack();
		aboutBox.setParentFrame(this);
		coords6Form.setParentFrame(this);

		chkboxDST.setSelected(false);
		chkboxShowInterimCalcs.setSelected(false);
		setLonRadBtn();
		radbtnSimple.setSelected(true);
		
		txtboxLat.setText("");
		txtboxLon.setText("");
		txtboxDate.setText("");
		txtboxLCT.setText("");
		setResults("");
		
		// Load the default observer location (if it exists), create TLEs, create Sites
		observer = new ASTObserver();
		setObsDataInGUI(observer);
		TLEdb = new ASTTLE(prt);
		sitesDB = new ASTSites();

		// Explicitly indicate the order in which we want the GUI components traversed
		Vector<Component> order = new Vector<Component>(4);
		order.add(txtboxLat); order.add(txtboxLon); order.add(txtboxDate); order.add(txtboxLCT);
		ASTStyle.GUIFocusTraversalPolicy GUIPolicy = new ASTStyle.GUIFocusTraversalPolicy(order);
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setFocusTraversalPolicy(GUIPolicy);
	}
	
	/*=============================================================================
	 * The methods below return references to various GUI components such as the
	 * scrollable output text area. 
	 *============================================================================*/
	
	/**
	 * Gets the Coord Sys menu.
	 * 
	 * @return		the Coord Sys menu
	 */
	protected static JMenu getCoordSysMenu() {
		return mnCoordSys;
	}
	
	/**
	 * Gets the Launch Sites menu.
	 * 
	 * @return		the Launch Sites menu
	 */
	protected static JMenu getLaunchSitesMenu() {
		return mnLaunchSites;
	}

	/**
	 * Gets the listener for the menu items
	 * 
	 * @return		the action listener for handling the menus
	 */
	protected static ActionListener getMenuListener() {
		return listenerMenus;
	}

	/**
	 * Gets the ASTPrt instance for this application's scrollable text pane area.
	 * 
	 * @return		the ASTPrt instance for this application
	 */
	protected static ASTPrt getPrtInstance() {
		return prt;
	}
	
	/**
	 * Gets the Satellites menu
	 * 
	 * @return		the Satellites menu
	 */
	protected static JMenu getSatellitesMenu() {
		return mnSatellites;
	}
	
	/**
	 * Gets the scrollable text pane area for this GUI.
	 * 
	 * @return		the JTextPane for this GUI
	 */
	protected static JTextPane getTextPane() {
		return textPane;
	}

	/**
	 * Gets the TLEs menu.
	 * 
	 * @return		the TLEs menu
	 */
	protected static JMenu getTLEsMenu() {
		return mnTLEs;
	}

	/*=============================================================================
	 * The methods below are 'getters' and 'setters' for various items in the GUI.
	 *============================================================================*/

	/**
	 * Clears the text areas in the GUI
	 */
	protected static void clearTextAreas() {
		prt.clearTextArea();	
		setResults("");
	}
	
	/**
	 * Gets the coordinate query form
	 * 
	 * @return		the query form for asking the user for 6 coordinates
	 */
	protected static Form6Coords getCoordQueryForm() {
		return coords6Form;
	}
	
	/**
	 * Gets the current observer.
	 * 
	 * @return			current observer object
	 */
	protected static ASTObserver getcurrentObserver() {
		return observer;
	}
	
	/**
	 * Gets the currently loaded Launch sites
	 * 
	 * @return			current sites database
	 */
	protected static ASTSites getcurrentSitesDB() {
		return sitesDB;
	}
	
	/**
	 * Gets the currently loaded TLEs
	 * 
	 * @return			current TLE database
	 */
	protected static ASTTLE getcurrentTLEDB() {
		return TLEdb;
	}

	/**
	 * Gets the current status of the DST check box
	 * 
	 * @return		true if the DST check box is checked, else false
	 */
	protected static boolean getDSTStatus() {
		return chkboxDST.isSelected();
	}
	
	/**
	 * Gets the status of the Show Interim Calculations checkbox
	 * 
	 * @return			true if checkbox is selected else false
	 */
	protected static boolean getShowInterimCalcsStatus() {
		return chkboxShowInterimCalcs.isSelected();
	}

	/**
	 * Sets the results label in the GUI
	 * 
	 * @param result	string for the results label in the GUI
	 */
	protected static void setResults(String result) {
		lblResults.setText("Result: " + result);
	}

	/*-----------------------------------------------------------------------
	 * Handle the radio buttons. Make sure only 1 is selected at a time
	 -----------------------------------------------------------------------*/

					/* Handle time zone radio buttons */
	
	/**
	 * Clears all of the GUI radio buttons (i.e., sets them to unchecked). This
	 * routine is useful since Swing does not automatically manage radio buttons.
	 */
	private static void clearAllRadioBtns() {
		radbtnPST.setSelected(false);
		radbtnPST.setFocusPainted(false);
		radbtnPST.setFocusable(false);

		radbtnMST.setSelected(false);
		radbtnMST.setFocusPainted(false);
		radbtnMST.setFocusable(false);

		radbtnCST.setSelected(false);
		radbtnCST.setFocusPainted(false);
		radbtnCST.setFocusable(false);

		radbtnEST.setSelected(false);
		radbtnEST.setFocusPainted(false);
		radbtnEST.setFocusable(false);

		radbtnLon.setSelected(false);
		radbtnLon.setFocusPainted(false);
		radbtnLon.setFocusable(false);
	}
		
	/**
	 * Determine what time zone radio button is selected
	 * 
	 * @return		a time zone type for whatever is the
	 *				currently selected time zone radio button
	 */
	protected static ASTLatLon.TimeZoneType getSelectedRBStatus() {
		if (radbtnPST.isSelected()) return ASTLatLon.TimeZoneType.PST;
		else if (radbtnMST.isSelected()) return ASTLatLon.TimeZoneType.MST;
		else if (radbtnCST.isSelected()) return ASTLatLon.TimeZoneType.CST;
		else if (radbtnEST.isSelected()) return ASTLatLon.TimeZoneType.EST;
		else return ASTLatLon.TimeZoneType.LONGITUDE;
	}

	/**
	 * Gets the command that represents the PST radio button.
	 * 
	 * @return			PST radio button command
	 */
	protected static String getPSTCommand() {
		return radbtnPSTCommand;
	}

	/**
	 * Gets the command that represents the MST radio button.
	 * 
	 * @return			MST radio button command
	 */
	protected static String getMSTCommand() {
		return radbtnMSTCommand;
	}

	/**
	 * Gets the command that represents the CST radio button.
	 * 
	 * @return			CST radio button command
	 */
	protected static String getCSTCommand() {
		return radbtnCSTCommand;
	}

	/**
	 * Gets the command that represents the EST radio button.
	 * 
	 * @return			EST radio button command
	 */
	protected static String getESTCommand() {
		return radbtnESTCommand;
	}

	/**
	 * Gets the command that represents the Longitude radio button.
	 * 
	 * @return			Longitude radio button command
	 */
	protected static String getLongitudeCommand() {
		return radbtnLonCommand;
	}

	/**
	 * Sets the PST radio button to true
	 */
	protected static void setPSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnPST.setSelected(true);
	}

	/**
	 * Sets the MST radio button to true
	 */
	protected static void setMSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnMST.setSelected(true);
	}

	/**
	 * Sets the CST radio button to true
	 */
	protected static void setCSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnCST.setSelected(true);
	}

	/**
	 * Sets the EST radio button to true
	 */
	protected static void setESTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnEST.setSelected(true);
	}

	/**
	 * Sets the Longitude radio button to true
	 */
	protected static void setLonRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnLon.setSelected(true);
	}
	
					/* Handle Kepler's equation radio buttons */

	/**
	 * Clears all of the Kepler's equation GUI radio buttons (i.e., sets them to unchecked). This
	 * routine is useful since Swing does not automatically manage radio buttons.
	 */
	private static void clearAllKeplerEQRadioBtns() {
		radbtnSimple.setSelected(false);
		radbtnSimple.setFocusPainted(false);
		radbtnSimple.setFocusable(false);

		radbtnNewtonMethod.setSelected(false);
		radbtnNewtonMethod.setFocusPainted(false);
		radbtnNewtonMethod.setFocusable(false);
	}
	
	/**
	 * Gets the command that represents the Newton's Method radio button.
	 * 
	 * @return			Newton's Method radio button command
	 */
	protected static String getNewtonMethodCommand() {
		return radbtnNewtonMethodCommand;
	}
	
	/**
	 * Gets the status of the Newton's Method radio button.
	 * 
	 * @return			Newton's Method radio button status
	 */
	protected static boolean getNewtonMethodRBStatus() {
		return radbtnNewtonMethod.isSelected();
	}

	/**
	 * Gets the command that represents the Simple Iteration radio button.
	 * 
	 * @return			Simple Iteration radio button command
	 */
	protected static String getSimpleIterationCommand() {
		return radbtnSimpleIterationCommand;
	}
	
	/**
	 * Gets the status of the Simple Iteration radio button.
	 * 
	 * @return			Simple Iteration radio button status
	 */
	protected static boolean getSimpleIterationRBStatus() {
		return radbtnSimple.isSelected();
	}
	
	/**
	 * Gets the current selection of the radio buttons for solving
	 * Kepler's equation
	 * 
	 * @return				ASTKepler.TrueAnomalyType
	 */
	protected static ASTKepler.TrueAnomalyType getTrueAnomalyRBStatus() {
		if (radbtnSimple.isSelected()) return ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else return ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
	}
	
	/**
	 * Sets the Newton's Method radio button to true
	 */
	protected static void setNewtonMethodRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllKeplerEQRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnNewtonMethod.setSelected(true);
	}

	/**
	 * Sets the Simple Iteration radio button to true
	 */
	protected static void setSimpleIterationRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllKeplerEQRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnSimple.setSelected(true);
	}

	/*===============================================================================
	 * The methods below return the command associated with a specific menu or
	 * GUI button. This is done to separate the actual string in the GUI that
	 * represents an action (e.g., "Exit") from the semantics of the action that is
	 * to occur.
	 *==============================================================================*/

	/**
	 * Gets the command that represents the Solar Sys Info, Data Files, and Star Charts menu items.
	 * 
	 * @return			Coord Sys, TLEs, Launch Sites, and Satellites command
	 */
	protected static String getChapMenuCommand() {
		return 	chapMenuCommand;
	}
	
	/**
	 * Gets the command that represents the Exit menu item.
	 * 
	 * @return			Exit command
	 */
	protected static String getExitCommand() {
		return exitCommand;
	}

	/**
	 * Gets the command that represents the Instructions menu item.
	 * 
	 * @return			Instructions command
	 */
	protected static String getInstructionsCommand() {
		return instructionsCommand;
	}

	/**
	 * Gets the command that represents the About menu item.
	 * 
	 * @return			About command
	 */
	protected static String getAboutCommand() {
		return aboutCommand;
	}
	
	/*=======================================================================
	 * The methods below conditionally print to the scrollable text area
	 *======================================================================*/

	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.print(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 */
	protected static void printlnCond() {
		if (chkboxShowInterimCalcs.isSelected()) prt.println();
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printlnCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 * @param centerTxt		true if the text is to be centered
	 */
	protected static void printlnCond(String txt, boolean centerTxt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt,centerTxt);
	}
	
	/*========================
	 * AboutBox methods
	 *=======================*/

	/**
	 * Shows the About Box
	 */
	protected static void showAboutBox() {
		aboutBox.showAboutBox();
	}
	
	/*===============================================
	 * Some helper methods that are GUI-related
	 *==============================================*/
	
	/**
	 * See if the observer location, date, and time
	 * currently in the GUI is valid.
	 * 
	 * @return				true if valid, otherwise false
	 */
	protected static boolean validateGUIObsLoc() {
		ASTLatLon.TimeZoneType tZone = getSelectedRBStatus();
		String tzStr;
		
		if (tZone == ASTLatLon.TimeZoneType.LONGITUDE) tzStr = "LON";
		else tzStr = tZone.toString();
		return ASTObserver.isValidObsLoc(observer, txtboxLat.getText(), txtboxLon.getText(), tzStr, txtboxDate.getText(), txtboxLCT.getText());
	}
	
	/*-----------------------------------------------------------------------------
	 * Define some useful helper methods used only in this class
	 *----------------------------------------------------------------------------*/
	
	/**
	 * Sets the observer location in the GUI. This is intended to be
	 * done one time only during the program initialization since a
	 * default location may be read from a data file.
	 * 
	 * @param obs			observer location object
	 */
	private void setObsDataInGUI(ASTObserver obs) {
		txtboxLat.setText(ASTLatLon.latToStr(obs.getObsLocation(), ASTMisc.DMSFORMAT));
		txtboxLon.setText(ASTLatLon.lonToStr(obs.getObsLocation(), ASTMisc.DMSFORMAT));
		txtboxDate.setText(ASTDate.dateToStr(obs.getObsDate()));
		txtboxLCT.setText(ASTTime.timeToStr(obs.getObsTime(), ASTMisc.HMSFORMAT));

		if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.PST) setPSTRadBtn();
		else if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.MST) setMSTRadBtn();
		else if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.CST) setCSTRadBtn();
		else if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.EST) setESTRadBtn();
		else setLonRadBtn();
	}	
}
