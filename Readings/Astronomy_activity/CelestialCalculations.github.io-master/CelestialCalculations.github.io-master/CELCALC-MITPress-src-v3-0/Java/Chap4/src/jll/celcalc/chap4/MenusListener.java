package jll.celcalc.chap4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap4.ChapMenuItems.CalculationType;
import jll.celcalc.chap4.ChapMenuItems.ChapMnuItm;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class MenusListener implements ActionListener {
	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}

		// Handle Coord Systems and Kepler's Equation menus
		else if (action.equals(ChapGUI.getChapMenuCommand())) {
			ChapMnuItm objMenuItem = (ChapMnuItm) e.getSource();		// the menu item the user clicked on

			prt.clearTextArea();

			ChapGUI.setCalcToDo(objMenuItem.getCalcType());

			switch (objMenuItem.getCalcType()) {
			//**** Coord Systems
			case HA_RA:
				ChapGUI.setDataLabels("Hour Angle (hh:mm:ss.ss)", "UT (hh:mm:ss.ss)",
						"Longitude (ex: 96.52E, 30.4W)", "Date (mm/dd/yyyy)");
				ChapGUI.setResults("Convert Hour Angle to Right Ascension");
				break;
			case RA_HA:
				ChapGUI.setDataLabels("Right Ascension (hh:mm:ss.ss)", "UT (hh:mm:ss.ss)",
						"Longitude (ex: 96.52E, 30.4W)", "Date (mm/dd/yyyy)");
				ChapGUI.setResults("Convert Right Ascension to Hour Angle");
				break;
			case HORIZON_EQUATORIAL:
				ChapGUI.setDataLabels("Altitude (xxxd yym zz.zzs)", "Azimuth (xxxd yym zz.zzs)",
						"Latitude (ex: 38.5N, 20.5S)");
				ChapGUI.setResults("Convert Horizon Coordinates to Equatorial Coordinates");
				break;
			case EQUATORIAL_HORIZON:
				ChapGUI.setDataLabels("Hour Angle (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
						"Latitude (ex: 38.5N, 20.5S)");
				ChapGUI.setResults("Convert Equatorial Coordinates to Horizon Coordinates");
				break;
			case ECLIPTIC_EQUATORIAL:
				ChapGUI.setDataLabels("Ecliptic Lat (xxxd yym zz.zzs)", "Ecliptic Lon (xxxd yym zz.zzs)",
						"Epoch Coordinates are in (ex: 2000.0)");
				ChapGUI.setResults("Convert Ecliptic Coordinates to Equatorial Coordinates");
				break;
			case EQUATORIAL_ECLIPTIC:
				ChapGUI.setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
						"Epoch Coordinates are in (ex: 2000.0)");
				ChapGUI.setResults("Convert Equatorial Coordinates to Ecliptic Coordinates");
				break;
			case GALACTIC_EQUATORIAL:
				ChapGUI.setDataLabels("Galactic Lat (xxxd yym zz.zzs)", "Galactic Lon (xxxd yym zz.zzs)",
						"Epoch (1950.0 or 2000.0)");
				ChapGUI.setResults("Convert Galactic Coordinates to Equatorial Coordinates");
				break;
			case EQUATORIAL_GALACTIC:
				ChapGUI.setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
						"Epoch (1950.0 or 2000.0)");
				ChapGUI.setResults("Convert Equatorial Coordinates to Galactic Coordinates");
				break;
			case PRECESSION_CORR:
				ChapGUI.setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
						"Epoch to Convert From (ex: 1950.0)", "Epoch to Convert To (ex: 2000.0)");
				ChapGUI.setResults("Compute Precession Correction");
				break;

				//**** Kepler's Equation menu
			case KEPLER_SIMPLE:
				ChapGUI.setDataLabels("Mean Anomaly (in degrees)", "Stop Criteria in Radians (ex: 0.000002)",
						"Orbital Eccentricity");
				ChapGUI.setResults("Solve Kepler's Equation via Simple Iteration");
				break;
			case KEPLER_NEWTON:
				ChapGUI.setDataLabels("Mean Anomaly (in degrees)", "Stop Criteria in Radians (ex: 0.000002)",
						"Orbital Eccentricity");
				ChapGUI.setResults("Solve Kepler's Equation via Newton/Raphson Method");
				break;
			default:
				ASTMsg.criticalErrMsg("Invalid menu item " + action);
				ChapGUI.setCalcToDo(CalculationType.NO_CALC_SELECTED);
				break;
			}
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			prt.clearTextArea();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " +
					"intermediate results as the various calculations are performed. Next, select the desired " +
					"calculation to perform from the 'Coord Systems' or 'Kepler's Equation' menu. " +
					"Once a menu item is selected, you will be asked to enter one to four lines of data in the text " +
					"boxes beneath the 'Show Intermediate Calculations' checkbox. The values to be entered " +
					"depend upon what menu item was selected. Finally, after entering all required data, click the " +
					"'Calculate' button at the bottom right of the data input area. The selected computation " +
					"(e.g., Precession Correction) will be done.");
			prt.println();

			prt.println("If you checked the 'Show Intermediate Calculations' checkbox, the steps " +
					"required to find a solution will be shown in the scrollable text output area (i.e., where " +
					"these instructions are being displayed). Regardless of whether intermediate results are shown, " +
					"the final result will be shown just below the data input text boxes.");
			prt.println();

			prt.setBoldFont(true);
			prt.println("Notes:");
			prt.setBoldFont(false);
			prt.println("1. Angles can be entered in either decimal or DMS format (e.g., xxxd yym zz.zzs). Time-related " +
					"(e.g., Right Ascension, Hour Angle) values can be entered in decimal or HMS format (i.e., hh:mm:ss.ss).");
			prt.println();

			prt.print("2. The formulas used in the calculations assume that angles are expressed in degrees. However, " +
					"the trig functions provided in most computer languages and on pocket calculators assume angles " +
					"are in radians. So, if necessary, convert degrees to radians before invoking a trig function, " +
					"and convert radians to degrees when inverse trig functions are used.");

			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
