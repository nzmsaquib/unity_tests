package jll.celcalc.chap4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;

/**
 * <b>Implements an action listener for GUI buttons.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class GUIButtonListener implements ActionListener {

	private static GUIBtnActions ba = new GUIBtnActions();

	/**
	 * Handles clicks on individual GUI buttons.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */	
	public void actionPerformed(ActionEvent e) {

		switch (ChapGUI.getCalcToDo()) {
		//**** Coord Systems
		case HA_RA:
			ba.calcHA_RA();
			break;
		case RA_HA:
			ba.calcRA_HA();
			break;
		case HORIZON_EQUATORIAL:
			ba.calcHorizon_Equatorial();
			break;
		case EQUATORIAL_HORIZON:
			ba.calcEquatorial_Horizon();
			break;
		case ECLIPTIC_EQUATORIAL:
			ba.calcEcliptic_Equatorial();
			break;
		case EQUATORIAL_ECLIPTIC:
			ba.calcEquatorial_Ecliptic();
			break;
		case GALACTIC_EQUATORIAL:
			ba.calcGalactic_Equatorial();
			break;
		case EQUATORIAL_GALACTIC:
			ba.calcEquatorial_Galactic();
			break;
		case PRECESSION_CORR:
			ba.calcPrecessionCorr();
			break;

			//**** Kepler's Equation
		case KEPLER_SIMPLE:
			ba.calcKeplerSimple();
			break;
		case KEPLER_NEWTON:
			ba.calcKeplerNewton();
			break;
		default:
			ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
					"from the 'Coord Systems' or 'Kepler's Equation' menu and try again.", "No Menu Item Selected");
			break;
		}
	}
}
