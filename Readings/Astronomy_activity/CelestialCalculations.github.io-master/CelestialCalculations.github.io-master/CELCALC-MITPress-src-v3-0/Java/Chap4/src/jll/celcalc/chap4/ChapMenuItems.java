package jll.celcalc.chap4;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
* This class extends JMenuItem to add some additional fields, which
* simplifies the code for adding items to the menu bar and determining
* what actions to take as various menu items are selected. 
* <p>
* Copyright (c) 2018
* 
* @author J. L. Lawrence
* @version 3.0, 2018
*/

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {
	
	/** Define values for the eCalcType field in a ChapMenuItem class.
	 * These indicate what coordinate system-related conversion to do. */
	
	protected enum CalculationType {
		/** no calculation is currently selected */ NO_CALC_SELECTED,
		// Coord Systems
		/** convert Hour Angle to Right Ascension */ HA_RA,
		/** convert Right Ascension to Hour Angle */ RA_HA,
		/** convert Horizon Coordinates to Equatorial */ HORIZON_EQUATORIAL,
		/** convert Equatorial Coordinates to Horizon */ EQUATORIAL_HORIZON,
		/** convert Ecliptic Coordinates to Equatorial */ ECLIPTIC_EQUATORIAL,
		/** convert Equatorial Coordinates to Ecliptic */ EQUATORIAL_ECLIPTIC,
		/** convert Galactic Coordinates to Equatorial */ GALACTIC_EQUATORIAL,
		/** convert Equatorial Coordinates to Galactic */ EQUATORIAL_GALACTIC,
		/** perform a precession correction */ PRECESSION_CORR,
		// Kepler's Equation
		/** use simple iteration to solve Kepler's equation */ KEPLER_SIMPLE, 
		/** use Newton's method to solve Kepler's equation */ KEPLER_NEWTON
	}
	
	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Coord Systems and Kepler's Equation menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;

		// Coordinate System Conversions menu
		menu = ChapGUI.getCoordSystemsMenu();
		createMenuItem(menu, "Hour Angle to Right Ascension", CalculationType.HA_RA);
		createMenuItem(menu, "Right Ascension to Hour Angle", CalculationType.RA_HA);
		menu.addSeparator();

		createMenuItem(menu, "Horizon to Equatorial", CalculationType.HORIZON_EQUATORIAL);
		createMenuItem(menu, "Equatorial to Horizon", CalculationType.EQUATORIAL_HORIZON);
		menu.addSeparator();

		createMenuItem(menu, "Ecliptic to Equatorial", CalculationType.ECLIPTIC_EQUATORIAL);
		createMenuItem(menu, "Equatorial to Ecliptic", CalculationType.EQUATORIAL_ECLIPTIC);
		menu.addSeparator();

		createMenuItem(menu, "Galactic to Equatorial", CalculationType.GALACTIC_EQUATORIAL);
		createMenuItem(menu, "Equatorial to Galactic", CalculationType.EQUATORIAL_GALACTIC);
		menu.addSeparator();

		createMenuItem(menu, "Precession Correction", CalculationType.PRECESSION_CORR);

		// Kepler's Equation menu
		menu = ChapGUI.getKeplersEqMenu();
		createMenuItem(menu, "Simple Iteration", CalculationType.KEPLER_SIMPLE);
		createMenuItem(menu, "Newton/Raphson", CalculationType.KEPLER_NEWTON);
	}
		
	/** Define a class for what a  menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of calculation to do
		private CalculationType eCalcType;   // what type of calculation to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of calculation is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {
			super(title);
			eCalcType = calcType;
		}
		
		/*=================================================================
		 * Define accessor methods for the menu item fields
		 *================================================================*/

		/**
		 * Gets the type of calculation to perform.
		 * 
		 * @return		the type of calculation to perform.
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}
	
	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 -----------------------------------------------------------------------------*/

	/**
	 * Creates a Coord Systems or Kepler's Equation menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of calculation to perform
	 */     
	private void createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getChapMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());	
	}

}
