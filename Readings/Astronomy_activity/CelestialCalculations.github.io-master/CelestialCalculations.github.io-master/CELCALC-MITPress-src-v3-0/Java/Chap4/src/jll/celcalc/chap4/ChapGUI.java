package jll.celcalc.chap4;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;

import jll.celcalc.ASTUtils.ASTAboutBox;
import jll.celcalc.ASTUtils.ASTBookInfo;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.chap4.ChapMenuItems.CalculationType;

/**
 * <b>Implements the main GUI</b>
 * <p>
 * The GUI was created with, and is maintained by, the Eclipse WindowBuilder.
 * All methods and data are declared static because it only makes sense to have
 * one main GUI per application, and making them static avoids the problem of
 * having to pass a reference to the GUI instance in other classes that need
 * to reference the main GUI.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 * <p>
 * Note that the Eclipse Window Builder has trouble with complex, dynamic GUIs so that
 * one should first build the GUI, then modify the code by hand to use
 * the definitions in ASTUtils.ASTStyle. The Window Builder start/stop hiding tags can be used to
 * surround code that the Window Builder parser has trouble handling.
 * The start hiding tag is <code>{@literal //$hide>>$}</code> while the stop
 * hiding tag is <code>{@literal //$hide<<$}</code>. For example,
 * <p>
 * <code>
 * {@literal //$hide>>$}
 * 		code to be hidden
 * {@literal //$hide<<$}
 * </code>
 */

@SuppressWarnings("serial")
class ChapGUI extends JFrame {
	private static final String WINDOW_TITLE = "Chapter 4 - Coordinate Sys Conversions";
	
	// Save the scrollable text pane area for output
	private static JTextPane textPane = new JTextPane();
	private static ASTPrt prt = null;				// create instance (actually done below) for printing to the output textPane
	
	// Create an About Box
	private static ASTAboutBox aboutBox = new ASTAboutBox(WINDOW_TITLE);
	
	// Create menu items, which will be added to in the ChapMenuItems class
	private static JMenu mnCoordSystems = null;
	private static JMenu mnKeplersEq = null;
	
	// Each menu item selection sets this variable to know what to do when the calculate button is pressed
	private static CalculationType calculationToDo = CalculationType.NO_CALC_SELECTED;	
	
	// Create listeners for the various GUI components
	private static ActionListener listenerMenus = new MenusListener();
	private static ActionListener listenerGUIBtn = new GUIButtonListener();
	
	// Various GUI components that user will interact with
	private static JCheckBox chkboxShowInterimCalcs = new JCheckBox("Show Intermediate Calculations");
	private static JTextField txtboxData1 = new JTextField("1234");
	private static JLabel lblData1 = new JLabel("Data 1");
	private static JTextField txtboxData2 = new JTextField("1234");
	private static JLabel lblData2 = new JLabel("Data 2");
	private static JTextField txtboxData3 = new JTextField("1234");
	private static JLabel lblData3 = new JLabel("Data 3");
	private static JTextField txtboxData4 = new JTextField("1234");
	private static JLabel lblData4 = new JLabel("Data 4");
	private static JLabel lblResults = new JLabel("Results");

	/*============================================================================================
	 * The string text collected in these next items is done to separate the strings displayed
	 * in the various GUI components (such as buttons and menus) from the semantics of what 
	 * clicking on a component means. This is important because otherwise, a change to the text
	 * displayed in the GUI, such as for a button label, via Eclipse WindowBuilder may well 
	 * break other code, particularly the action listeners. Through this technique, the 
	 * WindowBuilder can be used to change the label on a GUI element (e.g., button) without
	 * breaking any other code. The command that a listener will receive is the same regardless
	 * of what the actual label on the GUI component says. So, for example, the text "Exit"
	 * be changed in the menu to "Quit" without breaking the menu's listener because the
	 * listener will receive the command exitCommand regardless of what text is actually
	 * displayed in the menu. Get methods below (e.g., getExitCommand) return the appropriate
	 * command for the listeners to use.
	 *===========================================================================================*/	
	/* define menu item commands */
	private static final String exitCommand = "exit";
	private static final String instructionsCommand = "instruct";
	private static final String aboutCommand = "about";
	private static final String chapMenuCommand = "chapmenucommand";
	
	/* define button controls */
	private static final String calculateCommand = "calculate";

	/**
	 * Create the GUI frame.
	 */
	protected ChapGUI() {
		JPanel contentPane = new JPanel();
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChapGUI.class.getResource("/resources/BlueMarble.png")));
		setTitle(WINDOW_TITLE);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 840, 750);
		contentPane.setDoubleBuffered(false);
		contentPane.setFocusable(false);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		/*====================== Create Menus ======================*/
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(ASTStyle.MENU_FONT);
		setJMenuBar(menuBar);

		// File menu
		JMenu mnFile = new JMenu(" File ");
		mnFile.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setFont(ASTStyle.MENU_FONT);
		mntmExit.setActionCommand(exitCommand);
		mntmExit.addActionListener(listenerMenus);
		mnFile.add(mntmExit);
		
		// Coord Systems menu
		mnCoordSystems = new JMenu(" Coord Systems ");
		mnCoordSystems.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnCoordSystems);

		// Kepler's Equation menu
		mnKeplersEq = new JMenu(" Kepler's Equation ");
		mnKeplersEq.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnKeplersEq);
		
		// This much simpler code will populate the previous 2 menus via the separate ChapMenuItems class,
		// but at the price of not being able to see the menu via WindowBuilder
		// No need to store the menu items created because the results of the "new" is never referenced elsewhere
		new ChapMenuItems();

		// Help menu
		JMenu mnHelp = new JMenu(" Help ");
		mnHelp.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnHelp);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setFont(ASTStyle.MENU_FONT);
		mntmInstructions.setActionCommand(instructionsCommand);
		mntmInstructions.addActionListener(listenerMenus);
		mnHelp.add(mntmInstructions);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setFont(ASTStyle.MENU_FONT);
		mntmAbout.addActionListener(listenerMenus);
		mntmAbout.setActionCommand(aboutCommand);
		mnHelp.add(mntmAbout);
		/*====================== End of Menus ======================*/		

		setContentPane(contentPane);
		
		JPanel panelBookTitle = new JPanel();
		panelBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		panelBookTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelBookTitle.setFocusable(false);
		panelBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.setDoubleBuffered(false);
		
		JScrollPane scrollPaneOutput = new JScrollPane();
		scrollPaneOutput.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setVerifyInputWhenFocusTarget(false);
		scrollPaneOutput.setRequestFocusEnabled(false);
		scrollPaneOutput.setFocusable(false);
		scrollPaneOutput.setFocusTraversalKeysEnabled(false);
		
		JPanel panelChkBox = new JPanel();
		
		JPanel panelInputData = new JPanel();
		panelInputData.setAlignmentY(Component.TOP_ALIGNMENT);
		
		lblResults.setFont(ASTStyle.TEXT_BOLDFONT);
		lblResults.setHorizontalAlignment(SwingConstants.LEFT);
		lblResults.setAlignmentY(Component.TOP_ALIGNMENT);
		lblResults.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JButton btnCalculate = new JButton("Calculate");
		btnCalculate.setFont(ASTStyle.BTN_FONT);
		btnCalculate.addActionListener(listenerGUIBtn);
		btnCalculate.setActionCommand(calculateCommand);
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addComponent(panelBookTitle, GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
				.addComponent(panelChkBox, GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
				.addComponent(panelInputData, GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblResults, GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE)
					.addGap(35)
					.addComponent(btnCalculate)
					.addGap(23))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 708, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panelBookTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelChkBox, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelInputData, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnCalculate)
						.addComponent(lblResults))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		txtboxData1.setFont(ASTStyle.TEXT_FONT);
		txtboxData1.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxData1.setColumns(10);
		
		txtboxData3.setFont(ASTStyle.TEXT_FONT);
		txtboxData3.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxData3.setColumns(10);
		
		lblData1.setFont(ASTStyle.TEXT_FONT);
		lblData1.setHorizontalAlignment(SwingConstants.LEFT);
		
		lblData3.setFont(ASTStyle.TEXT_FONT);
		lblData3.setHorizontalAlignment(SwingConstants.LEFT);
		
		txtboxData2.setFont(ASTStyle.TEXT_FONT);
		txtboxData2.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxData2.setColumns(10);
		
		txtboxData4.setFont(ASTStyle.TEXT_FONT);
		txtboxData4.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxData4.setColumns(10);
		
		lblData2.setFont(ASTStyle.TEXT_FONT);
		lblData2.setHorizontalAlignment(SwingConstants.LEFT);
		
		lblData4.setFont(ASTStyle.TEXT_FONT);
		lblData4.setHorizontalAlignment(SwingConstants.LEFT);
		GroupLayout gl_panelInputData = new GroupLayout(panelInputData);
		gl_panelInputData.setHorizontalGroup(
			gl_panelInputData.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelInputData.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelInputData.createParallelGroup(Alignment.LEADING, false)
						.addComponent(txtboxData3)
						.addComponent(txtboxData1, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelInputData.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblData1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblData3, GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panelInputData.createParallelGroup(Alignment.LEADING, false)
						.addComponent(txtboxData4)
						.addComponent(txtboxData2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panelInputData.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblData4, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
						.addComponent(lblData2, GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panelInputData.setVerticalGroup(
			gl_panelInputData.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelInputData.createSequentialGroup()
					.addGroup(gl_panelInputData.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panelInputData.createSequentialGroup()
							.addGroup(gl_panelInputData.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtboxData1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblData1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panelInputData.createParallelGroup(Alignment.LEADING)
								.addComponent(txtboxData3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblData3, GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)))
						.addGroup(gl_panelInputData.createSequentialGroup()
							.addGroup(gl_panelInputData.createParallelGroup(Alignment.LEADING)
								.addComponent(txtboxData2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panelInputData.createSequentialGroup()
									.addGap(3)
									.addComponent(lblData2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panelInputData.createParallelGroup(Alignment.LEADING)
								.addComponent(lblData4)
								.addComponent(txtboxData4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panelInputData.setLayout(gl_panelInputData);
		
		chkboxShowInterimCalcs.setSelected(false);
		chkboxShowInterimCalcs.setHorizontalAlignment(SwingConstants.CENTER);
		chkboxShowInterimCalcs.setAlignmentY(Component.TOP_ALIGNMENT);
		chkboxShowInterimCalcs.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkboxShowInterimCalcs.setFocusable(false);
		chkboxShowInterimCalcs.setFocusTraversalKeysEnabled(false);
		chkboxShowInterimCalcs.setFocusPainted(false);
		chkboxShowInterimCalcs.setFont(ASTStyle.CBOX_FONT);
		chkboxShowInterimCalcs.setSelected(false);
		
		panelChkBox.add(chkboxShowInterimCalcs);
		
		panelBookTitle.setLayout(new BorderLayout(0, 0));		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setVerifyInputWhenFocusTarget(false);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookTitle.setFont(ASTStyle.BOOK_TITLE_BOLDFONT);
		lblBookTitle.setFocusable(false);
		lblBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.add(lblBookTitle, BorderLayout.CENTER);
		contentPane.setLayout(gl_contentPane);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		
		// Define the actual text area and add it to scrollPaneOutput
		textPane.setFont(ASTStyle.OUT_TEXT_FONT);
		textPane.setFocusable(false);
		textPane.setFocusTraversalKeysEnabled(false);
		textPane.setFocusCycleRoot(false);
		textPane.setEditable(false);
		scrollPaneOutput.setViewportView(textPane);
		
		// Initialize the rest of the GUI
		ASTStyle.setASTStyle();
		aboutBox.pack();
		aboutBox.setParentFrame(this);	
		
		// Set parent frame for static methods that need to center a pane/frame/etc. on the GUI
		ASTQuery.setParentFrame(this);
		ASTMsg.setParentFrame(this);
		
		textPane.setText("     "); // must initialize with at least a few blanks
		prt = new ASTPrt(textPane);
		clearAllTextAreas();
		
		calculationToDo = CalculationType.NO_CALC_SELECTED;
		chkboxShowInterimCalcs.setSelected(false);
		
		// Explicitly indicate the order in which we want the GUI components traversed
		Vector<Component> order = new Vector<Component>(5);
		order.add(txtboxData1); order.add(txtboxData2); order.add(txtboxData3);
		order.add(txtboxData4); order.add(btnCalculate);
		ASTStyle.GUIFocusTraversalPolicy GUIPolicy = new ASTStyle.GUIFocusTraversalPolicy(order);
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setFocusTraversalPolicy(GUIPolicy);
	}
	
	/*=============================================================================
	 * The methods below return references to various GUI components such as the
	 * scrollable output text area. 
	 *============================================================================*/
	
	/**
	 * Gets the Coord Systems menu.
	 * 
	 * @return		the Coord Systems menu
	 */
	protected static JMenu getCoordSystemsMenu() {
		return mnCoordSystems;
	}
	
	/**
	 * Gets the Kepler's Equation menu.
	 * 
	 * @return		the Kepler's Equation menu
	 */
	protected static JMenu getKeplersEqMenu() {
		return mnKeplersEq;
	}
	
	/**
	 * Gets the listener for the menu items
	 * 
	 * @return		the action listener for handling the menus
	 */
	protected static ActionListener getMenuListener() {
		return listenerMenus;
	}
	
	/**
	 * Gets the ASTPrt instance for this application's scrollable text pane area.
	 * 
	 * @return		the ASTPrt instance for this application
	 */
	protected static ASTPrt getPrtInstance() {
		return prt;
	}
	
	/**
	 * Gets the scrollable text pane area for this GUI.
	 * 
	 * @return		the JTextPane for this GUI
	 */
	protected static JTextPane getTextPane() {
		return textPane;
	}
	
	/*=============================================================================
	 * The methods below are 'getters' and 'setters' for various items in the GUI.
	 *============================================================================*/
	
	/**
	 * Clears all of the text areas in the GUI
	 */
	protected static void clearAllTextAreas() {
		setDataLabels("","","","");
		prt.clearTextArea();	
		setResults("");
	}
	
	/**
	 * Gets the calculation to perform based upon the most recent menu selection.
	 * 
	 * @return			calculation to do
	 */
	protected static CalculationType getCalcToDo() {
		return calculationToDo;
	}
	
	/**
	 * Gets the string data associated with the 1st input area
	 * 
	 * @return		string from the 1st input area in the GUI
	 */
	protected static String getData1() {
		return txtboxData1.getText();
	}

	/**
	 * Gets the string data associated with the 2nd input area
	 * 
	 * @return		string from the 2nd input area in the GUI
	 */
	protected static String getData2() {
		return txtboxData2.getText();
	}
	
	/**
	 * Gets the string data associated with the 3rd input area
	 * 
	 * @return		string from the 3rd input area in the GUI
	 */
	protected static String getData3() {
		return txtboxData3.getText();
	}

	/**
	 * Gets the string data associated with the 4th input area
	 * 
	 * @return		string from the 4th input area in the GUI
	 */
	protected static String getData4() {
		return txtboxData4.getText();
	}

	/**
	 * Saves the calculation to perform based upon the just selected menu item
	 * 
	 * @param calcToDo		which calculation to perform
	 */
	protected static void setCalcToDo(CalculationType calcToDo) {
		calculationToDo = calcToDo;
	}
	
	/**
	 * Sets the data labels in the GUI
	 * 
	 * @param s1	string for the 1st data label in the GUI
	 */
	protected static void setDataLabels(String s1) {
		setDataLabels(s1,"","","");
		txtboxData2.setFocusable(false);
		txtboxData2.setFocusTraversalKeysEnabled(false);
		txtboxData3.setFocusable(false);
		txtboxData3.setFocusTraversalKeysEnabled(false);
		txtboxData4.setFocusable(false);
		txtboxData4.setFocusTraversalKeysEnabled(false);
	}

	/**
	 * Sets the data labels in the GUI
	 * 
	 * @param s1	string for the 1st data label in the GUI
	 * @param s2	string for the 2nd data label in the GUI
	 */
	protected static void setDataLabels(String s1,String s2) {
		setDataLabels(s1,s2,"","");
		txtboxData3.setFocusable(false);
		txtboxData3.setFocusTraversalKeysEnabled(false);
		txtboxData4.setFocusable(false);
		txtboxData4.setFocusTraversalKeysEnabled(false);
	}
	
	/**
	 * Sets the data labels in the GUI
	 * 
	 * @param s1	string for the 1st data label in the GUI
	 * @param s2	string for the 2nd data label in the GUI
	 * @param s3	string for the 3rd data label in the GUI
	 */
	protected static void setDataLabels(String s1,String s2,String s3) {
		setDataLabels(s1,s2,s3,"");
		txtboxData4.setFocusable(false);
		txtboxData4.setFocusTraversalKeysEnabled(false);
	}
	
	/**
	 * Sets the data labels in the GUI
	 * 
	 * @param s1	string for the 1st data label in the GUI
	 * @param s2	string for the 2nd data label in the GUI
	 * @param s3	string for the 3rd data label in the GUI
	 * @param s4	string for the 4th data label in the GUI
	 */
	protected static void setDataLabels(String s1,String s2,String s3,String s4) {
		lblData1.setText(s1);
		lblData2.setText(s2);
		lblData3.setText(s3);
		lblData4.setText(s4);
		setResults("");
		
		// Must also clear the input data areas and set focus
		txtboxData1.setText("");
		txtboxData1.setFocusable(true);
		txtboxData1.setFocusTraversalKeysEnabled(true);
		txtboxData1.requestFocus(true);
		
		txtboxData2.setText("");
		txtboxData2.setFocusable(true);
		txtboxData2.setFocusTraversalKeysEnabled(true);
		
		txtboxData3.setText("");
		txtboxData3.setFocusable(true);
		txtboxData3.setFocusTraversalKeysEnabled(true);
		
		txtboxData4.setText("");
		txtboxData4.setFocusable(true);
		txtboxData4.setFocusTraversalKeysEnabled(true);
	}
	
	/**
	 * Sets the results label in the GUI
	 * 
	 * @param result	string for the results label in the GUI
	 */
	protected static void setResults(String result) {
		lblResults.setText(result);
	}

	/*===============================================================================
	 * The methods below return the command associated with a specific menu or
	 * GUI button. This is done to separate the actual string in the GUI that
	 * represents an action (e.g., "Exit") from the semantics of the action that is
	 * to occur.
	 *==============================================================================*/

	/**
	 * Gets the command that represents the Coord Sys and Kepler's Eq menu items.
	 * 
	 * @return			Coord Sys and Kepler's Eq command
	 */
	protected static String getChapMenuCommand() {
		return 	chapMenuCommand;
	}
	
	/**
	 * Gets the command that represents the Exit menu item.
	 * 
	 * @return			Exit command
	 */
	protected static String getExitCommand() {
		return exitCommand;
	}

	/**
	 * Gets the command that represents the Instructions menu item.
	 * 
	 * @return			Instructions command
	 */
	protected static String getInstructionsCommand() {
		return instructionsCommand;
	}

	/**
	 * Gets the command that represents the About menu item.
	 * 
	 * @return			About command
	 */
	protected static String getAboutCommand() {
		return aboutCommand;
	}
	
	/*======================================================================
	 * The methods below conditionally print to the scrollable text area
	 *=====================================================================*/

	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.print(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 */
	protected static void printlnCond() {
		if (chkboxShowInterimCalcs.isSelected()) prt.println();
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printlnCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 * @param centerTxt		true if the text is to be centered
	 */
	protected static void printlnCond(String txt, boolean centerTxt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt,centerTxt);
	}
	
	/*========================
	 * AboutBox methods
	 *=======================*/
	
	/**
	 * Shows the About Box
	 */
	protected static void showAboutBox() {
		aboutBox.showAboutBox();
	}
}
