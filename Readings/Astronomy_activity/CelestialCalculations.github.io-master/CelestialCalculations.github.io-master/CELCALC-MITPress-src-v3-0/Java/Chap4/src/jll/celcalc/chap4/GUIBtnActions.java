package jll.celcalc.chap4;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.*;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Calculate button</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class GUIBtnActions {

	// Galactic coordinates for Epoch 1950.0
	private static final double GalEpoch1950 = 1950.0;
	private static final double GalRA1950 = 192.25;					// in degrees
	private static final double GalDecl1950 = 27.4;					// in degrees
	private static final double GalAscNode1950 = 33.0;				// in degrees

	// Galactic coordinates for Epoch J2000
	private static final double GalEpoch2000 = 2000.0;
	private static final double GalRA2000 = 192.8598;				// in degrees
	private static final double GalDecl2000 = 27.128027;			// in degrees
	private static final double GalAscNode2000 = 32.9319;			// in degrees

	/*=================================================================
	 * Routines to do the actual work for the Coord Systems menu
	 *================================================================*/

	/**
	 * Convert Ecliptic Coordinates to Equatorial Coordinates
	 */
	protected void calcEcliptic_Equatorial() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String EclLatInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String EclLonInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String EpochInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		double Epoch;
		String result, strtmp;
		double dEclLat, dEclLon, dE, dT, dDecl, dY, dX, dR, dQA, dRA;

		prt.clearTextArea();

		// Validate the Ecliptic Latitude and Longitude
		ASTAngle eclLatObj = ASTAngle.isValidAngle(EclLatInput,ASTMisc.HIDE_ERRORS);
		if (eclLatObj.isValidAngleObj()) {
			strtmp = ASTAngle.angleToStr(eclLatObj, ASTMisc.DMSFORMAT) + " Ecl Lat";
			dEclLat = eclLatObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Ecliptic Latitude was entered - try again", "Invalid Ecl Lat");
			return;
		}
		ASTAngle eclLonObj = ASTAngle.isValidAngle(EclLonInput,ASTMisc.HIDE_ERRORS);
		if (eclLonObj.isValidAngleObj()) {
			strtmp = strtmp + ", " + ASTAngle.angleToStr(eclLonObj, ASTMisc.DMSFORMAT) + " Ecl Lon";
			dEclLon = eclLonObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Ecliptic Longitude was entered - try again", "Invalid Ecl Lon");
			return;
		}

		// Validate the Epoch
		ASTReal rTmp = ASTReal.isValidReal(EpochInput,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) {
			Epoch = rTmp.getRealValue();
			strtmp = strtmp + ", Epoch " + Epoch;
		} else {
			ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch");
			return;
		}

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + strtmp + " to Equatorial Coordinates", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		dE = computeEclipticObliquity(Epoch);

		ChapGUI.printlnCond("5.  Convert Ecliptic Latitude to decimal format.");
		ChapGUI.printlnCond("    Ecl Lat = " + ASTAngle.angleToStr(dEclLat, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  Convert Ecliptic Longitude to decimal format.");
		ChapGUI.printlnCond("    Ecl Lon = " + ASTAngle.angleToStr(dEclLon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dT = ASTMath.SIN_D(dEclLat) * ASTMath.COS_D(dE) + ASTMath.COS_D(dEclLat) * ASTMath.SIN_D(dE) * ASTMath.SIN_D(dEclLon);
		ChapGUI.printlnCond("7.  Compute T = sin(Ecl Lat) * cos(E) + cos(Ecl Lat) * sin(E) * sin(Ecl Lon)");
		ChapGUI.printlnCond("    where E is the obliquity of the ecliptic for the given Epoch");
		ChapGUI.printlnCond("    T = sin(" + String.format(ASTStyle.decDegFormat,dEclLat) + ")*cos(" + 
				String.format(ASTStyle.decDegFormat,dE) + ") + cos(" + String.format(ASTStyle.decDegFormat,dEclLat) + 
				")*sin(" + String.format(ASTStyle.decDegFormat,dE) +
				")*sin(" + String.format(ASTStyle.decDegFormat,dEclLon) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dT));
		ChapGUI.printlnCond();

		dDecl = ASTMath.INVSIN_D(dT);
		ChapGUI.printlnCond("8.  Decl = inv sin(T) = inv sin(" + String.format(ASTStyle.genFloatFormat,dT) + ") = " +
				String.format(ASTStyle.decDegFormat,dDecl) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  Y = sin(Ecl Lon) * cos(E) - tan(Ecl Lat) * sin(E)");
		ChapGUI.printlnCond("      = sin(" + String.format(ASTStyle.decDegFormat,dEclLon) + ")*cos(" + 
				String.format(ASTStyle.decDegFormat,dE) + ") - tan(" + String.format(ASTStyle.decDegFormat,dEclLat) +
				")*sin(" + String.format(ASTStyle.decDegFormat,dE) + ")");
		dY = ASTMath.SIN_D(dEclLon) * ASTMath.COS_D(dE) - ASTMath.TAN_D(dEclLat) * ASTMath.SIN_D(dE);
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dY));
		ChapGUI.printlnCond();

		dX = ASTMath.COS_D(dEclLon);
		ChapGUI.printlnCond("10. X = cos(Ecl Lon) = cos(" + String.format(ASTStyle.decDegFormat,dEclLon) + 
				") = " + String.format(ASTStyle.genFloatFormat,dX));
		ChapGUI.printlnCond();

		dR = ASTMath.INVTAN_D(dY / dX);
		ChapGUI.printlnCond("11. R = inv tan(Y / X) = inv tan(" + String.format(ASTStyle.genFloatFormat,dY) +
				" / " + String.format(ASTStyle.genFloatFormat,dX) + ") = " + String.format(ASTStyle.decDegFormat,dR));
		ChapGUI.printlnCond();

		dQA = ASTMath.quadAdjust(dY, dX);
		dRA = dR + dQA;
		ChapGUI.printlnCond("12. RAdeg = R + quadrant adjustment factor");
		ChapGUI.printlnCond("    Since X = " + String.format(ASTStyle.genFloatFormat,dX) + " and Y = " + 
				String.format(ASTStyle.genFloatFormat,dY) + ",");
		ChapGUI.printlnCond("    the quadrant adjustment factor is " + String.format(ASTStyle.decDegFormat,dQA));
		ChapGUI.printlnCond("    Thus, RAdeg = " + String.format(ASTStyle.decDegFormat,dRA) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("13. Divide RAdeg by 15 to convert to hours, so RA = " +
				String.format(ASTStyle.decDegFormat,dRA) + "/15 = " + 
				String.format(ASTStyle.genFloatFormat,dRA / 15.0) + " hours");
		dRA = dRA / 15.0;
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("14. Finally, convert RA to HMS format and Decl to DMS format.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(dRA, ASTMisc.HMSFORMAT) + ", Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTAngle.angleToStr(eclLatObj, ASTMisc.DMSFORMAT) + " Ecl Lat, " + 
				ASTAngle.angleToStr(eclLonObj, ASTMisc.DMSFORMAT) + " Ecl Lon = " +
				ASTTime.timeToStr(dRA, ASTMisc.HMSFORMAT) + " RA, " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT) + " Decl";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Equatorial Coordinates to Ecliptic Coordinates
	 */
	protected void calcEquatorial_Ecliptic() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String RAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String DeclInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String EpochInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		double Epoch;
		String result, strtmp;
		double dRA, dDecl, dEclLat, dEclLon, dE, dT, dY, dX, dQA, dR;

		prt.clearTextArea();

		// Validate RA and Declination
		ASTTime RAObj = ASTTime.isValidTime(RAInput,ASTMisc.HIDE_ERRORS);
		if (RAObj.isValidTimeObj()) {
			strtmp = "RA " + ASTTime.timeToStr(RAObj, ASTMisc.HMSFORMAT);
			dRA = RAObj.getDecTime();
		} else {
			ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(DeclInput,ASTMisc.HIDE_ERRORS);
		if (declObj.isValidAngleObj()) {
			strtmp = strtmp + ", Decl " + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT);
			dDecl = declObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination");
			return;
		}

		// Validate the Epoch
		ASTReal rTmp = ASTReal.isValidReal(EpochInput,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) {
			Epoch = rTmp.getRealValue();
			strtmp = strtmp + ", Epoch " + Epoch;
		} else {
			ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch");
			return;
		}

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + strtmp + " to Ecliptic Coordinates", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		dE = computeEclipticObliquity(Epoch);

		ChapGUI.printlnCond("5.  Convert RA to decimal format. RA = " + ASTTime.timeToStr(dRA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  Multiply RA by 15 to convert to degrees.");
		ChapGUI.printlnCond("    RAdeg = " + String.format(ASTStyle.genFloatFormat,dRA) + " * 15 = " +
				String.format(ASTStyle.genFloatFormat,dRA * 15.0) + " degrees");
		dRA = dRA * 15.0;
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("7.  Convert Decl to decimal format. Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dT = ASTMath.SIN_D(dDecl) * ASTMath.COS_D(dE) - ASTMath.COS_D(dDecl) * ASTMath.SIN_D(dE) * ASTMath.SIN_D(dRA);
		ChapGUI.printlnCond("8.  Compute T = sin(Decl) * cos(E) - cos(Decl) * sin(E) * sin(RAdeg)");
		ChapGUI.printlnCond("    where E is the obliquity of the ecliptic for the given Epoch");
		ChapGUI.printlnCond("    T = sin(" + String.format(ASTStyle.genFloatFormat,dDecl) + ")*cos(" + 
				String.format(ASTStyle.decDegFormat,dE) + ") - cos(" + String.format(ASTStyle.genFloatFormat,dDecl) + 
				")*sin(" + String.format(ASTStyle.decDegFormat,dE) +
				")*sin(" + String.format(ASTStyle.genFloatFormat,dRA) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dT));
		ChapGUI.printlnCond();

		dEclLat = ASTMath.INVSIN_D(dT);
		ChapGUI.printlnCond("9.  Ecl Lat = inv sin(T) = inv sin(" + String.format(ASTStyle.genFloatFormat,dT) + ") = " +
				String.format(ASTStyle.decDegFormat,dEclLat) + " degrees");
		ChapGUI.printlnCond();

		dY = ASTMath.SIN_D(dRA) * ASTMath.COS_D(dE) + ASTMath.TAN_D(dDecl) * ASTMath.SIN_D(dE);
		ChapGUI.printlnCond("10. Y = sin(RAdeg) * cos(E) + tan(Decl) * sin(E)");
		ChapGUI.printlnCond("      = sin(" + String.format(ASTStyle.decDegFormat,dRA) + ") * cos(" +
				String.format(ASTStyle.genFloatFormat,dE) + ") + tan(" + 
				String.format(ASTStyle.decDegFormat,dDecl) + ") * sin(" +
				String.format(ASTStyle.genFloatFormat,dE) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dY));
		ChapGUI.printlnCond();

		dX = ASTMath.COS_D(dRA);
		ChapGUI.printlnCond("11. X = cos(RAdeg) = cos(" + String.format(ASTStyle.decDegFormat,dRA) + ") = " + 
				String.format(ASTStyle.genFloatFormat,dX));
		ChapGUI.printlnCond();

		dR = ASTMath.INVTAN_D(dY / dX);
		ChapGUI.printlnCond("12. R = inv tan (Y / X) = inv tan(" + String.format(ASTStyle.genFloatFormat,dY) + " / " +
				String.format(ASTStyle.genFloatFormat,dX) + ") = " + String.format(ASTStyle.genFloatFormat,dR) + " degrees");
		ChapGUI.printlnCond();

		dQA = ASTMath.quadAdjust(dY, dX);
		dEclLon = dR + dQA;
		ChapGUI.printlnCond("13. Ecl Lon = R + quadrant adjustment factor");
		ChapGUI.printlnCond("    Since X = " + String.format(ASTStyle.genFloatFormat,dX) + " and Y = " + 
				String.format(ASTStyle.genFloatFormat,dY) + ",");
		ChapGUI.printlnCond("    the quadrant adjustment factor is " + String.format(ASTStyle.genFloatFormat,dQA));
		ChapGUI.printlnCond("    Thus, Ecl Lon = " + String.format(ASTStyle.decDegFormat,dEclLon) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("14. Finally, convert Ecl Lat and Ecl Lon to DMS format.");
		ChapGUI.printlnCond("    Ecl Lat = " + ASTAngle.angleToStr(dEclLat, ASTMisc.DMSFORMAT) + 
				", Ecl Lon = " + ASTAngle.angleToStr(dEclLon, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTTime.timeToStr(RAObj, ASTMisc.HMSFORMAT) + " RA, " + 
				ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl = " +
				ASTAngle.angleToStr(dEclLat, ASTMisc.DMSFORMAT) + " Ecl Lat, " + 
				ASTAngle.angleToStr(dEclLon, ASTMisc.DMSFORMAT) + " Ecl Lon";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Equatorial Coordinates to Galactic Coordinates
	 */
	protected void calcEquatorial_Galactic() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String RAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String DeclInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String GalEpochInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		String result;
		double dRA, dDecl, dGalLat, dGalLon, dT, dY, dX, dQA;
		double GalEpoch, GalRA, GalDecl, GalAscNode;

		prt.clearTextArea();

		// Validate RA and Declination
		ASTTime RAObj = ASTTime.isValidTime(RAInput,ASTMisc.HIDE_ERRORS);
		if (RAObj.isValidTimeObj()) {
			result = ASTTime.timeToStr(RAObj, ASTMisc.HMSFORMAT) + " RA, ";
			dRA = RAObj.getDecTime();
		} else {
			ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(DeclInput,ASTMisc.HIDE_ERRORS);
		if (declObj.isValidAngleObj()) {
			result = result + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl";
			dDecl = declObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination");
			return;
		}

		// Validate Epoch
		ASTReal rTmp = ASTReal.isValidReal(GalEpochInput,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch");
			return;
		}
		GalEpoch = rTmp.getRealValue();
		if (GalEpoch == GalEpoch1950) {				// use Epoch 1950.0 values
			GalRA = GalRA1950;
			GalDecl = GalDecl1950;
			GalAscNode = GalAscNode1950;
		} else if (GalEpoch == GalEpoch2000) {		// use Epoch J2000 values
			GalRA = GalRA2000;
			GalDecl = GalDecl2000;
			GalAscNode = GalAscNode2000;
		} else {
			ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch");
			return;
		}

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " (Epoch " + 
				String.format(ASTStyle.epochFormat,GalEpoch) + ")", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond("to Galactic Coordinates (Epoch " + 
				String.format(ASTStyle.epochFormat,GalEpoch) + ")", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Convert RA to decimal format. RA = " + ASTTime.timeToStr(dRA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		dRA = dRA * 15.0;
		ChapGUI.printlnCond("2.  Multiply RA by 15 to convert to degrees. RAdeg = " + 
				ASTAngle.angleToStr(dRA, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3.  Convert Decl to decimal format. Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dGalLat = ASTMath.COS_D(dDecl) * ASTMath.COS_D(GalDecl) * ASTMath.COS_D(dRA - GalRA) + 
				ASTMath.SIN_D(dDecl) * ASTMath.SIN_D(GalDecl);
		ChapGUI.printlnCond("4.  Compute T0 = cos(Decl) * cos(" + GalDecl + " * cos(RAdeg - " + GalRA +
				") + sin(Decl) * sin(" + GalDecl + ")");
		ChapGUI.printlnCond("               = cos(" + String.format(ASTStyle.decDegFormat,dDecl) + ")*cos(" + GalDecl +
				")*cos(" + String.format(ASTStyle.decDegFormat,dRA) + "-" + GalRA + ")+sin(" +
				String.format(ASTStyle.decDegFormat,dDecl) + ")*sin(" + GalDecl + ")");
		ChapGUI.printlnCond("               = " + String.format(ASTStyle.genFloatFormat,dGalLat));
		ChapGUI.printlnCond();

		ChapGUI.printCond("5.  Gal Lat = inv sin(T0) = inv sin(" + String.format(ASTStyle.genFloatFormat,dGalLat) + ") = ");
		dGalLat = ASTMath.INVSIN_D(dGalLat);
		ChapGUI.printlnCond(String.format(ASTStyle.decDegFormat,dGalLat) + " degrees");
		ChapGUI.printlnCond();

		dY = ASTMath.SIN_D(dDecl) - ASTMath.SIN_D(dGalLat) * ASTMath.SIN_D(GalDecl);
		ChapGUI.printlnCond("6.  Y = sin(Decl) - sin(Gal Lat) * sin(" + GalDecl + ") = sin(" +
				String.format(ASTStyle.decDegFormat,dDecl) + ") - sin(" + 
				String.format(ASTStyle.decDegFormat,dGalLat) + ")*sin(" + GalDecl + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dY));
		ChapGUI.printlnCond();

		dX = ASTMath.COS_D(dDecl) * ASTMath.SIN_D(dRA - GalRA) * ASTMath.COS_D(GalDecl);
		ChapGUI.printlnCond("7.  X = cos(Decl) * sin(RAdeg - " + GalRA + ") * cos(27.4)");
		ChapGUI.printlnCond("      = cos(" + String.format(ASTStyle.decDegFormat,dDecl) + ")*sin(" + 
				String.format(ASTStyle.decDegFormat,dRA) + " - " + GalRA + ")*cos(" + GalDecl + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dX));
		ChapGUI.printlnCond();

		dT = ASTMath.INVTAN_D(dY / dX);
		ChapGUI.printlnCond("8.  T1 = inv tan(Y / X) = inv tan(" + String.format(ASTStyle.genFloatFormat,dY) + " / " +
				String.format(ASTStyle.genFloatFormat,dX) + ") = " + String.format(ASTStyle.genFloatFormat,dT) + " degrees");
		ChapGUI.printlnCond();

		dQA = ASTMath.quadAdjust(dY, dX);
		dGalLon = dT + dQA + GalAscNode;
		ChapGUI.printlnCond("9.  Gal Lon = T1 + quadrant adjustment + " + GalAscNode);
		ChapGUI.printlnCond("    Since X = " + String.format(ASTStyle.genFloatFormat,dX) + " and Y = " + 
				String.format(ASTStyle.genFloatFormat,dY) + ",");
		ChapGUI.printlnCond("    the quadrant adjustment factor is " + String.format(ASTStyle.genFloatFormat,dQA));
		ChapGUI.printlnCond("    Thus, Gal Lon = " + String.format(ASTStyle.decDegFormat,dGalLon) + " degrees");
		ChapGUI.printlnCond();

		if (dGalLon > 360.0) dGalLon = dGalLon - 360.0;
		ChapGUI.printlnCond("10. If Gal Lon is greater than 360, subtract 360. Gal Lon = " + 
				String.format(ASTStyle.decDegFormat,dGalLon) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Finally, convert Gal Lat and Gal Lon to DMS format.");
		ChapGUI.printlnCond("    Gal Lat = " + ASTAngle.angleToStr(dGalLat, ASTMisc.DMSFORMAT) + 
				", Gal Lon = " + ASTAngle.angleToStr(dGalLon, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = result + " = " + ASTAngle.angleToStr(dGalLat, ASTMisc.DMSFORMAT) + 
				" Gal Lat, " + ASTAngle.angleToStr(dGalLon, ASTMisc.DMSFORMAT) +
				" Gal Lon (" + String.format(ASTStyle.epochFormat,GalEpoch) + ")";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Equatorial Coordinates to Horizon Coordinates
	 */
	protected void calcEquatorial_Horizon() {
		ASTPrt prt = ChapGUI.getPrtInstance();	
		String HAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String DeclInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String latInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		String result, strtmp;
		double dLat, dAlt, dAz, dDecl, dHA, dT, dT1, dT2;

		prt.clearTextArea();

		// Validate HA and Declination
		ASTTime HAObj = ASTTime.isValidTime(HAInput,ASTMisc.HIDE_ERRORS);
		if (HAObj.isValidTimeObj()) {
			result = "HA " + ASTTime.timeToStr(HAObj, ASTMisc.HMSFORMAT);
			dHA = HAObj.getDecTime();
		} else {
			ASTMsg.errMsg("Invalid Hour Angle was entered - try again", "Invalid HA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(DeclInput,ASTMisc.HIDE_ERRORS);
		if (declObj.isValidAngleObj()) {
			result = result + ", Declination " + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT);
			dDecl = declObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination");
			return;
		}

		// Validate latitude
		ASTLatLon dLatObj = ASTLatLon.isValidLat(latInput);
		if (!dLatObj.isLatValid()) return;
		dLat = dLatObj.getLat();
		strtmp = "for " + ASTLatLon.latToStr(dLatObj, ASTMisc.DECFORMAT) + " latitude";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to Horizon Coordinates", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond(strtmp, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Convert Hour Angle to decimal format. HA = " + 
				ASTTime.timeToStr(HAObj, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		dHA = dHA * 15.0;
		ChapGUI.printlnCond("2.  Multiply step 1 by 15 to convert to degrees. HAdeg = " + 
				ASTAngle.angleToStr(dHA, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3.  Convert Declination to decimal format. Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4.  Compute T0 = sin(Decl) * sin(Lat) + cos(Decl) * cos(Lat) * cos(HAdeg)");
		ChapGUI.printlnCond("               = sin(" + String.format(ASTStyle.genFloatFormat,dDecl) + 
				")*sin(" + String.format(ASTStyle.genFloatFormat,dLat) +
				") + cos(" + String.format(ASTStyle.genFloatFormat,dDecl) + 
				")*cos(" + String.format(ASTStyle.genFloatFormat,dLat) + ")*cos(" +
				String.format(ASTStyle.genFloatFormat,dHA) + ")");
		dT = ASTMath.SIN_D(dDecl) * ASTMath.SIN_D(dLat) + ASTMath.COS_D(dDecl) * ASTMath.COS_D(dLat) * ASTMath.COS_D(dHA);
		ChapGUI.printlnCond("               = " + String.format(ASTStyle.genFloatFormat,dT));
		ChapGUI.printlnCond();

		dAlt = ASTMath.INVSIN_D(dT);
		ChapGUI.printlnCond("5.  Alt = inv sin(T0) = inv sin(" + String.format(ASTStyle.genFloatFormat,dT) + ") = " +
				String.format(ASTStyle.genFloatFormat,dAlt) + " degrees");
		ChapGUI.printlnCond();

		dT1 = ASTMath.SIN_D(dDecl) - ASTMath.SIN_D(dLat) * ASTMath.SIN_D(dAlt);
		ChapGUI.printlnCond("6.  T1 = sin(Decl) - sin(Lat) * sin(Alt) = sin(" + 
				String.format(ASTStyle.genFloatFormat,dDecl) + ") - sin(" +
				String.format(ASTStyle.genFloatFormat,dLat) + ")*sin(" + 
				String.format(ASTStyle.genFloatFormat,dAlt) + ")");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dT1));
		ChapGUI.printlnCond();

		dT2 = ASTMath.COS_D(dLat) * ASTMath.COS_D(dAlt);
		ChapGUI.printlnCond("7.  T2 = T1 / [cos(Lat) * cos(Alt)] = " + 
				String.format(ASTStyle.genFloatFormat,dT1) + " / [cos(" +
				String.format(ASTStyle.genFloatFormat,dLat) + ") * cos(" + 
				String.format(ASTStyle.genFloatFormat,dAlt) + ")]");
		dT2 = dT1 / dT2;
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dT2));
		ChapGUI.printlnCond();

		dAz = ASTMath.INVCOS_D(dT2);
		ChapGUI.printlnCond("8.  Az = inv cos(T2) = inv cos(" + String.format(ASTStyle.genFloatFormat,dT2) + ") = " +
				String.format(ASTStyle.genFloatFormat,dAz) + " degrees");
		ChapGUI.printlnCond();

		dT1 = ASTMath.SIN_D(dHA);
		ChapGUI.printlnCond("9.  Compute sin(HAdeg) = sin(" + String.format(ASTStyle.genFloatFormat,dHA) + ") = " + 
				String.format(ASTStyle.genFloatFormat,dT1));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. If the result of step 9 is positive, then Az = 360 - Az.");
		if (dT1 > 0) dAz = 360.0 - dAz;
		ChapGUI.printlnCond("    Az = " + String.format(ASTStyle.genFloatFormat,dAz) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Finally, convert Altitude and Azimuth to DMS format. Thus,");
		ChapGUI.printlnCond("    Alt = " + ASTAngle.angleToStr(dAlt, ASTMisc.DMSFORMAT) + ", Az = " + 
				ASTAngle.angleToStr(dAz, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTTime.timeToStr(HAObj, ASTMisc.DMSFORMAT) + " HA, " + 
				ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl = " +
				ASTAngle.angleToStr(dAlt, ASTMisc.DMSFORMAT) + " Alt, " + 
				ASTAngle.angleToStr(dAz, ASTMisc.DMSFORMAT) + " Az";
		ChapGUI.printlnCond("Thus, " + result);
		ChapGUI.printlnCond(strtmp);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Galactic Coordinates to Equatorial Coordinates
	 */
	protected void calcGalactic_Equatorial() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String GalLatInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String GalLonInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String GalEpochInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		String result;
		double dLat, dLon, dDecl, dY, dX, dR, dQA, dRA;
		double GalEpoch, GalRA, GalDecl, GalAscNode;

		prt.clearTextArea();

		// Validate Galactic lat/lon
		ASTAngle latObj = ASTAngle.isValidAngle(GalLatInput);
		if (!latObj.isValidAngleObj()) return;
		dLat = latObj.getDecAngle();
		ASTAngle lonObj = ASTAngle.isValidAngle(GalLonInput);
		if (!lonObj.isValidAngleObj()) return;
		dLon = lonObj.getDecAngle();

		// Validate Epoch
		ASTReal rTmp = ASTReal.isValidReal(GalEpochInput,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch");
			return;
		}
		GalEpoch = rTmp.getRealValue();
		if (GalEpoch == GalEpoch1950) {				//use Epoch 1950.0 values
			GalRA = GalRA1950;
			GalDecl = GalDecl1950;
			GalAscNode = GalAscNode1950;
		} else if (GalEpoch == GalEpoch2000) {		// use Epoch J2000 values
			GalRA = GalRA2000;
			GalDecl = GalDecl2000;
			GalAscNode = GalAscNode2000;
		} else {
			ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch");
			return;
		}

		result = ASTAngle.angleToStr(dLat, ASTMisc.DMSFORMAT) + " Gal Lat, " + 
				ASTAngle.angleToStr(dLon, ASTMisc.DMSFORMAT) + " Gal Lon";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " (Epoch " + 
				String.format(ASTStyle.epochFormat,GalEpoch) + ")", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond("to Equatorial Coordinates " + "(Epoch " + 
				String.format(ASTStyle.epochFormat,GalEpoch) + ")", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Convert Galactic Lat to decimal format. Gal Lat = " + 
				ASTAngle.angleToStr(dLat, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2.  Convert Galactic Lon to decimal format. Gal Lon = " + 
				ASTAngle.angleToStr(dLon, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dDecl = ASTMath.COS_D(dLat) * ASTMath.COS_D(GalDecl) * ASTMath.SIN_D(dLon - GalAscNode) + 
				ASTMath.SIN_D(dLat) * ASTMath.SIN_D(GalDecl);
		ChapGUI.printlnCond("3.  Compute T = cos(Gal Lat) * cos(" + GalDecl + ") * sin(Gal Lon - " + 
				GalAscNode + ") + " + "sin(Gal Lat) * sin(" + GalDecl + ")");
		ChapGUI.printlnCond("              = cos(" + String.format(ASTStyle.decDegFormat,dLat) +
				")*cos(" + GalDecl + ")*sin(" +
				String.format(ASTStyle.decDegFormat,dLon) + " - " + GalAscNode + ") + sin(" + 
				String.format(ASTStyle.decDegFormat,dLat) + ") * sin(" + GalDecl + ")");
		ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat,dDecl));
		ChapGUI.printlnCond();

		ChapGUI.printCond("4.  Decl = inv sin(T) = inv sin(" + String.format(ASTStyle.genFloatFormat,dDecl) + ") = ");
		dDecl = ASTMath.INVSIN_D(dDecl);
		ChapGUI.printlnCond(String.format(ASTStyle.decDegFormat,dDecl) + " degrees");
		ChapGUI.printlnCond();

		dY = ASTMath.COS_D(dLat) * ASTMath.COS_D(dLon - GalAscNode);
		ChapGUI.printlnCond("5.  Y = cos(Gal Lat) * cos(Gal Lon - " + GalAscNode + ") = cos(" +
				String.format(ASTStyle.decDegFormat,dLat) + ")*cos(" + 
				String.format(ASTStyle.decDegFormat,dLon) + " - " + GalAscNode + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dY));
		ChapGUI.printlnCond();

		dX = ASTMath.SIN_D(dLat) * ASTMath.COS_D(GalDecl) - ASTMath.COS_D(dLat) * ASTMath.SIN_D(GalDecl) * ASTMath.SIN_D(dLon - GalAscNode);
		ChapGUI.printlnCond("6.  X = sin(Gal Lat) * cos(" + GalDecl + ") - cos(Gal Lat) * sin(" + GalDecl + ") * " +
				"sin(Gal Lon - " + GalAscNode + ")");
		ChapGUI.printlnCond("      = sin(" + String.format(ASTStyle.decDegFormat,dLat) + ")*cos(" + GalDecl + ") - cos(" +
				String.format(ASTStyle.decDegFormat,dLat) + ") * sin(" + GalDecl + ")*sin(" +
				String.format(ASTStyle.decDegFormat,dLon) + " - " + GalAscNode + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dX));
		ChapGUI.printlnCond();

		dR = ASTMath.INVTAN_D(dY / dX);
		ChapGUI.printlnCond("7.  R = inv tan (Y / X) = inv tan(" + String.format(ASTStyle.genFloatFormat,dY) + " / " +
				String.format(ASTStyle.genFloatFormat,dX) + ") = " + String.format(ASTStyle.genFloatFormat,dR) + " degrees");
		ChapGUI.printlnCond();

		dQA = ASTMath.quadAdjust(dY, dX);
		dRA = dR + dQA + GalRA;
		ChapGUI.printlnCond("8.  RAdeg = R + quadrant adjustment factor + " + GalRA);
		ChapGUI.printlnCond("    Since X = " + String.format(ASTStyle.genFloatFormat,dX) + " and Y = " + 
				String.format(ASTStyle.genFloatFormat,dY) + ",");
		ChapGUI.printlnCond("    the quadrant adjustment factor is " + String.format(ASTStyle.genFloatFormat,dQA));
		ChapGUI.printlnCond("    Thus, RAdeg = " + String.format(ASTStyle.decDegFormat,dRA) + " degrees");
		ChapGUI.printlnCond();

		if (dRA > 360) dRA = dRA - 360.0;
		ChapGUI.printlnCond("9.  If RAdeg is > 360, subtract 360. RAdeg = " + String.format(ASTStyle.decDegFormat,dRA) + " degrees");
		ChapGUI.printlnCond();

		dRA = dRA / 15.0;
		ChapGUI.printlnCond("10. Divide RAdeg by 15 to convert it to hours, giving RA = " + 
				String.format(ASTStyle.decHoursFormat,dRA) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Finally, convert Right Ascension to HMS format and Declination to DMS format.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(dRA, ASTMisc.HMSFORMAT) + ", Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = result + " = " + ASTTime.timeToStr(dRA, ASTMisc.HMSFORMAT) + " RA, " + 
				ASTAngle.angleToStr(dDecl,ASTMisc.DMSFORMAT) + " Decl (" +
				String.format(ASTStyle.epochFormat,GalEpoch) + ")";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Hour Angle to Right Ascension
	 */
	protected void calcHA_RA() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String HAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String UTInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String lonInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		String dateInput = ASTStr.removeWhitespace(ChapGUI.getData4());
		String result, strtmp;
		double LST, dLongitude, RA;

		prt.clearTextArea();

		// Validate Hour Angle and UT
		ASTTime HAtimeObj = ASTTime.isValidTime(HAInput,ASTMisc.HIDE_ERRORS);
		if (HAtimeObj.isValidTimeObj()) result = ASTTime.timeToStr(HAtimeObj, ASTMisc.HMSFORMAT);
		else {
			ASTMsg.errMsg("Invalid Hour Angle was entered - try again", "Invalid Hour Angle");
			return;
		}
		ASTTime UTtimeObj = ASTTime.isValidTime(UTInput, ASTMisc.HIDE_ERRORS);
		if (UTtimeObj.isValidTimeObj()) strtmp = "at " + ASTTime.timeToStr(UTtimeObj, ASTMisc.HMSFORMAT) + " UT";
		else {
			ASTMsg.errMsg("Invalid UT was entered - try again", "Invalid UT");
			return;
		}

		// Validate longitude and date
		ASTLatLon dLonObj = ASTLatLon.isValidLon(lonInput);
		if (!dLonObj.isLonValid()) return;
		dLongitude = dLonObj.getLon();
		strtmp = strtmp + " for " + ASTLatLon.lonToStr(dLonObj, ASTMisc.DECFORMAT) + " longitude";

		ASTDate dateObj = ASTDate.isValidDate(dateInput);
		if (!dateObj.isValidDateObj())return;
		strtmp = strtmp + " on " + ASTDate.dateToStr(dateObj);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert Hour Angle " + result + " to Right Ascension", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond(strtmp, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		LST = ASTTime.GSTtoLST(ASTTime.UTtoGST(UTtimeObj.getDecTime(), dateObj), dLongitude);
		ChapGUI.printlnCond("1. Convert UT to LST. Thus, for date " + ASTDate.dateToStr(dateObj) + " at longitude " + 
				ASTLatLon.lonToStr(dLonObj, ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("   " + ASTTime.timeToStr(UTtimeObj, ASTMisc.HMSFORMAT) + " UT = " + ASTTime.timeToStr(LST, ASTMisc.HMSFORMAT) +
				" = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " LST");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2. Convert Hour Angle to decimal format. Thus, HA = " +
				ASTTime.timeToStr(HAtimeObj, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		RA = LST - HAtimeObj.getDecTime();
		ChapGUI.printlnCond("3. RA = LST - Hour Angle = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " - " + 
				ASTTime.timeToStr(HAtimeObj, ASTMisc.DECFORMAT) + " = " + ASTTime.timeToStr(RA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		if (RA < 0.0) RA = RA + 24.0;
		ChapGUI.printlnCond("4. If RA is negative add 24. Thus, RA = " + ASTTime.timeToStr(RA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5. Finally, convert RA to hours, minutes, seconds format, giving RA = " + ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTTime.timeToStr(HAtimeObj, ASTMisc.HMSFORMAT) + " HA = " + ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT) + " RA";
		ChapGUI.printlnCond("Thus, " + result + " at " + ASTTime.timeToStr(UTtimeObj, ASTMisc.HMSFORMAT) + " UT");
		ChapGUI.printlnCond(strtmp);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Horizon Coordinates to Equatorial Coordinates
	 */
	protected void calcHorizon_Equatorial() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String altInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String azInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String latInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		String result, strtmp;
		double dLat, dAlt, dAz, dDecl, dHA, dTmp;

		prt.clearTextArea();

		// Validate Altitude and Azimuth
		ASTAngle altObj = ASTAngle.isValidAngle(altInput,ASTMisc.HIDE_ERRORS);
		if (altObj.isValidAngleObj()) {
			result = "Altitude " + ASTAngle.angleToStr(altObj, ASTMisc.DMSFORMAT);
			dAlt = altObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Altitude was entered - try again", "Invalid Altitude");
			return;
		}
		ASTAngle azObj = ASTAngle.isValidAngle(azInput,ASTMisc.HIDE_ERRORS);
		if (azObj.isValidAngleObj()) {
			result = result + ", Azimuth " + ASTAngle.angleToStr(azObj, ASTMisc.DMSFORMAT);
			dAz = azObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Azimuth was entered - try again", "Invalid Azimuth");
			return;
		}

		// Validate latitude
		ASTLatLon dLatObj = ASTLatLon.isValidLat(latInput);
		if (!dLatObj.isLatValid()) return;
		dLat = dLatObj.getLat();
		strtmp = "for " + ASTLatLon.latToStr(dLatObj, ASTMisc.DECFORMAT) + " latitude";

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to Equatorial Coordinates", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond(strtmp, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Convert Altitude to decimal format. Alt = " +
				ASTAngle.angleToStr(altObj, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2.  Convert Azimuth to decimal format. Az = " +
				ASTAngle.angleToStr(azObj, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3.  Compute T0 = sin(Alt) * sin(Lat) + cos(Alt) * cos(Lat) * cos(Az)");
		ChapGUI.printlnCond("               = sin(" + String.format(ASTStyle.genFloatFormat,dAlt) + ") * sin(" + 
				String.format(ASTStyle.genFloatFormat,dLat) +
				") + cos(" + String.format(ASTStyle.genFloatFormat,dAlt) + ") * cos(" + String.format(ASTStyle.genFloatFormat,dLat) +
				") * cos(" + String.format(ASTStyle.genFloatFormat,dAz) + ")");
		dTmp = ASTMath.SIN_D(dAlt) * ASTMath.SIN_D(dLat) + ASTMath.COS_D(dAlt) * ASTMath.COS_D(dLat) * ASTMath.COS_D(dAz);
		ChapGUI.printlnCond("               = " + String.format(ASTStyle.genFloatFormat,dTmp));
		ChapGUI.printlnCond();

		dDecl = ASTMath.INVSIN_D(dTmp);
		ChapGUI.printlnCond("4.  Decl = inverse sin(T0) = inv sin(" + String.format(ASTStyle.genFloatFormat,dTmp) + ") = " +
				String.format(ASTStyle.genFloatFormat,dDecl) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5.  T1 = sin(Alt) - sin(Lat) * sin(Decl)");
		ChapGUI.printlnCond("       = sin(" + String.format(ASTStyle.genFloatFormat,dAlt) + ") - sin(" +
				String.format(ASTStyle.genFloatFormat,dLat) + ") * sin(" + String.format(ASTStyle.genFloatFormat,dDecl) + ")");
		dTmp = ASTMath.SIN_D(dAlt) - ASTMath.SIN_D(dLat) * ASTMath.SIN_D(dDecl);
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dTmp));
		ChapGUI.printlnCond();

		dHA = ASTMath.COS_D(dLat) * ASTMath.COS_D(dDecl);
		ChapGUI.printlnCond("6.  cos(H) = T1 / [cos(Lat) * cos(Decl)]");
		ChapGUI.printlnCond("           = " + String.format(ASTStyle.genFloatFormat,dTmp) + " / [cos(" +
				String.format(ASTStyle.genFloatFormat,dLat) + ") * cos(" + String.format(ASTStyle.genFloatFormat,dDecl) + ")]");
		dHA = dTmp / dHA;
		ChapGUI.printlnCond("           = " + String.format(ASTStyle.genFloatFormat,dHA));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("7.  H = inv cos(H) = inv cos( " + String.format(ASTStyle.genFloatFormat,dHA) + ") = " +
				String.format(ASTStyle.genFloatFormat,ASTMath.INVCOS_D(dHA)) + " degrees");
		dHA = ASTMath.INVCOS_D(dHA);
		ChapGUI.printlnCond();

		dTmp = ASTMath.SIN_D(dAz);
		ChapGUI.printlnCond("8.  Compute sin(Az) = sin(" + String.format(ASTStyle.genFloatFormat,dAz) + ") = " + 
				String.format(ASTStyle.genFloatFormat,dTmp));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  If sin(Az) is positive, then subtract the result of step 7 from 360.");
		if (dTmp > 0) dHA = 360 - dHA;
		ChapGUI.printlnCond("    H = " + String.format(ASTStyle.genFloatFormat,dHA) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printCond("10. Hour Angle = HA = H / 15 = " + String.format(ASTStyle.genFloatFormat,dHA) + " / 15 = ");
		dHA = dHA / 15.0;
		ChapGUI.printlnCond(String.format(ASTStyle.genFloatFormat,dHA) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Finally, convert Declination to DMS format and HA to HMS format.");
		ChapGUI.printlnCond("    HA = " + ASTTime.timeToStr(dHA, ASTMisc.HMSFORMAT) + ", Decl = " + ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTAngle.angleToStr(altObj, ASTMisc.DMSFORMAT) + " Alt, " + 
				ASTAngle.angleToStr(azObj, ASTMisc.DMSFORMAT) + " Az = " +
				ASTTime.timeToStr(dHA, ASTMisc.HMSFORMAT) + " HA, " + ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT) + " Decl";
		ChapGUI.printlnCond("Thus, " + result);
		ChapGUI.printlnCond(strtmp);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Convert Right Ascension to Hour Angle
	 */
	protected void calcRA_HA() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String RAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String UTInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String lonInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		String dateInput = ASTStr.removeWhitespace(ChapGUI.getData4());
		String result, strtmp;
		double LST, dLongitude, HA;

		prt.clearTextArea();

		// Validate Right Ascension and UT
		ASTTime RAtimeObj = ASTTime.isValidTime(RAInput,ASTMisc.HIDE_ERRORS);
		if (RAtimeObj.isValidTimeObj()) result = ASTTime.timeToStr(RAtimeObj, ASTMisc.HMSFORMAT);
		else {
			ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid Right Ascension");
			return;
		}

		ASTTime UTtimeObj = ASTTime.isValidTime(UTInput,ASTMisc.HIDE_ERRORS);
		if (UTtimeObj.isValidTimeObj()) strtmp = "at " + ASTTime.timeToStr(UTtimeObj, ASTMisc.HMSFORMAT) + " UT";
		else {
			ASTMsg.errMsg("Invalid UT was entered - try again", "Invalid UT");
			return;
		}

		// Validate longitude and date
		ASTLatLon dLonObj = ASTLatLon.isValidLon(lonInput);
		if (!dLonObj.isLonValid()) return;
		dLongitude = dLonObj.getLon();
		strtmp = strtmp + " for " + ASTLatLon.lonToStr(dLonObj, ASTMisc.DECFORMAT) + " longitude";

		ASTDate dateObj = ASTDate.isValidDate(dateInput);
		if (!dateObj.isValidDateObj()) return;
		strtmp = strtmp + " on " + ASTDate.dateToStr(dateObj);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert Right Ascension " + result + " to Hour Angle (HA)", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond(strtmp, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		LST = ASTTime.GSTtoLST(ASTTime.UTtoGST(UTtimeObj.getDecTime(), dateObj), dLongitude);
		ChapGUI.printlnCond("1. Convert UT to LST. Thus, for date " + ASTDate.dateToStr(dateObj) + " at longitude " + 
				ASTLatLon.lonToStr(dLonObj, ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("   " + ASTTime.timeToStr(UTtimeObj, ASTMisc.HMSFORMAT) + " UT = " + ASTTime.timeToStr(LST, ASTMisc.HMSFORMAT) +
				" = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " LST");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("2. Convert Right Ascension to decimal format. Thus, RA = " +
				ASTTime.timeToStr(RAtimeObj, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		HA = LST - RAtimeObj.getDecTime();
		ChapGUI.printlnCond("3. HA = LST - RA = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " - " + ASTTime.timeToStr(RAtimeObj, ASTMisc.DECFORMAT) +
				" = " + ASTTime.timeToStr(HA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		if (HA < 0.0) HA = HA + 24.0;
		ChapGUI.printlnCond("4. If HA is negative add 24. Thus, HA = " + ASTTime.timeToStr(HA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5. Finally, convert HA to hours, minutes, seconds format, giving HA = " + ASTTime.timeToStr(HA, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTTime.timeToStr(RAtimeObj, ASTMisc.HMSFORMAT) + " RA = " + ASTTime.timeToStr(HA, ASTMisc.HMSFORMAT) + " HA";
		ChapGUI.printlnCond("Thus, " + result);
		ChapGUI.printlnCond(strtmp);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Compute a precession correction
	 */
	protected void calcPrecessionCorr() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String RAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String DeclInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String strEpochFrom = ASTStr.removeWhitespace(ChapGUI.getData3());
		String strEpochTo = ASTStr.removeWhitespace(ChapGUI.getData4());
		String result;
		double dRA, dRAdeg, dDecl, deltaRA, deltaDecl, dT, dM, dNd, dNt;
		double EpochTo, EpochFrom, dDiff;

		prt.clearTextArea();

		// Validate RA and Declination
		ASTTime RAObj = ASTTime.isValidTime(RAInput,ASTMisc.HIDE_ERRORS);
		if (RAObj.isValidTimeObj()) {
			result = ASTTime.timeToStr(RAObj, ASTMisc.HMSFORMAT) + " RA, ";
			dRA = RAObj.getDecTime();
		} else {
			ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(DeclInput,ASTMisc.HIDE_ERRORS);
		if (declObj.isValidAngleObj()) {
			result = result + ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl";
			dDecl = declObj.getDecAngle();
		} else {
			ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination");
			return;
		}

		// Validate the to and from Epochs
		ASTReal rTmp = ASTReal.isValidReal(strEpochFrom,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()){
			EpochFrom = rTmp.getRealValue();
			result = result + " in Epoch " + EpochFrom;
		} else {
			ASTMsg.errMsg("Invalid 'From' Epoch was entered - try again", "Invalid 'From' Epoch");
			return;
		}
		rTmp = ASTReal.isValidReal(strEpochTo,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid 'To' Epoch was entered - try again", "Invalid 'To' Epoch");
			return;
		}
		EpochTo = rTmp.getRealValue();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + result + " to Epoch " + EpochTo, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		ChapGUI.printlnCond("1.  Convert RA to decimal format. RA = " + ASTTime.timeToStr(dRA, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		dRAdeg = dRA * 15.0;
		ChapGUI.printlnCond("2.  Multiply RA by 15 to convert to degrees. RAdeg = " + 
				ASTAngle.angleToStr(dRAdeg, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3.  Convert Decl to decimal format. Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dT = (EpochTo - 1900.0) / 100.0;
		ChapGUI.printlnCond("4.  T = (Epoch to convert to - 1900) / 100 = (" + EpochTo + 
				" - 1900) / 100 = " + String.format(ASTStyle.genFloatFormat,dT));
		ChapGUI.printlnCond();

		dM = 3.07234 + 0.00186 * dT;
		ChapGUI.printlnCond("5.  M = 3.07234 + 0.00186 * T = 3.07234 + 0.00186 * " + 
				String.format(ASTStyle.genFloatFormat,dT) + " = " +
				String.format(ASTStyle.genFloatFormat,dM) + " arcseconds");
		ChapGUI.printlnCond();

		dNd = 20.0468 - 0.0085 * dT;
		ChapGUI.printlnCond("6.  Nd = 20.0468 - 0.0085 * T = 20.0468 - 0.0085 * " + 
				String.format(ASTStyle.genFloatFormat,dT) + " = " +
				String.format(ASTStyle.genFloatFormat,dNd) + " arcseconds");
		ChapGUI.printlnCond();

		dNt = dNd / 15.0;
		ChapGUI.printlnCond("7.  Nt = Nd / 15 = " + String.format(ASTStyle.genFloatFormat,dNd) + 
				" / 15 = " + String.format(ASTStyle.genFloatFormat,dNt) + " seconds (of time)");
		ChapGUI.printlnCond();

		dDiff = (EpochTo - EpochFrom);
		ChapGUI.printlnCond("8.  Diff = (Epoch to convert to - Epoch to convert from) = (" + EpochTo + " - " +
				EpochFrom + ") = " + dDiff);
		ChapGUI.printlnCond();

		deltaRA = (dM + dNt * ASTMath.SIN_D(dRAdeg) * ASTMath.TAN_D(dDecl)) * dDiff;
		ChapGUI.printlnCond("9.  D.ra = [M + Nt * sin(RAdeg) * tan(Decl)] * Diff");
		ChapGUI.printlnCond("         = [" + String.format(ASTStyle.genFloatFormat,dM) + " + " + 
				String.format(ASTStyle.genFloatFormat,dNt) +
				" * sin(" + String.format(ASTStyle.decDegFormat,dRAdeg) + 
				") * tan(" + String.format(ASTStyle.decDegFormat,dDecl) + "]) * " + dDiff);
		ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat,deltaRA) + " seconds (of time)");
		ChapGUI.printlnCond();

		deltaDecl = dNd * ASTMath.COS_D(dRAdeg) * dDiff;
		ChapGUI.printlnCond("10. D.decl = Nd * cos(RAdeg) * Diff = " + String.format(ASTStyle.genFloatFormat,dNd) + " * cos(" +
				String.format(ASTStyle.decDegFormat,dRAdeg) + ") * " + dDiff);
		ChapGUI.printlnCond("           = " + String.format(ASTStyle.genFloatFormat,deltaDecl) + " arcseconds");
		ChapGUI.printlnCond();

		deltaRA = deltaRA / 3600.0;
		deltaDecl = deltaDecl / 3600.0;
		ChapGUI.printlnCond("11. Divide D.ra by 3600 to convert to hours and divide D.decl by 3600 to convert to degrees.");
		ChapGUI.printlnCond("    D.ra = " + String.format(ASTStyle.genFloatFormat,deltaRA) + " hours and D.decl = " +
				String.format(ASTStyle.genFloatFormat,deltaDecl) + " degrees");
		ChapGUI.printlnCond();

		dRA = dRA + deltaRA;
		dDecl = dDecl + deltaDecl;
		ChapGUI.printlnCond("12. Add D.ra to RA and D.decl to Decl, giving an");
		ChapGUI.printlnCond("    adjusted RA = " + String.format(ASTStyle.decHoursFormat,dRA) + " hours and Decl = " +
				String.format(ASTStyle.decDegFormat,dDecl) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("13. Finally, convert RA to HMS format and Decl to DMS format. Thus, in Epoch " + EpochTo + ",");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(dRA, ASTMisc.HMSFORMAT) + " hours, Decl = " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT) + " degrees");
		ChapGUI.printlnCond();

		result = ASTTime.timeToStr(RAObj, ASTMisc.HMSFORMAT) + " RA, " + 
				ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl (" + EpochFrom + ") = " +
				ASTTime.timeToStr(dRA, ASTMisc.HMSFORMAT) + " RA, " + 
				ASTAngle.angleToStr(dDecl, ASTMisc.DMSFORMAT) + " Decl (" + EpochTo + ")";
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/*=================================================================
	 * Routines to do the actual work for the Kepler's Equation menu
	 *================================================================*/

	/**
	 * Solve Kepler's equation by the Newton/Raphson method
	 */
	protected void calcKeplerNewton() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String MAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String TermInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String EccInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		double dTerm, dMA, dMARad, dEA, dEANext, dEcc, delta;
		int iterCnt = 0;
		String result;

		prt.clearTextArea();

		// Validate the mean anomaly
		ASTAngle tmpAngle = ASTAngle.isValidAngle(MAInput,ASTMisc.HIDE_ERRORS);
		if (tmpAngle.isValidAngleObj()) dMA = tmpAngle.getDecAngle();
		else {
			ASTMsg.errMsg("Invalid Mean Anomaly was entered - try again", "Invalid Mean Anomaly");
			return;
		}

		// Validate the termination criteria
		ASTReal rTmp = ASTReal.isValidReal(TermInput,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria");
			return;
		}
		dTerm = rTmp.getRealValue();
		if (dTerm < ASTKepler.KeplerMinCriteria) dTerm = ASTKepler.KeplerMinCriteria;


		// Validate the orbital eccentricity
		rTmp = ASTReal.isValidReal(EccInput,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity");
			return;
		}
		dEcc = rTmp.getRealValue();
		if (dEcc < 0) {
			ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity");
			return;
		}

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Solve Kepler's Equation for Mean Anomaly " + ASTAngle.angleToStr(dMA, ASTMisc.DMSFORMAT) +
				" and Orbital Eccentricity = " + dEcc, ASTPrt.CENTERTXT);
		ChapGUI.printlnCond("Iterate until difference is less than " + dTerm + " radians (" +
				String.format(ASTStyle.genFloatFormat,ASTMath.rad2deg(dTerm) * 3600.0) + " arcseconds)", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		dMARad = ASTMath.deg2rad(dMA);
		ChapGUI.printlnCond("1. Convert mean anomaly to radians.");
		ChapGUI.printlnCond("   MA_rad = " + String.format(ASTStyle.genFloatFormat,dMARad) + " radians");
		ChapGUI.printlnCond();

		if (dEcc < 0.75) dEA = dMARad;
		else dEA = Math.PI;

		ChapGUI.printlnCond("2. Set an initial estimate for E0. If the orbital eccentricity is < 0.75,");
		ChapGUI.printlnCond("   E0 = MA_rad, otherwise E0 = pi radians. Thus, ");
		ChapGUI.printlnCond("   E0 = " + String.format(ASTStyle.genFloatFormat,dEA) + " radians");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3. Using the iteration scheme");
		ChapGUI.printlnCond("   E_i = E_(i-1) - [E_(i-1) - e*sin(E_(i-1)-MA_rad]/[1-e*cos(E_(i-1))],");
		ChapGUI.printlnCond("   iterate until the difference between successive estimates is");
		ChapGUI.printlnCond("   less than the termination criteria (" + dTerm + " radians)");
		ChapGUI.printlnCond();

		for (iterCnt = 1; iterCnt <= ASTKepler.KeplerMaxIterations; iterCnt++) {
			// Note that the sine + cosine functions here uses radians, not degrees!
			dEANext = dEA - (dEA - dEcc * Math.sin(dEA) - dMARad) / (1 - dEcc * Math.cos(dEA));
			delta = Math.abs(dEANext - dEA);
			ChapGUI.printlnCond("   E" + iterCnt + " = " + String.format(ASTStyle.genFloatFormat,dEANext) + ", Delta = " +
					String.format(ASTStyle.genFloatFormat,delta));
			dEA = dEANext;
			if (delta <= dTerm) break;
		}
		if (iterCnt >= ASTKepler.KeplerMaxIterations) {
			ChapGUI.printlnCond("WARNING: Did not converge, so stopped after " + ASTKepler.KeplerMaxIterations + " iterations.");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4. The eccentric anomaly (in radians) is the last result. So,");
		ChapGUI.printlnCond("   Eccentric Anomaly = E" + iterCnt + " = " + 
				String.format(ASTStyle.genFloatFormat,dEA) + " radians");
		ChapGUI.printlnCond();

		dEA = ASTMath.rad2deg(dEA);
		ChapGUI.printlnCond("5. Convert the eccentric anomaly from radians to degrees.");
		ChapGUI.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr(dEA, ASTMisc.DECFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6. Convert the Eccentric Anomaly to DMS format.");
		ChapGUI.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr(dEA, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = "After " + iterCnt + " iterations (Newton/Raphson method), Eccentric Anomaly = " + 
				ASTAngle.angleToStr(dEA, ASTMisc.DMSFORMAT);
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Solve Kepler's equation via a simple iteration method
	 */
	protected void calcKeplerSimple() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String MAInput = ASTStr.removeWhitespace(ChapGUI.getData1());
		String TermInput = ASTStr.removeWhitespace(ChapGUI.getData2());
		String EccInput = ASTStr.removeWhitespace(ChapGUI.getData3());
		double dTerm, dMA, dMARad, dEA, dEANext, dEcc, delta;
		int iterCnt = 0;
		String result;

		prt.clearTextArea();

		// Validate the mean anomaly
		ASTAngle tmpAngle = ASTAngle.isValidAngle(MAInput,ASTMisc.HIDE_ERRORS);
		if (tmpAngle.isValidAngleObj()) dMA = tmpAngle.getDecAngle();
		else {
			ASTMsg.errMsg("Invalid Mean Anomaly was entered - try again", "Invalid Mean Anomaly");
			return;
		}

		// Validate the termination criteria
		ASTReal rTmp = ASTReal.isValidReal(TermInput,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria");
			return;
		}
		dTerm = rTmp.getRealValue();
		if (dTerm < ASTKepler.KeplerMinCriteria) dTerm = ASTKepler.KeplerMinCriteria;

		// Validate the orbital eccentricity
		rTmp = ASTReal.isValidReal(EccInput,ASTMisc.HIDE_ERRORS);
		if (!rTmp.isValidRealObj()) {
			ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity");
			return;
		}
		dEcc = rTmp.getRealValue();
		if (dEcc < 0) {
			ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity");
			return;
		}

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Solve Kepler's Equation for Mean Anomaly " + ASTAngle.angleToStr(dMA, ASTMisc.DMSFORMAT) +
				" and Orbital Eccentricity = " + dEcc, ASTPrt.CENTERTXT);
		ChapGUI.printlnCond("Iterate until difference is less than " + dTerm + " radians (" +
				String.format(ASTStyle.genFloatFormat,ASTMath.rad2deg(dTerm)*3600.0) + " arcseconds)", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		dMARad = ASTMath.deg2rad(dMA);
		ChapGUI.printlnCond("1. Convert mean anomaly to radians.");
		ChapGUI.printlnCond("   MA_rad = " + String.format(ASTStyle.genFloatFormat,dMARad) + " radians");
		ChapGUI.printlnCond();

		dEA = dMARad;
		ChapGUI.printlnCond("2. Set initial estimate E0 = MA_rad = " + 
				String.format(ASTStyle.genFloatFormat,dEA) + " radians");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("3. Using the iteration scheme E_i = MA_rad + e*sin(E_(i-1)),");
		ChapGUI.printlnCond("   iterate until the difference between successive estimates is");
		ChapGUI.printlnCond("   less than the termination criteria (" + dTerm + " radians)");
		ChapGUI.printlnCond();

		for (iterCnt = 1; iterCnt <= ASTKepler.KeplerMaxIterations; iterCnt++) {
			// Note that the sine function here uses radians, not degrees!
			dEANext = dMARad + dEcc * Math.sin(dEA);
			delta = Math.abs(dEANext - dEA);
			ChapGUI.printlnCond("   E" + iterCnt + " = " + String.format(ASTStyle.genFloatFormat,dEANext) + ", Delta = " +
					String.format(ASTStyle.genFloatFormat,delta));
			dEA = dEANext;
			if (delta <= dTerm) break;
		}
		if (iterCnt >= ASTKepler.KeplerMaxIterations) {
			ChapGUI.printlnCond("WARNING: Did not converge, so stopped after " + ASTKepler.KeplerMaxIterations + " iterations.");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("4. The eccentric anomaly (in radians) is the last result. So,");
		ChapGUI.printlnCond("   Eccentric Anomaly = E" + iterCnt + " = " + 
				String.format(ASTStyle.genFloatFormat,dEA) + " radians");
		ChapGUI.printlnCond();

		dEA = ASTMath.rad2deg(dEA);
		ChapGUI.printlnCond("5. Convert the eccentric anomaly from radians to degrees.");
		ChapGUI.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr(dEA, ASTMisc.DECFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6. Convert the Eccentric Anomaly to DMS format.");
		ChapGUI.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr(dEA, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = "After " + iterCnt + " iterations (Simple Iteration), Eccentric Anomaly = " + ASTAngle.angleToStr(dEA, ASTMisc.DMSFORMAT);
		ChapGUI.printlnCond("Thus, " + result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/*-----------------------------------------------------------------
	 * Helper routine used only in this class
	 *----------------------------------------------------------------*/

	/**
	 * Compute the obliquity of the ecliptic for a given epoch
	 * 
	 * @param Epoch			epoch for which obliquity is desired
	 * @return				obliquity of the ecliptic calculated for the
	 * 						stated epoch.
	 */
	private double computeEclipticObliquity(double Epoch) {
		double JD, dT, dDE, dE;
		int iEpoch = ASTMath.Trunc(Epoch);

		ChapGUI.printlnCond("Steps 1-4 compute the Obliquity of the Ecliptic for Epoch " + 
				String.format("%.2f",Epoch));
		ChapGUI.printlnCond();

		JD = ASTDate.dateToJD(1, 0.0, iEpoch);
		ChapGUI.printlnCond("1.  Calculate the Julian Day Number for January 0.0 of the given Epoch");
		ChapGUI.printlnCond("    JD = " + ASTDate.dateToStr(1, 0.0, iEpoch) + " = " +
				String.format(ASTStyle.JDFormat,JD));
		ChapGUI.printlnCond();

		dT = (JD - 2451545.0) / 36525.0;
		ChapGUI.printlnCond("2.  T = (JD - 2451545.0) / 36525 = (" + 
				String.format(ASTStyle.JDFormat, JD) + " - 2451545.0) / 36525");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat, dT) + " centuries");
		ChapGUI.printlnCond();        

		dDE = 46.815 * dT + 0.0006 * dT * dT - 0.00181 * dT * dT * dT;
		ChapGUI.printlnCond("3.  DE = 46.815 * T + 0.0006 * T * T - 0.00181 * T * T * T");
		ChapGUI.printlnCond("       = 46.815*" + String.format(ASTStyle.genFloatFormat,dT) +
				" + 0.0006*" + String.format(ASTStyle.genFloatFormat,dT * dT) +
				" - 0.00181*" + String.format(ASTStyle.genFloatFormat,dT * dT * dT));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.decDegFormat,dDE) + " arcsecs");
		ChapGUI.printlnCond();

		dE = 23.439292 - (dDE / 3600.0);
		ChapGUI.printlnCond("4.  E = 23.439292 - (DE / 3600) = 23.439292 - (" + 
				String.format(ASTStyle.decDegFormat,dDE) + " / 3600)");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.decDegFormat,dE) + " degrees");
		ChapGUI.printlnCond();

		return dE;
	}

}
