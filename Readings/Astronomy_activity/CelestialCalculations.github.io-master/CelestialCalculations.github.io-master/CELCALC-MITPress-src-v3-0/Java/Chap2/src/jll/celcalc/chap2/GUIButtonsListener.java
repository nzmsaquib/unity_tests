package jll.celcalc.chap2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;

/**
 * <b>Implements an action listener for GUI buttons.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class GUIButtonsListener implements ActionListener {

	private static ConvertBtnActions ba = new ConvertBtnActions();
	
	/**
	 * Handles clicks on individual GUI buttons.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */	
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		if (action.equals(ChapGUI.getSwapCommand())) {
			prt.clearTextArea();
			ChapGUI.swapUnits();
		} else if (action.equals(ChapGUI.getConvertCommand())) {
			prt.clearTextArea();

			switch (ChapGUI.getConvType()) {
			case CROSS_MULTIPLY:
				ChapGUI.setResults(ba.crossMultiply());
				break;
			case HorDMS_HRSorDEGS:
				ChapGUI.setResults(ba.convertHorDMS_decHrsorDegs());
				break;
			case CENTIGRADE_FAHRENHEIT:
				ChapGUI.setResults(ba.convertC_F());
				break;
			default:
				ASTMsg.errMsg("Invalid Conversion Type"," ");
				break;
			}
		}

	}
}
