package jll.celcalc.chap2;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
 * This class extends JMenuItem to add some additional fields, which
 * simplifies the code for adding items to the menu bar and determining
 * what actions to take as various menu items are selected. 
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {

	/** Define values for the eConvType field in a ChapMenuItem class. These indicate how to do unit conversions. */
	protected enum ConversionType {
		/** Cross-multiply to find an unknown quantity. */ CROSS_MULTIPLY,
		/** Convert HMS to/from hours or convert DMS to/from degrees. */ HorDMS_HRSorDEGS,
		/** Convert between Centigrade and Fahrenheit. */  CENTIGRADE_FAHRENHEIT
	}

	/**
	 * This class constructor creates ChapMenuItem objects (e.g., menu items) for
	 * the Conversions menu, and initializes the appropriate GUI variables to be the first menu item.
	 * 
	 * @param menu				menu to which individual menu items are to be added
	 */
	protected ChapMenuItems(JMenu menu) {
		ChapMnuItem tmp;

		// The first menu item created is a special case because we want to initialize the GUI
		// to have the first menu item already selected.
		tmp = createMenuItem(menu,"mm",25.4,"%.6f","inches",1.0,"%.6f",ConversionType.CROSS_MULTIPLY);
		ChapGUI.saveFromToValues(tmp.strFromUnits,tmp.dFromDenom,tmp.strFromFormat,
				tmp.strToUnits,tmp.dToDenom,tmp.strToFormat,tmp.eConvType);	
			
		createMenuItem(menu,"m",0.3048,"%.6f","feet",1.0,"%.6f",ConversionType.CROSS_MULTIPLY);
		menu.addSeparator();

		createMenuItem(menu,"km",1.609344,"%.6f","miles",1.0,"%.6f",ConversionType.CROSS_MULTIPLY);
		createMenuItem(menu,"light years",1.0,"%.4E","miles",5.87E12,"%.6E",ConversionType.CROSS_MULTIPLY);
		createMenuItem(menu,"light years",1.0,"%.4f","parsecs",0.3068,"%.4f",ConversionType.CROSS_MULTIPLY);
		createMenuItem(menu,"AU",1.0,"%.6E","miles",9.29E7,"%.4E",ConversionType.CROSS_MULTIPLY);
		menu.addSeparator();

		createMenuItem(menu,"degs",180.0,ASTStyle.decDegFormat,"radians",Math.PI,ASTStyle.decDegFormat,ConversionType.CROSS_MULTIPLY);
		createMenuItem(menu,"decimal hrs",24.0,ASTStyle.decHoursFormat,
				"decimal degs",360.0,ASTStyle.decDegFormat,ConversionType.CROSS_MULTIPLY);
		createMenuItem(menu,"HMS",-1.0,"@","decimal hrs",1.0,ASTStyle.decHoursFormat,ConversionType.HorDMS_HRSorDEGS);
		createMenuItem(menu,"DMS",-1.0,"@","decimal degs",1.0,ASTStyle.decDegFormat,ConversionType.HorDMS_HRSorDEGS);		
		menu.addSeparator();

		createMenuItem(menu,"degs C",-1.0,ASTStyle.temperatureFormat,"degs F",1.0,
				ASTStyle.temperatureFormat,ConversionType.CENTIGRADE_FAHRENHEIT);
	}

	/** Define a class for what a menu item looks like. */
	protected class ChapMnuItem extends JMenuItem {
		// Note that the to/from denominators below are actually overloaded in their usage.
		// For menu items that are to be cross-multiplied, they are the denominators
		// in the equation to form to do the cross multiplication. For other usages,
		// such as C to F conversion, dToDenom is -1.0 while dFromDenom is 1.0. These values
		// may be swapped when the switch units button is clicked so that they continue
		// to track with the other "to/from" values (e.g., strToUnits/strFromUnits). When
		// it is time to do a conversion, if the Chap2GUI class's private dFromDenom is
		// negative, the conversion to perform is in the direction indicated by the
		// corresponding menu entry. For example, for the "degs C - degs F" menu entry,
		// if fFronDenom is negative then convert degs C to degs F. If dFromDenom is
		// positive, convert degs F to degs C.
		// Info about data to convert from
		private String strFromUnits;		// units
		private double dFromDenom;			// denominator
		private String strFromFormat;		// how to format data
		// Info about units to convert to
		private String strToUnits;			// units
		private double dToDenom;			// denominator
		private String strToFormat;			// how to format data
		private ConversionType eConvType;   // what type of conversion to perform

		/**
		 * This class constructor creates a ChapMenuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param FromUnits		text to display for the 'from' units (e.g., km)
		 * @param FromDenom		numeric value for the 'from' denominator (e.g, 5.0)
		 * @param FromFormat	how to format the 'from' denominator
		 * @param ToUnits		text to display for the 'to' units (e.g., miles)
		 * @param ToDenom		numeric value for the 'to' denominator (e.g, 5.0)
		 * @param ToFormat		how to format the 'to' denominator
		 * @param ConvType		what type of conversion is to be done
		 */          
		protected ChapMnuItem(String title,String FromUnits,double FromDenom,String FromFormat, 
				String ToUnits,double ToDenom,String ToFormat,ConversionType ConvType) {

			super(title);
			strFromUnits = FromUnits;
			dFromDenom = FromDenom;
			strFromFormat = FromFormat;

			strToUnits = ToUnits;
			dToDenom = ToDenom;
			strToFormat = ToFormat;

			eConvType = ConvType;
		}
		
		/*=====================================================
		 * Define accessor methods for the menu item fields
		 *====================================================*/

		/**
		 * Gets the "from" units for a conversion
		 * 
		 * @return		the units to convert from (e.g., 'mm')
		 */
		protected String getFromUnits() {
			return this.strFromUnits;
		}
		
		/**
		 * Gets the "from" denominator when doing a cross-multiply conversion.
		 * 
		 * @return		the "from" cross-multiply denominator
		 */
		protected double getFromDenom() {
			return this.dFromDenom;
		}
		
		/**
		 * Gets the format for displaying the "from" data value.
		 * 
		 * @return		the format to use for display the "from" data
		 */
		protected String getFromFormat() {
			return this.strFromFormat;
		}
		
		/**
		 * Gets the "to" units for a conversion.
		 * 
		 * @return		the units to convert to (e.g., "inches")
		 */
		protected String getToUnits() {
			return this.strToUnits;
		}
		
		/**
		 * Gets the "to" denominator when doing a cross-multiply conversion.
		 * 
		 * @return		the "to" cross-multiply denominator
		 */
		protected double getToDenom() {
			return this.dToDenom;
		}
		
		/**
		 * Gets the format for displaying the "to" data value.
		 * 
		 * @return		the format to use for display the "to" data
		 */
		protected String getToFormat() {
			return this.strToFormat;
		}
		
		/**
		 * Gets the type of conversion to perform.
		 * 
		 * @return		the type of conversion to perform
		 */
		protected ConversionType getConvType() {
			return this.eConvType;
		}
	}

	/*--------------------------------------------------------
	 * Private methods used only in this class
	 --------------------------------------------------------*/

	/**
	 * Creates a menu item and adds it to the Conversions menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param FromUnits			the units being converted from (e.g., mm)
	 * @param FromDenom			the value of the 'from' denominator (e.g., 5.0)
	 * @param FromFormat		how to format the 'from' data value
	 * @param ToUnits			the units to convert to (e.g., inches)
	 * @param ToDenom			the value of the 'to' denominator
	 * @param ToFormat			how to format the 'to' data value 
	 * @param ConvType			the type of conversion to perform
	 * @return					the ChapMnuItem object that was created
	 */     
	private ChapMnuItem createMenuItem(JMenu menu,String FromUnits,double FromDenom,String FromFormat,
			String ToUnits,double ToDenom,String ToFormat,
			ConversionType ConvType) {

		ChapMnuItem tmp = new ChapMnuItem(FromUnits+" - "+ToUnits,FromUnits,FromDenom,FromFormat,ToUnits,ToDenom,ToFormat,ConvType);			
		menu.add(tmp);
		
		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getConvMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());	

		return tmp;
	}
}
