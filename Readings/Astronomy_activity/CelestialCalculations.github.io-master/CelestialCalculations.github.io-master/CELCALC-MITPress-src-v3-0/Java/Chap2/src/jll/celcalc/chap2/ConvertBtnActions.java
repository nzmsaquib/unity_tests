package jll.celcalc.chap2;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Convert button.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class ConvertBtnActions {

	/**
	 * Convert between degrees C and degrees F. The data
	 * to be converted is in the txtboxUserInput field in the GUI.
	 * <p>
	 * Gets the input data from the GUI, validates it, then does the conversion.
	 * 
	 * @return			the temperature conversion results as a string
	 */
	protected String convertC_F() {
		double degOut, degIn;
		String strInput = ASTStr.removeWhitespace(ChapGUI.getUserInput());
		ASTReal rTmp;
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strFromUnits = ChapGUI.getFromUnits();
		String strToUnits = ChapGUI.getToUnits();
		String strToFormat = ChapGUI.getToFormat();
		String strFromFormat = ChapGUI.getFromFormat();
		double dFromDenom = ChapGUI.getFromDenom();

		rTmp = ASTReal.isValidReal(strInput);
		if (!rTmp.isValidRealObj()) return " ";

		degIn = rTmp.getRealValue();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		// See if we're converting C to F or F to C
		if (dFromDenom < 0.0) {				// convert C to F
			degOut = 32.0 + (degIn * 9.0) / 5.0;
			ChapGUI.printlnCond("1. The proper formula is deg F = 32 + (deg C * 9) / 5");
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("2. Putting in the known values and performing the math, we have");
			ChapGUI.printlnCond("   deg F = 32 + (" + String.format(strFromFormat,degIn) + " deg * 9) / 5");
			ChapGUI.printlnCond("         = " + String.format(strToFormat,degOut) + " deg F");

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("Thus, " + String.format(strFromFormat,degIn) + " degrees C = " + 
					String.format(strToFormat,degOut) + " degrees F");
		} else {
			degOut = (degIn - 32.0) * (5.0 / 9.0);

			ChapGUI.printlnCond("1. The proper formula is deg C = (deg F - 32) * (5/9)");

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("2. Putting in the known values and performing the math, we have");
			ChapGUI.printlnCond("   deg C = (" + String.format(strFromFormat,degIn) + " deg - 32) * (5/9)");
			ChapGUI.printlnCond("         = " + String.format(strToFormat,degOut) + " deg C");

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("Thus, " + String.format(strFromFormat,degIn) + " degrees F = " + 
					String.format(strToFormat,degOut) + " degrees C");
		}

		prt.setProportionalFont();
		prt.resetCursor();
		return String.format(strToFormat,degOut);
	}

	/**
	 * Cross-multiply to do the requested conversion. The data
	 * to be converted is in the txtboxUserInput field in the GUI.
	 * 
	 * @return			the conversion results as a string
	 */
	protected String crossMultiply() {
		String strInput = ASTStr.removeWhitespace(ChapGUI.getUserInput());
		ASTReal rTmp;
		ASTPrt prt = ChapGUI.getPrtInstance();
		String result = " ";
		String strFromFormat = ChapGUI.getFromFormat();
		String strFromUnits = ChapGUI.getFromUnits();
		String strToFormat = ChapGUI.getToFormat();
		String strToUnits = ChapGUI.getToUnits();
		double xTo, yFrom;
		double dFromDenom = ChapGUI.getFromDenom();
		double dToDenom = ChapGUI.getToDenom();

		rTmp = ASTReal.isValidReal(strInput,ASTMisc.HIDE_ERRORS);
		if (rTmp.isValidRealObj()) {
			prt.setBoldFont(true);
			ChapGUI.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			ChapGUI.printlnCond();

			yFrom = rTmp.getRealValue();
			xTo = (yFrom * dToDenom)/dFromDenom;
			result = String.format(strToFormat,xTo);

			prt.setFixedWidthFont();

			ChapGUI.printlnCond("1. Form an equation relating " + strToUnits + " and " + strFromUnits + ". Since");
			ChapGUI.printlnCond("        " + String.format(strToFormat,dToDenom) + " " + strToUnits + " = " +
					String.format(strFromFormat,dFromDenom) + " " + strFromUnits + ",");
			ChapGUI.printlnCond("   the equation we must form is");
			ChapGUI.printlnCond("        x / (" + String.format(strToFormat,dToDenom) + " " + strToUnits + ") = " +
					"(" + String.format(strFromFormat,yFrom) + " " + strFromUnits + " / " +
					String.format(strFromFormat,dFromDenom) + " " + strFromUnits + ")");

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("2. The units on the right side are the same, so cancel them out");
			ChapGUI.printlnCond("   and then cross-multiply to get");
			ChapGUI.printlnCond("        (x) * (" + String.format(strFromFormat,dFromDenom) + ")" +
					" = (" + String.format(strFromFormat,yFrom) + ") * (" + String.format(strToFormat,dToDenom) +
					" " + strToUnits + "),");
			ChapGUI.printlnCond("   which simplifies to");
			ChapGUI.printlnCond("        " + String.format(strFromFormat,dFromDenom) + "x = " +
					String.format(strToFormat, yFrom * dToDenom) + " " + strToUnits);

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("3. Divide both sides of the equation by " + String.format(strFromFormat,dFromDenom) +
					" to get the answer.");
			ChapGUI.printlnCond("   Doing so gives");
			ChapGUI.printlnCond("        x = " + result + " " + strToUnits);

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("Thus, " + String.format(strFromFormat,yFrom) + " " + strFromUnits + " = " +
					result + " " + strToUnits);
		} else prt.println("Invalid input data. Try again.");

		prt.setProportionalFont();
		prt.resetCursor();
		return result;
	}

	/**
	 * Convert between HMS and decimal hours, and DMS and decimal
	 * degrees. The process is the same with only the units being 
	 * different between the two.
	 *  
	 * @return			the conversion result as a string
	 */
	protected String convertHorDMS_decHrsorDegs() {
		String strInput = ASTStr.removeWhitespace(ChapGUI.getUserInput());
		ASTReal rTmp;
		ASTPrt prt = ChapGUI.getPrtInstance();
		String strFromUnits = ChapGUI.getFromUnits();
		String strToUnits = ChapGUI.getToUnits();
		String strToFormat = ChapGUI.getToFormat();
		String strFromFormat = ChapGUI.getFromFormat();
		double dFromDenom = ChapGUI.getFromDenom();
		String result = " ";
		String strDorH, strSIGN;
		boolean doingHMS, bNeg;
		ASTAngle resultDMSObj;
		ASTTime resultHMSObj;
		int SIGN, dORh, m;
		double decValue, s;

		// See if we're converting HMS/DMS to dec hrs/degs or vice versa based on menu entry
		if (dFromDenom < 0.0) {					// convert HMS/DMS to decimal hrs/degs
			// see if we're dealing with time or angles based on the menu entry
			if (strFromUnits.toUpperCase().trim().charAt(0) == 'D') {
				resultDMSObj = ASTAngle.isValidDMSAngle(strInput);
				if (!resultDMSObj.isValidAngleObj()) return result;
				strDorH = "Degrees";
				doingHMS = false;
				bNeg = resultDMSObj.isNegAngle();
				dORh = resultDMSObj.getDegrees();
				m = resultDMSObj.getMinutes();
				s = resultDMSObj.getSeconds();
				strInput = ASTAngle.angleToStr(resultDMSObj,ASTMisc.DMSFORMAT);
			} else {
				resultHMSObj = ASTTime.isValidHMSTime(strInput);
				if (!resultHMSObj.isValidTimeObj()) return result;
				doingHMS = true;
				strDorH = "Hours";
				bNeg = resultHMSObj.isNegTime();
				dORh = resultHMSObj.getHours();
				m = resultHMSObj.getMinutes();
				s = resultHMSObj.getSeconds();
				strInput = ASTTime.timeToStr(bNeg,dORh,m,s,ASTMisc.HMSFORMAT);			
			}
			prt.setBoldFont(true);
			ChapGUI.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits, ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			ChapGUI.printlnCond();

			if (bNeg) SIGN = -1;
			else SIGN = 1;

			prt.setFixedWidthFont();			
			ChapGUI.printlnCond("1. If the " + strFromUnits + " value entered is negative, let SIGN = -1");
			ChapGUI.printlnCond("   else let SIGN = 1. Thus, SIGN = " + String.format("%d",SIGN));

			if (bNeg) dORh = -dORh;				// Temporarily make it negative for the next step
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("2. Let " + strDorH + " = ABS(" + strDorH + " entered) = ABS(" + 
					String.format("%d",dORh) + ") = " +	String.format("%d",Math.abs(dORh)));
			dORh = Math.abs(dORh);

			decValue = s / 60.0;
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("3. Convert seconds entered to decimal minutes (i.e., dm)");
			ChapGUI.printlnCond("   dm = seconds / 60 = " + String.format(strToFormat,s) + 
					" / 60 = " + String.format(strToFormat,decValue));

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("4. Add results of step 3 to minutes entered");
			ChapGUI.printlnCond("   Total minutes = dm + m = " + String.format(strToFormat,decValue) + " + " + 
					String.format("%d",m) + " = " + String.format(strToFormat,decValue + m));
			decValue = decValue + m;

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("5. Convert total minutes to " + strToUnits);
			ChapGUI.printlnCond("   " + strToUnits + " = (Total minutes) / 60 = (" +
					String.format(strToFormat,decValue) + ") / 60" + " = " + String.format(strToFormat,decValue / 60.0));
			decValue = decValue / 60.0;

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("6. Add results of step 5 to the " + strDorH + " from step 2");
			ChapGUI.printlnCond("   " + strToUnits + " = " + strToUnits + " + " + strDorH +
					" = " + String.format(strToFormat,decValue) + " + " + String.format("%d",dORh) +
					" = " + String.format(strToFormat,decValue + dORh));
			decValue = decValue + dORh;

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("7. Account for the sign of the " + strDorH + " that were entered");
			ChapGUI.printlnCond("   " + strToUnits + " = SIGN * (" + strToUnits + ") = " +
					String.format("%d",SIGN) + " * " + String.format(strToFormat,decValue) +
					" = " + String.format(strToFormat,SIGN * decValue));
			decValue = decValue * SIGN;

			result = String.format(strToFormat,decValue);

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("Thus, " + strInput + " " + strFromUnits + " = " + result);
		} else {									// convert decimal hrs/degs to HMS/DMS
			rTmp = ASTReal.isValidReal(strInput);
			if (!rTmp.isValidRealObj()) return result;
			decValue = rTmp.getRealValue();
			strInput = String.format(strFromFormat,decValue);

			prt.setBoldFont(true);
			ChapGUI.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			ChapGUI.printlnCond();

			// Figure out if we're doing degrees or time
			if (strToUnits.toLowerCase().trim().charAt(0) == 'd') {
				doingHMS = false;
				strDorH = "Degrees";
			} else {
				doingHMS = true;
				strDorH = "Hours";	
			}

			prt.setFixedWidthFont();
			ChapGUI.printlnCond("Dec is the decimal number to convert = " + 
					String.format(strFromFormat,decValue) + " " + strDorH);
			ChapGUI.printlnCond();

			if (decValue < 0) {
				SIGN = -1;
				bNeg = true;
			} else {
				SIGN = 1;
				bNeg = false;
			}

			ChapGUI.printlnCond("1. If Dec is negative, let SIGN = -1 else let SIGN = 1. Thus, SIGN = " + 
					String.format("%d",SIGN));
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("2. Dec = ABS(Dec) = ABS(" + String.format(strFromFormat,decValue) + ") = " +
					String.format(strFromFormat,Math.abs(decValue)));
			decValue = Math.abs(decValue);

			dORh = ASTMath.Trunc(decValue);
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("3. " + strDorH + " = INT(Dec) = INT(" + String.format(strFromFormat,decValue) + ") = " +
					String.format("%d",dORh));

			m = (int) (60.0 * ASTMath.Frac(decValue));
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("4. Minutes = INT[ 60 * FRAC(Dec) ] = " + "INT[ 60 * FRAC(" + 
					String.format(strFromFormat,decValue) + ") ]");
			ChapGUI.printlnCond("           = " + String.format("%d",m));

			s = ASTMath.Frac(60.0 * ASTMath.Frac(decValue));
			ChapGUI.printlnCond();
			ChapGUI.printlnCond("5. Seconds = 60 * FRAC[ 60 * FRAC(Dec) ]");
			ChapGUI.printlnCond("           = 60 * FRAC[ 60 * FRAC(" + String.format(strFromFormat,decValue) + ") ]");
			ChapGUI.printlnCond("           = " + String.format(strFromFormat,60.0 * s));
			s = 60.0 * s;

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("6. To account for a negative decimal number being converted,");
			ChapGUI.printlnCond("   use the SIGN from step 1. Multiply by SIGN if " + strDorH + " is > 0");
			ChapGUI.printlnCond("   and merely append '-' if " + strDorH + " is 0. So,");
			if (dORh == 0) {
				if (bNeg) strSIGN = "-";
				else strSIGN = "";
				ChapGUI.printlnCond("   " + strDorH + " = " + strSIGN + dORh);
			} else {
				ChapGUI.printlnCond("   " + strDorH + " = SIGN * " + strDorH + " = (" + String.format("%d",SIGN) +
						") * (" + String.format("%d",dORh) + ") = " + String.format("%d",SIGN * dORh));
			}

			if (doingHMS) result = ASTTime.timeToStr(bNeg,dORh,m,s,ASTMisc.HMSFORMAT);
			else result = ASTAngle.angleToStr(bNeg, dORh,m,s,ASTMisc.DMSFORMAT);

			ChapGUI.printlnCond();
			ChapGUI.printlnCond("Thus, " + strInput + " " + strDorH + " = " + result);
		}		

		prt.setProportionalFont();
		prt.resetCursor();
		return result;
	}
}
