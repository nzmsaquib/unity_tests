package jll.celcalc.chap2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap2.ChapMenuItems.ChapMnuItem;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class MenusListener implements ActionListener {
	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}
		
		// Handle Conversions menu
		else if (action.equals(ChapGUI.getConvMenuCommand())) {
			ChapMnuItem objMenuItem = (ChapMnuItem) e.getSource();		// the menu item the user clicked on
			
			// A click on a menu item merely means to set the conversion units data
			ChapGUI.saveFromToValues(objMenuItem.getFromUnits(),objMenuItem.getFromDenom(),objMenuItem.getFromFormat(),
					objMenuItem.getToUnits(),objMenuItem.getToDenom(),objMenuItem.getToFormat(),
					objMenuItem.getConvType());
			prt.clearTextArea();			
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			prt.clearTextArea();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " +
					"intermediate results as the conversions are performed. Then select the " +
					"conversion to perform from the 'Conversions' menu. For example, select the " +
					"'mm - inches' menu item to convert between millimeters and inches.");
			prt.println();

			prt.println("The units on the left side of the menu (e.g., mm) will be converted to the units " +
					"on the right side of the menu (e.g., inches). To perform a conversion the other " +
					"way (e.g., inches to mm), click the 'Swap Units' button.");
			prt.println();

			prt.println("Enter the value to convert in the white input box to the right of the 'Convert' " +
					"button. For all conversions except HMS/DMS, enter only digits, a decimal point, and " +
					"a + or - sign, if needed. To convert HMS values, enter the HMS value in the form " +
					"hh:mm:ss.ssss where hh is the hours, mm is the minutes, and ss.ssss is the seconds. (Note that " +
					"time is expressed in 24 hour format, so 15:30:20 would be entered for 3:30:20 PM.) " +
					"For DMS values, enter them in the form xxxd yym zz.zzzzs where xxx is the degrees, " +
					"yy is the arcminutes, and zz.zzzzs is the arcseconds (e.g., 160d 8m 15.2s).");
			prt.println();

			prt.print("Finally, click the 'Convert' button to perform the conversion. The result of the " +
					"conversion will be shown directly underneath the white input box. If the " +
					"'Show Intermediate Calculations' checkbox is checked, intermediate calculation results " +
					"will be shown in the scrollable text area at the bottom of the application window.");
			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}
}
