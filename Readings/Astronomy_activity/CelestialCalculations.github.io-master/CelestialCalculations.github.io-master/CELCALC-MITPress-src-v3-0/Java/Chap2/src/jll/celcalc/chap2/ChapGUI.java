package jll.celcalc.chap2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jll.celcalc.ASTUtils.ASTAboutBox;
import jll.celcalc.ASTUtils.ASTBookInfo;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.chap2.ChapMenuItems.ConversionType;

/**
 * <b>Implements the main GUI</b>
 * <p>
 * The GUI was created with, and is maintained by, the Eclipse WindowBuilder.
 * All methods and data are declared static because it only makes sense to have
 * one main GUI per application, and making them static avoids the problem of
 * having to pass a reference to the GUI instance in other classes that need
 * to reference the main GUI.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 * <p>
 * Note that the Eclipse Window Builder has trouble with complex, dynamic GUIs so that
 * one should first build the GUI, then modify the code by hand to use
 * the definitions in ASTUtils.ASTStyle. The Window Builder start/stop hiding tags can be used to
 * surround code that the Window Builder parser has trouble handling.
 * The start hiding tag is <code>{@literal //$hide>>$}</code> while the stop
 * hiding tag is <code>{@literal //$hide<<$}</code>. For example,
 * <p>
 * <code>
 * {@literal //$hide>>$}
 * 		code to be hidden
 * {@literal //$hide<<$}
 * </code>
 */

@SuppressWarnings("serial")
class ChapGUI extends JFrame {
	private static final String WINDOW_TITLE = "Chapter 2 - Unit Conversions";

	// Save the scrollable text pane area for output
	private static JTextPane textPane = new JTextPane();
	private static ASTPrt prt = null;				// create instance (actually done below) for printing to the output textPane

	// Create an About Box
	private static ASTAboutBox aboutBox = new ASTAboutBox(WINDOW_TITLE);

	// Create listeners for the various GUI components
	private static ActionListener listenerMenus = new MenusListener();
	private static ActionListener listenerGUIBtns = new GUIButtonsListener();

	// Various GUI components that user will interact with
	private static JCheckBox chkboxShowInterimCalcs = new JCheckBox("Show Intermediate Calculations");
	private static JLabel lblResults = new JLabel("");			// label for displaying conversion results	
	private static JLabel lblFromUnits = new JLabel("");		// units that are to be converted from
	private static JLabel lblToUnits = new JLabel("");			// units to convert to
	private static JTextField txtboxUserInput;					// area for user to enter data

	// These variables are used to keep track of what conversion to do when the user
	// clicks on the Convert button
	private static double dFromDenom = 1.0;
	private static String strFromFormat = " ";
	private static double dToDenom = 1.0;
	private static String strToFormat = " ";
	private static ConversionType eConvType = ConversionType.CROSS_MULTIPLY;			// type of conversion to do

	/*=============================================================================================
	 * The string text collected in these next items is done to separate the strings displayed
	 * in the various GUI components (such as buttons and menus) from the semantics of what 
	 * clicking on a component means. This is important because otherwise, a change to the text
	 * displayed in the GUI, such as for a button label, via Eclipse WindowBuilder may well 
	 * break other code, particularly the action listeners. Through this technique, the 
	 * WindowBuilder can be used to change the label on a GUI element (e.g., button) without
	 * breaking any other code. The command that a listener will receive is the same regardless
	 * of what the actual label on the GUI component says. So, for example, the text "Exit"
	 * be changed in the menu to "Quit" without breaking the menu's listener because the
	 * listener will receive the command exitCommand regardless of what text is actually
	 * displayed in the menu. Get methods below (e.g., getExitCommand) return the appropriate
	 * command for the listeners to use.
	 *============================================================================================*/	
	/* define menu item commands */
	private static final String exitCommand = "exit";
	private static final String instructionsCommand = "instruct";
	private static final String aboutCommand = "about";
	private static final String convMenuCommand = "convMenuItem";

	/* define button controls */
	private static final String convertCommand = "convert";
	private static final String swapCommand = "swap";

	/**
	 * Create the GUI frame.
	 */
	protected ChapGUI() {
		JPanel contentPane = new JPanel();
		contentPane.setFocusCycleRoot(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChapGUI.class.getResource("/resources/BlueMarble.png")));
		setTitle(WINDOW_TITLE);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 700);
		contentPane.setDoubleBuffered(false);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		contentPane.setFocusTraversalPolicyProvider(true);

		/*======================== Create Menus =====================*/
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(ASTStyle.MENU_FONT);
		setJMenuBar(menuBar);

		// File menu
		JMenu mnFile = new JMenu(" File ");
		mnFile.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setFont(ASTStyle.MENU_FONT);
		mntmExit.setActionCommand(exitCommand);
		mntmExit.addActionListener(listenerMenus);
		mnFile.add(mntmExit);

		// Conversions menu
		JMenu mnConversions = new JMenu(" Conversions ");
		mnConversions.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnConversions);

		// This much simpler code will populate the menu via the separate ConversionMenuItems class,
		// but at the price of not being able to see the menu via WindowBuilder
		// No need to store the menu items created because the results of the "new" is never referenced elsewhere
		new ChapMenuItems(mnConversions);

		// Help menu
		JMenu mnHelp = new JMenu(" Help ");
		mnHelp.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnHelp);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setFont(ASTStyle.MENU_FONT);
		mntmInstructions.setActionCommand(instructionsCommand);
		mntmInstructions.addActionListener(listenerMenus);
		mnHelp.add(mntmInstructions);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setFont(ASTStyle.MENU_FONT);
		mntmAbout.addActionListener(listenerMenus);
		mntmAbout.setActionCommand(aboutCommand);
		mnHelp.add(mntmAbout);
		/*========================== End of Menus ==========================*/		

		setContentPane(contentPane);

		JPanel panelBookTitle = new JPanel();
		panelBookTitle.setFocusable(false);
		panelBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		panelBookTitle.setBorder(new EmptyBorder(0, 0, 0, 0));

		JScrollPane scrollPaneOutput = new JScrollPane();
		scrollPaneOutput.setFocusable(false);
		scrollPaneOutput.setFocusTraversalKeysEnabled(false);
		scrollPaneOutput.setRequestFocusEnabled(false);
		scrollPaneOutput.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));

		JPanel panelGrouping = new JPanel();
		panelGrouping.setFocusCycleRoot(true);
		panelGrouping.setDoubleBuffered(false);
		panelGrouping.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		chkboxShowInterimCalcs.setHorizontalAlignment(SwingConstants.CENTER);
		chkboxShowInterimCalcs.setAlignmentY(Component.TOP_ALIGNMENT);
		chkboxShowInterimCalcs.setAlignmentX(Component.CENTER_ALIGNMENT);
		chkboxShowInterimCalcs.setFocusable(false);
		chkboxShowInterimCalcs.setFocusTraversalKeysEnabled(false);
		chkboxShowInterimCalcs.setFocusPainted(false);
		chkboxShowInterimCalcs.setFont(ASTStyle.CBOX_FONT);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBookTitle, GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 758, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(261)
					.addComponent(chkboxShowInterimCalcs, GroupLayout.DEFAULT_SIZE, 260, Short.MAX_VALUE)
					.addGap(261))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(10)
					.addComponent(panelGrouping, GroupLayout.DEFAULT_SIZE, 760, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panelBookTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(chkboxShowInterimCalcs)
					.addGap(9)
					.addComponent(panelGrouping, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
					.addContainerGap())
		);

		JPanel panelUserInputs = new JPanel();
		panelUserInputs.setVerifyInputWhenFocusTarget(false);
		panelUserInputs.setFocusCycleRoot(true);

		JPanel panelUnits = new JPanel();
		panelUnits.setVerifyInputWhenFocusTarget(false);
		panelUnits.setRequestFocusEnabled(false);
		panelUnits.setFocusable(false);
		panelUnits.setFocusTraversalKeysEnabled(false);
		GroupLayout gl_panelGrouping = new GroupLayout(panelGrouping);
		gl_panelGrouping.setHorizontalGroup(
			gl_panelGrouping.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelGrouping.createSequentialGroup()
					.addContainerGap()
					.addComponent(panelUserInputs, GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
					.addGap(2)
					.addComponent(panelUnits, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panelGrouping.setVerticalGroup(
			gl_panelGrouping.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelGrouping.createSequentialGroup()
					.addGap(9)
					.addGroup(gl_panelGrouping.createParallelGroup(Alignment.LEADING)
						.addComponent(panelUnits, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
						.addComponent(panelUserInputs, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(6))
		);

		lblFromUnits.setVerifyInputWhenFocusTarget(false);
		lblFromUnits.setHorizontalAlignment(SwingConstants.LEFT);
		lblFromUnits.setFont(ASTStyle.TEXT_FONT);

		lblToUnits.setVerifyInputWhenFocusTarget(false);
		lblToUnits.setHorizontalAlignment(SwingConstants.LEFT);
		lblToUnits.setFont(ASTStyle.TEXT_FONT);

		GroupLayout gl_panelUnits = new GroupLayout(panelUnits);
		gl_panelUnits.setHorizontalGroup(
			gl_panelUnits.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelUnits.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panelUnits.createParallelGroup(Alignment.LEADING)
						.addComponent(lblToUnits, GroupLayout.PREFERRED_SIZE, 244, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblFromUnits, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panelUnits.setVerticalGroup(
			gl_panelUnits.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelUnits.createSequentialGroup()
					.addGap(7)
					.addComponent(lblFromUnits, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblToUnits, GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE))
		);
		panelUnits.setLayout(gl_panelUnits);

		JButton btnConvert = new JButton("Convert");
		btnConvert.setFont(ASTStyle.BTN_FONT);
		btnConvert.addActionListener(listenerGUIBtns);
		btnConvert.setActionCommand(convertCommand);	

		JButton btnSwapUnits = new JButton("Swap Units");
		btnSwapUnits.setFont(ASTStyle.BTN_FONT);
		btnSwapUnits.addActionListener(listenerGUIBtns);
		btnSwapUnits.setActionCommand(swapCommand);		
		lblResults.setFocusable(false);
		lblResults.setFocusTraversalKeysEnabled(false);

		lblResults.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResults.setFont(ASTStyle.TEXT_BOLDFONT);

		txtboxUserInput = new JTextField();
		txtboxUserInput.setVerifyInputWhenFocusTarget(false);
		txtboxUserInput.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxUserInput.setAutoscrolls(false);
		txtboxUserInput.setFont(ASTStyle.TEXT_FONT);
		txtboxUserInput.setColumns(10);
		GroupLayout gl_panelUserInputs = new GroupLayout(panelUserInputs);
		gl_panelUserInputs.setHorizontalGroup(
			gl_panelUserInputs.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelUserInputs.createSequentialGroup()
					.addGroup(gl_panelUserInputs.createParallelGroup(Alignment.LEADING, false)
						.addComponent(btnConvert, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(btnSwapUnits, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panelUserInputs.createParallelGroup(Alignment.LEADING)
						.addComponent(txtboxUserInput, GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
						.addComponent(lblResults, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 221, GroupLayout.PREFERRED_SIZE)))
		);
		gl_panelUserInputs.setVerticalGroup(
			gl_panelUserInputs.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelUserInputs.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panelUserInputs.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnConvert)
						.addComponent(txtboxUserInput, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addGroup(gl_panelUserInputs.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblResults)
						.addComponent(btnSwapUnits)))
		);
		panelUserInputs.setLayout(gl_panelUserInputs);
		panelGrouping.setLayout(gl_panelGrouping);

		panelBookTitle.setLayout(new BorderLayout(0, 0));		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookTitle.setFont(ASTStyle.BOOK_TITLE_BOLDFONT);
		panelBookTitle.add(lblBookTitle, BorderLayout.CENTER);
		contentPane.setLayout(gl_contentPane);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		// Define the actual text area and add it to scrollPaneOutput
		textPane.setFont(ASTStyle.OUT_TEXT_FONT);
		scrollPaneOutput.setViewportView(textPane);

		// Initialize the rest of the GUI
		ASTStyle.setASTStyle();
		aboutBox.pack();
		aboutBox.setParentFrame(this);	
		setResults(" ");
		chkboxShowInterimCalcs.setSelected(false);
		// Set parent frame for static methods that need to center a pane/frame/etc. on the GUI
		ASTQuery.setParentFrame(this);
		ASTMsg.setParentFrame(this);

		txtboxUserInput.setText("");
		textPane.setText("     "); // must initialize with at least a few blanks
		prt = new ASTPrt(textPane);
		
		// Explicitly indicate the order in which we want the GUI components traversed
		Vector<Component> order = new Vector<Component>(3);
		order.add(txtboxUserInput); order.add(btnConvert); order.add(btnSwapUnits);
		ASTStyle.GUIFocusTraversalPolicy GUIPolicy = new ASTStyle.GUIFocusTraversalPolicy(order);
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setFocusTraversalPolicy(GUIPolicy);
		
	}

	/*============================================================================
	 * The methods below return references to various GUI components such as the
	 * scrollable output text area.
	 *===========================================================================*/
	
	/**
	 * Gets the listener for the menu items
	 * 
	 * @return		the action listener for handling the menus
	 */
	protected static ActionListener getMenuListener() {
		return listenerMenus;
	}

	/**
	 * Gets the ASTPrt instance for this application's scrollable text pane area.
	 * 
	 * @return		the ASTPrt instance for this application
	 */
	protected static ASTPrt getPrtInstance() {
		return prt;
	}

	/**
	 * Gets the scrollable text pane area for this GUI.
	 * 
	 * @return		the JTextPane for this GUI
	 */
	protected static JTextPane getTextPane() {
		return textPane;
	}

	/**
	 * Saves the various from/to conversion related items in the appropriate GUI variables.
	 * 
	 * @param FromUnits		units to convert from
	 * @param FromDenom		the 'from' denominator for cross-multiplying
	 * @param FromFormat	the format to use when displaying the "from" value
	 * @param ToUnits		units to convert to
	 * @param ToDenom		the "to" denominator for cross-multiplying
	 * @param ToFormat		the format to use when displaying the "to" value
	 * @param ConvType		the type of conversion required (e.g., cross-multiply)
	 */
	protected static void saveFromToValues(String FromUnits,double FromDenom,String FromFormat,
			String ToUnits,double ToDenom,String ToFormat,ChapMenuItems.ConversionType ConvType) {
		lblFromUnits.setText(FromUnits);
		dFromDenom = FromDenom;
		strFromFormat = FromFormat;
		lblToUnits.setText(ToUnits);
		dToDenom = ToDenom;
		strToFormat = ToFormat;
		eConvType = ConvType;
		setResults(" ");
	}
	
	/**
	 * Swaps the conversion related information and clears the results label in the GUI.
	 */
	protected static void swapUnits() {
		String strTemp;
		double dTemp;

		strTemp = lblToUnits.getText();
		lblToUnits.setText(lblFromUnits.getText());
		lblFromUnits.setText(strTemp);

		strTemp = strToFormat;
		strToFormat = strFromFormat;
		strFromFormat = strTemp;

		dTemp = dToDenom;
		dToDenom = dFromDenom;
		dFromDenom = dTemp;

		// Also make the current results the input before clearing the results
		txtboxUserInput.setText(getResults());
		setResults(" ");
	}
	
	/*==============================================================================
	 * The methods below are 'getters' and 'setters' for various items in the GUI.
	 *=============================================================================*/

	/**
	 * Gets the type of conversion to be performed (cross-multiply, HMS/DMS, deg C/F).
	 * 
	 * @return		the type of conversion to perform
	 */
	protected static ConversionType getConvType() {
		return eConvType;
	}
	
	/**
	 * Gets the "from" denominator for a cross multiplication.
	 * 
	 * @return		the "from" denominator for a cross multiplication
	 */
	protected static double getFromDenom() {
		return dFromDenom;
	}
	
	/**
	 * Gets the "from" format for displaying results.
	 * 
	 * @return		the "from" formatting string
	 */
	protected static String getFromFormat() {
		return strFromFormat;
	}
	
	/**
	 * Gets the "from" units.
	 * 
	 * @return		the units from which a conversion is to be performed
	 */
	protected static String getFromUnits() {
		return lblFromUnits.getText();
	}
	
	/**
	 * Gets the results label value from the GUI.
	 * 
	 * @return		string currently stored in the results label in the GUI
	 */
	protected static String getResults() {
		return lblResults.getText();
	}

	/**
	 * Gets the "to" denominator for a cross multiplication.
	 * 
	 * @return		the "to" denominator for a cross multiplication
	 */
	protected static double getToDenom() {
		return dToDenom;
	}

	/**
	 * Gets the "to" format for displaying results.
	 * 
	 * @return		the "to" formatting string
	 */
	protected static String getToFormat() {
		return strToFormat;
	}
	
	/**
	 * Gets the "to" units.
	 * 
	 * @return		the units to which a conversion is to be performed
	 */
	protected static String getToUnits() {
		return lblToUnits.getText();
	}	
	
	/**
	 * Gets the data that the user entered in the GUI.
	 * <p>
	 * It is up to the calling routine to perform any error checking.
	 * 
	 * @return		the String that the user entered
	 */
	protected static String getUserInput() {
		return txtboxUserInput.getText();
	}
	
	/**
	 * Sets the results label in the GUI.
	 * 
	 * @param results 	the conversion results to be displayed in the GUI
	 */
	protected static void setResults(String results) {
		lblResults.setText(results);
	}
	
	/*=============================================================================
	 * The methods below return the command associated with a specific menu or
	 * GUI button. This is done to separate the actual string in the GUI that
	 * represents an action (e.g., "Exit") from the semantics of the action that is
	 * to occur.
	 *============================================================================*/
	
	//*********************** GUI buttons
	
	/**
	 * Gets the command that represents the Convert button.
	 * 
	 * @return			Convert command
	 */
	protected static String getConvertCommand() {
		return convertCommand;
	}

	/**
	 * Gets the command that represents the Swap Units button
	 * 
	 * @return			Swap command
	 */
	protected static String getSwapCommand() {
		return swapCommand;
	}
	
	//*********************** Menu items

	/**
	 * Gets the command that represents a Conversions menu item.
	 * 
	 * Note that all conversion menu items will trigger off the
	 * same command. Fields in the ChapMenuItem class will be used
	 * to figure out what specific menu item was selected.
	 * 
	 * @return			Conversions Menu command
	 */
	protected static String getConvMenuCommand() {
		return convMenuCommand;
	}
	
	/**
	 * Gets the command that represents the Exit menu item.
	 * 
	 * @return			Exit command
	 */
	protected static String getExitCommand() {
		return exitCommand;
	}

	/**
	 * Gets the command that represents the Instructions menu item.
	 * 
	 * @return			Instructions command
	 */
	protected static String getInstructionsCommand() {
		return instructionsCommand;
	}

	/**
	 * Gets the command that represents the About menu item.
	 * 
	 * @return			About command
	 */
	protected static String getAboutCommand() {
		return aboutCommand;
	}

	/*=================================
	 * AboutBox methods
	 *================================*/

	/**
	 * Shows the About Box.
	 */
	protected static void showAboutBox() {
		aboutBox.showAboutBox();
	}
		
	/*=====================================================================
	 * The methods below conditionally print to the scrollable text area
	 *====================================================================*/

	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 */
	protected static void printlnCond() {
		if (chkboxShowInterimCalcs.isSelected()) prt.println();
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printlnCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 * @param centerTxt		true if the text is to be centered
	 */
	protected static void printlnCond(String txt, boolean centerTxt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt,centerTxt);
	}

}
