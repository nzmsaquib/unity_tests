package jll.celcalc.chap8;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import jll.celcalc.ASTUtils.ASTAboutBox;
import jll.celcalc.ASTUtils.ASTBookInfo;
import jll.celcalc.ASTUtils.ASTCharts;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTLatLon;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Implements the main GUI</b>
 * <p>
 * The GUI was created with, and is maintained by, the Eclipse WindowBuilder.
 * All methods and data are declared static because it only makes sense to have
 * one main GUI per application, and making them static avoids the problem of
 * having to pass a reference to the GUI instance in other classes that need
 * to reference the main GUI.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 * <p>
 * Note that the Eclipse Window Builder has trouble with complex, dynamic GUIs so that
 * one should first build the GUI, then modify the code by hand to use
 * the definitions in ASTUtils.ASTStyle. The Window Builder start/stop hiding tags can be used to
 * surround code that the Window Builder parser has trouble handling.
 * The start hiding tag is <code>{@literal //$hide>>$}</code> while the stop
 * hiding tag is <code>{@literal //$hide<<$}</code>. For example,
 * <p>
 * <code>
 * {@literal //$hide>>$}
 * 		code to be hidden
 * {@literal //$hide<<$}
 * </code>
 */

@SuppressWarnings("serial")
class ChapGUI extends JFrame {
	private static final String WINDOW_TITLE = "Chapter 8 - Our Solar System";

	// Save the scrollable text pane area for output
	private static JTextPane textPane = new JTextPane();
	private static JScrollPane scrollPaneOutput = new JScrollPane();
	
	private static ASTPrt prt = null;				// create instance (actually done below) for printing to the output textPane
	private static ASTObserver observer = null;		// default observer location for the GUI
	private static ASTOrbits orbElements = null;	// Orbital elements database

	// Create an About Box
	private static ASTAboutBox aboutBox = new ASTAboutBox(WINDOW_TITLE);
	
	// Create a dialog for selecting the method to use for solving Kepler's equation
	private static TrueAnomalyRBs dialogKepler = new TrueAnomalyRBs();
	
	// Maximum length of a filename to display. Actual filename can be longer, but only a limited
	// number of characters can be displayed in the GUI.
	private static final int MAX_FNAME_DISPLAY = 70;

	private static final double DEFAULT_mV_FILTER = ASTMisc.mV_NAKED_EYE;
	private static double currentmVFilter;					// whatever the current mV filter in the GUI is

	// Create menu items, which will be added to in the ChapMenuItems class
	private static JMenu mnSolarSysInfo = null;
	private static JMenu mnDataFiles = null;
	private static JMenu mnStarCharts = null;

	// Create listeners for the various GUI components
	private static ActionListener listenerMenus = new MenusListener();
	private static ActionListener listenerRadioBtns = new RadioBtnsListener();

	// Various GUI components that user will interact with
	private static JTextField txtboxLat = new JTextField("180d 30m 25.56sW");
	private static JTextField txtboxLon = new JTextField("180d 30m 25.56sW");
	private static JTextField txtboxDate = new JTextField("12/30/2020");
	private static JTextField txtboxLCT = new JTextField("24:55:55");
	private static JCheckBox chkboxDST = new JCheckBox("Daylight Saving Time");
	private static JCheckBox chkboxShowInterimCalcs = new JCheckBox("Show Intermediate Calculations");
	private static JCheckBox chkboxEQChart = new JCheckBox("Equatorial Coord Charts");
	private static JCheckBox chkboxWhiteChart = new JCheckBox("White Bkg for Charts");
	private static JCheckBox chkboxLabelObjs = new JCheckBox("Label Objects");
	private static JLabel lblCatalogType = new JLabel("Catalog Type:");
	private static JLabel lblEpoch = new JLabel("Epoch: 2000.0");
	private static JLabel lblFilename = new JLabel("Filename:");
	private static JLabel lblmVFilter = new JLabel("mvFillter: -22.5");
	private static JLabel lblResults = new JLabel("Result:");

	// define radio buttons for selecting Time Zone
	private static JRadioButton radbtnPST = new JRadioButton("PST");
	private static JRadioButton radbtnMST = new JRadioButton("MST");
	private static JRadioButton radbtnCST = new JRadioButton("CST");
	private static JRadioButton radbtnEST = new JRadioButton("EST");
	private static JRadioButton radbtnLon = new JRadioButton("Use Lon");

	// Flag indicating whether there is currently a star chart on the screen. This is
	// used so that various stars (in a constellation, brightest, etc.) can be highlighted
	// on an existing chart.
	private static boolean chartOnScreen = false;
	
	// Create a star chart instance. It will be filled in later.
	private static ASTCharts starChart = null;

	/*=============================================================================================
	 * The string text collected in these next items is done to separate the strings displayed
	 * in the various GUI components (such as buttons and menus) from the semantics of what 
	 * clicking on a component means. This is important because otherwise, a change to the text
	 * displayed in the GUI, such as for a button label, via Eclipse WindowBuilder may well 
	 * break other code, particularly the action listeners. Through this technique, the 
	 * WindowBuilder can be used to change the label on a GUI element (e.g., button) without
	 * breaking any other code. The command that a listener will receive is the same regardless
	 * of what the actual label on the GUI component says. So, for example, the text "Exit"
	 * be changed in the menu to "Quit" without breaking the menu's listener because the
	 * listener will receive the command exitCommand regardless of what text is actually
	 * displayed in the menu. Get methods below (e.g., getExitCommand) return the appropriate
	 * command for the listeners to use.
	 *============================================================================================*/	
	/* define menu item commands */
	private static final String exitCommand = "exit";
	private static final String instructionsCommand = "instruct";
	private static final String aboutCommand = "about";
	private static final String chapMenuCommand = "chapmenucommand";

	/* define radio button commands */
	private static final String radbtnPSTCommand = "PST";
	private static final String radbtnMSTCommand = "MST";
	private static final String radbtnCSTCommand = "CST";
	private static final String radbtnESTCommand = "EST";
	private static final String radbtnLonCommand = "Longitude";

	/**
	 * Create the GUI frame.
	 */
	protected ChapGUI() {
		JPanel contentPane = new JPanel();
		setIconImage(Toolkit.getDefaultToolkit().getImage(ChapGUI.class.getResource("/resources/BlueMarble.png")));
		setTitle(WINDOW_TITLE);
		setMinimumSize(new Dimension(400, 400));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 970, 860);
		contentPane.setDoubleBuffered(false);
		contentPane.setFocusable(false);
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));

		/*===================== Create Menus =====================*/
		JMenuBar menuBar = new JMenuBar();
		menuBar.setFont(ASTStyle.MENU_FONT);
		setJMenuBar(menuBar);

		// File menu
		JMenu mnFile = new JMenu(" File ");
		mnFile.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnFile);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setFont(ASTStyle.MENU_FONT);
		mntmExit.setActionCommand(exitCommand);
		mntmExit.addActionListener(listenerMenus);
		mnFile.add(mntmExit);

		// Solar Sys Info menu		
		mnSolarSysInfo = new JMenu(" Solar Sys Info ");
		mnSolarSysInfo.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnSolarSysInfo);
		
		// Data Files menu
		mnDataFiles = new JMenu(" Data Files ");
		mnDataFiles.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnDataFiles);
		
		// Star Charts menu
		mnStarCharts = new JMenu(" Star Charts ");
		mnStarCharts.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnStarCharts);

		// This much simpler code will populate the previous 2 menus via the separate ChapMenuItems class,
		// but at the price of not being able to see the menu via WindowBuilder
		// No need to store the menu items created because the results of the "new" is never referenced elsewhere
		new ChapMenuItems();

		// Help menu
		JMenu mnHelp = new JMenu(" Help ");
		mnHelp.setFont(ASTStyle.MENU_FONT);
		menuBar.add(mnHelp);

		JMenuItem mntmInstructions = new JMenuItem("Instructions");
		mntmInstructions.setFont(ASTStyle.MENU_FONT);
		mntmInstructions.setActionCommand(instructionsCommand);
		mntmInstructions.addActionListener(listenerMenus);
		mnHelp.add(mntmInstructions);

		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.setFont(ASTStyle.MENU_FONT);
		mntmAbout.addActionListener(listenerMenus);
		mntmAbout.setActionCommand(aboutCommand);
		mnHelp.add(mntmAbout);
		/*========================= End of Menus ======================*/		

		setContentPane(contentPane);

		JPanel panelBookTitle = new JPanel();
		panelBookTitle.setBackground(ASTStyle.BOOK_TITLE_BKG);
		panelBookTitle.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelBookTitle.setFocusable(false);
		panelBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.setDoubleBuffered(false);

		scrollPaneOutput.setViewportBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		scrollPaneOutput.setVerifyInputWhenFocusTarget(false);
		scrollPaneOutput.setRequestFocusEnabled(false);
		scrollPaneOutput.setFocusable(false);
		scrollPaneOutput.setFocusTraversalKeysEnabled(false);

		JPanel panelObsTitle = new JPanel();
		panelObsTitle.setRequestFocusEnabled(false);

		JPanel panelForObsData = new JPanel();

		JPanel panelchkBoxes = new JPanel();
		panelchkBoxes.setFocusable(false);
		panelchkBoxes.setFocusTraversalKeysEnabled(false);
		panelchkBoxes.setRequestFocusEnabled(false);

		lblCatalogType.setFont(ASTStyle.TEXT_BOLDFONT);		
		lblEpoch.setFont(ASTStyle.TEXT_BOLDFONT);		
		lblFilename.setFocusTraversalKeysEnabled(false);
		lblFilename.setFocusable(false);
		lblFilename.setHorizontalTextPosition(SwingConstants.LEFT);
		lblFilename.setHorizontalAlignment(SwingConstants.LEFT);
		lblFilename.setVerifyInputWhenFocusTarget(false);
		lblFilename.setFont(ASTStyle.TEXT_BOLDFONT);
		lblmVFilter.setFont(ASTStyle.TEXT_BOLDFONT);		
		lblResults.setFont(ASTStyle.TEXT_BOLDFONT);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panelBookTitle, GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE)
				.addComponent(panelObsTitle, GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE)
				.addComponent(panelForObsData, GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE)
				.addComponent(panelchkBoxes, GroupLayout.DEFAULT_SIZE, 952, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblResults, GroupLayout.DEFAULT_SIZE, 791, Short.MAX_VALUE)
							.addGap(4))
						.addComponent(lblCatalogType, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblFilename, GroupLayout.PREFERRED_SIZE, 724, GroupLayout.PREFERRED_SIZE))
					.addGap(0)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblEpoch, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblmVFilter, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE))
					.addGap(30))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 928, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panelBookTitle, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panelObsTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(1)
					.addComponent(panelForObsData, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(2)
					.addComponent(panelchkBoxes, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblEpoch)
						.addComponent(lblCatalogType))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblFilename)
						.addComponent(lblmVFilter))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblResults)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPaneOutput, GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
					.addContainerGap())
		);
		chkboxShowInterimCalcs.setFocusable(false);
		chkboxShowInterimCalcs.setFocusTraversalKeysEnabled(false);
		chkboxShowInterimCalcs.setRequestFocusEnabled(false);

		chkboxShowInterimCalcs.setFont(ASTStyle.CBOX_FONT_SMALL);
		chkboxShowInterimCalcs.setFocusPainted(false);
		chkboxEQChart.setFocusable(false);
		chkboxEQChart.setFocusTraversalKeysEnabled(false);
		chkboxEQChart.setRequestFocusEnabled(false);
		chkboxEQChart.setFont(ASTStyle.CBOX_FONT_SMALL);
		chkboxEQChart.setFocusPainted(false);
		chkboxEQChart.addActionListener(new ChartStateChangeListener());
		
		chkboxWhiteChart.setFocusable(false);
		chkboxWhiteChart.setFocusTraversalKeysEnabled(false);
		chkboxWhiteChart.setRequestFocusEnabled(false);
		chkboxWhiteChart.setFont(ASTStyle.CBOX_FONT_SMALL);
		chkboxWhiteChart.setFocusPainted(false);
		chkboxWhiteChart.addActionListener(new ChartStateChangeListener());
		
		chkboxLabelObjs.setFocusable(false);
		chkboxLabelObjs.setFocusTraversalKeysEnabled(false);
		chkboxLabelObjs.setRequestFocusEnabled(false);
		chkboxLabelObjs.setFont(ASTStyle.CBOX_FONT_SMALL);
		chkboxLabelObjs.setFocusPainted(false);
		panelchkBoxes.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelchkBoxes.add(chkboxShowInterimCalcs);
		panelchkBoxes.add(chkboxEQChart);
		panelchkBoxes.add(chkboxWhiteChart);
		panelchkBoxes.add(chkboxLabelObjs);

		JPanel panelObsData = new JPanel();
		panelObsData.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));

		txtboxLat.setFont(ASTStyle.TEXT_FONT);
		txtboxLat.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxLat.setColumns(10);

		JLabel lblLatitude = new JLabel("Latitude");
		lblLatitude.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLatitude.setHorizontalAlignment(SwingConstants.LEFT);

		txtboxLon.setFont(ASTStyle.TEXT_FONT);
		txtboxLon.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxLon.setColumns(10);

		JLabel lblLongitude = new JLabel("Longitude");
		lblLongitude.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLongitude.setHorizontalAlignment(SwingConstants.LEFT);

		txtboxLCT.setFont(ASTStyle.TEXT_FONT);
		txtboxLCT.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxLCT.setColumns(10);

		JLabel lblLCT = new JLabel("LCT");
		lblLCT.setFont(ASTStyle.TEXT_BOLDFONT);
		lblLCT.setHorizontalAlignment(SwingConstants.LEFT);

		JLabel lblDate = new JLabel("Date");
		lblDate.setFont(ASTStyle.TEXT_BOLDFONT);
		lblDate.setHorizontalAlignment(SwingConstants.LEFT);

		txtboxDate.setFont(ASTStyle.TEXT_FONT);
		txtboxDate.setHorizontalAlignment(SwingConstants.RIGHT);
		txtboxDate.setColumns(10);

		radbtnPST.setFont(ASTStyle.RADBTN_FONT_SMALL);
		radbtnPST.setActionCommand(radbtnPSTCommand);
		radbtnPST.addActionListener(listenerRadioBtns);

		radbtnMST.setFont(ASTStyle.RADBTN_FONT_SMALL);
		radbtnMST.setActionCommand(radbtnMSTCommand);
		radbtnMST.addActionListener(listenerRadioBtns);

		radbtnCST.setFont(ASTStyle.RADBTN_FONT_SMALL);
		radbtnCST.setActionCommand(radbtnCSTCommand);
		radbtnCST.addActionListener(listenerRadioBtns);

		radbtnEST.setFont(ASTStyle.RADBTN_FONT_SMALL);
		radbtnEST.setActionCommand(radbtnESTCommand);
		radbtnEST.addActionListener(listenerRadioBtns);

		radbtnLon.setFont(ASTStyle.RADBTN_FONT_SMALL);
		radbtnLon.setActionCommand(radbtnLonCommand);
		radbtnLon.addActionListener(listenerRadioBtns);

		chkboxDST.setFocusable(false);
		chkboxDST.setFocusTraversalKeysEnabled(false);
		chkboxDST.setRequestFocusEnabled(false);
		chkboxDST.setFocusPainted(false);
		chkboxDST.setFont(ASTStyle.CBOX_FONT_SMALL);

		JLabel lblTimeZone = new JLabel("Time Zone");
		lblTimeZone.setFont(ASTStyle.TEXT_BOLDFONT);
		GroupLayout gl_panelObsData = new GroupLayout(panelObsData);
		gl_panelObsData.setHorizontalGroup(
				gl_panelObsData.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelObsData.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addComponent(txtboxLat, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(lblLatitude, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addComponent(txtboxLon, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(lblLongitude, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)))
						.addGap(18)
						.addComponent(lblTimeZone)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addComponent(radbtnPST)
										.addGap(4)
										.addComponent(radbtnMST)
										.addGap(4)
										.addComponent(radbtnCST)
										.addComponent(radbtnEST)
										.addGap(4)
										.addComponent(radbtnLon))
								.addComponent(chkboxDST, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtboxDate, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtboxLCT, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addComponent(lblDate, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblLCT, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
						.addGap(16))
				);
		gl_panelObsData.setVerticalGroup(
				gl_panelObsData.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelObsData.createSequentialGroup()
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panelObsData.createSequentialGroup()
										.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panelObsData.createSequentialGroup()
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblDate)
																.addComponent(chkboxDST))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxLCT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblLCT)))
												.addGroup(gl_panelObsData.createSequentialGroup()
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxLat, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblLatitude))
														.addPreferredGap(ComponentPlacement.RELATED)
														.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
																.addComponent(txtboxLon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																.addComponent(lblLongitude))))
										.addGap(6))
								.addGroup(Alignment.TRAILING, gl_panelObsData.createSequentialGroup()
										.addGroup(gl_panelObsData.createParallelGroup(Alignment.LEADING)
												.addGroup(gl_panelObsData.createParallelGroup(Alignment.BASELINE)
														.addComponent(radbtnPST)
														.addComponent(lblTimeZone))
												.addComponent(radbtnMST)
												.addComponent(radbtnCST)
												.addComponent(radbtnEST)
												.addComponent(radbtnLon))
										.addGap(7))))
				);
		panelObsData.setLayout(gl_panelObsData);
		panelObsTitle.setLayout(new BorderLayout(0, 0));

		JLabel lblObs = new JLabel("Observer Location and Time");
		lblObs.setFont(ASTStyle.TEXT_BOLDFONT);
		lblObs.setHorizontalAlignment(SwingConstants.CENTER);
		panelObsTitle.add(lblObs);
		
		panelForObsData.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panelForObsData.add(panelObsData);

		panelBookTitle.setLayout(new BorderLayout(0, 0));		
		JLabel lblBookTitle = new JLabel(ASTBookInfo.BOOK_TITLE);
		lblBookTitle.setForeground(ASTStyle.BOOK_TITLE_COLOR);
		lblBookTitle.setVerifyInputWhenFocusTarget(false);
		lblBookTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblBookTitle.setFont(ASTStyle.BOOK_TITLE_BOLDFONT);
		lblBookTitle.setFocusable(false);
		lblBookTitle.setFocusTraversalKeysEnabled(false);
		panelBookTitle.add(lblBookTitle, BorderLayout.CENTER);
		contentPane.setLayout(gl_contentPane);
		textPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		// Define the actual text area and add it to scrollPaneOutput
		textPane.setFont(ASTStyle.OUT_TEXT_FONT);
		textPane.setFocusable(false);
		textPane.setFocusTraversalKeysEnabled(false);
		textPane.setFocusCycleRoot(false);
		textPane.setEditable(false);
		scrollPaneOutput.setViewportView(textPane);

		// Set parent frame for static methods that need to center a pane/frame/etc. on the GUI
		ASTQuery.setParentFrame(this);
		ASTMsg.setParentFrame(this);

		textPane.setText("     "); // must initialize with at least a few blanks
		prt = new ASTPrt(textPane);
		
		// Create a starChart instance and set a listener on the scroll pane
		// for when its size changes. Must do listener on the scroll pane rather
		// than the text pane because the text pane size changes if there are scroll bars.
		starChart = new ASTCharts(textPane);
		scrollPaneOutput.addComponentListener(new ScrollPaneListener());

		// Initialize the rest of the GUI
		ASTStyle.setASTStyle();
		aboutBox.pack();
		aboutBox.setParentFrame(this);
		dialogKepler.pack();
		dialogKepler.setParentFrame(this);

		chkboxDST.setSelected(false);
		chkboxShowInterimCalcs.setSelected(false);
		chkboxEQChart.setSelected(true);
		chkboxWhiteChart.setSelected(true);
		chkboxLabelObjs.setSelected(true);
		setLonRadBtn();

		txtboxLat.setText("");
		txtboxLon.setText("");
		txtboxDate.setText("");
		txtboxLCT.setText("");
		setFilename("");
		setCatalogType("");
		setResults("");
		setEpoch(ASTMisc.DEFAULT_EPOCH);
		setmVFilter(DEFAULT_mV_FILTER);
		chartOnScreen = false;

		// Load the default orbital elements and observer location (if it exists)
		orbElements = new ASTOrbits(prt);
		setEpoch(orbElements.getOEEpochDate());
		observer = new ASTObserver();
		setObsDataInGUI(observer);

		// Explicitly indicate the order in which we want the GUI components traversed
		Vector<Component> order = new Vector<Component>(4);
		order.add(txtboxLat); order.add(txtboxLon); order.add(txtboxDate); order.add(txtboxLCT);
		ASTStyle.GUIFocusTraversalPolicy GUIPolicy = new ASTStyle.GUIFocusTraversalPolicy(order);
		contentPane.setFocusTraversalPolicyProvider(true);
		contentPane.setFocusTraversalPolicy(GUIPolicy);
	}
	
	/**
	 * This class implements a listener to handle when the GUI is resized.
	 * We don't need to do anything for events other than resize.
	 */
	private class ScrollPaneListener implements ComponentListener{
		public void componentShown(ComponentEvent arg0) { }
		public void componentHidden(ComponentEvent arg0) { }
		public void componentMoved(ComponentEvent arg0) { }
		public void componentResized(ComponentEvent arg0) {
			if (chartOnScreen) {
				chartOnScreen = false;
				starChart.releaseChart();
			}
		}
	}
	
	/**
	 * This class implements a listener to handle state changes in 
	 * the checkboxes that affect the currently displayed star chart.
	 * If a state changes, we need to erase the current star chart,
	 * if there is one. 
	 */
	private class ChartStateChangeListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			starChart.releaseChart();
			setChartOnScreen(false);
		}
	}

	/*==============================================================================
	 * The methods below return references to various GUI components such as the
	 * scrollable output text area. 
	 *=============================================================================*/
	
	/**
	 * Determines the size of the drawing canvas for a star chart.
	 * 
	 * Note: The scroll pane area is used rather than the text area in
	 * determining dimensions because the text area size changes based
	 * upon whether there are scroll bars present. The scroll pane area
	 * changes only when the GUI itself is resized.
	 * 
	 * @return			drawing canvas dimensions
	 */
	protected static Dimension getCanvasSize() {
		Dimension d = new Dimension();
		Rectangle border;
		double width, height;
		prt.clearTextArea(); // get rid of the scroll bars that could be there from previous text output
		textPane.updateUI();
		textPane.validate();

		// Figure out size of text pane by using its parent's (scrollPaneOutput) viewport setting.
		// We must account for any borders placed around the viewport and the width of the scroll bars
		// in determining the proper size.
		border = scrollPaneOutput.getViewportBorderBounds();
		width = border.getWidth() - border.getX()*2.0 + scrollPaneOutput.getVerticalScrollBar().getWidth();
		height = border.getHeight() - border.getY()*2.0 + scrollPaneOutput.getHorizontalScrollBar().getHeight();
		d.setSize(width, height);
		return d;
	}
	
	/**
	 * Gets the Data Files menu.
	 * 
	 * @return		the Data Files menu
	 */
	protected static JMenu getDataFilesMenu() {
		return mnDataFiles;
	}

	/**
	 * Gets the listener for the menu items
	 * 
	 * @return		the action listener for handling the menus
	 */
	protected static ActionListener getMenuListener() {
		return listenerMenus;
	}
	
	/**
	 * Gets the Solar System Info menu.
	 * 
	 * @return		the Solar System Info menu
	 */
	protected static JMenu getSolarSysInfoMenu() {
		return mnSolarSysInfo;
	}

	/**
	 * Gets the Star Charts menu.
	 * 
	 * @return		the Star Charts menu
	 */
	protected static JMenu getStarChartsMenu() {
		return mnStarCharts;
	}

	/**
	 * Gets the ASTPrt instance for this application's scrollable text pane area.
	 * 
	 * @return		the ASTPrt instance for this application
	 */
	protected static ASTPrt getPrtInstance() {
		return prt;
	}
	
	/**
	 * Gets the current star chart instance.
	 * 
	 * @return				star chart object
	 */
	protected static ASTCharts getStarChart() {
		return starChart;
	}

	/**
	 * Gets the scrollable text pane area for this GUI.
	 * 
	 * @return		the JTextPane for this GUI
	 */
	protected static JTextPane getTextPane() {
		return textPane;
	}

	/*==============================================================================
	 * The methods below are 'getters' and 'setters' for various items in the GUI.
	 *=============================================================================*/

	/**
	 * Clears the text areas in the GUI and any currently
	 * displayed star chart.
	 */
	protected static void clearTextAreas() {
		chartOnScreen = false;
		prt.clearTextArea();	
		setResults("");
		starChart.releaseChart();
	}
	
	/**
	 * Return the status of whether there is a chart currently being displayed.
	 * 
	 * @return		whether a chart is currently being displayed
	 */
	protected static boolean getChartOnScreen() {
		return chartOnScreen;
	}

	/**
	 * Gets the current mV filter setting
	 * 
	 * @return			current mV filter setting
	 */
	protected static double getcurrentmVFilter() {
		return currentmVFilter;
	}
	
	/**
	 * Gets the current observer.
	 * 
	 * @return			current observer object
	 */
	protected static ASTObserver getcurrentObserver() {
		return observer;
	}
	
	/**
	 * Gets the currently loaded orbital elements
	 * 
	 * @return			current orbital elements db
	 */
	protected static ASTOrbits getcurrentOrbitalElements() {
		return orbElements;
	}
	
	/**
	 * Gets the current status of the DST check box
	 * 
	 * @return		true if the DST check box is checked, else false
	 */
	protected static boolean getDSTStatus() {
		return chkboxDST.isSelected();
	}
	
	/**
	 * Gets the current status of the EQChart check box
	 * 
	 * @return		true if the EQChart check box is checked, else false
	 */
	protected static boolean getEQChartStatus() {
		return chkboxEQChart.isSelected();
	}
	
	/**
	 * Gets the current status of the Label Objects check box
	 * 
	 * @return		true if the Label Objects check box is checked, else false
	 */
	protected static boolean getLabelObjs() {
		return chkboxLabelObjs.isSelected();
	}
	
	/**
	 * Gets the status of the Show Interim Calculations checkbox
	 * 
	 * @return			true if checkbox is selected else false
	 */
	protected static boolean getShowInterimCalcsStatus() {
		return chkboxShowInterimCalcs.isSelected();
	}
	
	/**
	 * Gets the current selection of the radio buttons for solving Kepler's equation
	 * 
	 * @return				ASTKepler.TrueAnomalyType
	 */
	protected static ASTKepler.TrueAnomalyType getTrueAnomalyRBStatus() {
		return dialogKepler.getKeplerRadBtnSetting();
	}
	
	/**
	 * Gets the current status of the White Chart check box
	 * 
	 * @return		true if the White Chart check box is checked, else false
	 */
	protected static boolean getWhiteChartStatus() {
		return chkboxWhiteChart.isSelected();
	}

	/**
	 * Sets the Catalog type in the GUI.
	 * 
	 * @param cat_type		String representing the catalog type
	 * 						which is read from the star catalog
	 * 						data file
	 */
	protected static void setCatalogType(String cat_type) {
		lblCatalogType.setText("Catalog Type: " + cat_type);
	}
	
	/**
	 * Sets a flag indicating whether a chart is currently displayed.
	 * 
	 * @param state				true if a chart is displayed, false if not
	 */
	protected static void setChartOnScreen(boolean state) {
		chartOnScreen = state;
	}

	/**
	 * Sets the Epoch label in the GUI.
	 * 
	 * @param epoch				Epoch to display
	 */
	protected static void setEpoch(double epoch) {
		lblEpoch.setText("Epoch: " + String.format(ASTStyle.epochFormat,epoch));
	}

	/**
	 * Sets the filename label to be displayed in the GUI.
	 * 
	 * @param fname			star catalog filename, including pathname
	 */
	protected static void setFilename(String fname) {
		lblFilename.setText("File: " + ASTStr.abbrevRight(fname,MAX_FNAME_DISPLAY));
	}

	/**
	 * Sets the mv Filter label to be displayed in the GUI.
	 * 
	 * @param mV			visual magnitude filter to display
	 */
	protected static void setmVFilter(double mV) {
		currentmVFilter = mV;
		lblmVFilter.setText("mV Filter: " + String.format(ASTStyle.mVFormat,mV));
	}

	/**
	 * Sets the results label in the GUI
	 * 
	 * @param result	string for the results label in the GUI
	 */
	protected static void setResults(String result) {
		lblResults.setText("Result: " + result);
	}
	
	/**
	 * Sets the status of the Show Interim Calculations checkbox
	 * 
	 * @param state				state to set for the checkbox
	 */
	protected static void setShowInterimCalcsStatus(boolean state) {
		chkboxShowInterimCalcs.setSelected(state);
	}

	/*-----------------------------------------------------------------------
	 * Handle the radio buttons. Make sure only 1 is selected at a time
	 -----------------------------------------------------------------------*/

	/**
	 * Clears all of the GUI radio buttons (i.e., sets them to unchecked). This
	 * routine is useful since Swing does not automatically manage radio buttons.
	 */
	private static void clearAllRadioBtns() {
		radbtnPST.setSelected(false);
		radbtnPST.setFocusPainted(false);
		radbtnPST.setFocusable(false);

		radbtnMST.setSelected(false);
		radbtnMST.setFocusPainted(false);
		radbtnMST.setFocusable(false);

		radbtnCST.setSelected(false);
		radbtnCST.setFocusPainted(false);
		radbtnCST.setFocusable(false);

		radbtnEST.setSelected(false);
		radbtnEST.setFocusPainted(false);
		radbtnEST.setFocusable(false);

		radbtnLon.setSelected(false);
		radbtnLon.setFocusPainted(false);
		radbtnLon.setFocusable(false);
	}

	/**
	 * Determine what time zone radio button is selected
	 * 
	 * @return		a time zone type for whatever is the
	 *				currently selected time zone radio button
	 */
	protected static ASTLatLon.TimeZoneType getSelectedRBStatus() {
		if (radbtnPST.isSelected()) return ASTLatLon.TimeZoneType.PST;
		else if (radbtnMST.isSelected()) return ASTLatLon.TimeZoneType.MST;
		else if (radbtnCST.isSelected()) return ASTLatLon.TimeZoneType.CST;
		else if (radbtnEST.isSelected()) return ASTLatLon.TimeZoneType.EST;
		else return ASTLatLon.TimeZoneType.LONGITUDE;
	}

	/**
	 * Gets the command that represents the PST radio button.
	 * 
	 * @return			PST radio button command
	 */
	protected static String getPSTCommand() {
		return radbtnPSTCommand;
	}

	/**
	 * Gets the command that represents the MST radio button.
	 * 
	 * @return			MST radio button command
	 */
	protected static String getMSTCommand() {
		return radbtnMSTCommand;
	}

	/**
	 * Gets the command that represents the CST radio button.
	 * 
	 * @return			CST radio button command
	 */
	protected static String getCSTCommand() {
		return radbtnCSTCommand;
	}

	/**
	 * Gets the command that represents the EST radio button.
	 * 
	 * @return			EST radio button command
	 */
	protected static String getESTCommand() {
		return radbtnESTCommand;
	}

	/**
	 * Gets the command that represents the Longitude radio button.
	 * 
	 * @return			Longitude radio button command
	 */
	protected static String getLongitudeCommand() {
		return radbtnLonCommand;
	}

	/**
	 * Sets the PST radio button to true
	 */
	protected static void setPSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnPST.setSelected(true);
	}

	/**
	 * Sets the MST radio button to true
	 */
	protected static void setMSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnMST.setSelected(true);
	}

	/**
	 * Sets the CST radio button to true
	 */
	protected static void setCSTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnCST.setSelected(true);
	}

	/**
	 * Sets the EST radio button to true
	 */
	protected static void setESTRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnEST.setSelected(true);
	}

	/**
	 * Sets the Longitude radio button to true
	 */
	protected static void setLonRadBtn() {
		// make sure only 1 radio button is set when we're done
		clearAllRadioBtns();				// make sure only 1 radio button is set when we're done
		radbtnLon.setSelected(true);
	}

	/*==============================================================================
	 * The methods below return the command associated with a specific menu or
	 * GUI button. This is done to separate the actual string in the GUI that
	 * represents an action (e.g., "Exit") from the semantics of the action that is
	 * to occur.
	 *=============================================================================*/

	/**
	 * Gets the command that represents the Solar Sys Info, Data Files, and Star Charts menu items.
	 * 
	 * @return			Solar Sys Info, Data Files, and Star Charts command
	 */
	protected static String getChapMenuCommand() {
		return 	chapMenuCommand;
	}

	/**
	 * Gets the command that represents the Exit menu item.
	 * 
	 * @return			Exit command
	 */
	protected static String getExitCommand() {
		return exitCommand;
	}

	/**
	 * Gets the command that represents the Instructions menu item.
	 * 
	 * @return			Instructions command
	 */
	protected static String getInstructionsCommand() {
		return instructionsCommand;
	}

	/**
	 * Gets the command that represents the About menu item.
	 * 
	 * @return			About command
	 */
	protected static String getAboutCommand() {
		return aboutCommand;
	}
	
	/*======================================================================
	 * The methods below conditionally print to the scrollable text area
	 *=====================================================================*/

	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.print(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 */
	protected static void printlnCond() {
		if (chkboxShowInterimCalcs.isSelected()) prt.println();
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 */
	protected static void printlnCond(String txt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt);
	}
	/**
	 * Routines to handle sending output text to the scrollable
	 * output area. These are wrappers around ASTUtils.println
	 * that see if the 'Show Intermediate Calculations' checkbox
	 * is checked before invoking the println functions.
	 * 
	 * This method is overloaded to allow various combinations
	 * of what is passed as parameters.
	 * 
	 * @param txt			string to be printed
	 * @param centerTxt		true if the text is to be centered
	 */
	protected static void printlnCond(String txt, boolean centerTxt) {
		if (chkboxShowInterimCalcs.isSelected()) prt.println(txt,centerTxt);
	}
	
	/*=======================================
	 * AboutBox and Kepler dialog methods
	 *======================================*/

	/**
	 * Shows the About Box
	 */
	protected static void showAboutBox() {
		aboutBox.showAboutBox();
	}
	
	/**
	 * Shows the  dialog for selecting a method for solving Kepler's equation. 
	 */
	protected static void showKeplerDialog() {
		dialogKepler.showDialog();
	}
	
	/*=======================================================
	 * Some helper methods that are GUI or orbits related
	 *======================================================*/
	
	/**
	 * Checks to see if an orbital elements database has been
	 * successfully loaded, and display an error message if not.
	 * 
	 * This method is overloaded.
	 * 
	 * @return					true if orbital elements have been loaded, else false
	 */
	protected static boolean checkOEDBLoaded() {
		return checkOEDBLoaded(ASTMisc.SHOW_ERRORS);
	}
	/**
	 * Checks to see if an orbital elements database has been
	 * successfully loaded, and display an error message if not.
	 * 
	 * This method is overloaded.
	 * 
	 * @param showErrors		display error message if true
	 * @return					true if orbital elements have been loaded, else false
	 */
	protected static boolean checkOEDBLoaded(boolean showErrors) {
		// Logic below checks 1st to be sure an orbits instance was created, then
		// that orbital elements were loaded.
		boolean errorOccurred = (orbElements == null);

		if (!errorOccurred) errorOccurred = (!orbElements.isOrbElementsDBLoaded());
		
		if (errorOccurred) {
			if (showErrors) ASTMsg.errMsg("No Orbital Elements data is currently loaded.", "No Orbital Elements Data Loaded");
          return false;
		}

		return true;
  }
	
	/**
	 * See if the observer location, date, and time
	 * currently in the GUI is valid.
	 * 
	 * @return				true if valid, otherwise false
	 */
	protected static boolean validateGUIObsLoc() {
		ASTLatLon.TimeZoneType tZone = getSelectedRBStatus();
		String tzStr;
		
		if (tZone == ASTLatLon.TimeZoneType.LONGITUDE) tzStr = "LON";
		else tzStr = tZone.toString();
		return ASTObserver.isValidObsLoc(observer, txtboxLat.getText(), txtboxLon.getText(), tzStr, txtboxDate.getText(), txtboxLCT.getText());
	}

	/*-----------------------------------------------------------------------------
	 * Define some useful helper methods used only in this class
	 *----------------------------------------------------------------------------*/

	/**
	 * Sets the observer location in the GUI. This is intended to be
	 * done one time only during the program initialization since a
	 * default location may be read from a data file.
	 * 
	 * @param obs			observer location object
	 */
	private void setObsDataInGUI(ASTObserver obs) {
		txtboxLat.setText(ASTLatLon.latToStr(obs.getObsLocation(), ASTMisc.DMSFORMAT));
		txtboxLon.setText(ASTLatLon.lonToStr(obs.getObsLocation(), ASTMisc.DMSFORMAT));
		txtboxDate.setText(ASTDate.dateToStr(obs.getObsDate()));
		txtboxLCT.setText(ASTTime.timeToStr(obs.getObsTime(), ASTMisc.HMSFORMAT));

		if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.PST) setPSTRadBtn();
		else if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.MST) setMSTRadBtn();
		else if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.CST) setCSTRadBtn();
		else if (obs.getObsTimeZone() == ASTLatLon.TimeZoneType.EST) setESTRadBtn();
		else setLonRadBtn();
	}	
}
