package jll.celcalc.chap8;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Solar Sys Info menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class SolarSysInfoActions {

	/**
	 * Calculate an object's distance and angular diameter.
	 */
	protected void calcDistAndAngDiameter() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		ASTPrt prt = ChapGUI.getPrtInstance();
		String objName;
		int idx;
		double R_p, R_e, L_e, L_p, dDist, dDist_km, Theta;
		double[] eclCoord;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(!Misc.GETSUN,!Misc.GETEARTH,!Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Distance from Earth and the Angular Diameter for " + objName +
				" for the Observer's Date", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		// Technically, we should be using the UT for the observer rather than UT=0, but
		// the difference is so small that it isn't worth converting LCT to UT
		eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, Misc.termCriteria);
		L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT];
		R_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT];
		L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT];
		R_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT];
		ChapGUI.printlnCond("1.  Calculate the radius vector length and heliocentric ecliptic longitude");
		ChapGUI.printlnCond("    for the Earth and " + objName + " at UT = 0.0 hours");
		ChapGUI.printlnCond("    Lp = " + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " degrees, Rp = " + 
				String.format(ASTStyle.genFloatFormat,R_p) + " AUs");
		ChapGUI.printlnCond("    Le = " + ASTAngle.angleToStr(L_e,ASTMisc.DECFORMAT) + " degrees, Re = " + 
				String.format(ASTStyle.genFloatFormat,R_e) + " AUs");
		ChapGUI.printlnCond();

		dDist = ASTOrbits.calcObjDistToEarth(R_e, L_e, R_p, L_p);
		ChapGUI.printlnCond("2.  Calculate the distance from " + objName + " to the Earth");
		ChapGUI.printlnCond("    Dist = sqrt[Re^2 + Rp^2 - 2*Re*Rp*cos(Lp - Le)]");
		ChapGUI.printlnCond("         = sqrt[" + String.format(ASTStyle.genFloatFormat,R_e) + "^2 + " + 
				String.format(ASTStyle.genFloatFormat,R_p) + "^2 - 2*" +
				String.format(ASTStyle.genFloatFormat,R_e) + "*" + String.format(ASTStyle.genFloatFormat,R_p) +
				"*cos(" + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " - " + 
				ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat,dDist) + " AUs");
		ChapGUI.printlnCond();

		Theta = orbElements.getOEObjAngDiamArcSec(idx) / dDist;
		ChapGUI.printlnCond("3.  Compute the angular diameter for " + objName + ".");
		ChapGUI.printlnCond("    Theta = Theta_p/Dist = " + 
				ASTAngle.angleToStr(orbElements.getOEObjAngDiamArcSec(idx), ASTMisc.DECFORMAT) +
				"/" +String.format(ASTStyle.genFloatFormat,dDist) + " = " + 
				ASTAngle.angleToStr(Theta, ASTMisc.DECFORMAT) + " arcseconds");
		ChapGUI.printlnCond("    (Note: distance must be in AUs and Theta_p must be in arcseconds)");
		ChapGUI.printlnCond();

		dDist_km = ASTMath.Round(ASTMath.AU2KM(dDist), 0);
		Theta = Theta / 3600.0; 	// Convert from arcseconds to degrees
		ChapGUI.printlnCond("4.  Convert the distance to km or miles, and angular diameter to DMS format.");
		ChapGUI.printlnCond("    Dist = " + ASTStr.insertCommas(dDist_km) + " km = " + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist_km), 0)) + " miles");
		ChapGUI.printlnCond("    Theta = " + ASTAngle.angleToStr(Theta, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("On " + ASTDate.dateToStr(observer.getObsDate()) + ", " + objName + 
				" is/was " + ASTStr.insertCommas(dDist_km) + " km away");
		ChapGUI.printlnCond("and its angular diameter is/was " + ASTAngle.angleToStr(Theta, ASTMisc.DMSFORMAT));

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Distance to " + objName + ": " + String.format(ASTStyle.genFloatFormat,dDist) +
				" AUs (" + ASTStr.insertCommas(dDist_km) + " km, " + 
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist_km), 0)) +
				" miles), Angular Diameter: " + ASTAngle.angleToStr(Theta, ASTMisc.DMSFORMAT));
	}

	/**
	 * Calculate how much of an object is illuminated.
	 */
	protected void calcObjIllumination() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName;
		int idx;
		double[] eclCoord;
		double R_p, R_e, L_e, L_p, dDist, dKpcnt;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(!Misc.GETSUN,!Misc.GETEARTH,!Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute how much " + objName + " is illuminated as seen from Earth for the Observer's Date", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		// Technically, we should be using the UT for the observer rather than UT=0, but
		// the difference is so small that it isn't worth converting LCT to UT
		eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, Misc.termCriteria);
		L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT];
		R_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT];
		L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT];
		R_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT];
		ChapGUI.printlnCond("1.  Calculate the radius vector length for the Earth and " + objName + " at UT = 0.0 hours");
		ChapGUI.printlnCond("    Rp = " + String.format(ASTStyle.genFloatFormat,R_p) + " AUs, Re = " + 
				String.format(ASTStyle.genFloatFormat,R_e) + " AUs");
		ChapGUI.printlnCond();

		dDist = ASTOrbits.calcObjDistToEarth(R_e, L_e, R_p, L_p);
		ChapGUI.printlnCond("2.  Calculate the distance from " + objName + " to the Earth");
		ChapGUI.printlnCond("    Dist = " + String.format(ASTStyle.genFloatFormat,dDist) + " AUs");
		ChapGUI.printlnCond();

		dKpcnt = (Math.pow((R_p + dDist),2) - R_e*R_e) / (4 * R_p * dDist);
		dKpcnt = 100 * dKpcnt;;
		ChapGUI.printlnCond("3.  Calculate the percent illumination.");
		ChapGUI.printlnCond("    K% = 100*[((Rp + Dist)^2 - Re^2)/(4*Rp*Dist)]");
		ChapGUI.printlnCond("       = 100*[((" + String.format(ASTStyle.genFloatFormat,R_p) + " + " + 
				String.format(ASTStyle.genFloatFormat,dDist) + ")^2 - " + 
				String.format(ASTStyle.genFloatFormat,R_e) + "^2)/(4*" + 
				String.format(ASTStyle.genFloatFormat,R_p) + "*" +
				String.format(ASTStyle.genFloatFormat,dDist) + ")]");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dKpcnt));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(), 
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
				", " + objName + " is/was " + ASTMath.Round(dKpcnt, 1) + "% illuminated as seen from Earth");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("On " + ASTDate.dateToStr(observer.getObsDate().getMonth(), 
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
				", " + objName + " is/was " + ASTMath.Round(dKpcnt, 1) + "% illuminated.");
	}

	/**
	 * Calculate an object's (apparent) visual magnitude.
	 */
	protected void calcObjMagnitude() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName;
		int idx;
		double[] eclCoord;
		double R_p, R_e, L_e, L_p, Lon_p, Vp, mV, dDist, dPA;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(!Misc.GETSUN,!Misc.GETEARTH,!Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the visual magnitude for " + objName, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		// Technically, we should be using the UT for the observer rather than UT=0, but
		// the difference is so small that it isn't worth converting LCT to UT
		eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, Misc.termCriteria);
		L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT];
		R_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT];
		L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT];
		R_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT];
		Lon_p = eclCoord[ASTOrbits.OBJ_ECLLON];
		ChapGUI.printlnCond("1.  Calculate the object's position at UT = 0.0 hours to get its heliocentric (Lp)");
		ChapGUI.printlnCond("    and geocentric longitude (Lon_p), and its radius vector length (Rp).");
		ChapGUI.printlnCond("    Lp = " + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Lon_p = " + ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Rp = " + String.format(ASTStyle.genFloatFormat,R_p) + " AUs");
		ChapGUI.printlnCond();

		dDist = ASTOrbits.calcObjDistToEarth(R_e, L_e, R_p, L_p);
		ChapGUI.printlnCond("2.  Calculate the distance from " + objName + " to the Earth");
		ChapGUI.printlnCond("    Dist = " + String.format(ASTStyle.genFloatFormat,dDist) + " AUs");
		ChapGUI.printlnCond();

		dPA = (1 + ASTMath.COS_D(Lon_p - L_p)) / 2.0;
		ChapGUI.printlnCond("3.  Calculate the object's phase angle.");
		ChapGUI.printlnCond("    PA = [1 + cos(Lon_p - Lp)]/2 = [1 + cos(" + ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + ")]/2]");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(dPA, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    (The elongation is Lon_p - Lp = " + ASTAngle.angleToStr(Lon_p - L_p, ASTMisc.DECFORMAT) + " degrees)");
		ChapGUI.printlnCond();

		Vp = orbElements.getOEObjmV(idx);
		mV = Vp + 5 * Math.log10((R_p * dDist) / Math.sqrt(dPA));
		ChapGUI.printlnCond("4.  Calculate the object's visual magnitude.");
		ChapGUI.printlnCond("    mV = Vp + 5*Log10[(Rp*Dist)/sqrt(PA)]");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,Vp) + " + 5*Log10[(" + 
				String.format(ASTStyle.genFloatFormat,R_p) + "*" + 
				String.format(ASTStyle.genFloatFormat,dDist) + ")/sqrt(" + 
				ASTAngle.angleToStr(dPA, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.mVFormat,mV));
		ChapGUI.printlnCond();

		ChapGUI.printCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(), 
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) + 
				", the visual magnitude for ");
		ChapGUI.printlnCond(objName + " is/was " + String.format(ASTStyle.mVFormat,mV));

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("On " + ASTDate.dateToStr(observer.getObsDate().getMonth(), 
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
				", for " + objName + " mV = " + String.format(ASTStyle.mVFormat,mV) + " at 1 AU");
	}

	/**
	 * Calculate miscellaneous data items about an object.
	 */
	protected void calcObjMiscData() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName;
		String result = "";
		int idx;
		boolean saveCBStatus;

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(Misc.GETSUN,Misc.GETEARTH,Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute miscellaneous data items for " + objName, ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		// Show how calculations were done, but only if needed
		if (ChapGUI.getShowInterimCalcsStatus()) Misc.calcMiscData(idx, false);

		// Now do the summary, but temporarily set checkbox to false
		// so that we don't do detailed display again
		saveCBStatus = ChapGUI.getShowInterimCalcsStatus();
		ChapGUI.setShowInterimCalcsStatus(false);
		prt.setProportionalFont();
		Misc.calcMiscData(idx, true);
		ChapGUI.setShowInterimCalcsStatus(saveCBStatus);

		prt.setProportionalFont();
		prt.resetCursor();

		result = "Misc data items for ";
		if ((idx == orbElements.getOEDBMoonIndex()) || (idx == orbElements.getOEDBSunIndex())) result = result + "the ";
		ChapGUI.setResults(result + objName + " are given in the text area below");
	}

	/**
	 * Calculate the distance an object will be from the Sun when
	 * it passes through perhelion/aphelion.
	 */
	protected void calcObjPeriAphDist() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName;
		int idx;
		double a_p, e_p, Distper, Distaph, Distper_km, Distaph_km;

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(!Misc.GETSUN,Misc.GETEARTH,!Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Determine distance from the Sun when " + objName + " passes through Perihelion/Aphelion", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		a_p = orbElements.getOEObjSemiMajAxisAU(idx);
		e_p = orbElements.getOEObjEccentricity(idx);

		Distper = a_p * (1 - e_p);
		ChapGUI.printlnCond("1.  Compute the object's distance at perihelion.");
		ChapGUI.printlnCond("    Dist_per = a_p * (1 - eccentricity_p) = " + a_p + " * (1 - " + e_p + ")");
		ChapGUI.printlnCond("             = " + String.format(ASTStyle.genFloatFormat,Distper) + " AUs");
		ChapGUI.printlnCond();

		Distaph = a_p * (1 + e_p);
		ChapGUI.printlnCond("2.  Compute the object's distance at aphelion.");
		ChapGUI.printlnCond("    Dist_aph = a_p * (1 + eccentricity_p) = " + a_p + " * (1 + " + e_p + ")");
		ChapGUI.printlnCond("             = " + String.format(ASTStyle.genFloatFormat,Distaph) + " AUs");
		ChapGUI.printlnCond();

		Distper_km = ASTMath.Round(ASTMath.AU2KM(Distper), 0);
		Distaph_km = ASTMath.Round(ASTMath.AU2KM(Distaph), 0);
		ChapGUI.printlnCond("3.  Convert the distances to km or miles, if desired.");
		ChapGUI.printlnCond("    Dist_per = " + ASTStr.insertCommas(Distper_km) + " km = " +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(Distper_km), 0)) + " miles");
		ChapGUI.printlnCond("    Dist_aph = " + ASTStr.insertCommas(Distaph_km) + " km = " +
				ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(Distaph_km), 0)) + " miles");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond(objName + " is " + String.format(ASTStyle.genFloatFormat,Distper) + " AUs (" +
				ASTStr.insertCommas(Distper_km) + " km) from the Sun at perihelion");
		ChapGUI.printlnCond("and " + String.format(ASTStyle.genFloatFormat,Distaph) + " AUs (" +
				ASTStr.insertCommas(Distaph_km) + " km) from the Sun at aphelion");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(objName + " Perihelion Dist: " + String.format(ASTStyle.genFloatFormat,Distper) +
				" AUs (" + ASTStr.insertCommas(Distper_km) + " km), Aphelion Dist: " + 
				String.format(ASTStyle.genFloatFormat,Distaph) + " AUs (" + ASTStr.insertCommas(Distaph_km) + " km)");
	}

	/**
	 * Calculate the position of an object, using the currently
	 * loaded orbital elements and the current observer position.
	 * The object can be anything in the orbital elements file
	 * except the Sun, Moon, or Earth.
	 */
	protected void calcObjPosition() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTPrt prtObj = null;
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		double[] tmpKepler;
		String objName;
		int idx, iter;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		String result;
		ASTCoord horizonCoord, eqCoord;
		double LCT, UT, LST, GST, JD, JDe, De;
		double Ecc_p, inclin_p, M_p, v_p, Ec_p, Ea_p, L_p, H_p, R_p, Lp_p, Lon_p, Lat_p;
		double Ecc_e, inclin_e, M_e, v_e, Ec_e, Ea_e, L_e, H_e, R_e;
		double x, y, dT;
		int dateAdjust;
		ASTInt dateAdjustObj = new ASTInt();
		int idxEarth = orbElements.getOEDBEarthIndex();

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(!Misc.GETSUN,!Misc.GETEARTH,!Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		// Set up whether to display interim results when solving Kepler's equation
		if (ChapGUI.getShowInterimCalcsStatus()) prtObj = prt;

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Position of " + objName + " for the Current Observer", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		Ecc_p = orbElements.getOEObjEccentricity(idx);
		inclin_p = orbElements.getOEObjInclination(idx);
		Ecc_e = orbElements.getOEObjEccentricity(idxEarth);
		inclin_e = orbElements.getOEObjInclination(idxEarth);

		// Do all the time-related calculations
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST,observer.getObsLon());

		ChapGUI.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.");
		ChapGUI.printlnCond("    LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(observer.getObsDate()) + ")");
		ChapGUI.printCond("     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0) ChapGUI.printCond("previous day ");
		else if (dateAdjust > 0) ChapGUI.printCond("next day ");
		ChapGUI.printlnCond(ASTDate.dateToStr(adjustedDate) + ")");
		ChapGUI.printlnCond("    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		JDe = orbElements.getOEEpochJD();
		ChapGUI.printlnCond("2.  Compute the Julian day number for the standard epoch.");
		ChapGUI.printlnCond("    Epoch: " + String.format(ASTStyle.epochFormat,orbElements.getOEEpochDate()));
		ChapGUI.printlnCond("    JDe = " + String.format(ASTStyle.JDFormat,JDe));
		ChapGUI.printlnCond();

		JD = ASTDate.dateToJD(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear());
		ChapGUI.printlnCond("3.  Compute the Julian day number for the desired date, being sure to use the");
		ChapGUI.printlnCond("    Greenwich date and UT from step 1, and including the fractional part of the day.");
		ChapGUI.printlnCond("    JD = " + String.format(ASTStyle.JDFormat,JD));
		ChapGUI.printlnCond();

		De = JD - JDe;
		ChapGUI.printlnCond("4.  Compute the total number of elapsed days, including fractional days,");
		ChapGUI.printlnCond("    since the standard epoch.");
		ChapGUI.printlnCond("    De = JD - JDe = " + String.format(ASTStyle.JDFormat,JD) + 
				" - " + String.format(ASTStyle.JDFormat,JDe));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,De) + " days");
		ChapGUI.printlnCond();

		// Calculate the object's mean and true anomalies
		M_p = (360 * De) / (365.242191 * orbElements.getOEObjPeriod(idx)) + orbElements.getOEObjLonAtEpoch(idx) -
				orbElements.getOEObjLonAtPeri(idx);
		ChapGUI.printlnCond("5.  Compute the mean anomaly for " + objName);
		ChapGUI.printlnCond("    Mp = (360*De)/(365.242191*Tp) + Ep - Wp");
		ChapGUI.printlnCond("       = (360*" + String.format(ASTStyle.genFloatFormat,De) + ")/(365.242191*" +
				String.format(ASTStyle.genFloatFormat,orbElements.getOEObjPeriod(idx)) + " + " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAtEpoch(idx), ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAtPeri(idx), ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(M_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  If necessary, adjust Mp so that it falls in the range [0,360]");
		ChapGUI.printCond("    Mp = Mp MOD 360 = " + ASTAngle.angleToStr(M_p, ASTMisc.DECFORMAT) + " MOD 360 = ");
		M_p = ASTMath.xMOD(M_p, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(M_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		// Find the true anomaly from equation of center or Kepler's equation
		if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER) {
			Ec_p = (360.0 / Math.PI) * Ecc_p * ASTMath.SIN_D(M_p);
			ChapGUI.printlnCond("7.  Solve the equation of the center for " + objName + ".");
			ChapGUI.printlnCond("    Ep = (360/pi) * eccentricity_p * sin(Mp)");
			ChapGUI.printlnCond("       = (360/3.1415927) * " +
					Ecc_p + " * sin(" + ASTAngle.angleToStr(M_p, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Ec_p, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			v_p = M_p + Ec_p;
			ChapGUI.printlnCond("8.  Add Ep to Mp to get the true anomaly.");
			ChapGUI.printlnCond("    vp = Mp + Ep = " + ASTAngle.angleToStr(M_p, ASTMisc.DECFORMAT) + " + " + 
					ASTAngle.angleToStr(Ec_p, ASTMisc.DECFORMAT));
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(v_p, ASTMisc.DECFORMAT) + " degrees");
		} else {
			ChapGUI.printlnCond("7.  Solve Kepler's equation to get the eccentric anomaly.");
			if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
				tmpKepler = ASTKepler.calcSimpleKepler(prtObj, M_p, Ecc_p, Misc.termCriteria);
				Ea_p = tmpKepler[0];
				iter = (int) tmpKepler[1];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(prtObj, M_p, Ecc_p, Misc.termCriteria);
				Ea_p = tmpKepler[0];
				iter = (int) tmpKepler[1];
			}
			ChapGUI.printlnCond("    Eap = E" + iter + " = " + ASTAngle.angleToStr(Ea_p, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			v_p = (1 + Ecc_p) / (1 - Ecc_p);
			v_p = Math.sqrt(v_p) * ASTMath.TAN_D(Ea_p / 2.0);
			v_p = 2.0 * ASTMath.INVTAN_D(v_p);
			ChapGUI.printlnCond("8.  vp = 2 * inv tan[ sqrt[(1+eccentricity_p)/(1-eccentricity_p)] * tan(Eap/2) ]");
			ChapGUI.printlnCond("       = 2 * inv tan[ sqrt[(1+" + Ecc_p + ")/(1-" + Ecc_p + ")] * tan(" +
					ASTAngle.angleToStr(Ea_p, ASTMisc.DECFORMAT) + "/2) ]");
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(v_p, ASTMisc.DECFORMAT) + " degrees");
		}
		ChapGUI.printlnCond();

		// Calculate the object's heliocentric ecliptic coordinates (L_p, H_p)
		// and radius vector length (R_p)
		L_p = v_p + orbElements.getOEObjLonAtPeri(idx);
		ChapGUI.printlnCond("9.  Calculate the object's heliocentric longitude.");
		ChapGUI.printlnCond("    Lp = vp + Wp = " + ASTAngle.angleToStr(v_p, ASTMisc.DECFORMAT) + " + " + orbElements.getOEObjLonAtPeri(idx));
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("10. If necessary, adjust Lp to be in the range [0,360]");
		ChapGUI.printCond("    Lp = Lp MOD 360 = " + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " MOD 360 = ");
		L_p = ASTMath.xMOD(L_p, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		H_p = ASTMath.SIN_D(L_p - orbElements.getOEObjLonAscNode(idx)) * ASTMath.SIN_D(inclin_p);
		H_p = ASTMath.INVSIN_D(H_p);
		ChapGUI.printlnCond("11. Compute the object's heliocentric latitude.");
		ChapGUI.printlnCond("    Hp = inv sin[sin(Lp-Omega_p)*sin(inclination_p)]");
		ChapGUI.printlnCond("       = inv sin[sin(" + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAscNode(idx), ASTMisc.DECFORMAT) + ")*sin(" +
				ASTAngle.angleToStr(inclin_p, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("12. If necessary, adjust Hp to be in the range [0,360]");
		ChapGUI.printCond("    Hp = Hp MOD 360 = " + ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + " MOD 360 = ");
		H_p = ASTMath.xMOD(H_p, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		R_p = orbElements.getOEObjSemiMajAxisAU(idx) * (1 - Ecc_p * Ecc_p);
		R_p = R_p / (1 + Ecc_p * ASTMath.COS_D(v_p));
		ChapGUI.printlnCond("13. Compute the object's radius vector length");
		ChapGUI.printlnCond("    Rp = [a_p*(1-eccentricity_p^2)]/[1+eccentricity_p*cos(vp)]");
		ChapGUI.printlnCond("       = [" + orbElements.getOEObjSemiMajAxisAU(idx) + "*(1-" + 
				String.format(ASTStyle.genFloatFormat,Ecc_p) + "^2)]/[1+" +
				String.format(ASTStyle.genFloatFormat,Ecc_p) + "*cos(" + ASTAngle.angleToStr(v_p, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,R_p) + " AUs");
		ChapGUI.printlnCond();

		// Repeat the steps just done for the object for the Earth

		// Calculate the Earth's mean and true anomalies
		M_e = (360 * De) / (365.242191 * orbElements.getOEObjPeriod(idxEarth)) + orbElements.getOEObjLonAtEpoch(idxEarth) -
				orbElements.getOEObjLonAtPeri(idxEarth);
		ChapGUI.printlnCond("14. Compute the mean anomaly for the Earth");
		ChapGUI.printlnCond("    Me = (360*De)/(365.242191*Te) + Ee - We");
		ChapGUI.printlnCond("       = (360*" + String.format(ASTStyle.genFloatFormat,De) + ")/(365.242191*" +
				String.format(ASTStyle.genFloatFormat,orbElements.getOEObjPeriod(idxEarth)) + " + " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAtEpoch(idxEarth), ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAtPeri(idxEarth), ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(M_e, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("15. If necessary, adjust Me so that it falls in the range [0,360]");
		ChapGUI.printCond("    Me = Me MOD 360 = " + ASTAngle.angleToStr(M_e, ASTMisc.DECFORMAT) + " MOD 360 = ");
		M_e = ASTMath.xMOD(M_e, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(M_e, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		// Find the Earth's true anomaly from equation of center or Kepler's equation
		if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER) {
			Ec_e = (360.0 / Math.PI) * Ecc_e * ASTMath.SIN_D(M_e);
			ChapGUI.printlnCond("16. Solve the equation of the center for the Earth.");
			ChapGUI.printlnCond("    Ee = (360/pi) * eccentricity_e * sin(Me) = (360/3.1415927) * " +
					Ecc_e + " * sin(" + ASTAngle.angleToStr(M_e, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Ec_e, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			v_e = M_e + Ec_e;
			ChapGUI.printlnCond("17. Add Ee to Me to get the Earth's true anomaly.");
			ChapGUI.printlnCond("    ve = Me + Ee = " + ASTAngle.angleToStr(M_e, ASTMisc.DECFORMAT) + " + " + 
					ASTAngle.angleToStr(Ec_e, ASTMisc.DECFORMAT));
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(v_e, ASTMisc.DECFORMAT) + " degrees");
		} else {
			ChapGUI.printlnCond("16. Solve Kepler's equation to get the Earth's eccentric anomaly.");
			if (solveTrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER) {
				tmpKepler = ASTKepler.calcSimpleKepler(prtObj, M_e, Ecc_e, Misc.termCriteria);
				Ea_e = tmpKepler[0];
				iter = (int) tmpKepler[1];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(prtObj, M_e, Ecc_e, Misc.termCriteria);
				Ea_e = tmpKepler[0];
				iter = (int) tmpKepler[1];
			}
			ChapGUI.printlnCond("    Eae = E" + iter + " = " + ASTAngle.angleToStr(Ea_e, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			v_e = (1 + Ecc_e) / (1 - Ecc_e);
			v_e = Math.sqrt(v_e) * ASTMath.TAN_D(Ea_e / 2.0);
			v_e = 2.0 * ASTMath.INVTAN_D(v_e);
			ChapGUI.printlnCond("17. ve = 2 * inv tan[ sqrt[(1+eccentricity_e)/(1-eccentricity_e)] * tan(Eae/2) ]");
			ChapGUI.printlnCond("       = 2 * inv tan[ sqrt[(1+" + Ecc_e + ")/(1-" + Ecc_e + ")] * tan(" +
					ASTAngle.angleToStr(Ea_e, ASTMisc.DECFORMAT) + "/2) ]");
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(v_e, ASTMisc.DECFORMAT) + " degrees");
		}
		ChapGUI.printlnCond();

		// Calculate the Earth's heliocentric ecliptic coordinates (L_e, H_e)
		// and radius vector length (R_e)
		L_e = v_e + orbElements.getOEObjLonAtPeri(idxEarth);
		ChapGUI.printlnCond("18. Calculate the Earth's heliocentric longitude.");
		ChapGUI.printlnCond("    Le = ve + We = " + ASTAngle.angleToStr(v_e, ASTMisc.DECFORMAT) + " + " + orbElements.getOEObjLonAtPeri(idxEarth));
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("19. If necessary, adjust Le to be in the range [0,360]");
		ChapGUI.printCond("    Le = Le MOD 360 = " + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " MOD 360 = ");
		L_e = ASTMath.xMOD(L_e, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		H_e = ASTMath.SIN_D(L_e - orbElements.getOEObjLonAscNode(idxEarth)) * ASTMath.SIN_D(inclin_e);
		H_e = ASTMath.INVSIN_D(H_e);
		ChapGUI.printlnCond("20. Compute the Earth's heliocentric latitude.");
		ChapGUI.printlnCond("    He = inv sin[sin(Le-Omega_e)*sin(inclination_e)]");
		ChapGUI.printlnCond("       = inv sin[sin(" + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAscNode(idxEarth), ASTMisc.DECFORMAT) + ")*sin(" +
				ASTAngle.angleToStr(inclin_e, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(H_e, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("21. If necessary, adjust He to be in the range [0,360]");
		ChapGUI.printCond("    He = He MOD 360 = " + ASTAngle.angleToStr(H_e, ASTMisc.DECFORMAT) + " MOD 360 = ");
		H_e = ASTMath.xMOD(H_e, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(H_e, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		R_e = orbElements.getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e);
		R_e = R_e / (1 + Ecc_e * ASTMath.COS_D(v_e));
		ChapGUI.printlnCond("22. Compute the Earth's radius vector length");
		ChapGUI.printlnCond("    Re = [a_e*(1-eccentricity_e^2)]/[1+eccentricity_e*cos(ve)]");
		ChapGUI.printlnCond("       = [" + orbElements.getOEObjSemiMajAxisAU(idxEarth) + "*(1-" + 
				String.format(ASTStyle.genFloatFormat,Ecc_e) + "^2)]/[1+" +
				String.format(ASTStyle.genFloatFormat,Ecc_e) + "*cos(" + ASTAngle.angleToStr(v_e, ASTMisc.DECFORMAT) + ")]");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,R_e) + " AUs");
		ChapGUI.printlnCond();

		// Given the heliocentric location and radius vector length for both the Earth
		// and the object, project the object's location onto the ecliptic plane
		// with respect to the Earth

		y = ASTMath.SIN_D(L_p - orbElements.getOEObjLonAscNode(idx)) * ASTMath.COS_D(inclin_p);
		x = ASTMath.COS_D(L_p - orbElements.getOEObjLonAscNode(idx));
		ChapGUI.printlnCond("23. Calculate an adjustment to the object's ecliptic longitude.");
		ChapGUI.printlnCond("    y = sin(Lp - Omega_p)*cos(inclination_p)");
		ChapGUI.printlnCond("      = sin(" + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAscNode(idx), ASTMisc.DECFORMAT) + ")*cos(" + 
				ASTAngle.angleToStr(inclin_p, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,y));
		ChapGUI.printlnCond("    x = cos(Lp - Omega_p) = cos(" + ASTAngle.angleToStr(L_p, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(orbElements.getOEObjLonAscNode(idx), ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,x));
		ChapGUI.printlnCond();

		dT = ASTMath.INVTAN_D(y / x);
		ChapGUI.printlnCond("24. Compute T = inv tan(y/x) = inv tan(" + String.format(ASTStyle.genFloatFormat,y) + 
				"/" + String.format(ASTStyle.genFloatFormat,x) + ") = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		x = ASTMath.quadAdjust(y, x);
		ChapGUI.printlnCond("25. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.");
		ChapGUI.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + 
				" + " + ASTAngle.angleToStr(x, ASTMisc.DECFORMAT) + " = ");
		dT = dT + x;
		ChapGUI.printlnCond(ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Lp_p = orbElements.getOEObjLonAscNode(idx) + dT;
		ChapGUI.printlnCond("26. Finish computing the object's adjustment for the ecliptic longitude.");
		ChapGUI.printlnCond("    Lp_prime = Omega_p + T = " + ASTAngle.angleToStr(orbElements.getOEObjLonAscNode(idx), ASTMisc.DECFORMAT) + " + " +
				ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " = " + ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("27. If necessary, adjust Lp_prime to be in the range [0,360]");
		ChapGUI.printCond("    Lp_prime = Lp_prime MOD 360 = " + ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + " MOD 360 = ");
		Lp_p = ASTMath.xMOD(Lp_p, 360.0);
		ChapGUI.printlnCond(ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		// See if this is an inferior or superior object and compute accordingly
		if (orbElements.getOEObjInferior(idx)) {
			y = R_p * ASTMath.COS_D(H_p) * ASTMath.SIN_D(L_e - Lp_p);
			x = R_e - R_p * ASTMath.COS_D(H_p) * ASTMath.COS_D(L_e - Lp_p);
			ChapGUI.printlnCond("28. This is an inferior object, so compute its geocentric ecliptic longitude.");
			ChapGUI.printlnCond("    y = Rp*cos(Hp)*sin(Le - Lp_prime)");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,R_p) + "*cos(" +
					ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + ")*sin(" + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " - " +
					ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,y));
			ChapGUI.printlnCond("    x = Re - Rp*cos(Hp)*cos(Le - Lp_prime)");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,R_e) + " - " +
					String.format(ASTStyle.genFloatFormat,R_p) + "*cos(" + ASTAngle.angleToStr(H_p,ASTMisc.DECFORMAT) +
					")*cos(" + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " - " + 
					ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,x));
			ChapGUI.printlnCond();

			dT = ASTMath.INVTAN_D(y / x);
			ChapGUI.printlnCond("29. Compute T = inv tan(y/x) = inv tan(" + String.format(ASTStyle.genFloatFormat,y) + 
					"/" + String.format(ASTStyle.genFloatFormat,x) +
					") = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			x = ASTMath.quadAdjust(y, x);
			ChapGUI.printlnCond("30. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.");
			ChapGUI.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " + " + 
					ASTAngle.angleToStr(x, ASTMisc.DECFORMAT) + " = ");
			dT = dT + x;
			ChapGUI.printlnCond(ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			Lon_p = 180 + L_e + dT;
			ChapGUI.printlnCond("31. Finish computing the inferior object's geocentric ecliptic longitude.");
			ChapGUI.printlnCond("    Lon_p = 180 + Le + T = 180 + " + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + 
					" + " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) +
					" = " + ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("32. If necessary, adjust Lon_p to be in the range [0,360]");
			ChapGUI.printCond("    Lon_p = Lon_p MOD 360 = " + ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " MOD 360 = ");
			Lon_p = ASTMath.xMOD(Lon_p, 360.0);
			ChapGUI.printlnCond(ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " degrees");
		} else {
			y = R_e * ASTMath.SIN_D(Lp_p - L_e);
			x = R_p * ASTMath.COS_D(H_p) - R_e * ASTMath.COS_D(L_e - Lp_p);
			ChapGUI.printlnCond("28. This is a superior object, so compute its geocentric ecliptic longitude.");
			ChapGUI.printlnCond("    y = Re*sin(Lp_prime - Le) = " + String.format(ASTStyle.genFloatFormat,R_e) + "*sin(" +
					ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + " - " + ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,y));
			ChapGUI.printlnCond("    x = Rp*cos(Hp) - Re*cos(Le - Lp_prime)");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,R_p) + "*cos(" +
					ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + ") - " + String.format(ASTStyle.genFloatFormat,R_e) + "*cos(" +
					ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + " - " + ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,x));
			ChapGUI.printlnCond();

			dT = ASTMath.INVTAN_D(y / x);
			ChapGUI.printlnCond("29. Compute T = inv tan(y/x) = inv tan(" + String.format(ASTStyle.genFloatFormat,y) + 
					"/" + String.format(ASTStyle.genFloatFormat,x) + ") = " + 
					ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			x = ASTMath.quadAdjust(y, x);
			ChapGUI.printlnCond("30. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.");
			ChapGUI.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + 
					" + " + ASTAngle.angleToStr(x, ASTMisc.DECFORMAT) + " = ");
			dT = dT + x;
			ChapGUI.printlnCond(ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			Lon_p = Lp_p + dT;
			ChapGUI.printlnCond("31. Finish computing the superior object's geocentric ecliptic longitude.");
			ChapGUI.printlnCond("    Lon_p = Lp_prime + T = " + ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + 
					" + " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) +
					" = " + ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("32. If necessary, adjust Lon_p to be in the range [0,360]");
			ChapGUI.printCond("    Lon_p = Lon_p MOD 360 = " + ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " MOD 360 = ");
			Lon_p = ASTMath.xMOD(Lon_p, 360.0);
			ChapGUI.printlnCond(ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " degrees");
		}
		ChapGUI.printlnCond();

		Lat_p = R_p * ASTMath.COS_D(H_p) * ASTMath.TAN_D(H_p) * ASTMath.SIN_D(Lon_p - Lp_p);
		Lat_p = Lat_p / (R_e * ASTMath.SIN_D(Lp_p - L_e));
		Lat_p = ASTMath.INVTAN_D(Lat_p);
		ChapGUI.printlnCond("33. Compute the object's geocentric ecliptic latitude.");
		ChapGUI.printlnCond("    Lat_p = inv tan[(Rp*cos(Hp)*tan(Hp)*sin(Lon_p-Lp_prime))/(Re*sin(Lp_prime-Le))]");
		ChapGUI.printlnCond("          = inv tan[(" + String.format(ASTStyle.genFloatFormat,R_p) + "*cos(" + 
				ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + ")*tan(" +
				ASTAngle.angleToStr(H_p, ASTMisc.DECFORMAT) + ")*sin(" + 
				ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " - " + 
				ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + "))/");
		ChapGUI.printlnCond("                    (" + String.format(ASTStyle.genFloatFormat,R_e) + 
				"*sin(" + ASTAngle.angleToStr(Lp_p, ASTMisc.DECFORMAT) + " - " +
				ASTAngle.angleToStr(L_e, ASTMisc.DECFORMAT) + "))]");
		ChapGUI.printlnCond("          = " + ASTAngle.angleToStr(Lat_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		// All that remains now is to convert (Lat_p,Lon_p) to equatorial and horizon coordinates

		eqCoord = ASTCoord.EclipticToEquatorial(Lat_p, Lon_p, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("34. Convert the object's ecliptic coordinates (Lat_p, Lon_p) to equatorial coordinates.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(eqCoord.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) +
				" hours, Decl = " +	ASTAngle.angleToStr(eqCoord.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		horizonCoord = ASTCoord.RADecltoHorizon(eqCoord.getRAAngle().getDecTime(), eqCoord.getDeclAngle().getDecAngle(),
				observer.getObsLat(), LST);
		ChapGUI.printlnCond("35. Convert the object's equatorial coordinates to horizon coordinates.");
		ChapGUI.printlnCond("    Alt = " + ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) +
				", Az = " + ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Alt, " +
				ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Az";
		ChapGUI.printlnCond("Thus, for this observer location and date/time, " + objName);
		ChapGUI.printlnCond("is at " + result + ".");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults(objName + " is located at " + result);
	}

	/**
	 * Calculate the times that an object will rise and set.
	 */
	protected void calcObjRiseSet() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName, result;
		int idx;
		ASTInt dateAdjustObj = new ASTInt();
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double[] eclCoord;
		ASTCoord eqCoord;
		double[] LSTTimes;
		double Lat_p, Lon_p;
		double LSTr, LSTs, LCTr, LCTs, UT, GST;
		boolean riseSet = true;

		ChapGUI.clearTextAreas();

		idx = Misc.getValidObj(!Misc.GETSUN,!Misc.GETEARTH,!Misc.GETMOON);
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate when " + objName + " will rise and set for the Current Observer", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, Misc.termCriteria);
		Lat_p = eclCoord[ASTOrbits.OBJ_ECLLAT];
		Lon_p = eclCoord[ASTOrbits.OBJ_ECLLON];
		ChapGUI.printlnCond("1.  Calculate the ecliptic location for " + objName + " at midnight for the date in question.");
		ChapGUI.printlnCond("    For UT=0 hours on the date " +
				ASTDate.dateToStr(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(), 
						observer.getObsDate().getYear()));
		ChapGUI.printlnCond("    " + objName + " has eclipitic coordinates Lat_p = " +
				ASTAngle.angleToStr(Lat_p, ASTMisc.DECFORMAT) +	" degrees, Lon_p = " + 
				ASTAngle.angleToStr(Lon_p, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eqCoord = ASTCoord.EclipticToEquatorial(Lat_p, Lon_p, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("2.  Convert the object's geocentric ecliptic coordinates to equatorial coordinates.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(eqCoord.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) +
				" hours, Decl = " +	ASTAngle.angleToStr(eqCoord.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord.getRAAngle().getDecTime(), 
				eqCoord.getDeclAngle().getDecAngle(), observer.getObsLat());
		riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0);
		LSTr = LSTTimes[ASTOrbits.RISE_TIME];
		LSTs = LSTTimes[ASTOrbits.SET_TIME];		
		ChapGUI.printlnCond("3.  Using these equatorial coordinates, compute the LST rising and setting times.");
		if (riseSet) {
			ChapGUI.printlnCond("    LSTr = " + ASTTime.timeToStr(LSTr,ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond("    LSTs = " + ASTTime.timeToStr(LSTs, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond();

			GST = ASTTime.LSTtoGST(LSTr, observer.getObsLon());
			UT = ASTTime.GSTtoUT(GST, observer.getObsDate());
			LCTr = ASTTime.UTtoLCT(UT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);

			GST = ASTTime.LSTtoGST(LSTs, observer.getObsLon());
			UT = ASTTime.GSTtoUT(GST, observer.getObsDate());
			LCTs = ASTTime.UTtoLCT(UT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);

			ChapGUI.printlnCond("4.  Convert the LST rising/setting times to their corresponding LCT times.");
			ChapGUI.printlnCond("    LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.DECFORMAT) + " hours, LCTs = " +
					ASTTime.timeToStr(LCTs, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("5. Convert the LCT times to HMS format.");
			ChapGUI.printlnCond("   LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + 
					", LCTs = " + ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT));
			ChapGUI.printlnCond();

			ChapGUI.printlnCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(), 
					observer.getObsDate().getiDay(), observer.getObsDate().getYear()) + " for the current observer");
			result = objName + " will rise at " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + 
					" LCT and set at " + ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT) + " LCT";
		} else {
			ChapGUI.printlnCond();
			result = objName + " does not rise or set for this observer.";
		}
		ChapGUI.printlnCond(result);

		ChapGUI.setResults(result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate when a planet, including the Earth, will pass
	 * through perihelion and aphelieon.
	 */
	protected void calcPlanetPeriAphelion() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName, result;
		int idx, iTab, iKper, iTmp;
		double dK, dK0, dK1, dJ0, dJ1, dJ2, dKaph, JDper, JDaph;
		double UTperi, UTaph;
		int iDays;
		ASTDate periDate = new ASTDate();
		ASTDate aphDate = new ASTDate();
		double dY;

		ChapGUI.clearTextAreas();

		// This only works for the planets, so make sure the user specifies a planet
		// The orbital elements file could have non-planets in it (e.g, Pluto, Ceres)
		idx = Planets.getValidPlanet();
		if (idx < 0) return;

		objName = orbElements.getOEObjName(idx);

		iTab = Planets.findPlanetIdx(orbElements.getOEObjName(idx));

		if (iTab <0) {
			ASTMsg.errMsg("The specified object is not in the planets table", "Object not a Planet");
			return;
		}

		dK0 = Planets.getk0(iTab);
		dK1 = Planets.getk1(iTab);
		dJ0 = Planets.getj0(iTab);
		dJ1 = Planets.getj1(iTab);
		dJ2 = Planets.getj2(iTab);

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Determine when " + objName + " will pass through Perihelion/Aphelion", ASTPrt.CENTERTXT);
		ChapGUI.printlnCond("closest to the date " + ASTDate.dateToStr(observer.getObsDate()), ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		iDays = ASTDate.daysIntoYear(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(), 
				observer.getObsDate().getYear());
		ChapGUI.printlnCond("1.  Compute the number of days into the year for the given date.");
		ChapGUI.printlnCond("    Days = " + iDays);
		ChapGUI.printlnCond();

		dY = observer.getObsDate().getYear() + (iDays / 365.25);
		ChapGUI.printlnCond("2.  Compute Y = Year + (Days/365.25) = " + observer.getObsDate().getYear() + 
				" + (" + iDays + "/365.25) = " + String.format(ASTStyle.genFloatFormat,dY));
		ChapGUI.printlnCond();

		dK = dK0 * (dY - dK1);
		ChapGUI.printlnCond("3.  Using the appropriate 'k' coefficients for " + objName + ",");
		ChapGUI.printlnCond("    Compute K = k0*(Y - k1) = " + dK0 + "*(" +
				String.format(ASTStyle.genFloatFormat,dY) + " - (" + dK1 + "))");
		ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat,dK));
		ChapGUI.printlnCond();

		iKper = ASTMath.Trunc(Math.abs(dK) + 0.5);
		if (dK < 0) iKper = -iKper;
		iTmp = (int) Math.signum(dK - iKper);
		if (iTmp == 0) iTmp = 1;
		dKaph = iKper + 0.5 * iTmp;
		ChapGUI.printlnCond("4.  Let Kper be the integer value closest to K and let Kaph");
		ChapGUI.printlnCond("    be the fraction ending in 0.5 that is closest to K.");
		ChapGUI.printlnCond("    Kper = " + iKper + ", Kaph = " + String.format(ASTStyle.genFloatFormat,dKaph));
		ChapGUI.printlnCond();

		JDper = dJ0 + dJ1 * iKper + dJ2 * iKper * iKper;
		JDaph = dJ0 + dJ1 * dKaph + dJ2 * dKaph * dKaph;
		ChapGUI.printlnCond("5.  Compute JDper = j0 + j1*Kper + j2*Kper^2. This is the Julian day number for perihelion.");
		ChapGUI.printlnCond("                  = " + dJ0 + " + (" + dJ1 + ")*(" + iKper + ") + (" + dJ2 + ")*(" + iKper + ")^2");
		ChapGUI.printlnCond("                  = " + String.format(ASTStyle.JDFormat,JDper));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  Compute JDaph = j0 + j1*Kaph + j2*Kaph^2. This is the Julian day number for aphelion.");
		ChapGUI.printlnCond("                  = " + dJ0 + " + (" + dJ1 + ")*(" + dKaph + ") + (" + dJ2 + ")*(" + dKaph + ")^2");
		ChapGUI.printlnCond("                  = " + String.format(ASTStyle.JDFormat,JDaph));
		ChapGUI.printlnCond();

		periDate = ASTDate.JDtoDate(JDper);
		aphDate = ASTDate.JDtoDate(JDaph);
		ChapGUI.printlnCond("7.  Convert JDper and JDaph to calendar dates.");
		ChapGUI.printlnCond("    JDper gives " + periDate.getMonth() + "/" + 
				String.format(ASTStyle.genFloatFormat,periDate.getdDay()) +
				"/" + periDate.getYear());
		ChapGUI.printlnCond("    JDaph gives " + aphDate.getMonth() + "/" + 
				String.format(ASTStyle.genFloatFormat,aphDate.getdDay()) +
				"/" + aphDate.getYear());
		ChapGUI.printlnCond();

		UTperi = ASTMath.Frac(periDate.getdDay()) * 24.0;
		UTaph = ASTMath.Frac(aphDate.getdDay()) * 24.0;
		ChapGUI.printlnCond("8.  Convert the fractional part of the days to a decimal UT.");
		ChapGUI.printlnCond("    Perihelion: day " + periDate.getiDay() + ", " + 
				ASTTime.timeToStr(UTperi, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    Aphelion: day " + aphDate.getiDay() + ", " + 
				ASTTime.timeToStr(UTaph, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		result = objName + " will pass through ";
		ChapGUI.printlnCond("9.  Convert the UT results to HMS format.");
		prt.println("    " + result + "Perihelion on " + ASTDate.dateToStr(periDate) + " at " + 
				ASTTime.timeToStr(UTperi, ASTMisc.HMSFORMAT) + " UT");
		prt.println("    " + result + "Aphelion on " + ASTDate.dateToStr(aphDate) + " at " + 
				ASTTime.timeToStr(UTaph, ASTMisc.HMSFORMAT) + " UT");

		ChapGUI.setResults(result + "Perihelion/Aphelion as listed in the text area below");
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Set the termination criteria for solving Kepler's equation
	 */
	protected void setTerminationCriteria() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double dTerm = Misc.termCriteria;

		ChapGUI.clearTextAreas();;
		prt.setBoldFont(true);
		prt.println("Set Termination Criteria for use in solving Kepler's Equation", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		if (ASTQuery.showQueryForm("Enter Termination Criteria in radians\n(ex: 0.000002)") != ASTQuery.QUERY_OK) {
			prt.println("Termination criteria was not changed from " + Misc.termCriteria + " radians");
			return;
		}

		// Validate the termination criteria
		ASTReal rTmpObj = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (rTmpObj.isValidRealObj()) dTerm = rTmpObj.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria");
			prt.println("Termination criteria was not changed from " + Misc.termCriteria + " radians");
			return;
		}

		prt.println("Prior termination criteria was: " + Misc.termCriteria + " radians");
		if (dTerm < ASTKepler.KeplerMinCriteria) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too small, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		if (dTerm > 1) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too large, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		Misc.termCriteria = dTerm;

		prt.println("Termination criteria is now set to " + Misc.termCriteria + " radians");
		prt.resetCursor();
	}

}
