package jll.celcalc.chap8;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
* This class extends JMenuItem to add some additional fields, which
* simplifies the code for adding items to the menu bar and determining
* what actions to take as various menu items are selected. 
* <p>
* Copyright (c) 2018
* 
* @author J. L. Lawrence
* @version 3.0, 2018
*/

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {
	
	/** Define values for the eCalcType field in a ChapMenuItem class.
	 * These indicate what menu actions to take. */
	
	protected enum CalculationType {
		// Solar System Info menu
		/** Find an object's position */ OBJ_LOCATION,
		/** Find when an object will rise or set */ OBJ_RISE_SET,
		/** Calculate an object's distance and angular diameter */ OBJ_DIST_AND_ANG_DIAMETER,
		/** Calculate a planet's perihelion/aphelion times */ PLANET_PERI_APHELION,
		/** Calculate a object's perihelion/aphelion distance */ OBJ_PERI_APH_DIST,
		/** Calculate an object's % illumination */ OBJ_ILLUMINATION,
		/** Calculate an object's visual magnitude */ OBJ_MAGNITUDE,
		/** Calculate misc data about an object */ OBJ_MISC_DATA,
		/** Set a termination criteria for solving Kepler's equation */ TERM_CRITERIA,
		/** Select method for solving true anomaly */ KEPLER_OR_EQOFCENTER,
		
		// Data Files menu
		/** Load orbital elements data */ LOAD_ORBITAL_ELEMENTS,
		/** Display all orbital elements currently loaded */ SHOW_ALL_ORBITAL_ELEMENTS,
		/** Display an object's orbital elements */ SHOW_OBJ_ORBITAL_ELEMENTS,
        /** Load a star catalog */ LOAD_CATALOG,
        /** Show catalog header information */ SHOW_CATALOG_INFO,
        /** List all objects in the catalog */ LIST_ALL_OBJS_IN_CAT,
        /** List all constellations */ LIST_ALL_CONST,
        /** List all objects in a constellation */ LIST_ALL_CAT_OBJS_IN_CONST,
        /** Find constellation an object is in */ FIND_CONST_CAT_OBJ_IN,
        /** Find constellation a solar system object is in */ FIND_CONST_SOLAR_SYS_OBJ_IN,
        
        // Star Charts menu
        /** Set a visual magnitude filter */ SET_mV_FILTER,
        /** Draw all stars in the catalog */ DRAW_ALL_STARS,
        /** Draw all stars in a constellation */ DRAW_ALL_STARS_IN_CONST,
        /** Draw brightest star in each constellation */ DRAW_ALL_CONST,
        /** Draw all Solar System objects */ DRAW_ALL_SOLAR_SYS_OBJS,
        /** Draw a specified Solar System object */ DRAW_SOLAR_SYS_OBJ,
        /** Locate an object in the catalog */ FIND_OBJ,
        /** Locate the brightest object in the catalog */ FIND_BRIGHTEST_OBJ,
        /** Locate an equatorial coordinate location */ FIND_EQ_LOC,
        /** Locate a horizon coordinate location */ FIND_HORIZ_LOC
	}
	
	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Solar Info and Orbital Elements menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;
		
		// Create Solar Sys Info menu items
		menu = ChapGUI.getSolarSysInfoMenu();
		createMenuItem(menu,"Find Object's Position", CalculationType.OBJ_LOCATION);
		menu.addSeparator();
		createMenuItem(menu,"Object Rise/Set Times", CalculationType.OBJ_RISE_SET);
		createMenuItem(menu,"Object Distance and Angular Diameter", CalculationType.OBJ_DIST_AND_ANG_DIAMETER);
		menu.addSeparator();
		createMenuItem(menu,"Planetary Perihelion and Aphelion", CalculationType.PLANET_PERI_APHELION);
		createMenuItem(menu,"Distance at Perihelion and Aphelion", CalculationType.OBJ_PERI_APH_DIST);
		menu.addSeparator();
		createMenuItem(menu,"Object Percent Illumination", CalculationType.OBJ_ILLUMINATION);
		createMenuItem(menu,"Object Visual Magnitude", CalculationType.OBJ_MAGNITUDE);
		menu.addSeparator();
		createMenuItem(menu,"Miscellaneous Data", CalculationType.OBJ_MISC_DATA);
		menu.addSeparator();
		createMenuItem(menu,"Set Termination Criteria", CalculationType.TERM_CRITERIA);
		createMenuItem(menu,"Select True Anomaly Method", CalculationType.KEPLER_OR_EQOFCENTER);
		
		// Create Data Files menu
		menu = ChapGUI.getDataFilesMenu();
		createMenuItem(menu,"Load Orbital Elements", CalculationType.LOAD_ORBITAL_ELEMENTS);
		createMenuItem(menu,"Display All Orbital Elements", CalculationType.SHOW_ALL_ORBITAL_ELEMENTS);
		createMenuItem(menu,"Display Object's Orbital Elements", CalculationType.SHOW_OBJ_ORBITAL_ELEMENTS);
		menu.addSeparator();
		createMenuItem(menu,"Load a Star Catalog", CalculationType.LOAD_CATALOG);
		createMenuItem(menu,"Show Catalog Information", CalculationType.SHOW_CATALOG_INFO);
		createMenuItem(menu,"List all Objects in the Catalog", CalculationType.LIST_ALL_OBJS_IN_CAT);
		menu.addSeparator();
		createMenuItem(menu,"List All Constellations", CalculationType.LIST_ALL_CONST);
		createMenuItem(menu,"List all Catalog Objects in a Constellation", CalculationType.LIST_ALL_CAT_OBJS_IN_CONST);
		menu.addSeparator();
		createMenuItem(menu,"Find Constellation a RA/Decl is In", CalculationType.FIND_CONST_CAT_OBJ_IN);
		createMenuItem(menu,"Find Constellation a Solar System Object is In", CalculationType.FIND_CONST_SOLAR_SYS_OBJ_IN);
		
		// Create Star Charts menu
		JMenu tmpSubMenu;
		menu = ChapGUI.getStarChartsMenu();
		createMenuItem(menu,"Set mV Filter", CalculationType.SET_mV_FILTER);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Draw ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"All Stars in the Catalog", CalculationType.DRAW_ALL_STARS);
		createMenuItem(tmpSubMenu,"All Stars in a Constellation", CalculationType.DRAW_ALL_STARS_IN_CONST);
		createMenuItem(tmpSubMenu,"Brightest Star in each Constellation", CalculationType.DRAW_ALL_CONST);
		tmpSubMenu.addSeparator();
        createMenuItem(tmpSubMenu,"All Solar System Objects", CalculationType.DRAW_ALL_SOLAR_SYS_OBJS);
        createMenuItem(tmpSubMenu,"Sun, Moon, or a Planet", CalculationType.DRAW_SOLAR_SYS_OBJ);
		menu.addSeparator();
		tmpSubMenu = new JMenu("Locate ...");
		tmpSubMenu.setFont(ASTStyle.MENU_FONT);
		menu.add(tmpSubMenu);
		createMenuItem(tmpSubMenu,"An Object in the Catalog", CalculationType.FIND_OBJ);
		createMenuItem(tmpSubMenu,"Brightest Object in the Catalog", CalculationType.FIND_BRIGHTEST_OBJ);
		tmpSubMenu.addSeparator();
		createMenuItem(tmpSubMenu,"An Equatorial Coordinate", CalculationType.FIND_EQ_LOC);
		createMenuItem(tmpSubMenu,"A Horizon Coordinate", CalculationType.FIND_HORIZ_LOC);
	}
		
	/** Define a class for what a  menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of calculation to do
		private CalculationType eCalcType;   // what type of calculation to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of calculation is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {
			super(title);
			eCalcType = calcType;
		}
		
		/*========================================================
		 * Define accessor methods for the menu item fields
		 *=======================================================*/
		
		/**
		 * Gets the type of calculation to perform.
		 * 
		 * @return		the type of calculation to perform
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}
	
	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 *----------------------------------------------------------------------------*/

	/**
	 * Creates a menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of calculation to perform
	 * @return					ChapMnuItm created
	 */     
	private ChapMnuItm createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getChapMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());
		
		return tmp;
	}

}
