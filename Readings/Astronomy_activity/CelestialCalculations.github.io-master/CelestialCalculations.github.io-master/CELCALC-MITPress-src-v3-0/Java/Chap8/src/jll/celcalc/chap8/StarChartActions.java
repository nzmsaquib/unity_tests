package jll.celcalc.chap8;

import java.awt.Color;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCatalog;
import jll.celcalc.ASTUtils.ASTCharts;
import jll.celcalc.ASTUtils.ASTConstellation;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Star Charts menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class StarChartActions {

	// Color to use when highlighting an object, brightest object, and located object.
	// Also define large mV values to use to display brightest and located objects.
	private static Color HIGHLIGHT_COLOR = Color.red;
	private static Color BRIGHTEST_OBJ_COLOR = Color.blue;
	private static Color LOCATE_OBJ_COLOR = Color.green;
	private static Color SUN_COLOR = new Color(244,102,25); // bright orange
	private static Color MOON_COLOR = new Color(192,192,192); // silver
	private static Color PLANET_COLOR = new Color(166, 77, 232); // purple
	private static final double BRIGHT_mV = -8.0;
	private static final double LOCATE_mV = -12.0;

	/**
	 * Ask user for a filter and update the GUI
	 */
	protected void changemVFilter() {	
		ChapGUI.clearTextAreas();

		if (ASTQuery.showQueryForm("Enter desired visual magnitude filter") == ASTQuery.QUERY_OK) {
			// validate data
			ASTReal rTmp = ASTReal.isValidReal(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
			if (rTmp.isValidRealObj()) ChapGUI.setmVFilter(rTmp.getRealValue());
			else ASTMsg.errMsg("The mV Filter entered is invalid - try again.", "Invalid mV Filter");
		}
	}

	/**
	 * Draw all of the objects in the orbital elements
	 * file, except for the Earth
	 */
	protected void drawAllSolarSysObjs() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		boolean labelObjs = ChapGUI.getLabelObjs();
		boolean eqChart, bkgColor;
		int idx;
		int idxEarth = orbElements.getOEDBEarthIndex();
		int idxSun = orbElements.getOEDBSunIndex();
		int idxMoon = orbElements.getOEDBMoonIndex();
		String objName;
		double RA, Decl, mV;
		double LST;
		ASTCoord horizonCoord;
		ASTCoord eqCoord;
		Color plotColor;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		eqChart = ChapGUI.getEQChartStatus();

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		// Cycle through all the objects in the orbital elements data file
		for (idx = 0; idx < orbElements.getNumOEDBObjs(); idx++) {

			// Skip the Earth
			if (idx == idxEarth) continue;

			objName = orbElements.getOEObjName(idx);
			eqCoord = Misc.getSolarSysEqCoord(idx);
			RA = eqCoord.getRAAngle().getDecTime();
			Decl = eqCoord.getDeclAngle().getDecAngle();
			mV = orbElements.getOEObjmV(idx);
			if (idx == idxSun) plotColor = SUN_COLOR;
			else if (idx == idxMoon) plotColor = MOON_COLOR;
			else plotColor = PLANET_COLOR;

			if (eqChart) {
				starChart.plotRADecl(RA, Decl, mV, plotColor);
				if (labelObjs) starChart.drawLabelRADecl(objName, RA, Decl, plotColor);
			} else {
				horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
				// plotAltAz and drawLabelAltAz will ignore if object is below the horizon
				starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), mV, plotColor);
				if (labelObjs) starChart.drawLabelAltAz(objName, horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), plotColor);
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults(" ... all done plotting Solar System objects ...");
	}

	/**
	 * Draw all of the stars in the catalog that are at
	 * least as bright as the mV filter.
	 */
	protected void drawAllStars() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		boolean eqChart, bkgColor;
		int iConst, plotLimit, plotCount;
		String result = "";
		double RA, Decl, mV;
		double LST;
		boolean showObj;
		ASTCoord horizonCoord;
		double currentmVFilter = ChapGUI.getcurrentmVFilter();

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;
		
		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded");
			return;			// No need to go any further since there is nothing to plot
		}		

		ChapGUI.setChartOnScreen(true);

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		eqChart = ChapGUI.getEQChartStatus();

		starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		// See if we need to bother about pausing during plotting
		plotLimit = ASTMisc.OBJS_LIMIT + 10;          // this really means to ignore pausing during display
		if (ASTCatalog.getCatNumObjs() > ASTMisc.OBJS_LIMIT) {
			if (ASTMsg.abortMsg("Plotting " + ASTCatalog.getCatNumObjs() + " objects may take a long time.")) return;

			if (ASTQuery.showQueryForm("Enter Max # of Objs to Plot at a Time\n" +
					"(Recommend no More Than " + ASTMisc.MAX_OBJS_TO_PLOT + " Objs)") == ASTQuery.QUERY_OK) {
				ASTInt iTmp = ASTInt.isValidInt(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
				if (iTmp.isValidIntObj()) plotLimit = iTmp.getIntValue();
				else plotLimit = ASTMisc.MAX_OBJS_TO_PLOT;
			}
		}

		iConst = -1;				// set a bogus constellation
		plotCount = 0;
		for (int i=0; i < ASTCatalog.getCatNumObjs(); i++) {

			plotCount = plotCount + 1;
			if (plotCount > plotLimit) {
				starChart.refreshChart();		// show what's been plotted so far
				if (ASTMsg.abortMsg("Plotted " + i + " of " + ASTCatalog.getCatNumObjs() + " objects")) break;
				plotCount = 0;
			}

			RA = ASTCatalog.getCatObjRA(i);
			Decl = ASTCatalog.getCatObjDecl(i);
			if (ASTCatalog.getCatObjConstIdx(i) != iConst) {
				iConst = ASTCatalog.getCatObjConstIdx(i);
				result = "Plotting constellation " + ASTConstellation.getConstName(iConst);
				ChapGUI.setResults(result);
			}

			// See if this object should be filtered out
			if (ASTCatalog.getCatObjmVKnown(i)) {
				mV = ASTCatalog.getCatObjmV(i);
				showObj = (mV <= currentmVFilter);
			} else {
				showObj = true;      				// show object even if we don't know its mV value
				mV = ASTMisc.mV_NAKED_EYE;  		// default to what the naked eye can see
			}

			if (showObj) {
				if (eqChart) starChart.plotRADecl(RA,Decl,mV);
				else {
					horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
					starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
							horizonCoord.getAzAngle().getDecAngle(), mV);
				}
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults(" ... all done plotting ...");
	}

	/**
	 * Draw all of the stars in the catalog within a user specified
	 * constellation that are at least as bright as the mV filter.
	 * Also draw the brightest star in the constellation.
	 */
	protected void drawAllStarsInConst() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		double currentmVFilter = ChapGUI.getcurrentmVFilter();
		boolean bkgColor;
		boolean eqChart = ChapGUI.getEQChartStatus();
		boolean labelStars = ChapGUI.getLabelObjs();
		int i, iConst;
		double RA, brightRA, Decl, brightDecl, mV;
		double LST;
		boolean showObj;
		String constAbbrevName;
		ASTCoord horizonCoord;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded");
			return;			// No need to go any further since there is nothing to plot
		}		

		if (ASTQuery.showQueryForm("Enter Constellation's 3 Character\nAbbreviated Name") == ASTQuery.QUERY_OK) {
			constAbbrevName = ASTQuery.getData1();
			if ((constAbbrevName == null) || (constAbbrevName.length() <= 0)) return;
			iConst = ASTConstellation.findConstellationByAbbrvName(constAbbrevName.trim());
			if (iConst < 0) {
				ASTMsg.errMsg("No Constellation whose abbreviated name is '" + constAbbrevName + "' was found",
						"Invalid Constellation");
				return;
			}
		} else return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		ChapGUI.setResults("Plotting constellation " + ASTConstellation.getConstName(iConst));

		brightRA = ASTConstellation.getConstBrightestStarRA(iConst);
		brightDecl = ASTConstellation.getConstBrightestStarDecl(iConst);

		// Cycle through the catalog and get only those objects in the specified constellation
		for (i=0; i < ASTCatalog.getCatNumObjs(); i++) {
			RA = ASTCatalog.getCatObjRA(i);
			Decl = ASTCatalog.getCatObjDecl(i);
			if (ASTCatalog.getCatObjConstIdx(i) == iConst) {
				// See if this object should be filtered out
				if (ASTCatalog.getCatObjmVKnown(i)) {
					mV = ASTCatalog.getCatObjmV(i);
					showObj = (mV <= currentmVFilter);
				} else {
					showObj = true;					// show object even if we don't know its mV value
					mV = ASTMisc.mV_NAKED_EYE;		// default to what the naked eye can see
				}

				if (showObj) {
					if (eqChart) starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR);
					else {
						horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
						starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
								horizonCoord.getAzAngle().getDecAngle(), mV, HIGHLIGHT_COLOR);
					}
				}
			}
		}

		// Now draw the brightest star in the constellation
		if (eqChart) {
			starChart.plotRADecl(brightRA, brightDecl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR);
			if (labelStars) starChart.drawLabelRADecl(ASTConstellation.getConstBrightestStarName(iConst), 
					brightRA, brightDecl, BRIGHTEST_OBJ_COLOR);
		} else {
			horizonCoord = ASTCoord.RADecltoHorizon(brightRA, brightDecl, observer.getObsLat(), LST);
			starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
					horizonCoord.getAzAngle().getDecAngle(), BRIGHT_mV, BRIGHTEST_OBJ_COLOR);
			if (labelStars) starChart.drawLabelAltAz(ASTConstellation.getConstBrightestStarName(iConst),
					horizonCoord.getAltAngle().getDecAngle(), horizonCoord.getAzAngle().getDecAngle(),
					BRIGHTEST_OBJ_COLOR);
		}

		starChart.refreshChart();
		ChapGUI.setResults("Brightest star in " + ASTConstellation.getConstName(iConst) +
				" is " + ASTConstellation.getConstBrightestStarName(iConst));
	}

	/**
	 * Draw the brightest star in each of the constellations.
	 */
	protected void drawBrightestStarInAllConst() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		boolean eqChart = ChapGUI.getEQChartStatus();
		boolean labelStars = ChapGUI.getLabelObjs();
		boolean bkgColor;
		int i;
		double RA, Decl;
		double LST;
		ASTCoord horizonCoord;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		if (labelStars) {
			if (!ASTMsg.pleaseConfirm("Labelling the stars may make the display\n" +
					"unreadable. Do you wish to continue anyway?", " ")) return;
		}

		for (i=0; i < ASTConstellation.getNumConstellations(); i++) {
			ChapGUI.setResults("Plotting brightest star in constellation " + ASTConstellation.getConstName(i));

			RA = ASTConstellation.getConstBrightestStarRA(i);
			Decl = ASTConstellation.getConstBrightestStarDecl(i);

			if (eqChart) {
				starChart.plotRADecl(RA, Decl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR);
				if (labelStars) starChart.drawLabelRADecl(ASTConstellation.getConstBrightestStarName(i), 
						RA, Decl, BRIGHTEST_OBJ_COLOR);
			} else {
				horizonCoord = ASTCoord.RADecltoHorizon(RA,Decl, observer.getObsLat(), LST);
				starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), BRIGHT_mV, BRIGHTEST_OBJ_COLOR);
				if (labelStars) starChart.drawLabelAltAz(ASTConstellation.getConstBrightestStarName(i),
						horizonCoord.getAltAngle().getDecAngle(), horizonCoord.getAzAngle().getDecAngle(),
						BRIGHTEST_OBJ_COLOR);
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults("Finished plotting brightest stars in each constellation ...");
	}

	/**
	 * Draw an object in the orbital elements
	 * file, except for the Earth
	 */
	protected void drawSolarSysObj() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		boolean labelObjs = ChapGUI.getLabelObjs();
		boolean eqChart, bkgColor;
		int idx;
		int idxSun = orbElements.getOEDBSunIndex();
		int idxMoon = orbElements.getOEDBMoonIndex();
		String objName, result;
		double RA, Decl, mV;
		double LST;
		ASTCoord horizonCoord;
		ASTCoord eqCoord;
		Color plotColor;
	
		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;
		
		idx = Misc.getValidObj(Misc.GETSUN,!Misc.GETEARTH,Misc.GETMOON);
		if (idx < 0) return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		eqChart = ChapGUI.getEQChartStatus();
		
		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		objName = orbElements.getOEObjName(idx);
		eqCoord = Misc.getSolarSysEqCoord(idx);
		RA = eqCoord.getRAAngle().getDecTime();
		Decl = eqCoord.getDeclAngle().getDecAngle();
		mV = orbElements.getOEObjmV(idx);
		if (idx == idxSun) plotColor = SUN_COLOR;
		else if (idx == idxMoon) plotColor = MOON_COLOR;
		else plotColor = PLANET_COLOR;
		
		if (eqChart) {
			starChart.plotRADecl(RA, Decl, mV, plotColor);
			if (labelObjs) starChart.drawLabelRADecl(objName, RA, Decl, plotColor);
		} else {
			horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
			if (horizonCoord.getAltAngle().getDecAngle() > 0) {
				starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), mV, plotColor);
				if (labelObjs) starChart.drawLabelAltAz(objName, horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), plotColor);
			} else {
				ASTMsg.infoMsg("The object '" + objName + "' is below the observer's horizon");
			}
		}
	
		starChart.refreshChart();
		if ((idx == idxSun) || (idx == idxMoon)) result = "The ";
		else result = "";
		ChapGUI.setResults(result + objName + " is at " + ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT) +
				" RA, " + ASTAngle.angleToStr(Decl, ASTMisc.DMSFORMAT) + " Decl");

	  }

	/**
	 * Locate a horizon coordinate that the user specifies
	 */
	protected void locateAltAz() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		boolean bkgColor;
		boolean eqChart = ChapGUI.getEQChartStatus();
		boolean labelStars = ChapGUI.getLabelObjs();
		double LST;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		if (ASTQuery.showQueryForm("Enter Altitude (xxxd yym zz.zzs)", 
				"Enter Azimuth (xxxd yym zz.zzs)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTAngle altObj = ASTAngle.isValidAngle(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (!altObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Altitude entered is invalid - try again.", "Invalid Altitude");
			return;
		}
		ASTAngle azObj = ASTAngle.isValidAngle(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (!azObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth");
			return;
		}

		if (eqChart) {
			ASTCoord eqCoord = ASTCoord.HorizontoRADecl(altObj.getDecAngle(), azObj.getDecAngle(), observer.getObsLat(), LST);
			starChart.plotRADecl(eqCoord.getRAAngle().getDecTime(), eqCoord.getDeclAngle().getDecAngle(),
					LOCATE_mV, LOCATE_OBJ_COLOR);
			if (labelStars) starChart.drawLabelRADecl("LOC", eqCoord.getRAAngle().getDecTime(),
					eqCoord.getDeclAngle().getDecAngle(), LOCATE_OBJ_COLOR);
		} else {
			if (altObj.getDecAngle() < 0.0) ASTMsg.infoMsg("The stated coordinate is below the observer's horizon");
			else {
				starChart.plotAltAz(altObj.getDecAngle(), azObj.getDecAngle(), LOCATE_mV, LOCATE_OBJ_COLOR);
				if (labelStars) starChart.drawLabelAltAz("LOC", altObj.getDecAngle(), azObj.getDecAngle(), LOCATE_OBJ_COLOR);
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults(ASTAngle.angleToStr(altObj.getDecAngle(), ASTMisc.DMSFORMAT) + " Alt, " + 
				ASTAngle.angleToStr(azObj.getDecAngle(),ASTMisc.DMSFORMAT) + " Az");
	}

	/**
	 * Locate and highlight the brightest object in the catalog
	 */
	protected void locateBrightestObjInCat() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		boolean bkgColor;
		boolean eqChart = ChapGUI.getEQChartStatus();
		boolean labelStars = ChapGUI.getLabelObjs();
		int i, idx = 0;
		double LST;
		double RA, Decl, mV;
		double brightestmV = ASTMisc.UNKNOWN_mV;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;
		
		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded");
			return;			// No need to go any further since there is nothing to plot
		}

		if (!ASTCatalog.getCatmVProvided()) {
			ASTMsg.infoMsg("The currently loaded catalog does not provide magnitudes...");
			return;
		}

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		// Cycle through and find brightest object
		for (i = 0; i < ASTCatalog.getCatNumObjs(); i++) {
			mV = ASTCatalog.getCatObjmV(i);
			if (mV < brightestmV) {
				brightestmV = mV;
				idx = i;
			}
		}

		RA = ASTCatalog.getCatObjRA(idx);
		Decl = ASTCatalog.getCatObjDecl(idx);
		mV = ASTCatalog.getCatObjmV(idx);

		if (eqChart) {
			starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR);
			if (labelStars) starChart.drawLabelRADecl(ASTCatalog.getCatObjName(idx), RA, Decl, HIGHLIGHT_COLOR);
		} else {
			ASTCoord horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
			if (horizonCoord.getAltAngle().getDecAngle() < 0) ASTMsg.infoMsg("The object '" + 
					ASTCatalog.getCatObjName(idx) + "' is below the observer's horizon");
			else {
				starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), mV, HIGHLIGHT_COLOR);
				if (labelStars) starChart.drawLabelAltAz(ASTCatalog.getCatObjName(idx),
						horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), HIGHLIGHT_COLOR);       
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults("Brightest Object is '" + ASTCatalog.getCatObjName(idx) + "' at " + 
				ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT) + " RA, " + 
				ASTAngle.angleToStr(Decl, ASTMisc.DMSFORMAT) + " Decl, " + 
				String.format(ASTStyle.mVFormat,mV) + " mV");
	}

	/**
	 * Locate an equatorial coordinate that the user specifies
	 */
	protected void locateRADecl() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		boolean bkgColor;
		boolean eqChart = ChapGUI.getEQChartStatus();
		boolean labelStars = ChapGUI.getLabelObjs();
		double LST;

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		if (ASTQuery.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", 
				"Enter Declination (xxxd yym zz.zzs)") != ASTQuery.QUERY_OK) return;

		// validate data
		ASTTime raObj = ASTTime.isValidTime(ASTQuery.getData1(),ASTMisc.HIDE_ERRORS);
		if (!raObj.isValidTimeObj()) {
			ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA");
			return;
		}
		ASTAngle declObj = ASTAngle.isValidAngle(ASTQuery.getData2(),ASTMisc.HIDE_ERRORS);
		if (!declObj.isValidAngleObj()) {
			ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl");
			return;
		}

		if (eqChart) {
			starChart.plotRADecl(raObj.getDecTime(), declObj.getDecAngle(), LOCATE_mV, LOCATE_OBJ_COLOR);
			if (labelStars) starChart.drawLabelRADecl("LOC", raObj.getDecTime(), declObj.getDecAngle(), LOCATE_OBJ_COLOR);
		} else {
			ASTCoord horizonCoord = ASTCoord.RADecltoHorizon(raObj.getDecTime(), declObj.getDecAngle(), observer.getObsLat(), LST);
			if (horizonCoord.getAltAngle().getDecAngle() < 0.0) ASTMsg.infoMsg("The stated coordinate is below the observer's horizon");
			else {
				starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), LOCATE_mV, LOCATE_OBJ_COLOR);
				if (labelStars) starChart.drawLabelAltAz("LOC", horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), LOCATE_OBJ_COLOR);
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults(ASTTime.timeToStr(raObj.getDecTime(), ASTMisc.HMSFORMAT) + " RA, " +
				ASTAngle.angleToStr(declObj.getDecAngle(), ASTMisc.DMSFORMAT) + " Decl");
	}

	/**
	 * Locate and highlight a user-specified object from the catalog
	 */
	protected void locateStarCatalogObj() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTCharts starChart = ChapGUI.getStarChart();
		boolean bkgColor;
		boolean eqChart = ChapGUI.getEQChartStatus();
		boolean labelStars = ChapGUI.getLabelObjs();
		int idx;
		double LST;
		double RA, Decl, mV;
		String searchStr;		

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded");
			return;			// No need to go any further since there is nothing to plot
		}

		// Validate the observer location data
		if (!ChapGUI.validateGUIObsLoc()) return;

		if (ASTQuery.showQueryForm("Enter the Object's Name") == ASTQuery.QUERY_OK) {
			searchStr = ASTQuery.getData1();
			if ((searchStr == null) || (searchStr.length() <= 0)) return;
			idx = ASTCatalog.findObjByName(searchStr.trim());
			if (idx < 0) {
				ASTMsg.errMsg("No Object with the name '" + searchStr + "' was found in the catalog",
						"Invalid Object Name");
				return;
			}
		} else return;

		LST = Misc.computeLST();

		if (ChapGUI.getWhiteChartStatus()) bkgColor = ASTCharts.WHITE_BKG;
		else bkgColor = ASTCharts.BLACK_BKG;

		// If there's already a plot on the screen, don't erase it
		if (!ChapGUI.getChartOnScreen()) starChart.initDrawingCanvas(ChapGUI.getCanvasSize(), bkgColor, eqChart);

		ChapGUI.setChartOnScreen(true);

		RA = ASTCatalog.getCatObjRA(idx);
		Decl = ASTCatalog.getCatObjDecl(idx);

		// Show the object regardless of whether it is within the mV filter
		if (ASTCatalog.getCatObjmVKnown(idx)) mV = ASTCatalog.getCatObjmV(idx);
		else mV = ASTMisc.mV_NAKED_EYE;				// default to what the naked eye can see

		if (eqChart) {
			starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR);
			if (labelStars) starChart.drawLabelRADecl(searchStr, RA, Decl, HIGHLIGHT_COLOR);
		} else {
			ASTCoord horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST);
			if (horizonCoord.getAltAngle().getDecAngle() < 0.0) 
				ASTMsg.infoMsg("The object '" + searchStr + "' is below the observer's horizon");
			else {
				starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(), mV, HIGHLIGHT_COLOR);
				if (labelStars) starChart.drawLabelAltAz(searchStr, horizonCoord.getAltAngle().getDecAngle(),
						horizonCoord.getAzAngle().getDecAngle(),HIGHLIGHT_COLOR);
			}
		}

		starChart.refreshChart();
		ChapGUI.setResults("The object '" + searchStr + "' is at " + 
				ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT) + " RA, " +
				ASTAngle.angleToStr(Decl, ASTMisc.DMSFORMAT) + " Decl");
	}

}
