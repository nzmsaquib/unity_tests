package jll.celcalc.chap8;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EtchedBorder;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTStyle;

/**
 * <b>Provides a GUI for selecting a method for solving Kepler's equation.</b>
 * <p>
 * This GUI was created by, and is maintained by, the 
 * Eclipse WindowBuilder. Some 'hand editing' was done to use the 
 * styles in ASTStyle to enforce a common GUI style across all programs.
 * <p>
 * Copyright (c) 2018
 *  
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

@SuppressWarnings("serial")
class TrueAnomalyRBs extends JDialog {
	
	// save the parent frame from the routine that creates this dialog so that the
	// dialog can be centered on the parent
	private JFrame parentFrame = null;
	
	// Define the radio buttons
	private JRadioButton radbtnEQofCenter = new JRadioButton("Equation of Center");
	private JRadioButton radbtnSimple = new JRadioButton("Simple Iteration");
	private JRadioButton radbtnNewton = new JRadioButton("Newton/Raphson Method");
	
	// Define a listener for managing the radio buttons
	private ActionListener listenerRadioBtns = new KeplerRadioBtnsListener();
	
	/* define radio button commands */
	private static final String radbtnEQofCenterCommand = "eqofcenter";
	private static final String radbtnSimpleCommand = "simple";
	private static final String radbtnNewtonCommand = "newton";
	
	/* define button controls */
	private static final String OKCommand = "ok";

	/**
	 * Create the dialog.
	 */
	protected TrueAnomalyRBs() {
		setType(Type.POPUP);
		setResizable(false);
		setTitle("Choose method ...");
		setBounds(100, 100, 264, 247);
		
		JPanel trueAnomalyGroupBox = new JPanel();
		trueAnomalyGroupBox.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		
		JLabel lblTitle = new JLabel("True Anomaly Methods");
		lblTitle.setFont(ASTStyle.TEXT_BOLDFONT);
		lblTitle.setLabelFor(trueAnomalyGroupBox);
		
		JButton btnOK = new JButton("OK");
		btnOK.setFont(ASTStyle.BTN_FONT);
		btnOK.setActionCommand(OKCommand);
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(43)
							.addComponent(lblTitle))
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(trueAnomalyGroupBox, GroupLayout.PREFERRED_SIZE, 222, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(104)
							.addComponent(btnOK)))
					.addContainerGap(24, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblTitle)
					.addGap(13)
					.addComponent(trueAnomalyGroupBox, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
					.addComponent(btnOK)
					.addContainerGap())
		);
		radbtnEQofCenter.setFont(ASTStyle.RADBTN_FONT);
		radbtnEQofCenter.setActionCommand(radbtnEQofCenterCommand);
		radbtnEQofCenter.addActionListener(listenerRadioBtns);
		radbtnSimple.setFont(ASTStyle.RADBTN_FONT);
		radbtnSimple.setActionCommand(radbtnSimpleCommand);
		radbtnSimple.addActionListener(listenerRadioBtns);
		radbtnNewton.setFont(ASTStyle.RADBTN_FONT);
		radbtnNewton.setActionCommand(radbtnNewtonCommand);
		radbtnNewton.addActionListener(listenerRadioBtns);
		
		GroupLayout gl_trueAnomalyGroupBox = new GroupLayout(trueAnomalyGroupBox);
		gl_trueAnomalyGroupBox.setHorizontalGroup(
			gl_trueAnomalyGroupBox.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_trueAnomalyGroupBox.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_trueAnomalyGroupBox.createParallelGroup(Alignment.LEADING)
						.addComponent(radbtnSimple)
						.addComponent(radbtnEQofCenter)
						.addComponent(radbtnNewton))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_trueAnomalyGroupBox.setVerticalGroup(
			gl_trueAnomalyGroupBox.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_trueAnomalyGroupBox.createSequentialGroup()
					.addContainerGap()
					.addComponent(radbtnEQofCenter)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(radbtnSimple)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(radbtnNewton)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		
		ASTStyle.setASTStyle();
		radbtnEQofCenter.setSelected(true);
		
		trueAnomalyGroupBox.setLayout(gl_trueAnomalyGroupBox);
		getContentPane().setLayout(groupLayout);
		getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{radbtnEQofCenter, radbtnSimple, radbtnNewton, btnOK}));
	}
	
	/**
	 * Gets the current selection of the radio buttons
	 * 
	 * @return				ASTKepler.TrueAnomalyType
	 */
	protected ASTKepler.TrueAnomalyType getKeplerRadBtnSetting() {
		if (radbtnSimple.isSelected()) return ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (radbtnNewton.isSelected()) return ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else return ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;
	}
		
	/**
	 * Saves a reference to the caller's parent frame so the dialog can be centered on the 
	 * parent application's window.
	 * 
	 * @param parent		parent frame for the main application
	 */
	protected void setParentFrame(JFrame parent) {
		parentFrame = parent;
	}
	
	/**
	 * Shows the dialog
	 */
	protected void showDialog() {
		this.setLocationRelativeTo(parentFrame);
		this.setVisible(true);
	}
	
	/*------------------------------------------------------------------------
	 * Define a private class and some methods for managing the radio buttons
	 *-----------------------------------------------------------------------*/

	private class KeplerRadioBtnsListener implements ActionListener {

		/**
		 * Handles clicks on individual radio buttons.
		 * 
		 * @param e		ActionEvent object with information about what the user clicked on
		 */	
		public void actionPerformed(ActionEvent e) {
			String action = e.getActionCommand();
			if (action.equals(getSimpleCommand())) setSimpleRadBtn();
			else if (action.equals(getNewtonMethodCommand())) setNewtonRadBtn();
			else setEQofCenterRadBtn();
		}
		
		/**
		 * Clears all of the Kepler's equation GUI radio buttons (i.e., sets them to unchecked). This
		 * routine is useful since Swing does not automatically manage radio buttons.
		 */
		private void clearAllKeplerEQRadioBtns() {
			radbtnEQofCenter.setSelected(false);
			radbtnEQofCenter.setFocusPainted(false);
			radbtnEQofCenter.setFocusable(false);

			radbtnSimple.setSelected(false);
			radbtnSimple.setFocusPainted(false);
			radbtnSimple.setFocusable(false);

			radbtnNewton.setSelected(false);
			radbtnNewton.setFocusPainted(false);
			radbtnNewton.setFocusable(false);
		}
		
		/**
		 * Gets the command that represents the EQ of Center radio button.
		 * 
		 * @return			EQ of Center radio button command
		 */
		@SuppressWarnings("unused")
		private String getEQofCenterCommand() {
			return radbtnEQofCenterCommand;
		}

		/**
		 * Gets the command that represents the Newton's Method radio button.
		 * 
		 * @return			Newton's Method radio button command
		 */
		private String getNewtonMethodCommand() {
			return radbtnNewtonCommand;
		}

		/**
		 * Gets the command that represents the Simple Iteration radio button.
		 * 
		 * @return			Simple Iteration radio button command
		 */
		private String getSimpleCommand() {
			return radbtnSimpleCommand;
		}

		/**
		 * Sets the EQ of Center radio button to true
		 */
		private void setEQofCenterRadBtn() {
			// make sure only 1 radio button is set when we're done
			clearAllKeplerEQRadioBtns();				// make sure only 1 radio button is set when we're done
			radbtnEQofCenter.setSelected(true);
		}

		/**
		 * Sets the Newton's Method radio button to true
		 */
		private void setNewtonRadBtn() {
			// make sure only 1 radio button is set when we're done
			clearAllKeplerEQRadioBtns();				// make sure only 1 radio button is set when we're done
			radbtnNewton.setSelected(true);
		}

		/**
		 * Sets the Simple Iteration radio button to true
		 */
		private void setSimpleRadBtn() {
			// make sure only 1 radio button is set when we're done
			clearAllKeplerEQRadioBtns();				// make sure only 1 radio button is set when we're done
			radbtnSimple.setSelected(true);
		}
	}
	
}
