package jll.celcalc.chap8;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCatalog;
import jll.celcalc.ASTUtils.ASTConstellation;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Data Files menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class DataFilesActions {

	/**
	 * Finds the constellation that an object is currently in.
	 */
	protected void findConstellationForObj() {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName;
		ASTCoord eclCoord;
		double RA, Decl;
		int idxObj, idxConst;
		String result = "";

		ChapGUI.clearTextAreas();

		idxObj = Misc.getValidObj(Misc.GETSUN,!Misc.GETEARTH,Misc.GETMOON);
		if (idxObj < 0) return;

		objName = orbElements.getOEObjName(idxObj);

		eclCoord = Misc.getSolarSysEqCoord(idxObj);
		RA = eclCoord.getRAAngle().getDecTime();
		Decl = eclCoord.getDeclAngle().getDecAngle();

		idxConst = ASTConstellation.findConstellationFromCoord(RA, Decl, ASTMisc.DEFAULT_EPOCH);

		if (idxConst < 0) ASTMsg.criticalErrMsg("Could not determine a constellation for the location entered.");
		else {
			if ((idxObj == orbElements.getOEDBSunIndex()) || (idxObj == orbElements.getOEDBMoonIndex())) result = "The ";
			result = result + objName + " at " + ASTTime.timeToStr(RA, ASTMisc.HMSFORMAT) + " RA, " + 
					ASTAngle.angleToStr(Decl, ASTMisc.DMSFORMAT) + " Decl is in the " + 
					ASTConstellation.getConstName(idxConst) + " (" + 
					ASTConstellation.getConstAbbrevName(idxConst) + ") constellation";
			ChapGUI.setResults(result);
		}
	}

	/**
	 * Finds the constellation that a given RA/Decl falls within
	 */
	protected void findConstellationForRA_Decl() {
		String str, result;
		int idx;

		if (ASTQuery.showQueryForm("Enter Right Ascension (hh:mm:ss.ss) for Epoch 2000.0",
				"Enter Declination (xxxd yym zz.zzs) for Epoch 2000.0") == ASTQuery.QUERY_OK) {

			// validate data
			str=ASTQuery.getData1(); // RA
			if ((str == null) || (str.length() <= 0)) return;
			ASTTime raObj = ASTTime.isValidTime(str,ASTMisc.HIDE_ERRORS);
			if (!raObj.isValidTimeObj()) {
				ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA");
				return;
			}

			str=ASTQuery.getData2(); // Decl
			if ((str == null) || (str.length() <= 0)) return;
			ASTAngle declObj = ASTAngle.isValidAngle(str,ASTMisc.HIDE_ERRORS);
			if (!declObj.isValidAngleObj()) {
				ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl");
				return;
			}

			ChapGUI.clearTextAreas();
			idx = ASTConstellation.findConstellationFromCoord(raObj.getDecTime(),declObj.getDecAngle(),ASTMisc.DEFAULT_EPOCH);

			if (idx < 0) ASTMsg.criticalErrMsg("Could not determine a constellation for the location entered.");
			else {
				result = "Location " + ASTTime.timeToStr(raObj, ASTMisc.HMSFORMAT) + " RA, " + 
						ASTAngle.angleToStr(declObj, ASTMisc.DMSFORMAT) + " Decl is in the " + 
						ASTConstellation.getConstName(idx) + " (" + ASTConstellation.getConstAbbrevName(idx) + ") constellation";
				ChapGUI.setResults(result);
			}
		}
	}

	/**
	 * Displays a list of all of the constellations.
	 */
	protected void listAllConstellations() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		ChapGUI.clearTextAreas();
		prt.setFixedWidthFont();
		ASTConstellation.displayAllConstellations();
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Shows all catalog information, including space objects,
	 * in the currently loaded catalog.
	 */
	protected void listAllObjsInCatalog() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double currentmVFilter = ChapGUI.getcurrentmVFilter();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}

		ChapGUI.clearTextAreas();
		ASTCatalog.displayCatalogInfo();
		prt.println();
		prt.setBoldFont(true);
		prt.println("Only Those Objects at Least as Bright as mV = " + String.format(ASTStyle.mVFormat,currentmVFilter) +
				" are listed", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.setFixedWidthFont();
		prt.println(String.format("%80s", "*").replace(' ', '*'));	
		ASTCatalog.displayAllCatalogObjects(currentmVFilter);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Shows all space objects in the currently loaded catalog
	 * that fall within a user-specified constellation. The
	 * catalog must already be grouped by constellation.
	 */
	protected void listAllObjsInConstellation() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String constAbbrevName;
		int idx;
		double currentmVFilter = ChapGUI.getcurrentmVFilter();

		if (!ASTCatalog.isCatalogLoaded()) {
			ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
			return;
		}

		if (ASTQuery.showQueryForm("Enter Constellation's 3 Character\nAbbreviated Name") == ASTQuery.QUERY_OK) {
			ChapGUI.clearTextAreas();
			constAbbrevName = ASTQuery.getData1();
			if ((constAbbrevName == null) || (constAbbrevName.length() <= 0)) return;
			idx = ASTConstellation.findConstellationByAbbrvName(constAbbrevName.trim());
			if (idx < 0) prt.println("No Constellation whose abbreviated name is '" + constAbbrevName + "' was found");
			else {
				prt.setBoldFont(true);
				prt.println("Only Those Objects at Least as Bright as mV = " + String.format(ASTStyle.mVFormat,currentmVFilter) +
						" are listed", ASTPrt.CENTERTXT);
				prt.setBoldFont(false);
				prt.setFixedWidthFont();
				ASTCatalog.displayAllObjsByConstellation(idx,ASTMisc.ASCENDING_ORDER,currentmVFilter);		
				prt.setProportionalFont();

			}
			prt.resetCursor();
		}
	}

	/**
	 * Loads a star catalog from disk from disk
	 */
	protected void loadCatalog() {	
		ASTPrt prt = ChapGUI.getPrtInstance();
		String fullFilename;

		ChapGUI.clearTextAreas();
		if (ASTCatalog.isCatalogLoaded()) {
			if (ASTMsg.pleaseConfirm("A catalog is already loaded. Are you\nsure you want to load a new one?","Clear Catalog Data")) {
				ASTCatalog.clearCatalogAndSpaceObjects();
				ChapGUI.setFilename("");
				ChapGUI.setCatalogType("");
				ChapGUI.setEpoch(ASTMisc.DEFAULT_EPOCH);
				prt.println("The currently loaded star catalog was cleared ...");
			} else {
				prt.println("The currently loaded star catalog was not cleared ...");
				return;
			}			
		}

		fullFilename = ASTCatalog.getCatFileToOpen();
		if ((fullFilename == null) || (fullFilename.length() <= 0)) return;

		ChapGUI.setFilename(fullFilename);

		if (ASTCatalog.loadFormattedStarCatalog(fullFilename)) {
			prt.println(String.format("Read in %d different Constellations with a total of %d Objects",
					ASTCatalog.getCatNumConst(),ASTCatalog.getCatNumObjs()));
			ChapGUI.setCatalogType(ASTCatalog.getCatType());
			ChapGUI.setEpoch(ASTCatalog.getCatEpoch());
		} else {			
			ASTMsg.errMsg("Could not load the catalog data from " + fullFilename, "Catalog Load Failed");
			ChapGUI.setFilename("");
			ChapGUI.setCatalogType("");
			ChapGUI.setEpoch(ASTMisc.DEFAULT_EPOCH);
		}

		prt.resetCursor();
	}

	/**
	 * Load orbital elements from disk
	 */
	protected void loadOrbitalElements() {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		ASTPrt prt = ChapGUI.getPrtInstance();
		String fullFilename = "";

		ChapGUI.clearTextAreas();

		if (ChapGUI.checkOEDBLoaded(ASTMisc.HIDE_ERRORS)) {
			if (!ASTMsg.pleaseConfirm("Orbital Elements have already been loaded. Are you\n" +
					"sure you want to load a new set of orbital elements?", "Clear Orbital Elements Data")) {
				prt.println("The currently loaded orbital elements were not cleared ...");
				return;
			}
		}

		fullFilename = ASTOrbits.getOEDBFileToOpen();
		if ((fullFilename == null) || (fullFilename.length() <= 0)) {
			prt.println("The currently loaded orbital elements were not cleared ...");
			return;
		}

		if (orbElements.loadOEDB(fullFilename)) {
			prt.println("New orbital elements have been successfully loaded ...");
			ChapGUI.setEpoch(orbElements.getOEEpochDate());
		} else {
			prt.println("The currently loaded orbital elements were cleared, but");
			prt.println("new orbital elements could not be loaded from the selected file.");
			prt.println("Please try again ...");
		}

		prt.resetCursor();
	}

	/**
	 * Shows all of the orbital elements and other data from whatever
	 * orbital elements database is currently loaded.
	 */
	protected void showAllOrbitalElements() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ChapGUI.checkOEDBLoaded()) return;

		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();

		ChapGUI.clearTextAreas();
		orbElements.displayAllOrbitalElements();
		prt.resetCursor();
	}

	/**
	 * Shows the orbital elements and other data for an object from
	 * whatever orbital elements database is currently loaded.
	 */
	protected void showObjOrbitalElements() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		int idx;
		String objName;

		if (!ChapGUI.checkOEDBLoaded()) return;

		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();

		if (ASTQuery.showQueryForm("Enter Object's Name") != ASTQuery.QUERY_OK) return;

		// validate data
		objName = ASTQuery.getData1();
		if ((objName == null) || (objName.length() <= 0)) return;
		idx = orbElements.findOrbElementObjIndex(objName.trim());
		if (idx < 0) {
			ASTMsg.errMsg("No Object with the name '" + objName + "' was found","Invalid Object Name");
			return;
		}

		ChapGUI.clearTextAreas();
		orbElements.displayObjOrbElements(idx);
		prt.resetCursor();
	}

	/**
	 * Shows the catalog information from the currently loaded catalog.
	 */
	protected void showCatalogInfo() {
		ASTPrt prt = ChapGUI.getPrtInstance();

		if (!ASTCatalog.isCatalogLoaded()) ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded");
		else {
			ChapGUI.clearTextAreas();
			ASTCatalog.displayCatalogInfo();
			prt.resetCursor();
		}
	}

}
