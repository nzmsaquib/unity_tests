package jll.celcalc.chap8;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStr;

/**
 * <b>Provides some miscellaneous data and methods related to the planets</b>
 * <p>
 * The methods and data here are static so that an object instance
 * does not have to be created before use the methods/data
 * in this class
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class Planets {

	/*------------------------------------------------------------------------------------
	 * Define a class for data about the planets for computing perihelion/aphelion															
	 *-----------------------------------------------------------------------------------*/
	private static class PlanetObj {		// must be static so that we don't have to have a Planets instance
		private String sName;				// the planet's name
		private double k0;					// see book for explanation of k0, k1, j0, j1, j2
		private double k1;
		private double j0;
		private double j1;
		private double j2;
		
		/**
		 * Constructor for creating a planet object
		 */
		private PlanetObj(String name,double k0,double k1, double j0, double j1, double j2) {
			this.sName = name;
			this.k0 = k0;
			this.k1 = k1;
			this.j0 = j0;
			this.j1 = j1;
			this.j2 = j2;
		}
	}
	
	private static final int NUM_PLANETS = 8;
	private static PlanetObj planetsTable[] = {
		new PlanetObj("Mercury", 4.15201, 2000.12, 2451590.257, 87.96934963, 0.0),
		new PlanetObj("Venus", 1.62549, 2000.53, 2451738.233, 224.7008188, -0.0000000327),
		new PlanetObj("Earth", 0.99997, 2000.01, 2451547.507, 365.2596358, 0.0000000156),
		new PlanetObj("Mars", 0.53166, 2001.78, 2452195.026, 686.9957857, -0.0000001187),
		new PlanetObj("Jupiter", 0.0843, 2011.2, 2455636.936, 4332.897065, 0.0001367),
		new PlanetObj("Saturn", 0.03393, 2003.52, 2452830.12, 10764.21676, 0.000827),
		new PlanetObj("Uranus", 0.0119, 2051.1, 2470213.5, 30694.8767, -0.00541),
		new PlanetObj("Neptune", 0.00607, 2047.5, 2468895.1, 60190.33, 0.03429)};
	
	
	/*===========================================================================
	 * Define 'getters' for the planetsTable. In each case, an index is passed
	 * indicating which planet is of interest. No checks are made to be sure
	 * the index is valid, so caller beware!
	 *===========================================================================*/
	
	/**
	 * Gets a planet's name.
	 * 
	 * @param i		index into planetsTable
	 * @return		planet's name
	 */
	protected static String getPlanetName(int i) {
		return planetsTable[i].sName;
	}
	/**
	 * Gets the k0 parameter for interpolating to get perihelion/aphelion.
	 * 
	 * @param i		index into planetsTable
	 * @return		k0 parameter
	 */
	protected static double getk0(int i) {
		return planetsTable[i].k0;
	}
	/**
	 * Gets the k1 parameter for interpolating to get perihelion/aphelion.
	 * 
	 * @param i		index into planetsTable
	 * @return		k1 parameter
	 */
	protected static double getk1(int i) {
		return planetsTable[i].k1;
	}
	/**
	 * Gets the j0 parameter for interpolating to get perihelion/aphelion.
	 * 
	 * @param i		index into planetsTable
	 * @return		j0 parameter
	 */
	protected static double getj0(int i) {
		return planetsTable[i].j0;
	}
	/**
	 * Gets the j1 parameter for interpolating to get perihelion/aphelion.
	 * 
	 * @param i		index into planetsTable
	 * @return		j1 parameter
	 */
	protected static double getj1(int i) {
		return planetsTable[i].j1;
	}
	/**
	 * Gets the j2 parameter for interpolating to get perihelion/aphelion.
	 * 
	 * @param i		index into planetsTable
	 * @return		j2 parameter
	 */
	protected static double getj2(int i) {
		return planetsTable[i].j2;
	}
	/**
	 * Gets the number of planets in the planetsTable.
	 * 
	 * @return		number of planets in the table
	 */
	protected static int getNumPlanets() {
		return NUM_PLANETS;
	}
	
	/*==============================================================================
	 * Various planet-related routines for the algorithms in this chapter's program
	 *=============================================================================*/
	
	/**
	 * Find the index into the planetsTable for the given name.
	 * 
	 * @param sName			name of the planet to find
	 * @return				index into planetsTable for the planet
	 * 						or -1 if the given name doesn't exist
	 */
	protected static int findPlanetIdx(String sName) {
		String s = ASTStr.removeWhitespace(sName);
		
		for (int i = 0; i < NUM_PLANETS; i++) {
			if (s.compareToIgnoreCase(planetsTable[i].sName) == 0) return i;
		}
		
		return -1;		// if we get here, the passed in sName was not found
	}

	/**
	 * Ask user for a planet's name and return its index from the currently
	 * loaded orbital elements. Pluto is **not** considered to be a planet in this context.
	 * 
	 * @return				-1 if object is invalid or not allowed, else returns index
	 * 						into the orbital elements array.
	 */
	protected static int getValidPlanet() {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		int idx = -1;
		String objName;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return -1;
		if (!ChapGUI.checkOEDBLoaded()) return -1;

		if (ASTQuery.showQueryForm("Enter Object's Name (must be a planet!)") != ASTQuery.QUERY_OK) return -1;

		// validate data
		objName = ASTQuery.getData1();
		idx = orbElements.findOrbElementObjIndex(objName.trim());
		if (idx < 0) {
			ASTMsg.errMsg("No Planet with the name '" + objName + "' was found","Invalid Planet Name");
			return -1;
		}

		// This only works for the planets, so make sure the user specified a planet
		// The orbital elements file could have non-planets in it (e.g, Pluto, Ceres)
		if (!isAPlanet(idx)) {
			ASTMsg.errMsg("This method only works for Solar System planets\n" +
					"(Remember, Pluto is not a planet)", "Invalid Choice");
			return -1;
		}

		return idx;

	}
	
	/*---------------------------------------------------------------------
	 * Private methods used only in this class
	 *--------------------------------------------------------------------*/

	/**
	 * Determines whether an object is a planet. This method
	 * does **not** count Pluto as a planet.
	 * 
	 * @param idx			index into orbital elements for the object
	 * 						under consideration
	 * @return				true if the object is a planet in the currently
	 * 						loaded orbital elements.
	 */

	private static boolean isAPlanet(int idx) {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		
        if (idx == orbElements.findOrbElementObjIndex("Mercury")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Venus")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Earth")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Mars")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Jupiter")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Saturn")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Uranus")) return true;
        else if (idx == orbElements.findOrbElementObjIndex("Neptune")) return true;
        
        return false;
	}

}
