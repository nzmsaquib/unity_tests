package jll.celcalc.chap8;

import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Provides some miscellaneous helper methods used throughout the
 * Solar System chapter</b>
 * <p>
 * The methods and data here are static so that an object instance
 * does not have to be created before use the methods/data
 * in this class
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class Misc {

	/** Set a default termination criteria (in radians) for solving Kepler's equation */
	protected static double termCriteria = 0.000002;
	
    // These constants are used purely to make code more readable when
    // asking the user for a Solar System object
	/** whether to ask user for the Sun */
    protected static final boolean GETSUN = true;
    /** whether to ask user for the Earth */
    protected static final boolean GETEARTH = true;
    /** whether to ask user for the Moon */
    protected static final boolean GETMOON = true;
	
	/**
	 * Calculate miscellaneous data items about an object. This
	 * method is intended to be called twice: 1st to show how the
	 * calculations are done (if user wants to see intermediate
	 * results) and then to do a summary. It is done this way
	 * because mixing the summary and details makes for a
	 * messy combination of summary and details for the user
	 * to weed through.
	 * 
	 * @param idx			which object is of interest
	 * @param summary		true if this is the summary pass
	 */
	protected static void calcMiscData(int idx, boolean summary) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		String objName;
		int iTmp;
		int idxSun = orbElements.getOEDBSunIndex();
		int idxMoon = orbElements.getOEDBMoonIndex();
		int idxEarth = orbElements.getOEDBEarthIndex();
		double m_p, r_p, m_e, r_e, dDist, dT, L_p, L_e, dTmp, Ecc_p, a_p, Tp;
		double RVL_p, RVL_e, RVL_x;				// radius vector length for object and Earth
		double[] eclCoord;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		
		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		// Note that if idx = idxEarth, the "_p" variables are actually the Earth
		objName = orbElements.getOEObjName(idx);
		Ecc_p = orbElements.getOEObjEccentricity(idx);
		a_p = orbElements.getOEObjSemiMajAxisAU(idx);
		Tp = orbElements.getOEObjPeriod(idx);
		m_p = orbElements.getOEObjMass(idx);
		r_p = orbElements.getOEObjRadius(idx);
		RVL_p = 1.0;	// This is actually calculated below, but initialized here to avoid compiler warning
		m_e = orbElements.getOEObjMass(idxEarth);
		r_e = orbElements.getOEObjRadius(idxEarth);
		RVL_e = orbElements.calcEarthRVLength(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);

		if (summary) {
			prt.setBoldFont(true);
			prt.println("Summary of miscellaneous data items for " + objName, ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();
		}

		prt.setFixedWidthFont();

		// Calculate weight on any object except the Earth
		if (idx != idxEarth) {
			ChapGUI.printlnCond("**************************************");
			dT = (m_p * r_e*r_e) / (r_p*r_p * m_e);
			ChapGUI.printCond("Calculate the weight of an object on ");
			if ((idx == idxSun) || (idx == idxMoon)) ChapGUI.printCond("the ");
			ChapGUI.printlnCond(objName);
			ChapGUI.printlnCond("  Weight = (m_p*r_e^2)/(r^p*m_e) = (" + String.format(ASTStyle.genFloatFormat,m_p) + " * " +
					String.format(ASTStyle.genFloatFormat,r_e) + "^2) / (" + String.format(ASTStyle.genFloatFormat,r_p) + "^2 * " +
					String.format(ASTStyle.genFloatFormat,m_e) + ")");
			ChapGUI.printlnCond("         = " + String.format(ASTStyle.genFloatFormat,dT));
			ChapGUI.printlnCond();
			if (summary) prt.println("Weight on " + objName + ": Wp = " + String.format(ASTStyle.genFloatFormat,dT) + " * We");
		}

		// Calculate transmissions delays for any object except the Sun, Earth, or Moon
		if ((idx != idxEarth) && (idx != idxSun) && (idx != idxMoon)) {
			ChapGUI.printlnCond("**************************************");
			// Find the object's location and then its distance.
			eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
					observer.getObsDate().getYear(), 0.0, idx, solveTrueAnomaly, termCriteria);
			L_p = eclCoord[ASTOrbits.OBJ_HELIOLAT];
			RVL_p = eclCoord[ASTOrbits.OBJ_RADIUSVECT];
			L_e = eclCoord[ASTOrbits.EARTH_HELIOLAT];
			RVL_e = eclCoord[ASTOrbits.EARTH_RADIUSVECT];
			dDist = ASTOrbits.calcObjDistToEarth(RVL_e, L_e, RVL_p, L_p);
			dT = 0.1384 * dDist;
			ChapGUI.printCond("Calculate the transmission delay between ");
			if (idx == idxSun) ChapGUI.printCond("the ");
			ChapGUI.printlnCond(objName + " and the Earth.");
			ChapGUI.printlnCond("  Delay = 0.1384*Dist = 0.1384*" + String.format(ASTStyle.genFloatFormat,dDist));
			ChapGUI.printlnCond("        = " + ASTTime.timeToStr(dT, ASTMisc.DECFORMAT) + " hours");
			ChapGUI.printlnCond();
			if (summary) prt.println("Transmission delay: " + ASTTime.timeToStr(dT,ASTMisc.HMSFORMAT));
		}

		// Find length of a year relative to Earth for any object except the Earth, Moon, or Sun
		if ((idx != idxEarth) && (idx != idxMoon) && (idx != idxSun)) {
			ChapGUI.printlnCond("**************************************");
			dT = 365.242191 * Tp;
			ChapGUI.printlnCond("Calculate length of a year on " + objName + " relative to Earth");
			ChapGUI.printlnCond("  Year = 365.242191*Tp = 365.242191 * " + Tp);
			ChapGUI.printlnCond("       = " + ASTStr.insertCommas(dT) + " Earth days");
			ChapGUI.printlnCond();
			if (summary) {
				dT = dT / 365.25;							// convert to years
				iTmp = ASTMath.Trunc(dT);
				prt.print("Length of Year: " + iTmp + " years, ");
				dTmp = (dT - iTmp) * 365.25;				// days
				iTmp = ASTMath.Trunc(dTmp);
				prt.print(iTmp + " days, ");
				dTmp = (dTmp - iTmp) * 24;				// hours;
				prt.println(ASTTime.timeToStr(dTmp, ASTMisc.HMSFORMAT));
			}
		}

		// Calculate orbital period given the semi-major axis
		ChapGUI.printlnCond("**************************************");
		dT = Math.sqrt(Math.pow(a_p,3));
		ChapGUI.printCond("Calculate the orbital period for ");
		if ((idx == idxSun) || (idx == idxMoon)) ChapGUI.printCond("the ");
		ChapGUI.printlnCond(objName + " given its semi-major axis in AUs.");
		ChapGUI.printlnCond("  Tp = sqrt(a_p^3) = sqrt(" + String.format(ASTStyle.genFloatFormat,a_p) + "^3)");
		ChapGUI.printlnCond("     = " + String.format(ASTStyle.genFloatFormat,dT) + " tropical years");
		ChapGUI.printlnCond();
		if (summary) prt.println("Orbital period calculated from semi-major axis: " +
				String.format(ASTStyle.genFloatFormat,dT) + " tropical years");

		// Calculate the semi-major axis given the orbital period for all but the Sun and Moon
		if ((idx != idxMoon) && (idx != idxSun)) {
			ChapGUI.printlnCond("**************************************");
			dT = ASTMath.cuberoot(Tp * Tp);
			ChapGUI.printlnCond("Calculate the semi-major axis for " + objName);
			ChapGUI.printlnCond("given its orbital period in tropical years.");
			ChapGUI.printlnCond("  a_p = ASTMath.cuberoot(Tp^2) = ASTMath.cuberoot(" + String.format(ASTStyle.genFloatFormat,Tp) + "^2)");
			ChapGUI.printlnCond("      = " + String.format(ASTStyle.genFloatFormat,dT) + " AUs");
			ChapGUI.printlnCond();
			if (summary) prt.println("Semi-major axis calculated from orbital period: " +
					String.format(ASTStyle.genFloatFormat,dT) + " AUs");
		}

		// Calculate orbital velocities for all except the Sun and Moon
		if ((idx != idxSun) && (idx != idxMoon)) {
			ChapGUI.printlnCond("**************************************");
			dTmp = orbElements.getOEObjGravParm(idxSun);
			dT = dTmp * (1 + Ecc_p) / (a_p * (1 - Ecc_p) * 150000000.0);
			dT = Math.sqrt(dT);
			ChapGUI.printlnCond("Calculate the velocity at perihelion for " + objName);
			ChapGUI.printlnCond("  Vper = sqrt[(Mu_sun*(1+eccentricity_p))/(a_p*(1-eccentricity_p)*1.5E08)]");
			ChapGUI.printlnCond("       = sqrt[(" + String.format(ASTStyle.genFloatFormat,dTmp) + "*(1+" + 
					String.format(ASTStyle.genFloatFormat,Ecc_p) + "))/(" +	String.format(ASTStyle.genFloatFormat,a_p) +
					"*(1-" + String.format(ASTStyle.genFloatFormat,Ecc_p) + ")*1.5E08)]");
			ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dT) + " km/s");
			ChapGUI.printlnCond();
			if (summary) prt.println("Vper = " + ASTMath.Round(dT, 2) + " km/s (" + ASTMath.Round(ASTMath.KM2Miles(dT), 2) + " miles/s)");

			dT = 29.865958 * ((a_p * Math.sqrt(1 - Ecc_p*Ecc_p)) / Tp);
			ChapGUI.printlnCond("Calculate the average orbital velocity for " + objName);
			ChapGUI.printlnCond("  Vavg = 29.86598*[(a_p*sqrt(1-eccentricity_p^2)/Tp]");
			ChapGUI.printlnCond("       = 29.86598*[(" + String.format(ASTStyle.genFloatFormat,a_p) + "*sqrt(1-" +
					String.format(ASTStyle.genFloatFormat,Ecc_p) + "^2)/(" + String.format(ASTStyle.genFloatFormat,Tp) + "]");
			ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dT) + " km/s");
			ChapGUI.printlnCond();
			if (summary) prt.println("Vavg = " + ASTMath.Round(dT, 2) + " km/s (" + ASTMath.Round(ASTMath.KM2Miles(dT), 2) + " miles/s)");

			dT = dTmp * (1 - Ecc_p) / (a_p * (1 + Ecc_p) * 150000000.0);
			dT = Math.sqrt(dT);
			ChapGUI.printlnCond("Calculate the velocity at aphelion for " + objName);
			ChapGUI.printlnCond("  Vaph = sqrt[(Mu_sun*(1-eccentricity_p))/(a_p*(1+eccentricity_p)*1.5E08)]");
			ChapGUI.printlnCond("       = sqrt[(" + String.format(ASTStyle.genFloatFormat,dTmp) + "*(1-" + 
					String.format(ASTStyle.genFloatFormat,Ecc_p) + "))/(" +	String.format(ASTStyle.genFloatFormat,a_p) +
					"*(1+" + String.format(ASTStyle.genFloatFormat,Ecc_p) + ")*1.5E08)]");
			ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat,dT) + " km/s");
			ChapGUI.printlnCond();
			if (summary) prt.println("Vaph = " + ASTMath.Round(dT, 2) + " km/s (" + ASTMath.Round(ASTMath.KM2Miles(dT), 2) + " miles/s)");

			if (idx == idxEarth) RVL_x = RVL_e;
			else RVL_x = RVL_p;
			dT = (dTmp / 150000000.0) * ((2 / RVL_x) - (1 / a_p));
			dT = Math.sqrt(dT);
			ChapGUI.printlnCond("Calculate the velocity for " + objName + " for the current observer's date");
			ChapGUI.printlnCond("  Vdate = sqrt[(Mu_sun/1.5E8)*((2/Rp) - (1/a_p))]");
			ChapGUI.printlnCond("        = sqrt[(" + String.format(ASTStyle.genFloatFormat,dTmp) + "/1.5E8)*((2/" + 
					String.format(ASTStyle.genFloatFormat,RVL_x) + ") - (1/" + String.format(ASTStyle.genFloatFormat,a_p) + "))]");
			ChapGUI.printlnCond("        = " + String.format(ASTStyle.genFloatFormat,dT) + " km/s");
			if (summary) prt.println("Vdate = " + ASTMath.Round(dT, 2) + " km/s (" + ASTMath.Round(ASTMath.KM2Miles(dT), 2) + 
					" miles/s)" + " Date is " +	ASTDate.dateToStr(observer.getObsDate()));
		}

		// Calculate the escape velocity
		ChapGUI.printlnCond("**************************************");
		dT = orbElements.getOEObjGravParm(idx);
		ChapGUI.printCond("Calculate the escape velocity for ");
		if ((idx == idxSun) || (idx == idxMoon)) ChapGUI.printCond("the ");
		ChapGUI.printlnCond(objName);
		ChapGUI.printlnCond("  Vescape = sqrt[(2*Mu_p)/(r_p)] = sqrt[(2*" + String.format(ASTStyle.genFloatFormat,dT) + ")/(" +
				String.format(ASTStyle.genFloatFormat,r_p) + ")]");
		dT = Math.sqrt(2 * dT / r_p);
		ChapGUI.printlnCond("          = " + String.format(ASTStyle.genFloatFormat,dT) + " km/s");
		ChapGUI.printlnCond();
		if (summary) prt.println("Vescape = " + ASTMath.Round(dT, 2) + " km/s (" + ASTMath.Round(ASTMath.KM2Miles(dT), 2) + " miles/s)");

	}
	
	/**
	 * This method computes the LST for the observer's current location
	 * and time. This is needed in order to convert equatorial coordinates
	 * to horizon coordinates. The observer's location is retrieved
	 * from the GUI.
	 * 
	 * @return			LST for the current observer data that's in the GUI
	 * 
	 * NOTE: It is assumed that the observer's location/time has already
	 * been validated prior to this method being invoked.
	 */
	protected static double computeLST() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		double LCT, UT, GST, LST;
		ASTInt dateAdjustObj = new ASTInt();
		int dateAdjust;

		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);

		LST = ASTTime.GSTtoLST(GST, observer.getObsLon());
		return LST;
	}
	
	/**
	 * Get the equatorial coordinates for a solar system object
	 * for the current observer.
	 * 
	 * @param idx			index into orbital elements for the desired object,
	 * 						which cannot be the Earth but can be any other
	 * 						object in the orbital elements data file
	 * @return				returns the equatorial coordinates for the object.
	 */
	protected static ASTCoord getSolarSysEqCoord(int idx) {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		double[] eclCoord;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double UT, LCT;
		int dateAdjust;
		ASTInt dateAdjustObj = new ASTInt();
		double EclLat, EclLon;

		solveTrueAnomaly = ChapGUI.getTrueAnomalyRBStatus();

		// Get the observer's UT time
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(), dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();

		// adjust the date, if needed, since the LCTtoUT conversion could have changed the date
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);

		if (idx == orbElements.getOEDBSunIndex()) {
			eclCoord = orbElements.calcSunEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),
					adjustedDate.getYear(),UT, solveTrueAnomaly, termCriteria);
			EclLat = eclCoord[ASTOrbits.SUN_ECLLAT];
			EclLon = eclCoord[ASTOrbits.SUN_ECLLON];
		} else if (idx == orbElements.getOEDBMoonIndex()) {
			eclCoord = orbElements.calcMoonEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),
					adjustedDate.getYear(), UT, solveTrueAnomaly, termCriteria);
			EclLat = eclCoord[ASTOrbits.MOON_ECLLAT];
			EclLon = eclCoord[ASTOrbits.MOON_ECLLON];
		} else {
			eclCoord = orbElements.calcObjEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),
					adjustedDate.getYear(), UT, idx, solveTrueAnomaly, termCriteria);
			EclLat = eclCoord[ASTOrbits.OBJ_ECLLAT];
			EclLon = eclCoord[ASTOrbits.OBJ_ECLLON];
		}

		return ASTCoord.EclipticToEquatorial(EclLat, EclLon, orbElements.getOEEpochDate());
	}

	/**
	 * Ask user for a Solar System object's name and return its index.
	 * 
	 * @param Sun		true if the Sun is allowed
	 * @param Earth		true if the Earth is allowed
	 * @param Moon		true if the Moon is allowed
	 * @return			-1 if object is invalid or not allowed, else returns index
	 * 					into the orbital elements array.
	 */
	protected static int getValidObj(boolean Sun, boolean Earth, boolean Moon) {
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		int idx = -1;
		String objName;
		String exclusions = "";

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return -1;
		if (!ChapGUI.checkOEDBLoaded()) return -1;
		
		// See if any objects are to be excluded
        if (!Sun || !Moon || !Earth) {
    	  if (!Sun) exclusions = "Sun";
    	  if (!Earth) {
    		  if (exclusions.length() > 0) exclusions = exclusions + ", Earth";
    		  else exclusions = "Earth";
    	  }
    	  if (!Moon) {
    		  if (exclusions.length() > 0) exclusions = exclusions + ", Moon";
    		  else exclusions = "Moon";
    	  }
          exclusions = "(cannot choose " + exclusions + ")";
        } else exclusions = "(any Solar System object can be used)";
      
		if (ASTQuery.showQueryForm("Enter Solar System object's name\n" + exclusions) != ASTQuery.QUERY_OK) return -1;

		// validate data
		objName = ASTQuery.getData1();
		if ((objName == null) || (objName.length() <= 0)) return -1;
		idx = orbElements.findOrbElementObjIndex(objName.trim());
		if (idx < 0) {
			ASTMsg.errMsg("No Object with the name '" + objName + "' was found","Invalid Object Name");
			return -1;
		}

		if ((idx == orbElements.getOEDBSunIndex()) && !Sun) {
			ASTMsg.errMsg("Cannot choose the Sun", "Invalid Choice");
			return -1;
		} else if ((idx == orbElements.getOEDBEarthIndex()) && !Earth) {
			ASTMsg.errMsg("Cannot choose the Earth", "Invalid Choice");
			return -1;
		} else if ((idx == orbElements.getOEDBMoonIndex()) && !Moon) {
			ASTMsg.errMsg("Cannot choose the Moon", "Invalid Choice");
			return -1;
		}

		return idx;
	}
	
}
