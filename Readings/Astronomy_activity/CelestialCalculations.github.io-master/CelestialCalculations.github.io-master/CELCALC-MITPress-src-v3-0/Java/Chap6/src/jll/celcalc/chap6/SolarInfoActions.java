package jll.celcalc.chap6;

import jll.celcalc.ASTUtils.ASTAngle;
import jll.celcalc.ASTUtils.ASTCoord;
import jll.celcalc.ASTUtils.ASTDate;
import jll.celcalc.ASTUtils.ASTInt;
import jll.celcalc.ASTUtils.ASTKepler;
import jll.celcalc.ASTUtils.ASTMath;
import jll.celcalc.ASTUtils.ASTMisc;
import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTObserver;
import jll.celcalc.ASTUtils.ASTOrbits;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.ASTUtils.ASTQuery;
import jll.celcalc.ASTUtils.ASTReal;
import jll.celcalc.ASTUtils.ASTStr;
import jll.celcalc.ASTUtils.ASTStyle;
import jll.celcalc.ASTUtils.ASTTime;

/**
 * <b>Handles the Solar Info menu items</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */

class SolarInfoActions {

	// Set a default termination criteria (in radians) for solving Kepler's
	// equation
	private static double termCriteria = 0.000002;

	/**
	 * Calculate the Sun's distance and angular diameter.
	 */
	protected void calcDistAndAngDiameter() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		int idxSun;
		double[] eclCoord; 						// entries defined by calcSunEclipticCoord
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double Vsun, dF, Ecc, dDist, dAngDiameter;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		// Get Sun index after validating that orbital elements are loaded
		idxSun = orbElements.getOEDBSunIndex();

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Sun's Distance and Angular Diameter for the Current Date", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;
		Ecc = orbElements.getOEObjEccentricity(idxSun);

		// Technically, we should be using the UT for the observer rather than
		// UT=0, but the difference is so small that it isn't worth converting LCT to UT
		eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Vsun = eclCoord[ASTOrbits.SUN_TRUEANOM];
		ChapGUI.printlnCond("1.  Compute the Sun's true anomaly at 0 hours UT.");
		ChapGUI.printlnCond("    Vsun = " + ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dF = (1 + Ecc * ASTMath.COS_D(Vsun)) / (1 - Ecc * Ecc);
		ChapGUI.printlnCond("2.  Compute F = [1 + eccentricity*cos(Vsun)] / [1-eccentricity^2]");
		ChapGUI.printlnCond("              = [1 + " + String.format(ASTStyle.genFloatFormat, Ecc) + "*cos("
				+ ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + ")] / [1 - "
				+ String.format(ASTStyle.genFloatFormat, Ecc) + "^2]");
		ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat, dF));
		ChapGUI.printlnCond();

		dDist = orbElements.getOEObjSemiMajAxisKM(idxSun) / dF;
		ChapGUI.printlnCond("3.  Compute the distance to the Sun.");
		ChapGUI.printlnCond("    Dist = a0/F where a0 is the length of the Sun's semi-major axis in km");
		ChapGUI.printlnCond("    Dist = (" + ASTStr.insertCommas(orbElements.getOEObjSemiMajAxisKM(idxSun)) + ")/"
				+ String.format(ASTStyle.genFloatFormat, dF));
		ChapGUI.printlnCond("         = " + ASTStr.insertCommas(dDist) + " km");
		ChapGUI.printlnCond();

		dAngDiameter = orbElements.getOEObjAngDiamDeg(idxSun) * dF;
		ChapGUI.printlnCond("4.  Compute the Sun's angular diameter.");
		ChapGUI.printlnCond("    Theta_sun = Theta_0*F where Theta_0 is the Sun's angular diameter in degrees");
		ChapGUI.printlnCond("    when the Sun is distance a0 away.");
		ChapGUI.printlnCond("    Theta_sun = " + ASTAngle.angleToStr(orbElements.getOEObjAngDiamDeg(idxSun), ASTMisc.DECFORMAT)
				+ "*" + String.format(ASTStyle.genFloatFormat, dF) + " = "
				+ ASTAngle.angleToStr(dAngDiameter, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dDist = ASTMath.Round(dDist, 2);
		ChapGUI.printlnCond("5.  Convert distance to miles and angular diameter to DMS format.");
		ChapGUI.printlnCond("    Dist = " + ASTStr.insertCommas(dDist) + " km = "
				+ ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist), 2)) + " miles");
		ChapGUI.printlnCond("    Theta_sun = " + ASTAngle.angleToStr(dAngDiameter, ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("On " + ASTDate.dateToStr(observer.getObsDate()) + ", the Sun is/was "
				+ ASTStr.insertCommas(dDist) + " km away");
		ChapGUI.printlnCond("and its angular diameter is/was " + ASTAngle.angleToStr(dAngDiameter, ASTMisc.DMSFORMAT));

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("Sun's Distance: " + ASTStr.insertCommas(dDist) + " km ("
				+ ASTStr.insertCommas(ASTMath.Round(ASTMath.KM2Miles(dDist), 2)) + " miles), Angular Diameter: "
				+ ASTAngle.angleToStr(dAngDiameter, ASTMisc.DMSFORMAT));
	}

	/**
	 * Calculate the equation of time.
	 */
	protected void calcEqOfTime() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		int idxSun;
		ASTKepler.TrueAnomalyType solveTrueAnomaly;
		double Msun, dE, y, Lsun, dT, Ecc;
		double[] eclCoord; 					// entries defined by calcSunEclipticCoord

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		// Get Sun index after validating that orbital elements are loaded
		idxSun = orbElements.getOEDBSunIndex();

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Equation of Time for " + ASTDate.dateToStr(observer.getObsDate()),ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;
		Ecc = orbElements.getOEObjEccentricity(idxSun);

		eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Lsun = eclCoord[ASTOrbits.SUN_ECLLON];
		Msun = eclCoord[ASTOrbits.SUN_MEANANOM];
		ChapGUI.printlnCond("1.  Compute the Sun's ecliptic longitude and mean anomaly for the given date.");
		ChapGUI.printlnCond("    Lsun = " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    Msun = " + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dE = ASTCoord.calcEclipticObliquity(observer.getObsDate().getYear());
		ChapGUI.printlnCond("2.  Compute the obliquity of the ecliptic for the given year.");
		ChapGUI.printlnCond("    obliq = " + ASTAngle.angleToStr(dE, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		y = ASTMath.TAN_D(dE / 2.0);
		y = y * y;
		ChapGUI.printlnCond("3.  Compute y = tan^2(obliq/2) = tan^2(" + ASTAngle.angleToStr(dE, ASTMisc.DECFORMAT) + "/2");
		ChapGUI.printlnCond("              = " + String.format(ASTStyle.genFloatFormat, y));
		ChapGUI.printlnCond();

		dT = y * ASTMath.SIN_D(2 * Lsun) - 2 * Ecc * ASTMath.SIN_D(Msun)
				+ 4 * Ecc * y * ASTMath.SIN_D(Msun) * ASTMath.COS_D(2 * Lsun);
		dT = dT - (y * y / 2.0) * ASTMath.SIN_D(4 * Lsun) - ((5 * Ecc * Ecc) / 4) * ASTMath.SIN_D(2 * Msun);
		ChapGUI.printlnCond("4.  Calculate a delta T correction in radians from the interpolation formula");
		ChapGUI.printlnCond("    dT = y*sin(2*Lsun) - 2*eccentricity*sin(Msun) + ");
		ChapGUI.printlnCond("         4*eccentricity*y*sin(Msun)*cos(2*Lsun) -");
		ChapGUI.printlnCond("         (y^2/2.0)*sin(4*Lsun) - (5/4)*(eccentricity^2)*sin(2*Msun)");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat, y) + "*sin(2*"
				+ ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + ") - 2*" + String.format(ASTStyle.genFloatFormat, Ecc)
				+ "*sin(" + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + ") + ");
		ChapGUI.printlnCond("         4*" + String.format(ASTStyle.genFloatFormat, Ecc) + "*"
				+ String.format(ASTStyle.genFloatFormat, y) + "sin(" + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT)
				+ ")*cos(2*" + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + ") - ");
		ChapGUI.printlnCond("         (" + String.format(ASTStyle.genFloatFormat, y) + "^2/2.0)*sin(4*"
				+ ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + ") - (5/4)*("
				+ String.format(ASTStyle.genFloatFormat, Ecc) + "^2)*sin(2*"
				+ ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat, dT) + " radians");
		ChapGUI.printlnCond();
		
		dT = -dT;
		ChapGUI.printlnCond("5.  Multiply by -1.");
		ChapGUI.printlnCond("    dT = -1.0*dT = " + String.format(ASTStyle.genFloatFormat, dT) + " radians");
		ChapGUI.printlnCond();

		dT = ASTMath.rad2deg(dT);
		ChapGUI.printlnCond("6.  Convert dT from radians to degrees.");
		ChapGUI.printlnCond("    dT = (180/Math.PI)*dT = " + ASTAngle.angleToStr(dT, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		dT = dT / 15.0;
		ChapGUI.printlnCond("7.  Convert dT from degrees to hours by dividing by 15.");
		ChapGUI.printlnCond("    dT = " + ASTTime.timeToStr(dT, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("8.  Convert dT to HMS format.");
		ChapGUI.printlnCond("    dT = " + ASTTime.timeToStr(dT, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("For the given date, the equation of time correction is " + ASTTime.timeToStr(dT, ASTMisc.HMSFORMAT));

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("For " + ASTDate.dateToStr(observer.getObsDate()) + ", the equation of time is "
				+ ASTTime.timeToStr(dT, ASTMisc.HMSFORMAT));
	}

	/**
	 * Calculate the times of the equinoxes and solstices.
	 */
	protected void calcEquinoxesSolstices() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		double dT, dT2, dT3, dY, JDm, JDj, JDs, JDd;
		double UTm, UTj, UTs, UTd;
		ASTDate marchDate = new ASTDate();
		ASTDate juneDate = new ASTDate();
		ASTDate septDate = new ASTDate();
		ASTDate decDate = new ASTDate();

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Equinoxes and Solstices for the Year", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		dY = observer.getObsDate().getYear();
		dT = dY / 1000.0;
		dT2 = dT * dT;
		dT3 = dT2 * dT;
		ChapGUI.printlnCond("1.  Calculate T = Year/1000 = " + dY + "/1000 = " + String.format(ASTStyle.genFloatFormat, dT));
		ChapGUI.printlnCond();

		JDm = 1721139.2855 + 365.2421376 * dY + 0.067919 * dT2 - 0.0027879 * dT3;
		JDj = 1721233.2486 + 365.2417284 * dY - 0.053018 * dT2 + 0.009332 * dT3;
		JDs = 1721325.6978 + 365.2425055 * dY - 0.126689 * dT2 + 0.0019401 * dT3;
		JDd = 1721414.392 + 365.2428898 * dY - 0.010965 * dT2 - 0.0084885 * dT3;
		ChapGUI.printlnCond("2.  Compute the Julian day numbers for the equinoxes and solstices.");
		ChapGUI.printlnCond("    JDm = 1721139.2855 + 365.2421376*Year + 0.067919*T^2 - 0.0027879*T^3");
		ChapGUI.printlnCond("        = 1721139.2855 + 365.2421376*" + dY + " + 0.067919*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^2 - 0.0027879*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^3");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.JDFormat, JDm));
		ChapGUI.printlnCond("    JDj = 1721233.2486 + 365.2417284*Year - 0.053018*T^2 + 0.009332*T^3");
		ChapGUI.printlnCond("        = 1721233.2486 + 365.2417284*" + dY + " - 0.053018*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^2 + 0.009332*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^3");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.JDFormat, JDj));
		ChapGUI.printlnCond("    JDs = 1721325.6978 + 365.2425055*Year - 0.126689*T^2 + 0.0019401*T^3");
		ChapGUI.printlnCond("        = 1721325.6978 + 365.2425055*" + dY + " - 0.126689*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^2 + 0.0019401*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^3");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.JDFormat, JDs));
		ChapGUI.printlnCond("    JDd = 1721414.392 + 365.2428898*Year - 0.010965*T^2 - 0.0084885*T^3");
		ChapGUI.printlnCond("        = 1721414.392 + 365.2428898*" + dY + " - 0.010965*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^2 - 0.0084885*("
				+ String.format(ASTStyle.genFloatFormat, dT) + ")^3");
		ChapGUI.printlnCond("        = " + String.format(ASTStyle.JDFormat, JDd));
		ChapGUI.printlnCond();

		marchDate = ASTDate.JDtoDate(JDm);
		juneDate = ASTDate.JDtoDate(JDj);
		septDate = ASTDate.JDtoDate(JDs);
		decDate = ASTDate.JDtoDate(JDd);
		ChapGUI.printlnCond("3.  Convert the Julian day numbers from the previous step to calendar dates.");
		ChapGUI.printlnCond("    JDm gives " + marchDate.getMonth() + "/"
				+ String.format(ASTStyle.genFloatFormat, marchDate.getdDay()) + "/" + marchDate.getYear());
		ChapGUI.printlnCond("    JDj gives " + juneDate.getMonth() + "/"
				+ String.format(ASTStyle.genFloatFormat, juneDate.getdDay()) + "/" + juneDate.getYear());
		ChapGUI.printlnCond("    JDs gives " + septDate.getMonth() + "/"
				+ String.format(ASTStyle.genFloatFormat, septDate.getdDay()) + "/" + septDate.getYear());
		ChapGUI.printlnCond("    JDd gives " + decDate.getMonth() + "/"
				+ String.format(ASTStyle.genFloatFormat, decDate.getdDay()) + "/" + decDate.getYear());
		ChapGUI.printlnCond();

		UTm = ASTMath.Frac(marchDate.getdDay()) * 24.0;
		UTj = ASTMath.Frac(juneDate.getdDay()) * 24.0;
		UTs = ASTMath.Frac(septDate.getdDay()) * 24.0;
		UTd = ASTMath.Frac(decDate.getdDay()) * 24.0;
		ChapGUI.printlnCond("4.  Convert the data part of the dates to a day and decimal UT.");
		ChapGUI.printlnCond(
				"    March: day " + marchDate.getiDay() + ", " + ASTTime.timeToStr(UTm, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond(
				"    June: day " + juneDate.getiDay() + ", " + ASTTime.timeToStr(UTj, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond(
				"    Sept: day " + septDate.getiDay() + ", " + ASTTime.timeToStr(UTs, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond(
				"    Dec: day " + decDate.getiDay() + ", " + ASTTime.timeToStr(UTd, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("5.  Convert the UT results to HMS format.");
		prt.println("    March equinox on " + ASTDate.dateToStr(marchDate) + " at "
				+ ASTTime.timeToStr(UTm, ASTMisc.HMSFORMAT) + " UT");
		prt.println("    June solstice on " + ASTDate.dateToStr(juneDate) + " at "
				+ ASTTime.timeToStr(UTj, ASTMisc.HMSFORMAT) + " UT");
		prt.println("    September equinox on " + ASTDate.dateToStr(septDate) + " at "
				+ ASTTime.timeToStr(UTs, ASTMisc.HMSFORMAT) + " UT");
		prt.println("    December equinox on " + ASTDate.dateToStr(decDate) + " at "
				+ ASTTime.timeToStr(UTd, ASTMisc.HMSFORMAT) + " UT");

		ChapGUI.setResults("Solstices and Equinoxes are listed in the text area below");
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate the position of the Sun, using the currently loaded orbital
	 * elements and the current observer position.
	 */
	protected void calcSunPosition() {
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		double[] tmpKepler;
		int idxSun;
		String result;
		double LCT, UT, LST, GST, JD, JDe;
		double De, Msun, Ec, Ea, Vsun, Lsun, Ecc;
		double Bsun = 0.0;
		ASTInt dateAdjustObj = new ASTInt();
		int dateAdjust;
		int iter = 0;
		ASTCoord eqCoord, horizonCoord;

		ASTPrt prtObj = null;
		ASTPrt prt = ChapGUI.getPrtInstance();

		// Set up whether to display interim results when solving Kepler's equation
		if (ChapGUI.getShowInterimCalcsStatus()) prtObj = prt;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		// Get Sun index after validating that orbital elements are loaded
		idxSun = orbElements.getOEDBSunIndex();

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Compute the Sun's Position for the Current Observer", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		Ecc = orbElements.getOEObjEccentricity(idxSun);

		// Do all the time-related calculations
		LCT = observer.getObsTime().getDecTime();
		UT = ASTTime.LCTtoUT(LCT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj);
		dateAdjust = dateAdjustObj.getIntValue();

		// adjust the date, if needed, since the LCTtoUT conversion could have changed the date
		ASTDate adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust);
		GST = ASTTime.UTtoGST(UT, adjustedDate);
		LST = ASTTime.GSTtoLST(GST, observer.getObsLon());

		ChapGUI.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.");
		ChapGUI.printlnCond("    LCT = " + ASTTime.timeToStr(observer.getObsTime(), ASTMisc.DECFORMAT) +
				" hours, date is " + ASTDate.dateToStr(observer.getObsDate()) + ")");
		ChapGUI.printCond("     UT = " + ASTTime.timeToStr(UT, ASTMisc.DECFORMAT) + " hours (");
		if (dateAdjust < 0)	ChapGUI.printCond("previous day ");
		else if (dateAdjust > 0) ChapGUI.printCond("next day ");
		ChapGUI.printlnCond(ASTDate.dateToStr(adjustedDate) + ")");
		ChapGUI.printlnCond("    GST = " + ASTTime.timeToStr(GST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    LST = " + ASTTime.timeToStr(LST, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		JDe = orbElements.getOEEpochJD();
		ChapGUI.printlnCond("2.  Compute the Julian day number for the standard epoch.");
		ChapGUI.printlnCond("    Epoch: " + String.format(ASTStyle.epochFormat, orbElements.getOEEpochDate()));
		ChapGUI.printlnCond("    JDe = " + String.format(ASTStyle.JDFormat, JDe));
		ChapGUI.printlnCond();

		JD = ASTDate.dateToJD(adjustedDate.getMonth(), adjustedDate.getdDay() + (UT / 24.0), adjustedDate.getYear());
		ChapGUI.printlnCond("3.  Compute the Julian day number for the desired date, being sure to use the");
		ChapGUI.printlnCond("    Greenwich date and UT from step 1, and including the fractional part of the day.");
		ChapGUI.printlnCond("    JD = " + String.format(ASTStyle.JDFormat, JD));
		ChapGUI.printlnCond();

		De = JD - JDe;
		ChapGUI.printlnCond("4.  Compute the total number of elapsed days since the standard epoch.");
		ChapGUI.printlnCond("    De = JD - JDe = " + String.format(ASTStyle.JDFormat, JD) + " - "
				+ String.format(ASTStyle.JDFormat, JDe));
		ChapGUI.printlnCond("       = " + String.format(ASTStyle.genFloatFormat, De) + " days");
		ChapGUI.printlnCond();

		Msun = ((360.0 * De) / 365.242191) + orbElements.getOEObjLonAtEpoch(idxSun) - orbElements.getOEObjLonAtPeri(idxSun);
		ChapGUI.printlnCond("5.  Compute the Sun's Mean Anomaly.");
		ChapGUI.printlnCond("    Msun = [(360.0 * De)/365.242191] + Eg - Wg");
		ChapGUI.printlnCond("         = [(360.0 * " + De + ")/365.242191] + " + orbElements.getOEObjLonAtEpoch(idxSun)
		+ " - " + orbElements.getOEObjLonAtPeri(idxSun));
		ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("6.  Use the MOD function to adjust Msun to the range [0, 360].");
		ChapGUI.printlnCond("    Msun = Msun MOD 360 = " + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + " MOD 360");
		Msun = ASTMath.xMOD(Msun, 360.0);
		ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		// Find the true anomaly from equation of center or Kepler's equation
		if (ChapGUI.getEQofCenterStatus()) {
			Ec = (360.0 / Math.PI) * Ecc * ASTMath.SIN_D(Msun);
			ChapGUI.printlnCond("7.  Solve the equation of the center for the Sun.");
			ChapGUI.printlnCond("    Ec = (360/pi) * eccentricity * sin(Msun) = (360/3.1415927) * " + Ecc + " * sin("
					+ ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT) + ")");
			ChapGUI.printlnCond("       = " + ASTAngle.angleToStr(Ec, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			Vsun = Ec + Msun;
			ChapGUI.printlnCond("8.  Add Ec to Msun to get the true anomaly.");
			ChapGUI.printlnCond("    Vsun = Ec + Msun = " + ASTAngle.angleToStr(Ec, ASTMisc.DECFORMAT) + " + "
					+ ASTAngle.angleToStr(Msun, ASTMisc.DECFORMAT));
			ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + " degrees");
		} else {
			ChapGUI.printlnCond("7.  Solve Kepler's equation to get the eccentric anomaly.");
			if (ChapGUI.getSimpleIterationStatus()) {
				tmpKepler = ASTKepler.calcSimpleKepler(prtObj, Msun, Ecc, termCriteria);
				Ea = tmpKepler[0];
				iter = (int) tmpKepler[1];
			} else {
				tmpKepler = ASTKepler.calcNewtonKepler(prtObj, Msun, Ecc, termCriteria);
				Ea = tmpKepler[0];
				iter = (int) tmpKepler[1];
			}
			ChapGUI.printlnCond("    Ea = E" + iter + " = " + ASTAngle.angleToStr(Ea, ASTMisc.DECFORMAT) + " degrees");
			ChapGUI.printlnCond();

			Vsun = (1 + Ecc) / (1 - Ecc);
			Vsun = Math.sqrt(Vsun) * ASTMath.TAN_D(Ea / 2.0);
			Vsun = 2.0 * ASTMath.INVTAN_D(Vsun);
			ChapGUI.printlnCond("8.  Vsun = 2 * inv tan[ sqrt[(1+eccentricity)/(1-eccentricity)] * tan(Ea/2) ]");
			ChapGUI.printlnCond("         = 2 * inv tan[ sqrt[(1+" + Ecc + ")/(1-" + Ecc + ")] * tan("
					+ ASTAngle.angleToStr(Ea, ASTMisc.DECFORMAT) + "/2) ]");
			ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + " degrees");
		}
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("9.  Use the MOD function to adjust Vsun to the range [0, 360].");
		ChapGUI.printlnCond("    Vsun = Vsun MOD 360 = " + ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + " MOD 360");
		Vsun = ASTMath.xMOD(Vsun, 360.0);
		ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		Lsun = Vsun + orbElements.getOEObjLonAtPeri(idxSun);
		ChapGUI.printlnCond("10. Add Vsun and Wg to get the ecliptic longitude.");
		ChapGUI.printlnCond("    Lsun = Vsun + Wg = " + ASTAngle.angleToStr(Vsun, ASTMisc.DECFORMAT) + " + "
				+ ASTAngle.angleToStr(orbElements.getOEObjLonAtPeri(idxSun), ASTMisc.DECFORMAT));
		ChapGUI.printlnCond("         = " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		if (Lsun > 360.0) Lsun = Lsun - 360.0;
		ChapGUI.printlnCond("11. if Lsun > 360, subtract 360.");
		ChapGUI.printlnCond("    Lsun = " + ASTAngle.angleToStr(Lsun, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond("    (Ecliptic latitude is 0 degrees)");
		ChapGUI.printlnCond();

		eqCoord = ASTCoord.EclipticToEquatorial(Bsun, Lsun, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("12. Convert ecliptic coordinates to equatorial coordinates.");
		ChapGUI.printlnCond("    RA = " + ASTTime.timeToStr(eqCoord.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) +
				" hours, Decl = " + ASTAngle.angleToStr(eqCoord.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();
		
		horizonCoord = ASTCoord.RADecltoHorizon(eqCoord.getRAAngle().getDecTime(), eqCoord.getDeclAngle().getDecAngle(),
				observer.getObsLat(), LST);
		ChapGUI.printlnCond("13. Convert equatorial coordinates to horizon coordinates.");
		ChapGUI.printlnCond("    Alt = " + ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) +
				", Az = " + ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT));
		ChapGUI.printlnCond();

		result = ASTAngle.angleToStr(horizonCoord.getAltAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Alt, " +
				ASTAngle.angleToStr(horizonCoord.getAzAngle().getDecAngle(), ASTMisc.DMSFORMAT) + " Az";
		ChapGUI.printlnCond("Thus, for this observer location and date/time, the Sun");
		ChapGUI.printlnCond("is at " + result + ".");

		ChapGUI.setResults("Sun's Location is " + result);
		prt.setProportionalFont();
		prt.resetCursor();
	}

	/**
	 * Calculate the times of sunrise and sunset.
	 */
	protected void calcSunRiseSet() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		ASTObserver observer = ChapGUI.getcurrentObserver();
		ASTOrbits orbElements = ChapGUI.getcurrentOrbitalElements();
		double[] eclCoord; 				// entries defined by calcSunEclipticCoord
		ASTCoord eqCoord1 = new ASTCoord();
		ASTCoord eqCoord2 = new ASTCoord();
		double ST1r, ST1s, ST2r, ST2s, Tr, Ts, LCTr, LCTs, GST, UT;
		double Bsun1, Bsun2, Lsun1, Lsun2;
		double[] LSTTimes;
		boolean riseSet = true;
		ASTInt dateAdjustObj = new ASTInt();
		ASTKepler.TrueAnomalyType solveTrueAnomaly;

		// Validate the observer location data and be sure orbital elements are loaded
		if (!ChapGUI.validateGUIObsLoc()) return;
		if (!ChapGUI.checkOEDBLoaded()) return;

		ChapGUI.clearTextAreas();

		prt.setBoldFont(true);
		ChapGUI.printlnCond("Calculate Sunrise and Sunset for the Current Observer", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		ChapGUI.printlnCond();

		prt.setFixedWidthFont();

		if (ChapGUI.getSimpleIterationStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER;
		else if (ChapGUI.getNewtonMethodStatus()) solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER;
		else solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER;

		eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);
		Bsun1 = eclCoord[ASTOrbits.SUN_ECLLAT];
		Lsun1 = eclCoord[ASTOrbits.SUN_ECLLON];
		ChapGUI.printlnCond("1.  Calculate the Sun's ecliptic location at midnight for the date in question.");
		ChapGUI.printlnCond("    For UT=0 hours on the date " + ASTDate.dateToStr(observer.getObsDate().getMonth(),
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()));
		ChapGUI.printlnCond("    The Sun's coordinates are Bsun1 = " + ASTAngle.angleToStr(Bsun1, ASTMisc.DECFORMAT) +
				" degrees, Lsun1 = " + ASTAngle.angleToStr(Lsun1, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eqCoord1 = ASTCoord.EclipticToEquatorial(Bsun1, Lsun1, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("2.  Convert the Sun's ecliptic coordinates to equatorial coordinates.");
		ChapGUI.printlnCond("    RA1 = " + ASTTime.timeToStr(eqCoord1.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) +
				" hours, Decl1 = " + ASTAngle.angleToStr(eqCoord1.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
				observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria);

		LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord1.getRAAngle().getDecTime(), eqCoord1.getDeclAngle().getDecAngle(),
				observer.getObsLat());
		riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0);
		ST1r = LSTTimes[ASTOrbits.RISE_TIME];
		ST1s = LSTTimes[ASTOrbits.SET_TIME];
		ChapGUI.printlnCond("3.  Using the equatorial coordinates from step 2, compute the");
		ChapGUI.printlnCond("    LST rising and setting times.");
		if (!riseSet) {
			ChapGUI.printlnCond("    The Sun does not rise or set for this observer.");
			return;
		}
		ChapGUI.printlnCond("    ST1r = " + ASTTime.timeToStr(ST1r, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    ST1s = " + ASTTime.timeToStr(ST1s, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		Lsun2 = Lsun1 + 0.985647;
		Bsun2 = Bsun1;
		ChapGUI.printlnCond("4.  Calculate the Sun's ecliptic coordinates 24 hours later.");
		ChapGUI.printlnCond("    24 hours later, the Sun's coordinates are");
		ChapGUI.printlnCond("    Bsun2 = " + ASTAngle.angleToStr(Bsun1, ASTMisc.DECFORMAT) + " degrees (same as Bsun1)");
		ChapGUI.printlnCond("    Lsun2 = Lsun1 + 0.985647 = " + ASTAngle.angleToStr(Lsun1, ASTMisc.DECFORMAT) + " + 0.985647");
		ChapGUI.printlnCond("          = " + ASTAngle.angleToStr(Lsun2, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		if (Lsun2 > 360.0) Lsun2 = Lsun2 - 360.0;
		ChapGUI.printlnCond("5.  if Lsun2 > 360.0, subtract 360 degrees.");
		ChapGUI.printlnCond("    Lsun2 = " + ASTAngle.angleToStr(Lsun2, ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		eqCoord2 = ASTCoord.EclipticToEquatorial(Bsun2, Lsun2, orbElements.getOEEpochDate());
		ChapGUI.printlnCond("6.  Convert the ecliptic coordinates from the previous step to equatorial coordinates.");
		ChapGUI.printlnCond("    RA2 = " + ASTTime.timeToStr(eqCoord2.getRAAngle().getDecTime(), ASTMisc.DECFORMAT) +
				" hours, Decl2 = " + ASTAngle.angleToStr(eqCoord2.getDeclAngle().getDecAngle(), ASTMisc.DECFORMAT) + " degrees");
		ChapGUI.printlnCond();

		LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord2.getRAAngle().getDecTime(), eqCoord2.getDeclAngle().getDecAngle(),
				observer.getObsLat());
		riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0);
		ST2r = LSTTimes[ASTOrbits.RISE_TIME];
		ST2s = LSTTimes[ASTOrbits.SET_TIME];
		ChapGUI.printlnCond("7.  Using the equatorial coordinates from step 6, compute the");
		ChapGUI.printlnCond("    second set of LST rising and setting times.");
		if (!riseSet) {
			ChapGUI.printlnCond("    The Sun does not rise or set for this observer.");
			return;
		}
		ChapGUI.printlnCond("    ST2r = " + ASTTime.timeToStr(ST2r, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond("    ST2s = " + ASTTime.timeToStr(ST2s, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		Tr = (24.07 * ST1r) / (24.07 + ST1r - ST2r);
		ChapGUI.printlnCond("8.  Interpolate the two sets of LST rising times.");
		ChapGUI.printlnCond("    Tr = (24.07 * ST1r) / (24.07 + ST1r - ST2r)");
		ChapGUI.printlnCond("       = (24.07*" + ASTTime.timeToStr(ST1r, ASTMisc.DECFORMAT) + ")/(24.07+"
				+ ASTTime.timeToStr(ST1r, ASTMisc.DECFORMAT) + "-" + ASTTime.timeToStr(ST2r, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + ASTTime.timeToStr(Tr, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		Ts = (24.07 * ST1s) / (24.07 + ST1s - ST2s);
		ChapGUI.printlnCond("9.  Interpolate the two sets of LST setting times.");
		ChapGUI.printlnCond("    Ts = (24.07 * ST1s) / (24.07 + ST1s - ST2s)");
		ChapGUI.printlnCond("       = (24.07*" + ASTTime.timeToStr(ST1s, ASTMisc.DECFORMAT) + ")/(24.07+" +
				ASTTime.timeToStr(ST1s, ASTMisc.DECFORMAT) + "-" + ASTTime.timeToStr(ST2s, ASTMisc.DECFORMAT) + ")");
		ChapGUI.printlnCond("       = " + ASTTime.timeToStr(Ts, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		GST = ASTTime.LSTtoGST(Tr, observer.getObsLon());
		UT = ASTTime.GSTtoUT(GST, observer.getObsDate());
		LCTr = ASTTime.UTtoLCT(UT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj);
		GST = ASTTime.LSTtoGST(Ts, observer.getObsLon());
		UT = ASTTime.GSTtoUT(GST, observer.getObsDate());
		LCTs = ASTTime.UTtoLCT(UT, ChapGUI.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj);
		ChapGUI.printlnCond("10. Convert the LST rising/setting times to their corresponding LCT times.");
		ChapGUI.printlnCond("    LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.DECFORMAT) + " hours, LCTs = " +
				ASTTime.timeToStr(LCTs, ASTMisc.DECFORMAT) + " hours");
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("11. Convert the LCT times to HMS format.");
		ChapGUI.printlnCond("    LCTr = " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + ", LCTs = " +
				ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT));
		ChapGUI.printlnCond();

		ChapGUI.printlnCond("Thus, on " + ASTDate.dateToStr(observer.getObsDate().getMonth(),
				observer.getObsDate().getiDay(), observer.getObsDate().getYear()) + " for the current observer");
		ChapGUI.printlnCond("the Sun will rise at " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + " LCT and set at " +
				ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT) + " LCT");

		prt.setProportionalFont();
		prt.resetCursor();

		ChapGUI.setResults("On " + 
				ASTDate.dateToStr(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),observer.getObsDate().getYear()) +
				", the Sun will rise at " + ASTTime.timeToStr(LCTr, ASTMisc.HMSFORMAT) + " LCT and set at " +
				ASTTime.timeToStr(LCTs, ASTMisc.HMSFORMAT) + " LCT");
	}

	/**
	 * Set the termination criteria for solving Kepler's equation
	 */
	protected void setTerminationCriteria() {
		ASTPrt prt = ChapGUI.getPrtInstance();
		double dTerm = termCriteria;

		ChapGUI.clearTextAreas();;
		prt.setBoldFont(true);
		prt.println("Set Termination Criteria for use in solving Kepler's Equation", ASTPrt.CENTERTXT);
		prt.setBoldFont(false);
		prt.println();

		if (ASTQuery.showQueryForm("Enter Termination Criteria in radians\n(ex: 0.000002)") != ASTQuery.QUERY_OK) {
			prt.println("Termination criteria was not changed from " + termCriteria + " radians");
			return;
		}

		// Validate the termination criteria
		ASTReal rTmpObj = ASTReal.isValidReal(ASTQuery.getData1(), ASTMisc.HIDE_ERRORS);
		if (rTmpObj.isValidRealObj()) dTerm = rTmpObj.getRealValue();
		else {
			ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria");
			prt.println("Termination criteria was not changed from " + termCriteria + " radians");
			return;
		}

		prt.println("Prior termination criteria was: " + termCriteria + " radians");
		if (dTerm < ASTKepler.KeplerMinCriteria) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too small, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		if (dTerm > 1) {
			dTerm = ASTKepler.KeplerMinCriteria;
			prt.println("Criteria entered was too large, so it has been reset to " + dTerm + " radians");
			prt.println();
		}
		termCriteria = dTerm;

		prt.println("Termination criteria is now set to " + termCriteria + " radians");
		prt.resetCursor();
	}

}
