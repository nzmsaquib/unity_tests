package jll.celcalc.chap6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jll.celcalc.ASTUtils.ASTMsg;
import jll.celcalc.ASTUtils.ASTPrt;
import jll.celcalc.chap6.ChapMenuItems.ChapMnuItm;

/**
 * <b>Implements an action listener for menu items.</b>
 * <p>
 * Copyright (c) 2018
 * 
 * @author J. L. Lawrence
 * @version 3.0, 2018
 */
class MenusListener implements ActionListener {

	private SolarInfoActions si_actions = new SolarInfoActions();
	private OrbitalElementsActions oe_actions = new OrbitalElementsActions();

	/**
	 * Handles clicks on individual menu items.
	 * 
	 * @param e		ActionEvent object with information about what the user clicked on
	 */
	public void actionPerformed(ActionEvent e) {
		ASTPrt prt = ChapGUI.getPrtInstance();
		String action = e.getActionCommand();

		// Handle File menu
		if (action.equals(ChapGUI.getExitCommand())) {
			if (ASTMsg.pleaseConfirm("Are you sure you want to exit?"," ")) System.exit(0);
		}

		// Handle Solar Info and Orbital Elements menus
		else if (action.equals(ChapGUI.getChapMenuCommand())) {
			ChapMnuItm objMenuItem = (ChapMnuItm) e.getSource();		// the menu item the user clicked on

			switch (objMenuItem.getCalcType()) {
			// ************* Solar Info menu
			case SUN_LOCATION:
				si_actions.calcSunPosition();
				break;
			case SUN_RISE_SET:
				si_actions.calcSunRiseSet();
				break;
			case EQUINOXES_SOLSTICES:
				si_actions.calcEquinoxesSolstices();
				break;
			case SUN_DIST_AND_ANG_DIAMETER:
				si_actions.calcDistAndAngDiameter();
				break;
			case EQ_OF_TIME:
				si_actions.calcEqOfTime();
				break;
			case TERM_CRITERIA:
				si_actions.setTerminationCriteria();
				break;
				// ************* Orbital Elements menu
			case LOAD_ORBITAL_ELEMENTS:
				oe_actions.loadOrbitalElements();
				break;
			case SHOW_ORBITAL_ELEMENTS:
				oe_actions.showSunsOrbitalElements();
				break;
			default:
				ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
						"from the 'Solar Info' or 'Orbital Elements' menu and try again.", "No Menu Item Selected");
				break;
			}
		}

		// Handle Help menu
		else if (action.equals(ChapGUI.getInstructionsCommand())) {
			ChapGUI.clearTextAreas();
			prt.setBoldFont(true);
			prt.println("Instructions",ASTPrt.CENTERTXT);
			prt.setBoldFont(false);
			prt.println();

			prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
					"be used in subsequent calculations. Not all of the observer information is required for every " +
					"calculation (e.g., only the year is used to calculate the solstices and equinoxes), but all " +
					"data items must be entered anyway. You may find it convenient to enter an initial latitude, longitude, " +
					"and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
					"longitude, and time zone are already filled in with default values when the program starts. When this program " +
					"begins, the date and time will default to the local date and time at which the program is started.");
			prt.println();
			prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
					"various calculations are performed. Also, select the appropriate radio button to choose what method to " +
					"use when it is necessary to determine the Sun's true anomaly. The radio button 'EQ of Center' will solve " +
					"the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " +
					"'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " +
					"Newton/Raphson method. The menu entry 'Solar Info->Set Termination Critera' allows you to enter the " +
					"termination criteria (in radians) for when to stop iterating to solve Kepler's equation.");
			prt.println();
			prt.println("The menu 'Solar Info' performs various calculations related to the Sun, such as determining its " +
					"location for the current information in the 'Observer Location and Time' data area. The menu " +
					"'Orbital Elements' allow you to load data files that contain the Sun's orbital elements referenced to " +
					"a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " +
					"starts up. The menu entry 'Orbital Elements->Load Orbital Elements' allows you to load and use orbital elements " +
					"referenced to some other epoch.");

			prt.resetCursor();
		} else if (action.equals(ChapGUI.getAboutCommand())) ChapGUI.showAboutBox();

		// Handle error condition - should never get here
		else ASTMsg.criticalErrMsg("Unimplemented menu item " + action);
	}

}
