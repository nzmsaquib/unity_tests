package jll.celcalc.chap6;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import jll.celcalc.ASTUtils.ASTStyle;

/**
* This class extends JMenuItem to add some additional fields, which
* simplifies the code for adding items to the menu bar and determining
* what actions to take as various menu items are selected. 
* <p>
* Copyright (c) 2018
* 
* @author J. L. Lawrence
* @version 3.0, 2018
*/

@SuppressWarnings("serial")
class ChapMenuItems extends JMenuItem {
	
	/** Define values for the eCalcType field in a ChapMenuItem class.
	 * These indicate what menu actions to take. */
	protected enum CalculationType {
		
		// Solar Info menu
		/** Calculate the Sun's location */ SUN_LOCATION,
		/** Calculate Sunrise/Sunset */ SUN_RISE_SET,
		/** Calculate time of equinoxes and solstices */ EQUINOXES_SOLSTICES,
		/** Calculate Solar distance and angular diameter */ SUN_DIST_AND_ANG_DIAMETER,
		/** Calculate the equation of time */ EQ_OF_TIME,
		/** Set a termination criteria for solving Kepler's equation */ TERM_CRITERIA,
		// Orbital Elements menu
		/** Load Sun's orbital elements from a disk file */ LOAD_ORBITAL_ELEMENTS,
		/** Display the Sun's orbital elements */ SHOW_ORBITAL_ELEMENTS
	}
	
	/**
	 * This class constructor creates ChapMnuItm objects (e.g., menu items) for
	 * the Solar Info and Orbital Elements menus.
	 */
	protected ChapMenuItems() {
		JMenu menu;

		// Create Solar Info menu items
		menu = ChapGUI.getSolarInfoMenu();
		createMenuItem(menu,"Sun's Position", CalculationType.SUN_LOCATION);
		menu.addSeparator();
		createMenuItem(menu,"Sunrise/Sunset", CalculationType.SUN_RISE_SET);
		createMenuItem(menu,"Equinoxes and Solstices", CalculationType.EQUINOXES_SOLSTICES);
		menu.addSeparator();
		createMenuItem(menu,"Solar Distance and Angular Diameter", CalculationType.SUN_DIST_AND_ANG_DIAMETER);
		menu.addSeparator();
		createMenuItem(menu,"Equation of Time", CalculationType.EQ_OF_TIME);
		menu.addSeparator();
		createMenuItem(menu,"Set Termination Criteria", CalculationType.TERM_CRITERIA);

		// Create Orbital Elements menu
		menu = ChapGUI.getOrbitalElementsMenu();
		createMenuItem(menu,"Load Orbital Elements", CalculationType.LOAD_ORBITAL_ELEMENTS);
		createMenuItem(menu,"Display Sun's Orbital Elements", CalculationType.SHOW_ORBITAL_ELEMENTS);
	}
		
	/** Define a class for what a  menu item looks like */
	protected class ChapMnuItm extends JMenuItem {
		// The only extra thing we need to save is what type of calculation to do
		private CalculationType eCalcType;   // what type of calculation to perform

		/**
		 * This class constructor creates a ChapMnuItem when conversion data is supplied.
		 * 
		 * @param title			title for this menu item
		 * @param calcType		what type of calculation is to be done
		 */          
		protected ChapMnuItm(String title,CalculationType calcType) {
			super(title);
			eCalcType = calcType;
		}
		
		/*====================================================
		 * Define accessor methods for the menu item fields
		 *===================================================*/
		
		/**
		 * Gets the type of calculation to perform.
		 * 
		 * @return		the type of calculation to perform
		 */
		protected CalculationType getCalcType() {
			return this.eCalcType;
		}
		
	}
	
	/*-----------------------------------------------------------------------------
	 * Private methods used only in this class
	 *----------------------------------------------------------------------------*/

	/**
	 * Creates a menu item and adds it to the appropriate menu bar.
	 * 
	 * @param menu				menu to which items are to be added
	 * @param title				title for this menu item
	 * @param calcType			the type of calculation to perform
	 * @return					ChapMnuItm created
	 */     
	private ChapMnuItm createMenuItem(JMenu menu, String title, CalculationType calcType) {
		ChapMnuItm tmp = new ChapMnuItm(title, calcType);			
		menu.add(tmp);

		tmp.setFont(ASTStyle.MENU_FONT);
		tmp.setActionCommand(ChapGUI.getChapMenuCommand());
		tmp.addActionListener(ChapGUI.getMenuListener());
		
		return tmp;
	}

}
