"""
Handles the Launch Sites menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTLatLon as ASTLatLon
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS, DECFORMAT,DMSFORMAT
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTSites as ASTSites
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle

import Chap9.ChapEnums as cen

def calcInclination(gui,calctype):
    """
    Calculate the orbital inclination when launched
    from a launch site.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: INCLINATION_FROM_FILE or INCLINATION_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    idx = 0

    if (calctype == cen.CalculationType.INCLINATION_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter Launch Site latitude","Enter Launch Azimuth"]) != ASTQuery.QUERY_OK):
            return
    
        latObj = ASTLatLon.isValidLat(ASTQuery.getData(1),HIDE_ERRORS)
        if not (latObj.isLatValid()):
            ASTMsg.errMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude")
            return
    else:                        # INCLINATION_FROM_FILE       
        if not (ASTSites.areSitesLoaded()):
            ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded")
            return
        if (ASTQuery.showQueryForm(["Enter Name of Launch Site","Enter Launch Azimuth"]) != ASTQuery.QUERY_OK):
            return
        idx = ASTSites.findSiteName(ASTQuery.getData(1))
        if (idx < 0):
            ASTMsg.errMsg("No Launch Site named " + ASTQuery.getData(1) + " exists\n" + 
                          "in the currently loaded launch sites database", "Invalid Site name")
            return
        latObj = ASTSites.getSiteLat(idx)

    dLat = latObj.getLat()
    
    Az = ASTAngle.isValidAngle(ASTQuery.getData(2),HIDE_ERRORS)
    if not (Az.isValidAngleObj()):
        ASTMsg.errMsg("Invalid Azimuth - try again", "Invalid Azimuth")
        return

    if ((Az.getDecAngle() < 0.0) or (Az.getDecAngle() > 360.0)):
        ASTMsg.errMsg("Invalid Azimuth - try again", "Invalid Azimuth")
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.INCLINATION_FROM_INPUT):
        gui.printlnCond("Calculate the Orbital Inclination when launched from " + 
                        ASTLatLon.latToStr_obj(latObj, DECFORMAT) + " degrees", CENTERTXT)
    else:
        gui.printlnCond("Calculate the Orbital Inclination when launched from " + 
                        ASTSites.getSiteName(idx), CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.INCLINATION_FROM_INPUT):
        gui.printlnCond("The Launch Site latitude entered was " + ASTLatLon.latToStr_obj(latObj, DECFORMAT) +
                        " degrees (" + ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + ")")
        gui.printlnCond("The Azimuth entered was A = " + str(Az.getDecAngle()) + " degrees (" + 
                        ASTAngle.angleToStr_obj(Az, DMSFORMAT) + ")")
    else:
        gui.printlnCond("The selected launch site is at latitude " + str(dLat) + " degrees (" + 
                        ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + ")")
        gui.printlnCond("The Azimuth entered was A = " + str(Az.getDecAngle()) + " degrees (" + 
                        ASTAngle.angleToStr_obj(Az, DMSFORMAT) + ")")
    gui.printlnCond()
    
    inclin = ASTMath.SIN_D(Az.getDecAngle()) * ASTMath.COS_D(dLat)
    inclin = ASTMath.INVCOS_D(inclin)
    gui.printlnCond("inclination = inv cos[sin(A)*cos(Lat)]")
    gui.printlnCond("            = inv cos[sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Az.getDecAngle()) +
                    ")*cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dLat) + ")]")
    gui.printlnCond("            = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    gui.printlnCond()
    
    if (calctype == cen.CalculationType.INCLINATION_FROM_INPUT):
        gui.printlnCond("Launching from " + ASTAngle.angleToStr_dec(dLat, DMSFORMAT) +
                        " at an Azimuth of " + ASTAngle.angleToStr_obj(Az, DMSFORMAT))
    else:
        gui.printlnCond("Launching from " + ASTSites.getSiteName(idx) + " at an Azimuth of " +
                        ASTAngle.angleToStr_obj(Az, DMSFORMAT))
    gui.printlnCond("results in an orbital inclination of " + 
                    ASTAngle.angleToStr_dec(inclin, DMSFORMAT) + " (" +
                    ASTAngle.angleToStr_dec(inclin, DECFORMAT) + " deg)")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Lat = " + ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + \
                   ", A = " + ASTAngle.angleToStr_obj(Az, DMSFORMAT) +\
                   " results in inclination = " + ASTAngle.angleToStr_dec(inclin, DMSFORMAT) + " (" +\
                   ASTAngle.angleToStr_dec(inclin, DECFORMAT) + " deg)")



def calcLaunchAzimuth(gui,calctype):
    """
    Calculate the launch azimuth required to achieve
    a specific orbital inclination.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: LAUNCH_AZIMUTH_FROM_FILE or LAUNCH_AZIMUTH_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    idx = 0
    
    if (calctype == cen.CalculationType.LAUNCH_AZIMUTH_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter Launch Site latitude","Enter Desired Orbital Inclination"]) != ASTQuery.QUERY_OK):
            return
        latObj = ASTLatLon.isValidLat(ASTQuery.getData(1),HIDE_ERRORS) 
    else:
        if not (ASTSites.areSitesLoaded()):
            ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded")
            return
        if (ASTQuery.showQueryForm(["Enter Name of Launch Site","Enter Desired Orbital Inclination"]) != ASTQuery.QUERY_OK):
            return
        idx = ASTSites.findSiteName(ASTQuery.getData(1))
        if (idx < 0):
            ASTMsg.errMsg("No Launch Site named " + ASTQuery.getData(1) + " exists\n" + 
                    "in the currently loaded launch sites database", "Invalid Site name")
            return
        latObj= ASTSites.getSiteLat(idx)

    if (latObj.isLatValid()):
        dLat = latObj.getLat()
    else:
        ASTMsg.errMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude")
        return
    # If too close to polar, the cos function "blows up"
    if ((dLat < -85.5) or (dLat > 85.5)):
        ASTMsg.errMsg("Launch site latitude is out of range [-85.5, 85.5] - try again", "Invalid Site Latitude")
        return
    
    tmpAngle = ASTAngle.isValidAngle(ASTQuery.getData(2),HIDE_ERRORS)
    if (tmpAngle.isValidAngleObj()):
        inclin = tmpAngle.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid inclination - try again", "Invalid inclination")
        return
    if ((inclin < -90.0) or (inclin > 90.0)):
        ASTMsg.errMsg("Invalid Inclination - try again", "Invalid Inclination")
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.LAUNCH_AZIMUTH_FROM_INPUT):
        gui.printlnCond("Calculate the required launch azimuth when launched from " +
                        ASTLatLon.latToStr_obj(latObj, DECFORMAT) + " degrees", CENTERTXT)
    else:
        gui.printlnCond("Calculate the required launch azimuth when launched from " + 
                        ASTSites.getSiteName(idx), CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("The selected launch site is at latitude " + str(dLat) + " degrees (" +
                    ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + ")")
    gui.printlnCond("The desired inclination is " + str(inclin) + " degrees (" +
                    ASTAngle.angleToStr_dec(inclin, DMSFORMAT) + ")")
    gui.printlnCond()
    
    if (abs(inclin) < abs(dLat)):
        gui.printlnCond("Error: Orbital Inclination must be greater than the launch latitude")
        gui.setResults("Error: Orbital Inclination must be greater than the launch latitude")
        return
    
    Az = ASTMath.COS_D(inclin) / ASTMath.COS_D(dLat)
    Az = ASTMath.INVSIN_D(Az)
    gui.printlnCond("A = inv sin[cos(inclination)/cos(Lat)]")
    gui.printlnCond("  = inv sin[cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) +
                    ")/cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dLat) + ")]")
    gui.printlnCond("  = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,Az) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("To achieve an orbital inclination of " + 
                    ASTAngle.angleToStr_dec(inclin, DECFORMAT) + " deg (" +
                    ASTAngle.angleToStr_dec(inclin, DMSFORMAT) + ")")
    if (calctype == cen.CalculationType.LAUNCH_AZIMUTH_FROM_INPUT):
        gui.printlnCond("when launched from a site at " + str(dLat) + " degrees (" +
                        ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + ") requires a")
    else:
        gui.printlnCond("when launching from " + ASTSites.getSiteName(idx) + " requires a")
    gui.printlnCond("launch azimuth of " + ASTAngle.angleToStr_dec(Az, DMSFORMAT) + 
                    " (" + ASTAngle.angleToStr_dec(Az, DECFORMAT) + " deg)")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Lat = " + ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + 
            ", inclination = " + ASTAngle.angleToStr_dec(inclin, DMSFORMAT) + 
            " requires azimuth of " + ASTAngle.angleToStr_dec(Az, DMSFORMAT) +
            " (" + ASTAngle.angleToStr_dec(Az, DECFORMAT) + " deg)")
    


def calcLaunchVelocity(gui,calctype):
    """
    Calculate the launch azimuth required to achieve
    a specific orbital inclination.
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: VELOCITY_FROM_FILE or VELOCITY_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    idx = 0
    
    if (calctype == cen.CalculationType.VELOCITY_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter Launch Site latitude","Enter Desired Velocity (km/hr)"]) != ASTQuery.QUERY_OK):
            return      
        latObj = ASTLatLon.isValidLat(ASTQuery.getData(1),HIDE_ERRORS)
    else:                # VELOCITY_FROM_FILE
        if not (ASTSites.areSitesLoaded()):
            ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded")
            return
        if (ASTQuery.showQueryForm(["Enter Name of Launch Site","Enter Desired Velocity (km/hr)"]) != ASTQuery.QUERY_OK):
            return
        idx = ASTSites.findSiteName(ASTQuery.getData(1))
        if (idx < 0):
            ASTMsg.errMsg("No Launch Site named " + ASTQuery.getData(1) + " exists\n" + 
                    "in the currently loaded launch sites database", "Invalid Site name")
            return
        latObj = ASTSites.getSiteLat(idx)
    if not (latObj.isLatValid()):
        ASTMsg.errMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude")
        return   
    dLat = latObj.getLat()
    if ((dLat < -85.5) or (dLat > 85.5)):
        ASTMsg.errMsg("Launch site latitude is out of range [-85.5, 85.5] - try again", "Invalid Site Latitude")
        return
    
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        dVelocity = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Velocity - try again", "Invalid Velocity")
        return
    if (dVelocity < 0.0):
        ASTMsg.errMsg("Invalid Velocity - try again", "Invalid Velocity")
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.VELOCITY_FROM_INPUT):
        gui.printlnCond("Calculate the Velocity a rocket must provide when launched from " +
                        ASTLatLon.latToStr_obj(latObj, DECFORMAT) + " degrees", CENTERTXT)
    else:
        gui.printlnCond("Calculate the Velocity a rocket must provide when launched from " +
                        ASTSites.getSiteName(idx), CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("The selected launch site is at latitude " + str(dLat) + " degrees (" + 
                    ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + ")")
    gui.printlnCond("The required veclocity is " + 
                    ASTStr.insertCommas(dVelocity, 2) + " km/hr (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(dVelocity), 2) + " mph)")
    gui.printlnCond()
    
    deltaV = 1669.81 * ASTMath.COS_D(dLat)
    gui.printlnCond("'Free' velocity = 1669.81*cos(Lat) = 1669.81*cos(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,dLat) + ")")
    gui.printlnCond("                = " + str(ASTMath.Round(deltaV, 2)) + " km/hr")
    gui.printlnCond()
    
    gui.printlnCond("Rocket must provide difference in velocities")
    gui.printlnCond("deltaV = Vel - 'free' = " + 
                    ASTStr.insertCommas(dVelocity, 2) + " - " + 
                    str(ASTMath.Round(deltaV, 2)))
    deltaV = dVelocity - deltaV
    gui.printlnCond("       = " + ASTStr.insertCommas(deltaV, 2) + " km/hr")
    gui.printlnCond()
    
    gui.printlnCond("To achieve an orbital velocity of " + 
                    ASTStr.insertCommas(dVelocity, 2) + " km/hr (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(dVelocity), 2) + 
                    " mph) when launched")
    if (calctype == cen.CalculationType.VELOCITY_FROM_INPUT):
        tmpStr = "from " + ASTLatLon.latToStr_obj(latObj, DMSFORMAT)
    else:
        tmpStr = "from " + ASTSites.getSiteName(idx)
    gui.printlnCond(tmpStr + ", a rocket must supply a")
    gui.printlnCond("velocity of " + ASTStr.insertCommas(deltaV, 2) + " km/hr (" +
                    ASTStr.insertCommas(ASTMath.KM2Miles(deltaV), 2) + " mph)")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Launch " + tmpStr + " requires " + ASTStr.insertCommas(deltaV, 2) +
                   " km/hr to achieve " + ASTStr.insertCommas(dVelocity, 2) + " km/hr orbital velocity")



def displayAllLaunchSites(gui):
    """
    Display all of the currently loaded launch sites
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    name = None
    
    if not (ASTSites.areSitesLoaded()):
        ASTMsg.errMsg("No Launch Sites are currently loaded.", "No Launch Sites Loaded")
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    prt.println("Currently Loaded Launch Sites", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.println("Launch Sites Data Filename: " + ASTSites.getSitesFilename())
    prt.println("Number of Launch Sites: " + str(ASTSites.getNumSites()))
    prt.println()
    
    prt.setFixedWidthFont()
    
    for i in range(0,ASTSites.getNumSites()):
        name = ASTSites.getSiteName(i)
        if (name == None):
            prt.println("*** Invalid Name at Site " + str(i+1))
            continue
    
        # We don't have to worry about the latitudes value
        # since getting the name validates that i is in range
        # and latitudes are validated when they are read from disk.
        latObj = ASTSites.getSiteLat(i)
        prt.println("Site " + str(i + 1) + ": " + name + " at latitude " + str(latObj.getLat()) +
                    " degrees (" + ASTLatLon.latToStr_obj(latObj, DMSFORMAT) + ")")

    prt.setProportionalFont()
    prt.resetCursor()



def loadLaunchSites(gui):
    """
    Load a Launch Sites data file
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    if (ASTSites.areSitesLoaded()):
        if (ASTMsg.pleaseConfirm("Launch Sites have already been loaded. Are you sure\n" +
                                 "you want to load a new set of Launch Site Data?","Clear Launch Sites")):
            ASTSites.clearLaunchSites()
            prt.println("The currently loaded Launch Sites were cleared ...")
        else:
            prt.println("The currently loaded Launch Sites were not cleared ...")
            return

    fullFilename = ASTSites.getSitesFileToOpen()
    if ((fullFilename == None) or (len(fullFilename) <= 0)):
        return
    
    if (ASTSites.loadSitesDB(fullFilename)):
        prt.println(str(ASTSites.getNumSites()) + " Launch Sites have been successfully loaded ...")
    else:            
        ASTMsg.errMsg("Could not load Launch Sites data from " + fullFilename, "Launch Sites Data Load Failed")

    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
