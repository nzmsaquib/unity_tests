"""
Allows a user to enter orbital elements and then validate
the user's input.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
from ASTUtils.ASTMisc import HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTVect as ASTVect

#=================================================================
# Handle Keplerian elements
#=================================================================

# Define temporary variables to hold Keplerian elements.
# These variables are reset every time validateKeplerianElements
# is invoked.
__inclin = 0.0                         # orbital inclination
__ecc = 0.0                            # orbital eccentricity
__axis = 0.0                           # length of the orbital semi-major axis
__RAAN = 0.0                           # right ascension of the ascending node
__argofperi = 0.0                      # argument of perigee
__M0 = 0.0                             # mean anomaly at the epoch



def validateKeplerianElements():
    """
    Display a form to get Keplerian elements and validate the
    data values the user enters. User will enter inclination,
    eccentricity, axis, RAAN, argument of perigee, and M0.
    
    :return: true if user enters valid Keplerian elements and clicks on OK. Otherwise,
             return false. The calling routine must invoke  the appropriate 'get'
             to get the actual data that the user entered.
    """      
    global __inclin, __ecc, __axis, __RAAN, __argofperi, __M0
    
    __inclin = 0.0
    __ecc = 0.0
    __axis = 0.0
    __RAAN = 0.0
    __argofperi = 0.0
    __M0 = 0.0
    
    if (ASTQuery.showQueryForm(["inclination","eccentricity","axis","RAAN","Arg of Perigee","M0"],\
                               "Enter Keplerian Elements ...") != ASTQuery.QUERY_OK):
        return
    
    angleObj = ASTAngle.isValidAngle(ASTQuery.getData(1),HIDE_ERRORS)
    if (angleObj.isValidAngleObj()):
        __inclin = angleObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid inclination - try again", "Invalid Inclination")
        return False

    
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        __ecc = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
        return False
    if ((__ecc < 0.0) or (__ecc > 1.0)):
        ASTMsg.errMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
        return False
    
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        __axis = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
        return False
    
    rTmp = ASTReal.isValidReal(ASTQuery.getData(4),HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        __RAAN = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid RAAN - try again", "Invalid RAAN")
        return False
    
    angleObj = ASTAngle.isValidAngle(ASTQuery.getData(5),HIDE_ERRORS)
    if (angleObj.isValidAngleObj()):
        __argofperi = angleObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Argument of Perigee - try again", "Invalid Arg of Peri")
        return False

    angleObj = ASTAngle.isValidAngle(ASTQuery.getData(6),HIDE_ERRORS)
    if (angleObj.isValidAngleObj()):
        __M0 = angleObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Mean Anomaly - try again", "Invalid Mean Anomaly")
        return False
    
    return True



#====================================================================
# Define 'getters' for the Keplerian elements
#====================================================================

def getInclination():
    return __inclin
def getEccentricity():
    return __ecc
def getAxisLength():
    return __axis
def getRAAN():
    return __RAAN
def getArgOfPeri():
    return __argofperi
def getMeanAnomaly():
    return __M0



#=================================================================
# Handle state vectors
#=================================================================

# Define temporary variables to hold two state vectors.
# These variables are reset every time validateStateVector
# is invoked.
__R = ASTVect.ASTVect(0.0,0.0,0.0)        # positional vector
__V = ASTVect.ASTVect(0.0,0.0,0.0)        # velocity vector

def validateStateVect():
    """
    Display a form to get a state vector and validate the
    data values the user enters. User will enter a state vector
    consisting of a positional vector (R) and a velocity vector (V).
    
    :return: true if user enters a valid state vector and clicks on OK. Otherwise,
             return false. The calling routine must invoke the appropriate 'get'
             to get the actual data that the user entered.
    """
    global __R, __V
    
    __R.setVect(0.0, 0.0, 0.0)
    __V.setVect(0.0, 0.0, 0.0)
    
    if (ASTQuery.showQueryForm(["Rx","Ry","Rz","Vx","Vy","Vz"],"Enter State Vector components ...") != ASTQuery.QUERY_OK):
        return
    
    # Validate the positional (R) vector
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1))
    if (rTmp.isValidRealObj()):
        x = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid positional Rx value - try again", "Invalid Rx")
        return False
    rTmp = ASTReal.isValidReal(ASTQuery.getData(2))
    if (rTmp.isValidRealObj()):
        y = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid positional Ry value - try again", "Invalid Ry")
        return False
    rTmp = ASTReal.isValidReal(ASTQuery.getData(3))
    if (rTmp.isValidRealObj()):
        z = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid positional Rz value - try again", "Invalid Rz")
        return False
    __R.setVect(x, y, z)
    
    # Validate the velocity (V) vector
    rTmp = ASTReal.isValidReal(ASTQuery.getData(4))
    if (rTmp.isValidRealObj()):
        x = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid positional Vx value - try again", "Invalid Vx")
        return False
    rTmp = ASTReal.isValidReal(ASTQuery.getData(5))
    if (rTmp.isValidRealObj()):
        y = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid positional Vy value - try again", "Invalid Vy")
        return False
    rTmp = ASTReal.isValidReal(ASTQuery.getData(6))
    if (rTmp.isValidRealObj()):
        z = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid positional Vz value - try again", "Invalid Vz")
        return False
    __V.setVect(x, y, z)
    
    return True



#====================================================================
# Define 'getters' for the state vector components
#====================================================================

def getPosVect():
    return __R
def getVelVect():
    return __V



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
