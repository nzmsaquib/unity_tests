"""
Handles the TLE menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS,HMSFORMAT,DMSFORMAT
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime
import ASTUtils.ASTTLE as ASTTLE

import Chap9.ChapEnums as cen
import Chap9.Misc as Misc

def calcAxisFromMMotion(gui,calctype):
    """
    Calculate length of the semi-major axis from the mean motion
    
    :param tkwidget gui: main application window
    :param CalculationType calctype: AXIS_FROM_MMOTION_FROM_TLE or AXIS_FROM_MMOTION_FROM_INPUT
    """
    prt = gui.getPrtInstance()
    idx = 0
    MMotion = 0.0
    
    if (calctype == cen.CalculationType.AXIS_FROM_MMOTION_FROM_INPUT):
        if (ASTQuery.showQueryForm(["Enter Mean Motion (revs/day)"]) != ASTQuery.QUERY_OK):
            return
        rTmp = ASTReal.isValidReal(ASTQuery.getData(1))
        if (rTmp.isValidRealObj()):
            MMotion = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid Mean Motion - try again", "Invalid Mean Motion")
            return
        if (MMotion < 0.0):
            ASTMsg.errMsg("Invalid Mean Motion - try again", "Invalid Mean Motion")
            return
    else:                        # calculationtype.AXIS_FROM_MMOTION_FROM_TLE
        if not (ASTTLE.areTLEsLoaded()):
            ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
            return
        idx = Misc.getValidTLENum()
        if (idx < 0):
            return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    if (calctype == cen.CalculationType.AXIS_FROM_MMOTION_FROM_INPUT):
        gui.printlnCond("Calculate the Length of the Semi-Major Axis from Mean Motion", CENTERTXT)
    else:
        gui.printlnCond("Calculate the Length of the Semi-Major Axis from TLE Data Set " + str(idx+1), CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    if (calctype == cen.CalculationType.AXIS_FROM_MMOTION_FROM_INPUT):
        gui.printlnCond("Mean Motion entered: " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) +
                        " revolutions per day")
    else:
        if (gui.getShowInterimCalcsStatus()):
            ASTTLE.displayTLEHeader()
            ASTTLE.displayTLEItem(idx)
            gui.printlnCond()
    
        rTmp = ASTTLE.getTLEMeanMotion(idx)
        if (rTmp.isValidRealObj()):
            MMotion = rTmp.getRealValue()
        else:
            ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
            return
        gui.printlnCond("Mean Motion extracted from TLE Data Set: " + 
                        ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " revolutions per day")
    
    gui.printlnCond()
    
    axis = Misc.MeanMotion2Axis(gui,MMotion, True)
    
    strTmp = "Mean Motion of " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " rev/day gives a = " +\
             ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,axis) + " km"
    
    gui.printlnCond("Thus, " + strTmp)
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(strTmp)



def calcMeanMotionFromAxis(gui):
    """
    Calculate the mean motion from the length of the semi-major axis
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    axis = 0.0
    
    if (ASTQuery.showQueryForm(["Enter Length of Semi-Major Axis (in km)"]) != ASTQuery.QUERY_OK):
        return
    
    rTmp = ASTReal.isValidReal(ASTQuery.getData(1), HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        axis = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Axis - try again", "Invalid Axis")
        return
    
    if (axis < 0.0):
        ASTMsg.errMsg("Invalid Axis - try again", "Invalid Axis")
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Calculate Mean Motion from Length of the Semi-Major Axis", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("Axis entered: " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,axis) + " km")
    gui.printlnCond()
    
    MMotion = Misc.Axis2MeanMotion(gui,axis, True)
    gui.printlnCond()
    
    result = "a = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,axis) + " km gives Mean Motion = " +\
             ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,MMotion) + " rev/day"
    
    gui.printlnCond("Thus, " + result)
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults(result)



def decodeTLEData(gui,decodeBy):
    """
    Decode a TLE data set to show what its elements are. There may 
    be multiple data sets to display based on how the selection
    of what to display is done.
    
    :param tkwidget gui: main application window
    :param CalculationType decodeBy: DECODE_TLE_BY_NAME, DECODE_TLE_BY_CATID, or DECODE_TLE_BY_SETNUM
    """
    prt = gui.getPrtInstance()
    idx = 0
    obj = ""
    
    if not (ASTTLE.areTLEsLoaded()):
        ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
        return
    
    if (decodeBy == cen.CalculationType.DECODE_TLE_BY_CATID):
        if (ASTQuery.showQueryForm(["Enter Object's Catalog ID"]) != ASTQuery.QUERY_OK):
            return
        obj = ASTQuery.getData(1)
        obj = obj.strip()
        idx = ASTTLE.findTLECatID(obj, idx)
        if (idx < 0):
            ASTMsg.errMsg("No Object with the Catalog ID '" + obj + "' was found", "Invalid Catalog ID")
            return
    elif (decodeBy == cen.CalculationType.DECODE_TLE_BY_NAME):
        if (ASTQuery.showQueryForm(["Enter Object's Name (or partial name)"]) != ASTQuery.QUERY_OK):
            return
        obj = ASTQuery.getData(1)
        obj = obj.strip()
        idx = ASTTLE.findTLEName(obj, idx)
        if (idx < 0):
            ASTMsg.errMsg("No Object with the name '" + obj + "' was found", "Invalid Object Name")
            return
    else:            # DECODE_TLE_BY_SETNUM
        idx = Misc.getValidTLENum()

    
    gui.clearTextAreas()
    
    if (decodeBy == cen.CalculationType.DECODE_TLE_BY_SETNUM):
        ASTTLE.decodeTLE(idx, "TLE Data for Data Set " + str(idx+1))
    else:
        while (idx >= 0):
            if (decodeBy == cen.CalculationType.DECODE_TLE_BY_CATID):
                ASTTLE.decodeTLE(idx, "TLE Data for Catlog ID '" + obj + "', Data Set " + str(idx+1))
                idx = ASTTLE.findTLECatID(obj, idx + 1)
            else:
                ASTTLE.decodeTLE(idx, "TLE Data for Object '" + obj + "', Data Set " + str(idx+1))
                idx = ASTTLE.findTLEName(obj, idx + 1)
    
    prt.resetCursor()



def displayRawTLEData(gui):
    """
    Display all of the currently loaded TLE data.
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    if not (ASTTLE.areTLEsLoaded()):
        ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
        return
    
    TLEdbsize = ASTTLE.getNumTLEDBObjs()
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    prt.println("Currently Loaded TLE Data Sets", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.println("TLE Data Filename: " + ASTTLE.getTLEFilename())
    prt.println("Number of TLE data sets: " + str(TLEdbsize))
    prt.println()
    
    prt.setFixedWidthFont()
    ASTTLE.displayTLEHeader()
    
    for i in range(0,TLEdbsize):
        ASTTLE.displayTLEItem(i)
    
    prt.setProportionalFont()
    prt.resetCursor()



def loadTLEData(gui):
    """
    Load a TLE data file
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    if (ASTTLE.areTLEsLoaded()):
        if (ASTMsg.pleaseConfirm("TLEs have already been loaded. Are you sure\n" +
                                 "you want to load a new set of TLE Data?","Clear TLEs")):
            ASTTLE.clearTLEData()
            prt.println("The currently loaded TLEs were cleared ...")
        else:
            prt.println("The currently loaded TLEs were not cleared ...")
            return

    fullFilename = ASTTLE.getTLEFileToOpen()
    if ((fullFilename == None) or (len(fullFilename) <= 0)):
        return
    
    if (ASTTLE.loadTLEDB(fullFilename)):
        prt.println(str(ASTTLE.getNumTLEDBObjs()) + " TLE data sets have been successfuly loaded ...")
    else:            
        ASTMsg.errMsg("Could not load TLE data from " + fullFilename, "TLE Data Load Failed")

    prt.resetCursor()



def extractKeplerianFromTLE(gui):
    """
    Extract Keplerian elements from a TLE data set
    
    :param tkwidget gui: main application window
    """
    prt = gui.getPrtInstance()
    
    if not (ASTTLE.areTLEsLoaded()):
        ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
        return
    
    idx = Misc.getValidTLENum()
    if (idx < 0):
        return
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    gui.printlnCond("Extract Keplerian Elements from TLE Data Set " + str(idx+1), CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    if (gui.getShowInterimCalcsStatus()):
        ASTTLE.displayTLEHeader()
        ASTTLE.displayTLEItem(idx)
        gui.printlnCond()
    
    line1 = ASTTLE.getTLELine1(idx)
    line2 = ASTTLE.getTLELine2(idx)
    
    gui.printlnCond("1.  Get the epoch year from columns 19-20 of TLE Line 1")
    gui.printlnCond("    Epoch year from TLE is " + line1[18:20])
    iTmp = ASTTLE.getTLEEpochYear(idx)
    if (iTmp.isValidIntObj()):
        iYear = iTmp.getIntValue()
    else:
        ASTMsg.errMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year")
        return
    gui.printlnCond("    Years < 57 are in the 2000s. Years > 56 are in the 1900s")
    if (iYear < 57):
        iYear = iYear + 2000
        gui.printlnCond("    Thus, Epoch Year = Year + 2000 = " + str(iYear))
    else:
        iYear = iYear + 1900
        gui.printlnCond("    Thus, Epoch Year = Year + 1900 = " + str(iYear))
    gui.printlnCond()
    
    gui.printlnCond("2.  Get the epoch day of year from columns 21-32 of TLE Line 1")
    gui.printlnCond("    Epoch day of year from TLE is " + line1[20:32])
    rTmp = ASTTLE.getTLEEpochDay(idx)
    if (rTmp.isValidRealObj()):
        dT = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year")
        return
    gui.printlnCond()
    
    daysIntoYear = ASTMath.Trunc(dT)
    gui.printlnCond("3.  Extract days into year as first 3 digits from " + line1[20:32])
    gui.printlnCond("    days into year = INT(" + line1[20:32] + ")")
    gui.printlnCond("                   = " + str(daysIntoYear))
    gui.printlnCond()
    
    epochDate = ASTDate.daysIntoYear2Date(iYear, daysIntoYear)
    gui.printlnCond("4.  Convert days into year to a calendar date for the year " + str(iYear))
    gui.printlnCond("    Epoch date = " + ASTDate.dateToStr_obj(epochDate))
    gui.printlnCond()
    
    dT = dT - daysIntoYear
    gui.printlnCond("5.  Extract fractional part of the day from " + line1[20:32])
    gui.printlnCond("    Frac Day = " + str(ASTMath.Round(dT, 8)))
    gui.printlnCond()
    
    epochUT = ASTMath.Round(dT, 8) * 24.0
    gui.printlnCond("6.  Convert fractional part of day to UT")
    gui.printlnCond("    UT = (Frac Day)*24 = " + str(ASTMath.Round(dT, 8)) + "*24")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,epochUT) + 
                    " = " + ASTTime.timeToStr_dec(epochUT, HMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("7.  Get the orbital inclination from columns 9-16 of TLE Line 2")
    gui.printlnCond("    Inclination = " + line2[8:16].strip() + " degrees")
    rTmp = ASTTLE.getTLEInclination(idx)
    if (rTmp.isValidRealObj()):
        inclin = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Inclination in TLE Data Set", "Invalid Inclination")
        return
    gui.printlnCond("                = " + ASTAngle.angleToStr_dec(inclin, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("8.  Get the RAAN from columns 18-25 of TLE Line 2")
    gui.printlnCond("    RAAN = " + line2[17:25] + " degrees")
    rTmp = ASTTLE.getTLERAAN(idx)
    if (rTmp.isValidRealObj()):
        RAAN = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid RAAN in TLE Data Set", "Invalid RAAN")
        return
    gui.printlnCond("         = " + ASTAngle.angleToStr_dec(RAAN, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("9.  Get the orbital eccentricity from columns 27-33 of TLE Line 2")
    gui.printlnCond("    (A leading decimal point is assumed)")
    strTmp = "0." + line2[26:33].strip()
    rTmp = ASTTLE.getTLEEccentricity(idx)
    if (rTmp.isValidRealObj()):
        ecc = rTmp.getRealValue()
        gui.printlnCond("    e = " + strTmp)
    else:
        ASTMsg.errMsg("Invalid Eccentricity in TLE Data Set", "Invalid Eccentricity")
        return
    gui.printlnCond()
    
    gui.printlnCond("10. Get the argument of perigee from columns 35-42 of TLE Line 2")
    gui.printlnCond("    w = " + line2[34:42].strip() + " degrees")
    rTmp = ASTTLE.getTLEArgofPeri(idx)
    if (rTmp.isValidRealObj()):
        argofperi = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Argument of Perigee in TLE Data Set", "Invalid Arg of Perigee")
        return
    gui.printlnCond("      = " + ASTAngle.angleToStr_dec(argofperi, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("11. Get the mean anomaly from columns 44-51 of TLE Line 2")
    gui.printlnCond("    M0 = " + line2[43:51].strip() + " degrees")
    rTmp = ASTTLE.getTLEMeanAnomaly(idx)
    if (rTmp.isValidRealObj()):
        M0 = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Mean Anomaly in TLE Data Set", "Invalid Mean Anomaly")
        return
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(M0, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("12. Get the mean motion from columns 53-63 of TLE Line 2")
    gui.printlnCond("    Mean Motion = " + line2[52:63].strip() + " rev/day")
    rTmp = ASTTLE.getTLEMeanMotion(idx)
    if (rTmp.isValidRealObj()):
        MMotion = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
        return
    gui.printlnCond()
    
    gui.printlnCond("13. Convert the mean motion to length of the semi-major axis")
    axis = Misc.MeanMotion2Axis(gui,MMotion, True)
    
    prt.println("Epoch Date " + ASTDate.dateToStr_obj(epochDate) + " at " + 
                ASTTime.timeToStr_dec(epochUT, HMSFORMAT) + " UT")
    prt.println("Keplerian Elements are:")
    prt.println("  Inclination = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,inclin) + " degrees")
    prt.println("  Eccentricity = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,ecc))
    prt.println("  Length of Semi-major axis = " + ASTStr.insertCommas(axis, 8) + " km")
    prt.println("  RAAN = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,RAAN) + " degrees")
    prt.println("  Argument of Perigee = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,argofperi) + " degrees")
    prt.println("  Mean anomaly at the epoch = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT8,M0) + " degrees")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Keplerian Elements extracted from TLE Data Set " + str(idx+1) +
                   " are listed in the text area below")



def searchForTLE(gui,searchBy):
    """
    Search the TLE database by catalog ID or name and display the
    raw data for that object. Note that the TLE database may have
    multiple entries with the same catalog ID/name. An exact match
    is required for catalog IDs, but a partial match is all that
    is required when done by name.
    
    :param tkwidget gui: main application window
    :param CalculationType searchBy: SEARCH_TLE_BY_NAME or SEARCH_TLE_BY_CATID
    """
    prt = gui.getPrtInstance()
    idx = 0
    obj = ""
    
    if not (ASTTLE.areTLEsLoaded()):
        ASTMsg.errMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
        return
    
    if (searchBy == cen.CalculationType.SEARCH_TLE_BY_CATID):
        if (ASTQuery.showQueryForm(["Enter Object's Catalog ID"]) != ASTQuery.QUERY_OK):
            return
        obj = ASTQuery.getData(1)
        obj = obj.strip()
        idx = ASTTLE.findTLECatID(obj, idx)
        if (idx < 0):
            ASTMsg.errMsg("No Object with the catalog ID '" + obj + "' was found", "Invalid Catalog ID")
            return
    else:
        if (ASTQuery.showQueryForm(["Enter Object's Name (or partial name)"]) != ASTQuery.QUERY_OK):
            return
        obj = ASTQuery.getData(1)
        obj = obj.strip()
        idx = ASTTLE.findTLEName(obj, idx)
        if (idx < 0):
            ASTMsg.errMsg("No Object with the name '" + obj + "' was found", "Invalid Object Name")
            return
    
    gui.clearTextAreas()
    prt.setFixedWidthFont()
    ASTTLE.displayTLEHeader()
    
    while (idx >= 0):
        ASTTLE.displayTLEItem(idx)
        if (searchBy == cen.CalculationType.SEARCH_TLE_BY_CATID):
            idx = ASTTLE.findTLECatID(obj, idx + 1)
        else:
            idx = ASTTLE.findTLEName(obj, idx + 1)

    prt.setProportionalFont()
    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
