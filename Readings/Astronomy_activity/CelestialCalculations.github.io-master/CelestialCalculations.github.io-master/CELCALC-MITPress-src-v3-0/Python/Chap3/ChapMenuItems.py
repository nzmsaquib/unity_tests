"""
Create menu entries for this chapter's Time Conversions
and Local vs Greenwich menus. This module also creates
a class and structure that associates data items with
menu entries to make it easier to determine what actions
to take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap3.ChapEnums
import Chap3.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    


#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what type of calculation to do
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what menu is to be done
    
    def __init__(self,eCalcType):
        """
        Create a ChapMenuItem instance
        
        :param CalculationType eCalcType: what menu item is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getCalcType(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createCalcType(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what type of conversion is to be done
    :return: the menu object that got created
    """
    # define a listener for responding to clicks on a menu item
    convitemCallback = lambda: ml.conversionsMenuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the conversions menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=convitemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: GUI object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """
    cen = Chap3.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create the Time Conversions menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Time Conversions ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu,"Leap Years",cen.LEAP_YEARS)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Calendar Date to Julian Day Number", cen.CALENDAR_2_JD)
    __createCalcType(gui,tmpmenu,"Julian Day Number to Calendar Date", cen.JD_2_CALENDAR)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"Day of Week", cen.DAY_OF_WEEK)
    __createCalcType(gui,tmpmenu,"Date to Days into Year", cen.DATE_2_DAYS_INTO_YEAR)
    __createCalcType(gui,tmpmenu,"Days into Year to Date", cen.DAYS_INTO_YEAR_2_DATE)

    # Create the Local vs Greenwich menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Local vs Greenwich ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu,"LCT to UT", cen.LCT_2_UT)
    __createCalcType(gui,tmpmenu,"UT to LCT", cen.UT_2_LCT)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"UT to GST", cen.UT_2_GST)
    __createCalcType(gui,tmpmenu,"GST to UT", cen.GST_2_UT)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu,"GST to LST", cen.GST_2_LST)
    __createCalcType(gui,tmpmenu,"LST to GST", cen.LST_2_GST) 
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
