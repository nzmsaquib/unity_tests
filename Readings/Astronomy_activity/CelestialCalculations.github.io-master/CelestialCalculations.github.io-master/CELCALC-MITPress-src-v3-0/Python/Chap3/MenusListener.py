"""
Implements action listeners for the menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import sys

import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT

import Chap3.ChapEnums

#==================================================
# Define listeners for about, exit, instructions
#==================================================

def aboutMenuListener(gui):
    """
    Handle a click on the About menu item
    
    :param tkwidget gui: GUI that the About Box is associated with
    """
    gui.showAboutBox()
    


def exitMenuListener():
    """Handle a click on the Exit menu item"""
    if ASTMsg.pleaseConfirm("Are you sure you want to exit?"," "):
        sys.exit()    
    


def instructionsMenuListener(prt):
    """
    Handle a click on the Instructions menu item
    
    :param ASTPrt prt: ASTPrt object instance created by the top level GUI
    """
    prt.clearTextArea()
    prt.setBoldFont(True)
    prt.println("Instructions",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
 
    prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " +
                "intermediate results as the various calculations are performed. Also check the 'Daylight Saving Time' " +
                "checkbox if the times that will be entered are on Daylight Saving Time.")
    prt.println()
 
    prt.println("Second, select the appropriate time zone (EST, CST, etc.), or select the 'Longitude' radio " +
                "button if a longitude will be entered rather than a time zone. If a longitude is to be entered, " +
                "enter the longtiude in the text box beside the 'Longitude' radio button. Longitudes may be entered " +
                "in decimal format (e.g., 96.5E, 30.45W) or DMS format (e.g., 96d 30m 18.5sW). The N, S, E, W direction " +
                "must be capitalized. That is, 96.5e and 30.45w are both errors because the direction is not capitalized.")
    prt.println()
 
    prt.printnoln("Third, after establishing the desired checkbox and radio button settings, select the desired " +
                 "calculation to perform from the 'Time Conversions' or the 'Local vs. Greenwich' menus. Once a " +
                 "menu item is selected, you will be asked to enter one or two lines of data in the text boxes beneath the " +
                 "Time Zone radio buttons. The values to be entered depend upon what menu item was selected. For " +
                 "converting between GST and LST, the longitude must be entered in the text box beside the 'Longitude' " +
                 "radio button ")
    prt.setBoldFont(True)
    prt.printnoln("*and not*")
    prt.setBoldFont(False)
    prt.println(" in one of the text boxes below the Time Zone radio buttons. Note that time is " +
                "entered in 24 hour format. Thus, 3:30:20 PM would be entered as 15:30:20.")
    prt.println()
 
    prt.printnoln("Finally, after entering all required data, click the 'Calculate' button at the bottom " +
                  "right of the data input area. The selected computation (e.g., LCT to UT) will be done. " +
                  "If you selected the 'Show Intermedicate Calculations' checkbox, the steps required to find " +
                  "a solution will be shown in the scrollable text output area (i.e., where these instructions " +
                  "are being displayed). The final result will be shown just below the data input text boxes.")
 
    prt.resetCursor()
   


#==================================================
# Define listener for the remaining menu items
#==================================================

def conversionsMenuListener(calcType,gui):
    """
    Handle a click on the menu items
    
    :param CalculationType calcType: the calculation to perform
    :param tkwidget gui: GUI object to which menu items are associated
    """
    cen = Chap3.ChapEnums.CalculationType           # shorten to make typing easier!!!
    
    gui.setCalcToDo(calcType)
    gui.getPrtInstance().clearTextArea()

    #**************** Time Conversions
    if (calcType == cen.LEAP_YEARS):
        gui.setDataLabels("Year (in form YYYY)")
        gui.setResults("Determine if a year is a leap year")
    elif (calcType == cen.CALENDAR_2_JD):
        gui.setDataLabels("Date (in form MM/DD.dd/YYYY)")
        gui.setResults("Convert Calendar Date to a Julian Day Number")
    elif (calcType == cen.JD_2_CALENDAR):
        gui.setDataLabels("Julian Day Number (ex: 2455928.50)")
        gui.setResults("Convert Julian Day Number to a Calendar Date")
    elif (calcType == cen.DAY_OF_WEEK):
        gui.setDataLabels("Date (in form MM/DD/YYYY)")
        gui.setResults("Calculate the day of the week for a given date")
    elif (calcType == cen.DATE_2_DAYS_INTO_YEAR):
        gui.setDataLabels("Date (in form MM/DD/YYYY)")
        gui.setResults("Convert a Calendar Date to Days into the Year")
    elif (calcType == cen.DAYS_INTO_YEAR_2_DATE):
        gui.setDataLabels("Days into Year (e.g., 15)", "Year")
        gui.setResults("Convert Days into the Year to a Calendar Date")

    #**************** Local vs Greenwich
    elif (calcType == cen.LCT_2_UT):
        gui.setDataLabels("Enter LCT time (hh:mm:ss.ss)", "(Set Time Zone or Longitude above)")
        gui.setResults("Convert Local Civil Time to Universal Time")
    elif (calcType == cen.UT_2_LCT):
        gui.setDataLabels("Enter UT time (hh:mm:ss.ss)", "(Set Time Zone or Longitude above)")
        gui.setResults("Convert Universal Time to Local Civil Time")
    elif (calcType == cen.UT_2_GST):
        gui.setDataLabels("Enter UT time (hh:mm:ss.ss)", "Date (in form MM/DD/YYYY)")
        gui.setResults("Convert Universal Time to Greenwich Sidereal Time")
    elif (calcType == cen.GST_2_UT):
        gui.setDataLabels("Enter GST time (hh:mm:ss.ss)", "Date (in form MM/DD/YYYY)")
        gui.setResults("Convert Greenwich Sidereal Time to Universal Time")
    elif (calcType == cen.GST_2_LST):
        gui.setLonRadBtn()
        gui.setDataLabels("Enter GST time (hh:mm:ss.ss)", "(Enter Longitude in the Time Zone box above)")
        gui.setResults("Convert Greenwich Sidereal Time to Local Sidereal Time")
    elif (calcType == cen.LST_2_GST):
        gui.setLonRadBtn()
        gui.setDataLabels("Enter LST time (hh:mm:ss.ss)", "(Enter Longitude in the Time Zone box above)")
        gui.setResults("Convert Local Sidereal Time to Greenwich Sidereal Time")
        
    else:           # This should never happen
        ASTMsg.criticalErrMsg("Invalid menu item " + str(calcType))
        gui.setCalcToDo(cen.NO_CALC_SELECTED)        



#=========== Main entry point ===============
if __name__ == '__main__':
    pass    
