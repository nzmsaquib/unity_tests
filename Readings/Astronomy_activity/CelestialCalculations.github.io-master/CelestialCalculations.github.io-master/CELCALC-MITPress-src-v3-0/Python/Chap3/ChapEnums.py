"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what time-related conversion to do."""
    NO_CALC_SELECTED = auto()        # no calculation is currently selected
    
    # Time Conversions
    LEAP_YEARS = auto()              # determine if a Gregorian year is a leap year
    CALENDAR_2_JD = auto()           # convert a calendar date to a Julian day number
    JD_2_CALENDAR = auto()           # convert a Julian day number to a calendar date
    DAY_OF_WEEK = auto()             # determine what day of the week a particular date falls
    DATE_2_DAYS_INTO_YEAR = auto()   # determine the number of days into the year that a given date falls
    DAYS_INTO_YEAR_2_DATE = auto()   # given a number of days into the year, determine the date
    
    # Local vs Greenwich
    LCT_2_UT = auto()
    UT_2_LCT = auto()
    UT_2_GST = auto()
    GST_2_UT = auto()
    GST_2_LST = auto()
    LST_2_GST = auto()
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
