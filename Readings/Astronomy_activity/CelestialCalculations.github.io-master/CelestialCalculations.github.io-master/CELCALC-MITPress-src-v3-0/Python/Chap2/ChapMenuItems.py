"""
Create menu entries for this chapter's Conversions menu.
This module also creates a class and structure
that associates data items with menu entries to make it
easier to determine what actions to take when a menu
item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTStyle as ASTStyle

import Chap2.ChapEnums
import Chap2.MenusListener as ml

# convMenuItems is a dynamically constructed list of conversion menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__convMenuItems = []
 
#====================================================================================
# Define a class for what a menu item looks like. The data in a class
# instance tells how to do a conversion between "from" and "to."
#
# Note that the to/from denominators below are actually overloaded in their usage.
# For menu items that are to be cross-multiplied, they are the denominators
# in the equation to form to do the cross multiplication. For other usages,
# such as C to F conversion, dToDenom is -1.0 while dFromDenom is 1.0. These values
# may be swapped when the switch units button is clicked so that they continue
# to track with the other "to/from" values (e.g., strToUnits/strFromUnits). When
# it is time to do a conversion, if dFromDenom is negative, the conversion to 
# perform is in the direction indicated by the corresponding menu entry. For example,
# for the "degs C - degs F" menu entry, if fFronDenom is negative then convert 
# degs C to degs F. If dFromDenom is positive, convert degs F to degs C.
#====================================================================================

class ChapMenuItem():
    """Defines a class for Conversions menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # strFromUnits        text to display for the 'from' units (e.g., km)
    # dFromDenom          numeric value for the 'from' denominator (e.g, 5.0)
    # strFromFormat       how to format the 'from' denominator
    # strToUnits          text to display for the 'to' units (e.g., miles)
    # dToDenom            numeric value for the 'to' denominator (e.g, 5.0)
    # strToFormat         how to format the 'to' denominator
    # eConvType           what type of conversion is to be done    
     
    def __init__(self,strFromUnits,dFromDenom,strFromFormat,
                 strToUnits,dToDenom,strToFormat,eConvType):
        """
        Create an object instance
        
        :param str strFromUnits: text to display for the 'from' units (e.g., km)
        :param float dFromDenom: numeric value for the 'from' denominator (e.g, 5.0)
        :param str strFromFormat: how to format the 'from' denominator
        :param str strToUnits: text to display for the 'to' units (e.g., miles)
        :param float dToDenom: numeric value for the 'to' denominator (e.g, 5.0)
        :param str strToFormat: how to format the 'to' denominator
        :param ConversionType eConvType: what type of conversion is to be done
        """
        self.strFromUnits = strFromUnits
        self.dFromDenom = dFromDenom
        self.strFromFormat = strFromFormat
        self.strToUnits = strToUnits
        self.dToDenom = dToDenom
        self.strToFormat = strToFormat
        self.eConvType = eConvType
        
    # define 'getter' methods to return the various fields for a menu item
    def getFromUnits(self):
        return self.strFromUnits
    def getFromDenom(self):
        return self.dFromDenom
    def getFromFormat(self):
        return self.strFromFormat
    def getToUnits(self):
        return self.strToUnits
    def getToDenom(self):
        return self.dToDenom
    def getToFormat(self):
        return self.strToFormat
    def getConvType(self):
        return self.eConvType



#==============================================================
# Define this module's functions
#==============================================================

def __createMenuItem(gui,convmenu,strFromUnits,dFromDenom,strFromFormat,
                     strToUnits,dToDenom,strToFormat,eConvType):
    """
    Create an individual menu item for the Conversions cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget convmenu: cascade menu to which this item is to be added
    :param str strFromUnits: text to display for the 'from' units (e.g., km)
    :param float dFromDenom: numeric value for the 'from' denominator (e.g, 5.0)
    :param str strFromFormat: how to format the 'from' denominator
    :param str strToUnits: text to display for the 'to' units (e.g., miles)
    :param float dToDenom: numeric value for the 'to' denominator (e.g, 5.0)
    :param str strToFormat: how to format the 'to' denominator
    :param ConversionType eConvType: what type of conversion is to be done
    :return: the menu object that got created
    """
    # define a listener for responding to clicks on a Conversions menu item
    convitemCallback = lambda: ml.conversionsMenuListener(__convMenuItems[idx],gui)    
     
    menuObj = ChapMenuItem(strFromUnits,dFromDenom,strFromFormat,strToUnits,dToDenom,strToFormat,eConvType)
    # Add the menu data to the convMenuItems list and then to the conversions menu cascade
    __convMenuItems.append(menuObj)
    idx = len(__convMenuItems) - 1                  # which menu item this is
    convmenu.add_command(label=menuObj.strFromUnits+" - "+menuObj.strToUnits,command=convitemCallback)    
    return menuObj
 


def createConvMenuItems(gui,convmenu):
    """
    Create Conversion menu items and add them to the menu cascade passed in.
    
    :param tkwidget gui: gui object that will hold the menu items
    :param menuwidget convmenu: menu cascade to add the Conversion menu items to
    """
    cen = Chap2.ChapEnums.ConvType          # shorten to make typing easier!!!
     
    # The first menu item created is a special case because we want to initialize the GUI
    # to have the first menu item already selected.
    tmp = __createMenuItem(gui,convmenu,"mm",25.4,"%.6f","inches",1.0,"%.6f",cen.CROSS_MULTIPLY)
    gui.saveFromToValues(tmp.strFromUnits,tmp.dFromDenom,tmp.strFromFormat,
                         tmp.strToUnits,tmp.dToDenom,tmp.strToFormat,tmp.eConvType)                 
    __createMenuItem(gui,convmenu,"m",0.3048,"%.6f","feet",1.0,"%.6f",cen.CROSS_MULTIPLY)
    convmenu.add_separator()
  
    __createMenuItem(gui,convmenu,"km",1.609344,"%.6f","miles",1.0,"%.6f",cen.CROSS_MULTIPLY)
    __createMenuItem(gui,convmenu,"light years",1.0,"%.4E","miles",5.87E12,"%.6E",cen.CROSS_MULTIPLY)
    __createMenuItem(gui,convmenu,"light years",1.0,"%.4f","parsecs",0.3068,"%.4f",cen.CROSS_MULTIPLY)
    __createMenuItem(gui,convmenu,"AU",1.0,"%.6E","miles",9.29E7,"%.4E",cen.CROSS_MULTIPLY)
    convmenu.add_separator()
 
    __createMenuItem(gui,convmenu,"degs",180.0,ASTStyle.DEC_DEG_FMT,"radians",math.pi,
                     ASTStyle.DEC_DEG_FMT,cen.CROSS_MULTIPLY)
    __createMenuItem(gui,convmenu,"decimal hrs",24.0,ASTStyle.DEC_HRS_FMT,"decimal degs",360.0,
                     ASTStyle.DEC_DEG_FMT,cen.CROSS_MULTIPLY)
    __createMenuItem(gui,convmenu,"HMS",-1.0,"@","decimal hrs",1.0,ASTStyle.DEC_HRS_FMT,cen.HorDMS_HRSorDEGS)
    __createMenuItem(gui,convmenu,"DMS",-1.0,"@","decimal degs",1.0,ASTStyle.DEC_DEG_FMT,cen.HorDMS_HRSorDEGS)        
    convmenu.add_separator()
   
    __createMenuItem(gui,convmenu,"degs C",-1.0,ASTStyle.TEMPERATURE_FMT,"degs F",
                     1.0,ASTStyle.TEMPERATURE_FMT,cen.CENTIGRADE_FAHRENHEIT)    



#=========== Main entry point ===============
if __name__ == '__main__':
    pass    
