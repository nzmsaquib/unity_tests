"""
Implements action listeners for the menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import sys

import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT

#==================================================
# Define listeners for about, exit, instructions
#==================================================

def aboutMenuListener(gui):
    """
    Handle a click on the About menu item
    
    :param tkwidget gui: GUI that the About Box is associated with
    """
    gui.showAboutBox()
    


def exitMenuListener():
    """Handle a click on the Exit menu item"""
    if ASTMsg.pleaseConfirm("Are you sure you want to exit?"," "):
        sys.exit()    
    


def instructionsMenuListener(prt):
    """
    Handle a click on the Instructions menu item
    
    :param ASTPrt prt: ASTPrt object instance created by the top level GUI
    """
    prt.clearTextArea()
    prt.setBoldFont(True)
    prt.println("Instructions",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
 
    prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " +
                "intermediate results as the conversions are performed. Then select the " +
                "conversion to perform from the 'Conversions' menu. For example, select the " +
                "'mm - inches' menu item to convert between millimeters and inches.")
    prt.println()
 
    prt.println("The units on the left side of the menu (e.g., mm) will be converted to the units " +
                "on the right side of the menu (e.g., inches). To perform a conversion the other " +
                "way (e.g., inches to mm), click the 'Swap Units' button.")
    prt.println()
 
    prt.println("Enter the value to convert in the white input box to the right of the 'Convert' " +
                "button. For all conversions except HMS/DMS, enter only digits, a decimal point, and " +
                "a + or - sign, if needed. To convert HMS values, enter the HMS value in the form " +
                "hh:mm:ss.ssss where hh is the hours, mm is the minutes, and ss.ssss is the seconds. (Note that " +
                "time is expressed in 24 hour format, so 15:30:20 would be entered for 3:30:20 PM.) " +
                "For DMS values, enter them in the form xxxd yym zz.zzzzs where xxx is the degrees, " +
                "yy is the arcminutes, and zz.zzzzs is the arcseconds (e.g., 160d 8m 15.2s).")
    prt.println()
 
    prt.printnoln("Finally, click the 'Convert' button to perform the conversion. The result of the " +
                  "conversion will be shown directly underneath the white input box. If the " +
                  "'Show Intermediate Calculations' checkbox is checked, intermediate calculation results " +
                  "will be shown in the scrollable text area at the bottom of the application window.")
    prt.resetCursor()
    


#==================================================
# Define listener for the Conversion menu items
#==================================================

def conversionsMenuListener(menuObj,gui):
    """
    Handle a click on the Conversions menu items
    
    :param ChapMenuItem menuObj: the menu item object that was clicked on
    :param tkwidget gui: GUI object to which menu items are associated
    """
    # A click on a menu item merely means to set the conversion units data
    gui.saveFromToValues(menuObj.getFromUnits(),menuObj.getFromDenom(),menuObj.getFromFormat(),
                         menuObj.getToUnits(),menuObj.getToDenom(),menuObj.getToFormat(),
                         menuObj.getConvType())
    gui.getPrtInstance().clearTextArea()  



#=========== Main entry point ===============
if __name__ == '__main__':
    pass    
