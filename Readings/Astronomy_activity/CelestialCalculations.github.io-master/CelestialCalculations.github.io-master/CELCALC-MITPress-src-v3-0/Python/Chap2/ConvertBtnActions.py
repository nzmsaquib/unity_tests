"""
Handles the Convert Button actions

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HMSFORMAT, DMSFORMAT
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTTime as ASTTime

def convertC_F(gui):
    """
    Convert between degrees C and degrees F. The data
    to be converted is in the txtboxUserInput field in the GUI.
    
    Gets the input data from the GUI, validates it, then does the conversion.

    :param tkwidget gui: GUI object requesting a cross multiply
    :return: the temperature conversion results as a string
    """
    strInput = ASTStr.removeWhitespace(gui.getUserInput())
    prt = gui.getPrtInstance()
    result = ""
    
    # Get what needs to be converted from the GUI
    strFromFormat = gui.getFromFormat()
    strFromUnits = gui.getFromUnits()
    strToFormat = gui.getToFormat()
    strToUnits = gui.getToUnits()
    dFromDenom = gui.getFromDenom()

    rTmp = ASTReal.isValidReal(strInput)
    if not rTmp.isValidRealObj():
        return result

    degIn = rTmp.getRealValue()
 
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
 
    prt.setFixedWidthFont()
 
    # See if we're converting C to F or F to C
    if (dFromDenom < 0.0):                  # convert C to F
        degOut = 32.0 + (degIn * 9.0) / 5.0
        gui.printlnCond("1. The proper formula is deg F = 32 + (deg C * 9) / 5")
        gui.printlnCond()
 
        gui.printlnCond("2. Putting in the known values and performing the math, we have")
        gui.printlnCond("   deg F = 32 + (" + ASTStr.strFormat(strFromFormat,degIn) + " deg * 9) / 5")
        gui.printlnCond("         = " + ASTStr.strFormat(strToFormat,degOut) + " deg F")
 
        gui.printlnCond()
        gui.printlnCond("Thus, " + ASTStr.strFormat(strFromFormat,degIn) + " degrees C = " + 
                         ASTStr.strFormat(strToFormat,degOut) + " degrees F")
    else:
        degOut = (degIn - 32.0) * (5.0 / 9.0)
 
        gui.printlnCond("1. The proper formula is deg C = (deg F - 32) * (5/9)")
 
        gui.printlnCond()
        gui.printlnCond("2. Putting in the known values and performing the math, we have")
        gui.printlnCond("   deg C = (" + ASTStr.strFormat(strFromFormat,degIn) + " deg - 32) * (5/9)")
        gui.printlnCond("         = " + ASTStr.strFormat(strToFormat,degOut) + " deg C")
 
        gui.printlnCond()
        gui.printlnCond("Thus, " + ASTStr.strFormat(strFromFormat,degIn) + " degrees F = " + 
                        ASTStr.strFormat(strToFormat,degOut) + " degrees C")
 
    prt.setProportionalFont()
    prt.resetCursor()
    return ASTStr.strFormat(strToFormat,degOut)



def crossMultiply(gui):
    """
    Cross-multiply to do the requested conversion. The data
    to be converted is in the txtboxUserInput field in the GUI.

    :param tkwidget gui: GUI object requesting a cross multiply
    :return: the conversion results as a string
    """
    strInput = ASTStr.removeWhitespace(gui.getUserInput())
    prt = gui.getPrtInstance()
    result = ""
    
    # Get what needs to be converted from the GUI
    strFromFormat = gui.getFromFormat()
    strFromUnits = gui.getFromUnits()
    strToFormat = gui.getToFormat()
    strToUnits = gui.getToUnits()
    dFromDenom = gui.getFromDenom()
    dToDenom = gui.getToDenom()

    rTmp = ASTReal.isValidReal(strInput)
    if not rTmp.isValidRealObj():
        return result
        
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()

    yFrom = rTmp.getRealValue()
    xTo = (yFrom * dToDenom)/dFromDenom
    result = ASTStr.strFormat(strToFormat,xTo)

    prt.setFixedWidthFont()
 
    gui.printlnCond("1. Form an equation relating " + strToUnits + " and " + strFromUnits + ". Since")
    gui.printlnCond("        " + ASTStr.strFormat(strToFormat,dToDenom) + " " + strToUnits + " = " +
                    ASTStr.strFormat(strFromFormat,dFromDenom) + " " + strFromUnits + ",")
    gui.printlnCond("   the equation we must form is")
    gui.printlnCond("        x / (" + ASTStr.strFormat(strToFormat,dToDenom) + " " + strToUnits + ") = " +
                    "(" + ASTStr.strFormat(strFromFormat,yFrom) + " " + strFromUnits + " / " +
                    ASTStr.strFormat(strFromFormat,dFromDenom) + " " + strFromUnits + ")")
 
    gui.printlnCond()
    gui.printlnCond("2. The units on the right side are the same, so cancel them out")
    gui.printlnCond("   and then cross-multiply to get")
    gui.printlnCond("        (x) * (" + ASTStr.strFormat(strFromFormat,dFromDenom) + ")" +
                    " = (" + ASTStr.strFormat(strFromFormat,yFrom) + ") * (" + 
                    ASTStr.strFormat(strToFormat,dToDenom) + " " + strToUnits + "),")
    gui.printlnCond("   which simplifies to")
    gui.printlnCond("        " + ASTStr.strFormat(strFromFormat,dFromDenom) + "x = " +
                    ASTStr.strFormat(strToFormat, yFrom * dToDenom) + " " + strToUnits)
 
    gui.printlnCond()
    gui.printlnCond("3. Divide both sides of the equation by " + ASTStr.strFormat(strFromFormat,dFromDenom) +
                    " to get the answer.")
    gui.printlnCond("   Doing so gives")
    gui.printlnCond("        x = " + result + " " + strToUnits)
 
    gui.printlnCond()
    gui.printlnCond("Thus, " + ASTStr.strFormat(strFromFormat,yFrom) + " " + strFromUnits + " = " +
                     result + " " + strToUnits)

    prt.setProportionalFont()
    prt.resetCursor()
    return result



def convertHorDMS_decHrsorDegs(gui):
    """
    Convert between HMS and decimal hours, and DMS and decimal
    degrees. The process is the same with only the units being
    different between the two.

    :param tkwidget gui: GUI object requesting a cross multiply
    :return: the conversion results as a string
    """
    strInput = ASTStr.removeWhitespace(gui.getUserInput())
    prt = gui.getPrtInstance()
    result = ""
  
    # Get what needs to be converted from the GUI
    strFromFormat = gui.getFromFormat()
    strFromUnits = gui.getFromUnits()
    strToFormat = gui.getToFormat()
    strToUnits = gui.getToUnits()
    dFromDenom = gui.getFromDenom()
    
    # See if we're converting HMS/DMS to dec hrs/degs or vice versa
    if (dFromDenom < 0.0):                      # convert HMS/DMS to decimal hrs/degs
        # see if we're dealing with time or angles based on the menu entry
        if (strFromUnits.upper().find('D') >= 0):
            resultDMSObj = ASTAngle.isValidDMSAngle(strInput)
            if (not resultDMSObj.isValidAngleObj()):
                return result
            strDorH = "Degrees"
            doingHMS = False
            bNeg = resultDMSObj.isNegAngle()
            dORh = resultDMSObj.getDegrees()
            m = resultDMSObj.getMinutes()
            s = resultDMSObj.getSeconds()
            strInput = ASTAngle.angleToStr_obj(resultDMSObj,DMSFORMAT)
        else:
            resultHMSObj = ASTTime.isValidHMSTime(strInput)
            if (not resultHMSObj.isValidTimeObj()):
                return result
            doingHMS = True
            strDorH = "Hours"
            bNeg = resultHMSObj.isNegTime()
            dORh = resultHMSObj.getHours()
            m = resultHMSObj.getMinutes()
            s = resultHMSObj.getSeconds()
            strInput = ASTTime.timeToStr_hms(bNeg,dORh,m,s,HMSFORMAT)            
        
        prt.setBoldFont(True)
        gui.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,CENTERTXT)
        prt.setBoldFont(False)
        gui.printlnCond()
 
        if (bNeg):
            SIGN = -1
        else:
            SIGN = 1
 
        prt.setFixedWidthFont()            
        gui.printlnCond("1. If the " + strFromUnits + " value entered is negative, let SIGN = -1")
        gui.printlnCond("   else let SIGN = 1. Thus, SIGN = " + ASTStr.strFormat("%d",SIGN))
 
        if (bNeg):
            dORh = -dORh                # Temporarily make it negative for the next step
        gui.printlnCond()
        gui.printlnCond("2. Let " + strDorH + " = ABS(" + strDorH + " entered) = ABS(" + 
                        ASTStr.strFormat("%d",dORh) + ") = " +    ASTStr.strFormat("%d",abs(dORh)))
        dORh = abs(dORh)
 
        decValue = s / 60.0
        gui.printlnCond()
        gui.printlnCond("3. Convert seconds entered to decimal minutes (i.e., dm)")
        gui.printlnCond("   dm = seconds / 60 = " + ASTStr.strFormat(strToFormat,s) + 
                        " / 60 = " + ASTStr.strFormat(strToFormat,decValue))
 
        gui.printlnCond()
        gui.printlnCond("4. Add results of step 3 to minutes entered")
        gui.printlnCond("   Total minutes = dm + m = " + ASTStr.strFormat(strToFormat,decValue) + " + " + 
                        ASTStr.strFormat("%d",m) + " = " + ASTStr.strFormat(strToFormat,decValue + m))
        decValue = decValue + m
 
        gui.printlnCond()
        gui.printlnCond("5. Convert total minutes to " + strToUnits)
        gui.printlnCond("   " + strToUnits + " = (Total minutes) / 60 = (" +
                        ASTStr.strFormat(strToFormat,decValue) + ") / 60" + " = " +
                        ASTStr.strFormat(strToFormat,decValue / 60.0))
        decValue = decValue / 60.0
 
        gui.printlnCond()
        gui.printlnCond("6. Add results of step 5 to the " + strDorH + " from step 2")
        gui.printlnCond("   " + strToUnits + " = " + strToUnits + " + " + strDorH +
                        " = " + ASTStr.strFormat(strToFormat,decValue) + " + " + ASTStr.strFormat("%d",dORh) +
                        " = " + ASTStr.strFormat(strToFormat,decValue + dORh))
        decValue = decValue + dORh
  
        gui.printlnCond()
        gui.printlnCond("7. Account for the sign of the " + strDorH + " that were entered")
        gui.printlnCond("   " + strToUnits + " = SIGN * (" + strToUnits + ") = " +
                        ASTStr.strFormat("%d",SIGN) + " * " + ASTStr.strFormat(strToFormat,decValue) +
                        " = " + ASTStr.strFormat(strToFormat,SIGN * decValue))
        decValue = decValue * SIGN
 
        result = ASTStr.strFormat(strToFormat,decValue)
 
        gui.printlnCond()
        gui.printlnCond("Thus, " + strInput + " " + strFromUnits + " = " + result)
    else:                                       # convert decimal hrs/degs to HMS/DMS
        rTmp = ASTReal.isValidReal(strInput)
        if not rTmp.isValidRealObj():
            return result
        decValue = rTmp.getRealValue()
        strInput = ASTStr.strFormat(strFromFormat,decValue)
 
        prt.setBoldFont(True)
        gui.printlnCond("Convert " + strInput + " " + strFromUnits + " to " + strToUnits,CENTERTXT)
        prt.setBoldFont(False)
        gui.printlnCond()
 
        if (strToUnits.lower().find('d') >= 0):       # Figure out if we're doing degrees or time
            doingHMS = False
            strDorH = "Degrees"
        else:
            doingHMS = True
            strDorH = "Hours"
 
        prt.setFixedWidthFont()
        gui.printlnCond("Dec is the decimal number to convert = " + 
                         ASTStr.strFormat(strFromFormat,decValue) + " " + strDorH)
        gui.printlnCond()
 
        if (decValue < 0):
            SIGN = -1
            bNeg = True
        else:
            SIGN = 1
            bNeg = False

        gui.printlnCond("1. If Dec is negative, let SIGN = -1 else let SIGN = 1. Thus, SIGN = " + 
                        ASTStr.strFormat("%d",SIGN))
        gui.printlnCond()
 
        gui.printlnCond("2. Dec = ABS(Dec) = ABS(" + ASTStr.strFormat(strFromFormat,decValue) + ") = " +
                        ASTStr.strFormat(strFromFormat,abs(decValue)))
        decValue = abs(decValue)
 
        dORh = ASTMath.Trunc(decValue)
        gui.printlnCond()
        gui.printlnCond("3. " + strDorH + " = INT(Dec) = INT(" + ASTStr.strFormat(strFromFormat,decValue) + ") = " +
                        ASTStr.strFormat("%d",dORh))
 
        m = 60.0 * ASTMath.Frac(decValue)
        gui.printlnCond()
        gui.printlnCond("4. Minutes = INT[ 60 * FRAC(Dec) ] = " + "INT[ 60 * FRAC(" + 
                        ASTStr.strFormat(strFromFormat,decValue) + ") ]")
        gui.printlnCond("           = " + ASTStr.strFormat("%d",m))
 
        s = ASTMath.Frac(60.0 * ASTMath.Frac(decValue))
        gui.printlnCond()
        gui.printlnCond("5. Seconds = 60 * FRAC[ 60 * FRAC(Dec) ]")
        gui.printlnCond("           = 60 * FRAC[ 60 * FRAC(" + ASTStr.strFormat(strFromFormat,decValue) + ") ]")
        gui.printlnCond("           = " + ASTStr.strFormat(strFromFormat,60.0 * s))
        s = 60.0 * s
 
        gui.printlnCond()
        gui.printlnCond("6. To account for a negative decimal number being converted,")
        gui.printlnCond("   use the SIGN from step 1. Multiply by SIGN if " + strDorH + " is > 0")
        gui.printlnCond("   and merely append '-' if " + strDorH + " is 0. So,")
        if (dORh == 0):
            if (bNeg):
                strSIGN = "-"
            else:
                strSIGN = ""
            gui.printlnCond("   " + strDorH + " = " + strSIGN + "0")
        else:
            gui.printlnCond("   " + strDorH + " = SIGN * " + strDorH + " = (" + ASTStr.strFormat("%d",SIGN) +
                            ") * (" + ASTStr.strFormat("%d",dORh) + ") = " + ASTStr.strFormat("%d",SIGN * dORh))

        if (doingHMS):
            result = ASTTime.timeToStr_hms(bNeg,dORh,m,s,HMSFORMAT)
        else:
            result = ASTAngle.angleToStr_dms(bNeg,dORh,m,s,DMSFORMAT)
 
        gui.printlnCond()
        gui.printlnCond("Thus, " + strInput + " " + strDorH + " = " + result)

    prt.setProportionalFont()
    prt.resetCursor()
    return result



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
