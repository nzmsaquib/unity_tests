"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what menu item to do."""
    # Solar System Info menu
    OBJ_LOCATION = auto()                   # Find an object's position
    OBJ_RISE_SET = auto()                   # Find when an object will rise or set 
    OBJ_DIST_AND_ANG_DIAMETER = auto()      # Calculate an object's distance and angular diameter 
    PLANET_PERI_APHELION = auto()           # Calculate a planet's perihelion/aphelion times 
    OBJ_PERI_APH_DIST = auto()              # Calculate a object's perihelion/aphelion distance 
    OBJ_ILLUMINATION = auto()               # Calculate an object's % illumination 
    OBJ_MAGNITUDE = auto()                  # Calculate an object's visual magnitude 
    OBJ_MISC_DATA = auto()                  # Calculate misc data about an object 
    TERM_CRITERIA = auto()                  # Set a termination criteria for solving Kepler's equation 
    KEPLER_OR_EQOFCENTER = auto()           # Select method for solving true anomaly 
    
    # Data Files menu
    LOAD_ORBITAL_ELEMENTS = auto()          # Load orbital elements data 
    SHOW_ALL_ORBITAL_ELEMENTS = auto()      # Display all orbital elements currently loaded 
    SHOW_OBJ_ORBITAL_ELEMENTS = auto()      # Display an object's orbital elements 
    LOAD_CATALOG = auto()                   # Load a star catalog 
    SHOW_CATALOG_INFO = auto()              # Show catalog header information 
    LIST_ALL_OBJS_IN_CAT = auto()           # List all objects in the catalog 
    LIST_ALL_CONST = auto()                 # List all constellations 
    LIST_ALL_CAT_OBJS_IN_CONST = auto()     # List all objects in a constellation 
    FIND_CONST_CAT_OBJ_IN = auto()          # Find constellation an object is in 
    FIND_CONST_SOLAR_SYS_OBJ_IN = auto()    # Find constellation a solar system object is in 
    
    # Star Charts menu
    SET_mV_FILTER = auto()                  # Set a visual magnitude filter 
    DRAW_ALL_STARS = auto()                 # Draw all stars in the catalog 
    DRAW_ALL_STARS_IN_CONST = auto()        # Draw all stars in a constellation 
    DRAW_ALL_CONST = auto()                 # Draw brightest star in each constellation 
    DRAW_ALL_SOLAR_SYS_OBJS = auto()        # Draw all Solar System objects 
    DRAW_SOLAR_SYS_OBJ = auto()             # Draw a specified Solar System object 
    FIND_OBJ = auto()                       # Locate an object in the catalog 
    FIND_BRIGHTEST_OBJ = auto()             # Locate the brightest object in the catalog 
    FIND_EQ_LOC = auto()                    # Locate an equatorial coordinate location 
    FIND_HORIZ_LOC = auto()                 # Locate a horizon coordinate location
     


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
