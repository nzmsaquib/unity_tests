"""
Implements action listeners for the menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import sys

import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT

import Chap8.ChapEnums
import Chap8.DataFilesActions as df_actions
import Chap8.StarChartActions as sc_actions
import Chap8.SolarSysInfoActions as si_actions

#==================================================
# Define listeners for about, exit, instructions
#==================================================

def aboutMenuListener(gui):
    """
    Handle a click on the About menu item
    
    :param tkwidget gui: GUI that the About Box is associated with
    """
    gui.showAboutBox()
    


def exitMenuListener():
    """Handle a click on the Exit menu item"""
    if ASTMsg.pleaseConfirm("Are you sure you want to exit?"," "):
        sys.exit()    
    


def instructionsMenuListener(gui):
    """
    Handle a click on the Instructions menu item
    
    :param ASTPrt prt: ASTPrt object instance created by the top level GUI
    """
    gui.clearTextAreas()
    prt = gui.getPrtInstance()
    prt.setBoldFont(True)
    prt.println("Instructions",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()

    prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
            "be used in subsequent calculations. You may find it convenient to enter an initial latitude, longitude, " +
            "and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
            "longitude, and time zone are already filled in with default values when the program starts. When this program " +
            "begins, the date and time will default to the local date and time at which the program is started.")
    prt.println()
    prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
            "various calculations are performed. Also, select 'Solar Sys Info->Select True Anomaly Method' to indicate what " +
            "method to use when finding the true anomaly for the Sun or a planet. This menu entry will bring up a window with " +
            "radio buttons corresponding to the methods supported. The radio button 'Equation of Center' will solve " +
            "the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " +
            "'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " +
            "Newton/Raphson method. The menu entry 'Solar Sys Info->Set Termination Critera' allows you to enter the " +
            "termination criteria (in radians) for when to stop iterating to solve Kepler's equation. These radio buttons " +
            "only affect how the true anomaly is computed for the Sun and planets. Finding the Moon's true anomaly is " +
            "accomplished by solving the equation of the center regardless of which of these radio buttons is set.")
    prt.println()
    prt.println("The menu 'Solar Sys Info' performs various calculations related to objects in the Solar System, such as " +
            "determining a planet's location for the current information in the 'Observer Location and Time' data area. The " +
            "menu 'Data Files->Load Orbital Elements' allow you to load data files that contain the orbital elements referenced " +
            "to a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " +
            "starts up. The menu entries under 'Solar Sys Info' in general will work for any object in the orbital elements " +
            "data file except for the Sun, Moon, and Earth. There are, however, some exceptions to this rule. For example, " +
            "'Planetary Perihelion and Aphelion' will not work for Pluto because it is no longer considered to be a planet. It " +
            "does work for Earth because Earth is a planet. As another example, 'Miscellaneous Data' allows any object in " +
            "the orbital elements data file to be selected, but not all data items are computed for every object.")
    prt.println()
    prt.println("The menu 'Data Files' allows orbital elements data files and star catalogs to be loaded. The menu also provides " +
            "entries for listing the constellations, stars in the currently loaded star catalog, and orbital elements " +
            "from the currently loaded orbital elements data file. The last two entries will determine what constellation " +
            "an equatorial coordinate (RA, Decl) falls within as well as what constellation a solar system object falls " +
            "within for the current observer's location.")
    prt.println()
    prt.println("The first menu entry under the 'Star Charts' menu allows the mV filter to be set. This filter affects what stars " +
            "are plotted that is, only those stars that are at least as bright as the mV filter setting will be plotted. " +
            "The Sun, Moon, and planets will be plotted (assuming they are visible) regardless of the mV filter setting. " +
            "The remaining items in this menu allow star charts to be created and displayed. The 'Equatorial Coords Charts' " +
            "checkbox determines what type of star chart to create. If checked, the star chart will be a rectangular plot of " +
            "right ascension and declination (i.e., equatorial coordinates) while if the checkbox " +
            "is not checked, a circular plot of horizon coordinates will be generated. Resizing the application window will " +
            "destroy any star chart that has been created. If this happens, after resizing the window " +
            "to the size you want, simply go through the menu items to redraw the star chart that you want. It is " +
            "typically better to resize the application window to the size you want before generating a star chart.")
    prt.println()
    prt.println("The 'White Bkg for Charts' checkbox allows you to produce charts with a white background (checkbox is checked) " +
            "or a black background (checkbox is not checked). The remaining checkbox, ('Label Objects') indicates whether " +
            "the name of an object should be displayed on the star charts. The menu items under 'Star Charts' provide the " +
            "ability to plot all of the objects in the currently loaded star catalog, only those in a particular " +
            "constellation, just the brightest star in a constellation, or objects in the Solar System.")
    prt.println()
    prt.println("Two cautions are worth noting regarding displaying the names of objects on a star chart. First," +
            "an object's name will appear below where the object is plotted. If an object is near one of the chart's sides," +
            "the label may be only partially displayed. Second, you should plot labels sparingly because plotting " +
            "too many labels will clutter the star chart. For example, check the 'Label Objects' checkbox and then select the " +
            "'Star Charts->Draw ...->Brightest Star in Each Constellation' menu entry to see how cluttered a chart can become!")
    
    prt.println()
    prt.setBoldFont(True)
    prt.println("***Important Note about the Star Charts***",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    prt.printnoln("Python's tkinter graphics library does not support clipping to a region such as a rectangle or a "+
                  "circle. Consequently, portions of objects and text plotted on the display may appear outside " +
                  "the plotting rectangle for an Equatorial Chart and outside the plotting circle for a Horizon Chart.")

    prt.resetCursor()
   


#==================================================
# Define listener for the Conversion menu items
#==================================================

def menuListener(calcToDo,gui):
    """
    Handle a click on the menu items
    
    :param CalculationType calcToDo: the calculation to perform
    :param tkwidget gui: GUI object to which menu items are associated
    """
    cen = Chap8.ChapEnums.CalculationType           # shorten to make typing easier!!!
    
    #************* Solar Sys Info menu
    if (calcToDo == cen.OBJ_LOCATION):
        si_actions.calcObjPosition(gui)
    elif (calcToDo == cen.OBJ_RISE_SET):
        si_actions.calcObjRiseSet(gui)
    elif (calcToDo == cen.OBJ_DIST_AND_ANG_DIAMETER):
        si_actions.calcDistAndAngDiameter(gui)
    elif (calcToDo == cen.PLANET_PERI_APHELION):
        si_actions.calcPlanetPeriAphelion(gui)
    elif (calcToDo == cen.OBJ_PERI_APH_DIST):
        si_actions.calcObjPeriAphDist(gui)
    elif (calcToDo == cen.OBJ_ILLUMINATION):
        si_actions.calcObjIllumination(gui)
    elif (calcToDo == cen.OBJ_MAGNITUDE):
        si_actions.calcObjMagnitude(gui)
    elif (calcToDo == cen.OBJ_MISC_DATA):
        si_actions.calcObjMiscData(gui)
    elif (calcToDo == cen.TERM_CRITERIA):
        si_actions.setTerminationCriteria(gui)
    elif (calcToDo == cen.KEPLER_OR_EQOFCENTER):
        gui.showKeplerDialog()
    
    #************* Data Files menu
    elif (calcToDo == cen.LOAD_ORBITAL_ELEMENTS):
        df_actions.loadOrbitalElements(gui)
    elif (calcToDo == cen.SHOW_ALL_ORBITAL_ELEMENTS):
        df_actions.showAllOrbitalElements(gui)
    elif (calcToDo == cen.SHOW_OBJ_ORBITAL_ELEMENTS):
        df_actions.showObjOrbitalElements(gui)
    elif (calcToDo == cen.LOAD_CATALOG):
        df_actions.loadCatalog(gui)
    elif (calcToDo == cen.SHOW_CATALOG_INFO):
        df_actions.showCatalogInfo(gui)
    elif (calcToDo == cen.LIST_ALL_OBJS_IN_CAT):
        df_actions.listAllObjsInCatalog(gui)
    elif (calcToDo == cen.LIST_ALL_CONST):
        df_actions.listAllConstellations(gui)
    elif (calcToDo == cen.LIST_ALL_CAT_OBJS_IN_CONST):
        df_actions.listAllObjsInConstellation(gui)
    elif (calcToDo == cen.FIND_CONST_CAT_OBJ_IN):
        df_actions.findConstellationForRA_Decl(gui)
    elif (calcToDo == cen.FIND_CONST_SOLAR_SYS_OBJ_IN):
        df_actions.findConstellationForObj(gui)

    #************* Star Charts menu
    elif (calcToDo == cen.DRAW_ALL_STARS):
        sc_actions.drawAllStars(gui)
    elif (calcToDo == cen.DRAW_ALL_STARS_IN_CONST):
        sc_actions.drawAllStarsInConst(gui)
    elif (calcToDo == cen.DRAW_ALL_CONST):
        sc_actions.drawBrightestStarInAllConst(gui)
    elif (calcToDo == cen.DRAW_ALL_SOLAR_SYS_OBJS):
        sc_actions.drawAllSolarSysObjs(gui)
    elif (calcToDo == cen.DRAW_SOLAR_SYS_OBJ):
        sc_actions.drawSolarSysObj(gui)
    elif (calcToDo == cen.FIND_OBJ):
        sc_actions.locateStarCatalogObj(gui)
    elif (calcToDo == cen.FIND_BRIGHTEST_OBJ):
        sc_actions.locateBrightestObjInCat(gui)
    elif (calcToDo == cen.FIND_EQ_LOC):
        sc_actions.locateRADecl(gui)
    elif (calcToDo == cen.FIND_HORIZ_LOC):
        sc_actions.locateAltAz(gui)
    elif (calcToDo == cen.SET_mV_FILTER):
        sc_actions.changemVFilter(gui)

    else:           # This should never happen
        ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
                      "from the 'Solar Sys Info,' 'Data Files,' or 'Star Charts'\nmenu and try again.",
                      "No Menu Item Selected")



#=========== Main entry point ===============
if __name__ == '__main__':
    pass    
