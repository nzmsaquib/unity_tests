"""
Handles the Star Charts menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCatalog as ASTCatalog
import ASTUtils.ASTCharts as ASTCharts
import ASTUtils.ASTConstellation as ASTConstellation
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTMisc as ASTMisc
from ASTUtils.ASTMisc import HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
from ASTUtils.ASTStyle import mVFORMAT
import ASTUtils.ASTTime as ASTTime

import Chap8.Misc as Misc

# Color to use when highlighting an object, brightest object, and located object.
# Also define large mV values to use to display brightest and located objects.
HIGHLIGHT_COLOR = "red"
BRIGHTEST_OBJ_COLOR = "blue"
LOCATE_OBJ_COLOR = "green"
SUN_COLOR = "orange"
MOON_COLOR = "silver"
PLANET_COLOR = "purple"
BRIGHT_mV = -8.0
LOCATE_mV = -12.0



def changemVFilter(gui):
    """
    Ask user for a filter and update the GUI
    
    :param tkwidget gui: GUI object from which the request came
    """
    gui.clearTextAreas()
   
    if (ASTQuery.showQueryForm(["Enter desired visual magnitude filter"]) == ASTQuery.QUERY_OK):
        # validate data
        rTmp = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
        if (rTmp.isValidRealObj()):
            gui.setmVFilter(rTmp.getRealValue())
        else:
            ASTMsg.errMsg("The mV Filter entered is invalid - try again.", "Invalid mV Filter")
            


def drawAllSolarSysObjs(gui):
    """
    Draw all of the objects in the orbital elements
    file, except for the Earth
    
    :param tkwidget gui: GUI object from which the request came
    """  
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()
    idxEarth = ASTOrbits.getOEDBEarthIndex()
    idxSun = ASTOrbits.getOEDBSunIndex()
    idxMoon = ASTOrbits.getOEDBMoonIndex()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelObjs = gui.getLabelObjs()
 
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)
    
    # Cycle through all the objects in the orbital elements data file
    for idx in range(0,ASTOrbits.getNumOEDBObjs()):
    
        # Skip the Earth
        if (idx == idxEarth):
            continue
    
        objName = ASTOrbits.getOEObjName(idx)
        eqCoord = Misc.getSolarSysEqCoord(gui,idx)
        RA = eqCoord.getRAAngle().getDecTime()
        Decl = eqCoord.getDeclAngle().getDecAngle()
        mV = ASTOrbits.getOEObjmV(idx)
        if (idx == idxSun):
            plotColor = SUN_COLOR
        elif (idx == idxMoon):
            plotColor = MOON_COLOR
        else:
            plotColor = PLANET_COLOR
    
        if (eqChart):
            starChart.plotRADecl(RA, Decl, mV, plotColor)
            if (labelObjs):
                starChart.drawLabelRADecl(objName, RA, Decl, plotColor)
        else:
            horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
            # plotAltAz and drawLabelAltAz will ignore if object is below the horizon
            starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                horizonCoord.getAzAngle().getDecAngle(), mV, plotColor)
            if (labelObjs):
                starChart.drawLabelAltAz(objName, horizonCoord.getAltAngle().getDecAngle(),
                                         horizonCoord.getAzAngle().getDecAngle(), plotColor)
                
    starChart.refreshChart()
    gui.setResults(" ... all done plotting Solar System objects ...")



def drawAllStars(gui):
    """
    Draw all of the stars in the catalog that are at
    least as bright as the mV filter.

    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()
    result = ""
    currentmVFilter = gui.getcurrentmVFilter()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if not (ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
        return            # No need to go any further since there is nothing to plot        
      
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
       
    # We can't set that there is a chart on the screen until **after**
    # initDrawingCanvas because initDrawingCanvas exposes the drawing 
    # canvas, which will trigger a resize event
    starChart.initDrawingCanvas(bkgColor,eqChart)
    gui.setChartOnScreen(True)  
    
    # See if we need to bother about pausing during plotting
    plotLimit = ASTMisc.OBJS_LIMIT + 10          # this really means to ignore pausing during display
    if (ASTCatalog.getCatNumObjs() > ASTMisc.OBJS_LIMIT):
        if (ASTMsg.abortMsg("Plotting " + str(ASTCatalog.getCatNumObjs()) + " objects may take a long time.")):
            return
     
        if (ASTQuery.showQueryForm(["Enter Max # of Objs to Plot at a Time\n" +
                                    "(Recommend no More Than " + str(ASTMisc.MAX_OBJS_TO_PLOT) + " Objs)"]) == ASTQuery.QUERY_OK):
            iTmp = ASTInt.isValidInt(ASTQuery.getData(1),HIDE_ERRORS)
            if (iTmp.isValidIntObj()):
                plotLimit = iTmp.getIntValue()
            else:
                plotLimit = ASTMisc.MAX_OBJS_TO_PLOT           

    iConst = -1                # set a bogus constellation
    plotCount = 0
    sTmp = str(ASTCatalog.getCatNumObjs())
    for i in range(0,ASTCatalog.getCatNumObjs()):
     
        plotCount = plotCount + 1
        if (plotCount > plotLimit):
            starChart.refreshChart()        # show what's been plotted so far
            if (ASTMsg.abortMsg("Plotted " + str(i) + " of " + sTmp + " objects")):
                break
            plotCount = 0
     
        RA = ASTCatalog.getCatObjRA(i)
        Decl = ASTCatalog.getCatObjDecl(i)
        if (ASTCatalog.getCatObjConstIdx(i) != iConst):
            iConst = ASTCatalog.getCatObjConstIdx(i)
            result = "Plotting constellation " + ASTConstellation.getConstName(iConst)
            gui.setResults(result)
     
        # See if this object should be filtered out
        if (ASTCatalog.getCatObjmVKnown(i)):
            mV = ASTCatalog.getCatObjmV(i)
            showObj = (mV <= currentmVFilter)
        else:
            showObj = True                     # show object even if we don't know its mV value
            mV = ASTMisc.mV_NAKED_EYE          # default to what the naked eye can see
     
        if (showObj):
            if (eqChart):
                starChart.plotRADecl(RA,Decl,mV)
            else:
                horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
                starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),horizonCoord.getAzAngle().getDecAngle(),mV)
    
    starChart.refreshChart()
    gui.setResults(" ... all done plotting ...")

    
    
def drawAllStarsInConst(gui):
    """
    Draw all of the stars in the catalog within a user specified
    constellation that are at least as bright as the mV filter.
    Also draw the brightest star in the constellation.

    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()
    currentmVFilter = gui.getcurrentmVFilter()

    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if not (ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
        return            # No need to go any further since there is nothing to plot    
    
    if (ASTQuery.showQueryForm(["Enter Constellation's 3 Character\nAbbreviated Name"]) == ASTQuery.QUERY_OK):
        constAbbrevName = ASTQuery.getData(1)
        if ((constAbbrevName == None) or (len(constAbbrevName) <= 0)):
            return
        iConst = ASTConstellation.findConstellationByAbbrvName(constAbbrevName.strip())
        if (iConst < 0):
            ASTMsg.errMsg("No Constellation whose abbreviated name is '" + constAbbrevName + "' was found",
                          "Invalid Constellation")
            return
    else:
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelStars = gui.getLabelObjs()

    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)

    gui.setResults("Plotting constellation " + ASTConstellation.getConstName(iConst))
    
    brightRA = ASTConstellation.getConstBrightestStarRA(iConst)
    brightDecl = ASTConstellation.getConstBrightestStarDecl(iConst)

    # Cycle through the catalog and get only those objects in the specified constellation
    for i in range(0,ASTCatalog.getCatNumObjs()):
        RA = ASTCatalog.getCatObjRA(i)
        Decl = ASTCatalog.getCatObjDecl(i)
        if (ASTCatalog.getCatObjConstIdx(i) == iConst):
            # See if this object should be filtered out
            if (ASTCatalog.getCatObjmVKnown(i)):
                mV = ASTCatalog.getCatObjmV(i)
                showObj = (mV <= currentmVFilter)
            else:
                showObj = True                   # show object even if we don't know its mV value
                mV = ASTMisc.mV_NAKED_EYE        # default to what the naked eye can see
    
            if (showObj):
                if (eqChart):
                    starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
                else:
                    horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
                    starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                        horizonCoord.getAzAngle().getDecAngle(), mV, HIGHLIGHT_COLOR)
 
    # Now draw the brightest star in the constellation
    if (eqChart):
        starChart.plotRADecl(brightRA, brightDecl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
        if (labelStars):
            starChart.drawLabelRADecl(ASTConstellation.getConstBrightestStarName(iConst), 
                                      brightRA, brightDecl, BRIGHTEST_OBJ_COLOR)
    else:
        horizonCoord = ASTCoord.RADecltoHorizon(brightRA, brightDecl, observer.getObsLat(), LST)
        # plotAltAz and drawLabelAltAz will ignore plotting if below the horizon
        starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                            horizonCoord.getAzAngle().getDecAngle(), BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
        if (labelStars):
            starChart.drawLabelAltAz(ASTConstellation.getConstBrightestStarName(iConst),
                                     horizonCoord.getAltAngle().getDecAngle(),horizonCoord.getAzAngle().getDecAngle(),
                                     BRIGHTEST_OBJ_COLOR)

    starChart.refreshChart()
    gui.setResults("Brightest star in " + ASTConstellation.getConstName(iConst) +
                   " is " + ASTConstellation.getConstBrightestStarName(iConst))    



def drawBrightestStarInAllConst(gui):
    """
    Draw the brightest star in each of the constellations.
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()

    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelStars = gui.getLabelObjs() 
    
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)
 
    if (labelStars):
        if not (ASTMsg.pleaseConfirm("Labelling the stars may make the display\n" +
                                     "unreadable. Do you wish to continue anyway?", " ")):
            return

    for i in range(0,ASTConstellation.getNumConstellations()):
        gui.setResults("Plotting brightest star in constellation " + ASTConstellation.getConstName(i))
    
        RA = ASTConstellation.getConstBrightestStarRA(i)
        Decl = ASTConstellation.getConstBrightestStarDecl(i)
    
        if (eqChart):
            starChart.plotRADecl(RA, Decl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
            if (labelStars):
                starChart.drawLabelRADecl(ASTConstellation.getConstBrightestStarName(i), 
                                          RA, Decl, BRIGHTEST_OBJ_COLOR)
        else:
            horizonCoord = ASTCoord.RADecltoHorizon(RA,Decl, observer.getObsLat(), LST)
            starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                horizonCoord.getAzAngle().getDecAngle(), BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
            if (labelStars):
                starChart.drawLabelAltAz(ASTConstellation.getConstBrightestStarName(i),
                                         horizonCoord.getAltAngle().getDecAngle(), horizonCoord.getAzAngle().getDecAngle(),
                                         BRIGHTEST_OBJ_COLOR)

    starChart.refreshChart()
    gui.setResults("Finished plotting brightest stars in each constellation ...")



def drawSolarSysObj(gui):
    """
    Draw an object in the orbital elements
    file, except for the Earth
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()
    idxSun = ASTOrbits.getOEDBSunIndex()
    idxMoon = ASTOrbits.getOEDBMoonIndex()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    idx = Misc.getValidObj(gui,Misc.GETSUN,not Misc.GETEARTH,Misc.GETMOON)
    if (idx < 0):
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelObjs = gui.getLabelObjs()
    
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)
    
    objName = ASTOrbits.getOEObjName(idx)
    eqCoord = Misc.getSolarSysEqCoord(gui,idx)
    RA = eqCoord.getRAAngle().getDecTime()
    Decl = eqCoord.getDeclAngle().getDecAngle()
    mV = ASTOrbits.getOEObjmV(idx)
    if (idx == idxSun):
        plotColor = SUN_COLOR
    elif (idx == idxMoon):
        plotColor = MOON_COLOR
    else:
        plotColor = PLANET_COLOR
     
    if (eqChart):
        starChart.plotRADecl(RA, Decl, mV, plotColor)
        if (labelObjs):
            starChart.drawLabelRADecl(objName, RA, Decl, plotColor)
    else:
        horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
        if (horizonCoord.getAltAngle().getDecAngle() > 0):
            starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                horizonCoord.getAzAngle().getDecAngle(), mV, plotColor)
            if (labelObjs):
                starChart.drawLabelAltAz(objName, horizonCoord.getAltAngle().getDecAngle(),
                                         horizonCoord.getAzAngle().getDecAngle(), plotColor)
        else:
            ASTMsg.infoMsg("The object '" + objName + "' is below the observer's horizon")
    
    starChart.refreshChart()
    if ((idx == idxSun) or (idx == idxMoon)):
        result = "The "
    else:
        result = ""
    gui.setResults(result + objName + " is at " + ASTTime.timeToStr_dec(RA, ASTMisc.HMSFORMAT) +
                   " RA, " + ASTAngle.angleToStr_dec(Decl, ASTMisc.DMSFORMAT) + " Decl")



def locateAltAz(gui):
    """
    Locate a horizon coordinate that the user specifies
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()

    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelStars = gui.getLabelObjs() 
    
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)
    
    if (ASTQuery.showQueryForm(["Enter Altitude (xxxd yym zz.zzs)","Enter Azimuth (xxxd yym zz.zzs)"]) != ASTQuery.QUERY_OK):
        return
    
    # validate data
    altObj = ASTAngle.isValidAngle(ASTQuery.getData(1),HIDE_ERRORS)
    if not (altObj.isValidAngleObj()):
        ASTMsg.errMsg("The Altitude entered is invalid - try again.", "Invalid Altitude")
        return
    azObj = ASTAngle.isValidAngle(ASTQuery.getData(2),HIDE_ERRORS)
    if not (azObj.isValidAngleObj()):
        ASTMsg.errMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth")
        return
    
    if (eqChart):
        eqCoord = ASTCoord.HorizontoRADecl(altObj.getDecAngle(), azObj.getDecAngle(), observer.getObsLat(), LST)
        starChart.plotRADecl(eqCoord.getRAAngle().getDecTime(), eqCoord.getDeclAngle().getDecAngle(),
                             LOCATE_mV, LOCATE_OBJ_COLOR)
        if (labelStars):
            starChart.drawLabelRADecl("LOC", eqCoord.getRAAngle().getDecTime(),
                                      eqCoord.getDeclAngle().getDecAngle(), LOCATE_OBJ_COLOR)
    else:
        if (altObj.getDecAngle() < 0.0):
            ASTMsg.infoMsg("The stated coordinate is below the observer's horizon")
        else:
            starChart.plotAltAz(altObj.getDecAngle(), azObj.getDecAngle(), LOCATE_mV, LOCATE_OBJ_COLOR)
            if (labelStars):
                starChart.drawLabelAltAz("LOC", altObj.getDecAngle(), azObj.getDecAngle(), LOCATE_OBJ_COLOR)
    
    starChart.refreshChart()
    gui.setResults(ASTAngle.angleToStr_dec(altObj.getDecAngle(), ASTMisc.DMSFORMAT) + " Alt, " + 
                   ASTAngle.angleToStr_dec(azObj.getDecAngle(),ASTMisc.DMSFORMAT) + " Az")



def locateBrightestObjInCat(gui):
    """
    Locate and highlight the brightest object in the catalog
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()
    idx = 0
    brightestmV = ASTMisc.UNKNOWN_mV

    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if not (ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
        return            # No need to go any further since there is nothing to plot
    
    if not (ASTCatalog.getCatmVProvided()):
        ASTMsg.infoMsg("The currently loaded catalog does not provide magnitudes...")
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelStars = gui.getLabelObjs() 
    
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)
    
    # Cycle through and find brightest object
    for i in range(0,ASTCatalog.getCatNumObjs()):
        mV = ASTCatalog.getCatObjmV(i)
        if (mV < brightestmV):
            brightestmV = mV
            idx = i

    RA = ASTCatalog.getCatObjRA(idx)
    Decl = ASTCatalog.getCatObjDecl(idx)
    mV = ASTCatalog.getCatObjmV(idx)
    
    if (eqChart):
        starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
        if (labelStars):
            starChart.drawLabelRADecl(ASTCatalog.getCatObjName(idx), RA, Decl, HIGHLIGHT_COLOR)
    else:
        horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
        if (horizonCoord.getAltAngle().getDecAngle() < 0):
            ASTMsg.infoMsg("The object '" + ASTCatalog.getCatObjName(idx) + "' is below the observer's horizon")
        else:
            starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                horizonCoord.getAzAngle().getDecAngle(), mV, HIGHLIGHT_COLOR)
            if (labelStars):
                starChart.drawLabelAltAz(ASTCatalog.getCatObjName(idx),
                                         horizonCoord.getAltAngle().getDecAngle(),
                                         horizonCoord.getAzAngle().getDecAngle(), HIGHLIGHT_COLOR)       

    starChart.refreshChart()
    gui.setResults("Brightest Object is '" + ASTCatalog.getCatObjName(idx) + "' at " + 
                   ASTTime.timeToStr_dec(RA, ASTMisc.HMSFORMAT) + " RA, " + 
                   ASTAngle.angleToStr_dec(Decl, ASTMisc.DMSFORMAT) + " Decl, " + 
                   ASTStr.strFormat(mVFORMAT,mV) + " mV")



def locateRADecl(gui):
    """
    Locate an equatorial coordinate that the user specifies
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()

    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    LST = Misc.computeLST(gui)
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelStars = gui.getLabelObjs() 
    
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)
    
    if (ASTQuery.showQueryForm(["Enter Right Ascension (hh:mm:ss.ss)",
                                "Enter Declination (xxxd yym zz.zzs)"]) != ASTQuery.QUERY_OK):
        return
    
    # validate data
    raObj = ASTTime.isValidTime(ASTQuery.getData(1),HIDE_ERRORS)
    if not (raObj.isValidTimeObj()):
        ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA")
        return
    declObj = ASTAngle.isValidAngle(ASTQuery.getData(2),HIDE_ERRORS)
    if not (declObj.isValidAngleObj()):
        ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl")
        return

    if (eqChart):
        starChart.plotRADecl(raObj.getDecTime(), declObj.getDecAngle(), LOCATE_mV, LOCATE_OBJ_COLOR)
        if (labelStars):
            starChart.drawLabelRADecl("LOC", raObj.getDecTime(), declObj.getDecAngle(), LOCATE_OBJ_COLOR)
    else:
        horizonCoord = ASTCoord.RADecltoHorizon(raObj.getDecTime(), declObj.getDecAngle(), observer.getObsLat(), LST)
        if (horizonCoord.getAltAngle().getDecAngle() < 0.0):
            ASTMsg.infoMsg("The stated coordinate is below the observer's horizon")
        else:
            starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                horizonCoord.getAzAngle().getDecAngle(), LOCATE_mV, LOCATE_OBJ_COLOR)
            if (labelStars):
                starChart.drawLabelAltAz("LOC", horizonCoord.getAltAngle().getDecAngle(),
                                         horizonCoord.getAzAngle().getDecAngle(), LOCATE_OBJ_COLOR)
    
    starChart.refreshChart()
    gui.setResults(ASTTime.timeToStr_dec(raObj.getDecTime(), ASTMisc.HMSFORMAT) + " RA, " +
                   ASTAngle.angleToStr_dec(declObj.getDecAngle(), ASTMisc.DMSFORMAT) + " Decl")



def locateStarCatalogObj(gui):
    """
    Locate and highlight a user-specified object from the catalog
    
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    starChart = gui.getStarChart()
    
    # Validate the observer location data
    if not (gui.validateGUIObsLoc()):
        return
    
    if not (ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
        return            # No need to go any further since there is nothing to plot        
    
    if (gui.getWhiteChartStatus()):
        bkgColor = ASTCharts.WHITE_BKG
    else:
        bkgColor = ASTCharts.BLACK_BKG
    
    eqChart = gui.getEQChartStatus()
    labelStars = gui.getLabelObjs()
    
    if (ASTQuery.showQueryForm(["Enter the Object's Name"]) == ASTQuery.QUERY_OK):
        searchStr = ASTQuery.getData(1)
        if ((searchStr == None) or (len(searchStr) <= 0)):
            return
        idx = ASTCatalog.findObjByName(searchStr.strip())
        if (idx < 0):
            ASTMsg.errMsg("No Object with the name '" + searchStr + "' was found in the catalog",
                          "Invalid Object Name")
            return
    else:
        return
    
    LST = Misc.computeLST(gui)
    
    # If there's already a plot on the screen, don't erase it
    if not (gui.getChartOnScreen()):
        starChart.initDrawingCanvas(bkgColor,eqChart)

    gui.setChartOnScreen(True)

    RA = ASTCatalog.getCatObjRA(idx)
    Decl = ASTCatalog.getCatObjDecl(idx)
    
    # Show the object regardless of whether it is within the mV filter
    if (ASTCatalog.getCatObjmVKnown(idx)):
        mV = ASTCatalog.getCatObjmV(idx)
    else:
        mV = ASTMisc.mV_NAKED_EYE                # default to what the naked eye can see
    
    if (eqChart):
        starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
        if (labelStars):
            starChart.drawLabelRADecl(searchStr, RA, Decl, HIGHLIGHT_COLOR)
    else:
        horizonCoord = ASTCoord.RADecltoHorizon(RA, Decl, observer.getObsLat(), LST)
        if (horizonCoord.getAltAngle().getDecAngle() < 0.0):
            ASTMsg.infoMsg("The object '" + searchStr + "' is below the observer's horizon")
        else:
            starChart.plotAltAz(horizonCoord.getAltAngle().getDecAngle(),
                                horizonCoord.getAzAngle().getDecAngle(), mV, HIGHLIGHT_COLOR)
            if (labelStars):
                starChart.drawLabelAltAz(searchStr, horizonCoord.getAltAngle().getDecAngle(),
                                         horizonCoord.getAzAngle().getDecAngle(),HIGHLIGHT_COLOR)

    starChart.refreshChart()
    gui.setResults("The object '" + searchStr + "' is at " + 
                   ASTTime.timeToStr_dec(RA, ASTMisc.HMSFORMAT) + " RA, " +
                   ASTAngle.angleToStr_dec(Decl, ASTMisc.DMSFORMAT) + " Decl")



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
