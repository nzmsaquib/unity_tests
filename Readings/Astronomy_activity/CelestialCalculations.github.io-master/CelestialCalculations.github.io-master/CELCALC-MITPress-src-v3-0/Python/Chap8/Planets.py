"""
Provides some miscellaneous data and functions related to the planets

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTStr as ASTStr

# Define data about the planets for computing perihelion/aphelion. The data
# entries are in the form (planet name, k0, k1, j0, j1, j2). See the book
# for an explanation of k0, k1, j0, j1, j2
_NUM_PLANETS = 8
_planetsTable = [("Mercury", 4.15201, 2000.12, 2451590.257, 87.96934963, 0.0),
                 ("Venus", 1.62549, 2000.53, 2451738.233, 224.7008188, -0.0000000327),
                 ("Earth", 0.99997, 2000.01, 2451547.507, 365.2596358, 0.0000000156),
                 ("Mars", 0.53166, 2001.78, 2452195.026, 686.9957857, -0.0000001187),
                 ("Jupiter", 0.0843, 2011.2, 2455636.936, 4332.897065, 0.0001367),
                 ("Saturn", 0.03393, 2003.52, 2452830.12, 10764.21676, 0.000827),
                 ("Uranus", 0.0119, 2051.1, 2470213.5, 30694.8767, -0.00541),
                 ("Neptune", 0.00607, 2047.5, 2468895.1, 60190.33, 0.03429)]



#===========================================================================
# Define 'getters' for the planetsTable. In each case, an index is passed
# indicating which planet is of interest. No checks are made to be sure
# the index is valid, so caller beware! 0-based indexing is assumed.
#===========================================================================

def getPlanetName(i):
    return _planetsTable[i][0]
    
def getk0(i):
    return _planetsTable[i][1]

def getk1(i):
    return _planetsTable[i][2]

def getj0(i):
    return _planetsTable[i][3]

def getj1(i):
    return _planetsTable[i][4]

def getj2(i):
    return _planetsTable[i][5]

def getNumPlanets():
    return _NUM_PLANETS



#===============================================================================
# Various planet-related routines for the algorithms in this chapter's program
#===============================================================================

def findPlanetIdx(sName):
    """
    Find the index into the planetsTable for the given name.
    
    :param str sName: name of the planet to find
    :return: int index into planetsTable for the planet or -1 if the given name doesn't exist
    """
    s = ASTStr.removeWhitespace(sName)
    
    for i in range(0,getNumPlanets()):
        if (ASTStr.compareIgnoreCase(s,getPlanetName(i))):
            return i
         
    return -1        # if we get here, the passed in sName was not found



def getValidPlanet(gui):
    """
    Ask user for a planet's name and return its index from the currently
    loaded orbital elements. Pluto is **not** considered to be a planet in this context.

    :param tkwidget gui: GUI object from which the request came
    :return: -1 if object is invalid or not allowed, else returns index
             into the orbital elements array.
    """   
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return -1
    if not (gui.checkOEDBLoaded()):
        return -1
    
    if (ASTQuery.showQueryForm(["Enter Object's Name (must be a planet!)"]) != ASTQuery.QUERY_OK):
        return -1
    
    # validate data
    objName = ASTQuery.getData(1)
    idx = ASTOrbits.findOrbElementObjIndex(objName.strip())
    if (idx < 0):
        ASTMsg.errMsg("No Planet with the name '" + objName + "' was found","Invalid Planet Name")
        return -1

    # This only works for the planets, so make sure the user specified a planet
    # The orbital elements file could have non-planets in it (e.g, Pluto, Ceres)
    if not (__isAPlanet(idx)):
        ASTMsg.errMsg("This method only works for Solar System planets\n" +
                      "(Remember, Pluto is not a planet)", "Invalid Choice")
        return -1
    
    return idx



#=====================================================================
# Private functions used only in this module
#=====================================================================

def __isAPlanet(idx):
    """
    Determines whether an object is a planet. This method
    does **not** count Pluto as a planet.
    
    :param int idx: index into orbital elements for the object under consideration
    :return: true if the object is a planet in the currently loaded orbital elements.
    """
    for i in range(0,getNumPlanets()):
        if (idx == ASTOrbits.findOrbElementObjIndex(getPlanetName(i))):
            return True
        
    return False



#=========== Main entry point ===============
if __name__ == '__main__':
    pass 