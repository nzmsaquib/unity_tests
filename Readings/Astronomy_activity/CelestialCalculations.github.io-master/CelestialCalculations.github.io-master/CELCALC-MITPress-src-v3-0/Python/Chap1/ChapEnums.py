"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what menu item was selected."""
    # Constellations menu
    LISTCONSTBYNAME = auto()
    LISTCONSTBYABBREVNAME = auto()
    LISTCONSTBYMEANING = auto()
    LISTALLCONST = auto()
    FINDCONST = auto()

    # Star Catalogs menu
    CLEARCAT = auto()
    LOADCAT = auto()
    SHOWCATINFO = auto()
    
    # Space Objects menu
    #   List Object by ...
    LISTOBJBYNAME = auto()
    LISTOBJBYALTNAME = auto()
    LISTOBJBYCOMMENTS = auto()
    #   List all Objects by ...
    LISTALLOBJSINCAT = auto()
    LISTALLOBJSINRANGE = auto()
    LISTALLOBJSINCONST = auto()
    #   Sort catalog by ...
    SORTCATBYCONST = auto()
    SORTCATBYCONSTANDOBJNAME = auto()
    SORTCATBYOBJNAME = auto()
    SORTCATBYOBJALTNAME = auto()
    SORTCATBYOBJRA = auto()
    SORTCATBYOBJDECL = auto()
    SORTCATBYOBJMV = auto()
 


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
