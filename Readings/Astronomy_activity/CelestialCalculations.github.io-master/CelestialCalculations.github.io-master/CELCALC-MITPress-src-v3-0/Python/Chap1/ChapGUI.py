"""
Implements the main GUI for the application
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.scrolledtext as tkst

import ASTUtils.ASTAboutBox as ASTAboutBox
import ASTUtils.ASTBookInfo as ASTBookInfo
from ASTUtils.ASTMisc import centerWindow, DEFAULT_EPOCH, ASCENDING_ORDER, DESCENDING_ORDER  
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTPrt as ASTPrt
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle

import Chap1.ChapMenuItems as cmi
import Chap1.MenusListener as ml

class ChapGUI():
    """
    Define a class for this chapter's main application window.
    Although the code is written to allow multiple top level
    application windows, there really should only be one.
    """
    #==================================================
    # Class variables
    #==================================================
    WINDOW_TITLE = "Chapter 1 - Introduction"
    # Maximum length of a filename to display. Actual filename can be longer, but only a limited
    # number of characters can be displayed in the GUI.
    MAX_FNAME_DISPLAY = 95
    
    #==================================================
    # Class instance variables
    #==================================================
    # aboutCallback                     callback for the About Box menu item
    # exitCallback                      callback for the Exit menu item
    # instructionsCallback              callback for the Instructions menu item    
    # aboutBox                          instance of an ASTAboutBox object
    # prt                               instance of an ASTPrt object for doing output
    # textPane                          the scrollable text output area
    
    # chkboxSortOrder                   how to sort objects in a displayed list (ascending order if true)
    # lblEpoch                          Epoch to which catalog contents are referenced
    # lblFilename                       File from which star catalog was read
    # libCatalogType                    type of star catalog (NGC, SAO, etc.)
    
    def __init__(self,root):
        """
        Constructor for creating a GUI object in the root window.
        The root window (e.g., root = Tk()) must be created external
        to this object.

        :param tkwidget root: window for this object
        """
        # To avoid an annoying 'flash' as widgets are added to the GUI,
        # we must hide it and then make it visible after all widgets have
        # been created.
        root.withdraw()
        
        # create dynamic variables for various widgets
        self.chkboxSortOrder = tk.BooleanVar()         # current state of the sorting order checkbox
        self.lblEpoch = tk.StringVar()
        self.lblFilename = tk.StringVar()
        self.lblCatalogType = tk.StringVar()
        
        # define listeners for menu items
        self.aboutCallback = lambda: ml.aboutMenuListener(self)
        self.exitCallback = lambda: ml.exitMenuListener()
        self.instructionsCallback = lambda: ml.instructionsMenuListener(self.prt)
    
        # Initial size of the application window
        winWidth = 980
        winHeight = 740
           
        # create the root window and center it after all the widgets are created
        root.title(ChapGUI.WINDOW_TITLE)
        root.iconbitmap('BlueMarble.ico')
        root.geometry('{}x{}+{}+{}'.format(winWidth,winHeight,100,100))
        
        #=====================================================
        # create the menu bar
        #=====================================================
        # Note: In the code below, the menu font for cascades may not work because
        # tkinter conforms to the underlying OS's rules for the font and style for
        # top level menu items. If the underlying OS handles the font at the system
        # level (as MS Windows does), then any attempt in the code to change the menu font
        # will not work. However, setting the menu font for individual commands
        # does work.        
        
        menubar = tk.Menu(root)
        
        # create the File menu
        filemenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" File ",menu=filemenu)
        filemenu.add_command(label="Exit",command=self.exitCallback)
        
        # create the chapter menus
        cmi.createChapMenuItems(self,menubar)
        
        # create the Help menu
        helpmenu=tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" Help",menu=helpmenu)
        helpmenu.add_command(label="Instructions",command=self.instructionsCallback)
        helpmenu.add_command(label="About",command=self.aboutCallback)

        root.config(menu=menubar)
        
        #=====================================================
        # create the rest of the gui widgets
        #=====================================================
     
        # create the book title
        tk.Label(root,text=ASTBookInfo.BOOK_TITLE,font=ASTStyle.BOOK_TITLE_BOLDFONT,
                 fg=ASTStyle.BOOK_TITLE_COLOR,bg=ASTStyle.BOOK_TITLE_BKG).pack(fill=tk.X,expand=False)

        # create the intermediate calculations checkbox
        tk.Checkbutton(root,text="Sort in Ascending Order",
                       variable=self.chkboxSortOrder,
                       onvalue=True,font=ASTStyle.CBOX_FONT).pack(fill=tk.X,expand=False)
 
        # Create the catalog type, epoch, and filename labels
        txtWidth=40
        f = tk.LabelFrame(root,padx=5,pady=2,bd=0)
        f.columnconfigure(0,weight=5)
        f.columnconfigure(1,weight=0)
        tk.Label(f,width=txtWidth,textvariable=self.lblCatalogType,font=ASTStyle.TEXT_BOLDFONT,
                 anchor="w").grid(row=0,column=0,sticky=tk.W)
        tk.Label(f,width=15,font=ASTStyle.TEXT_BOLDFONT,textvariable=self.lblEpoch,
                 anchor="e").grid(row=0,column=1,sticky=tk.E,padx=15)
        tk.Label(f,textvariable=self.lblFilename,font=ASTStyle.TEXT_BOLDFONT,
                 anchor="w").grid(row=1,column=0,columnspan=2,sticky=tk.W)                                 
        f.pack(fill=tk.X,expand=False)
                 
        # create the scrollable text area
        self.textPane = tkst.ScrolledText(root,font=ASTStyle.TEXT_FONT,padx=5,pady=5,borderwidth=5,wrap="word")
        self.textPane.pack(fill=tk.BOTH,expand=True)

        # center the window
        centerWindow(root,winWidth,winHeight)
        
        # Initialize the rest of the GUI)
        self.chkboxSortOrder.set(True)
        self.setEpoch(DEFAULT_EPOCH)
        self.setCatalogType("")
        self.setFilename("") 
        # Set parent frame for functions that need to relate to a pane/frame/etc. on the GUI
        ASTMsg.setParentFrame(root)
        ASTQuery.setParentFrame(root)
        self.prt = ASTPrt.ASTPrt(self.textPane)
        self.prt.clearTextArea()

        # Finally, make the GUI visible and create a
        # reusable About Box. For some reason, tkinter requires
        # the parent window to be visible before the About Box
        # can be created.
        root.deiconify()
        self.aboutBox = ASTAboutBox.ASTAboutBox(root,ChapGUI.WINDOW_TITLE)
        
    #=====================================================================
    # The methods below conditionally print to the scrollable text area
    #=====================================================================
    
    def printCond(self,txt,center=False):
        """
        Print text to scrollable output area if the
        'Show Intermediate Calculations' checkbox
        is checked.

        :param str txt: text to be printed
        :param bool center: True if text is to be centered
        """          
        if self.chkboxShowInterimCalcs.get():
            self.prt.printnoln(txt,center)    
        
    def printlnCond(self,txt="",centerTxt=False):
        """
        Routines to handle sending output text to the scrollable
        output area. These are wrappers around ASTPrt.println
        that see if the 'Show Intermediate Calculations' checkbox
        is checked before invoking the println functions.

        :param str txt: text to be printed
        :param bool centerTxt: true if the text is to be centered
        """            
        
        if self.chkboxShowInterimCalcs.get():
            self.prt.println(txt,centerTxt)

    #==================================================================================
    # Define various methods for manipulating the GUI
    #==================================================================================
        
    def setCatalogType(self,cat_type):
        """
        Sets the Catalog Type label in the GUI.
        
        :param str cat_type: the Catalog Type (string) to be displayed in the GUI
        """
        self.lblCatalogType.set("Catalog Type: " + cat_type)

    def setEpoch(self,epoch):
        """
        Sets the Epoch label in the GUI.
        
        :param float epoch: the epoch to be displayed in the GUI
        """
        self.lblEpoch.set("Epoch: " + ASTStr.strFormat(ASTStyle.EPOCHFORMAT,epoch))
    
    def setFilename(self,fname):
        """
        Sets the filename label to be displayed in the GUI.
        
        :param str fname: the filename (with pathname) to be displayed in the GUI
        """
        self.lblFilename.set("File: " + ASTStr.abbrevRight(fname,ChapGUI.MAX_FNAME_DISPLAY))
        
    #============================================================================
    # The methods below return references to various GUI components such as the
    # scrollable output text area.
    #============================================================================        
        
    def getPrtInstance(self):
        """
        Gets the ASTPrt instance for this application's scrollable text pane area.
        
        :return: the ASTPrt object for printing to the scrollable text area
        """         
        return self.prt
    
    def showAboutBox(self):
        """Shows the About Box."""
        self.aboutBox.show() 
        
    def getSortOrderChkbox(self):
        """
        Gets the current checkbox setting for the sort order.
        
        :return: ASCENDING_ORDER or DESCENDING_ORDER
        """
        if (self.chkboxSortOrder.get()):
            return ASCENDING_ORDER
        else:
            return DESCENDING_ORDER             
        


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
