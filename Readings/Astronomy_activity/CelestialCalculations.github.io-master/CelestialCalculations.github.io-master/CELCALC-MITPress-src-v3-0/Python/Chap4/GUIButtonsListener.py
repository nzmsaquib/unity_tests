"""
Implements action listeners for the GUI buttons

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTLatLon as ASTLatLon
import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTMisc import HIDE_ERRORS, HMSFORMAT, DECFORMAT,DMSFORMAT
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

import Chap4.ChapEnums

# Galactic coordinates for Epoch 1950.0
GalEpoch1950 = 1950.0
GalRA1950 = 192.25                    # in degrees
GalDecl1950 = 27.4                    # in degrees
GalAscNode1950 = 33.0                 # in degrees

# Galactic coordinates for Epoch J2000
GalEpoch2000 = 2000.0
GalRA2000 = 192.8598                  # in degrees
GalDecl2000 = 27.128027               # in degrees
GalAscNode2000 = 32.9319              # in degrees



def calculateListener(gui):
    """
    Handle a click on the Calculate button
    
    :param tkwidget gui: GUI object from which the request came
    """
    cen = Chap4.ChapEnums.CalculationType  # shorten to make typing easier!!
   
    calcToDo = gui.getCalcToDo()
    
    #****************Coordinate System Conversions
    if (calcToDo == cen.HA_RA):
        calcHA_RA(gui)
    elif (calcToDo == cen.RA_HA):
        calcRA_HA(gui)
    elif (calcToDo == cen.HORIZON_EQUATORIAL):
        calcHorizon_Equatorial(gui)
    elif (calcToDo == cen.EQUATORIAL_HORIZON):
        calcEquatorial_Horizon(gui)
    elif (calcToDo == cen.ECLIPTIC_EQUATORIAL):
        calcEcliptic_Equatorial(gui)
    elif (calcToDo == cen.EQUATORIAL_ECLIPTIC):
        calcEquatorial_Ecliptic(gui)
    elif (calcToDo == cen.GALACTIC_EQUATORIAL):
        calcGalactic_Equatorial(gui)
    elif (calcToDo == cen.EQUATORIAL_GALACTIC):
        calcEquatorial_Galactic(gui)
    elif (calcToDo == cen.PRECESSION_CORR):
        calcPrecessionCorr(gui)
    
    #****************Kepler's Equation menu
    elif (calcToDo == cen.KEPLER_SIMPLE):
        calcKeplerSimple(gui)
    elif (calcToDo == cen.KEPLER_NEWTON):
        calcKeplerNewton(gui)
    
    else:
        ASTMsg.errMsg("No menu item has been selected. Choose a menu item from\n" + 
                    "the 'Coord Systems' or 'Kepler's Equation' menu and try again.",
                    "No Menu Item Selected")



#====================================================================
# Routines to do the actual work for the Coord Sys menu
#====================================================================

def calcEcliptic_Equatorial(gui):
    """
    Convert Ecliptic Coordinates to Equatorial Coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    EclLatInput = ASTStr.removeWhitespace(gui.getData1())
    EclLonInput = ASTStr.removeWhitespace(gui.getData2())
    EpochInput = ASTStr.removeWhitespace(gui.getData3())
    
    prt.clearTextArea()
    
    # Validate the Ecliptic Latitude and Longitude
    eclLatObj = ASTAngle.isValidAngle(EclLatInput,HIDE_ERRORS)
    if (eclLatObj.isValidAngleObj()):
        strtmp = ASTAngle.angleToStr_obj(eclLatObj, DMSFORMAT) + " Ecl Lat"
        dEclLat = eclLatObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Ecliptic Latitude was entered - try again", "Invalid Ecl Lat")
        return
    eclLonObj = ASTAngle.isValidAngle(EclLonInput,HIDE_ERRORS)
    if (eclLonObj.isValidAngleObj()):
        strtmp = strtmp + ", " + ASTAngle.angleToStr_obj(eclLonObj, DMSFORMAT) + " Ecl Lon"
        dEclLon = eclLonObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Ecliptic Longitude was entered - try again", "Invalid Ecl Lon")
        return
 
    # Validate the Epoch
    rTmp = ASTReal.isValidReal(EpochInput,HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        Epoch = rTmp.getRealValue()
        strtmp = strtmp + ", Epoch " + str(Epoch)
    else:
        ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
        return

    prt.setBoldFont(True)
    gui.printlnCond("Convert " + strtmp + " to Equatorial Coordinates", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    dE = __computeEclipticObliquity(gui,Epoch)
    
    gui.printlnCond("5.  Convert Ecliptic Latitude to decimal format.")
    gui.printlnCond("    Ecl Lat = " + ASTAngle.angleToStr_dec(dEclLat, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("6.  Convert Ecliptic Longitude to decimal format.")
    gui.printlnCond("    Ecl Lon = " + ASTAngle.angleToStr_dec(dEclLon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dT = ASTMath.SIN_D(dEclLat) * ASTMath.COS_D(dE) + ASTMath.COS_D(dEclLat) * ASTMath.SIN_D(dE) * ASTMath.SIN_D(dEclLon)
    gui.printlnCond("7.  Compute T = sin(Ecl Lat) * cos(E) + cos(Ecl Lat) * sin(E) * sin(Ecl Lon)")
    gui.printlnCond("    where E is the obliquity of the ecliptic for the given Epoch")
    gui.printlnCond("    T = sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLat) + ")*cos(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) + ") + cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLat) + 
                    ")*sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) +
                    ")*sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLon) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT))
    gui.printlnCond()
    
    dDecl = ASTMath.INVSIN_D(dT)
    gui.printlnCond("8.  Decl = inv sin(T) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + ") = " +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("9.  Y = sin(Ecl Lon) * cos(E) - tan(Ecl Lat) * sin(E)")
    gui.printlnCond("      = sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLon) + ")*cos(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) + ") - tan(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLat) +
                    ")*sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) + ")")
    dY = ASTMath.SIN_D(dEclLon) * ASTMath.COS_D(dE) - ASTMath.TAN_D(dEclLat) * ASTMath.SIN_D(dE)
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY))
    gui.printlnCond()
    
    dX = ASTMath.COS_D(dEclLon)
    gui.printlnCond("10. X = cos(Ecl Lon) = cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLon) + 
            ") = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX))
    gui.printlnCond()
    
    dR = ASTMath.INVTAN_D(dY / dX)
    gui.printlnCond("11. R = inv tan(Y / X) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) +
                    " / " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + ") = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dR))
    gui.printlnCond()
    
    dQA = ASTMath.quadAdjust(dY, dX)
    dRA = dR + dQA
    gui.printlnCond("12. RAdeg = R + quadrant adjustment factor")
    gui.printlnCond("    Since X = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + " and Y = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + ",")
    gui.printlnCond("    the quadrant adjustment factor is " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dQA))
    gui.printlnCond("    Thus, RAdeg = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("13. Divide RAdeg by 15 to convert to hours, so RA = " +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + "/15 = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dRA / 15.0) + " hours")
    dRA = dRA / 15.0
    gui.printlnCond()
    
    gui.printlnCond("14. Finally, convert RA to HMS format and Decl to DMS format.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(dRA, HMSFORMAT) + ", Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DMSFORMAT))
    gui.printlnCond()
    
    result = ASTAngle.angleToStr_obj(eclLatObj, DMSFORMAT) + " Ecl Lat, " + \
             ASTAngle.angleToStr_obj(eclLonObj, DMSFORMAT) + " Ecl Lon = " + \
             ASTTime.timeToStr_dec(dRA, HMSFORMAT) + " RA, " + \
             ASTAngle.angleToStr_dec(dDecl, DMSFORMAT) + " Decl"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcEquatorial_Ecliptic(gui):
    """
    Convert Equatorial Coordinates to Ecliptic Coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    RAInput = ASTStr.removeWhitespace(gui.getData1())
    DeclInput = ASTStr.removeWhitespace(gui.getData2())
    EpochInput = ASTStr.removeWhitespace(gui.getData3())
    
    prt.clearTextArea()

    # Validate RA and Declination
    RAObj = ASTTime.isValidTime(RAInput,HIDE_ERRORS)
    if (RAObj.isValidTimeObj()):
        strtmp = "RA " + ASTTime.timeToStr_obj(RAObj, HMSFORMAT)
        dRA = RAObj.getDecTime()
    else:
        ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid RA")
        return

    declObj = ASTAngle.isValidAngle(DeclInput,HIDE_ERRORS)
    if (declObj.isValidAngleObj()):
        strtmp = strtmp + ", Decl " + ASTAngle.angleToStr_obj(declObj, DMSFORMAT)
        dDecl = declObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination")
        return
    
    # Validate the Epoch
    rTmp = ASTReal.isValidReal(EpochInput,HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        Epoch = rTmp.getRealValue()
        strtmp = strtmp + ", Epoch " + str(Epoch)
    else:
        ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
        return
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + strtmp + " to Ecliptic Coordinates", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    dE = __computeEclipticObliquity(gui,Epoch)
    
    gui.printlnCond("5.  Convert RA to decimal format. RA = " + ASTTime.timeToStr_dec(dRA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("6.  Multiply RA by 15 to convert to degrees.")
    gui.printlnCond("    RAdeg = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dRA) + " * 15 = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dRA * 15.0) + " degrees")
    dRA = dRA * 15.0
    gui.printlnCond()
    
    gui.printlnCond("7.  Convert Decl to decimal format. Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dT = ASTMath.SIN_D(dDecl) * ASTMath.COS_D(dE) - ASTMath.COS_D(dDecl) * ASTMath.SIN_D(dE) * ASTMath.SIN_D(dRA)
    gui.printlnCond("8.  Compute T = sin(Decl) * cos(E) - cos(Decl) * sin(E) * sin(RAdeg)")
    gui.printlnCond("    where E is the obliquity of the ecliptic for the given Epoch")
    gui.printlnCond("    T = sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + ")*cos(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) + ") - cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + 
                    ")*sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) +
                    ")*sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dRA) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT))
    gui.printlnCond()
    
    dEclLat = ASTMath.INVSIN_D(dT)
    gui.printlnCond("9.  Ecl Lat = inv sin(T) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + ") = " +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLat) + " degrees")
    gui.printlnCond()
    
    dY = ASTMath.SIN_D(dRA) * ASTMath.COS_D(dE) + ASTMath.TAN_D(dDecl) * ASTMath.SIN_D(dE)
    gui.printlnCond("10. Y = sin(RAdeg) * cos(E) + tan(Decl) * sin(E)")
    gui.printlnCond("      = sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + ") * cos(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dE) + ") + tan(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + ") * sin(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dE) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY))
    gui.printlnCond()
    
    dX = ASTMath.COS_D(dRA)
    gui.printlnCond("11. X = cos(RAdeg) = cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + ") = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX))
    gui.printlnCond()
    
    dR = ASTMath.INVTAN_D(dY / dX)
    gui.printlnCond("12. R = inv tan (Y / X) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + " / " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + ") = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dR) + " degrees")
    gui.printlnCond()
    
    dQA = ASTMath.quadAdjust(dY, dX)
    dEclLon = dR + dQA
    gui.printlnCond("13. Ecl Lon = R + quadrant adjustment factor")
    gui.printlnCond("    Since X = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + " and Y = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + ",")
    gui.printlnCond("    the quadrant adjustment factor is " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dQA))
    gui.printlnCond("    Thus, Ecl Lon = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dEclLon) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("14. Finally, convert Ecl Lat and Ecl Lon to DMS format.")
    gui.printlnCond("    Ecl Lat = " + ASTAngle.angleToStr_dec(dEclLat, DMSFORMAT) + 
                    ", Ecl Lon = " + ASTAngle.angleToStr_dec(dEclLon, DMSFORMAT))
    gui.printlnCond()
    
    result = ASTTime.timeToStr_obj(RAObj, HMSFORMAT) + " RA, " + \
             ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl = " + \
             ASTAngle.angleToStr_dec(dEclLat, DMSFORMAT) + " Ecl Lat, " + \
             ASTAngle.angleToStr_dec(dEclLon, DMSFORMAT) + " Ecl Lon"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcEquatorial_Galactic(gui):
    """
    Convert Equatorial Coordinates to Galactic Coordinates
      
    :param tkwidget gui: GUI object from which the request came
    """      
    prt = gui.getPrtInstance()
    RAInput = ASTStr.removeWhitespace(gui.getData1())
    DeclInput = ASTStr.removeWhitespace(gui.getData2())
    GalEpochInput = ASTStr.removeWhitespace(gui.getData3())
    
    prt.clearTextArea()

    # Validate RA and Declination
    RAObj = ASTTime.isValidTime(RAInput,HIDE_ERRORS)
    if (RAObj.isValidTimeObj()):
        result = ASTTime.timeToStr_obj(RAObj, HMSFORMAT) + " RA, "
        dRA = RAObj.getDecTime()
    else:
        ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid RA")
        return
    declObj = ASTAngle.isValidAngle(DeclInput,HIDE_ERRORS)
    if (declObj.isValidAngleObj()):
        result = result + ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl"
        dDecl = declObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination")
        return
    
    # Validate Epoch
    rTmp = ASTReal.isValidReal(GalEpochInput,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
        return
    GalEpoch = rTmp.getRealValue()
    if (GalEpoch == GalEpoch1950):                # use Epoch 1950.0 values
        GalRA = GalRA1950
        GalDecl = GalDecl1950
        GalAscNode = GalAscNode1950
    elif (GalEpoch == GalEpoch2000):              # use Epoch J2000 values
        GalRA = GalRA2000
        GalDecl = GalDecl2000
        GalAscNode = GalAscNode2000
    else:
        ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
        return

    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " (Epoch " + 
                    ASTStr.strFormat(ASTStyle.EPOCHFORMAT,GalEpoch) + ")", CENTERTXT)
    gui.printlnCond("to Galactic Coordinates (Epoch " + 
                    ASTStr.strFormat(ASTStyle.EPOCHFORMAT,GalEpoch) + ")", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1.  Convert RA to decimal format. RA = " + 
                    ASTTime.timeToStr_dec(dRA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    dRA = dRA * 15.0
    gui.printlnCond("2.  Multiply RA by 15 to convert to degrees. RAdeg = " + 
                    ASTAngle.angleToStr_dec(dRA, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("3.  Convert Decl to decimal format. Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dGalLat = ASTMath.COS_D(dDecl) * ASTMath.COS_D(GalDecl) * ASTMath.COS_D(dRA - GalRA) + \
              ASTMath.SIN_D(dDecl) * ASTMath.SIN_D(GalDecl)
    gui.printlnCond("4.  Compute T0 = cos(Decl) * cos(" + str(GalDecl) + " * cos(RAdeg - " + str(GalRA) +
                    ") + sin(Decl) * sin(" + str(GalDecl) + ")")
    gui.printlnCond("               = cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + ")*cos(" + 
                    str(GalDecl) + ")*cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + "-" + 
                    str(GalRA) + ")+sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + ")*sin(" + 
                    str(GalDecl) + ")")
    gui.printlnCond("               = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dGalLat))
    gui.printlnCond()

    gui.printCond("5.  Gal Lat = inv sin(T0) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dGalLat) + ") = ")
    dGalLat = ASTMath.INVSIN_D(dGalLat)
    gui.printlnCond(ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dGalLat) + " degrees")
    gui.printlnCond()
    
    dY = ASTMath.SIN_D(dDecl) - ASTMath.SIN_D(dGalLat) * ASTMath.SIN_D(GalDecl)
    gui.printlnCond("6.  Y = sin(Decl) - sin(Gal Lat) * sin(" + str(GalDecl) + ") = sin(" +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + ") - sin(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dGalLat) + ")*sin(" + str(GalDecl) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY))
    gui.printlnCond()
    
    dX = ASTMath.COS_D(dDecl) * ASTMath.SIN_D(dRA - GalRA) * ASTMath.COS_D(GalDecl)
    gui.printlnCond("7.  X = cos(Decl) * sin(RAdeg - " + str(GalRA) + ") * cos(27.4)")
    gui.printlnCond("      = cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + ")*sin(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + " - " + str(GalRA) +
                    ")*cos(" + str(GalDecl) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX))
    gui.printlnCond()
    
    dT = ASTMath.INVTAN_D(dY / dX)
    gui.printlnCond("8.  T1 = inv tan(Y / X) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + " / " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + ") = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " degrees")
    gui.printlnCond()
    
    dQA = ASTMath.quadAdjust(dY, dX)
    dGalLon = dT + dQA + GalAscNode
    gui.printlnCond("9.  Gal Lon = T1 + quadrant adjustment + " + str(GalAscNode))
    gui.printlnCond("    Since X = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + " and Y = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + ",")
    gui.printlnCond("    the quadrant adjustment factor is " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dQA))
    gui.printlnCond("    Thus, Gal Lon = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dGalLon) + " degrees")
    gui.printlnCond()
    
    if (dGalLon > 360.0):
        dGalLon = dGalLon - 360.0
    gui.printlnCond("10. If Gal Lon is greater than 360, subtract 360. Gal Lon = " + 
            ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dGalLon) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("11. Finally, convert Gal Lat and Gal Lon to DMS format.")
    gui.printlnCond("    Gal Lat = " + ASTAngle.angleToStr_dec(dGalLat, DMSFORMAT) + 
                    ", Gal Lon = " + ASTAngle.angleToStr_dec(dGalLon, DMSFORMAT))
    gui.printlnCond()
    
    result = result + " = " + ASTAngle.angleToStr_dec(dGalLat, DMSFORMAT) + \
            " Gal Lat, " + ASTAngle.angleToStr_dec(dGalLon, DMSFORMAT) + \
            " Gal Lon (" + ASTStr.strFormat(ASTStyle.EPOCHFORMAT,GalEpoch) + ")"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcEquatorial_Horizon(gui):
    """
    Convert Equatorial Coordinates to Horizon Coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """  
    prt = gui.getPrtInstance()    
    HAInput = ASTStr.removeWhitespace(gui.getData1())
    DeclInput = ASTStr.removeWhitespace(gui.getData2())
    latInput = ASTStr.removeWhitespace(gui.getData3())
    
    prt.clearTextArea()
    
    # Validate HA and Declination
    HAObj = ASTTime.isValidTime(HAInput,HIDE_ERRORS)
    if (HAObj.isValidTimeObj()):
        result = "HA " + ASTTime.timeToStr_obj(HAObj, HMSFORMAT)
        dHA = HAObj.getDecTime()
    else:
        ASTMsg.errMsg("Invalid Hour Angle was entered - try again", "Invalid HA")
        return
    declObj = ASTAngle.isValidAngle(DeclInput,HIDE_ERRORS)
    if (declObj.isValidAngleObj()):
        result = result + ", Declination " + ASTAngle.angleToStr_obj(declObj, DMSFORMAT)
        dDecl = declObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination")
        return

    # Validate latitude
    dLatObj = ASTLatLon.isValidLat(latInput)
    if (not dLatObj.isLatValid()):
        return
    dLat = dLatObj.getLat()
    strtmp = "for " + ASTLatLon.latToStr_obj(dLatObj, DECFORMAT) + " latitude"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to Horizon Coordinates", CENTERTXT)
    gui.printlnCond(strtmp, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1.  Convert Hour Angle to decimal format. HA = " + 
                    ASTTime.timeToStr_obj(HAObj, DECFORMAT) + " hours")
    gui.printlnCond()
    
    dHA = dHA * 15.0
    gui.printlnCond("2.  Multiply step 1 by 15 to convert to degrees. HAdeg = " + 
                    ASTAngle.angleToStr_dec(dHA, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("3.  Convert Declination to decimal format. Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("4.  Compute T0 = sin(Decl) * sin(Lat) + cos(Decl) * cos(Lat) * cos(HAdeg)")
    gui.printlnCond("               = sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + 
                    ")*sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) +
                    ") + cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + 
                    ")*cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) + ")*cos(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA) + ")")
    dT = ASTMath.SIN_D(dDecl) * ASTMath.SIN_D(dLat) + ASTMath.COS_D(dDecl) * ASTMath.COS_D(dLat) * ASTMath.COS_D(dHA)
    gui.printlnCond("               = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT))
    gui.printlnCond()
    
    dAlt = ASTMath.INVSIN_D(dT)
    gui.printlnCond("5.  Alt = inv sin(T0) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + ") = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAlt) + " degrees")
    gui.printlnCond()

    dT1 = ASTMath.SIN_D(dDecl) - ASTMath.SIN_D(dLat) * ASTMath.SIN_D(dAlt)
    gui.printlnCond("6.  T1 = sin(Decl) - sin(Lat) * sin(Alt) = sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + ") - sin(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) + ")*sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAlt) + ")")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT1))
    gui.printlnCond()
    
    dT2 = ASTMath.COS_D(dLat) * ASTMath.COS_D(dAlt)
    gui.printlnCond("7.  T2 = T1 / [cos(Lat) * cos(Alt)] = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT1) + " / [cos(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) + ") * cos(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAlt) + ")]")
    dT2 = dT1 / dT2
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT2))
    gui.printlnCond()
    
    dAz = ASTMath.INVCOS_D(dT2)
    gui.printlnCond("8.  Az = inv cos(T2) = inv cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT2) + ") = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAz) + " degrees")
    gui.printlnCond()
    
    dT1 = ASTMath.SIN_D(dHA)
    gui.printlnCond("9.  Compute sin(HAdeg) = sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA) + ") = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT1))
    gui.printlnCond()
    
    gui.printlnCond("10. If the result of step 9 is positive, then Az = 360 - Az.")
    if (dT1 > 0):
        dAz = 360.0 - dAz
    gui.printlnCond("    Az = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAz) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("11. Finally, convert Altitude and Azimuth to DMS format. Thus,")
    gui.printlnCond("    Alt = " + ASTAngle.angleToStr_dec(dAlt, DMSFORMAT) + ", Az = " + 
                    ASTAngle.angleToStr_dec(dAz, DMSFORMAT))
    gui.printlnCond()
    
    result = ASTTime.timeToStr_obj(HAObj, DMSFORMAT) + " HA, " + \
            ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl = " +\
            ASTAngle.angleToStr_dec(dAlt, DMSFORMAT) + " Alt, " + \
            ASTAngle.angleToStr_dec(dAz, DMSFORMAT) + " Az"
    gui.printlnCond("Thus, " + result)
    gui.printlnCond(strtmp)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcGalactic_Equatorial(gui):
    """
    Convert Galactic Coordinates to Equatorial Coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    GalLatInput = ASTStr.removeWhitespace(gui.getData1())
    GalLonInput = ASTStr.removeWhitespace(gui.getData2())
    GalEpochInput = ASTStr.removeWhitespace(gui.getData3())
    
    prt.clearTextArea()

    # Validate Galactic lat/lon
    latObj = ASTAngle.isValidAngle(GalLatInput)
    if (not latObj.isValidAngleObj()):
        return
    dLat = latObj.getDecAngle()
    lonObj = ASTAngle.isValidAngle(GalLonInput)
    if (not lonObj.isValidAngleObj()):
        return
    dLon = lonObj.getDecAngle()
    
    # Validate Epoch
    rTmp = ASTReal.isValidReal(GalEpochInput,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
        return
    GalEpoch = rTmp.getRealValue()
    if (GalEpoch == GalEpoch1950):                #use Epoch 1950.0 values
        GalRA = GalRA1950
        GalDecl = GalDecl1950
        GalAscNode = GalAscNode1950
    elif (GalEpoch == GalEpoch2000):              # use Epoch J2000 values
        GalRA = GalRA2000
        GalDecl = GalDecl2000
        GalAscNode = GalAscNode2000
    else:
        ASTMsg.errMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
        return
    
    result = ASTAngle.angleToStr_dec(dLat, DMSFORMAT) + " Gal Lat, " + \
            ASTAngle.angleToStr_dec(dLon, DMSFORMAT) + " Gal Lon"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " (Epoch " + 
                    ASTStr.strFormat(ASTStyle.EPOCHFORMAT,GalEpoch) + ")", CENTERTXT)
    gui.printlnCond("to Equatorial Coordinates " + "(Epoch " + 
                    ASTStr.strFormat(ASTStyle.EPOCHFORMAT,GalEpoch) + ")", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1.  Convert Galactic Lat to decimal format. Gal Lat = " + 
                    ASTAngle.angleToStr_dec(dLat, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("2.  Convert Galactic Lon to decimal format. Gal Lon = " + 
                    ASTAngle.angleToStr_dec(dLon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dDecl = ASTMath.COS_D(dLat) * ASTMath.COS_D(GalDecl) * ASTMath.SIN_D(dLon - GalAscNode) + \
            ASTMath.SIN_D(dLat) * ASTMath.SIN_D(GalDecl)
    gui.printlnCond("3.  Compute T = cos(Gal Lat) * cos(" + str(GalDecl) + ") * sin(Gal Lon - " + 
                    str(GalAscNode) + ") + " + "sin(Gal Lat) * sin(" + str(GalDecl) + ")")
    gui.printlnCond("              = cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLat) +
                    ")*cos(" + str(GalDecl) + ")*sin(" +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLon) + " - " + str(GalAscNode) + ") + sin(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLat) + ") * sin(" + str(GalDecl) + ")")
    gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl))
    gui.printlnCond()
    
    gui.printCond("4.  Decl = inv sin(T) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + ") = ")
    dDecl = ASTMath.INVSIN_D(dDecl)
    gui.printlnCond(ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + " degrees")
    gui.printlnCond()
    
    dY = ASTMath.COS_D(dLat) * ASTMath.COS_D(dLon - GalAscNode)
    gui.printlnCond("5.  Y = cos(Gal Lat) * cos(Gal Lon - " + str(GalAscNode) + ") = cos(" +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLat) + ")*cos(" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLon) + " - " + str(GalAscNode) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY))
    gui.printlnCond()
    
    dX = ASTMath.SIN_D(dLat) * ASTMath.COS_D(GalDecl) - ASTMath.COS_D(dLat) * ASTMath.SIN_D(GalDecl) * ASTMath.SIN_D(dLon - GalAscNode)
    gui.printlnCond("6.  X = sin(Gal Lat) * cos(" + str(GalDecl) + ") - cos(Gal Lat) * sin(" + 
                    str(GalDecl) + ") * " + "sin(Gal Lon - " + str(GalAscNode) + ")")
    gui.printlnCond("      = sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLat) + ")*cos(" + str(GalDecl) +
                    ") - cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLat) + ") * sin(" + 
                    str(GalDecl) + ")*sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dLon) + " - " + 
                    str(GalAscNode) + ")")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX))
    gui.printlnCond()
    
    dR = ASTMath.INVTAN_D(dY / dX)
    gui.printlnCond("7.  R = inv tan (Y / X) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + " / " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + ") = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dR) + " degrees")
    gui.printlnCond()
    
    dQA = ASTMath.quadAdjust(dY, dX)
    dRA = dR + dQA + GalRA
    gui.printlnCond("8.  RAdeg = R + quadrant adjustment factor + " + str(GalRA))
    gui.printlnCond("    Since X = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dX) + " and Y = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dY) + ",")
    gui.printlnCond("    the quadrant adjustment factor is " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dQA))
    gui.printlnCond("    Thus, RAdeg = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + " degrees")
    gui.printlnCond()
    
    if (dRA > 360):
        dRA = dRA - 360.0
    gui.printlnCond("9.  If RAdeg is > 360, subtract 360. RAdeg = " + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRA) + " degrees")
    gui.printlnCond()
    
    dRA = dRA / 15.0
    gui.printlnCond("10. Divide RAdeg by 15 to convert it to hours, giving RA = " + 
                    ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dRA) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("11. Finally, convert Right Ascension to HMS format and Declination to DMS format.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(dRA, HMSFORMAT) + ", Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DMSFORMAT))
    gui.printlnCond()

    result = result + " = " + ASTTime.timeToStr_dec(dRA, HMSFORMAT) + " RA, " +\
            ASTAngle.angleToStr_dec(dDecl,DMSFORMAT) + " Decl (" +\
            ASTStr.strFormat(ASTStyle.EPOCHFORMAT,GalEpoch) + ")"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcHA_RA(gui):
    """
    Convert Hour Angle to Right Ascension
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    HAInput = ASTStr.removeWhitespace(gui.getData1())
    UTInput = ASTStr.removeWhitespace(gui.getData2())
    lonInput = ASTStr.removeWhitespace(gui.getData3())
    dateInput = ASTStr.removeWhitespace(gui.getData4())
    
    prt.clearTextArea()

    # Validate Hour Angle and UT
    HAtimeObj = ASTTime.isValidTime(HAInput,HIDE_ERRORS)
    if (HAtimeObj.isValidTimeObj()):
        result = ASTTime.timeToStr_obj(HAtimeObj, HMSFORMAT)
    else:
        ASTMsg.errMsg("Invalid Hour Angle was entered - try again", "Invalid Hour Angle")
        return
    UTtimeObj = ASTTime.isValidTime(UTInput, HIDE_ERRORS)
    if (UTtimeObj.isValidTimeObj()):
        strtmp = "at " + ASTTime.timeToStr_obj(UTtimeObj, HMSFORMAT) + " UT"
    else:
        ASTMsg.errMsg("Invalid UT was entered - try again", "Invalid UT")
        return

    # Validate longitude and date
    dLonObj = ASTLatLon.isValidLon(lonInput)
    if (not dLonObj.isLonValid()):
        return
    dLongitude = dLonObj.getLon()
    strtmp = strtmp + " for " + ASTLatLon.lonToStr_obj(dLonObj, DECFORMAT) + " longitude"
    
    dateObj = ASTDate.isValidDate(dateInput)
    if (not dateObj.isValidDateObj()):
        return
    strtmp = strtmp + " on " + ASTDate.dateToStr_obj(dateObj)

    prt.setBoldFont(True)
    gui.printlnCond("Convert Hour Angle " + result + " to Right Ascension", CENTERTXT)
    gui.printlnCond(strtmp, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()

    LST = ASTTime.GSTtoLST(ASTTime.UTtoGST(UTtimeObj.getDecTime(), dateObj), dLongitude)
    gui.printlnCond("1. Convert UT to LST. Thus, for date " + ASTDate.dateToStr_obj(dateObj) + " at longitude " + 
                    ASTLatLon.lonToStr_obj(dLonObj, DECFORMAT))
    gui.printlnCond("   " + ASTTime.timeToStr_obj(UTtimeObj, HMSFORMAT) + " UT = " + 
                    ASTTime.timeToStr_dec(LST, HMSFORMAT) +
            " = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " LST")
    gui.printlnCond()

    gui.printlnCond("2. Convert Hour Angle to decimal format. Thus, HA = " +
                    ASTTime.timeToStr_obj(HAtimeObj, DECFORMAT) + " hours")
    gui.printlnCond()
    
    RA = LST - HAtimeObj.getDecTime()
    gui.printlnCond("3. RA = LST - Hour Angle = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " - " + 
                    ASTTime.timeToStr_obj(HAtimeObj, DECFORMAT) + " = " + 
                    ASTTime.timeToStr_dec(RA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    if (RA < 0.0):
        RA = RA + 24.0
    gui.printlnCond("4. If RA is negative add 24. Thus, RA = " + ASTTime.timeToStr_dec(RA, DECFORMAT) + " hours")
    gui.printlnCond()

    gui.printlnCond("5. Finally, convert RA to hours, minutes, seconds format, giving RA = " + 
                    ASTTime.timeToStr_dec(RA, HMSFORMAT))
    gui.printlnCond()
    
    result = ASTTime.timeToStr_obj(HAtimeObj, HMSFORMAT) + " HA = " + \
             ASTTime.timeToStr_dec(RA, HMSFORMAT) + " RA"
    gui.printlnCond("Thus, " + result + " at " + ASTTime.timeToStr_obj(UTtimeObj, HMSFORMAT) + " UT")
    gui.printlnCond(strtmp)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcHorizon_Equatorial(gui):
    """
    Convert Horizon Coordinates to Equatorial Coordinates
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    altInput = ASTStr.removeWhitespace(gui.getData1())
    azInput = ASTStr.removeWhitespace(gui.getData2())
    latInput = ASTStr.removeWhitespace(gui.getData3())
    
    prt.clearTextArea()
    
    # Validate Altitude and Azimuth
    altObj = ASTAngle.isValidAngle(altInput,HIDE_ERRORS)
    if (altObj.isValidAngleObj()):
        result = "Altitude " + ASTAngle.angleToStr_obj(altObj, DMSFORMAT)
        dAlt = altObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Altitude was entered - try again", "Invalid Altitude")
        return
    azObj = ASTAngle.isValidAngle(azInput,HIDE_ERRORS)
    if (azObj.isValidAngleObj()):
        result = result + ", Azimuth " + ASTAngle.angleToStr_obj(azObj, DMSFORMAT)
        dAz = azObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Azimuth was entered - try again", "Invalid Azimuth")
        return

    # Validate latitude
    dLatObj = ASTLatLon.isValidLat(latInput)
    if (not dLatObj.isLatValid()):
        return
    dLat = dLatObj.getLat()
    strtmp = "for " + ASTLatLon.latToStr_obj(dLatObj, DECFORMAT) + " latitude"
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to Equatorial Coordinates", CENTERTXT)
    gui.printlnCond(strtmp, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1.  Convert Altitude to decimal format. Alt = " +
                    ASTAngle.angleToStr_obj(altObj, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("2.  Convert Azimuth to decimal format. Az = " +
                    ASTAngle.angleToStr_obj(azObj, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("3.  Compute T0 = sin(Alt) * sin(Lat) + cos(Alt) * cos(Lat) * cos(Az)")
    gui.printlnCond("               = sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAlt) + ") * sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) +
                    ") + cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAlt) + ") * cos(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) +
                    ") * cos(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAz) + ")")
    dTmp = ASTMath.SIN_D(dAlt) * ASTMath.SIN_D(dLat) + ASTMath.COS_D(dAlt) * ASTMath.COS_D(dLat) * ASTMath.COS_D(dAz)
    gui.printlnCond("               = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp))
    gui.printlnCond()
    
    dDecl = ASTMath.INVSIN_D(dTmp)
    gui.printlnCond("4.  Decl = inverse sin(T0) = inv sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp) + ") = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("5.  T1 = sin(Alt) - sin(Lat) * sin(Decl)")
    gui.printlnCond("       = sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAlt) + ") - sin(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) + ") * sin(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + ")")
    dTmp = ASTMath.SIN_D(dAlt) - ASTMath.SIN_D(dLat) * ASTMath.SIN_D(dDecl)
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp))
    gui.printlnCond()
    
    dHA = ASTMath.COS_D(dLat) * ASTMath.COS_D(dDecl)
    gui.printlnCond("6.  cos(H) = T1 / [cos(Lat) * cos(Decl)]")
    gui.printlnCond("           = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp) + " / [cos(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dLat) + ") * cos(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dDecl) + ")]")
    dHA = dTmp / dHA
    gui.printlnCond("           = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA))
    gui.printlnCond()
    
    gui.printlnCond("7.  H = inv cos(H) = inv cos( " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA) + ") = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTMath.INVCOS_D(dHA)) + " degrees")
    dHA = ASTMath.INVCOS_D(dHA)
    gui.printlnCond()
    
    dTmp = ASTMath.SIN_D(dAz)
    gui.printlnCond("8.  Compute sin(Az) = sin(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dAz) + ") = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dTmp))
    gui.printlnCond()
    
    gui.printlnCond("9.  If sin(Az) is positive, then subtract the result of step 7 from 360.")
    if (dTmp > 0):
        dHA = 360 - dHA
    gui.printlnCond("    H = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA) + " degrees")
    gui.printlnCond()
    
    gui.printCond("10. Hour Angle = HA = H / 15 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA) + " / 15 = ")
    dHA = dHA / 15.0
    gui.printlnCond(ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dHA) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("11. Finally, convert Declination to DMS format and HA to HMS format.")
    gui.printlnCond("    HA = " + ASTTime.timeToStr_dec(dHA, HMSFORMAT) + ", Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DMSFORMAT))
    gui.printlnCond()
    
    result = ASTAngle.angleToStr_obj(altObj, DMSFORMAT) + " Alt, " + \
             ASTAngle.angleToStr_obj(azObj, DMSFORMAT) + " Az = " +\
             ASTTime.timeToStr_dec(dHA, HMSFORMAT) + " HA, " + \
             ASTAngle.angleToStr_dec(dDecl, DMSFORMAT) + " Decl"
    gui.printlnCond("Thus, " + result)
    gui.printlnCond(strtmp)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcRA_HA(gui):
    """
    Convert Right Ascension to Hour Angle
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    RAInput = ASTStr.removeWhitespace(gui.getData1())
    UTInput = ASTStr.removeWhitespace(gui.getData2())
    lonInput = ASTStr.removeWhitespace(gui.getData3())
    dateInput = ASTStr.removeWhitespace(gui.getData4())
    result = ""
    
    prt.clearTextArea()
    
    # Validate Right Ascension and UT
    RAtimeObj = ASTTime.isValidTime(RAInput,HIDE_ERRORS)
    if (RAtimeObj.isValidTimeObj()):
        result = ASTTime.timeToStr_obj(RAtimeObj, HMSFORMAT)
    else:
        ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid Right Ascension")
        return
    
    UTtimeObj = ASTTime.isValidTime(UTInput,HIDE_ERRORS)
    if (UTtimeObj.isValidTimeObj()):
        strtmp = "at " + ASTTime.timeToStr_obj(UTtimeObj, HMSFORMAT) + " UT"
    else:
        ASTMsg.errMsg("Invalid UT was entered - try again", "Invalid UT")
        return
 
    # Validate longitude and date
    dLonObj = ASTLatLon.isValidLon(lonInput)
    if (not dLonObj.isLonValid()):
        return
    dLongitude = dLonObj.getLon()
    strtmp = strtmp + " for " + ASTLatLon.lonToStr_obj(dLonObj, DECFORMAT) + " longitude"
    
    dateObj = ASTDate.isValidDate(dateInput)
    if (not dateObj.isValidDateObj()):
        return
    strtmp = strtmp + " on " + ASTDate.dateToStr_obj(dateObj)
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert Right Ascension " + result + " to Hour Angle (HA)", CENTERTXT)
    gui.printlnCond(strtmp, CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    LST = ASTTime.GSTtoLST(ASTTime.UTtoGST(UTtimeObj.getDecTime(), dateObj), dLongitude)
    gui.printlnCond("1. Convert UT to LST. Thus, for date " + ASTDate.dateToStr_obj(dateObj) + " at longitude " + 
                    ASTLatLon.lonToStr_obj(dLonObj, DECFORMAT))
    gui.printlnCond("   " + ASTTime.timeToStr_obj(UTtimeObj, HMSFORMAT) + " UT = " + 
                    ASTTime.timeToStr_dec(LST, HMSFORMAT) +
                    " = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " LST")
    gui.printlnCond()
    
    gui.printlnCond("2. Convert Right Ascension to decimal format. Thus, RA = " +
                    ASTTime.timeToStr_obj(RAtimeObj, DECFORMAT) + " hours")
    gui.printlnCond()
    
    HA = LST - RAtimeObj.getDecTime()
    gui.printlnCond("3. HA = LST - RA = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " - " + 
                    ASTTime.timeToStr_obj(RAtimeObj, DECFORMAT) + " = " + 
                    ASTTime.timeToStr_dec(HA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    if (HA < 0.0):
        HA = HA + 24.0
    gui.printlnCond("4. If HA is negative add 24. Thus, HA = " + ASTTime.timeToStr_dec(HA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("5. Finally, convert HA to hours, minutes, seconds format, giving HA = " + 
                    ASTTime.timeToStr_dec(HA, HMSFORMAT))
    gui.printlnCond()
    
    result = ASTTime.timeToStr_obj(RAtimeObj, HMSFORMAT) + " RA = " + \
             ASTTime.timeToStr_dec(HA, HMSFORMAT) + " HA"
    gui.printlnCond("Thus, " + result)
    gui.printlnCond(strtmp)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcPrecessionCorr(gui):
    """
    Compute a precession correction
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    RAInput = ASTStr.removeWhitespace(gui.getData1())
    DeclInput = ASTStr.removeWhitespace(gui.getData2())
    strEpochFrom = ASTStr.removeWhitespace(gui.getData3())
    strEpochTo = ASTStr.removeWhitespace(gui.getData4())

    prt.clearTextArea()

    # Validate RA and Declination
    RAObj = ASTTime.isValidTime(RAInput,HIDE_ERRORS)
    if (RAObj.isValidTimeObj()):
        result = ASTTime.timeToStr_obj(RAObj, HMSFORMAT) + " RA, "
        dRA = RAObj.getDecTime()
    else:
        ASTMsg.errMsg("Invalid Right Ascension was entered - try again", "Invalid RA")
        return
    declObj = ASTAngle.isValidAngle(DeclInput,HIDE_ERRORS)
    if (declObj.isValidAngleObj()):
        result = result + ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl"
        dDecl = declObj.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Declination was entered - try again", "Invalid Declination")
        return
    
    # Validate the to and from Epochs
    rTmp = ASTReal.isValidReal(strEpochFrom,HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        EpochFrom = rTmp.getRealValue()
        result = result + " in Epoch " + str(EpochFrom)
    else:
        ASTMsg.errMsg("Invalid 'From' Epoch was entered - try again", "Invalid 'From' Epoch")
        return
    rTmp = ASTReal.isValidReal(strEpochTo,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid 'To' Epoch was entered - try again", "Invalid 'To' Epoch")
        return
    EpochTo = rTmp.getRealValue()
    
    prt.setBoldFont(True)
    gui.printlnCond("Convert " + result + " to Epoch " + str(EpochTo), CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    gui.printlnCond("1.  Convert RA to decimal format. RA = " + ASTTime.timeToStr_dec(dRA, DECFORMAT) + " hours")
    gui.printlnCond()
    
    dRAdeg = dRA * 15.0
    gui.printlnCond("2.  Multiply RA by 15 to convert to degrees. RAdeg = " + 
                    ASTAngle.angleToStr_dec(dRAdeg, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("3.  Convert Decl to decimal format. Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dT = (EpochTo - 1900.0) / 100.0
    gui.printlnCond("4.  T = (Epoch to convert to - 1900) / 100 = (" + str(EpochTo) + 
                    " - 1900) / 100 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT))
    gui.printlnCond()
    
    dM = 3.07234 + 0.00186 * dT
    gui.printlnCond("5.  M = 3.07234 + 0.00186 * T = 3.07234 + 0.00186 * " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dM) + " arcseconds")
    gui.printlnCond()
    
    dNd = 20.0468 - 0.0085 * dT
    gui.printlnCond("6.  Nd = 20.0468 - 0.0085 * T = 20.0468 - 0.0085 * " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) + " = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dNd) + " arcseconds")
    gui.printlnCond()

    dNt = dNd / 15.0
    gui.printlnCond("7.  Nt = Nd / 15 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dNd) + 
                    " / 15 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dNt) + " seconds (of time)")
    gui.printlnCond()
    
    dDiff = (EpochTo - EpochFrom)
    gui.printlnCond("8.  Diff = (Epoch to convert to - Epoch to convert from) = (" + 
                    str(EpochTo) + " - " + str(EpochFrom) + ") = " + str(dDiff))
    gui.printlnCond()
    
    deltaRA = (dM + dNt * ASTMath.SIN_D(dRAdeg) * ASTMath.TAN_D(dDecl)) * dDiff
    gui.printlnCond("9.  D.ra = [M + Nt * sin(RAdeg) * tan(Decl)] * Diff")
    gui.printlnCond("         = [" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dM) + " + " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dNt) +
                    " * sin(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRAdeg) + 
                    ") * tan(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + "]) * " + str(dDiff))
    gui.printlnCond("         = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,deltaRA) + " seconds (of time)")
    gui.printlnCond()
    
    deltaDecl = dNd * ASTMath.COS_D(dRAdeg) * dDiff
    gui.printlnCond("10. D.decl = Nd * cos(RAdeg) * Diff = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dNd) + 
                    " * cos(" + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dRAdeg) + ") * " + str(dDiff))
    gui.printlnCond("           = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,deltaDecl) + " arcseconds")
    gui.printlnCond()
    
    deltaRA = deltaRA / 3600.0
    deltaDecl = deltaDecl / 3600.0
    gui.printlnCond("11. Divide D.ra by 3600 to convert to hours and divide D.decl by 3600 to convert to degrees.")
    gui.printlnCond("    D.ra = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,deltaRA) + " hours and D.decl = " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,deltaDecl) + " degrees")
    gui.printlnCond()
    
    dRA = dRA + deltaRA
    dDecl = dDecl + deltaDecl
    gui.printlnCond("12. Add D.ra to RA and D.decl to Decl, giving an")
    gui.printlnCond("    adjusted RA = " + ASTStr.strFormat(ASTStyle.DEC_HRS_FMT,dRA) + " hours and Decl = " +
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDecl) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("13. Finally, convert RA to HMS format and Decl to DMS format. Thus, in Epoch " + 
                    str(EpochTo) + ",")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(dRA, HMSFORMAT) + " hours, Decl = " + 
                    ASTAngle.angleToStr_dec(dDecl, DMSFORMAT) + " degrees")
    gui.printlnCond()
    
    result = ASTTime.timeToStr_obj(RAObj,HMSFORMAT) + " RA, " + \
             ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl (" + str(EpochFrom) + ") = " + \
             ASTTime.timeToStr_dec(dRA, HMSFORMAT) + " RA, " + \
             ASTAngle.angleToStr_dec(dDecl, DMSFORMAT) + " Decl (" + str(EpochTo) + ")"
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



#=================================================================
# Routines to do the actual work for the Kepler's Equation menu
#=================================================================

def calcKeplerNewton(gui):
    """
    Solve Kepler's equation by the Newton/Raphson method
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    MAInput = ASTStr.removeWhitespace(gui.getData1())
    TermInput = ASTStr.removeWhitespace(gui.getData2())
    EccInput = ASTStr.removeWhitespace(gui.getData3())
    iterCnt = 0
  
    prt.clearTextArea()
    
    # Validate the mean anomaly
    tmpAngle = ASTAngle.isValidAngle(MAInput,HIDE_ERRORS)
    if (tmpAngle.isValidAngleObj()):
        dMA = tmpAngle.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Mean Anomaly was entered - try again", "Invalid Mean Anomaly")
        return

    # Validate the termination criteria
    rTmp = ASTReal.isValidReal(TermInput,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
        return
    dTerm = rTmp.getRealValue()
    if (dTerm < ASTKepler.KeplerMinCriteria):
        dTerm = ASTKepler.KeplerMinCriteria
    
    # Validate the orbital eccentricity
    rTmp = ASTReal.isValidReal(EccInput,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
        return
    dEcc = rTmp.getRealValue()
    if (dEcc < 0):
        ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
        return
    
    prt.setBoldFont(True)
    gui.printlnCond("Solve Kepler's Equation for Mean Anomaly " + ASTAngle.angleToStr_dec(dMA, DMSFORMAT) +
                    " and Orbital Eccentricity = " + str(dEcc), CENTERTXT)
    gui.printlnCond("Iterate until difference is less than " + str(dTerm) + " radians (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTMath.rad2deg(dTerm) * 3600.0) + 
                    " arcseconds)", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()

    prt.setFixedWidthFont()
    
    dMARad = ASTMath.deg2rad(dMA)
    gui.printlnCond("1. Convert mean anomaly to radians.")
    gui.printlnCond("   MA_rad = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dMARad) + " radians")
    gui.printlnCond()
    
    if (dEcc < 0.75):
        dEA = dMARad
    else:
        dEA = math.pi
    
    gui.printlnCond("2. Set an initial estimate for E0. If the orbital eccentricity is < 0.75,")
    gui.printlnCond("   E0 = MA_rad, otherwise E0 = pi radians. Thus, ")
    gui.printlnCond("   E0 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEA) + " radians")
    gui.printlnCond()
    
    gui.printlnCond("3. Using the iteration scheme")
    gui.printlnCond("   E_i = E_(i-1) - [E_(i-1) - e*sin(E_(i-1)-MA_rad]/[1-e*cos(E_(i-1))],")
    gui.printlnCond("   iterate until the difference between successive estimates is")
    gui.printlnCond("   less than the termination criteria (" + str(dTerm) + " radians)")
    gui.printlnCond()
    
    for iterCnt in range(1,ASTKepler.KeplerMaxIterations+1):
        # Note that the sine + cosine functions here uses radians, not degrees!
        dEANext = dEA - (dEA - dEcc * math.sin(dEA) - dMARad) / (1 - dEcc * math.cos(dEA))
        delta = abs(dEANext - dEA)
        gui.printlnCond("   E" + str(iterCnt) + " = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEANext) + 
                        ", Delta = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,delta))
        dEA = dEANext
        if (delta <= dTerm):
            break

    if (iterCnt >= ASTKepler.KeplerMaxIterations):
        gui.printlnCond("WARNING: Did not converge, so stopped after " + 
                        str(ASTKepler.KeplerMaxIterations) + " iterations.")
    gui.printlnCond()
    
    gui.printlnCond("4. The eccentric anomaly (in radians) is the last result. So,")
    gui.printlnCond("   Eccentric Anomaly = E" + str(iterCnt) + " = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEA) + " radians")
    gui.printlnCond()
    
    dEA = ASTMath.rad2deg(dEA)
    gui.printlnCond("5. Convert the eccentric anomaly from radians to degrees.")
    gui.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr_dec(dEA, DECFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("6. Convert the Eccentric Anomaly to DMS format.")
    gui.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr_dec(dEA, DMSFORMAT))
    gui.printlnCond()
    
    result = "After " + str(iterCnt) + " iterations (Newton/Raphson method), Eccentric Anomaly = " + \
             ASTAngle.angleToStr_dec(dEA, DMSFORMAT)
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcKeplerSimple(gui):
    """
    Solve Kepler's equation via a simple iteration method
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    MAInput = ASTStr.removeWhitespace(gui.getData1())
    TermInput = ASTStr.removeWhitespace(gui.getData2())
    EccInput = ASTStr.removeWhitespace(gui.getData3())
    iterCnt = 0
    
    prt.clearTextArea()
    
    # Validate the mean anomaly
    tmpAngle = ASTAngle.isValidAngle(MAInput,HIDE_ERRORS)
    if (tmpAngle.isValidAngleObj()):
        dMA = tmpAngle.getDecAngle()
    else:
        ASTMsg.errMsg("Invalid Mean Anomaly was entered - try again", "Invalid Mean Anomaly")
        return

    # Validate the termination criteria
    rTmp = ASTReal.isValidReal(TermInput,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
        return
    dTerm = rTmp.getRealValue()
    if (dTerm < ASTKepler.KeplerMinCriteria):
        dTerm = ASTKepler.KeplerMinCriteria
    
    # Validate the orbital eccentricity
    rTmp = ASTReal.isValidReal(EccInput,HIDE_ERRORS)
    if (not rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
        return
    dEcc = rTmp.getRealValue()
    if (dEcc < 0):
        ASTMsg.errMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
        return
    
    prt.setBoldFont(True)
    gui.printlnCond("Solve Kepler's Equation for Mean Anomaly " + ASTAngle.angleToStr_dec(dMA, DMSFORMAT) +
                    " and Orbital Eccentricity = " + str(dEcc), CENTERTXT)
    gui.printlnCond("Iterate until difference is less than " + str(dTerm) + " radians (" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,ASTMath.rad2deg(dTerm)*3600.0) + " arcseconds)", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    dMARad = ASTMath.deg2rad(dMA)
    gui.printlnCond("1. Convert mean anomaly to radians.")
    gui.printlnCond("   MA_rad = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dMARad) + " radians")
    gui.printlnCond()
    
    dEA = dMARad
    gui.printlnCond("2. Set initial estimate E0 = MA_rad = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEA) + " radians")
    gui.printlnCond()
    
    gui.printlnCond("3. Using the iteration scheme E_i = MA_rad + e*sin(E_(i-1)),")
    gui.printlnCond("   iterate until the difference between successive estimates is")
    gui.printlnCond("   less than the termination criteria (" + str(dTerm) + " radians)")
    gui.printlnCond()
    
    for iterCnt in range(1,ASTKepler.KeplerMaxIterations+1):
        # Note that the sine function here uses radians, not degrees!
        dEANext = dMARad + dEcc * math.sin(dEA)
        delta = abs(dEANext - dEA)
        gui.printlnCond("   E" + str(iterCnt) + " = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEANext) + 
                        ", Delta = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,delta))
        dEA = dEANext
        if (delta <= dTerm):
            break
        
    if (iterCnt >= ASTKepler.KeplerMaxIterations):
        gui.printlnCond("WARNING: Did not converge, so stopped after " + 
                        str(ASTKepler.KeplerMaxIterations) + " iterations.")
        
    gui.printlnCond()
    
    gui.printlnCond("4. The eccentric anomaly (in radians) is the last result. So,")
    gui.printlnCond("   Eccentric Anomaly = E" + str(iterCnt) + " = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dEA) + " radians")
    gui.printlnCond()
    
    dEA = ASTMath.rad2deg(dEA)
    gui.printlnCond("5. Convert the eccentric anomaly from radians to degrees.")
    gui.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr_dec(dEA, DECFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("6. Convert the Eccentric Anomaly to DMS format.")
    gui.printlnCond("   Eccentric Anomaly = " + ASTAngle.angleToStr_dec(dEA, DMSFORMAT))
    gui.printlnCond()
    
    result = "After " + str(iterCnt) + " iterations (Simple Iteration), Eccentric Anomaly = " + \
             ASTAngle.angleToStr_dec(dEA, DMSFORMAT)
    gui.printlnCond("Thus, " + result)
    
    gui.setResults(result)
    prt.setProportionalFont()
    prt.resetCursor()



#=====================================================================
# Private functions used only in this module
#=====================================================================

def __computeEclipticObliquity(gui,Epoch):
    """
    Compute the obliquity of the ecliptic for a given epoch
    
    :param tkwidget gui: GUI object from which the request came
    :param float Epoch: epoch for which obliquity is desired
    :return: obliquity of the ecliptic calculated for the stated epoch.
    """
    iEpoch = ASTMath.Trunc(Epoch)

    gui.printlnCond("Steps 1-4 compute the Obliquity of the Ecliptic for Epoch " + 
                    ASTStr.strFormat("%.2f",Epoch))
    gui.printlnCond()
    
    JD = ASTDate.dateToJD_mdy(1,0.0, iEpoch)
    gui.printlnCond("1.  Calculate the Julian Day Number for January 0.0 of the given Epoch")
    gui.printlnCond("    JD = " + ASTDate.dateToStr_mdy(1, 0.0, iEpoch) + " = " +
                    ASTStr.strFormat(ASTStyle.JDFormat,JD))
    gui.printlnCond()
    
    dT = (JD - 2451545.0) / 36525.0
    gui.printlnCond("2.  T = (JD - 2451545.0) / 36525 = (" + 
                    ASTStr.strFormat(ASTStyle.JDFormat, JD) + " - 2451545.0) / 36525")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + " centuries")
    gui.printlnCond()        
    
    dDE = 46.815 * dT + 0.0006 * dT * dT - 0.00181 * dT * dT * dT
    gui.printlnCond("3.  DE = 46.815 * T + 0.0006 * T * T - 0.00181 * T * T * T")
    gui.printlnCond("       = 46.815*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT) +
                    " + 0.0006*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT * dT) +
                    " - 0.00181*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dT * dT * dT))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDE) + " arcsecs")
    gui.printlnCond()
    
    dE = 23.439292 - (dDE / 3600.0)
    gui.printlnCond("4.  E = 23.439292 - (DE / 3600) = 23.439292 - (" + 
                    ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dDE) + " / 3600)")
    gui.printlnCond("      = " + ASTStr.strFormat(ASTStyle.DEC_DEG_FMT,dE) + " degrees")
    gui.printlnCond()
    
    return dE
  


#=========== Main entry point ===============
if __name__ == '__main__':
    pass 
