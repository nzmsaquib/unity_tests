"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what time-related conversion to do."""
    NO_CALC_SELECTED = auto()        # no calculation is currently selected
    
    # Coord Systems
    HA_RA = auto()                      # convert Hour Angle to Right Ascension 
    RA_HA = auto()                      # convert Right Ascension to Hour Angle
    HORIZON_EQUATORIAL = auto()         # convert Horizon Coordinates to Equatorial
    EQUATORIAL_HORIZON = auto()         # convert Equatorial Coordinates to Horizon
    ECLIPTIC_EQUATORIAL = auto()        # convert Ecliptic Coordinates to Equatorial
    EQUATORIAL_ECLIPTIC = auto()        # convert Equatorial Coordinates to Ecliptic
    GALACTIC_EQUATORIAL = auto()        # convert Galactic Coordinates to Equatorial
    EQUATORIAL_GALACTIC = auto()        # convert Equatorial Coordinates to Galactic
    PRECESSION_CORR = auto()            # perform a precession correction
    
    # Kepler's Equation    
    KEPLER_SIMPLE = auto()              # use simple iteration to solve Kepler's equation
    KEPLER_NEWTON = auto()              # use Newton's method to solve Kepler's equation    
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
