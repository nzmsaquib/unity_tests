"""
Implements action listeners for the menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import sys

import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT

import Chap4.ChapEnums

#==================================================
# Define listeners for about, exit, instructions
#==================================================

def aboutMenuListener(gui):
    """
    Handle a click on the About menu item
    
    :param tkwidget gui: GUI that the About Box is associated with
    """
    gui.showAboutBox()
    


def exitMenuListener():
    """Handle a click on the Exit menu item"""
    if ASTMsg.pleaseConfirm("Are you sure you want to exit?"," "):
        sys.exit()    
    


def instructionsMenuListener(prt):
    """
    Handle a click on the Instructions menu item
    
    :param ASTPrt prt: ASTPrt object instance created by the top level GUI
    """
    prt.clearTextArea()
    prt.setBoldFont(True)
    prt.println("Instructions",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()

    prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " +
            "intermediate results as the various calculations are performed. Next, select the desired " +
            "calculation to perform from the 'Coord Systems' or 'Kepler's Equation' menu. " +
            "Once a menu item is selected, you will be asked to enter one to four lines of data in the text " +
            "boxes beneath the 'Show Intermediate Calculations' checkbox. The values to be entered " +
            "depend upon what menu item was selected. Finally, after entering all required data, click the " +
            "'Calculate' button at the bottom right of the data input area. The selected computation " +
            "(e.g., Precession Correction) will be done.")
    prt.println()
    
    prt.println("If you checked the 'Show Intermediate Calculations' checkbox, the steps " +
            "required to find a solution will be shown in the scrollable text output area (i.e., where " +
            "these instructions are being displayed). Regardless of whether intermediate results are shown, " +
            "the final result will be shown just below the data input text boxes.")
    prt.println()
    
    prt.setBoldFont(True)
    prt.println("Notes:")
    prt.setBoldFont(False)
    prt.println("1. Angles can be entered in either decimal or DMS format (e.g., xxxd yym zz.zzs). Time-related " +
            "(e.g., Right Ascension, Hour Angle) values can be entered in decimal or HMS format (i.e., hh:mm:ss.ss).")
    prt.println()
    
    prt.printnoln("2. The formulas used in the calculations assume that angles are expressed in degrees. However, " +
            "the trig functions provided in most computer languages and on pocket calculators assume angles " +
            "are in radians. So, if necessary, convert degrees to radians before invoking a trig function, " +
            "and convert radians to degrees when inverse trig functions are used.")
    
    prt.resetCursor()



#==================================================
# Define listener for the menu items
#==================================================

def menuListener(calcToDo,gui):
    """
    Handle a click on the menu items
    
    :param CalculationType calcToDo: the calculation to perform
    :param tkwidget gui: GUI object to which menu items are associated
    """
    cen = Chap4.ChapEnums.CalculationType           # shorten to make typing easier!!!
    
    gui.setCalcToDo(calcToDo)
    gui.clearAllTextAreas()

    #****************Coordinate System Conversions 
    if (calcToDo == cen.HA_RA):
        gui.setDataLabels("Hour Angle (hh:mm:ss.ss)", "UT (hh:mm:ss.ss)",
                           "Longitude (ex: 96.52E, 30.4W)", "Date (mm/dd/yyyy)")
        gui.setResults("Convert Hour Angle to Right Ascension")        
    elif (calcToDo == cen.RA_HA):
        gui.setDataLabels("Right Ascension (hh:mm:ss.ss)", "UT (hh:mm:ss.ss)",
                          "Longitude (ex: 96.52E, 30.4W)", "Date (mm/dd/yyyy)")
        gui.setResults("Convert Right Ascension to Hour Angle")
    elif (calcToDo == cen.HORIZON_EQUATORIAL):
        gui.setDataLabels("Altitude (xxxd yym zz.zzs)", "Azimuth (xxxd yym zz.zzs)",
                          "Latitude (ex: 38.5N, 20.5S)")
        gui.setResults("Convert Horizon Coordinates to Equatorial Coordinates")
    elif (calcToDo == cen.EQUATORIAL_HORIZON):
        gui.setDataLabels("Hour Angle (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                          "Latitude (ex: 38.5N, 20.5S)")
        gui.setResults("Convert Equatorial Coordinates to Horizon Coordinates")
    elif (calcToDo == cen.ECLIPTIC_EQUATORIAL):
        gui.setDataLabels("Ecliptic Lat (xxxd yym zz.zzs)", "Ecliptic Lon (xxxd yym zz.zzs)",
                          "Epoch Coordinates are in (ex: 2000.0)")
        gui.setResults("Convert Ecliptic Coordinates to Equatorial Coordinates")
    elif (calcToDo == cen.EQUATORIAL_ECLIPTIC):
        gui.setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                           "Epoch Coordinates are in (ex: 2000.0)")
        gui.setResults("Convert Equatorial Coordinates to Ecliptic Coordinates")
    elif (calcToDo == cen.GALACTIC_EQUATORIAL):
        gui.setDataLabels("Galactic Lat (xxxd yym zz.zzs)", "Galactic Lon (xxxd yym zz.zzs)",
                           "Epoch (1950.0 or 2000.0)")
        gui.setResults("Convert Galactic Coordinates to Equatorial Coordinates")
    elif (calcToDo == cen.EQUATORIAL_GALACTIC):
        gui.setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                           "Epoch (1950.0 or 2000.0)")
        gui.setResults("Convert Equatorial Coordinates to Galactic Coordinates")
    elif (calcToDo == cen.PRECESSION_CORR):
        gui.setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                           "Epoch to Convert From (ex: 1950.0)", "Epoch to Convert To (ex: 2000.0)")
        gui.setResults("Compute Precession Correction")

    #****************Kepler's Equation menu
    elif (calcToDo == cen.KEPLER_SIMPLE):
        gui.setDataLabels("Mean Anomaly (in degrees)", "Stop Criteria in Radians (ex: 0.000002)",
                           "Orbital Eccentricity")
        gui.setResults("Solve Kepler's Equation via Simple Iteration")
    elif (calcToDo == cen.KEPLER_NEWTON):
        gui.setDataLabels("Mean Anomaly (in degrees)", "Stop Criteria in Radians (ex: 0.000002)",
                          "Orbital Eccentricity")
        gui.setResults("Solve Kepler's Equation via Newton/Raphson Method")
        
    else:           # This should never happen
        ASTMsg.criticalErrMsg("Invalid menu item " + str(calcToDo))
        gui.setCalcToDo(cen.NO_CALC_SELECTED)  



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
