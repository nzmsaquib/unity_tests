"""
Implements the main GUI for the application
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.scrolledtext as tkst

import ASTUtils.ASTAboutBox as ASTAboutBox
import ASTUtils.ASTBookInfo as ASTBookInfo
from ASTUtils.ASTMisc import centerWindow
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTPrt as ASTPrt
import ASTUtils.ASTStyle as ASTStyle

import Chap4.ChapEnums
import Chap4.ChapMenuItems as cmi
import Chap4.GUIButtonsListener as gbl
import Chap4.MenusListener as ml

class ChapGUI():
    """
    Define a class for this chapter's main application window.
    Although the code is written to allow multiple top level
    application windows, there really should only be one.
    """
    #==================================================
    # Class variables
    #==================================================
    WINDOW_TITLE = "Chapter 4 - Coordinate Sys Conversions"
    
    #==================================================
    # Class instance variables
    #==================================================
    # aboutCallback                      callback for the About Box menu item
    # exitCallback                       callback for the Exit menu item
    # instructionsCallback               callback for the Instructions menu item    
    # aboutBox                           instance of an ASTAboutBox object
    # chkboxShowInterimCalcs             current state of the checkbox
    # lblResults                         label for showing the results
    # prt                                instance of an ASTPrt object for doing output
    # textPane                           the scrollable text output area
    
    # guiBtnsCallback                    callback for the Calculate button
    # txtboxData1                        text field for user input
    # lblData1                           label for text field 1
    # txtboxData2                        text field for user input
    # lblData2                           label for text field 2
    # txtboxData3                        text field for user input
    # lblData3                           label for text field 3
    # txtboxData4                        text field for user input
    # lblData4                           label for text field 4
    # calculationToDo                    which calculation to do when Calculate button is selected
      
    def __init__(self,root):
        """
        Constructor for creating a GUI object in the root window.
        The root window (e.g., root = Tk()) must be created external
        to this object.

        :param tkwidget root: parent for this object
        """
        # To avoid an annoying 'flash' as widgets are added to the GUI,
        # we must hide it and then make it visible after all widgets have
        # been created.
        root.withdraw()
        
        # create dynamic variables for various widgets
        self.lblResults = tk.StringVar()               # label for showing the results
        self.chkboxShowInterimCalcs = tk.BooleanVar()  # current state of the checkbox
        self.txtboxData1 = tk.StringVar()
        self.lblData1 = tk.StringVar()
        self.txtboxData2 = tk.StringVar()
        self.lblData2 = tk.StringVar()
        self.txtboxData3 = tk.StringVar()
        self.lblData3 = tk.StringVar()
        self.txtboxData4 = tk.StringVar()
        self.lblData4 = tk.StringVar()
        
        # define listeners for menu items
        self.aboutCallback = lambda: ml.aboutMenuListener(self)
        self.exitCallback = lambda: ml.exitMenuListener()
        self.instructionsCallback = lambda: ml.instructionsMenuListener(self.prt)
        self.guiBtnsCallback = lambda: gbl.calculateListener(self)

        # Initial size of the application window
        winWidth = 950
        winHeight = 750
           
        # create the root window and center it after all the widgets are created
        root.title(ChapGUI.WINDOW_TITLE)
        root.iconbitmap('BlueMarble.ico')
        root.geometry('{}x{}+{}+{}'.format(winWidth,winHeight,100,100))
        
        #=====================================================
        # create the menu bar
        #=====================================================
        # Note: In the code below, the menu font for cascades may not work because
        # tkinter conforms to the underlying OS's rules for the font and style for
        # top level menu items. If the underlying OS handles the font at the system
        # level (as MS Windows does), then any attempt in the code to change the menu font
        # will not work. However, setting the menu font for individual commands
        # does work.        
        
        menubar = tk.Menu(root)
        
        # create the File menu
        filemenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" File ",menu=filemenu)
        filemenu.add_command(label="Exit",command=self.exitCallback)
        
        # create the chapter menus
        cmi.createChapMenuItems(self,menubar)
        
        # create the Help menu
        helpmenu=tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" Help",menu=helpmenu)
        helpmenu.add_command(label="Instructions",command=self.instructionsCallback)
        helpmenu.add_command(label="About",command=self.aboutCallback)

        root.config(menu=menubar)
        
        #=====================================================
        # create the rest of the gui widgets
        #=====================================================
     
        # create the book title
        tk.Label(root,text=ASTBookInfo.BOOK_TITLE,font=ASTStyle.BOOK_TITLE_BOLDFONT,
                 fg=ASTStyle.BOOK_TITLE_COLOR,bg=ASTStyle.BOOK_TITLE_BKG).pack(fill=tk.X,expand=False)
                 
        # create the intermediate calculations checkbox
        tk.Checkbutton(root,text="Show Intermediate Calculations",
                       variable=self.chkboxShowInterimCalcs,
                       onvalue=True,font=ASTStyle.CBOX_FONT).pack(fill=tk.X,expand=False)
                       
        # create the user input areas
        txtWidth = 18
        lblWidth = 32
        # set column weights to allow stretching the labels
        f = tk.LabelFrame(root,padx=2,pady=4,bd=0)
        f.columnconfigure(0,weight=1)
        f.columnconfigure(1,weight=5)
        f.columnconfigure(2,weight=1)
        f.columnconfigure(3,weight=5)
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxData1).grid(row=0,column=0,padx=2,pady=2)
        tk.Label(f,width=lblWidth,textvariable=self.lblData1,justify=tk.LEFT,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=0,column=1,sticky=tk.W)
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxData2).grid(row=0,column=2,padx=2,pady=2)
        tk.Label(f,width=lblWidth,textvariable=self.lblData2,justify=tk.LEFT,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=0,column=3,sticky=tk.W)     
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxData3).grid(row=1,column=0,padx=2,pady=2)
        tk.Label(f,width=lblWidth,textvariable=self.lblData3,justify=tk.LEFT,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=1,column=1,sticky=tk.W)
        tk.Entry(f,width=txtWidth,font=ASTStyle.TEXT_FONT,
                 textvariable=self.txtboxData4).grid(row=1,column=2,padx=2,pady=2)
        tk.Label(f,width=lblWidth,textvariable=self.lblData4,justify=tk.LEFT,font=ASTStyle.TEXT_FONT,
                 anchor="w").grid(row=1,column=3,sticky=tk.W)
        f.pack(fill=tk.X,expand=False)
                       
        # create the results label and calculate button
        f = tk.LabelFrame(root,padx=2,pady=2,bd=0)
        tk.Label(f,textvariable=self.lblResults,font=ASTStyle.TEXT_BOLDFONT,
                 justify=tk.LEFT,anchor="w").pack(side="left",padx=2)
        tk.Button(f,text="Calculate",width=12,font=ASTStyle.BTN_FONT,
                  command=self.guiBtnsCallback).pack(side="right",padx=15)
        f.pack(fill=tk.X,expand=False)
             
        # create the scrollable text area
        self.textPane = tkst.ScrolledText(root,font=ASTStyle.TEXT_FONT,padx=5,pady=5,borderwidth=5,wrap="word")
        self.textPane.pack(fill=tk.BOTH,expand=True)

        # center the window
        centerWindow(root,winWidth,winHeight)
        
        # Initialize the rest of the GUI
        self.setResults(" ")        
        self.chkboxShowInterimCalcs.set(False)
        # Set parent frame for functions that need to relate to a pane/frame/etc. on the GUI
        ASTMsg.setParentFrame(root)
        self.prt = ASTPrt.ASTPrt(self.textPane)
        self.clearAllTextAreas()
        self.calculationToDo = Chap4.ChapEnums.CalculationType.NO_CALC_SELECTED
        
        # Finally, make the GUI visible and create a
        # reusable About Box. For some reason, tkinter requires
        # the parent window to be visible before the About Box
        # can be created.
        root.deiconify()
        self.aboutBox = ASTAboutBox.ASTAboutBox(root,ChapGUI.WINDOW_TITLE)
        
    #=====================================================================
    # The methods below conditionally print to the scrollable text area
    #=====================================================================
    
    def printCond(self,txt,center=False):
        """
        Print text to scrollable output area if the
        'Show Intermediate Calculations' checkbox
        is checked.

        :param str txt: text to be printed
        :param bool center: True if text is to be centered
        """
               
        if self.chkboxShowInterimCalcs.get():
            self.prt.printnoln(txt,center)    
        
    def printlnCond(self,txt="",centerTxt=False):
        """
        Routines to handle sending output text to the scrollable
        output area. These are wrappers around ASTPrt.println
        that see if the 'Show Intermediate Calculations' checkbox
        is checked before invoking the println functions.

        :param str txt: text to be printed
        :param bool centerTxt:true if the text is to be centered
        """        
        
        if self.chkboxShowInterimCalcs.get():
            self.prt.println(txt,centerTxt)

    #==================================================================================
    # Define various methods for manipulating the GUI
    #==================================================================================
    
    #==================================================================================
    # Define various methods for manipulating the GUI
    #==================================================================================
    
    def clearAllTextAreas(self):
        """Clears all of the text areas in the GUI"""
        self.setDataLabels("","","","")
        self.prt.clearTextArea()
        self.setResults("")
        # Must also clear the input data areas
        self.txtboxData1.set("")
        self.txtboxData2.set("")
        self.txtboxData3.set("")
        self.txtboxData4.set("")        
        
    def setDataLabels(self,s1,s2,s3,s4=""):
        """
        Sets data labels in the GUI
        
        :param str s1: string for the 1st data label in the GUI
        :param str s2: string for the 2nd data label in the GUI
        :param str s3: string for the 3rd data label in the GUI
        :param str s4: string for the 4th data label in the GUI
        """    
        self.lblData1.set(s1)
        self.lblData2.set(s2)
        self.lblData3.set(s3)
        self.lblData4.set(s4)
            
    def setResults(self,results):
        """
        Set the results label in the GUI.
        
        :param str results: string to be displayed in the results label
        """
        self.lblResults.set(results)
        
    #===============================================================================
    # The methods below are 'getters' and 'setters' for various items in the GUI.
    #===============================================================================
    
    def getData1(self):
        """Gets the string data associated with the 1st input area"""
        return self.txtboxData1.get()
    
    def getData2(self):
        """Gets the string data associated with the 2nd input area"""
        return self.txtboxData2.get()
    
    def getData3(self):
        """Gets the string data associated with the 3rd input area"""
        return self.txtboxData3.get()
    
    def getData4(self):
        """Gets the string data associated with the 4th input area"""
        return self.txtboxData4.get()

    def getCalcToDo(self):
        """Gets the calculation to perform based upon the most recent menu selection."""
        return self.calculationToDo
    
    def setCalcToDo(self,calcToDo):
        """
        Saves the calculation to perform based upon the just selected menu item
        
        :param CalculationType calcToDo: which calculation to perform
        """
        self.calculationToDo = calcToDo      
        
    #============================================================================
    # The methods below return references to various GUI components such as the
    # scrollable output text area.
    #============================================================================        
        
    def getPrtInstance(self):
        """
        Gets the ASTPrt instance for this application's scrollable text pane area.
        
        :return: the ASTPrt object for printing to the scrollable text area
        """    
        return self.prt
    
    def showAboutBox(self):
        """Shows the About Box."""
        self.aboutBox.show()              
        


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
