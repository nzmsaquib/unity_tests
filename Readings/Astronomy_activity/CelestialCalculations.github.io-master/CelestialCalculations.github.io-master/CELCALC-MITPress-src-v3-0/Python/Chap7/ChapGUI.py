"""
Implements the main GUI for the application
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.scrolledtext as tkst

import ASTUtils.ASTAboutBox as ASTAboutBox
import ASTUtils.ASTBookInfo as ASTBookInfo
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTLatLon as ASTLatLon
from ASTUtils.ASTMisc import centerWindow,DMSFORMAT,HMSFORMAT,SHOW_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTObserver as ASTObserver
import ASTUtils.ASTOrbits as ASTOrbits
import ASTUtils.ASTPrt as ASTPrt
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

import Chap7.ChapMenuItems as cmi
import Chap7.MenusListener as ml

class ChapGUI():
    """
    Define a class for this chapter's main application window.
    Although the code is written to allow multiple top level
    application windows, there really should only be one.
    """
    #==================================================
    # Class variables
    #==================================================
    WINDOW_TITLE = "Chapter 7 - The Moon"
    # define value for the radio buttons for solving Kepler's equation
    __EQofCENTERBTN = 1
    __SIMPLEBTN = 2
    __NEWTONBTN = 3
    
    #==================================================
    # Class instance variables
    #==================================================
    # aboutCallback                     callback for the About Box menu item
    # exitCallback                      callback for the Exit menu item
    # instructionsCallback              callback for the Instructions menu item    
    # aboutBox                          instance of an ASTAboutBox object
    # chkboxShowInterimCalcs            current state of the checkbox
    # lblResults                        label for showing the results
    # prt                               instance of an ASTPrt object for doing output
    # textPane                          the scrollable text output area
    
    # txtboxLat                         user's latitude
    # txtboxLon                         user's longitude
    # txtboxDate                        user's date
    # txtboxLCT                         user's LCT
    # chkboxDST                         whether user is on DST
    # radBtns                           time zone radio buttons group dynamic variable
    # radBtnsKepler                     group for how to solve Kepler's equation
    # lblEpoch                          epoch for the orbital elements
    # observer                          lat/lon, date, time for observer
    
    def __init__(self,root):
        """
        Constructor for creating a GUI object in the root window.
        The root window (e.g., root = Tk()) must be created external
        to this object.

        :param tkwidget root: parent for this object
        """
        # To avoid an annoying 'flash' as widgets are added to the GUI,
        # we must hide it and then make it visible after all widgets have
        # been created.
        root.withdraw()
        
        # create dynamic variables for various widgets
        self.lblResults = tk.StringVar()               # label for showing the results
        self.chkboxShowInterimCalcs = tk.BooleanVar()  # current state of the checkbox
        self.txtboxLat = tk.StringVar()
        self.txtboxLon = tk.StringVar()
        self.txtboxDate = tk.StringVar()
        self.txtboxLCT = tk.StringVar()
        self.chkboxDST = tk.BooleanVar()
        self.radBtns = tk.IntVar()
        self.radBtnsKepler = tk.IntVar()
        self.lblEpoch = tk.StringVar()
        
        # define listeners for menu items
        self.aboutCallback = lambda: ml.aboutMenuListener(self)
        self.exitCallback = lambda: ml.exitMenuListener()
        self.instructionsCallback = lambda: ml.instructionsMenuListener(self)
        
        # define a listener for changes to the radio buttons
        self.radBtnCallback = lambda: self.__radBtnCallback()
            
        # Initial size of the application window
        winWidth = 980
        winHeight = 740
           
        # create the root window and center it after all the widgets are created
        root.title(ChapGUI.WINDOW_TITLE)
        root.iconbitmap('BlueMarble.ico')
        root.geometry('{}x{}+{}+{}'.format(winWidth,winHeight,100,100))
        
        #=====================================================
        # create the menu bar
        #=====================================================
        # Note: In the code below, the menu font for cascades may not work because
        # tkinter conforms to the underlying OS's rules for the font and style for
        # top level menu items. If the underlying OS handles the font at the system
        # level (as MS Windows does), then any attempt in the code to change the menu font
        # will not work. However, setting the menu font for individual commands
        # does work.        
        
        menubar = tk.Menu(root)
        
        # create the File menu
        filemenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" File ",menu=filemenu)
        filemenu.add_command(label="Exit",command=self.exitCallback)
        
        # create the chapter menus
        cmi.createChapMenuItems(self,menubar)
        
        # create the Help menu
        helpmenu=tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
        menubar.add_cascade(label=" Help",menu=helpmenu)
        helpmenu.add_command(label="Instructions",command=self.instructionsCallback)
        helpmenu.add_command(label="About",command=self.aboutCallback)

        root.config(menu=menubar)
        
        #=====================================================
        # create the rest of the gui widgets
        #=====================================================
     
        # create the book title
        tk.Label(root,text=ASTBookInfo.BOOK_TITLE,font=ASTStyle.BOOK_TITLE_BOLDFONT,
                 fg=ASTStyle.BOOK_TITLE_COLOR,bg=ASTStyle.BOOK_TITLE_BKG).pack(fill=tk.X,expand=False)
                                     
        # create the Obs location grouping
        txtwidth = 16
        f = tk.LabelFrame(root,padx=2,pady=0,bd=4,relief=tk.RIDGE)
        tk.Label(f,text="Observer Location and Time",font=ASTStyle.TEXT_SMBOLDFONT,\
                 justify=tk.CENTER).grid(row=0,column=0,columnspan=6)
        tk.Entry(f,width=txtwidth,font=ASTStyle.TEXT_SMFONT,\
                 textvariable=self.txtboxLat).grid(row=1,column=0,padx=2)
        tk.Label(f,text="Latitude",font=ASTStyle.TEXT_SMBOLDFONT,\
                 anchor="w").grid(row=1,column=1,padx=2,sticky=tk.W)
        tk.Checkbutton(f,text="Daylight Saving Time",
                       variable=self.chkboxDST,
                       onvalue=True,font=ASTStyle.CBOX_FONT_SMALL).grid(row=1,column=2,padx=5)
        tk.Entry(f,width=12,font=ASTStyle.TEXT_SMFONT,\
                 textvariable=self.txtboxDate).grid(row=1,column=3,padx=5)
        tk.Label(f,text="Date",font=ASTStyle.TEXT_SMBOLDFONT,justify=tk.LEFT).grid(row=1,column=4,padx=5)      
        tk.Entry(f,width=txtwidth,font=ASTStyle.TEXT_SMFONT,\
                 textvariable=self.txtboxLon).grid(row=2,column=0,padx=2)
        tk.Label(f,text="Longitude",font=ASTStyle.TEXT_SMBOLDFONT,\
                 anchor="w").grid(row=2,column=1,padx=2,sticky=tk.W)
        f2=tk.LabelFrame(f,padx=2,pady=0,bd=0)
        tk.Label(f2,text="Time Zone",font=ASTStyle.TEXT_SMBOLDFONT,justify=tk.LEFT).pack(side=tk.LEFT)
        for zone in ASTLatLon.TimeZoneType:
            # Use the time zone adjustment as the value to be returned for the radio button
            if zone == ASTLatLon.TimeZoneType.LONGITUDE:
                txt = "Use Lon"
            else:
                txt = zone.toString()
            tk.Radiobutton(f2,text=txt,font=ASTStyle.RADBTN_FONT,
                           value=zone.getAdjust(),command=self.radBtnCallback,
                           variable=self.radBtns).pack(padx=2,side=tk.LEFT)
        f2.grid(row=2,column=2,padx=2,sticky=tk.W)
        tk.Entry(f,width=12,font=ASTStyle.TEXT_SMFONT,\
                 textvariable=self.txtboxLCT).grid(row=2,column=3,padx=5)
        tk.Label(f,text="LCT",font=ASTStyle.TEXT_SMBOLDFONT,\
                 anchor="w").grid(row=2,column=4,padx=5,sticky=tk.W)           
        f.pack()
        
        # Create the checkbox, Kepler Eq radio buttons, and epoch label
        f = tk.LabelFrame(root,padx=2,pady=2,bd=0)
        tk.Checkbutton(f,text="Show Intermediate Calculations",
                       variable=self.chkboxShowInterimCalcs,
                       onvalue=True,font=ASTStyle.CBOX_FONT).grid(row=0,column=0,padx=5)
        f2=tk.LabelFrame(f,padx=2,pady=0,bd=4,relief=tk.SUNKEN)
        tk.Radiobutton(f2,text="EQ of Center",font=ASTStyle.RADBTN_FONT,
                           value=ChapGUI.__EQofCENTERBTN,
                           variable=self.radBtnsKepler).grid(row=0,column=0,padx=2)
        tk.Radiobutton(f2,text="Simple",font=ASTStyle.RADBTN_FONT,
                           value=ChapGUI.__SIMPLEBTN,
                           variable=self.radBtnsKepler).grid(row=0,column=1,padx=2)
        tk.Radiobutton(f2,text="Newton",font=ASTStyle.RADBTN_FONT,
                           value=ChapGUI.__NEWTONBTN,
                           variable=self.radBtnsKepler).grid(row=0,column=2,padx=2)
        f2.grid(row=0,column=1,padx=2,sticky=tk.W)
        tk.Label(f,text="Epoch: ",textvariable=self.lblEpoch,font=ASTStyle.TEXT_BOLDFONT,\
                 anchor="w").grid(row=0,column=2,padx=2,sticky=tk.W)
        f.pack()
        
        # Create the result area
        tk.Label(root,text="Result: ",font=ASTStyle.TEXT_BOLDFONT,anchor="w",\
                 textvariable=self.lblResults,\
                 padx=5).pack(fill=tk.X,expand=False)
             
        # create the scrollable text area
        self.textPane = tkst.ScrolledText(root,font=ASTStyle.TEXT_FONT,padx=5,pady=5,borderwidth=5,wrap="word")
        self.textPane.pack(fill=tk.BOTH,expand=True)

        # center the window
        centerWindow(root,winWidth,winHeight)
               
        # Initialize the rest of the GUI
        self.setResults(" ")
        self.chkboxShowInterimCalcs.set(False)
        self.chkboxDST.set(False)
        # Set parent frame for functions that need to relate to a pane/frame/etc. on the GUI
        ASTMsg.setParentFrame(root)
        ASTQuery.setParentFrame(root)
        self.prt = ASTPrt.ASTPrt(self.textPane)
        self.prt.clearTextArea()
        
        self.setLonRadBtn()
        self.setKeplerRadBtn(ChapGUI.__EQofCENTERBTN)
        self.txtboxLat.set("")
        self.txtboxLon.set("")
        self.txtboxDate.set("")
        self.txtboxLCT.set("")
       
        # Load the default orbital elements and observer location (if it exists)
        ASTOrbits.initOrbitalElements(self.prt)
        self.setEpoch(ASTOrbits.getOEEpochDate())
        self.observer = ASTObserver.ASTObserver()
        self.__setObsDataInGUI(self.observer)

        # Finally, make the GUI visible and create a
        # reusable About Box. For some reason, tkinter requires
        # the parent window to be visible before the About Box
        # can be created.
        root.deiconify()
        self.aboutBox = ASTAboutBox.ASTAboutBox(root,ChapGUI.WINDOW_TITLE)
        
    #==================================================================
    # Define functions to manage the time zone radio buttons.
    # Python manages radio buttons as a group to ensure that only
    # one radio button in a group is selected at a time. However,
    # we need routines to set/get a radio button status.
    #==================================================================
    
    def getSelectedRBStatus(self):
        """
        Determine what time zone radio button is selected
        
        :return: a time zone type for whatever is the currently selected time zone radio button
        """
        return ASTLatLon.intToTZType(self.radBtns.get())
    
    def setKeplerRadBtn(self,btn):
        """Sets the Kepler's Equation radio button"""
        self.radBtnsKepler.set(btn)
    
    def setLonRadBtn(self):
        """Sets the Longitude radio button"""
        self.txtboxLon.set("")
        # Pydev in the Eclipse IDE may show the next line as an error, but it is not.
        # The @UndefinedVariable is added to avoid the erroneous error message.
        self.radBtns.set(ASTLatLon.TimeZoneType.LONGITUDE.getAdjust())  # @UndefinedVariable
    
    def __radBtnCallback(self):
        """Handles changes to which Time Zone radio button is selected"""
        self.prt.clearTextArea()
        if (self.radBtns.get() == ASTLatLon.TimeZoneType.LONGITUDE.getAdjust()): # @UndefinedVariable
            self.setLonRadBtn()
        
    #=====================================================================
    # The methods below conditionally print to the scrollable text area
    #=====================================================================
    
    def printCond(self,txt,center=False):
        """
        Print text to scrollable output area if the
        'Show Intermediate Calculations' checkbox
        is checked.

        :param str txt: text to be printed
        :param bool center: True if text is to be centered
        """        
        if self.chkboxShowInterimCalcs.get():
            self.prt.printnoln(txt,center)    
        
    def printlnCond(self,txt="",centerTxt=False):
        """
        Routines to handle sending output text to the scrollable
        output area. These are wrappers around ASTPrt.println
        that see if the 'Show Intermediate Calculations' checkbox
        is checked before invoking the println functions.

        :param str txt: text to be printed
        :param bool centerTxt: true if the text is to be centered
        """         
        if self.chkboxShowInterimCalcs.get():
            self.prt.println(txt,centerTxt)

    #==================================================================================
    # Define various methods for manipulating the GUI
    #==================================================================================
    
    def checkOEDBLoaded(self,showErrors=SHOW_ERRORS):
        """
        Checks to see if an orbital elements database has been
        successfully loaded, and display an error message if not.
        
        :param bool showErrors: display error message if true
        :return: true if orbital elements have been loaded, else false
        """
        if (ASTOrbits.isOrbElementsDBLoaded()):
            return True
        
        if (showErrors):
            ASTMsg.errMsg("No Orbital Elements data is currently loaded.", "No Orbital Elements Data Loaded");

        return False      
            
    def clearTextAreas(self):
        """Clears the text areas in the GUI"""
        self.prt.clearTextArea()    
        self.setResults("")
            
    def setResults(self,results):
        """
        Set the results label in the GUI.
        
        :param str results: string to be displayed in the results label
        """
        self.lblResults.set("Result: " + results)
        
    def setEpoch(self,epoch):
        """
        Sets the Epoch label in the GUI.
        
        :param float epoch: the epoch to be displayed in the GUI
        """
        self.lblEpoch.set("Epoch: " + ASTStr.strFormat(ASTStyle.EPOCHFORMAT,epoch))
    
    def validateGUIObsLoc(self):
        """
        See if the observer location, date, and time
        currently in the GUI is valid.

        :return: true if valid, otherwise false
        """
        tz = self.getSelectedRBStatus()
        tzStr = tz.toString()
        return ASTObserver.isValidObsLoc(self.observer,self.txtboxLat.get(),self.txtboxLon.get(),tzStr,\
                                 self.txtboxDate.get(),self.txtboxLCT.get())
        
    #============================================================================
    # The methods below return references to various GUI components such as the
    # scrollable output text area.
    #============================================================================        
        
    def getcurrentObserver(self):
        """Gets the current observer."""
        return self.observer
    
    def getDSTStatus(self):
        """
        Gets the current status of the DST check box
        
        :return: true if the DST check box is checked, else false
        """
        return self.chkboxDST.get()
    
    def getEQofCenterStatus(self):
        """
        Gets the current status of the EQ of Center radio button
        
        :return: true if the EQ of Center radio button is selected, else false
        """
        return (self.radBtnsKepler.get() == ChapGUI.__EQofCENTERBTN)

    def getNewtonMethodStatus(self):
        """
        Gets the current status of the Newton's Method radio button
        
        :return: true if the Newton's Method radio button is selected, else false
        """
        return (self.radBtnsKepler.get() == ChapGUI.__NEWTONBTN)

    def getSimpleIterationStatus(self):
        """
        Gets the current status of the Simple Iteration radio button
        
        :return: true if the Simple Iteration radio button is selected, else false
        """
        return (self.radBtnsKepler.get() == ChapGUI.__SIMPLEBTN)
    
    def getShowInterimCalcsStatus(self):
        """
        Gets the current status of the Show Interim Calculations check box
        
        :return: true if the interim calculations check box is true, else false
        """
        return self.chkboxShowInterimCalcs.get()
        
    def getPrtInstance(self):
        """
        Gets the ASTPrt instance for this application's scrollable text pane area.
        
        :return: the ASTPrt object for printing to the scrollable text area
        """       
        return self.prt
    
    def showAboutBox(self):
        """Shows the About Box."""
        self.aboutBox.show() 
        
    #============================================================
    # Define some useful helper methods used only in this class
    #============================================================
    
    def __setObsDataInGUI(self,obs):
        """
        Sets the observer location in the GUI. This is intended to be
        done one time only during the program initialization since a
        default location may be read from a data file.

        :param ASTObserver obs: observer location object
        """
        self.txtboxLat.set(ASTLatLon.latToStr_obj(obs.getObsLocation(), DMSFORMAT))
        self.txtboxLon.set(ASTLatLon.lonToStr_obj(obs.getObsLocation(), DMSFORMAT))
        self.txtboxDate.set(ASTDate.dateToStr_obj(obs.getObsDate()))
        self.txtboxLCT.set(ASTTime.timeToStr_obj(obs.getObsTime(), HMSFORMAT))
        
        self.__setRadBtn(obs.getObsTimeZone())
      
    def __setRadBtn(self,tZone):
        """
        Set the radio button corresponding to a time zone
        
        :param TimeZoneType tZone: time zone to set
        """
        # Pydev in the Eclipse IDE may show the next lines as errors, but they are not.
        # The @UndefinedVariable is added to avoid the erroneous error messages.
        if (tZone == ASTLatLon.TimeZoneType.PST):
            self.radBtns.set(ASTLatLon.TimeZoneType.PST.getAdjust()) # @UndefinedVariable
        elif (tZone == ASTLatLon.TimeZoneType.MST):
            self.radBtns.set(ASTLatLon.TimeZoneType.MST.getAdjust()) # @UndefinedVariable
        elif (tZone == ASTLatLon.TimeZoneType.CST):
            self.radBtns.set(ASTLatLon.TimeZoneType.CST.getAdjust()) # @UndefinedVariable
        elif (tZone == ASTLatLon.TimeZoneType.EST):
            self.radBtns.set(ASTLatLon.TimeZoneType.EST.getAdjust()) # @UndefinedVariable
        else:
            self.radBtns.set(ASTLatLon.TimeZoneType.LONGITUDE.getAdjust()) # @UndefinedVariable       
        


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
