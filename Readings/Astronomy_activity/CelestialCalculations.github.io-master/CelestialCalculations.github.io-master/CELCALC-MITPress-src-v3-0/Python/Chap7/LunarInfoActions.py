"""
Handles the Lunar Info menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import HIDE_ERRORS,DECFORMAT,DMSFORMAT,HMSFORMAT
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

# Set a default termination criteria (in radians) for solving Kepler's equation
__termCriteria = 0.000002



def calcDistAndAngDiameter(gui):
    """
    Calculate the Moon's distance and angular diameter.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()

    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):   
        return
    
    # Get Moon index after validating that orbital elements are loaded
    idxMoon = ASTOrbits.getOEDBMoonIndex()
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Moon's Distance and Angular Diameter for the Current Date", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    Ecc = ASTOrbits.getOEObjEccentricity(idxMoon)
    
    # Technically, we should be using the UT for the observer rather than UT=0, but
    # the difference is so small that it isn't worth converting LCT to UT
    eclCoord = ASTOrbits.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                               observer.getObsDate().getYear(),0.0,solveTrueAnomaly,__termCriteria)
    Vmoon = eclCoord[ASTOrbits.MOON_TRUEANOM]
    
    gui.printlnCond("1.  Compute the Moon's true anomaly at 0 hours UT for the given date.")
    gui.printlnCond("    Vmoon = " + ASTAngle.angleToStr_dec(Vmoon, DECFORMAT) + " degrees")
    gui.printlnCond()

    dF = (1 + Ecc * ASTMath.COS_D(Vmoon)) / (1 - Ecc * Ecc)
    gui.printlnCond("2.  Compute F = [1 + eccentricity*cos(Vmoon)] / [1-eccentricity^2]")
    gui.printlnCond("              = [1 + " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc) +
                    "*cos(" + ASTAngle.angleToStr_dec(Vmoon,DECFORMAT) +
                    ")] / [1 - " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Ecc) + "^2]")
    gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dF))
    gui.printlnCond()
    
    dDist = ASTOrbits.getOEObjSemiMajAxisKM(idxMoon) / dF
    dDist = ASTMath.Round(dDist, 2)
    gui.printlnCond("3.  Compute the distance to the Moon.")
    gui.printlnCond("    Dist = a0/F where a0 is the length of the Moon's semi-major axis in km")
    gui.printlnCond("    Dist = (" + ASTStr.insertCommas(ASTOrbits.getOEObjSemiMajAxisKM(idxMoon)) + 
                    ")/" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dF))
    gui.printlnCond("         = " + ASTStr.insertCommas(dDist) + " km")
    gui.printlnCond()
    
    dAngDiameter = ASTOrbits.getOEObjAngDiamDeg(idxMoon) * dF
    gui.printlnCond("4.  Compute the Moon's angular diameter.")
    gui.printlnCond("    Theta_Moon = Theta_0*F where Theta_0 is the Moon's angular diameter in degrees")
    gui.printlnCond("    when the Moon is distance a0 away.")
    gui.printlnCond("    Theta_Moon = " + ASTAngle.angleToStr_dec(ASTOrbits.getOEObjAngDiamDeg(idxMoon), DECFORMAT) + "*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dF) + " = " + ASTAngle.angleToStr_dec(dAngDiameter, DECFORMAT) + 
                    " degrees")
    gui.printlnCond()
    
    gui.printlnCond("5.  Convert distance to miles and angular diameter to DMS format.")
    gui.printlnCond("    Dist = " + ASTStr.insertCommas(dDist) + " km = " + 
                    ASTStr.insertCommas(ASTMath.KM2Miles(dDist), 2) + " miles")
    gui.printlnCond("    Theta_Moon = " + ASTAngle.angleToStr_dec(dAngDiameter, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("On " + ASTDate.dateToStr_obj(observer.getObsDate()) + ", the Moon is/was " + 
                    ASTStr.insertCommas(dDist) + " km away")
    gui.printlnCond("and its angular diameter is/was " + ASTAngle.angleToStr_dec(dAngDiameter, DMSFORMAT))
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Moon's Distance: " + ASTStr.insertCommas(dDist) + " km (" +
                   ASTStr.insertCommas(ASTMath.KM2Miles(dDist), 2) +
                   " miles), Angular Diameter: " + ASTAngle.angleToStr_dec(dAngDiameter, DMSFORMAT))



def calcMoonPercentIllum(gui):
    """
    Calculate percentage of the Moon's surface
    that is illuminated as seen from Earth

    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):     
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Calculate the Percentage Illumination for the Moon as seen from Earth", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    # How to find the Sun's true anomaly
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    
    eclCoordSun = ASTOrbits.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                 observer.getObsDate().getYear(),0.0,solveTrueAnomaly,__termCriteria)
    Lsun = eclCoordSun[ASTOrbits.SUN_ECLLON]
    gui.printlnCond("1.  Calculate the Sun's ecliptic longitude for the date in question.")
    gui.printlnCond("    Lsun = " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eclCoordMoon = ASTOrbits.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                   observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, __termCriteria)
    Bmoon = eclCoordMoon[ASTOrbits.MOON_ECLLAT]
    Lmoon = eclCoordMoon[ASTOrbits.MOON_ECLLON]
    Mmoon = eclCoordMoon[ASTOrbits.MOON_MEANANOM]
    
    gui.printlnCond("2.  Calculate the Moon's mean anomaly and ecliptic coordinates for the date in question.")
    gui.printlnCond("       Mm = " + ASTAngle.angleToStr_dec(Mmoon, DECFORMAT) + " degrees")
    gui.printlnCond("    Bmoon = " + ASTAngle.angleToStr_dec(Bmoon, DECFORMAT) + " degrees")
    gui.printlnCond("    Lmoon = " + ASTAngle.angleToStr_dec(Lmoon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    d = ASTMath.COS_D(Lmoon - Lsun) * ASTMath.COS_D(Bmoon)
    d = ASTMath.INVCOS_D(d)
    gui.printlnCond("3.  Calculate the Moon's age in degrees.")
    gui.printlnCond("    d = inv cos[cos(Lmoon - Lsun)*cos(Bmoon)]")
    gui.printlnCond("      = inv cos[cos(" + ASTAngle.angleToStr_dec(Lmoon, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + ")*cos(" +
                    ASTAngle.angleToStr_dec(Bmoon, DECFORMAT) + ")]")
    gui.printlnCond("      = " + ASTAngle.angleToStr_dec(d, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dPA = (1 - 0.0549 * ASTMath.SIN_D(Mmoon)) / (1 - 0.0167 * ASTMath.SIN_D(Mmoon))
    dPA = 180.0 - d - 0.1468 * dPA * ASTMath.SIN_D(d)
    gui.printlnCond("4.  Calculate the Moon's phase angle.")
    gui.printlnCond("    PA = 180 - d - 0.1468*[(1-0.0549*sin(Mm)) / (1-0.0167*sin(Mm))]*sin(d)")
    gui.printlnCond("       = 180-" + ASTAngle.angleToStr_dec(d, DECFORMAT) + 
                    "-0.1468*[(1-0.0549*sin(" + ASTAngle.angleToStr_dec(Mmoon, DECFORMAT) +
                    "))/(1-0.0167*sin(" + ASTAngle.angleToStr_dec(Mmoon, DECFORMAT) + 
                    "))]*sin(" + ASTAngle.angleToStr_dec(d, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(dPA, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dKpcnt = 50 * (1 + ASTMath.COS_D(dPA))
    gui.printlnCond("5.  Calculate the percent illumination.")
    gui.printlnCond("    K% = 100*[(1 - cos(PA))/2] = 0*[(1 - cos(" + ASTAngle.angleToStr_dec(dPA, DECFORMAT) + "))/2]")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dKpcnt))
    gui.printlnCond()
    iKpcnt = int(ASTMath.Round(dKpcnt))
    
    gui.printlnCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),observer.getObsDate().getiDay(),
                                                        observer.getObsDate().getYear()) +
                    ", the Moon is " + str(iKpcnt) + "% illuminated as seen from Earth")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("On " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),
                                                 observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
                   ", the Moon is " + str(iKpcnt) + "% illuminated.")



def calcMoonPhase(gui):
    """
    Calculate the Moon's phase and age
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):      
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Calculate the Moon's Phase and Age", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    # How to find the Sun's true anomaly
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    
    # Technically, we should be using the UT for the observer rather than UT=0, but
    # the difference is so small that it isn't worth converting LCT to UT
    eclCoordSun = ASTOrbits.calcSunEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                 observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, __termCriteria)
    Lsun = eclCoordSun[ASTOrbits.SUN_ECLLON]
    gui.printlnCond("1.  Calculate the Sun's ecliptic longitude for the date in question.")
    gui.printlnCond("    Lsun = " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eclCoordMoon = ASTOrbits.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                   observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, __termCriteria)
    L_t = eclCoordMoon[ASTOrbits.MOON_TRUELON]
    gui.printlnCond("2.  Calculate the Moon's true ecliptic longitude for the date in question.")
    gui.printlnCond("    L_t = " + ASTAngle.angleToStr_dec(L_t, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Age = L_t - Lsun
    gui.printlnCond("3.  Calculate the Moon's age in degrees.")
    gui.printlnCond("    Age = L_t - Lsun = " + ASTAngle.angleToStr_dec(L_t, DECFORMAT) + " - " + 
                    ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " = " + ASTAngle.angleToStr_dec(Age, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Age = ASTMath.xMOD(Age, 360)
    gui.printlnCond("4.  If necessary, adjust Age to be in the range [0, 360]")
    gui.printlnCond("    Age = " + ASTAngle.angleToStr_dec(Age, DECFORMAT) + " degrees")
    gui.printlnCond("    Note: The Moon's age in days is Age/12.1907 = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Age / 12.1907) + " days")
    gui.printlnCond()
    
    dF = (1 - ASTMath.COS_D(Age)) / 2.0
    dF = ASTMath.Round(dF, 2)
    Age = ASTMath.Round(Age, 2)
    gui.printlnCond("5.  Calculate the Moon's phase.")
    gui.printlnCond("    F = (1 - cos(Age))/2 = (1 - cos(" + ASTAngle.angleToStr_dec(Age, DECFORMAT) +
                    "))/2 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dF))
    gui.printlnCond()
    
    gui.printlnCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),
                                                        observer.getObsDate().getiDay(),
                                                        observer.getObsDate().getYear()) + " for the current observer")
    gui.printlnCond("the Moon's age is " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Age) + 
                    " degrees (F = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dF) +
                    "), which is closest to a " + __ageToStr(Age) + " Moon.")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("On " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                 observer.getObsDate().getYear()) + ", Age = " + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,Age) + 
                    " deg, F = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,dF) +
                    " (closest to a " + __ageToStr(Age) + " Moon)")    



def calcMoonPosition(gui):
    """
    Calculate the position of the Moon, using the currently
    loaded orbital elements and the current observer position.

    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance() 
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()

    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):   
        return
    
    # Get Sun index after validating that orbital elements are loaded
    idxMoon = ASTOrbits.getOEDBMoonIndex()
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Moon's Position for the Current Observer", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    inclin = ASTOrbits.getOEObjInclination(idxMoon)
    
    # Do all the time-related calculations
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    
    # adjust the date, if needed, since the LCTtoUT conversion could have
    # changed the date
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(), dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST, observer.getObsLon())
    
    gui.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
    gui.printlnCond("    LCT = " + ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) +
                    " hours, date is " + ASTDate.dateToStr_obj(observer.getObsDate()) + ")")
    gui.printCond("     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        gui.printCond("previous day ")
    elif (dateAdjust > 0):
        gui.printCond("next day ")
    gui.printlnCond(ASTDate.dateToStr_obj(adjustedDate) + ")")
    gui.printlnCond("    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond("    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    TT = UT + (63.8 / 3600.0)
    gui.printlnCond("2.  Compute TT = UT + 63.8s = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " + 63.8s = " +
                    ASTTime.timeToStr_dec(TT, DECFORMAT) + " hours")
    gui.printlnCond()
    
    JDe = ASTOrbits.getOEEpochJD()
    gui.printlnCond("3.  Compute the Julian day number for the standard epoch.")
    gui.printlnCond("    Epoch: " + ASTStr.strFormat(ASTStyle.EPOCHFORMAT,ASTOrbits.getOEEpochDate()))
    gui.printlnCond("    JDe = " + ASTStr.strFormat(ASTStyle.JDFormat,JDe))
    gui.printlnCond()
    
    JD = ASTDate.dateToJD_mdy(adjustedDate.getMonth(), adjustedDate.getdDay() + (TT / 24.0), adjustedDate.getYear())
    gui.printlnCond("4.  Compute the Julian day number for the desired date, being sure to use the")
    gui.printlnCond("    Greenwich date and TT from steps 1 and 2, and including the fractional part of the day.")
    gui.printlnCond("    JD = " + ASTStr.strFormat(ASTStyle.JDFormat,JD))
    gui.printlnCond()
    
    De = JD - JDe
    gui.printlnCond("5.  Compute the total number of elapsed days, including fractional days,")
    gui.printlnCond("    since the standard epoch.")
    gui.printlnCond("    De = JD - JDe = " + ASTStr.strFormat(ASTStyle.JDFormat,JD) + " - " +
                    ASTStr.strFormat(ASTStyle.JDFormat,JDe))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De) + " days")
    gui.printlnCond()
    
    # Use UT to get the Sun's position, not TT
    eclCoord = ASTOrbits.calcSunEclipticCoord(adjustedDate.getMonth(), adjustedDate.getiDay(),adjustedDate.getYear(),
                                              UT, solveTrueAnomaly, __termCriteria)
    Lsun = eclCoord[ASTOrbits.SUN_ECLLON]
    Msun = eclCoord[ASTOrbits.SUN_MEANANOM]
    gui.printlnCond("6.  Calculate the Sun's ecliptic longitude and mean anomaly for the UT date and UT time.")
    gui.printlnCond("    Lsun = " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " degrees")
    gui.printlnCond("    Msun = " + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    L_uncor = 13.176339686 * De + ASTOrbits.getOEObjLonAtEpoch(idxMoon)
    gui.printlnCond("7.  Calculate the Moon's uncorrected mean ecliptic longitude.")
    gui.printlnCond("    L_uncor = 13.176339686 * De + L_0 = 13.176339686 * " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De) +
                    " + " + ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtEpoch(idxMoon), DECFORMAT))
    gui.printlnCond("            = " + ASTAngle.angleToStr_dec(L_uncor, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("8.  If necessary, use the MOD function to adjust L_uncor to the range [0, 360].")
    gui.printCond("    L_uncor = L_uncor MOD 360 = " + ASTAngle.angleToStr_dec(L_uncor, DECFORMAT) + " MOD 360 = ")
    L_uncor = ASTMath.xMOD(L_uncor, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(L_uncor, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Omega = ASTOrbits.getOEObjLonAscNode(idxMoon) - 0.0529539 * De
    gui.printlnCond("9.  Calculate the Moon's uncorrected mean longitude of the ascending node.")
    gui.printlnCond("    Omega = Omega_0 - 0.0529539 * De = " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAscNode(idxMoon), DECFORMAT) + " - 0.0529539 * " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De))
    gui.printlnCond("          = " + ASTAngle.angleToStr_dec(Omega, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("10. If necessary, adjust Omega to be in the range [0, 360].")
    gui.printCond("    Omega = Omega MOD 360 = " + ASTAngle.angleToStr_dec(Omega, DECFORMAT) + " MOD 360 = ")
    Omega = ASTMath.xMOD(Omega, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(Omega, DECFORMAT) + " degrees")
    gui.printlnCond()

    Mm = L_uncor - 0.1114041 * De - ASTOrbits.getOEObjLonAtPeri(idxMoon)
    gui.printlnCond("11. Compute the Moon's uncorrected mean anomaly.")
    gui.printlnCond("    Mm = L_uncor - 0.1114041*De - W_0 = " + ASTAngle.angleToStr_dec(L_uncor, DECFORMAT) +
                    "-0.1114041*(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,De) + ")-" + 
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtPeri(idxMoon), DECFORMAT))
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Mm, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("12. Adjust Mm if necessary to be in the range [0, 360].")
    gui.printCond("    Mm = Mm MOD 360 = " + ASTAngle.angleToStr_dec(Mm, DECFORMAT) + " MOD 360 = ")
    Mm = ASTMath.xMOD(Mm, 360.0)
    gui.printlnCond(ASTAngle.angleToStr_dec(Mm, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Ae = 0.1858 * ASTMath.SIN_D(Msun)
    gui.printlnCond("13. Calculate the annual equation correction.")
    gui.printlnCond("    Ae = 0.1858 * sin(Msun) = 0.1858 * sin(" + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + ") = " +
                    ASTAngle.angleToStr_dec(Ae, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Ev = 1.2739 * ASTMath.SIN_D(2 * (L_uncor - Lsun) - Mm)
    gui.printlnCond("14. Calculate the evection correction.")
    gui.printlnCond("    Ev = 1.2739 * sin[2 * (L_uncor - Lsun) - Mm]")
    gui.printlnCond("       = 1.2739 * sin[2 * (" + ASTAngle.angleToStr_dec(L_uncor, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + ") - " + ASTAngle.angleToStr_dec(Mm, DECFORMAT) + "]")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Ev, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Ca = Mm + Ev - Ae - 0.37 * ASTMath.SIN_D(Msun)
    gui.printlnCond("15. Calculate the mean anomaly correction.")
    gui.printlnCond("    Ca = Mm + Ev - Ae - 0.37 * sin(Msun)")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Mm, DECFORMAT) + " + " + ASTAngle.angleToStr_dec(Ev, DECFORMAT) +
                    " - " + ASTAngle.angleToStr_dec(Ae, DECFORMAT) + " - 0.37 * sin(" + 
                    ASTAngle.angleToStr_dec(Msun, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Ca, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Vmoon = 6.2886 * ASTMath.SIN_D(Ca) + 0.214 * ASTMath.SIN_D(2 * Ca)
    gui.printlnCond("16. Calculate the Moon's true anomaly.")
    gui.printlnCond("    Vmoon = 6.2886 * sin(Ca) + 0.214 * sin(2*Ca)")
    gui.printlnCond("          = 6.2886 * sin(" + ASTAngle.angleToStr_dec(Ca, DECFORMAT) + ") + 0.214 * sin(2*" + 
                    ASTAngle.angleToStr_dec(Ca, DECFORMAT) + ")")
    gui.printlnCond("          = " + ASTAngle.angleToStr_dec(Vmoon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    L_p = L_uncor + Ev + Vmoon - Ae
    gui.printlnCond("17. Calculate the corrected ecliptic longitude.")
    gui.printlnCond("    L_p = L_uncor + Ev + Vmoon - Ae = " + ASTAngle.angleToStr_dec(L_uncor, DECFORMAT) + "+" +
                    ASTAngle.angleToStr_dec(Ev, DECFORMAT) + "+(" + ASTAngle.angleToStr_dec(Vmoon, DECFORMAT) + ")-(" + 
                    ASTAngle.angleToStr_dec(Ae, DECFORMAT) + ")")
    gui.printlnCond("        = " + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    V = 0.6583 * ASTMath.SIN_D(2 * (L_p - Lsun))
    gui.printlnCond("18. Compute the variation correction.")
    gui.printlnCond("    V = 0.6583 * sin[2 * (L_p - Lsun)] = 0.6583 * sin[2 * (" +
                    ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " - " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + ")]")
    gui.printlnCond("      = " + ASTAngle.angleToStr_dec(V, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    L_t = L_p + V
    gui.printlnCond("19. Calculate the Moon's true ecliptic longitude.")
    gui.printlnCond("    L_t = L_p + V = " + ASTAngle.angleToStr_dec(L_p, DECFORMAT) + " + " +
                    ASTAngle.angleToStr_dec(V, DECFORMAT) + " = " + ASTAngle.angleToStr_dec(L_t, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Omega_p = Omega - 0.16 * ASTMath.SIN_D(Msun)
    gui.printlnCond("20. Compute a corrected ecliptic longitude of the ascending node.")
    gui.printlnCond("    Omega_p = Omega - 0.16 * sin(Msun) = " + ASTAngle.angleToStr_dec(Omega, DECFORMAT) + 
                    " - 0.16 * sin(" + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + ")")
    gui.printlnCond("            = " + ASTAngle.angleToStr_dec(Omega_p, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    y = ASTMath.SIN_D(L_t - Omega_p) * ASTMath.COS_D(inclin)
    gui.printlnCond("21. Calculate y = sin(L_t - Omega_p) * cos(inclination)")
    gui.printlnCond("                = sin(" + ASTAngle.angleToStr_dec(L_t, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(Omega_p, DECFORMAT) + ") * cos( " + 
                    ASTAngle.angleToStr_dec(inclin, DECFORMAT) + ")")
    gui.printlnCond("                = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y))
    gui.printlnCond()
    
    x = ASTMath.COS_D(L_t - Omega_p)
    gui.printlnCond("22. Calculate x = cos(L_t - Omega_p) = cos(" + ASTAngle.angleToStr_dec(L_t, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(Omega_p, DECFORMAT) + ") = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x))
    gui.printlnCond()
    
    dT = ASTMath.INVTAN_D(y / x)
    gui.printlnCond("23. Compute T = inv tan(y/x) = inv tan(" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,y) + " / " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT,x) + ") = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    x = ASTMath.quadAdjust(y, x)
    gui.printlnCond("24. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
    gui.printCond("    T = T + Adjustment = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + " + " + 
                  ASTAngle.angleToStr_dec(x, DECFORMAT) + " = ")
    dT = dT + x
    gui.printlnCond(ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Lmoon = Omega_p + dT
    gui.printlnCond("25. Calculate the Moon's ecliptic longitude.")
    gui.printlnCond("    Lmoon = Omega_p + T = " + ASTAngle.angleToStr_dec(Omega_p, DECFORMAT) + " + " + 
                    ASTAngle.angleToStr_dec(dT, DECFORMAT) +
                    " = " + ASTAngle.angleToStr_dec(Lmoon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    if (Lmoon > 360.0):
        Lmoon = Lmoon - 360.0
    gui.printlnCond("26. If Lmoon > 360, then subtract 360.")
    gui.printlnCond("    Lmoon = " + ASTAngle.angleToStr_dec(Lmoon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Bmoon = ASTMath.SIN_D(L_t - Omega_p) * ASTMath.SIN_D(inclin)
    Bmoon = ASTMath.INVSIN_D(Bmoon)
    gui.printlnCond("27. Calculate the Moon's ecliptic latitude.")
    gui.printlnCond("    Bmoon = inv sin[sin(L_t - Omega_p) * sin(inclination)]")
    gui.printlnCond("          = inv sin[sin(" + ASTAngle.angleToStr_dec(L_t, DECFORMAT) + " - " +
                    ASTAngle.angleToStr_dec(Omega_p, DECFORMAT) + ") * sin(" + 
                    ASTAngle.angleToStr_dec(inclin, DECFORMAT) + ")]")
    gui.printlnCond("          = " + ASTAngle.angleToStr_dec(Bmoon, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eqCoord = ASTCoord.EclipticToEquatorial(Bmoon, Lmoon, ASTOrbits.getOEEpochDate())
    gui.printlnCond("28. Convert the Moon's ecliptic coordinates (Bmoon, Lmoon) to equatorial coordinates.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(eqCoord.getRAAngle().getDecTime(), DECFORMAT) + 
                    " hours, Decl = " + ASTAngle.angleToStr_dec(eqCoord.getDeclAngle().getDecAngle(), DECFORMAT) + " degrees")
    gui.printlnCond()
    
    horizonCoord = ASTCoord.RADecltoHorizon(eqCoord.getRAAngle().getDecTime(),
                                            eqCoord.getDeclAngle().getDecAngle(),observer.getObsLat(), LST)
    gui.printlnCond("29. Convert the Moon's equatorial coordinates to horizon coordinates.")
    gui.printlnCond("    Alt = " + ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) + 
                    ", Az = " + ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT))
    gui.printlnCond()
    
    result = ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) + " Alt, " +\
             ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT) + " Az"
    gui.printlnCond("Thus, for this observer location and date/time, the Moon")
    gui.printlnCond("is at " + result + ".")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Moon's Location is " + result)



def calcMoonRiseSet(gui):
    """
    Calculate the times of Moonrise and Moonset.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance() 
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()

    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):     
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Calculate Moonrise and Moonset for the Current Observer", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    #  How to find the Sun's true anomaly
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    
    eclCoord = ASTOrbits.calcMoonEclipticCoord(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                               observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, __termCriteria)
    Bmoon1 = eclCoord[ASTOrbits.MOON_ECLLAT]
    Lmoon1 = eclCoord[ASTOrbits.MOON_ECLLON]
    L_t1 = eclCoord[ASTOrbits.MOON_TRUELON]
    Omega_p1 = eclCoord[ASTOrbits.MOON_CORRLON]
    Ca1 = eclCoord[ASTOrbits.MOON_MEANANOM_CORR]
    gui.printlnCond("1.  Calculate the Moon's ecliptic location at midnight for the date in question,")
    gui.printlnCond("    as well as the true ecliptic longitude (L_t1), corrected ecliptic longitude of")
    gui.printlnCond("    the ascending node(Omega_p1), and mean anomaly correction (Ca1)")
    gui.printlnCond("    For UT=0 hours on the date " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),
                                                                              observer.getObsDate().getiDay(),
                                                                              observer.getObsDate().getYear()))
    gui.printlnCond("    The Moon's coordinates are Bmoon1 = " + ASTAngle.angleToStr_dec(Bmoon1, DECFORMAT) +
                    " degrees, Lmoon1 = " + ASTAngle.angleToStr_dec(Lmoon1, DECFORMAT) + " degrees")
    gui.printlnCond("    The Moon's true ecliptic longitude L_t1 = " + ASTAngle.angleToStr_dec(L_t1, DECFORMAT) + " degrees")
    gui.printlnCond("    The corrected longitude of the ascending node Omega_p1 = " + 
                    ASTAngle.angleToStr_dec(Omega_p1, DECFORMAT) + " degrees")
    gui.printlnCond("    The mean anomaly correction Ca1 = " + ASTAngle.angleToStr_dec(Ca1, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eqCoord1 = ASTCoord.EclipticToEquatorial(Bmoon1, Lmoon1,ASTOrbits.getOEEpochDate())
    gui.printlnCond("2.  Convert the Moon's ecliptic coordinates to equatorial coordinates.")
    gui.printlnCond("    RA1 = " + ASTTime.timeToStr_dec(eqCoord1.getRAAngle().getDecTime(), DECFORMAT) + 
                    " hours, Decl1 = " + ASTAngle.angleToStr_dec(eqCoord1.getDeclAngle().getDecAngle(), DECFORMAT) + 
                    " degrees")
    gui.printlnCond()
    
    LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord1.getRAAngle().getDecTime(),
                                       eqCoord1.getDeclAngle().getDecAngle(),observer.getObsLat())
    riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0)
    ST1r = LSTTimes[ASTOrbits.RISE_TIME]
    ST1s = LSTTimes[ASTOrbits.SET_TIME]
    gui.printlnCond("3.  Using the equatorial coordinates from step 2, compute the")
    gui.printlnCond("    LST rising and setting times.")
    if not (riseSet):
        gui.printlnCond("    The Moon does not rise or set for this observer.")
        return
    gui.printlnCond("    ST1r = " + ASTTime.timeToStr_dec(ST1r, DECFORMAT) + " hours")
    gui.printlnCond("    ST1s = " + ASTTime.timeToStr_dec(ST1s, DECFORMAT) + " hours")
    gui.printlnCond()
    
    Bmoon2 = Bmoon1 + 0.05 * ASTMath.COS_D(L_t1 - Omega_p1) * 12.0
    gui.printlnCond("4.  Use L_t1, Omega_p1, and Bmoon1 to compute the Moon's ecliptic latitude 12 hours later.")
    gui.printlnCond("    Bmoon2 = Bmoon1 + 0.05*cos(L_t1 - Omega_p1)*12")
    gui.printlnCond("           = " + ASTAngle.angleToStr_dec(Bmoon1, DECFORMAT) +
                    " + 0.05*cos(" + ASTAngle.angleToStr_dec(L_t1, DECFORMAT) + " - " + 
                    ASTAngle.angleToStr_dec(Omega_p1, DECFORMAT) + ")*12")
    gui.printlnCond("           = " + ASTAngle.angleToStr_dec(Bmoon2, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Lmoon2 = Lmoon1 + (0.55 + 0.06 * ASTMath.COS_D(Ca1)) * 12.0
    gui.printlnCond("5.  Use Ca1 and Lmoon1 to compute the Moon's ecliptic longitude 12 hours later.")
    gui.printlnCond("    Lmoon2 = Lmoon1 + [0.55 + 0.06*cos(Ca1)]*12")
    gui.printlnCond("           = " + ASTAngle.angleToStr_dec(Lmoon1, DECFORMAT) +
                    " + [0.55 + 0.06*cos(" + ASTAngle.angleToStr_dec(Ca1, DECFORMAT) + ")]*12")
    gui.printlnCond("           = " + ASTAngle.angleToStr_dec(Lmoon2, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    if (Lmoon2 > 360.0):
        Lmoon2 = Lmoon2 - 360.0
    gui.printlnCond("6.  If Lmoon2 > 360.0, subtract 360 degrees.")
    gui.printlnCond("    Lmoon2 = " + ASTAngle.angleToStr_dec(Lmoon2, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eqCoord2 = ASTCoord.EclipticToEquatorial(Bmoon2, Lmoon2, ASTOrbits.getOEEpochDate())
    gui.printlnCond("7.  Convert the ecliptic coordinates from the previous step to equatorial coordinates.")
    gui.printlnCond("    RA2 = " + ASTTime.timeToStr_dec(eqCoord2.getRAAngle().getDecTime(), DECFORMAT) + 
                    " hours, Decl2 = " + ASTAngle.angleToStr_dec(eqCoord2.getDeclAngle().getDecAngle(), DECFORMAT) + 
                    " degrees")
    gui.printlnCond()
    
    LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord2.getRAAngle().getDecTime(), 
                                       eqCoord2.getDeclAngle().getDecAngle(), observer.getObsLat())
    riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0)
    ST2r = LSTTimes[ASTOrbits.RISE_TIME]
    ST2s = LSTTimes[ASTOrbits.SET_TIME]
    
    gui.printlnCond("8.  Using the equatorial coordinates from step 7, compute the")
    gui.printlnCond("    second set of LST rising and setting times.")
    if not (riseSet):
        gui.printlnCond("    The Moon does not rise or set for this observer.")
        return
    gui.printlnCond("    ST2r = " + ASTTime.timeToStr_dec(ST2r, DECFORMAT) + " hours")
    gui.printlnCond("    ST2s = " + ASTTime.timeToStr_dec(ST2s, DECFORMAT) + " hours")
    gui.printlnCond()
    
    Tr = (12.03 * ST1r) / (12.03 + ST1r - ST2r)
    gui.printlnCond("9.  Interpolate the two sets of LST rising times.")
    gui.printlnCond("    Tr = (12.03 * ST1r) / (12.03 + ST1r - ST2r)")
    gui.printlnCond("       = (12.03*" + ASTTime.timeToStr_dec(ST1r, DECFORMAT) + ")/(12.03+" + 
                    ASTTime.timeToStr_dec(ST1r, DECFORMAT) +
                    "-" + ASTTime.timeToStr_dec(ST2r, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTTime.timeToStr_dec(Tr, DECFORMAT) + " hours")
    gui.printlnCond()
    
    Ts = (12.03 * ST1s) / (12.03 + ST1s - ST2s)
    gui.printlnCond("10. Interpolate the two sets of LST setting times.")
    gui.printlnCond("    Ts = (12.03 * ST1s) / (12.03 + ST1s - ST2s)")
    gui.printlnCond("       = (12.03*" + ASTTime.timeToStr_dec(ST1s, DECFORMAT) + ")/(12.03+" + 
                    ASTTime.timeToStr_dec(ST1s, DECFORMAT) +
                    "-" + ASTTime.timeToStr_dec(ST2s, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTTime.timeToStr_dec(Ts, DECFORMAT) + " hours")
    gui.printlnCond()
    
    GST = ASTTime.LSTtoGST(Tr, observer.getObsLon())
    UT = ASTTime.GSTtoUT(GST, observer.getObsDate())
    LCTr = ASTTime.UTtoLCT(UT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    crossedDate = (dateAdjust != 0)

    GST = ASTTime.LSTtoGST(Ts, observer.getObsLon())
    UT = ASTTime.GSTtoUT(GST, observer.getObsDate())
    LCTs = ASTTime.UTtoLCT(UT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    crossedDate = crossedDate or (dateAdjust != 0)
    
    gui.printlnCond("11. Convert the LST rising/setting times to their corresponding LCT times.")
    gui.printlnCond("    LCTr = " + ASTTime.timeToStr_dec(LCTr, DECFORMAT) + " hours, LCTs = " +
                    ASTTime.timeToStr_dec(LCTs, DECFORMAT) + " hours")
    if (crossedDate):
        gui.printlnCond("    WARNING: Converting LST to LCT crossed a date boundary, so the results are likely")
        gui.printlnCond("             inaccurate. Try the date before and the date after the given date,")
        gui.printlnCond("             then average the results to get a more accurate estimate.")
    gui.printlnCond()
    
    gui.printlnCond("12. Convert the LCT times to HMS format.")
    gui.printlnCond("    LCTr = " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + ", LCTs = " + 
                    ASTTime.timeToStr_dec(LCTs, HMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),
                                                        observer.getObsDate().getiDay(),
                                                        observer.getObsDate().getYear()) + " for the current observer")
    gui.printlnCond("the Moon will rise at " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + " LCT and set at " +
                    ASTTime.timeToStr_dec(LCTs, HMSFORMAT) + " LCT")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    result = "Moonrise: " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + " LCT, Moonset: " + \
            ASTTime.timeToStr_dec(LCTs, HMSFORMAT) + " LCT"
    if (crossedDate):
        result = result + " (Moonset on day before Moonrise-Try new dates)"
    elif (LCTr > LCTs):
        result = result + " (next day)"
    gui.setResults(result)



def setTerminationCriteria(gui):
    """
    Set the termination criteria for solving Kepler's equation
    
    :param tkwidget gui: GUI object from which the request came
    """
    global __termCriteria
    
    prt = gui.getPrtInstance()
    dTerm = __termCriteria
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    if (ASTQuery.showQueryForm(["Enter Termination Criteria in radians\n(ex: 0.000002)"]) != ASTQuery.QUERY_OK):
        prt.println("Termination criteria was not changed from " + str(__termCriteria) + " radians")
        return

    # Validate the termination criteria
    rTmpObj = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmpObj.isValidRealObj()):
        dTerm = rTmpObj.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
        prt.println("Termination criteria was not changed from " + str(__termCriteria) + " radians")
        return

    prt.println("Prior termination criteria was: " + str(__termCriteria) + " radians")
    if (dTerm < ASTKepler.KeplerMinCriteria):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too small, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    if (dTerm > 1):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too large, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    __termCriteria = dTerm
    
    prt.println("Termination criteria is now set to " + str(__termCriteria) + " radians")
    prt.resetCursor()



#==============================================================
# Some common helper routines used only in this module
#==============================================================

def __ageToStr(Age):
    """
    Convert the Moon's age into a printable string
    
    :param float Age: Moon's age in degrees
    :return: New Moon, First Quarter, etc. as a string
    """
    if (Age < 45 - 0.5 * 45):
        return "New"
    elif (Age < 90 - 0.5 * (90 - 45)):
        return "Waxing Crescent"
    elif (Age < 135 - 0.5 * (135 - 90)):
        return "First Quarter"
    elif (Age < 180 - 0.5 * (180 - 135)):
        return "Waxing Gibbous"
    elif (Age < 225 - 0.5 * (225 - 180)):
        return "Full"
    elif (Age < 270 - 0.5 * (270 - 225)):
        return "Waning Gibbous"
    elif (Age < 315 - 0.5 * (315 - 270)):
        return "Last Quarter"
    elif (Age < 360 - 0.5 * (360 - 315)):
        return "Waning Crescent"
    else:
        return "New"



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
