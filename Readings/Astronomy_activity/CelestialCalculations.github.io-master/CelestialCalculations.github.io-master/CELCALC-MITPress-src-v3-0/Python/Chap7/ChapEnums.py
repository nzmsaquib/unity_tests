"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what menu item to do."""
    # Lunar Info Menu
    MOON_LOCATION = auto()               # Calculate the Moon's location
    MOON_RISE_SET = auto()               # Calculate Moonrise/Moonset
    MOON_DIST_AND_ANG_DIAMETER = auto()  # Calculate Solar distance and angular diameter
    MOON_PHASES_AND_AGE = auto()         # Calculate Moon's phases and age 
    MOON_PERCENT_ILLUMINATED = auto()    # Calculate how much of the Moon is illuminated
    TERM_CRITERIA = auto()               # Set a termination criteria for solving Kepler's equation
    
    # Orbital Elements Menu
    LOAD_ORBITAL_ELEMENTS = auto()       # Load Moon's orbital elements from a disk file
    SHOW_ORBITAL_ELEMENTS = auto()       # Display the Moon's orbital elements
     


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
