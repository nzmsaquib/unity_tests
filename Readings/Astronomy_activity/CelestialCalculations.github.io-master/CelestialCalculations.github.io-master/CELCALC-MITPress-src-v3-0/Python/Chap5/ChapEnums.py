"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what menu item to do."""   
    # Coordinate Systems
    EQ_TO_HORIZ = auto()                # Convert Equatorial to Horizon Coordinates
    HORIZ_TO_EQ = auto()                # Convert Horizon to Equatorial Coordinates
    CALC_PRECESSION = auto()            # Calculate a precession correction
    STAR_RISE_SET = auto()              # Calculate star rise/set times
    
    # Star Catalogs
    LOAD_CATALOG = auto()               # Load a star catalog
    SHOW_CATALOG_INFO = auto()          # Show catalog header information
    LIST_ALL_OBJS_IN_CAT = auto()       # List all objects in the catalog
    LIST_ALL_CONST = auto()             # List all constellations
    LIST_ALL_OBJS_IN_CONST = auto()     # List all objects in a constellation
    FIND_CONST_OBJ_IN=auto()            # Find constellation an object is in
    
    # Star Charts
    SET_mV_FILTER = auto()              # Set a visual magnitude filter
    DRAW_ALL_STARS = auto()             # Draw all stars in the catalog
    DRAW_ALL_STARS_IN_CONST = auto()    # Draw all stars in a constellation
    DRAW_ALL_CONST = auto()             # Draw brightest star in each constellation
    FIND_OBJ = auto()                   # Locate an object in the catalog
    FIND_BRIGHTEST_OBJ = auto()         # Locate the brightest object in the catalog
    FIND_EQ_LOC = auto()                # Locate an equatorial coordinate location
    FIND_HORIZ_LOC = auto()             # Locate a horizon coordinate location



#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
