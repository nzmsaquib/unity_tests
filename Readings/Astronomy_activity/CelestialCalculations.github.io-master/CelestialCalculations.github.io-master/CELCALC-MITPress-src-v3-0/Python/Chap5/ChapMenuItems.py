"""
Create menu entries for this chapter's Coordinate System Conversions,
Star Catalogs, and Star Charts menus. This module also creates a
class and structure that associates data items with menu
entries to make it easier to determine what actions to 
take when a menu item is selected.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk

import ASTUtils.ASTStyle as ASTStyle

import Chap5.ChapEnums
import Chap5.MenusListener as ml

# chapMenuItems is a dynamically constructed list of this chapter's menu items. We could
# use a tuple (which is immutable) to save some processing time, but we'll construct
# the list as we create the individual menu items because we want to add separators
# between some of the menu items.
__chapMenuItems = []    
    


#====================================================================================
# Define a class for what a menu item looks like. The only extra thing we need to
# save is what menu action to do.
#====================================================================================

class ChapMenuItem():
    """Defines a class for menu items"""
    #=================================================================
    # Class instance variables
    #=================================================================
    # eCalcType           what menu item is to be done
    
    def __init__(self,eCalcType):
        """
        Creates a ChapMenuItem instance
        
        :param CalculationType eCalcType: what menu item is to be done
        """
        self.eCalcType = eCalcType
        
    # define 'getter' methods to return the various fields for a menu item
    def getCalcType(self):
        return self.eCalcType
    


#==============================================================
# Define this module's functions
#==============================================================

def __createCalcType(gui,menu,title,eCalcType):
    """
    Create an individual menu item for the current cascade
    
    :param tkwidget gui: GUI object with which menu items are associated
    :param menuwidget menu: cascade menu to which this item is to be added
    :param str title: text for this menu item
    :param CalculationType eCalcType: what type of conversion is to be done
    :return: the menu object that got created
    """
    # define a listener for responding to clicks on a menu item 
    menuItemCallback = lambda: ml.menuListener(__chapMenuItems[idx].eCalcType,gui)
        
    menuObj = ChapMenuItem(eCalcType)
    # Add the menu data to the convMenuItems list and then to the menu cascade
    __chapMenuItems.append(menuObj)
    idx = len(__chapMenuItems) - 1                  # which menu item this is
    menu.add_command(label=title,command=menuItemCallback) 
    return menuObj



def createChapMenuItems(gui,menubar):
    """
    Create menu items and add them to the menubar.
    
    :param tkwidget gui: GUI object that will hold the menu items
    :param menuwidget menubar: menubar to which the menu items are to be added
    """
    cen = Chap5.ChapEnums.CalculationType               # shorten to make typing easier!!!
    
    # Create Coord Systems menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Coord Systems ",menu=tmpmenu)  
    __createCalcType(gui,tmpmenu, "Equatorial to Horizon", cen.EQ_TO_HORIZ)
    __createCalcType(gui,tmpmenu, "Horizon to Equatorial", cen.HORIZ_TO_EQ)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu, "Precession Correction", cen.CALC_PRECESSION)
    tmpmenu.add_separator()
    __createCalcType(gui,tmpmenu, "Star Rise/Set Times", cen.STAR_RISE_SET)
    
    # Create Star Catalogs menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Star Catalogs ",menu=tmpmenu)      
    __createCalcType(gui,tmpmenu, "Load a Star Catalog", cen.LOAD_CATALOG)
    __createCalcType(gui,tmpmenu, "Show Catalog Information", cen.SHOW_CATALOG_INFO)   
    tmpmenu.add_separator()    
    __createCalcType(gui,tmpmenu, "List all Objects in the Catalog", cen.LIST_ALL_OBJS_IN_CAT)     
    tmpmenu.add_separator()   
    __createCalcType(gui,tmpmenu, "List all Constellations", cen.LIST_ALL_CONST)   
    __createCalcType(gui,tmpmenu, "List all Objects in a Constellation", cen.LIST_ALL_OBJS_IN_CONST)   
    __createCalcType(gui,tmpmenu, "Find Constellation an Object is In", cen.FIND_CONST_OBJ_IN) 
    
    # Create Star Charts menu
    tmpmenu = tk.Menu(menubar,tearoff=0,font=ASTStyle.MENU_FONT)
    menubar.add_cascade(label=" Star Charts ",menu=tmpmenu)
    __createCalcType(gui,tmpmenu, "Set mV Filter", cen.SET_mV_FILTER)
    tmpmenu.add_separator()  
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Draw ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"All Stars in the Catalog",cen.DRAW_ALL_STARS)
    __createCalcType(gui,tmpbar,"All Stars in a Constellation",cen.DRAW_ALL_STARS_IN_CONST)
    __createCalcType(gui,tmpbar,"Brightest Star in each Constellation",cen.DRAW_ALL_CONST)   
    tmpmenu.add_separator()
    tmpbar = tk.Menu(tmpmenu,tearoff=0,font=ASTStyle.MENU_FONT)
    tmpmenu.add_cascade(label="Locate ...",menu=tmpbar)
    __createCalcType(gui,tmpbar,"An Object in the Catalog",cen.FIND_OBJ)
    __createCalcType(gui,tmpbar,"Brightest Object in the Catalog",cen.FIND_BRIGHTEST_OBJ)
    tmpbar.add_separator()
    __createCalcType(gui,tmpbar,"An Equatorial Coordinate",cen.FIND_EQ_LOC)
    __createCalcType(gui,tmpbar,"A Horizon Coordinate",cen.FIND_HORIZ_LOC)     
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
