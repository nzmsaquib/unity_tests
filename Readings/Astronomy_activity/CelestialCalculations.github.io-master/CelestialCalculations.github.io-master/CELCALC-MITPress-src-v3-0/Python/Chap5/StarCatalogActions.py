"""
Handles the Star Catalogs menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCatalog as ASTCatalog
import ASTUtils.ASTConstellation as ASTConstellation
import ASTUtils.ASTFileIO as ASTFileIO
from ASTUtils.ASTMisc import DEFAULT_EPOCH, HIDE_ERRORS, HMSFORMAT, DMSFORMAT, ASCENDING_ORDER
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

def loadCatalog(gui):
    """
    Loads a star catalog from disk.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
        
    if (ASTCatalog.isCatalogLoaded()):
        if (ASTMsg.pleaseConfirm("A catalog is already loaded. Are you\n" +
                                 "sure you want to load a new one??","Clear Catalog Data")):
            ASTCatalog.clearCatalogAndSpaceObjects()
            gui.setFilename("")
            gui.setCatalogType("")
            gui.setEpoch(DEFAULT_EPOCH)
            prt.println("The currently loaded star catalog was cleared ...")
        else:
            prt.println("The currently loaded star catalog was not cleared ...")
            return

    gui.clearTextAreas()

    fileToRead = ASTFileIO.getFileToRead("Select Catalog to Load ...",ASTCatalog.ASTCatalog.getFileExtFilter(),
                                     ASTCatalog.ASTCatalog.getCatDataDir(),"")
    
    if ((fileToRead == None) or (len(fileToRead) <= 0)):
        return
    
    gui.setFilename(fileToRead)
    
    if ((ASTFileIO.getFileSize(fileToRead)/1024.0) > ASTCatalog.MAX_CATALOG_FILESIZE_KB):
        ASTMsg.infoMsg("Warning: Due to its size, it may take several seconds to load\n" +
                       "this Star Catalog. Click 'OK' and then please be patient.\n" +
                       "A message will be displayed when loading is completed.","")
    
    if (ASTCatalog.loadFormattedStarCatalog(fileToRead)):
        s = ASTStr.strFormat("Read in %d different Constellations with a total of ",ASTCatalog.getCatNumConst()) +\
            ASTStr.strFormat("%d Objects",ASTCatalog.getCatNumObjs())
        prt.println(s)
        gui.setCatalogType(ASTCatalog.getCatType())
        gui.setEpoch(ASTCatalog.getCatEpoch())
    else:          
        ASTMsg.errMsg("Could not load the catalog data from "+fileToRead, "Catalog Load Failed")
        gui.setFilename("")
        gui.setCatalogType("")
        gui.setEpoch(DEFAULT_EPOCH)
    
    prt.resetCursor()



def showCatalogInfo(gui):
    """
    Shows the catalog information from the currently loaded catalog.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    
    if not (ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded")
    else:
        gui.clearTextAreas()
        ASTCatalog.displayCatalogInfo()
        prt.resetCursor()



def listAllObjsInCatalog(gui):
    """
    Shows all catalog information, including space objects,
    in the currently loaded catalog.

    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    currentmVFilter = gui.getcurrentmVFilter()   
     
    if (not ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded")
        return
    
    gui.clearTextAreas()
    ASTCatalog.displayCatalogInfo()
    prt.println()
    prt.setBoldFont(True)
    prt.println("Only Those Objects at Least as Bright as mV = " + \
                ASTStr.strFormat(ASTStyle.mVFORMAT,currentmVFilter) + " are listed", CENTERTXT)
    prt.setBoldFont(False)
    prt.setFixedWidthFont()
    prt.println("*"*80)    
    ASTCatalog.displayAllCatalogObjects(currentmVFilter)        
    prt.setProportionalFont()
    prt.resetCursor()



def listAllConstellations(gui):
    """
    Displays a list of all of the constellations.
    
    :param tkwidget gui: GUI object from which the request came
    """ 
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    prt.setFixedWidthFont()
    ASTConstellation.displayAllConstellations()
    prt.setProportionalFont()
    prt.resetCursor()



def listAllObjsInConstellation(gui):
    """
    Shows all space objects in the currently loaded catalog
    that fall within a user-specified constellation. The
    catalog must already be grouped by constellation.

    :param tkwidget gui: GUI object from which the request came
    """ 
    prt = gui.getPrtInstance()
    currentmVFilter = gui.getcurrentmVFilter()
    
    if not (ASTCatalog.isCatalogLoaded()):
        ASTMsg.errMsg("No catalog is currently loaded.", "No Catalog Loaded")
        return
    
    if (ASTQuery.showQueryForm(["Enter Constellation's 3 Character\nAbbreviated Name"]) != ASTQuery.QUERY_OK):
        return
    
    gui.clearTextAreas()
    constAbbrevName = ASTQuery.getData(1)
    if ((constAbbrevName == None) or (len(constAbbrevName) <= 0)):
        return
    idx = ASTConstellation.findConstellationByAbbrvName(constAbbrevName.strip())
    if (idx < 0):
        prt.println("No Constellation whose abbreviated name is '" + constAbbrevName + "' was found")
    else:
        prt.setBoldFont(True)
        prt.println("Only Those Objects at Least as Bright as mV = " +
                    ASTStr.strFormat(ASTStyle.mVFORMAT,currentmVFilter) + " are listed", CENTERTXT)
        prt.setBoldFont(False)
        prt.setFixedWidthFont()
        ASTCatalog.displayAllObjsByConstellation(idx,ASCENDING_ORDER,currentmVFilter)        
        prt.setProportionalFont()

    prt.resetCursor()



def findConstellationForRA_Decl(gui):
    """
    Finds the constellation that a given RA/Decl falls within

    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()   

    # Get RA & Decl
    if (ASTQuery.showQueryForm(["Enter Right Ascension (hh:mm:ss.ss) for Epoch 2000.0",
                                "Enter Declination (xxxd yym zz.zzs) for Epoch 2000.0"]) != ASTQuery.QUERY_OK):
        return
    
    # validate data  
    strTmp=ASTQuery.getData(1) # RA
    if ((strTmp == None) or (len(strTmp) <= 0)):
        return
    raObj = ASTTime.isValidTime(strTmp,HIDE_ERRORS)
    if not (raObj.isValidTimeObj()):
        ASTMsg.errMsg("The RA entered is invalid - try again.", "Invalid RA")
        return
    
    strTmp=ASTQuery.getData(2) # Decl
    if ((strTmp == None) or (len(strTmp) <= 0)):
        return
    declObj = ASTAngle.isValidAngle(strTmp,HIDE_ERRORS)
    if not (declObj.isValidAngleObj()):
        ASTMsg.errMsg("The Declination entered is invalid - try again.", "Invalid Decl")
        return

    gui.clearTextAreas()
    
    idx = ASTConstellation.findConstellationFromCoord(raObj.getDecTime(),declObj.getDecAngle(),DEFAULT_EPOCH)
    
    if (idx < 0):
        ASTMsg.criticalErrMsg("Could not determine a constellation for the data entered.")
    else:
        result = "Location " + ASTTime.timeToStr_obj(raObj, HMSFORMAT) + " RA, " + \
                 ASTAngle.angleToStr_obj(declObj, DMSFORMAT) + " Decl is in the " + \
                 ASTConstellation.getConstName(idx) + " (" + ASTConstellation.getConstAbbrevName(idx) + ") constellation"
        gui.setResults(result)   

    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass   

