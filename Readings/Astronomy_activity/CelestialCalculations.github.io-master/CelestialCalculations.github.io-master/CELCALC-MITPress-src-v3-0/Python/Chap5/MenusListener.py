"""
Implements action listeners for the menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""
import sys

import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT

import Chap5.ChapEnums
import Chap5.CoordSystemActions as cs_actions
import Chap5.StarCatalogActions as starcat_actions
import Chap5.StarChartActions as starchart_actions

#==================================================
# Define listeners for about, exit, instructions
#==================================================

def aboutMenuListener(gui):
    """
    Handle a click on the About menu item
    
    :param tkwidget gui: GUI that the About Box is associated with
    """
    gui.showAboutBox()
    


def exitMenuListener():
    """Handle a click on the Exit menu item"""
    if ASTMsg.pleaseConfirm("Are you sure you want to exit?"," "):
        sys.exit()    
    


def instructionsMenuListener(gui):
    """
    Handle a click on the Instructions menu item
    
    :param tkwidget gui: GUI object to which menu items are associated
    """
    prt = gui.getPrtInstance()
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    prt.println("Instructions",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
            "be used in subsequent calculations. (Note that the observer location and time are irrelevant for creating " +
            "star charts of equatorial coordinates because equatorial coordinates are independent of an " +
            "observer's location.) You may find it convenient to enter a default latitude, longitude, and time zone " +
            "in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
            "longitude, and time zone are already filled in with default values when the program starts. By default, " +
            "the date and time will be set to the local date and time at which the program is started.")
    prt.println()
    prt.printnoln("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
            "various calculations are performed. This checkbox only applies to the 'Coord Systems' menu. Select the " +
            "item you want from the 'Coord Systems' menu, enter the required data, and the results will be displayed " +
            "immediately underneath the 'File' label and, if intermediate results are desired, in the scrollable text " +
            "output area (i.e., where these instructions are being displayed). Note that the 'Coord Systems' menu " +
            "provides an entry to adjust an equatorial coordinate for precession. This entry makes a precession " +
            "correction for ")
    prt.setBoldFont(True)
    prt.printnoln("**only**")
    prt.setBoldFont(False)
    prt.println(" the RA/Decl entered. It does not apply a precession correction to the coordinates in " +
            "a star catalog or to the coordinates when a star chart is plotted.")
    prt.println()
    prt.println("The 'Star Catalogs' menu allows you to load a star catalog as was done in the program for " +
            "Chapter 1. You may list the objects in the most recently loaded catalog as well as view basic information " +
            "about the catalog and the constellations. The catalog objects that will be listed are limited to those " +
            "objects that are at least as bright as the magnitude indicated by the 'mV Filter' data field near " +
            "the top right of the application window. For example, if the mV filter is set to 6.5, then " +
            "objects dimmer than magnitude 6.5 will be excluded from a catalog listing. Similarly, when " +
            "plotting catalog objects, those objects that are dimmer than the mV filter will be " +
            "excluded from the star chart that gets created. If a star catalog does not provide the visual " +
            "magnitude for objects in the catalog, the mV filter will be ignored.")
    prt.println()
    prt.println("The first menu entry under the 'Star Charts' menu allows the mV filter to be set. The remaining items in " +
            "this menu allow star charts to be created and displayed. The 'Equatorial Coords Charts' checkbox determines " +
            "what type of star chart to create. If checked, the star chart will be a rectangular plot of right " +
            "ascension and declination (i.e., equatorial coordinates) while if the checkbox " +
            "is not checked, a circular plot of horizon coordinates will be generated. Resizing the application window will " +
            "destroy any star chart that has been created. If this happens, after resizing the window " +
            "to the size you want, simply go through the menu items to redraw the star chart that you want. It is " +
            "typically better to resize the application window to the size you want before generating a star chart.")
    prt.println()
    prt.println("The 'White Bkg for Charts' checkbox allows you to produce charts with a white background (checkbox is checked) " +
            "or a black background (checkbox is not checked). The remaining checkbox, ('Label Stars') indicates whether " +
            "the name of an object should be displayed on the star charts. The menu items under 'Star Charts' provide the " +
            "ability to plot all of the objects in the currently loaded star catalog, only those in a particular " +
            "constellation, or just the brightest star in a constellation. These are provided as aids to help you find " +
            "a constellatiion or to locate an object in a constellation.")
    prt.println()
    prt.println("The 'StarCharts->Locate ...->An Equatorial Coordinate' is particularly useful for finding an object on a " +
            "star chart, even if the currently loaded star catalog does not contain the object. For example, if you know " +
            "the equatorial coordinates for the planet Saturn (perhaps from an astronomy magazine), you can first draw a " +
            "star chart from the currently loaded catalog, then select 'StarCharts->Locate ...->An Equatorial Coordinate' " +
            "to enter Saturn's equatorial coordinates and see where the planet is in relation to the other objects in the " +
            "star chart.")
    prt.println()
    prt.println("Two cautions are worth noting regarding displaying the names of stars on a star chart. First," +
            "the star's name will appear below where the star is plotted. If a star is near one of the chart's sides," +
            "the label may be only partially displayed. Second, you should plot star labels sparingly because plotting " +
            "too many labels will clutter the star chart. For example, check the 'Label Stars' checkbox and then select the " +
            "'Star Charts->Draw ...->Brightest Star in Each Constellation' menu entry to see how cluttered a chart can become!")
    prt.println()
    
    prt.setBoldFont(True)
    prt.println("***Important Note about the Star Charts***",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    prt.printnoln("Python's tkinter graphics library does not support clipping to a region such as a rectangle or a "+
                  "circle. Consequently, portions of objects and text plotted on the display may appear outside " +
                  "the plotting rectangle for an Equatorial Chart and outside the plotting circle for a Horizon Chart.")
    
    prt.resetCursor()
   


#==================================================
# Define listener for the menu items
#==================================================

def menuListener(calcToDo,gui):
    """
    Handle a click on the menu items
    
    :param CalulationType calcToDo: the calculation to perform
    :param tkwidget gui: GUI object to which menu items are associated
    """
    cen = Chap5.ChapEnums.CalculationType           # shorten to make typing easier!!!
    
    #**************** Coord Systems Menu
    if (calcToDo == cen.EQ_TO_HORIZ):
        cs_actions.calcEQ2Horiz(gui)
    elif (calcToDo == cen.HORIZ_TO_EQ):
        cs_actions.calcHoriz2EQ(gui)
    elif (calcToDo == cen.CALC_PRECESSION):
        cs_actions.calcPrecession(gui)
    elif (calcToDo == cen.STAR_RISE_SET):
        cs_actions.calcStarRiseSet(gui)

    #**************** Star Catalogs menu
    elif (calcToDo == cen.LOAD_CATALOG):
        starcat_actions.loadCatalog(gui)
    elif (calcToDo == cen.SHOW_CATALOG_INFO):
        starcat_actions.showCatalogInfo(gui)
    elif (calcToDo == cen.LIST_ALL_OBJS_IN_CAT):
        starcat_actions.listAllObjsInCatalog(gui)
    elif (calcToDo == cen.LIST_ALL_CONST):
        starcat_actions.listAllConstellations(gui)
    elif (calcToDo == cen.LIST_ALL_OBJS_IN_CONST):
        starcat_actions.listAllObjsInConstellation(gui)
    elif (calcToDo == cen.FIND_CONST_OBJ_IN):
        starcat_actions.findConstellationForRA_Decl(gui)

    #**************** Star Charts menu
    elif (calcToDo == cen.SET_mV_FILTER):
        starchart_actions.changemVFilter(gui)
    elif (calcToDo == cen.DRAW_ALL_STARS):
        starchart_actions.drawAllStars(gui)
    elif (calcToDo == cen.DRAW_ALL_STARS_IN_CONST):
        starchart_actions.drawAllStarsInConst(gui)
    elif (calcToDo == cen.DRAW_ALL_CONST):
        starchart_actions.drawBrightestStarInAllConst(gui)
    elif (calcToDo == cen.FIND_OBJ):
        starchart_actions.locateStarCatalogObj(gui)
    elif (calcToDo == cen.FIND_BRIGHTEST_OBJ):
        starchart_actions.locateBrightestObjInCat(gui)
    elif (calcToDo == cen.FIND_EQ_LOC):
        starchart_actions.locateRADecl(gui)
    elif (calcToDo == cen.FIND_HORIZ_LOC):
        starchart_actions.locateAltAz(gui)

    else:           # This should never happen
        ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
                      "from the 'Coord Systems,' 'Star Catalogs,' or 'Star Charts' menu\nand try again.",\
                      "No Menu Item Selected")



#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
