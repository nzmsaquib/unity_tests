"""
Handles the Solar Info menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTCoord as ASTCoord
import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import DECFORMAT,HMSFORMAT,DMSFORMAT,HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTOrbits as ASTOrbits
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

# Set a default termination criteria (in radians) for solving Kepler's equation
__termCriteria = 0.000002



def calcDistAndAngDiameter(gui):
    """
    Calculate the Sun's distance and angular diameter.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):
        return
    
    # Get Sun index after validating that orbital elements are loaded
    idxSun = ASTOrbits.getOEDBSunIndex()
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Sun's Distance and Angular Diameter for the Current Date", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()

    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    Ecc = ASTOrbits.getOEObjEccentricity(idxSun)
    
    # Technically, we should be using the UT for the observer rather than
    # UT=0, but the difference is so small that it isn't worth converting LCT to UT
    eclCoord = ASTOrbits.calcSunEclipticCoord(observer.getObsDate().getMonth(),observer.getObsDate().getiDay(),\
                                              observer.getObsDate().getYear(),0.0,solveTrueAnomaly, __termCriteria)
    Vsun = eclCoord[ASTOrbits.SUN_TRUEANOM]
    gui.printlnCond("1.  Compute the Sun's true anomaly at 0 hours UT.")
    gui.printlnCond("    Vsun = " + ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + " degrees")
    gui.printlnCond()

    dF = (1 + Ecc * ASTMath.COS_D(Vsun)) / (1 - Ecc * Ecc)
    gui.printlnCond("2.  Compute F = [1 + eccentricity*cos(Vsun)] / [1-eccentricity^2]")
    gui.printlnCond("              = [1 + " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, Ecc) + "*cos(" +
                    ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + ")] / [1 - " +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, Ecc) + "^2]")
    gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dF))
    gui.printlnCond()
    
    dDist = ASTOrbits.getOEObjSemiMajAxisKM(idxSun) / dF
    gui.printlnCond("3.  Compute the distance to the Sun.")
    gui.printlnCond("    Dist = a0/F where a0 is the length of the Sun's semi-major axis in km")
    gui.printlnCond("    Dist = (" + ASTStr.insertCommas(ASTOrbits.getOEObjSemiMajAxisKM(idxSun)) + ")/" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dF))
    gui.printlnCond("         = " + ASTStr.insertCommas(dDist) + " km")
    gui.printlnCond()
    
    dAngDiameter = ASTOrbits.getOEObjAngDiamDeg(idxSun) * dF
    gui.printlnCond("4.  Compute the Sun's angular diameter.")
    gui.printlnCond("    Theta_sun = Theta_0*F where Theta_0 is the Sun's angular diameter in degrees")
    gui.printlnCond("    when the Sun is distance a0 away.")
    gui.printlnCond("    Theta_sun = " + ASTAngle.angleToStr_dec(ASTOrbits.getOEObjAngDiamDeg(idxSun), DECFORMAT) +
                    "*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dF) + " = " +
                    ASTAngle.angleToStr_dec(dAngDiameter, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dDist = ASTMath.Round(dDist, 2)
    gui.printlnCond("5.  Convert distance to miles and angular diameter to DMS format.")
    gui.printlnCond("    Dist = " + ASTStr.insertCommas(dDist) + " km = " +
                    ASTStr.insertCommas(ASTMath.KM2Miles(dDist), 2) + " miles")
    gui.printlnCond("    Theta_sun = " + ASTAngle.angleToStr_dec(dAngDiameter, DMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("On " + ASTDate.dateToStr_obj(observer.getObsDate()) + ", the Sun is/was " +
                    ASTStr.insertCommas(dDist) + " km away")
    gui.printlnCond("and its angular diameter is/was " + ASTAngle.angleToStr_dec(dAngDiameter, DMSFORMAT))
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("Sun's Distance: " + ASTStr.insertCommas(dDist) + " km (" +
                   ASTStr.insertCommas(ASTMath.KM2Miles(dDist), 2) + " miles), Angular Diameter: " +
                   ASTAngle.angleToStr_dec(dAngDiameter, DMSFORMAT))



def calcEqOfTime(gui):
    """
    Calculate the equation of time.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):
        return
    
    # Get Sun index after validating that orbital elements are loaded
    idxSun = ASTOrbits.getOEDBSunIndex()
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Equation of Time for " + ASTDate.dateToStr_obj(observer.getObsDate()),CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    Ecc = ASTOrbits.getOEObjEccentricity(idxSun)
    
    eclCoord = ASTOrbits.calcSunEclipticCoord(observer.getObsDate().getMonth(),observer.getObsDate().getiDay(),
                                              observer.getObsDate().getYear(),0.0,solveTrueAnomaly,__termCriteria)
    Lsun = eclCoord[ASTOrbits.SUN_ECLLON]
    Msun = eclCoord[ASTOrbits.SUN_MEANANOM]
    gui.printlnCond("1.  Compute the Sun's ecliptic longitude and mean anomaly for the given date.")
    gui.printlnCond("    Lsun = " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " degrees")
    gui.printlnCond("    Msun = " + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dE = ASTCoord.calcEclipticObliquity(observer.getObsDate().getYear())
    gui.printlnCond("2.  Compute the obliquity of the ecliptic for the given year.")
    gui.printlnCond("    obliq = " + ASTAngle.angleToStr_dec(dE, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    y = ASTMath.TAN_D(dE / 2.0)
    y = y * y
    gui.printlnCond("3.  Compute y = tan^2(obliq/2) = tan^2(" + ASTAngle.angleToStr_dec(dE, DECFORMAT) + "/2")
    gui.printlnCond("              = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, y))
    gui.printlnCond()
    
    dT = y * ASTMath.SIN_D(2 * Lsun) - 2 * Ecc * ASTMath.SIN_D(Msun) +\
         4 * Ecc * y * ASTMath.SIN_D(Msun) * ASTMath.COS_D(2 * Lsun)
    dT = dT - (y * y / 2.0) * ASTMath.SIN_D(4 * Lsun) - ((5 * Ecc * Ecc) / 4) * ASTMath.SIN_D(2 * Msun)
    gui.printlnCond("4.  Calculate a delta T correction in radians from the interpolation formula")
    gui.printlnCond("    dT = y*sin(2*Lsun) - 2*eccentricity*sin(Msun) + ")
    gui.printlnCond("         4*eccentricity*y*sin(Msun)*cos(2*Lsun) -")
    gui.printlnCond("         (y^2/2.0)*sin(4*Lsun) - (5/4)*(eccentricity^2)*sin(2*Msun)")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, y) + "*sin(2*" +
                    ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + ") - 2*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, Ecc) +
                    "*sin(" + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + ") + ")
    gui.printlnCond("         4*" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, Ecc) + "*" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, y) + "sin(" + ASTAngle.angleToStr_dec(Msun, DECFORMAT) +
                    ")*cos(2*" + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + ") - ")
    gui.printlnCond("         (" + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, y) + "^2/2.0)*sin(4*" +
                    ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + ") - (5/4)*(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, Ecc) + "^2)*sin(2*" +
                    ASTAngle.angleToStr_dec(Msun, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + " radians")
    gui.printlnCond()
     
    dT = -dT
    gui.printlnCond("5.  Multiply by -1.")
    gui.printlnCond("    dT = -1.0*dT = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + " radians")
    gui.printlnCond()
    
    dT = ASTMath.rad2deg(dT)
    gui.printlnCond("6.  Convert dT from radians to degrees.")
    gui.printlnCond("    dT = (180/math.pi)*dT = " + ASTAngle.angleToStr_dec(dT, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    dT = dT / 15.0
    gui.printlnCond("7.  Convert dT from degrees to hours by dividing by 15.")
    gui.printlnCond("    dT = " + ASTTime.timeToStr_dec(dT, DECFORMAT) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("8.  Convert dT to HMS format.")
    gui.printlnCond("    dT = " + ASTTime.timeToStr_dec(dT, HMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("For the given date, the equation of time correction is " + ASTTime.timeToStr_dec(dT,HMSFORMAT))
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("For " + ASTDate.dateToStr_obj(observer.getObsDate()) + ", the equation of time is " +
                   ASTTime.timeToStr_dec(dT, HMSFORMAT))



def calcEquinoxesSolstices(gui):
    """
    Calculate the times of the equinoxes and solstices.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Equinoxes and Solstices for the Year", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    dY = observer.getObsDate().getYear()
    dT = dY / 1000.0
    dT2 = dT * dT
    dT3 = dT2 * dT
    gui.printlnCond("1.  Calculate T = Year/1000 = " + str(dY) + "/1000 = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT))
    gui.printlnCond()
    
    JDm = 1721139.2855 + 365.2421376 * dY + 0.067919 * dT2 - 0.0027879 * dT3
    JDj = 1721233.2486 + 365.2417284 * dY - 0.053018 * dT2 + 0.009332 * dT3
    JDs = 1721325.6978 + 365.2425055 * dY - 0.126689 * dT2 + 0.0019401 * dT3
    JDd = 1721414.392 + 365.2428898 * dY - 0.010965 * dT2 - 0.0084885 * dT3
    gui.printlnCond("2.  Compute the Julian day numbers for the equinoxes and solstices.")
    gui.printlnCond("    JDm = 1721139.2855 + 365.2421376*Year + 0.067919*T^2 - 0.0027879*T^3")
    gui.printlnCond("        = 1721139.2855 + 365.2421376*" + str(dY) + " + 0.067919*(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^2 - 0.0027879*(" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^3")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.JDFormat, JDm))
    gui.printlnCond("    JDj = 1721233.2486 + 365.2417284*Year - 0.053018*T^2 + 0.009332*T^3")
    gui.printlnCond("        = 1721233.2486 + 365.2417284*" + str(dY) + " - 0.053018*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^2 + 0.009332*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^3")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.JDFormat, JDj))
    gui.printlnCond("    JDs = 1721325.6978 + 365.2425055*Year - 0.126689*T^2 + 0.0019401*T^3")
    gui.printlnCond("        = 1721325.6978 + 365.2425055*" + str(dY) + " - 0.126689*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^2 + 0.0019401*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^3")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.JDFormat, JDs))
    gui.printlnCond("    JDd = 1721414.392 + 365.2428898*Year - 0.010965*T^2 - 0.0084885*T^3")
    gui.printlnCond("        = 1721414.392 + 365.2428898*" + str(dY) + " - 0.010965*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^2 - 0.0084885*(" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, dT) + ")^3")
    gui.printlnCond("        = " + ASTStr.strFormat(ASTStyle.JDFormat, JDd))
    gui.printlnCond()
    
    marchDate = ASTDate.JDtoDate(JDm)
    juneDate = ASTDate.JDtoDate(JDj)
    septDate = ASTDate.JDtoDate(JDs)
    decDate = ASTDate.JDtoDate(JDd)
    gui.printlnCond("3.  Convert the Julian day numbers from the previous step to calendar dates.")
    gui.printlnCond("    JDm gives " + str(marchDate.getMonth()) + "/" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, marchDate.getdDay()) + "/" + str(marchDate.getYear()))
    gui.printlnCond("    JDj gives " + str(juneDate.getMonth()) + "/" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, juneDate.getdDay()) + "/" + str(juneDate.getYear()))
    gui.printlnCond("    JDs gives " + str(septDate.getMonth()) + "/" +
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, septDate.getdDay()) + "/" + str(septDate.getYear()))
    gui.printlnCond("    JDd gives " + str(decDate.getMonth()) + "/" + 
                    ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, decDate.getdDay()) + "/" + str(decDate.getYear()))
    gui.printlnCond()
    
    UTm = ASTMath.Frac(marchDate.getdDay()) * 24.0
    UTj = ASTMath.Frac(juneDate.getdDay()) * 24.0
    UTs = ASTMath.Frac(septDate.getdDay()) * 24.0
    UTd = ASTMath.Frac(decDate.getdDay()) * 24.0
    gui.printlnCond("4.  Convert the data part of the dates to a day and decimal UT.")
    gui.printlnCond("    March: day " + str(marchDate.getiDay()) + ", " + ASTTime.timeToStr_dec(UTm, DECFORMAT) + " hours")
    gui.printlnCond("    June: day " + str(juneDate.getiDay()) + ", " + ASTTime.timeToStr_dec(UTj, DECFORMAT) + " hours")
    gui.printlnCond("    Sept: day " + str(septDate.getiDay()) + ", " + ASTTime.timeToStr_dec(UTs, DECFORMAT) + " hours")
    gui.printlnCond("    Dec: day " + str(decDate.getiDay()) + ", " + ASTTime.timeToStr_dec(UTd, DECFORMAT) + " hours")
    gui.printlnCond()

    gui.printlnCond("5.  Convert the UT results to HMS format.")
    prt.println("    March equinox on " + ASTDate.dateToStr_obj(marchDate) + " at " + 
                ASTTime.timeToStr_dec(UTm, HMSFORMAT) + " UT")
    prt.println("    June solstice on " + ASTDate.dateToStr_obj(juneDate) + " at " + 
                ASTTime.timeToStr_dec(UTj, HMSFORMAT) + " UT")
    prt.println("    September equinox on " + ASTDate.dateToStr_obj(septDate) + " at " +
                ASTTime.timeToStr_dec(UTs, HMSFORMAT) + " UT")
    prt.println("    December equinox on " + ASTDate.dateToStr_obj(decDate) + " at " + 
                ASTTime.timeToStr_dec(UTd, HMSFORMAT) + " UT")
    
    gui.setResults("Solstices and Equinoxes are listed in the text area below")
    prt.setProportionalFont()
    prt.resetCursor()



def calcSunPosition(gui):
    """
    Calculate the position of the Sun, using the currently loaded orbital
    elements and the current observer position.
 
    :param tkwidget gui: GUI object from which the request came
    """
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()
    iteration = 0
    Bsun = 0.0

    prtObj = None
    prt = gui.getPrtInstance()
    
    # Set up whether to display interim results when solving Kepler's equation
    if (gui.getShowInterimCalcsStatus()):
        prtObj = prt
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):
        return
    
    # Get Sun index after validating that orbital elements are loaded
    idxSun = ASTOrbits.getOEDBSunIndex()
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Compute the Sun's Position for the Current Observer", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    Ecc = ASTOrbits.getOEObjEccentricity(idxSun)
    
    # Do all the time-related calculations
    LCT = observer.getObsTime().getDecTime()
    UT = ASTTime.LCTtoUT(LCT,gui.getDSTStatus(),observer.getObsTimeZone(),observer.getObsLon(),dateAdjustObj)
    dateAdjust = dateAdjustObj.getIntValue()
    
    # adjust the date, if needed, since the LCTtoUT conversion could have changed the date
    adjustedDate = ASTTime.adjustDatebyDays(observer.getObsDate(),dateAdjust)
    GST = ASTTime.UTtoGST(UT, adjustedDate)
    LST = ASTTime.GSTtoLST(GST, observer.getObsLon())
    
    gui.printlnCond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
    gui.printlnCond("    LCT = " + ASTTime.timeToStr_obj(observer.getObsTime(), DECFORMAT) +
                    " hours, date is " + ASTDate.dateToStr_obj(observer.getObsDate()) + ")")
    gui.printCond("     UT = " + ASTTime.timeToStr_dec(UT, DECFORMAT) + " hours (")
    if (dateAdjust < 0):
        gui.printCond("previous day ")
    elif (dateAdjust > 0):
        gui.printCond("next day ")
    gui.printlnCond(ASTDate.dateToStr_obj(adjustedDate) + ")")
    gui.printlnCond("    GST = " + ASTTime.timeToStr_dec(GST, DECFORMAT) + " hours")
    gui.printlnCond("    LST = " + ASTTime.timeToStr_dec(LST, DECFORMAT) + " hours")
    gui.printlnCond()
    
    JDe = ASTOrbits.getOEEpochJD()
    gui.printlnCond("2.  Compute the Julian day number for the standard epoch.")
    gui.printlnCond("    Epoch: " + ASTStr.strFormat(ASTStyle.EPOCHFORMAT, ASTOrbits.getOEEpochDate()))
    gui.printlnCond("    JDe = " + ASTStr.strFormat(ASTStyle.JDFormat, JDe))
    gui.printlnCond()
    
    JD = ASTDate.dateToJD_mdy(adjustedDate.getMonth(),adjustedDate.getdDay() + (UT / 24.0),adjustedDate.getYear())
    gui.printlnCond("3.  Compute the Julian day number for the desired date, being sure to use the")
    gui.printlnCond("    Greenwich date and UT from step 1, and including the fractional part of the day.")
    gui.printlnCond("    JD = " + ASTStr.strFormat(ASTStyle.JDFormat, JD))
    gui.printlnCond()
    
    De = JD - JDe
    gui.printlnCond("4.  Compute the total number of elapsed days since the standard epoch.")
    gui.printlnCond("    De = JD - JDe = " + ASTStr.strFormat(ASTStyle.JDFormat, JD) + " - " +
                    ASTStr.strFormat(ASTStyle.JDFormat, JDe))
    gui.printlnCond("       = " + ASTStr.strFormat(ASTStyle.GENFLOATFORMAT, De) + " days")
    gui.printlnCond()
    
    Msun = ((360.0 * De) / 365.242191) + ASTOrbits.getOEObjLonAtEpoch(idxSun) - ASTOrbits.getOEObjLonAtPeri(idxSun)
    gui.printlnCond("5.  Compute the Sun's Mean Anomaly.")
    gui.printlnCond("    Msun = [(360.0 * De)/365.242191] + Eg - Wg")
    gui.printlnCond("         = [(360.0 * " + str(De) + ")/365.242191] + " + 
                    str(ASTOrbits.getOEObjLonAtEpoch(idxSun)) + " - " + 
                    str(ASTOrbits.getOEObjLonAtPeri(idxSun)))
    gui.printlnCond("         = " + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    gui.printlnCond("6.  Use the MOD function to adjust Msun to the range [0, 360].")
    gui.printlnCond("    Msun = Msun MOD 360 = " + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + " MOD 360")
    Msun = ASTMath.xMOD(Msun, 360.0)
    gui.printlnCond("         = " + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    # Find the true anomaly from equation of center or Kepler's equation
    if (gui.getEQofCenterStatus()):
        Ec = (360.0 / math.pi) * Ecc * ASTMath.SIN_D(Msun)
        gui.printlnCond("7.  Solve the equation of the center for the Sun.")
        gui.printlnCond("    Ec = (360/pi) * eccentricity * sin(Msun) = (360/3.1415927) * " + str(Ecc) +
                        " * sin(" + ASTAngle.angleToStr_dec(Msun, DECFORMAT) + ")")
        gui.printlnCond("       = " + ASTAngle.angleToStr_dec(Ec, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        Vsun = Ec + Msun
        gui.printlnCond("8.  Add Ec to Msun to get the true anomaly.")
        gui.printlnCond("    Vsun = Ec + Msun = " + ASTAngle.angleToStr_dec(Ec, DECFORMAT) + " + " +
                        ASTAngle.angleToStr_dec(Msun, DECFORMAT))
        gui.printlnCond("         = " + ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + " degrees")
    else:
        gui.printlnCond("7.  Solve Kepler's equation to get the eccentric anomaly.")
        if (gui.getSimpleIterationStatus()):
            tmpKepler = ASTKepler.calcSimpleKepler(Msun, Ecc, __termCriteria,prtObj)
            Ea = tmpKepler[0]
            iteration = int(tmpKepler[1])
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(Msun, Ecc, __termCriteria,prtObj)
            Ea = tmpKepler[0]
            iteration = int(tmpKepler[1])
            
        gui.printlnCond("    Ea = E" + str(iteration) + " = " + ASTAngle.angleToStr_dec(Ea, DECFORMAT) + " degrees")
        gui.printlnCond()
    
        Vsun = (1 + Ecc) / (1 - Ecc)
        Vsun = math.sqrt(Vsun) * ASTMath.TAN_D(Ea / 2.0)
        Vsun = 2.0 * ASTMath.INVTAN_D(Vsun)
        gui.printlnCond("8.  Vsun = 2 * inv tan[ sqrt[(1+eccentricity)/(1-eccentricity)] * tan(Ea/2) ]")
        gui.printlnCond("         = 2 * inv tan[ sqrt[(1+" + str(Ecc) + ")/(1-" + str(Ecc) + ")] * tan(" +
                        ASTAngle.angleToStr_dec(Ea, DECFORMAT) + "/2) ]")
        gui.printlnCond("         = " + ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + " degrees")

    gui.printlnCond()
    
    gui.printlnCond("9.  Use the MOD function to adjust Vsun to the range [0, 360].")
    gui.printlnCond("    Vsun = Vsun MOD 360 = " + ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + " MOD 360")
    Vsun = ASTMath.xMOD(Vsun, 360.0)
    gui.printlnCond("         = " + ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    Lsun = Vsun + ASTOrbits.getOEObjLonAtPeri(idxSun)
    gui.printlnCond("10. Add Vsun and Wg to get the ecliptic longitude.")
    gui.printlnCond("    Lsun = Vsun + Wg = " + ASTAngle.angleToStr_dec(Vsun, DECFORMAT) + " + " +
                    ASTAngle.angleToStr_dec(ASTOrbits.getOEObjLonAtPeri(idxSun), DECFORMAT))
    gui.printlnCond("         = " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    if (Lsun > 360.0):
        Lsun = Lsun - 360.0
    gui.printlnCond("11. if Lsun > 360, subtract 360.")
    gui.printlnCond("    Lsun = " + ASTAngle.angleToStr_dec(Lsun, DECFORMAT) + " degrees")
    gui.printlnCond("    (Ecliptic latitude is 0 degrees)")
    gui.printlnCond()
    
    eqCoord = ASTCoord.EclipticToEquatorial(Bsun, Lsun, ASTOrbits.getOEEpochDate())
    gui.printlnCond("12. Convert ecliptic coordinates to equatorial coordinates.")
    gui.printlnCond("    RA = " + ASTTime.timeToStr_dec(eqCoord.getRAAngle().getDecTime(), DECFORMAT) +
                    " hours, Decl = " + ASTAngle.angleToStr_dec(eqCoord.getDeclAngle().getDecAngle(), DECFORMAT) +
                    " degrees")
    gui.printlnCond()
     
    horizonCoord = ASTCoord.RADecltoHorizon(eqCoord.getRAAngle().getDecTime(),\
                                            eqCoord.getDeclAngle().getDecAngle(),observer.getObsLat(),LST)
    gui.printlnCond("13. Convert equatorial coordinates to horizon coordinates.")
    gui.printlnCond("    Alt = " + ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) +
                    ", Az = " + ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT))
    gui.printlnCond()
    
    result = ASTAngle.angleToStr_dec(horizonCoord.getAltAngle().getDecAngle(), DMSFORMAT) + " Alt, " +\
             ASTAngle.angleToStr_dec(horizonCoord.getAzAngle().getDecAngle(), DMSFORMAT) + " Az"
    gui.printlnCond("Thus, for this observer location and date/time, the Sun")
    gui.printlnCond("is at " + result + ".")
    
    gui.setResults("Sun's Location is " + result)
    prt.setProportionalFont()
    prt.resetCursor()



def calcSunRiseSet(gui):
    """
    Calculate the times of sunrise and sunset.
    
    :param tkwidget gui: GUI object from which the request came
    """
    prt = gui.getPrtInstance()
    observer = gui.getcurrentObserver()
    dateAdjustObj = ASTInt.ASTInt()
    
    # Validate the observer location data and be sure orbital elements are loaded
    if not (gui.validateGUIObsLoc()):
        return
    if not (gui.checkOEDBLoaded()):
        return
    
    gui.clearTextAreas()
    
    prt.setBoldFont(True)
    gui.printlnCond("Calculate Sunrise and Sunset for the Current Observer", CENTERTXT)
    prt.setBoldFont(False)
    gui.printlnCond()
    
    prt.setFixedWidthFont()
    
    if (gui.getSimpleIterationStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
    elif (gui.getNewtonMethodStatus()):
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
    else:
        solveTrueAnomaly = ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER
    
    eclCoord = ASTOrbits.calcSunEclipticCoord(observer.getObsDate().getMonth(),observer.getObsDate().getiDay(),\
                                              observer.getObsDate().getYear(),0.0, solveTrueAnomaly,__termCriteria)
    Bsun1 = eclCoord[ASTOrbits.SUN_ECLLAT]
    Lsun1 = eclCoord[ASTOrbits.SUN_ECLLON]
    gui.printlnCond("1.  Calculate the Sun's ecliptic location at midnight for the date in question.")
    gui.printlnCond("    For UT=0 hours on the date " + 
                    ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),
                                          observer.getObsDate().getiDay(), observer.getObsDate().getYear()))
    gui.printlnCond("    The Sun's coordinates are Bsun1 = " + ASTAngle.angleToStr_dec(Bsun1, DECFORMAT) +
                    " degrees, Lsun1 = " + ASTAngle.angleToStr_dec(Lsun1, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eqCoord1 = ASTCoord.EclipticToEquatorial(Bsun1, Lsun1, ASTOrbits.getOEEpochDate())
    gui.printlnCond("2.  Convert the Sun's ecliptic coordinates to equatorial coordinates.")
    gui.printlnCond("    RA1 = " + ASTTime.timeToStr_dec(eqCoord1.getRAAngle().getDecTime(), DECFORMAT) +
                    " hours, Decl1 = " + ASTAngle.angleToStr_dec(eqCoord1.getDeclAngle().getDecAngle(), DECFORMAT) + 
                    " degrees")
    gui.printlnCond()
    
    eclCoord = ASTOrbits.calcSunEclipticCoord(observer.getObsDate().getMonth(),observer.getObsDate().getiDay(),
                                              observer.getObsDate().getYear(),0.0,solveTrueAnomaly,__termCriteria)
    
    LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord1.getRAAngle().getDecTime(),eqCoord1.getDeclAngle().getDecAngle(),
                                          observer.getObsLat())
    riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0)
    ST1r = LSTTimes[ASTOrbits.RISE_TIME]
    ST1s = LSTTimes[ASTOrbits.SET_TIME]
    gui.printlnCond("3.  Using the equatorial coordinates from step 2, compute the")
    gui.printlnCond("    LST rising and setting times.")
    if not (riseSet):
        gui.printlnCond("    The Sun does not rise or set for this observer.")
        return

    gui.printlnCond("    ST1r = " + ASTTime.timeToStr_dec(ST1r, DECFORMAT) + " hours")
    gui.printlnCond("    ST1s = " + ASTTime.timeToStr_dec(ST1s, DECFORMAT) + " hours")
    gui.printlnCond()
    
    Lsun2 = Lsun1 + 0.985647
    Bsun2 = Bsun1
    gui.printlnCond("4.  Calculate the Sun's ecliptic coordinates 24 hours later.")
    gui.printlnCond("    24 hours later, the Sun's coordinates are")
    gui.printlnCond("    Bsun2 = " + ASTAngle.angleToStr_dec(Bsun1, DECFORMAT) + " degrees (same as Bsun1)")
    gui.printlnCond("    Lsun2 = Lsun1 + 0.985647 = " + ASTAngle.angleToStr_dec(Lsun1, DECFORMAT) + " + 0.985647")
    gui.printlnCond("          = " + ASTAngle.angleToStr_dec(Lsun2, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    if (Lsun2 > 360.0):
        Lsun2 = Lsun2 - 360.0
    gui.printlnCond("5.  if Lsun2 > 360.0, subtract 360 degrees.")
    gui.printlnCond("    Lsun2 = " + ASTAngle.angleToStr_dec(Lsun2, DECFORMAT) + " degrees")
    gui.printlnCond()
    
    eqCoord2 = ASTCoord.EclipticToEquatorial(Bsun2, Lsun2, ASTOrbits.getOEEpochDate())
    gui.printlnCond("6.  Convert the ecliptic coordinates from the previous step to equatorial coordinates.")
    gui.printlnCond("    RA2 = " + ASTTime.timeToStr_dec(eqCoord2.getRAAngle().getDecTime(), DECFORMAT) +
                    " hours, Decl2 = " + ASTAngle.angleToStr_dec(eqCoord2.getDeclAngle().getDecAngle(), DECFORMAT) +
                    " degrees")
    gui.printlnCond()
    
    LSTTimes = ASTOrbits.calcRiseSetTimes(eqCoord2.getRAAngle().getDecTime(), eqCoord2.getDeclAngle().getDecAngle(),
                                          observer.getObsLat())
    riseSet = (LSTTimes[ASTOrbits.RISE_SET_FLAG] > 0.0)
    ST2r = LSTTimes[ASTOrbits.RISE_TIME]
    ST2s = LSTTimes[ASTOrbits.SET_TIME]
    gui.printlnCond("7.  Using the equatorial coordinates from step 6, compute the")
    gui.printlnCond("    second set of LST rising and setting times.")
    if not (riseSet):
        gui.printlnCond("    The Sun does not rise or set for this observer.")
        return
    gui.printlnCond("    ST2r = " + ASTTime.timeToStr_dec(ST2r, DECFORMAT) + " hours")
    gui.printlnCond("    ST2s = " + ASTTime.timeToStr_dec(ST2s, DECFORMAT) + " hours")
    gui.printlnCond()
    
    Tr = (24.07 * ST1r) / (24.07 + ST1r - ST2r)
    gui.printlnCond("8.  Interpolate the two sets of LST rising times.")
    gui.printlnCond("    Tr = (24.07 * ST1r) / (24.07 + ST1r - ST2r)")
    gui.printlnCond("       = (24.07*" + ASTTime.timeToStr_dec(ST1r, DECFORMAT) + ")/(24.07+" +
                    ASTTime.timeToStr_dec(ST1r, DECFORMAT) + "-" + ASTTime.timeToStr_dec(ST2r, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTTime.timeToStr_dec(Tr, DECFORMAT) + " hours")
    gui.printlnCond()
    
    Ts = (24.07 * ST1s) / (24.07 + ST1s - ST2s)
    gui.printlnCond("9.  Interpolate the two sets of LST setting times.")
    gui.printlnCond("    Ts = (24.07 * ST1s) / (24.07 + ST1s - ST2s)")
    gui.printlnCond("       = (24.07*" + ASTTime.timeToStr_dec(ST1s, DECFORMAT) + ")/(24.07+" +
                    ASTTime.timeToStr_dec(ST1s, DECFORMAT) + "-" + ASTTime.timeToStr_dec(ST2s, DECFORMAT) + ")")
    gui.printlnCond("       = " + ASTTime.timeToStr_dec(Ts, DECFORMAT) + " hours")
    gui.printlnCond()
    
    GST = ASTTime.LSTtoGST(Tr, observer.getObsLon())
    UT = ASTTime.GSTtoUT(GST, observer.getObsDate())
    LCTr = ASTTime.UTtoLCT(UT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj)
    GST = ASTTime.LSTtoGST(Ts, observer.getObsLon())
    UT = ASTTime.GSTtoUT(GST, observer.getObsDate())
    LCTs = ASTTime.UTtoLCT(UT, gui.getDSTStatus(), observer.getObsTimeZone(), observer.getObsLon(),dateAdjustObj)
    gui.printlnCond("10. Convert the LST rising/setting times to their corresponding LCT times.")
    gui.printlnCond("    LCTr = " + ASTTime.timeToStr_dec(LCTr, DECFORMAT) + " hours, LCTs = " +
                    ASTTime.timeToStr_dec(LCTs, DECFORMAT) + " hours")
    gui.printlnCond()
    
    gui.printlnCond("11. Convert the LCT times to HMS format.")
    gui.printlnCond("    LCTr = " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + ", LCTs = " +
                    ASTTime.timeToStr_dec(LCTs, HMSFORMAT))
    gui.printlnCond()
    
    gui.printlnCond("Thus, on " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(),
                                                        observer.getObsDate().getiDay(), observer.getObsDate().getYear()) +
                    " for the current observer")
    gui.printlnCond("the Sun will rise at " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + " LCT and set at " +
                    ASTTime.timeToStr_dec(LCTs, HMSFORMAT) + " LCT")
    
    prt.setProportionalFont()
    prt.resetCursor()
    
    gui.setResults("On " + ASTDate.dateToStr_mdy(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                                                 observer.getObsDate().getYear()) +
                   ", the Sun will rise at " + ASTTime.timeToStr_dec(LCTr, HMSFORMAT) + " LCT and set at " +
                   ASTTime.timeToStr_dec(LCTs, HMSFORMAT) + " LCT")



def setTerminationCriteria(gui):
    """
    Set the termination criteria for solving Kepler's equation
    
    :param tkwidget gui: GUI object from which the request came
    """
    global __termCriteria
    
    prt = gui.getPrtInstance()
    dTerm = __termCriteria
    
    gui.clearTextAreas()
    prt.setBoldFont(True)
    prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    if (ASTQuery.showQueryForm(["Enter Termination Criteria in radians\n(ex: 0.000002)"]) != ASTQuery.QUERY_OK):
        prt.println("Termination criteria was not changed from " + str(__termCriteria) + " radians")
        return

    # Validate the termination criteria
    rTmpObj = ASTReal.isValidReal(ASTQuery.getData(1),HIDE_ERRORS)
    if (rTmpObj.isValidRealObj()):
        dTerm = rTmpObj.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
        prt.println("Termination criteria was not changed from " + str(__termCriteria) + " radians")
        return

    prt.println("Prior termination criteria was: " + str(__termCriteria) + " radians")
    if (dTerm < ASTKepler.KeplerMinCriteria):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too small, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    if (dTerm > 1):
        dTerm = ASTKepler.KeplerMinCriteria
        prt.println("Criteria entered was too large, so it has been reset to " + str(dTerm) + " radians")
        prt.println()
    __termCriteria = dTerm
    
    prt.println("Termination criteria is now set to " + str(__termCriteria) + " radians")
    prt.resetCursor()



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
