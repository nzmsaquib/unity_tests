"""
Implements action listeners for the menu items

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import sys

import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT

import Chap6.ChapEnums
import Chap6.OrbitalElementsActions as oe_actions
import Chap6.SolarInfoActions as si_actions

#==================================================
# Define listeners for about, exit, instructions
#==================================================

def aboutMenuListener(gui):
    """
    Handle a click on the About menu item
    
    :param tkwidget gui: GUI that the About Box is associated with
    """
    gui.showAboutBox()
    


def exitMenuListener():
    """Handle a click on the Exit menu item"""
    if ASTMsg.pleaseConfirm("Are you sure you want to exit?"," "):
        sys.exit()    
    


def instructionsMenuListener(gui):
    """
    Handle a click on the Instructions menu item
    
    :param tkwidget gui: GUI object from which the request came
    """
    gui.clearTextAreas()
    prt = gui.getPrtInstance()
    prt.setBoldFont(True)
    prt.println("Instructions",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " +
            "be used in subsequent calculations. Not all of the observer information is required for every " +
            "calculation (e.g., only the year is used to calculate the solstices and equinoxes), but all " +
            "data items must be entered anyway. You may find it convenient to enter an initial latitude, longitude, " +
            "and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " +
            "longitude, and time zone are already filled in with default values when the program starts. When this program " +
            "begins, the date and time will default to the local date and time at which the program is started.")
    prt.println()
    prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " +
            "various calculations are performed. Also, select the appropriate radio button to choose what method to " +
            "use when it is necessary to determine the Sun's true anomaly. The radio button 'EQ of Center' will solve " +
            "the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " +
            "'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " +
            "Newton/Raphson method. The menu entry 'Solar Info->Set Termination Critera' allows you to enter the " +
            "termination criteria (in radians) for when to stop iterating to solve Kepler's equation.")
    prt.println()
    prt.println("The menu 'Solar Info' performs various calculations related to the Sun, such as determining its " +
            "location for the current information in the 'Observer Location and Time' data area. The menu " +
            "'Orbital Elements' allow you to load data files that contain the Sun's orbital elements referenced to " +
            "a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " +
            "starts up. The menu entry 'Orbital Elements->Load Orbital Elements' allows you to load and use orbital elements " +
            "referenced to some other epoch.")
    prt.resetCursor()
   


#==================================================
# Define listener for the Conversion menu items
#==================================================

def menuListener(calcToDo,gui):
    """
    Handle a click on the menu items
    
    :param CalculationType calcToDo: the calculation to perform
    :param tkwidget gui: GUI object to which menu items are associated
    """
    cen = Chap6.ChapEnums.CalculationType           # shorten to make typing easier!!!
    
    #**************** Solar Info Menu
    if (calcToDo == cen.SUN_LOCATION):
        si_actions.calcSunPosition(gui)
    elif (calcToDo == cen.SUN_RISE_SET):
        si_actions.calcSunRiseSet(gui)
    elif (calcToDo == cen.EQUINOXES_SOLSTICES):
        si_actions.calcEquinoxesSolstices(gui)
    elif (calcToDo == cen.SUN_DIST_AND_ANG_DIAMETER):
        si_actions.calcDistAndAngDiameter(gui)
    elif (calcToDo == cen.EQ_OF_TIME):
        si_actions.calcEqOfTime(gui)
    elif (calcToDo == cen.TERM_CRITERIA):
        si_actions.setTerminationCriteria(gui)
 
    #************* Orbital Elements menu
    elif (calcToDo == cen.LOAD_ORBITAL_ELEMENTS):
        oe_actions.loadOrbitalElements(gui)
    elif (calcToDo == cen.SHOW_ORBITAL_ELEMENTS):
        oe_actions.showSunsOrbitalElements(gui)

    else:
        ASTMsg.errMsg("No appropriate menu item has been selected. Choose an item\n" +
                      "from the 'Solar Info' or 'Orbital Elements' menu and try again.",
                      "No Menu Item Selected")
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass    
