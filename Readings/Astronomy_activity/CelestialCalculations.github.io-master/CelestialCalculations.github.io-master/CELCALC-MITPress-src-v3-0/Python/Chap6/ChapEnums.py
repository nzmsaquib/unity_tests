"""
Define an enumerated type for the chapter's menu items.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""
from enum import Enum, auto

class CalculationType(Enum):
    """Define values that indicate what menu item to do."""
    # Solar Info Menu
    SUN_LOCATION = auto()               # Calculate the Sun's location
    SUN_RISE_SET = auto()               # Calculate Sunrise/Sunset
    EQUINOXES_SOLSTICES = auto()        # Calculate time of equinoxes and solstices
    SUN_DIST_AND_ANG_DIAMETER = auto()  # Calculate Solar distance and angular diameter
    EQ_OF_TIME = auto()                 # Calculate the equation of time
    TERM_CRITERIA = auto()              # Set a termination criteria for solving Kepler's equation
    
    # Orbital Elements Menu
    LOAD_ORBITAL_ELEMENTS = auto()     # Load Sun's orbital elements from a disk file
    SHOW_ORBITAL_ELEMENTS = auto()     # Display the Sun's orbital elements
     


#=========== Main entry point ===============
if __name__ == '__main__':
    pass   
