"""
Provides a class for a date object and related methods.

Methods provided include validating strings in the form
mm/dd/yyyy where all fields are required and yyyy could
be negative. The validate methods ensure that mm is in [1,12]
and that the day is in an appropriate range for the mm
including checking for leap years for Gregorian dates).

Note that different languages have date-related routines, but
they are not used here because (a) we need to allow
dates to be negative and (b) implementing these routines
rather than using a language's native date routines makes
it easier to translate to a different programming language.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import datetime

import ASTUtils.ASTInt as ASTInt
from ASTUtils.ASTMisc import SHOW_ERRORS, HIDE_ERRORS
import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr

daysOfWeek = ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

class ASTDate():
    """Defines a class for dates."""
    #===============================================================
    # Class instance variables
    #===============================================================
    # bValid                     true if object holds a valid date
    # iMonth
    # iDay                       provide both int and double day to support Julian day number calculations
    # dDay
    # iYear
    
    def __init__(self):
        """
        Constructor to create a date object.
        
        Since much of the usage of this class involves validating
        that a string has a valid date, assume a new object is invalid
        until it is proven otherwise.
        """
        self.bValid = False
        self.iMonth = 1
        self.iDay = 1
        self.dDay = 1.0
        self.iYear = 0
        
    #========================================================
    # Define 'get/set' accessors for the object's fields
    #========================================================
    def isValidDateObj(self):
        return self.bValid
    def getMonth(self):
        return self.iMonth
    def getiDay(self):
        return self.iDay
    def getdDay(self):
        return self.dDay
    def getYear(self):
        return self.iYear
    
    # In the setters, try to protect the call by ensuring valid
    # values for date components
    def setMonth(self,month):
        self.iMonth = 1
        if isinstance(month,(int)):
            if (month > 0) and (month <= 12):
                self.iMonth = month
            else:
                self.bValid = False
        else:
            self.bValid = False
    def setiDay(self,day):
        self.iDay = 1
        self.dDay = 1.0
        if isinstance(day,(int)):
            if (day > 0) and (day <= 31):
                self.iDay = day
                self.dDay = float(day)
            else:
                self.bValid = False
        else:
            self.bValid = False
    def setdDay(self,day):
        self.iDay = 1
        self.dDay = 1.0
        if isinstance(day,(int,float)):
            if (day > 0) and (day <= 32):
                self.iDay = int(day)
                self.dDay = day
            else:
                self.bValid = False
        else:
            self.bValid = False        
    def setYear(self,year):
        self.iYear = 1
        if isinstance(year,(int)):
            self.iYear = year
        else:
            self.bValid = False
            


#==================================================================================
# Define some general methods for validating that a string contains a valid date.
#==================================================================================

def isValidDate(inputStr,flag=SHOW_ERRORS,allowFrac=False):
    """
    Check a string to see if contains a valid Date. Do not display any error
    message unless flag is SHOW_ERRORS to let the calling routine display
    its own message. If allowFrac is true, the input string is allowed
    to have a decimal day. If allowFrac is false, the input string
    must have an integer day. Note that if allowFrac needs to be
    passed in, flag must also be passed in.
    
    The custom code here is needed rather than simply using date methods
    native to Python because we want to allow for the possibility that yyyy
    could be negative. Note that range checking is done to ensure that mm
    and dd are in the proper ranges. A check is also made to ensure
    that dd = 29 only for leap years, but this only works for years in the 
    Gregorian calendar (i.e., years greater than 1581).

    :param str inputStr: string to be validated
    :param bool flag: whether to display error messages 
    :param bool allowFrac: if true, allow fractional part of
                           a day to be in the input string
                           and allow day to be 0
    :return: an ASTDate object with the result
    """
    dateObj = ASTDate()
    dTempReal = ASTReal.ASTReal()
    iMaxDay = 28                    # Max day for February
    iMinDay = 1                     # minimum day (could be 0 if allowFrac is true)
     
    dateObj.bValid = False
    if (allowFrac):
        iMinDay = 0                # allow 0 so routines can convert a '0 day' to a JD
     
    inputStr = ASTStr.removeWhitespace(inputStr)
    if (len(inputStr) <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                      "Please enter a valid Date in the form mm/dd/yyyy","Invalid Date format",flag)
        return dateObj
    
    parts = inputStr.split("/")
    
    if (len(parts) != 3):               # Must have exactly 3 fields
        ASTMsg.errMsg("The Date entered is not in a valid format. Please enter\n" +
                      "a valid Date in the form mm/dd/yyyy","Invalid Date format",flag)
        return dateObj
     
    # Get the year
    iTempInt = ASTInt.isValidInt(parts[2],HIDE_ERRORS)
    if (iTempInt.isValidIntObj()):
        dateObj.iYear = iTempInt.getIntValue()
    else:
        ASTMsg.errMsg("The Year entered is not valid. Please enter\n" +
                      "a valid Date in the form mm/dd/yyyy","Invalid Date (Year)",flag)
        return dateObj

     
    # Get month
    iTempInt = ASTInt.isValidInt(parts[0], HIDE_ERRORS)
    if (iTempInt.isValidIntObj()):
        dateObj.iMonth = iTempInt.getIntValue()
    else:
        ASTMsg.errMsg("The Month entered is invalid. Please enter\n" +
                      "a valid Date in the form mm/dd/yyyy","Invalid Date (Month)",flag)
        return dateObj
    if ((dateObj.iMonth < 1) or (dateObj.iMonth > 12)):
        ASTMsg.errMsg("The Month entered is out of range. Please enter\n" +
                "a valid Date in the form mm/dd/yyyy","Invalid Date (Month)",flag)
        return dateObj
     
    # Get day and validate it according to the month
    if (allowFrac):                 # allow user to enter fractional days or 0
        dTempReal = ASTReal.isValidReal(parts[1],HIDE_ERRORS)
        if (dTempReal.isValidRealObj()):
            dateObj.dDay = dTempReal.getRealValue()
        else:
            ASTMsg.errMsg("The Day entered is not a valid value. Please enter\n" +
                          "a valid Date in the form mm/dd.dd/yyyy","Invalid Date (Day)",flag)
            return dateObj
        dateObj.iDay = ASTMath.Trunc(dateObj.dDay)            
    else:
        iTempInt = ASTInt.isValidInt(parts[1],HIDE_ERRORS)
        if (iTempInt.isValidIntObj()):
            dateObj.iDay = iTempInt.getIntValue()
        else:
            ASTMsg.errMsg("The Day entered is not a valid value. Please enter\n" +
                          "a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag)
            return dateObj
        dateObj.dDay = float(dateObj.iDay)
     
    if ((dateObj.iDay < iMinDay) or (dateObj.iDay > 31)):
        ASTMsg.errMsg("The Day entered is out of range. Please enter\n" +
                      "a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag)
        return dateObj

    # Check February
    if (dateObj.iMonth == 2):
        if (isLeapYear(dateObj.iYear)):
            iMaxDay = 29
        if (dateObj.iDay > iMaxDay):
            ASTMsg.errMsg("The Day entered for February is out of range. Please enter\n" +
                          "a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag)
            return dateObj

    # Check April, June, Sept, Nov
    if ((dateObj.iMonth == 4) or (dateObj.iMonth == 6) or (dateObj.iMonth == 9) or (dateObj.iMonth == 11)):
        if (dateObj.iDay > 30):
            ASTMsg.errMsg("The Day entered is out of range. Please enter\n" +
                          "a valid Date in the form mm/dd/yyyy","Invalid Date (Day)",flag)
            return dateObj
    
    dateObj.bValid = True
     
    return dateObj



# Python doesn't allow overloading so we have to have 2 different dateToStr functions
def dateToStr_obj(dateObj):
    """
    Convert a date to a string.
    
    :param ASTDate dateObj: date to be converted as an object
    :return: date as a string
    """
    return dateToStr_mdy(dateObj.iMonth,dateObj.iDay,dateObj.iYear)



def dateToStr_mdy(month,day,year):
    """
    Convert a date to a string.
    
    Day can be an integer or a decimal value to
    handle fractional days.

    :param int month: month as an integer
    :param int/float day: day as an integer or float
    :param int year: year as an integer
    :return: date as a string
    """
    result = ASTStr.strFormat("%02d/",month)
    if isinstance(day,(int)):
        result = result + ASTStr.strFormat("%02d/",day)
    else:
        result = result + ASTStr.strFormat("%05.2f/",day)   
    return result + ASTStr.strFormat("%d",year)
            


#==================================================================================
# These functions perform various date related calculations, such as determining
# if a year is a leap year and converting a date to a Julian day number.
# ==================================================================================

def getCurrentDate():
    """
    Define a function for returning today's current date. This is easy
    to do, but is provided as a function to ease portability to other languages.

    :return: current date as a string in the form MM/dd/yyyy
    """
    now = datetime.datetime.now()
    return dateToStr_mdy(now.month,now.day,now.year)
           


# Python doesn't allow overloading so we have to have 2 different dateToJDfunctions
def dateToJD_obj(dateObj):
    """
    Converts a calendar date to a Julian day number.
    
    :param ASTDate dateObj: date to be converted as an object
    :return: the Julian day number for the given date
    """
    return dateToJD_mdy(dateObj.iMonth,dateObj.dDay,dateObj.iYear)



def dateToJD_mdy(month,day,year):
    """
    Converts a calendar date to a Julian day number.
    
    :param int month: month to be converted
    :param float day: day, including fractional part of the day, to be converted
    :param int year: year to be converted
    :return: the Julian day number for the given date
    """
    if (month > 2):
        dY = float(year)
        dM = float(month)
    else:
        dY = float(year - 1)
        dM = float(month + 12)

    if (year < 0):
        dT = 0.75
    else:
        dT = 0.0

    dTemp = float(year) + float(month)/100.0 + day / 10000.0
    if (dTemp >= 1582.1015):
        dA = ASTMath.Trunc(dY/100.0)
        dB = 2.0 - dA + ASTMath.Trunc(dA/4.0)
    else:
        dA = 0.0
        dB = 0.0

    JD = dB + ASTMath.Trunc(365.25*dY-dT) + ASTMath.Trunc(30.6001*(dM+1.0)) + day + 1720994.5

    return JD



def daysIntoYear(iMonth,iDay,iYear):
    """
    Calculate how many days into a year a specific date is
    
    :param int iMonth: month for the date in question
    :param int iDay: day for the date in question
    :param int iYear: year for the date in question
    :return: # of days into the year iYear
    """
    if (isLeapYear(iYear)):
        T = 1
    else:
        T = 2
    
    N = ASTMath.Trunc(275.0 * iMonth / 9.0) - T * ASTMath.Trunc((iMonth + 9) / 12.0) + iDay - 30
    return N



def daysIntoYear2Date(iYear,N):
    """
    Calculate date when given the number of days into a year
    
    :param int iYear: year under consideration
    :param int N: number of days into iYear
    :return: the calendar date that is iDays into the year. No
             check is made to be sure N is in range.
    """
    calendarDate = ASTDate()
    
    if (isLeapYear(iYear)):
        iA = 1523
    else:
        iA = 1889
    
    iB = ASTMath.Trunc((N + iA - 122.1) / 365.25)
    iC = N + iA - ASTMath.Trunc(365.25 * iB)
    iE = ASTMath.Trunc(iC / 30.6001)
    
    if (iE < 13.5):
        iMonth = iE - 1
    else:
        iMonth = iE - 13
    
    iDay = iC - ASTMath.Trunc(30.6001 * iE)
    
    calendarDate.setYear(iYear)
    calendarDate.setMonth(iMonth)
    calendarDate.setiDay(iDay)
    
    return calendarDate



def isLeapYear(year):
    """
    Checks to see if a year is a leap year.
    
    By definition, a leap year must be in the Gregorian calendar (i.e., greater than 1581).
    This function will return false if the year is not a leap year or if the
    year is less than 1582.

    :param int year: year to check
    :return: true if year is a leap year
    """
    if (year < 1582):
        return False
    else:
        return ((year % 4 == 0) and (year % 100 != 0) or (year % 400 == 0))
    


def JDtoDate(JD):
    """
    Convert a Julian day number to a calendar date.
    
    :param float JD: Julian day number to convert
    :return: date as a date object
    """
    dateObj = ASTDate()

    JD1 = JD + 0.5
    iJD1 = ASTMath.Trunc(JD1)
    fJD1 = ASTMath.Frac(JD1)
    if (iJD1 > 2299160):
        iA = ASTMath.Trunc((iJD1 - 1867216.25) / 36524.25)
        iB = iJD1 + 1 + iA - ASTMath.Trunc(float(iA) / 4.0)
    else:
        iB = iJD1
    
    iC = iB + 1524
    iD = ASTMath.Trunc((iC - 122.1) / 365.25)
    iE = ASTMath.Trunc(365.25 * iD)
    iG = ASTMath.Trunc((iC - iE) / 30.6001)
    dDay = iC - iE + fJD1 - ASTMath.Trunc(30.6001 * iG)

    if (iG > 13.5):
        iMonth = int(iG - 13)
    else:
        iMonth = int(iG - 1)
    if (iMonth > 2.5):
        iYear = int(iD - 4716)
    else:
        iYear = int(iD - 4715)
    
    dateObj.bValid = True
    dateObj.iMonth = iMonth
    dateObj.dDay = dDay
    dateObj.iDay = ASTMath.Trunc(dDay)
    dateObj.iYear = iYear
    
    return dateObj
 


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
