"""
This class implements an observer class so that an
object can be created that represents a default observer
location and time.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import os, io

import ASTUtils.ASTDate as ASTDate  
import ASTUtils.ASTFileIO as ASTFileIO
import ASTUtils.ASTLatLon as ASTLatLon
from ASTUtils.ASTMisc import HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTTime as ASTTime

class __ConstException(Exception):
    """Define an exception for errors in reading the observer location data file"""
    pass



class ASTObserver():
    """Defines a class for observer location, date, time zone, and time"""
    #==================================================================
    # Class variables
    #==================================================================    
    __dataFilename = "DefaultObsLoc.dat"
    __dataDir = os.path.realpath(os.path.join("..","..","Ast Data Files"))      
    __fullFilename = os.path.realpath(os.path.join(__dataDir,__dataFilename))

    #==================================================================
    # Class instance variables
    #==================================================================
    # bValid                        true if this is a valid obs object
    # obsLatLon                     Observer's lat/lon location
    # tZone                         time zone as an enumerated type
    # obsDate                       default date
    # obsTime                       default obs time
    
    def __init__(self):
        """Constructor for an ASTObserver object"""
        # Initialize from the default observer data file, if it
        # exists. if it doesn't, do some reasonable default values.
        latStr = "0N"
        lonStr = "0E"
        zoneStr = "LON"
        dateStr = ASTDate.getCurrentDate()
        timeStr = ASTTime.getCurrentTime()

        self.bValid = True
        self.obsLatLon = ASTLatLon.ASTLatLon()
        self.tZone = ASTLatLon.TimeZoneType.LONGITUDE
        self.obsDate = ASTDate.ASTDate()
        self.obsTime = ASTTime.ASTTime()
        
        # If default observer location data file exists, read it in.
        # Otherwise, use the defaults set above
        if (ASTFileIO.doesFileExist(ASTObserver.__fullFilename)):
            # Note that we're using 'with open' so we don't have to explicitly close the file when we're done
            try:
                with io.open(ASTObserver.__fullFilename,"rt") as br:
                    # Validate that this is an observer location data file. if it is,
                    # continue processing. Otherwise, use defaults set up above.
                    strIn=ASTFileIO.readTillStr(br,"<ObserverLocation>",HIDE_ERRORS)
                    if (strIn == None):
                        raise __ConstException
                    if (len(strIn) <= 0):
                        raise __ConstException
                    strIn = ASTFileIO.getSimpleTaggedValue(br, "<Latitude>")
                    if (strIn != None):
                        latStr = strIn
                    strIn = ASTFileIO.getSimpleTaggedValue(br, "<Longitude>")
                    if (strIn != None):
                        lonStr = strIn
                    strIn = ASTFileIO.getSimpleTaggedValue(br, "<TimeZone>")
                    if (strIn != None):
                        zoneStr = strIn        
                
                br.close()          # strictly speaking, this isn't necessary
                
            except __ConstException:
                pass            # do nothing - accept the defaults
         
        if (isValidObsLoc(self,latStr,lonStr,zoneStr,dateStr,timeStr)):
            # do nothing - we just want the observer location object updated
            # which happens as a side effect of isValidObsLoc
            pass
    
    #==================================================================
    # Define 'get' accessors for the object's fields
    #==================================================================
    
    def isValidObjsObj(self):
        return self.bValid
    def getObsLocation(self):
        return self.obsLatLon
    def getObsLat(self):
        return self.obsLatLon.getLat()
    def getObsLon(self):
        return self.obsLatLon.getLon()
    def getObsTimeZone(self):
        return self.tZone
    def getObsDate(self):
        return self.obsDate
    def getObsTime(self):
        return self.obsTime
    
    def setObsDateTime(self,iMonth,iDay,iYear,dTime):
        """
        Sets the observer objects date and time
        
        :param int iMonth: observer month
        :param int iDay: observer day
        :param int iYear: observer year
        :param float dTime: observer time
        """
        self.obsDate.setMonth(iMonth)
        self.obsDate.setiDay(iDay)
        self.obsDate.setYear(iYear)
        self.obsDate.setdDay(iDay)
        self.obsTime.setDecTime(dTime)



#===============================================================
# Define some general functions to manipulate observer objects
#===============================================================
def isValidObsLoc(obsLoc,latStr,lonStr,tzStr,dateStr,timeStr):
        """
        Checks a collection of data strings to see if the data they contain
        specify a valid observer location, date, and time.

        :param ASTObserver obsLoc: observer object to update
        :param str latStr: string with a latitude
        :param str lonStr: string with a longitude
        :param str tzStr: string with a time zone (PST, MST, CST, EST, or LON)
        :param str dateStr: string with a date
        :param str timeStr: string with a time
        :return: true if the strings represent a valid location. Also, if
                 valid, obsLoc is updated. if not valid, obsLoc is undefined.
        """
        obsLoc.bValid = False
 
        tmpLatorLon = ASTLatLon.isValidLat(latStr,HIDE_ERRORS)
        if not (tmpLatorLon.isLatValid()):
            ASTMsg.errMsg("The observer latitude specified is invalid - try again", "Invalid Latitude")
            return False
        obsLoc.obsLatLon.setLat(tmpLatorLon.getLat())
        tmpLatorLon = ASTLatLon.isValidLon(lonStr, HIDE_ERRORS)
        if not (tmpLatorLon.isLonValid()):
            ASTMsg.errMsg("The observer longitude specified is invalid - try again", "Invalid Longitude")
            return False
        obsLoc.obsLatLon.setLon(tmpLatorLon.getLon())
 
        obsLoc.tZone = ASTLatLon.strToTimeZone(tzStr)
         
        tmpDate = ASTDate.isValidDate(dateStr,HIDE_ERRORS)
        if not (tmpDate.isValidDateObj()):
            ASTMsg.errMsg("The observer date specified is invalid - try again", "Invalid Date")
            return False

        tmpTime = ASTTime.isValidTime(timeStr,HIDE_ERRORS)
        if not (tmpTime.isValidTimeObj()):
            ASTMsg.errMsg("The observer time specified is invalid - try again", "Invalid Time")
            return False
        
        obsLoc.setObsDateTime(tmpDate.getMonth(),tmpDate.getiDay(),tmpDate.getYear(),tmpTime.getDecTime())
        
        obsLoc.bValid = True
        return True
 


#=========== Main entry point ===============
if __name__ == '__main__':
    pass  

