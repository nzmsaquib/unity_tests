"""
Defines information about the current version of the
software, the book name, author, etc.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

CODE_VER = "Py-3.0"             # book's software version, NOT the python version
BOOK_TITLE = "Celestial Calculations"
# BOOK_EDITION = "First Edition"
BOOK_EDITION = " "
BOOK_AUTHOR = "J. L. Lawrence, PhD"
BOOK_COPYRIGHT = "Copyright (c) 2018"



#=========== Main entry point ===============
if __name__ == '__main__':
    pass