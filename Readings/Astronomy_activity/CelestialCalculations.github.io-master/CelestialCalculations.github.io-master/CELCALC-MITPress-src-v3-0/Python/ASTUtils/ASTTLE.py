"""
This module provides the ability to load TLEs
from a data file and to store them in an accessible
structure. Only one TLE data file can be loaded at a time.

Note: TLE data bases are likely fairly small, so they aren't sorted by this module.
This means that any searches must be brute force through all the elements.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

import os, io

import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTFileIO as ASTFileIO
import ASTUtils.ASTInt as ASTInt
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import ABORT_PROG,HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr

class __TLEException(Exception):
    """Define an exception for errors while reading a TLE data file"""
    pass



class ASTTLE():
    """Provides a class for Two-Line Elements (TLEs)"""
    #==================================================================
    # Class variables
    #==================================================================
    _prt = None                     # instance so that we can output to a user's scrollable text area
    
    # File types and default directory for TLE data 
    _fileExtFilter = [("TLE Data","*.dat"),("All Files",".*")]
    _dataDir = os.path.realpath(os.path.join("..","..","Ast Data Files"))
    
    _TLEsLoaded = False
    _TLEsFilename = ""                            # The currently loaded TLE data file
    
    # This padding is used in case a TLE line is too short when extracting data
    _padding = " "*60
    
    _TLEdb = []            # This array will hold lines of TLE data

    def __init__(self,prtInstance):
        """
        Create a TLE instance. Only need one.
        
        :param ASTPrt prtInstance: instance of a scrollable text area
        """
        if (prtInstance == None):
            ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ABORT_PROG)
        
        ASTTLE._prt = prtInstance



class __RawTLE():
    """Define a private class for the TLEs"""
    #===============================================================
    # Class instance variables
    #===============================================================

    # We could also parse the data lines and store results,
    # but for our purposes we can just calculate them when
    # needed.
    # bNamed                true if set has a name
    # sName                 optional name for 2 TLE lines
    # sLine1
    # sLine2
    
    def __init__(self):
        """Create a raw TLE instance"""
        
        self.bNamed = False
        self.sName = ""
        self.sLine1 = ""
        self.sLine2 = ""
    


#=================================================================
# Define 'getter' methods to return information about
# the TLE database. Many of the methods return an
# object so that the caller can check the validity of
# the results.
#=================================================================

def getDataDir():
    """
    Gets the default data dir for TLE data
    
    :return: data directory path
    """
    return ASTTLE._dataDir



def getFileExtFilter():
    """
    Gets a filter for filtering TLE data files by extension.
    
    :return: file extensions filter
    """  
    return ASTTLE._fileExtFilter



def getTLEFilename():
    """
    Gets the filename for the currently loaded TLE database.
    
    :return: filename for the current TLE db
    """
    return ASTTLE._TLEsFilename



def getNumTLEDBObjs():
    """
    Gets the number of TLEs in the currently loaded TLE database.
    
    :return: number of TLEs in the db
    """   
    return len(ASTTLE._TLEdb)



def getTLEArgofPeri(idx):
    """
    Gets the argument of perigee from data line 2
    
    :param int idx: index into the TLE database
    :return: ASTReal with result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Argument of perigee is on the 2nd data line, columns 35-42
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[34:42].strip()
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



def getTLECatID(idx):
    """
    Gets the catalog ID from data line 1
    
    :param int idx: index into the TLE database
    :return: String with result
    """
    result = None
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Cat ID is on the 1st data line, columns 3-7
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[2:7].strip()
    return strTmp



def getTLEEccentricity(idx):
    """
    Gets the orbital eccentricity from data line 2
    
    :param int idx: index into the TLE database
    :return: ASTReal with result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Eccentricity is on the 2nd data line, columns 27-33
    # with a leading decimal point assumed.
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = "0."+ strTmp[26:33].strip()
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



def getTLEEpochDate(idx):
    """
    Gets the epoch date from the TLE.
    
    :param int idx: index into the TLE database
    :return: ASTDate with the result or null if an error occurs
    """
    result = None
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    iTmp = getTLEEpochYear(idx)
    if (iTmp.isValidIntObj()):
        iYear = iTmp.getIntValue()
    else:
        return result
     
    if (iYear < 57):
        iYear = iYear + 2000
    else:
        iYear = iYear + 1900
     
    rTmp = getTLEEpochDay(idx)
    if (rTmp.isValidRealObj()):
        dT = rTmp.getRealValue()
    else:
        return result
     
    daysIntoYear = ASTMath.Trunc(dT)
    return ASTDate.daysIntoYear2Date(iYear,daysIntoYear)



def getTLEEpochDay(idx):
    """
    Gets the Epoch day from data line 1
    
    :param int idx: index into the TLE database
    :return: ASTReal with result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Epoch day is on the 1st data line, columns 21-32
    strTmp = getTLELine1(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[20:32]
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



def getTLEEpochUT(idx):
    """
    Gets the epoch UT from the TLE.
    
    :param int idx: index into the TLE database
    :return: ASTReal with the result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
     
    rTmp = getTLEEpochDay(idx)
    if (rTmp.isValidRealObj()):
        dT = rTmp.getRealValue()
    else:
        return result
    
    dT = dT - ASTMath.Trunc(dT)         # strip off the day
    dT = ASTMath.Round(dT, 8) * 24.0    # convert to hours
    result.setRealValue(dT)
    return result



def getTLEEpochYear(idx):
    """
    Gets the Epoch year from data line 1
    
    :param int idx: index into the TLE database
    :return: ASTInt with result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTInt.isValidInt("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Epoch year is on the 1st data line, columns 19-20
    strTmp = getTLELine1(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[18:20]
    return ASTInt.isValidInt(strTmp,HIDE_ERRORS)



def getTLEInclination(idx):
    """
    Gets the orbital inclination from data line 2
    
    :param int idx: index into the TLE database
    :return: ASTReal with result
    """       
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Inclination is on the 2nd data line, columns 9-16
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[8:16]
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



def  getTLELine1(idx):
    """
    Returns 1st TLE data line
    
    :param int idx: index into TLE database
    :return: line1 or null if doesn't exist
    """
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return None
    return ASTTLE._TLEdb[idx].sLine1



def getTLELine2(idx):
    """
    Returns 2nd TLE data line
    
    :param int idx: index into TLE database
    :return: line2 or null if doesn't exist
    """
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return None
    return ASTTLE._TLEdb[idx].sLine2



def getTLEMeanAnomaly(idx):
    """
    Gets the Mean Anomaly from data line 2

    :param int idx: index into the TLE database
    :return: ASTReal with result
    """     
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Mean anomaly is on the 2nd data line, columns 44-51
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[43:51]
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



def getTLEMeanMotion(idx):
    """
    Gets the Mean Motion from data line 2
    
    :param int idx: index into the TLE database
    :return: ASTReal with result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # Mean motion is on the 2nd data line, columns 53-63
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[52:63]
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



def getTLERAAN(idx):
    """
    Gets the orbital RAAN from data line 2
    
    :param int idx: index into the TLE database
    :return: ASTReal with result
    """
    # Intentionally parse an invalid string to set the
    # return result as invalid
    result = ASTReal.isValidReal("xyz",HIDE_ERRORS)
     
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        return result
    
    # RAAN is on the 2nd data line, columns 18-25
    strTmp = getTLELine2(idx)
    if (strTmp == None):
        return result
     
    # Pad to avoid possibility of a substring error.
    strTmp = strTmp + ASTTLE._padding
    strTmp = strTmp[17:25]
    return ASTReal.isValidReal(strTmp,HIDE_ERRORS)



#============================================================
# Public functions for manipulating the TLE database
#============================================================

def areTLEsLoaded():
    """
    Check to see if a TLE database has been loaded.
    
    :return: true if TLE db has been loaded
    """
    return ASTTLE._TLEsLoaded



def clearTLEData():
    """Clear all the currently loaded TLE data"""
    
    # Delete all of the TLE data. In some languages, we'd need
    # to loop through the database and and delete objects individually.
    ASTTLE._TLEdb.clear()
    del ASTTLE._TLEdb[:]
    ASTTLE._TLEsFilename = ""
    ASTTLE._TLEsLoaded = False
    ASTTLE._TLEdb = []



def decodeTLE(idx,txt):
    """
    Decode a TLE data set to show what its elements are
    
    :param int idx: index into TLE db for set to display, assuming 0-based indexing!
    :param str txt: text to display as printout heading
    """
    prt = ASTTLE._prt
  
    prt.setBoldFont(True)
    prt.println(txt,CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.setFixedWidthFont()
    displayTLEHeader()
    displayTLEItem(idx)
    
    # Decode TLE data line 1
    strTmp = ASTTLE._TLEdb[idx].sLine1 + ASTTLE._padding
    prt.setBoldFont(True)
    prt.println("TLE Data Line 1")
    prt.setBoldFont(False)
    prt.println("Line ID (Always 1)    (Col 1)    : " + strTmp[0:1])
    prt.println("Catlog Number         (Col 3-7)  : " + strTmp[2:7])
    prt.println("Classification        (Col 8)    : " + strTmp[7:8])
    prt.println("Launch Year           (Col 10-11): " + strTmp[9:11])
    prt.println("Launch Number         (Col 12-14): " + strTmp[11:14])
    prt.println("Piece of the Launch   (Col 15-17): " + strTmp[14:17])
    prt.println("Epoch Year            (Col 19-20): " + strTmp[18:20])
    prt.println("Epoch Day of Year     (Col 21-32): " + strTmp[20:32])
    prt.println("1st Deriv Mean Motion (Col 34-43): " + strTmp[33:43])
    prt.println("2nd Deriv Mean Motion (Col 45-52): " + strTmp[44:52])
    prt.println("Drag Term             (Col 54-61): " + strTmp[53:61])
    prt.println("Ephemeris Type        (Col 63)   : " + strTmp[62:63])
    prt.println("Element Number        (Col 65-68): " + strTmp[64:68])
    prt.println("Checksum              (Col 69)   : " + strTmp[68:69])
    prt.println()
    
    strTmp = ASTTLE._TLEdb[idx].sLine2 + ASTTLE._padding
    prt.setBoldFont(True)
    prt.println("TLE Data Line 2")
    prt.setBoldFont(False)
    prt.println("Line ID (Always 2)    (Col 1)    : " + strTmp[0:1])
    prt.println("Catlog Number         (Col 3-7)  : " + strTmp[2:7])
    prt.println("Inclination (degrees) (Col 9-16) : " + strTmp[8:16])
    prt.println("RAAN (degrees)        (Col 18-25): " + strTmp[17:25])
    prt.println("Eccentricity          (Col 27-33): " + strTmp[26:33])
    prt.println("Arg of Perigee (deg)  (Col 35-42): " + strTmp[34:42])
    prt.println("Mean Anomaly (deg)    (Col 44-51): " + strTmp[43:51])
    prt.println("Mean Motion (rev/day) (Col 53-63): " + strTmp[52:63])
    prt.println("Num of Orbits         (Col 64-68): " + strTmp[63:68])
    prt.println("Checksum              (Col 69)   : " + strTmp[68:69])
    prt.println()
    
    prt.setProportionalFont()



def displayTLEHeader():
    """Display a header for the TLE data"""
    prt = ASTTLE._prt

    prt.println("TLE            1         2         3         4         5         6")
    prt.println("Set   123456789012345678901234567890123456789012345678901234567890123456789")
    prt.println("="*76)



def displayTLEItem(idx):
    """
    Display a single TLE data set
    
    :param int idx: which data set to display
    """
    prt = ASTTLE._prt
    
    if (ASTTLE._TLEdb[idx].bNamed):
        prt.println("      " + ASTTLE._TLEdb[idx].sName)
    prt.printnoln(ASTStr.strFormat("%-5d ", idx + 1))
    prt.println(ASTTLE._TLEdb[idx].sLine1)
    prt.println("      " + ASTTLE._TLEdb[idx].sLine2)



def findTLECatID(catID,iStart):
    """
    Search the currently loaded TLE database and return an index into the
    database for the requested catalog ID. An exact match is required. Note
    that there may be several items with the same name, so this routine
    must be invoked until all are found. Also note that the catalog ID is
    an integer, but we'll do a string search rather than converting the
    raw TLE data to an integer. The catalog ID is characters 3-7 on both
    TLE line 1 and line 2.
    
    :param str catID: catalog ID of the object to find
    :param int iStart: where to start searching in the db
    :return: If successful, returns an index into the currently
             loaded TLE database for the object requested. If 
             unsuccessful, -1 is returned. Note that this assumes
             0-based indexing!
    """
    dbSize = getNumTLEDBObjs()
    
    if (iStart >= dbSize):
        return -1
    
    targ = ASTStr.removeWhitespace(catID)
    for i in range(iStart, dbSize):
        # We can use either Line1 or Line2 - no need to check both
        # as they had better be the same catID! Since Python starts
        # indexing at 0, we want the substring that starts at
        # 2 and goes for 5 characters
        strTmp = ASTTLE._TLEdb[i].sLine1[2:7]
        if (ASTStr.compareIgnoreCase(targ,strTmp)):
            return i

    return -1



def findTLEName(name,iStart):
    """
    Search the currently loaded TLE database and return an index into the
    database for the requested object. The search performed is **not**
    case sensitive, and a partial match is counted as successful. Note
    that there may be several items with the same name, so this routine
    must be invoked until all are found.
 
    :param str name: name of the object to find
    :param int iStart: where to start searching in the db
    :return: If successful, returns an index into the currently
             loaded TLE database for the object requested. If 
             unsuccessful, -1 is returned. Note that this assumes
             0-based indexing!
    """
    dbSize = getNumTLEDBObjs()
    
    if (iStart >= dbSize):
        return -1
    
    targ = ASTStr.removeWhitespace(name).lower()
    for i in range(iStart,dbSize):
        if (ASTTLE._TLEdb[i].bNamed): 
            strTmp = ASTStr.removeWhitespace(ASTTLE._TLEdb[i].sName).lower()
            if (strTmp.find(targ) != -1):
                return i

    return -1



def getKeplerianElementsFromTLE(idx):
    """
    Validate the Keplerian elements and epoch from a TLE data set.

    :param int idx: index into the TLE database
    :return: returns true if the Keplerian elements are
             valid, else false. The calling routine
             must call the appropriate get* methods
             to actually get the elements.
    """
    if ((idx < 0) or (idx >= getNumTLEDBObjs())):
        ASTMsg.errMsg("No TLE Data Set number " + str(idx) + " exists", "Invalid Data Set Number")
        return False
    
    # Validate epoch year and epoch day
    iTmp = getTLEEpochYear(idx)
    if not (iTmp.isValidIntObj()):
        ASTMsg.errMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year")
        return False
    rTmp = getTLEEpochDay(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Epoch Day of Year in TLE Data Set", "Invalid Epoch Day of Year")
        return False

    # Validate Keplerian elements
    rTmp = getTLEInclination(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Inclination in TLE Data Set", "Invalid Inclination")
        return False
    rTmp = getTLERAAN(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid RAAN in TLE Data Set", "Invalid RAAN")
        return False
    rTmp = getTLEEccentricity(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Eccentricity in TLE Data Set", "Invalid Eccentricity")
        return False
    rTmp = getTLEArgofPeri(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Argument of Perigee in TLE Data Set", "Invalid Arg of Perigee")
        return False
    rTmp = getTLEMeanAnomaly(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Mean Anomaly in TLE Data Set", "Invalid Mean Anomaly")
        return False
    rTmp = getTLEMeanMotion(idx)
    if not (rTmp.isValidRealObj()):
        ASTMsg.errMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
        return False
    
    return True



def getTLEFileToOpen():
    """
    Puts up a browser window and gets a TLE data filename.

    :return: full name (file and path) of the TLE data file
             to open or null if no file is selected or
             file doesn't exist. This routine does **not**
             check to see if the file is a valid TLE data file, 
             but it **does** check to see that the file exists.
    """
    fileToRead = ASTFileIO.getFileToRead("Select TLE Data File to Load ...",getFileExtFilter(),getDataDir())

    # See if the file exists
    if not (ASTFileIO.doesFileExist(fileToRead)):
        ASTMsg.errMsg("The TLE data file specified does not exist","TLE File Does Not Exist")
        return None
    
    return fileToRead



def loadTLEDB(filename):
    """
    Loads TLEs database from disk.
    
    :param str filename: Full filename (including path) to load
    :return: true if successful, else false.
    """
    prt = ASTTLE._prt
    
    count = 0
    c = " "

    clearTLEData()
    
    # Note that we're using 'with open' so that we don't have to explicitly close the stream when we're done
    try:      
        with io.open(filename,"rt") as br:
            # Validate that the file is indeed a TLE data file
            strIn = ASTFileIO.readTillStr(br,"<TLEs>",HIDE_ERRORS)
            if (strIn == None):
                raise __TLEException
            
            # Look for the beginning of the data section, then cycle through
            # and extract each TLE data line.
            strIn = ASTFileIO.readTillStr(br,"<Data>")
            if (strIn == None):
                raise __TLEException

            for strIn in br:
                # Get rid of the line terminator, which differs between Windows (CRLF),
                # Unix (LF), MAC OS (CR).
                strIn = strIn.strip()
                
                if (strIn == None) or (len(strIn) <= 0):
                    continue                    # ignore blank lines
                
                name = ""
                named = False
                count = count + 1
 
                strIn = strIn.strip()
                str2 = ASTStr.removeWhitespace(strIn.lower())
                if (str2.find("</data>") >= 0):                # found end of data section
                    break
                   
                # See if this line is an object name
                c = strIn[0]
                if ((c != '1') and (c != '2')):
                    name = strIn
                    named = True
                    strIn = ASTFileIO.getNonBlankLine(br)
                    if (strIn == None):
                        break
                    strIn = strIn.strip()
 
                # if there's any error, may as well quit because we'll get out of synch
                # with the fact that line 1 must precede a line 2
                c = strIn[0]
                if (c != '1'):
                    prt.println("Error for TLE data set " + str(count) + ", line 1, near the string")
                    prt.println(strIn)
                    break
                else:
                    line1 = strIn
 
                strIn = ASTFileIO.getNonBlankLine(br)
                if (strIn == None):
                    break
                strIn = strIn.strip()
                c = strIn[0]
                if (c != '2'):
                    prt.println("Error for TLE data set " + str(count) + ", line 2, near the string")
                    prt.println(strIn)
                    break
                else:
                    line2 = strIn
 
                obj = __RawTLE()
                obj.sName = name
                obj.bNamed = named
                obj.sLine1 = line1
                obj.sLine2 = line2
                ASTTLE._TLEdb.append(obj)
 
            br.close()                # don't really have to do this ...
            
    except __TLEException:
        ASTMsg.errMsg("The specified file does not appear to\n" +
                      "be a valid TLE data file - try again.", "Invalid TLE Data File")
        br.close()                  # don't really have to do this ...
        return False                

    ASTTLE._TLEsFilename = filename
    ASTTLE._TLEsLoaded = True
    return ASTTLE._TLEsLoaded



#=========== Main entry point ===============
if __name__ == '__main__':
    pass  
