"""
Functions to display messages to the user.
 
Functions are used rather than creating a class so that a class instance does
not have to be created prior to usage. These functions simplify translating the
code to other languages, and they help enforce consistency in usage and style.
 
These message functions display a message in a dialog box that is placed on
top of a parent frame that was set by the setParentFrame(parent) function, which
should be invoked when the top level GUI window is created. The functions in
this module allow a calling routine to explicitly define a parent frame if the
top level application window is not appropriate.

======================= IMPORTANT NOTE ===========================================
To work properly, there should only be **one** top level application window and it
**must** set the default parent frame to be used via setParentFrame. If multiple
top level application windows are created and each calls setParentFrame, then the
last one to call setParentFrame wins. If for some reason multiple top level
application windows are needed, then either a parent frame should always be
passed in to the various message functions, or the calling routine should
call getParentFrame and save its value, call setParentFrame to set a new parent
frame, invoke whatever message routines are needed, and then call setParentFrame
again to restore the previous parent frame setting. While this is messy, it is
easier in most cases than creating a class instance and besides, it is very
unlikely that an application will require multiple top level application windows.
======================= IMPORTANT NOTE ===========================================
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import sys
import tkinter.messagebox as mbox

from ASTUtils.ASTMisc import SHOW_ERRORS
 
# Save a reference to the application's parent so that we can put messages on top of the
# parent frame without having to pass in the parent frame each time. This should
# ***always*** be the top level application window
__parentFrame = None



def getParentFrame():
    """
    Returns the currently set parent frame for the rare cases where multiple
    top level application windows may be needed. Having multiple top level
    application windows is strongly discouraged!

    :return: the currently set parent frame
    """
    return __parentFrame
 


def setParentFrame(parent):
    """
    Saves a reference to the caller's parent frame so error messages are placed
    on top of the parent application's window. This **must** be called, typically
    from the code that creates the top level GUI, before any functions in this
    module are invoked.

    :param tkwidget parent: parent frame for the main application
    """
    global __parentFrame
     
    __parentFrame = parent
         


#================================================================================
# Define some general functions for displaying various types of error messages.
# Messages are placed on top of the parent frame that is passed in. If no parent
# frame is passed in, __parentFrame is used instead, which should be set to the
# main application frame.
#================================================================================

def abortMsg(msg,frame=None):
    """
    Asks the user if they wish to abort
    
    :param str msg: text message to display as a prompt
    :param tkwidget frame: parent frame on which to place the message
    :return: true if user clicked on "Yes", else false
    """
    return pleaseConfirm(msg +"\n"+"Abort processing?","",frame)



def criticalErrMsg(msg,abort=False):
    """
    Display a critical error message
    
    :param str msg: text of the message to display
    :param bool abort: whether to abort after message is displayed. Default is no.
    """
    mbox.showerror("Severe Error", "Severe Error: " + msg)
    if abort:
        sys.exit()
    


def errMsg(msg,title,errFlag=SHOW_ERRORS,frame=None):
    """
    Conditionally displays an error message, but only if errFlag is SHOW_ERRORS.
    
    :param str msg: text of the error message to be displayed
    :param str title: text for the dialog window's title
    :param bool errFlag: display error message only if flag is ASTMisc.SHOW_ERRORS
    :param tkwidget frame: parent frame on which to show the message
    """
    if (errFlag != SHOW_ERRORS):
        return

    if frame == None:
        pf = __parentFrame
    else:
        pf = frame
        
    mbox.showwarning(title,msg,parent=pf)
 


def infoMsg(msg,title=" "):
    """
    Display a simple informational message.
    
    :param str msg: text message to display
    :param str title: title for the message window
    """
    mbox.showinfo(title,msg)  
    


def pleaseConfirm(msg,title,frame=None):
    """
    Asks the user to confirm something (such as whether to exit).

    :param str msg: text of the message to display
    :param str title: text for the dialog window's title
    :param tkwidget frame: parent frame on which to place the message
    :return: True if yes was selected, else False
    """
    if frame == None:
        pf = __parentFrame
    else:
        pf = frame
     
    return mbox.askyesno(title,msg,icon="question",parent=pf)
 


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
