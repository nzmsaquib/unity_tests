"""
Define orbital elements

This module provides the ability to load orbital elements
from a data file and to store them in an accessible
structure. This is done so that changing from one epoch
to another only requires that the orbital elements
be loaded from a different data file. The default
data file is J2000.dat under the data files directory.

An orbital elements data file **MUST** be in the format
specifically designed for this book. The format can be
gleaned by opening the J2000.dat data file and examining
its contents. The format is straightforward and is
documented in comments within the data file itself.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

#================IMPORTANT NOTE===============================
# Only one set of orbital elements can be in effect at a
# time. An initial set is read in at initialization, after
# which loading a new set will overwrite the prior elements.
#================IMPORTANT NOTE===============================

import os, io, math

import ASTUtils.ASTDate as ASTDate
import ASTUtils.ASTFileIO as ASTFileIO
import ASTUtils.ASTKepler as ASTKepler
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import ABORT_PROG, HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle

#===============================================================
# Define some globals that relate to orbital elements
#===============================================================

# Define the Earth's standard gravitational parameter. This is only
# used in the satellites chapter for a bit of extra accuracy when dealing with
# satellites. For other chapters, the value is taken from the orbital
# elements data file.
muEARTH = 398600.4418
  
# Define radius of the Earth. We could read this from a data file, but this is easier.
EARTH_RADIUS = 6378.135



#===============================================================
# Define some globals that are indices into a result list that
# is returned by various functions below
#===============================================================

# The following are indices into a double[] result returned by calcSunEclipticCoordinates
# for calculating Solar data.
SUN_ECLLAT = 0                      # Solar ecliptic latitude
SUN_ECLLON = 1                      # Solar ecliptic longitude
SUN_MEANANOM = 2                    # Solar mean anomaly
SUN_TRUEANOM = 3                    # Solar true anomaly

# The following are indices into a double[] result returned by calcMoonEclipticCoordinates
# for calculating Lunar data.
MOON_ECLLAT = 0                     # Lunar ecliptic latitude
MOON_ECLLON = 1                     # Lunar ecliptic longitude
MOON_TRUELON = 2                    # Lunar true ecliptic longitude
MOON_CORRLON = 3                    # Corrected lon of the asc. node      
MOON_MEANANOM_CORR = 4              # Mean anomaly correction for the Moon
MOON_MEANANOM = 5                   # Lunar mean anomaly
MOON_TRUEANOM = 6                   # Lunar true anomaly
 
# The following are indices into a double[] result returned from calcObjEclipticCoord
# for calculating data about the planets or other Solar System objects
OBJ_ECLLAT = 0                      # object's ecliptic latitude
OBJ_ECLLON = 1                      # object's ecliptic longitude
OBJ_HELIOLAT = 2                    # object's heliocentric latitude
OBJ_RADIUSVECT = 3                  # object's radius vector length
EARTH_HELIOLAT = 4                  # Earth's heliocentric latitude
EARTH_RADIUSVECT = 5                # Earth's radius vector length
     
# These indices indicate the results of the calcRiseSetTimes method
RISE_SET_FLAG = 0                   # whether object rise/set (1.0 = yes, 0.0 = no)
RISE_TIME = 1                       # LST for rising time
SET_TIME = 2                        # LST for setting time



class __ConstException(Exception):
    """Define an exception for errors in reading an orbital elements data file"""
    pass
    


class ASTOrbits():
    """Define a class as a container for various data items about orbits"""
    #==================================================================
    # Class variables
    #
    # Note that we use a single underscore to name these class
    # variables so that they can be accessed by functions below that
    # are not class methods, yet indicate that they should be
    # considered as private to this module. Using a double underscore
    # to name them will not work because they would be private to the
    # class and not accessible outside the class.
    #==================================================================
    # we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
    _prt = None
    
    _orbitalElementsInitialized = False             # avoid initializing more than once
    _orbitalElementsLoaded = False                  # whether orbital elements have been loaded
    
    # The following two globals in are used to pass error results back from
    # the __readElement method. This is needed because Python functions can only return a
    # single value (or an object or a list) and input parameters to a function cannot be modified.
    _globalReadError = False
    _globalDataError = False

    # Name of the data file containing the orbital elements data and its location,
    # and defaults in case the user must find it.
    _dataFilename = "J2000.dat"        
    _fullFilename = os.path.realpath(os.path.join("..","ASTUtils",_dataFilename))
    _fileExtFilter = [("Orbital Elements File","*.dat"),("All Files",".*")]
    _fallbackDir = os.path.realpath(os.path.join("..","..","Ast Data Files"))
    
    _NA_VALUE = -1.0        # value to use when something is N/A as an orbital element

    _epochDate = 2000.0     # epoch as a date, such as 2000.0 or 1980.0
    _epochJD = 1.0          # epoch as a Julian day number
    
    # The Earth, Sun, and Moon are mandatory and require special handling,
    # so save their index into the orbital elements db
    _idxSun = -1
    _idxMoon = -1
    _idxEarth = -1
    
    # Database of the currently loaded orbital elements
    _orbElementsDB = []
    
    def __init__(self):
        pass
    


class __OrbElemObj():
    """Define a private class and database for the orbital elements."""
    #===============================================================
    # Class instance variables
    #===============================================================
    # sName                the name of the object to which this orbital element data applies
    # bInferior            true if this is an object whose orbit is between the Earth and Sun
    # dPeriod              orbital period in tropical years
    # dMass                object's mass relative to the Earth
    # dRadius              object's radius in km
    # dDay                 length of day relative to Earth
    # dEccentricity        orbital eccentricity
    # dSemiMajAxisAU       length of the semi-major axis in AUs
    # dSemiMajAxisKM       length of the semi-major axis in km (Sun and Moon only)
    # dAngDiamArcSec       angular diameter in arcseconds
    # dAngDiamDeg          angular diameter in degrees (Sun and Moon only)
    # dmV                  visual magnitude
    # dGravParm            gravitational parameter in km^3/s^2
    # dInclination         orbital inclination in degrees
    # dLonAtEpoch          longitude at the epoch in degrees
    # dLonAtPeri           longitude at perihelion in degrees
    # dLonAscNode          longitude of the ascending node
    
    def __init__(self):
        """Constructor for __OrbElemObj"""
        self.sName = ""
        self.bInferior = True
        self.dPeriod = 0
        self.dMass = 0
        self.dRadius = 0
        self.dDay = 0
        self.dEccentricity = 0
        self.dSemiMajAxisAU = 0
        self.dSemiMajAxisKM = 0
        self.dAngDiamArcSec = 0
        self.dAngDiamDeg = 0
        self.dmV = 0
        self.dGravParm = 0
        self.dInclination = 0
        self.dLonAtEpoch = 0
        self.dLonAtPeri = 0
        self.dLonAscNode = 0
        


#===================================================================
# Define 'get' accessors for the instance data for this class and
# for the elements in the orbital elements database.
#
# Warning: No check is made to be sure idx is valid. Also, note that
# if an item is N/A (e.g., angular diameter of the Earth), the
# value NA_VALUE is returned.
#===================================================================

def getNumOEDBObjs():
    return len(ASTOrbits._orbElementsDB)
def getOEEpochDate():
    return ASTOrbits._epochDate
def getOEEpochJD():
    return ASTOrbits._epochJD
def getOEDBSunIndex():
    return ASTOrbits._idxSun
def getOEDBMoonIndex():
    return ASTOrbits._idxMoon
def getOEDBEarthIndex():
    return ASTOrbits._idxEarth
def getOEObjName(idx):
    return ASTOrbits._orbElementsDB[idx].sName
def getOEObjInferior(idx):
    return ASTOrbits._orbElementsDB[idx].bInferior
def getOEObjPeriod(idx):
    return ASTOrbits._orbElementsDB[idx].dPeriod
def getOEObjMass(idx):
    return ASTOrbits._orbElementsDB[idx].dMass
def getOEObjRadius(idx):
    return ASTOrbits._orbElementsDB[idx].dRadius
def getOEObjDay(idx):
    return ASTOrbits._orbElementsDB[idx].dDay
def getOEObjEccentricity(idx):
    return ASTOrbits._orbElementsDB[idx].dEccentricity
def getOEObjSemiMajAxisAU(idx):
    return ASTOrbits._orbElementsDB[idx].dSemiMajAxisAU
def getOEObjSemiMajAxisKM(idx):
    return ASTOrbits._orbElementsDB[idx].dSemiMajAxisKM
def getOEObjAngDiamArcSec(idx):
    return ASTOrbits._orbElementsDB[idx].dAngDiamArcSec
def getOEObjAngDiamDeg(idx):
    return ASTOrbits._orbElementsDB[idx].dAngDiamDeg
def getOEObjmV(idx):
    return ASTOrbits._orbElementsDB[idx].dmV
def getOEObjGravParm(idx):
    return ASTOrbits._orbElementsDB[idx].dGravParm
def getOEObjInclination(idx):
    return ASTOrbits._orbElementsDB[idx].dInclination
def getOEObjLonAtEpoch(idx):
    return ASTOrbits._orbElementsDB[idx].dLonAtEpoch
def getOEObjLonAtPeri(idx):
    return ASTOrbits._orbElementsDB[idx].dLonAtPeri
def getOEObjLonAscNode(idx):
    return ASTOrbits._orbElementsDB[idx].dLonAscNode
        


#=================================================================
# Functions for performing various orbital calculations
#=================================================================

def calcEarthRVLength(month,day,year,UT,solvetrueAnomaly,termCriteria):
    """
    Calculate the Earth's radius vector length (i.e., distance to the Sun)
    at the stated time and date.

    :param int month: month at which Earth's distance is desired
    :param int day: day at which Earth's distance is desired
    :param int year: year at which Earth's distance is desired
    :param float UT: time (UT) at which distance is desired
    :param TrueAnomalyType solvetrueAnomaly: how to compute the true anomaly
    :param float termCriteria: termination criteria if Kepler's equation is solved
    :return: the Earth's radius vector length (R_e)
    """
    R_e = 1.0
    obsDate = ASTDate.ASTDate()
     
    if not (ASTOrbits._orbitalElementsLoaded):
        ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
        return R_e
    
    obsDate.setMonth(month)
    obsDate.setYear(year)
    obsDate.setiDay(day)
    Ecc_e = getOEObjEccentricity(ASTOrbits._idxEarth)
    
    JDe = getOEEpochJD()
    JD = ASTDate.dateToJD_mdy(obsDate.getMonth(), obsDate.getdDay() + (UT / 24.0), obsDate.getYear())
    De = JD - JDe
    
    # Calculate the Earth's mean and true anomalies
    M_e = (360 * De) / (365.242191 * getOEObjPeriod(ASTOrbits._idxEarth)) + getOEObjLonAtEpoch(ASTOrbits._idxEarth) - getOEObjLonAtPeri(ASTOrbits._idxEarth)
    M_e = ASTMath.xMOD(M_e, 360.0)
    
    # Find the Earth's true anomaly from equation of center or Kepler's equation
    if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER):
        Ec_e = (360.0 / math.pi) * Ecc_e * ASTMath.SIN_D(M_e)
        v_e = M_e + Ec_e
    else:
        if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
            tmpKepler = ASTKepler.calcSimpleKepler(M_e, Ecc_e, termCriteria)
            Ea_e = tmpKepler[0]
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(M_e, Ecc_e, termCriteria)
            Ea_e = tmpKepler[0]
    
        v_e = (1 + Ecc_e) / (1 - Ecc_e)
        v_e = math.sqrt(v_e) * ASTMath.TAN_D(Ea_e / 2.0)
        v_e = 2.0 * ASTMath.INVTAN_D(v_e)

    
    # Calculate the Earth's radius vector length (R_e)
    R_e = getOEObjSemiMajAxisAU(ASTOrbits._idxEarth) * (1 - Ecc_e * Ecc_e)
    R_e = R_e / (1 + Ecc_e * ASTMath.COS_D(v_e))
    
    return R_e



def calcMoonEclipticCoord(month,day,year,UT,solvetrueAnomaly,termCriteria):
    """
    Calculate the Moon's ecliptic coordinates using the
    currently loaded orbital elements.

    :param int month: month at which Moon's position is desired
    :param int day: day at which Moon's position is desired
    :param int year: year at which Moon's position is desired
    :param float UT: time (UT) at which position is desired
    :param TrueAnomalyType solvetrueAnomaly: how to compute the true anomaly
    :param float termCriteria: termination criteria if Kepler's equation is solved
    :return: a 7 element array with Moon's ecliptic latitude (Bmoon), ecliptic longitude (Lmoon),
             true ecliptic longitude (Ltrue), corrected longitude of the ascending node (Omega_p),
             mean anomaly correction (Ca), mean anomaly (Mm), and true anomaly (Vmoon)
    """
    result = [0.0]*7
    obsDate = ASTDate.ASTDate()
     
    if not (ASTOrbits._orbitalElementsLoaded):
        ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
        return result

    obsDate.setMonth(month)
    obsDate.setYear(year)
    obsDate.setiDay(day)
    inclin = getOEObjInclination(ASTOrbits._idxMoon)
    
    # Adjust the time at which the Moon's position is required
    TT = UT + (63.8 / 3600.0)
    JDe = getOEEpochJD()
    JD = ASTDate.dateToJD_mdy(month, day + (TT / 24.0), year)
    De = JD - JDe

    # Get the Sun's position
    tmpResult = calcSunEclipticCoord(month, day, year, UT, solvetrueAnomaly, termCriteria)
    Lsun = tmpResult[SUN_ECLLON]
    Msun = tmpResult[SUN_MEANANOM]

    # Compute various corrections
    L_uncor = 13.176339686 * De + getOEObjLonAtEpoch(ASTOrbits._idxMoon)
    L_uncor = ASTMath.xMOD(L_uncor, 360.0)
    Omega = getOEObjLonAscNode(ASTOrbits._idxMoon) - 0.0529539 * De
    Omega = ASTMath.xMOD(Omega, 360.0)
    Mm = L_uncor - 0.1114041 * De - getOEObjLonAtPeri(ASTOrbits._idxMoon)
    Mm = ASTMath.xMOD(Mm, 360.0)
    Ae = 0.1858 * ASTMath.SIN_D(Msun)
    Ev = 1.2739 * ASTMath.SIN_D(2 * (L_uncor - Lsun) - Mm)
    Ca = Mm + Ev - Ae - 0.37 * ASTMath.SIN_D(Msun)
    
    # Calculate Moon's true anomaly
    Vmoon = 6.2886 * ASTMath.SIN_D(Ca) + 0.214 * ASTMath.SIN_D(2 * Ca)
    
    L_p = L_uncor + Ev + Vmoon - Ae
    V = 0.6583 * ASTMath.SIN_D(2 * (L_p - Lsun))
    Ltrue = L_p + V
    Omega_p = Omega - 0.16 * ASTMath.SIN_D(Msun)
    y = ASTMath.SIN_D(Ltrue - Omega_p) * ASTMath.COS_D(inclin)
    x = ASTMath.COS_D(Ltrue - Omega_p)
    dT = ASTMath.INVTAN_D(y / x)
    x = ASTMath.quadAdjust(y, x)
    dT = dT + x
    
    Lmoon = Omega_p + dT
    if (Lmoon > 360):
        Lmoon = Lmoon - 360
    Bmoon = ASTMath.SIN_D(Ltrue - Omega_p) * ASTMath.SIN_D(inclin)
    Bmoon = ASTMath.INVSIN_D(Bmoon)
     
    result[MOON_ECLLAT] = Bmoon
    result[MOON_ECLLON] = Lmoon
    result[MOON_TRUELON] = Ltrue
    result[MOON_CORRLON] = Omega_p
    result[MOON_MEANANOM_CORR] = Ca
    result[MOON_MEANANOM] = Mm
    result[MOON_TRUEANOM] = Vmoon
    
    return result



def calcObjDistToEarth(Re,Le,Rp,Lp):
    """
    Calculate the distance (in AUs) from an object to the Earth.
    
    :param float Re: Earth's radius vector in AUs
    :param float Le: Earth's heliocentric longitude
    :param float Rp: object's radius vector in AUs
    :param float Lp: object's heliocentric longitude
    :return: distance from Earth to object in AUs.
    """
    return math.sqrt(Re * Re + Rp * Rp - 2 * Re * Rp * ASTMath.COS_D(Lp - Le))



def calcObjEclipticCoord(month,day,year,UT,idx,solvetrueAnomaly,termCriteria):
    """
    Calculate an object's ecliptic coordinates using the
    currently loaded orbital elements. The object can be anything
    in the orbital elements file except the Sun, Moon, or Earth.

    :param int month: month at which object's position is desired
    :param int day: day at which object's position is desired
    :param int year: year at which object's position is desired
    :param float UT: time (UT) at which position is desired
    :param int idx: index into the orbital elements database
                    for the object whose position is desired
    :param TrueAnomalyType solvetrueAnomaly: how to compute the true anomaly for the Sun, Earth, and object
    :param float termCriteria: termination criteria if Kepler's equation is solved
    :return: a 6 element array with the object's ecliptic latitude (Lat_p), object's 
             ecliptic longitude (Lon_p), object's heliocentric latitude (L_p), object's
             radius vector length (R_p), Earth's heliocentric latitude (L_e), and Earth's
             radius vector length (R_e)
    """
    # Caution: No check is made to be sure idx is valid.

    result = [0.0]*6
    obsDate = ASTDate.ASTDate()
     
    if not (ASTOrbits._orbitalElementsLoaded):
        ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
        return result
    
    obsDate.setMonth(month)
    obsDate.setYear(year)
    obsDate.setiDay(day)
    Ecc_p = getOEObjEccentricity(idx)
    inclin_p = getOEObjInclination(idx)
    Ecc_e = getOEObjEccentricity(ASTOrbits._idxEarth)
    inclin_e = getOEObjInclination(ASTOrbits._idxEarth)
    
    JDe = getOEEpochJD()
    JD = ASTDate.dateToJD_mdy(obsDate.getMonth(), obsDate.getdDay() + (UT / 24.0), obsDate.getYear())
    De = JD - JDe
    
    # Calculate the object's mean and true anomalies
    M_p = (360 * De) / (365.242191 * getOEObjPeriod(idx)) + getOEObjLonAtEpoch(idx) - getOEObjLonAtPeri(idx)
    M_p = ASTMath.xMOD(M_p, 360.0)
    
    # Find the true anomaly from equation of center or Kepler's equation
    if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER):
        Ec_p = (360.0 / math.pi) * Ecc_p * ASTMath.SIN_D(M_p)
        v_p = M_p + Ec_p
    else:
        if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
            tmpKepler = ASTKepler.calcSimpleKepler(M_p, Ecc_p, termCriteria)
            Ea_p = tmpKepler[0]
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(M_p, Ecc_p, termCriteria)
            Ea_p = tmpKepler[0]
        v_p = (1 + Ecc_p) / (1 - Ecc_p)
        v_p = math.sqrt(v_p) * ASTMath.TAN_D(Ea_p / 2.0)
        v_p = 2.0 * ASTMath.INVTAN_D(v_p)
    
    # Calculate the object's heliocentric ecliptic coordinates (L_p, H_p)
    # and radius vector length (R_p)
    L_p = v_p + getOEObjLonAtPeri(idx)
    L_p = ASTMath.xMOD(L_p, 360.0)
    H_p = ASTMath.SIN_D(L_p - getOEObjLonAscNode(idx)) * ASTMath.SIN_D(inclin_p)
    H_p = ASTMath.INVSIN_D(H_p)
    H_p = ASTMath.xMOD(H_p, 360.0)
    
    R_p = getOEObjSemiMajAxisAU(idx) * (1 - Ecc_p * Ecc_p)
    R_p = R_p / (1 + Ecc_p * ASTMath.COS_D(v_p))
    
    # Repeat the steps just done for the object for the Earth
    
    # Calculate the Earth's mean and true anomalies
    M_e = (360 * De) / (365.242191 * getOEObjPeriod(ASTOrbits._idxEarth)) + getOEObjLonAtEpoch(ASTOrbits._idxEarth) - getOEObjLonAtPeri(ASTOrbits._idxEarth)
    M_e = ASTMath.xMOD(M_e, 360.0)
    
    # Find the Earth's true anomaly from equation of center or Kepler's equation
    if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER):
        Ec_e = (360.0 / math.pi) * Ecc_e * ASTMath.SIN_D(M_e)
        v_e = M_e + Ec_e
    else:
        if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
            tmpKepler = ASTKepler.calcSimpleKepler(M_e, Ecc_e, termCriteria)
            Ea_e = tmpKepler[0]
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(M_e, Ecc_e, termCriteria)
            Ea_e = tmpKepler[0]
                
        v_e = (1 + Ecc_e) / (1 - Ecc_e)
        v_e = math.sqrt(v_e) * ASTMath.TAN_D(Ea_e / 2.0)
        v_e = 2.0 * ASTMath.INVTAN_D(v_e)
    
    # Calculate the Earth's heliocentric ecliptic coordinates (L_e, H_e) and radius vector length (R_e)
    L_e = v_e + getOEObjLonAtPeri(ASTOrbits._idxEarth)
    L_e = ASTMath.xMOD(L_e, 360.0)
    H_e = ASTMath.SIN_D(L_e - getOEObjLonAscNode(ASTOrbits._idxEarth)) * ASTMath.SIN_D(inclin_e)
    H_e = ASTMath.INVSIN_D(H_e)
    H_e = ASTMath.xMOD(H_e, 360.0)
    
    R_e = getOEObjSemiMajAxisAU(ASTOrbits._idxEarth) * (1 - Ecc_e * Ecc_e)
    R_e = R_e / (1 + Ecc_e * ASTMath.COS_D(v_e))
    
    # Given the heliocentric location and radius vector length for both the Earth
    # and the object, project the object's location onto the ecliptic plane
    # with respect to the Earth
    y = ASTMath.SIN_D(L_p - getOEObjLonAscNode(idx)) * ASTMath.COS_D(inclin_p)
    x = ASTMath.COS_D(L_p - getOEObjLonAscNode(idx))
    dT = ASTMath.INVTAN_D(y / x)
    x = ASTMath.quadAdjust(y, x)
    dT = dT + x
    
    Lp_p = getOEObjLonAscNode(idx) + dT
    Lp_p = ASTMath.xMOD(Lp_p, 360.0)
    
    # See if this is an inferior or superior object and compute accordingly
    if (getOEObjInferior(idx)):
        y = R_p * ASTMath.COS_D(H_p) * ASTMath.SIN_D(L_e - Lp_p)
        x = R_e - R_p * ASTMath.COS_D(H_p) * ASTMath.COS_D(L_e - Lp_p)
        dT = ASTMath.INVTAN_D(y / x)
        x = ASTMath.quadAdjust(y, x)
        dT = dT + x
    
        Lon_p = 180 + L_e + dT
        Lon_p = ASTMath.xMOD(Lon_p, 360.0)
    else:
        y = R_e * ASTMath.SIN_D(Lp_p - L_e)
        x = R_p * ASTMath.COS_D(H_p) - R_e * ASTMath.COS_D(L_e - Lp_p)
        dT = ASTMath.INVTAN_D(y / x)
        x = ASTMath.quadAdjust(y, x)
        dT = dT + x
    
        Lon_p = Lp_p + dT
        Lon_p = ASTMath.xMOD(Lon_p, 360.0)
   
    Lat_p = R_p * ASTMath.COS_D(H_p) * ASTMath.TAN_D(H_p) * ASTMath.SIN_D(Lon_p - Lp_p)
    Lat_p = Lat_p / (R_e * ASTMath.SIN_D(Lp_p - L_e))
    Lat_p = ASTMath.INVTAN_D(Lat_p)
     
    result[OBJ_ECLLAT] = Lat_p
    result[OBJ_ECLLON] = Lon_p
    result[OBJ_HELIOLAT] = L_p
    result[OBJ_RADIUSVECT] = R_p
    result[EARTH_HELIOLAT] = L_e
    result[EARTH_RADIUSVECT] = R_e
    
    return result



def calcRiseSetTimes(RA,Decl,Lat):
    """
    Calculate the LST rising and setting times for a given
    equatorial coordinate.

    :param float RA: right ascension
    :param float Decl: eclination
    :param float Lat: observer's latitude
    :return: a 3 element array with flag indicating
             whether the object rise or sets (1.0 = rise/set,
            -1.0 = doesn't rise/set), LST rising time, and
            LST setting time
    """
    result = [0.0]*3
    TRUE = 1.0
    FALSE = -1.0
     
    result[RISE_SET_FLAG] = TRUE
    result[RISE_TIME] = 0.0                # LST Rise time
    result[SET_TIME] = 0.0                 # LST set time
    
    Ar = ASTMath.SIN_D(Decl) / ASTMath.COS_D(Lat)
    if (abs(Ar) > 1):
        result[RISE_SET_FLAG] = FALSE
        return result
    
    R = ASTMath.INVCOS_D(Ar)            # rising azimuth (isn't needed)  #@UnusedVariable
    S = 360.0 - R                       # setting azimuth (isn't needed) #@UnusedVariable
    H1 = ASTMath.TAN_D(Lat) * ASTMath.TAN_D(Decl)
    if (abs(H1) > 1):
        result[RISE_SET_FLAG] = FALSE
        return result
    
    H2 = ASTMath.INVCOS_D(-H1) / 15.0
    result[RISE_TIME] = 24 + RA - H2
    if (result[RISE_TIME] > 24):
        result[RISE_TIME] = result[RISE_TIME] - 24.0
    result[SET_TIME] = RA + H2
    if (result[SET_TIME] > 24):
        result[SET_TIME] = result[SET_TIME] - 24.0
     
    return result



def calcSunEclipticCoord(month,day,year,UT,solvetrueAnomaly,termCriteria):
    """
    Calculate the Sun's ecliptic coordinates using the
    currently loaded orbital elements.

    :param int month: month at which Sun's position is desired
    :param int day: day at which Sun's position is desired
    :param int year: year at which Sun's position is desired
    :param float UT: time (UT) at which position is desired
    :param TrueAnomalyType solvetrueAnomaly: how to compute the true anomaly
    :param float termCriteria: termination criteria if Kepler's equation is solved
    :return: a 4 element array with Sun's ecliptic latitude, ecliptic longitude,
             mean anomaly, and true anomaly as calculated by this method
    """
    result = [0.0]*4
    Vsun = 0.0
    obsDate = ASTDate.ASTDate()

    if not (ASTOrbits._orbitalElementsLoaded):
        ASTMsg.errMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
        return result
    
    Ecc = getOEObjEccentricity(ASTOrbits._idxSun)
    obsDate.setMonth(month)
    obsDate.setYear(year)
    obsDate.setiDay(day)
    
    JDe = getOEEpochJD()
    JD = ASTDate.dateToJD_mdy(month, day + (UT / 24.0), year)
    De = JD - JDe
    
    Msun = ((360.0 * De) / 365.242191) + getOEObjLonAtEpoch(ASTOrbits._idxSun) - getOEObjLonAtPeri(ASTOrbits._idxSun)
    Msun = ASTMath.xMOD(Msun, 360.0)

    # Find the true anomaly from equation of center or Kepler's equation
    if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_EQ_OF_CENTER):
        Ec = (360.0 / math.pi) * Ecc * ASTMath.SIN_D(Msun)
        Vsun = Ec + Msun
    else:
        if (solvetrueAnomaly == ASTKepler.TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER):
            tmpKepler = ASTKepler.calcSimpleKepler(Msun, Ecc, termCriteria)
            Ea = tmpKepler[0]
        else:
            tmpKepler = ASTKepler.calcNewtonKepler(Msun, Ecc, termCriteria)
            Ea = tmpKepler[0]
        Vsun = (1 + Ecc) / (1 - Ecc)
        Vsun = math.sqrt(Vsun) * ASTMath.TAN_D(Ea / 2.0)
        Vsun = 2.0 * ASTMath.INVTAN_D(Vsun)
    
    Vsun = ASTMath.xMOD(Vsun, 360.0)
    Lsun = Vsun + getOEObjLonAtPeri(ASTOrbits._idxSun)
    if (Lsun > 360.0):
        Lsun = Lsun - 360.0
    
    result[SUN_ECLLAT] = 0.0
    result[SUN_ECLLON] = Lsun
    result[SUN_MEANANOM] = Msun
    result[SUN_TRUEANOM] = Vsun
    return result



def displayAllOrbitalElements():
    """
    Displays a list in the user's scrollable output
    text area of all the orbital elements for all
    of the objects in the database.
    """
    prt = ASTOrbits._prt

    if not (isOrbElementsDBLoaded()):
        ASTMsg.errMsg("No Orbital Elements have been loaded", "No Orbital Elements")
        return
    
    prt.setBoldFont(True)
    prt.println("Currently Loaded Orbital Elements", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    prt.println("Orbital Elements are for Epoch " + ASTStr.strFormat(ASTStyle.EPOCHFORMAT,ASTOrbits._epochDate) +
                " (Julian Day Number " + ASTStr.strFormat(ASTStyle.JDFormat,ASTOrbits._epochJD) + ")")
    prt.println()
    
    for i in range(0,getNumOEDBObjs()):
        prt.println("="*75)      
        displayObjOrbElements(i)
        prt.println()



def displayObjOrbElements(idx):
    """
    Display the orbital elements for an object.
    
    :param int idx: index into OE db for the desired object
    """
    prt = ASTOrbits._prt

    st = "Orbital Elements and Other Data for "
    
    if not (isOrbElementsDBLoaded()):
        return
    if ((idx < 0) or (idx > getNumOEDBObjs())):
        return
    
    if ((idx == getOEDBEarthIndex()) or (idx == getOEDBMoonIndex()) or (idx == getOEDBSunIndex())):
        st = st + "the "
    
    prt.setBoldFont(True)
    prt.println(st + ASTOrbits._orbElementsDB[idx].sName + " (Epoch: " + str(getOEEpochDate()) + ")", CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    if ((idx != getOEDBEarthIndex()) and (idx != getOEDBMoonIndex()) and (idx != getOEDBSunIndex())):
        prt.printnoln("Object's orbit is ")
        if not (getOEObjInferior(idx)):
            prt.printnoln("not ")
        prt.println("between the Earth and the Sun")
        prt.println()

    prt.println("Mass: " + ASTStr.insertCommas(getOEObjMass(idx)) + " times the mass of the Earth")
    prt.println("Gravitational Parameter: " + ASTStr.insertCommas(getOEObjGravParm(idx)) + " km^3 per s^2")
    prt.println("Radius: " + ASTStr.insertCommas(getOEObjRadius(idx)) + " km")
    prt.println("Day: " + str(getOEObjDay(idx)) + " Earth days")
    if (idx == getOEDBMoonIndex()):
        prt.println("Visual Magnitude (Full Moon): " + str(getOEObjmV(idx)) + " when 1 AU from the Earth")
    elif (idx != getOEDBEarthIndex()):
        prt.println("Visual Magnitude: " + str(getOEObjmV(idx)) + " when 1 AU from the Earth")

    if ((idx != getOEDBEarthIndex()) and (idx != getOEDBMoonIndex())):
        prt.println("Angular Diameter: " + ASTStr.insertCommas(getOEObjAngDiamArcSec(idx)) +
                    " arc seconds when length of the semi-major axis away")
    if ((idx == getOEDBSunIndex()) or (idx == getOEDBMoonIndex())):
        prt.println("Angular Diameter: " + str(getOEObjAngDiamDeg(idx)) +
                    " degrees when length of the semi-major axis away")
    
    prt.println()
    
    if ((idx != getOEDBSunIndex()) and (idx != getOEDBMoonIndex())):
        prt.println("Orbital Period: " + str(getOEObjPeriod(idx)) + " tropical years")
    prt.println("Orbital Inclination: " + str(getOEObjInclination(idx)) + " degrees")
    prt.println("Orbital Eccentricity: " + str(getOEObjEccentricity(idx)))
    prt.printnoln("Length of Orbital Semi-Major Axis: " + ASTStr.insertCommas(getOEObjSemiMajAxisAU(idx)) + " AUs")
    if ((idx == getOEDBSunIndex()) or (idx == getOEDBMoonIndex())):
        prt.printnoln(" (which is approximately " + ASTStr.insertCommas(getOEObjSemiMajAxisKM(idx)) + " km)")     
    prt.println()
    prt.println("Longitude at the Epoch: " + str(getOEObjLonAtEpoch(idx)) + " degrees")
    prt.printnoln("Longitude at ")
    if (idx == getOEDBMoonIndex()):
        prt.printnoln("Perigee")
    else:
        prt.printnoln("Perihelion")
    prt.println(": " + str(getOEObjLonAtPeri(idx)) + " degrees")
    if (idx != getOEDBSunIndex()):
        prt.println("Longitude of the Ascending Node: " + str(getOEObjLonAscNode(idx)) + " degrees")



def findOrbElementObjIndex(name):
    """
    Searches the orbital elements database and returns an index into the
    database for the requested object. The search performed is **not**
    case sensitive, but an exact match, ignoring white space, is required.

    :param str name: name of the object to find
    :return: if successful, returns an index into the orbital elements database
             for the object requested. if unsuccessful, -1 is returned. Note that
             this assumes 0-based indexing!
    """
    if not (isOrbElementsDBLoaded()):
        return -1
    
    for i in range(0,getNumOEDBObjs()):
        if (ASTStr.compareIgnoreCase(name,getOEObjName(i))):
            return i
        
    return -1



def getOEDBFileToOpen():
    """
    Puts up a browser window and gets an orbital elements filename.
    
    :return: full name (file and path) of the orbital elements file   
             to open or null if no file is selected or
             file doesn't exist. This routine does **not**
             check to see if the file is a valid orbital elements data, 
             but it **does** check to see that the file exists.
    """
    fileToRead = ASTFileIO.getFileToRead("Select Orbital Elements file to Load ...",\
                                         ASTOrbits._fileExtFilter,ASTOrbits._fallbackDir)

    # See if the file exists
    if not (ASTFileIO.doesFileExist(fileToRead)):
        ASTMsg.errMsg("The Orbital Elements data file specified does not exist", \
                      "Orbital Elements File Does Not Exist")
        return None
    
    return fileToRead



def getSatOrbitType(e,inclin):
    """
    Determine an object's orbit type based on orbital
    inclination and eccentricity.

    :param float e: orbital eccentricity
    :param float inclin: orbital inclination in degrees
    :return: orbit type or -1 for an error
    """
    # Note: orbit types are as defined in the Satellites chapter.
      
    if (e < 0.0):             # negative eccentricity is impossible
        return -1
    if (e >= 1):              # we don't handle parabolic or hyperbolic orbits
        return -1
    
    if (ASTMath.isClose(e, 0.0)):                                              # circular orbits
        if (ASTMath.isClose(inclin, 0.0) or ASTMath.isClose(inclin, 180.0)):   # circular orbit in the equatorial plane
            return 1
        else:                                                                  # circular orbit inclined w.r.t. equatorial plane
            return 3
    if (e > 0.0):                                                              # non-circular orbits
        if (ASTMath.isClose(inclin, 0.0) or ASTMath.isClose(inclin, 180.0)):   # elliptical orbit in the equatorial plane
            return 2    
        else:                                                                  # elliptical orbit inclined w.r.t. equatorial plane
            return 4

    return -1                    # An error condition!



def initOrbitalElements(prtInstance):
    """
    Does a one-time initialization by reading in the default
    orbital elements data file, if it exists. if it doesn't, 
    require the user to find it. This function can only be
    called once and then the functions below should be used
    to load a new orbital elements data file under the calling
    application's control.

    :param ASTPrt prtInstance: instance of an output area
    """
    if (ASTOrbits._orbitalElementsInitialized):
        return                  # already initialized

    ASTOrbits._orbitalElementsLoaded = False
    ASTOrbits._globalReadError = False
    ASTOrbits._globalDataError = False

    if (prtInstance == None):
        ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ABORT_PROG)
    
    ASTOrbits._prt = prtInstance
    
    fileToRead = ASTOrbits._fullFilename
    
    # See if the file exists where it is expected. If not, ask the user to find it.
    if not (ASTFileIO.doesFileExist(fileToRead)):
        ASTMsg.errMsg("The Orbital Elements data file (" + ASTOrbits._dataFilename+") is missing.\n" +
                      "Please click 'OK' and then manually find an orbital elements data file",
                      "Missing Orbital Elements Data File")
        
        fileToRead = ASTFileIO.getFileToRead("Load Orbital Elements Data ...",ASTOrbits._fileExtFilter,
                                             ASTOrbits._fallbackDir,ASTOrbits._dataFilename)
        if (fileToRead == None) or (len(fileToRead) <= 0):
            ASTMsg.criticalErrMsg("An Orbital Elements data file is required but was\n" +
                                  "not found. Aborting program ...",ABORT_PROG)

    # If we get here, the orbital elements data file must exist
    # Note that we're using 'with open' so we don't have to explicitly close the file when we're done
    try:
        with io.open(fileToRead,"rt") as br:
            # Now that we've found an orbital elements data file, read in the data.
            if not (__readOrbitalElementsData(br)):
                raise __ConstException                               

        br.close()          # strictly speaking, this isn't necessary
        ASTOrbits._orbitalElementsLoaded = True
        ASTOrbits._orbitalElementsInitialized = True
        ASTOrbits._idxEarth = findOrbElementObjIndex("Earth")
        ASTOrbits._idxMoon = findOrbElementObjIndex("Moon")
        ASTOrbits._idxSun = findOrbElementObjIndex("Sun")
    
    except __ConstException:
        ASTMsg.criticalErrMsg("The file specified is not an Orbital Elements data\n" +
                              "file and could not be processed. Aborting program ...",ABORT_PROG)
        


def isOrbElementsDBLoaded():
    """
    Determine whether there is currently an orbital
    elements database loaded.

    :return: true if a db is loaded, else false
    """
    if ((ASTOrbits._idxSun < 0) or (ASTOrbits._idxMoon < 0) or (ASTOrbits._idxEarth < 0)):
        return False
    
    return ASTOrbits._orbitalElementsLoaded



def loadOEDB(filename):
    """
    Loads orbital elements database from disk.
    
    :param str filename: Full filename (including path) to load
    :return: true if successful, else false.
    """
    __clearOrbitalElementObjects()
           
    # Note that we're using 'with open' so we don't have to explicitly close the file when we're done
    try:
        with io.open(filename,"rt") as br:
            if not (__readOrbitalElementsData(br)):
                ASTMsg.errMsg("An Orbital Elements data file is required but could\n" + 
                              "not be processed. Try another data file.", "Invalid Orbital Elements Data File")
                br.close()
                return False

        br.close()          # strictly speaking, this isn't necessary
        ASTOrbits._orbitalElementsLoaded = True
        ASTOrbits._orbitalElementsInitialized = True
        ASTOrbits._idxEarth = findOrbElementObjIndex("Earth")
        ASTOrbits._idxMoon = findOrbElementObjIndex("Moon")
        ASTOrbits._idxSun = findOrbElementObjIndex("Sun")
    
    except __ConstException:
        ASTMsg.criticalErrMsg("The file specified is not an Orbital Elements data\n" +
                              "file and could not be processed. Aborting program ...",ABORT_PROG)
    
    return ASTOrbits._orbitalElementsLoaded



#=============================================================
# Private functions intended to be used only in this module.
#=============================================================

def __clearOrbitalElementObjects():
    """Clears all the currently loaded orbital elements."""
    ASTOrbits._idxSun = -1
    ASTOrbits._idxMoon = -1
    ASTOrbits._idxEarth = -1

    # Now delete all of the orbital element objects.
    # In some languages, we'd need to loop through the db
    # and delete objects individually.
    ASTOrbits._orbElementsDB.clear()
    del ASTOrbits._orbElementsDB[:]

    ASTOrbits._orbElementsDB = []
    ASTOrbits._orbitalElementsLoaded = False



def __readElement(br,tag):
    """
    This method reads the already opened orbital data file, looks for a
    specific tag, and returns its value. This only handles tags whose
    value is a real number.

    :param stream br: buffered stream to read from
    :param str tag: tag to look for
    :return: the real data value (or NA_VALUE) for the tag
    """
    # This function also sets the global flag globalReadError if a read error
    # occurs (e.g., tag not found) and the flag globalDataError if a
    # data error occurs (e.g., data format is invalid). This rather clunky
    # way of doing things is because Python functions can only return a single
    # value (or object or list) and cannot modify input parameters. Since these
    # globals are restricted to this module and only to a couple of functions,
    # the potential liability of this technique is limited.
    
    dResult = ASTOrbits._NA_VALUE
    
    strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, tag))
    if (strIn == None):
        ASTOrbits._globalReadError = True
        return ASTOrbits._NA_VALUE
    if (len(strIn) <= 0):
        return ASTOrbits._NA_VALUE
    
    rTmp = ASTReal.isValidReal(strIn,HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        dResult = rTmp.getRealValue()
    else:
        ASTOrbits._globalDataError = True
    
    return dResult



def __readOrbitalElementsData(br):
    """
    Reads an orbital elements data file to extract and validate its data.
    The data tags **must** be in the order specified by this code.

    :param stream br: buffered stream to read from
    :return: true if successfully processed the orbital elements data.
    """
    readError = False
    dataError = False
    i = -1 
    
    __clearOrbitalElementObjects()

    # Validate that the file is indeed a constellations data file
    strIn = ASTFileIO.readTillStr(br,"<OrbitalElements>",HIDE_ERRORS)
    if (strIn == None):
        return False
    if (len(strIn) <= 0):
        return False
    
    # Get the epoch before reading in the individual orbital elements
    strIn=ASTFileIO.getSimpleTaggedValue(br,"<Epoch>")
    if (strIn == None):
        strIn = ""
    rTmp = ASTReal.isValidReal(strIn,HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        ASTOrbits._epochDate = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Epoch Date at string [" + strIn + "]", "Invalid Epoch")
        return False

    strIn = ASTFileIO.getSimpleTaggedValue(br, "<JD>")
    if (strIn == None):
        strIn = ""
    rTmp = ASTReal.isValidReal(strIn,HIDE_ERRORS)
    if (rTmp.isValidRealObj()):
        ASTOrbits._epochJD = rTmp.getRealValue()
    else:
        ASTMsg.errMsg("Invalid Julian Day Number at string [" + strIn + "]", "Invalid JD")
        return False
    
    # Look for the beginning of the data section with the orbital elements,
    # then cycle through and extract each object and its orbital elements.
    strIn=ASTFileIO.readTillStr(br,"<Data>",HIDE_ERRORS)
    if ((strIn == None) or (len(strIn) <= 0)):
        return False
    
    strIn = ASTFileIO.getNonBlankLine(br)
    while (strIn != None):
        readError = False
        dataError = False

        strIn = ASTStr.removeWhitespace(strIn)
        str2 = strIn.lower()
        if (str2.find("</data>") >= 0):                # found end of data section
            break
        
        # Convert the current tag to be an object's name
        strIn = ASTStr.replaceStr(strIn, "<", "")
        objName = ASTStr.replaceStr(strIn, ">", "")
        i = i + 1
        
        obj = __OrbElemObj()
        obj.sName = objName
        
        if (ASTStr.compareIgnoreCase(objName,"Sun")):
            ASTOrbits._idxSun = i
        elif (ASTStr.compareIgnoreCase(objName,"Moon")):
            ASTOrbits._idxMoon = i
        elif (ASTStr.compareIgnoreCase(objName,"Earth")):
            ASTOrbits._idxEarth = i
        
        strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, "<Inferior>"))
        readError = readError or ((strIn == None) or (len(strIn) <= 0))
        obj.bInferior = ASTStr.compareIgnoreCase(strIn,"true")
        strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, "<Period>"))
        readError = readError or ((strIn == None) or (len(strIn) <= 0))
        if ((i == ASTOrbits._idxSun) or (i == ASTOrbits._idxMoon)):
            obj.dPeriod = ASTOrbits._NA_VALUE
        else:
            rTmp = ASTReal.isValidReal(strIn,HIDE_ERRORS)
            obj.dPeriod = rTmp.getRealValue()
            dataError = dataError or not (rTmp.isValidRealObj())
            
        obj.dMass = __readElement(br, "<Mass>")
        readError = readError or ASTOrbits._globalReadError 
        dataError = dataError or ASTOrbits._globalDataError
        obj.dRadius = __readElement(br, "<Radius>")
        readError = readError or ASTOrbits._globalReadError 
        dataError = dataError or ASTOrbits._globalDataError
        obj.dDay = __readElement(br, "<Day>")
        readError = readError or ASTOrbits._globalReadError 
        dataError = dataError or ASTOrbits._globalDataError
        obj.dEccentricity = __readElement(br, "<Eccentricity>")
        readError = readError or ASTOrbits._globalReadError 
        dataError = dataError or ASTOrbits._globalDataError
        obj.dSemiMajAxisAU = __readElement(br, "<SemiMajorAxisAU>")
        readError = readError or ASTOrbits._globalReadError 
        dataError = dataError or ASTOrbits._globalDataError
        
        if ((i == ASTOrbits._idxSun) or (i == ASTOrbits._idxMoon)):
            obj.dSemiMajAxisKM = __readElement(br, "<SemiMajorAxisKM>")
            readError = readError or ASTOrbits._globalReadError 
            dataError = dataError or ASTOrbits._globalDataError
        else:                       # N/A for all the rest of the objects
            obj.dAngDiamArcSec = ASTOrbits._NA_VALUE
            # Throw away value is irrelevant
            strIn = ASTStr.removeWhitespace(ASTFileIO.getSimpleTaggedValue(br, "<SemiMajorAxisKM>"))
            readError = readError or ((strIn == None) or (len(strIn) <= 0))        

        if ((i == ASTOrbits._idxEarth) or (i == ASTOrbits._idxMoon)):
            obj.dAngDiamArcSec = ASTOrbits._NA_VALUE
            strIn = ASTFileIO.getSimpleTaggedValue(br, "<AngDiamArcSec>")
            readError = readError or ((strIn == None) or (len(strIn) <= 0))        
        else:
            obj.dAngDiamArcSec = __readElement(br, "<AngDiamArcSec>")
            readError = readError or ASTOrbits._globalReadError
            dataError = dataError or ASTOrbits._globalDataError   
        if ((i == ASTOrbits._idxSun) or (i == ASTOrbits._idxMoon)):
            obj.dAngDiamDeg = __readElement(br, "<AngDiamDeg>")
            readError = readError or ASTOrbits._globalReadError
            dataError = dataError or ASTOrbits._globalDataError
        else:
            obj.dAngDiamDeg = ASTOrbits._NA_VALUE
            strIn = ASTFileIO.getSimpleTaggedValue(br, "<AngDiamDeg>")
            readError = readError or ((strIn == None) or (len(strIn) <= 0))  
        if (i == ASTOrbits._idxEarth):
            obj.dmV = ASTOrbits._NA_VALUE
            strIn = ASTFileIO.getSimpleTaggedValue(br, "<VisualMagnitude>")
            readError = readError or ((strIn == None) or (len(strIn) <= 0))  
        else:
            obj.dmV = __readElement(br, "<VisualMagnitude>")
            readError = readError or ASTOrbits._globalReadError 
            dataError = dataError or ASTOrbits._globalDataError
        
        obj.dGravParm = __readElement(br, "<GravParm>")
        readError = readError or ASTOrbits._globalReadError
        dataError = dataError or ASTOrbits._globalDataError
        obj.dInclination = __readElement(br, "<Inclination>")
        readError = readError or ASTOrbits._globalReadError
        dataError = dataError or ASTOrbits._globalDataError
        obj.dLonAtEpoch = __readElement(br, "<LonAtEpoch>")
        readError = readError or ASTOrbits._globalReadError
        dataError = dataError or ASTOrbits._globalDataError
        obj.dLonAtPeri = __readElement(br, "<LonAtPeri>")
        readError = readError or ASTOrbits._globalReadError 
        dataError = dataError or ASTOrbits._globalDataError

        if (i == ASTOrbits._idxSun):
            obj.dLonAscNode = ASTOrbits._NA_VALUE
            strIn = ASTFileIO.getSimpleTaggedValue(br, "<LonAscNode>")
            readError = readError or ((strIn == None) or (len(strIn) <= 0)) 
        else:
            obj.dLonAscNode = __readElement(br, "<LonAscNode>")
            readError = readError or ASTOrbits._globalReadError
            dataError = dataError or ASTOrbits._globalDataError
        
        if (readError or dataError):
            ASTMsg.errMsg("The Orbital Elements data file has one or more\n" +
                          "errors for the object [" + objName + "] Try another file.", "Orbital Elements Error")
            return False
        
        ASTOrbits._orbElementsDB.append(obj)
        
        # Throw away the end tag
        ASTFileIO.readTillStr(br, "</" + objName + ">")

        strIn = ASTFileIO.getNonBlankLine(br)
    # end of while loop
    
    if ((ASTOrbits._idxSun < 0) or (ASTOrbits._idxMoon < 0) or (ASTOrbits._idxEarth < 0)):
        ASTMsg.errMsg("Orbital Elements for the Sun, Moon, and Earth\n" +
                      "are required but one or more are missing.", "Missing Orbital Elements")
        return False

    ASTOrbits._orbitalElementsLoaded = True
    return True



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
