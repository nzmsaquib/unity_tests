"""
Provides the ability to load and view star catalogs.

The catalog data files **MUST** be in the format specifically
designed for this book. The format can be gleaned by opening
any of the star catalog data files and examining them. The
format is straightforward.
 
The data files provided with this book were created from
publicly available sources, particularly those data files
maintained in the NASA HEASARC archives, which are at the URL
http:#heasarc.gsfc.nasa.gov/docs/archive.html as of the
time this book was written.

Because the catalogs can be so large, only one catalog
can be loaded at a time.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import os, gc, io
from enum import Enum

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTConstellation as ASTConstellation
import ASTUtils.ASTFileIO as ASTFileIO
import ASTUtils.ASTInt as ASTInt
from ASTUtils.ASTMisc import ABORT_PROG, HIDE_ERRORS, UNKNOWN_mV, DEFAULT_EPOCH,\
                             UNKNOWN_RA_DECL, HMSFORMAT,DMSFORMAT, ASCENDING_ORDER,\
                             DESCENDING_ORDER
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTQuery as ASTQuery
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTStyle as ASTStyle
import ASTUtils.ASTTime as ASTTime

# Since the catalogs can be quite large, it may take a long time to display all objects. The MAX_OBJS_TO_PRT constant
# is the maximum number of objects to display before asking the user if they want to continue. However, if there
# are OBJS_LIMIT or less, this limit is ignored since the objects will still print in a reasonable amount of time.
MAX_OBJS_TO_PRT = 1000
OBJS_LIMIT = 2500



# Python is painfully slow when reading large text files (i.e., Star Catalogs).
# So, set a max size (in kilobytes) before giving the user a warning about
# the size of the file. This can be adjusted depending upon the speed
# of the user's machine
MAX_CATALOG_FILESIZE_KB = 3*1024



class __CatException(Exception):
    """Define an exception for errors while reading a star catalog"""
    pass



class CatalogSortField(Enum):
    """
    Define the ways a star catalog can be sorted and a method for converting the
    'enum' type to a printable string.
    """
    CONSTELLATION = "Constellation"                         # by constellation name
    CONST_AND_OBJNAME = "Constellation and Object Name"     # by constellation name and then by obj names
    OBJNAME = "Object's Name"                               # by object name
    OBJ_ALTNAME = "Object's Alternate Name"                 # by an an object's alternate name
    RA = "Object's Right Ascension"                         # by an object's Right Ascension
    DECL = "Object's Declination"                           # by an object's Declination
    VISUAL_MAGNITUDE = "Object's Visual Magnitude"          # by an object's visual magnitude

    def toStr(self):                 # printable string for this enumerated type
        return self.value



class ASTCatalog():
    """Defines a class for star catalog data"""
    #==================================================================
    # Class variables
    #==================================================================
    # A single underscore is used rather than a double underscore so
    # that the variables can be accessed by functions below that are
    # part of this package, but appear as 'private' for functions
    # outside this package.
    _prt = None                     # instance so that we can output to a user's scrollable text area
    _classInitialized = False       # keep track of whether the class has been initialized
    _catalogLoaded = False          # keep track of whether a catalog is loaded
    
    # File types and default directory for catalog data 
    __fileExtFilter = [("Star Catalog","*.dat"),("All Files",".*")]
    __dataDir = os.path.realpath(os.path.join("..","..","Ast Data Files","Star Catalogs"))    
      
    # Define class variables for the catalog 'header,' which provides
    # basic information about the catalog. We use class variables here
    # rather than class instance variables because we can only have
    # one catalog loaded at a time. These are also initialized in
    # clearCatalogAndSpaceObjects.
    _cat_type = ""              # type of catalog
    _sDate = ""                 # most recent date that the catalog was updated
    _sSource = ""               # source from which the catalog came
    _sDescription = ""          # brief overall description of the objects in the catalog
    _nObjects = 0               # how many objects are contained in the catalog
    _nConstellations = 0        # how many constellations the objects in the catalog cover
    _Epoch = DEFAULT_EPOCH      # the Epoch, if known, to which object coordinates are referenced
    _bEpochKnown = True         # true if the Epoch on which this catalog is based is known
    _bmVProvided = False        # true if the catalog provides visual magnitudes for the objects in the catalog
    _dmVLimit = UNKNOWN_mV      # The limiting visual magnitude that was used to filter the catalog to reduce its size
    _sAuthor = ""               # the author(s) that created the catalog
    
    # As per the format requirements, objects in the catalog data files are to be sorted
    # in ascending order by constellation and then in ascending order by object name
    # within the constellations. However, once a catalog is loaded, the user may want
    # the catalog sorted in some other order. The boolean saveCurrentSortOrder is needed
    # because we need a way to pass the desired sorting order to the various comparators defined below.
    # These two "save" flags are also used to avoid sorting when we don't need to.
    _saveCurrentSortOrder = ASCENDING_ORDER
    _saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME            
    
    @staticmethod
    def getCatDataDir():
        """Gets the data directory for the star catalogs"""
        return ASTCatalog.__dataDir
    
    @staticmethod
    def getFileExtFilter():
        """Gets a filter for filtering star catalog files by extension."""
        return ASTCatalog.__fileExtFilter
    


#===========================================================
# 'getters' for the catalog header class variables. No
# 'setters' are provided because only functions in
# this package set the catalog header class variables.
#===========================================================
def getCatAuthor():
    return ASTCatalog._sAuthor
def getCatDate():
    return ASTCatalog._sDate
def getCatDescription():
    return ASTCatalog._sDescription
def getCatEpoch():
    return ASTCatalog._Epoch
def getCatEpochKnown():
    return ASTCatalog._bEpochKnown
def getCatmVLimit():
    """
    Gets the visual magnitude limit used to filter the catalog. The catalog will contain only
    objects that are at least as bright as the limit.
    
    :return: visual magnitude limit (maximum brightness) for the objects in the catalog.
             This value is set regardless of whether the catalog actually contains any
             visual magnitude for its objects.
    """
    return ASTCatalog._dmVLimit
def getCatmVProvided():
    return ASTCatalog._bmVProvided
def getCatType():
    return ASTCatalog._cat_type
def getCatNumConst():
    return ASTCatalog._nConstellations
def getCatNumObjs():
    return ASTCatalog._nObjects
def getCatSource():
    return ASTCatalog._sSource



class __SpaceObj():
    """Define an internal class for a star catalog object"""
    #===============================================================
    # Class instance variables
    #=============================================================== 
    # sName        the object's primary name
    # sAltName     object's alternate name
    # dRA          right ascension (in decimal hours) for the object w.r.t. the catalog's Epoch
    # dDecl        declination (in decimal degrees) for the object w.r.t. the catalog's Epoch
    # bmVKnown     true if the catalog provides a visual magnitude for the object
    # dmV          object's visual magnitude, if known
    # sComment     optional comment contained in the catalog about the object
    # idx          index into the constellations database for the constellation that this object lies within
    
    def __init__(self,Name,AltName,RA,Decl,mVKnown,mV,Comment,i):
        """Create a __SpaceObj object"""
        self.sName = Name
        self.sAltName = AltName
        self.dRA = RA
        self.dDecl = Decl
        self.bmVKnown = mVKnown
        self.dmV = mV
        self.sComment = Comment
        self.idx = i
    
    # define comparisons for sorting the star catalog
    def __eq__(self,other):      
        if (_compare(self,other) == 0):
            return True
        else:
            return False
         
    def __ne__(self,other):
        return not self.__eq__(other)
        
    def __lt__(self,other):
        if (_compare(self,other) < 0):
            return True
        else:
            return False
 
    def __le__(self,other):
        if (_compare(self,other) <= 0):
            return True
        else:
            return False
        
    def __gt__(self,other):
        return self.__lt__(other)
 
    def __ge__(self,other):
        return self.__le__(other)   

    _StarCatalog = []                  # database of the star catalog objects



#==================================================================
# Public functions for getting star catalog object data.
#
# Note that the space objects are stored in an array, so a 'get/set'
# must specify which space object is desired in the array.
#==================================================================

def getCatObjAltName(idx):
    """
    Gets an object's alternate name
    
    :param int idx: which object (0-based indexing) from the space objects database is desired
    :return: object's alternate name or null if the object does not exist
    """
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return ""
    else:
        return __SpaceObj._StarCatalog[idx].sAltName



def getCatObjComment(idx):
    """
    Gets a comment about the object
    
    :param int idx: which object (0-based indexing) from the space objects database is desired
    :return: comment, if there is one  
    """
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return ""
    else:
        return __SpaceObj._StarCatalog[idx].sComment



def getCatObjConstIdx(idx):
    """
    Gets an index into the constellations database for the constellation
    in which this object lies within.
    
    :param int idx: which object (0-based indexing) from the space objects database is desired
    :return: index into constellation database or -1 if an error
    """  
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return -1
    else:
        return __SpaceObj._StarCatalog[idx].idx
    


def getCatObjDecl(idx):
    """
    Gets an object's Decl w.r.t. the catalog Epoch
    
    :param int idx: which object (0-based indexing) in the space objects database
    :return: object's Decl or UNKNOWN_RA_DECL if the object does not exist
    """    
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return UNKNOWN_RA_DECL
    else:
        return __SpaceObj._StarCatalog[idx].dDecl    



def getCatObjmV(idx):
    """
    Gets an object's visual magnitude
    
    :param int idx: which object (0-based indexing) in the space objects database
    :return: object's visual magnitude or UNKNOWN_mV if the object does not
             exist. This return value if valid only if the object's visual
             magnitude is known
    """
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return UNKNOWN_mV
    else:
        return __SpaceObj._StarCatalog[idx].dmV



def getCatObjmVKnown(idx):
    """
    Gets whether the object's visual magnitude is known
    
    :param int idx: which object (0-based indexing) in the space objects database
    :return: false if the visual magnitude is unknown or if the object does not exist
    """
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return False
    else:
        return __SpaceObj._StarCatalog[idx].bmVKnown



def getCatObjName(idx):
    """
    Gets an object's name
    
    :param int idx: which object (0-based indexing) from the space objects database is desired
    :return: object's name or null if the object does not exist
    """
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return ""
    else:
        return __SpaceObj._StarCatalog[idx].sName



def getCatObjRA(idx):
    """
    Gets an object's RA w.r.t. the catalog Epoch
    
    :param int idx: which object (0-based indexing) in the space objects database
    :return: object's RA or UNKNOWN_RA_DECL if the object does not exist
    """
    if ((idx < 0) or (idx >= ASTCatalog._nObjects)):
        return UNKNOWN_RA_DECL
    else:
        return __SpaceObj._StarCatalog[idx].dRA



#===================================================================
# Public functions for manipulating a catalog
#===================================================================

def clearCatalogAndSpaceObjects():
    """Clears all the currently loaded catalog data including all space objects in the catalog"""
    # In other languages, we'd have to do a dispose operation here to
    # get rid of all the old objects.

    __SpaceObj._StarCatalog.clear()
    del __SpaceObj._StarCatalog[:]
    gc.collect()                    # force garbage collection to happen to release space

    # Now clear the catalog header information
    ASTCatalog._cat_type = ""
    ASTCatalog._sDate = ""
    ASTCatalog._sSource = ""
    ASTCatalog._sDescription = ""
    ASTCatalog._nObjects = 0
    ASTCatalog._nConstellations = 0
    ASTCatalog._Epoch = DEFAULT_EPOCH
    ASTCatalog._bEpochKnown = True
    ASTCatalog._bmVProvided = False
    ASTCatalog._dmVLimit = UNKNOWN_mV
    ASTCatalog._sAuthor = ""

    ASTCatalog._saveCurrentSortOrder = ASCENDING_ORDER
    ASTCatalog._saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME

    __SpaceObj._StarCatalog = []

    ASTCatalog._catalogLoaded = False
    


def displayAllCatalogObjects(mVFilter=None):
    """
    Displays all space objects in the currently loaded catalog
    by however the catalog is currently sorted. If no value
    for mVFilter is specified, all objects are displayed.
    
    :param float mVFilter: filter for displaying only those objects that are at least as bright as mVFilter
    """
    lineCount = 0
    
    if not (__isInitAndLoaded()):
        return   

    # See if we need to bother about pausing during display
    prtLimit = OBJS_LIMIT + 10            # this really means to ignore pausing during display
    if (getCatNumObjs() > OBJS_LIMIT):
        if (ASTMsg.abortMsg("Displaying "+str(getCatNumObjs())+" objects may take a long time.")):
            return      
    
        if (ASTQuery.showQueryForm(["Enter Max # of Objects to Display at a Time\n"+
                                    "(Recommend no More Than " + str(MAX_OBJS_TO_PRT) + 
                                    " Objects at a Time)"]) != ASTQuery.QUERY_OK):
            return
        strTmp = ASTQuery.getData(1)
        if ((strTmp == None) or (len(strTmp) <= 0)):
            return
    
        n = ASTInt.isValidInt(strTmp,HIDE_ERRORS)
        if (n.isValidIntObj()):
            prtLimit = n.getIntValue()
        else:
            prtLimit = MAX_OBJS_TO_PRT

    __displayObjHeader()

    for i in range(0,getCatNumObjs()):
        lineCount += 1
        if (lineCount > prtLimit):
            if (ASTMsg.abortMsg("Displaying "+str(i)+" of "+str(getCatNumObjs())+" objects")):
                return
            lineCount = 0
    
        # See if this object should be filtered out
        if (getCatObjmVKnown(i) and (mVFilter != None)):
            showObj = (getCatObjmV(i) <= mVFilter)
        else:
            showObj = True                # show object even if we don't know its mV value
    
        if (showObj):
            __displayObject(i + 1, i)



def displayAllObjsByConstellation(idx,sortOrder,mVFilter=None):
    """
    Displays all space objects in the currently loaded catalog
    by constellation. If no value is given for mVFilter, all
    objects in the constellation will be displayed.

    :param int idx: index into constellations database for the constellation of interest
    :param bool sortOrder: Sort in ascending order if ASCENDING_ORDER, else sort in descending order
    :param float mVFilter: filter for displaying only those objects that are at least as bright as mVFilter
    """
    prt = ASTCatalog._prt
    
    if not (__isInitAndLoaded()):
        return   

    if ((idx < 0) or (idx > ASTConstellation.getNumConstellations() - 1)):
        ASTMsg.errMsg("The Constellation requested does not exist", "Invalid Constellation Name")
        return
    
    iStart = -1
    iEnd = -1
    iNumObjs = 0
    lineCount = 0

    # We need to be sure the star catalog is already sorted by constellation name
    sortStarCatalog(CatalogSortField.CONST_AND_OBJNAME,sortOrder)            # this will sort only if we need to

    # First, go through the objects and find out how many we need to display
    for i in range(0,getCatNumObjs()):
        if (getCatObjConstIdx(i) == idx):
            iNumObjs += 1
            iEnd = i
            if (iStart < 0):
                iStart = i
        else:
            if (iStart >= 0):
                break            # We reached the end of the objects in this constellation.
     
    # See if we need to bother about pausing during display
    prtLimit = OBJS_LIMIT + 10            # this really means to ignore pausing during display
    if (iNumObjs > OBJS_LIMIT):
        if (ASTMsg.abortMsg("Displaying "+str(iNumObjs)+" objects may take a long time.")):
            return
    
        if (ASTQuery.showQueryForm(["Enter Max # of Objects to Display at a Time\n"+
                                    "(Recommend no More Than " + str(MAX_OBJS_TO_PRT) + 
                                    " Objects at a Time)"]) != ASTQuery.QUERY_OK):
            return
        n = ASTInt.isValidInt(ASTQuery.getData(1),HIDE_ERRORS)
        if (n.isValidIntObj()):
            prtLimit = n.getIntValue()
        else:
            prtLimit = MAX_OBJS_TO_PRT

    prt.println("The currently loaded catalog has "+str(iNumObjs)+" objects in the constellation " +
                ASTConstellation.getConstName(idx) + " (" + ASTConstellation.getConstAbbrevName(idx)+")")
    __displayObjHeader()
    if (iNumObjs <= 0):
        return
    
    for i in range(iStart,iEnd+1):
        lineCount += 1
        if (lineCount > prtLimit):
            if (ASTMsg.abortMsg("Displaying "+str(i - iStart + 1) +" of " + str(iNumObjs)+" objects")):
                return
            lineCount = 0
            
        # See if this object should be filtered out
        if (getCatObjmVKnown(i) and (mVFilter != None)):
            showObj = (getCatObjmV(i) <= mVFilter)
        else:
            showObj = True                # show object even if we don't know its mV value
    
        if (showObj):
            __displayObject(i - iStart + 1, i)



def displayAllObjsByRange(nStart,nEnd,mVFilter=None):
    """
    Displays all space objects in the currently loaded catalog by
    range, however the catalog is currently sorted. If no value
    is specified for mVFilter, all objects in the range are
    displayed.

    :param int nStart: the range start (assuming 0-based numbering so that 0 is the 1st item)
    :param int nEnd: the end of the range (assuming 0-based numbering)
    :param float mVFilter: filter for displaying only those objects that are at least as bright as mVFilter
    """
    prt = ASTCatalog._prt
    
    if not (__isInitAndLoaded()):
        return
    
    # First, be sure that nStart <= nEnd and that both are in range
    if (nStart > nEnd):
        iStart = nEnd
        iEnd = nStart
    else:
        iStart = nStart
        iEnd = nEnd
    if (iStart < 0):
        iStart = 0
    if (nEnd > getCatNumObjs() - 1):
        iEnd = getCatNumObjs() - 1
    
    lineCount = 0
    iNumObjs = iEnd - iStart + 1            # number of objects to display
    
    # See if we need to bother about pausing during display
    prtLimit = OBJS_LIMIT + 10            # this really means to ignore pausing during display
    if (iNumObjs > OBJS_LIMIT):
        if (ASTMsg.abortMsg("Displaying " + str(iNumObjs) + " objects may take a long time.")):
            return
    
        if (ASTQuery.showQueryForm(["Enter Max # of Objects to Display at a Time\n"+
                                    "(Recommend no More Than " + str(MAX_OBJS_TO_PRT) +
                                    " Objects at a Time)"]) != ASTQuery.QUERY_OK):
            return
        strTmp = ASTQuery.getData(1)
        if ((strTmp == None) or (len(strTmp) <= 0)):
            return
    
        n = ASTInt.isValidInt(strTmp,HIDE_ERRORS)
        if (n.isValidIntObj()):
            prtLimit = n.getIntValue()
        else:
            prtLimit = MAX_OBJS_TO_PRT
    
    prt.println("The currently loaded catalog has "+str(iNumObjs)+" objects in the requested index range")
    __displayObjHeader()
    
    for i in range(iStart,iEnd+1):
        lineCount += 1
        if (lineCount > prtLimit):
            if (ASTMsg.abortMsg("Displaying "+str(i - iStart + 1)+" of "+str(iNumObjs)+" objects")):
                return
            lineCount = 0
    
        # See if this object should be filtered out
        if (getCatObjmVKnown(i) and (mVFilter != None)):
            showObj = (getCatObjmV(i) <= mVFilter)
        else:
            showObj = True                # show object even if we don't know its mV value
    
        if (showObj):
            __displayObject(i + 1, i)



def displayCatalogInfo():
    """Displays the catalog header information"""
    if not (__isInitAndLoaded()):
        return

    prt = ASTCatalog._prt

    prt.println("Catalog Type: " + getCatType(),CENTERTXT)
    prt.println(getCatDescription(),CENTERTXT)
    if (getCatmVProvided()):
        strTmp = ASTStr.strFormat(ASTStyle.mVFORMAT,getCatmVLimit())
    else:
        strTmp = "***Visual Magnitude not provided***"        
    prt.println("Limiting Visual Magnitude for Catalog: "+strTmp,CENTERTXT)
    if (getCatEpochKnown()):
        strTmp = ASTStr.strFormat(ASTStyle.EPOCHFORMAT,getCatEpoch())
    else:
        strTmp = "***Epoch not provided***"
    prt.println("Catalog Epoch: " + strTmp,CENTERTXT)
    prt.println("Catalog Author(s): " + getCatAuthor(),CENTERTXT)
    prt.println()
    prt.println("Catalog Date: " + getCatDate())
    prt.println("Original Source: " + getCatSource())
    prt.println()
    strTmp = ASTStr.strFormat("Catalog covers %d Constellations and has ",getCatNumConst()) +\
             ASTStr.strFormat("a total of %d Objects",getCatNumObjs()) 
    prt.println(strTmp)
    


def displayFullObjInfo(i):
    """
    Displays all information in the catalog about the i-th space
    object entry in the currently loaded and sorted catalog

    :param int i: Which object to display (assumes 0-based indexing).
    """
    prt = ASTCatalog._prt    
    
    if not (__isInitAndLoaded()):
        return
    
    if ((i < 0) or (i >= getCatNumObjs())):
        prt.println("The object specified does not exist in the currently loaded catalog")
        return

    idx = getCatObjConstIdx(i)
    strRA = ASTTime.timeToStr_dec(getCatObjRA(i),HMSFORMAT)
    strDecl = ASTAngle.angleToStr_dec(getCatObjDecl(i),DMSFORMAT)
    
    prt.println("The object named '" + getCatObjName(i)+"' is in the constellation " +
                ASTConstellation.getConstName(idx)+" ("+ASTConstellation.getConstAbbrevName(idx)+")")
    prt.println("It's alternate name (if any) in this catalog is: "+getCatObjAltName(i))
    prt.println("The object is located at "+strRA + " RA, " + strDecl + " Decl")

    if (getCatObjmVKnown(i)):
        prt.println("It's visual magnitude is "+ASTStr.strFormat(ASTStyle.mVFORMAT,getCatObjmV(i)))
    else:
        prt.println("It's visual magnitude is not given in the catalog")
        
    prt.println("Additional information (if any) in the catalog about this object: "+getCatObjComment(i))



def findObjByAltName(searchStr):
    """
    Finds an object in the current catalog given its alternate name.
    Searches are not case sensitive.

    :param str searchStr: The name to search for. An exact match, ignoring whitespace and case, is required.
    :return: index into the StarCatalog list if successful, else -1. 0-based indexing is assumed!
    """
    targ = ASTStr.removeWhitespace(searchStr)

    for i in range(0,ASTCatalog._nObjects):
        if (ASTStr.compareIgnoreCase(targ,ASTStr.removeWhitespace(getCatObjAltName(i)))):
            return i
    
    return -1            # not found           



def findObjByName(searchStr):
    """
    Finds an object in the current catalog given its name.
    Searches are not case sensitive.

    :param str searchStr: The name to search for. An exact match, ignoring whitespace and case, is required.
    :return: index into the StarCatalog list if successful, else -1. 0-based indexing is assumed!
    """
    targ = ASTStr.removeWhitespace(searchStr)
    for i in range(0,ASTCatalog._nObjects):
        if (ASTStr.compareIgnoreCase(targ,ASTStr.removeWhitespace(getCatObjName(i)))):
            return i
    
    return -1            # not found



def findObjsByComments(targ):
    """
    Searches the currently loaded star catalog and returns an index into
    the database for all objects that contain the target substring
    in their 'comments' field. Searches are not case sensitive.

    :param str targ: target substring to search for
    :return: dynamic array of integers that will contain the indices into
             the star catalog for the requested objects. If unsuccessful, the
             list is empty. 0-based indexing is assumed for the indices in
             the list that gets generated.
    """
    result = []
    
    targStr = ASTStr.removeWhitespace(targ.lower())
    
    for i in range(0,ASTCatalog._nObjects):
        tmpStr = ASTStr.removeWhitespace(getCatObjComment(i).lower())
        if (tmpStr.find(targStr) >= 0):
            result.append(i)
    
    return result



def getCatFileToOpen():
    """
    Puts up a browser window and gets a star catalog filename.
    
    :return: full name (file and path) of the catalog file   
             to open or null if no file is selected or
             file doesn't exist. This routine does **not**
             check to see if the file is a valid catalog, 
             but it **does** check to see that the file exists.
    """
    fileToRead = ASTFileIO.getFileToRead("Select Catalog to Load ...",ASTCatalog.getFileExtFilter(),\
                                     ASTCatalog.getCatDataDir())

    # See if the file exists
    if not (ASTFileIO.doesFileExist(fileToRead)):
        ASTMsg.errMsg("The Star Catalog file specified does not exist","Catalog Does Not Exist")
        return None
    
    return fileToRead



def initStarCatalogs(prtInstance):
    """
    Does a one-time initialization required for loading or using Star Catalogs
    
    :param ASTPrt prtInstance: instance for performing output to the application's scrollable text output area
    """
    # Return if we've already initialized
    if (ASTCatalog._classInitialized):
        return

    clearCatalogAndSpaceObjects()
    ASTCatalog._classInitialized = True
    
    if (prtInstance == None):
        ASTMsg.criticalErrMsg("An output text area cannot be null.\nAborting program ...", ABORT_PROG)
    
    ASTCatalog._prt = prtInstance



def isCatalogLoaded():
    """
    Checks to see if a star catalog has been loaded
    
    :return: true if a star catalog has been loaded.
    """
    return (ASTCatalog._catalogLoaded and ASTCatalog._classInitialized)



def loadFormattedStarCatalog(filename):
    """
    Loads a catalog from disk. The catalog file must have
    all the fields even If they have no value for them and
    the catalog must be in the format designed for this book.

    :param str filename: The full filename (including path) to load.
    :return: true if successful, else false
    """
    errorOccurred = False
    
    clearCatalogAndSpaceObjects()
    
    # Note that we're using 'with open' so we don't have to explicitly close the file when we're done.
    # Also, we're doing buffered file I/O to read the data line by line and not using
    # io.StringIO to read everything into an internal buffer of strings because the 
    # buffered I/O seems to be just as fast as StringIO and does not use as much memory
    # for large star catalogs.
    try:      
        with io.open(filename,"rt") as br:
            # Validate that the file is indeed a star catalog data file
            strIn = ASTFileIO.readTillStr(br,"<Catalog>",HIDE_ERRORS)
            if (strIn == None):
                ASTMsg.errMsg("The data file specified is not a Star Catalog\ndata file. Try again ...",
                              "Invalid Star Catalog")
                return False

            # Get the basic catalog header information. Each tag must be
            # present or else it is an error. A check could be made after
            # each invocation of getSimpleTaggedValue, but that would
            # complicate the code and make it messier. So, we set the
            # error flag errorOccurred after each getSimpleTaggedValue
            # call and only check once at the end. A null result from
            # getSimpleTaggedValue means a tag was missing while a "" result 
            # means the tag was there but no value was given for the tag
            ASTCatalog._cat_type = ASTFileIO.getSimpleTaggedValue(br,"<CatalogType>")
            errorOccurred = (ASTCatalog._cat_type == None)
 
            ASTCatalog._sDescription = ASTFileIO.getSimpleTaggedValue(br,"<Description>")
            errorOccurred = errorOccurred or (ASTCatalog._sDescription == None)
 
            ASTCatalog._dmVLimit = UNKNOWN_mV
            strIn=ASTFileIO.getSimpleTaggedValue(br,"<VisualMagnitudeLimit>")
            errorOccurred = errorOccurred or (strIn == None)
            if (errorOccurred):
                ASTCatalog._bmVProvided = False            # tag may have been there w/o a value so take defaults
            else:
                rTmp = ASTReal.isValidReal(strIn,HIDE_ERRORS)
                ASTCatalog._bmVProvided = rTmp.isValidRealObj()
                if (ASTCatalog._bmVProvided):
                    ASTCatalog._dmVLimit = rTmp.getRealValue()
            
            ASTCatalog._Epoch = DEFAULT_EPOCH
            strIn=ASTFileIO.getSimpleTaggedValue(br,"<Epoch>")
            errorOccurred = errorOccurred or (strIn == None)
            if (errorOccurred):
                ASTCatalog._bEpochKnown = False
            else:
                rTmp = ASTReal.isValidReal(strIn,HIDE_ERRORS)
                ASTCatalog._bEpochKnown = rTmp.isValidRealObj()
                if (ASTCatalog._bEpochKnown):
                    ASTCatalog._Epoch = rTmp.getRealValue()
 
            ASTCatalog._sAuthor=ASTFileIO.getSimpleTaggedValue(br,"<Author>")
            errorOccurred = errorOccurred or (ASTCatalog._sAuthor == None)
            ASTCatalog._sDate=ASTFileIO.getSimpleTaggedValue(br,"<Date>")
            errorOccurred = errorOccurred or (ASTCatalog._sDate == None)
            ASTCatalog._sSource=ASTFileIO.getSimpleTaggedValue(br,"<Source>")
            errorOccurred = errorOccurred or (ASTCatalog._sSource == None)

            # No point in continuing if an error has occurred in getting the header
            if (errorOccurred):
                raise __CatException
            
            # Now get the data about the space objects
            strIn = ASTFileIO.readTillStr(br,"<Data>")
            errorOccurred = errorOccurred or (strIn == None)
            
            for strIn in br:
                # Get rid of the line terminator, which differs between Windows (CRLF),
                # Unix (LF), MAC OS (CR).
                strIn = strIn.strip()
                
                if (strIn == None) or (len(strIn) <= 0):
                    continue                    # ignore blank lines
 
                str2 = ASTStr.removeWhitespace(strIn.lower())
                if (str2.find("</data>") >= 0):
                    break                # found end of data section
 
                skipLine = False
                RA = UNKNOWN_RA_DECL
                Decl = UNKNOWN_RA_DECL 
                
                parts = strIn.split(",")
                
                # Get the various fields, but skip if RA or Decl is missing
                if (len(parts) != 7):
                    continue
 
                Name = parts[0].strip()
                ConstAbbrvName = parts[4].strip()
                AltName = parts[5].strip()
                Comment = parts[6].strip()
                
                # Get RA, Decl, and mV. Skip if RA or Decl is missing
                rTmp = ASTReal.isValidReal(parts[1],HIDE_ERRORS)
                if (rTmp.isValidRealObj()):
                    RA = rTmp.getRealValue()
                else:
                    skipLine = True
 
                rTmp = ASTReal.isValidReal(parts[2],HIDE_ERRORS)
                if (rTmp.isValidRealObj()):
                    Decl = rTmp.getRealValue()
                else:
                    skipLine = True
                    
                rTmp = ASTReal.isValidReal(parts[3],HIDE_ERRORS)
                if (rTmp.isValidRealObj()):
                    mV = rTmp.getRealValue()
                    mVGiven = True
                else:
                    mVGiven = False
                    mV = UNKNOWN_mV
                    
                if (skipLine):
                    continue
 
                ConstIdx = ASTConstellation.findConstellationByAbbrvName(ConstAbbrvName)
                if (ConstIdx < 0):
                    continue              # skip object if constellation not found
                __addSpaceObjToCatalog(Name,AltName,RA,Decl,mVGiven,mV,Comment,ConstIdx)                                                        

        br.close()                # don't really have to do this ...

    except __CatException:
        ASTMsg.criticalErrMsg("The selected catalog could not be loaded")
        errorOccurred = True
    
    if (errorOccurred):
        clearCatalogAndSpaceObjects()        # if an error occurred, then reset back to having no catalog
    else:
        ASTCatalog._nObjects = len(__SpaceObj._StarCatalog)
        ASTCatalog._nConstellations = __countConstellations()
        ASTCatalog._catalogLoaded = True
        ASTCatalog._saveCurrentSortOrder = ASCENDING_ORDER
        ASTCatalog._saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME        

    # Note that errorOccurred is the opposite of what we need to return
    return not errorOccurred



def sortStarCatalog(sortField,sortOrder):
    """
    Sorts the catalog of space objects.
    
    :param CatalogSortField sortField: What field to sort on.
    :param bool sortOrder: Sort in ascending order if ASCENDING_ORDER, else sort in descending order
    """
    # See if there's anything to do
    if not (__isInitAndLoaded()):
        return
    if ((sortOrder == ASTCatalog._saveCurrentSortOrder) and (sortField == ASTCatalog._saveCurrentSortField)):
        return
     
    ASTCatalog._saveCurrentSortOrder = sortOrder
    ASTCatalog._saveCurrentSortField = sortField
    
    reverseOrder = (sortOrder == DESCENDING_ORDER)   
 
    # See _compare below for notes about sorting
    __SpaceObj._StarCatalog.sort(reverse=reverseOrder)



#===================================================================
# Private functions only used internally to this module
#===================================================================

def __addSpaceObjToCatalog(Name,AltName,RA,Decl,mVKnown,mV,Comment,i):
    """
    Create a new SpaceObj instance with the values passed in.
    
    :param str Name: name of the object to add
    :param str AltName: alternate name for the object
    :param float RA: the object's right ascension (in hours) w.r.t. the catalog Epoch
    :param float Decl: the object's declination (in degrees) w.r.t. the catalog Epoch
    :param bool mVKnown: true if the object's visual magnitude is known
    :param float mV: the object's visual magnitude (ignored if the mV is unknown)
    :param str Comment: optional comment about the object
    :param int i: index into the Constellations database for the constellation in which this object falls
    """
    __SpaceObj._StarCatalog.append(__SpaceObj(Name,AltName,RA,Decl,mVKnown,mV,Comment,i))
    ASTCatalog._nObjects += 1
  


def __countConstellations():
    """
    Counts the number of unique constellations in the currently
    loaded catalog. The count is put into the catalog data structure.
    he objects in the catalog **MUST** be grouped by constellation
    before this method is invoked. All properly formatted catalogs
    are stored on disk sorted by constellation, so they should be OK
    when loaded. How the objects are sorted does not matter as long
    as they are grouped by constellation.

    Note that the StarCatalog database has an index that points
    into the constellations database, so we can compare the indices
    without needing to actually find out the constellation name
    and do a string compare.

    :return: a count of the number of unique constellations in the currently loaded star catalog.
    """
    count = 0
    idx = -1
    
    for i in range(0,len(__SpaceObj._StarCatalog)):
        if (getCatObjConstIdx(i) != idx):
            count += 1
            idx = getCatObjConstIdx(i)

    ASTCatalog._nConstellations = count
    return count



def __displayObject(i,idx):
    """
    Displays catalog information for one specific object from the catalog.
    
    :param int i: Since this function is intended to be invoked from inside a loop,
              the parameter i is a counter from the calling method's loop.
    :param int idx: index into the Star Catalog for the object to display
    """
    sConstAbbrvName = " "
    
    if not (__isInitAndLoaded()):
        return
    if ((idx < 0) or (idx >= getCatNumObjs())):
        return
    
    strRA = ASTTime.timeToStr_dec(getCatObjRA(idx),HMSFORMAT)
    strDecl = ASTAngle.angleToStr_dec(getCatObjDecl(idx),DMSFORMAT)

    if (getCatObjmVKnown(idx)):
        sTmp = ASTStr.strFormat("%4.1f",getCatObjmV(idx))
    else:
        sTmp = "  ? "
    
    iConstName = getCatObjConstIdx(idx)
    if ((iConstName >= 0) and (iConstName < ASTConstellation.getNumConstellations())):
        sConstAbbrvName = ASTConstellation.getConstAbbrevName(iConstName)
    else:
        sConstAbbrvName = "ERR"
    
    # This format string must match the one in displayObjHeader
    s = ASTStr.strFormat("%5d ",i) + ASTStr.strFormat("%-18s ",getCatObjName(idx)) +\
        ASTStr.strFormat("%12s ",strRA) + ASTStr.strFormat("%16s ",strDecl) +\
        ASTStr.strFormat("%4s   ",sTmp) + ASTStr.strFormat("%-12s ",getCatObjAltName(idx)) + \
        ASTStr.strFormat("%-5s",sConstAbbrvName)    
    ASTCatalog._prt.println(s)



def __displayObjHeader():
    """
    Displays a header for the objects that will be displayed after the header.
    This function must be kept in synch with displayObject so that the header
    and the columns that are printed are consistent.
    """
    if not (__isInitAndLoaded()):
        return
    
    s = ASTStr.strFormat("      %-18s ","Obj Name") + ASTStr.strFormat("%-12s ","      RA") +\
        ASTStr.strFormat("%-16s ","        Decl") + ASTStr.strFormat("%4s   ", "mV ") +\
        ASTStr.strFormat("%-12s ","Alt Name") + ASTStr.strFormat("%5s","Const")
    ASTCatalog._prt.println(s)



def __isInitAndLoaded():
    """
    Checks to see if this module has been properly initialized
    and a catalog has been loaded.
    """
    if not (ASTCatalog._classInitialized):
        ASTMsg.criticalErrMsg("Catalogs have not been initialized ... Catalog data\n" +
                              "cannot be displayed in the scrollable text area.\nAborting program ...", ABORT_PROG)
        
    return isCatalogLoaded()



#=============================================================
# Comparators for sorting the objects in a catalog
#=============================================================

#===========================================IMPORTANT NOTE==========================================
# We need to sort star catalog options on several different fields, some of which are alphanumeric.
# Python's sort method does not do 'natural order' sorting and hence would sort 'A11' before 'A9' 
# when sorting in ascending order. One solution would be to use the natsort package, but that
# requires finding and installing that package since it is not part of the base Python language.
# The solution used here requires only the features found in the base Python programming language
# (e.g., sort).
#
# With Python's sort method, simple numeric sorting (e.g., RA, Decl) can be done with lambda
# functions or by using the key parameter to sort to invoke a function to do comparisons. For example,
#    def myfnct(obj):
#        return obj.dDecl
#    __SpaceObj._StarCatalog.sort(key=myfnct)
# will sort the star catalog in ascending order by Decl. The following sequence will do natural
# sorting in descending order on the space object's name:  
#    from re import compile, split
#    dre = compile(r'(\d+)')
#    key = lambda __SpaceObj: [int(s) if s.isdigit() else s.lower() for s in split(dre, __SpaceObj.sName)]
#    __SpaceObj._StarCatalog.sort(key=key,reverse=True).
#
# The web site
#    https://stackoverflow.com/questions/4836710/does-python-have-a-built-in-function-for-string-natural-sort
# gives some suggestions on implementing natural sorting with base Python features.
#
# To simplify porting to other languages, we'll define comparators to more explicitly describe how we
# want a sort to be done. The eq, lt, gt, etc. methods in the __SpaceObj class will invoke the
# _compare function which then chooses the appropriate comparator to used based on the current
# value of ASTCatlog._saveCurrentSortField. We'll use _saveCurrentSortOrder to determine
# whether True or False should be used in the sort method's reverse parameter.
#===========================================IMPORTANT NOTE==========================================

# Must use single underscore so that _compare is visible inside the __SpaceObj class
def _compare(obj1,obj2):
    """
    Compare two star catalog objects for sorting purposes.

    :param __SpaceObj obj1: 1st object to compare
    :param __SpaceObj obj2: 2nd object to compare
    :return: -1 if obj1 < obj2, 0 if obj1 = obj2, and 1 if obj1 > obj2
    """
    # Determine what fields to sort on based on _saveCurrentSortField
    sortField = ASTCatalog._saveCurrentSortField

    if (sortField == CatalogSortField.CONSTELLATION):
        return __constComparator(obj1, obj2)
    elif (sortField == CatalogSortField.CONST_AND_OBJNAME):
        return __constAndObjNameComparator(obj1, obj2)   
    elif (sortField == CatalogSortField.OBJNAME):
        return __spaceObjNameComparator(obj1, obj2)
    elif (sortField == CatalogSortField.OBJ_ALTNAME):
        return __spaceObjAltNameComparator(obj1, obj2)         
    elif (sortField == CatalogSortField.RA):
        return __spaceObjRAComparator(obj1, obj2)
    elif (sortField == CatalogSortField.DECL):
        return __spaceObjDeclComparator(obj1, obj2)
    elif (sortField == CatalogSortField.VISUAL_MAGNITUDE):
        return __spaceObjmVComparator(obj1, obj2)



def __constAndObjNameComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by the Constellation
    they are in, and then by the object's name
   
    :param __SpaceObj obj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    # This function doesn't worry about ascending vs descending
    # order since the calling function does that
    
    returnValue = __constComparator(obj1, obj2) 
       
    # If returnValue is 0, then the two objects are in the same constellation. So sort them
    # by their object name. We need to do a special alphanumeric comparison so that, for example,
    # A9 sorts before A11
    if (returnValue == 0):
        returnValue = ASTStr.compareAlphaNumeric(obj1.sName, obj2.sName)
    
    return returnValue



def __constComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by the constellation they are in.
     
    :param __SpaceObj obj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    s1 = ASTConstellation.getConstName(obj1.idx).strip().lower()
    s2 = ASTConstellation.getConstName(obj2.idx).strip().lower()

    # This function doesn't worry about ascending vs descending
    # order since the calling function does that     
    if (s1 > s2):
        return 1
    elif (s1 < s2):
        return -1
    else:
        return 0



def __spaceObjAltNameComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by an object's alternate name
    
    :param __SpaceObj obj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    # This function doesn't worry about ascending vs descending
    # order since the calling function does that
    
    # We need to do a special alphanumeric comparison so that, for example,
    # A9 sorts before A11
    return ASTStr.compareAlphaNumeric(obj1.sAltName,obj2.sAltName)



def __spaceObjDeclComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by Decl
    
    :param __SpaceObj obj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    # This function doesn't worry about ascending vs descending
    # order since the calling function does that
    if (obj1.dDecl > obj2.dDecl):
        returnValue = 1
    elif (obj1.dDecl < obj2.dDecl):
        returnValue = -1
    else:
        returnValue = 0

    # We could check sort order and use the following code: 
    #     if (saveCurrentSortOrder == ASCENDING_ORDER) return returnValue
    #     else return -1*returnValue
    # But, we'll use the reverse parameter in sort to do that.
    
    return returnValue



def __spaceObjmVComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by visual magnitude
    
    :param __SpaceObj obj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    # This function doesn't worry about ascending vs descending
    # order since the calling function does that
    if (obj1.dmV > obj2.dmV):
        return 1
    elif (obj1.dmV < obj2.dmV):
        return -1
    else:
        return 0



def __spaceObjNameComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by an object's name
    
    :param __SpaceObj bj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    # This function doesn't worry about ascending vs descending
    # order since the calling function does that    
    
    return ASTStr.compareAlphaNumeric(obj1.sName,obj2.sName)



def __spaceObjRAComparator(obj1,obj2):
    """
    Comparator for sorting catalog objects by RA
    
    :param __SpaceObj obj1, obj2: which objects are being considered by the sorter
    :return: -1, 0, or 1
    """
    # This function doesn't worry about ascending vs descending
    # order since the calling function does that
    if (obj1.dRA > obj2.dRA):
        return 1
    elif (obj1.dRA < obj2.dRA):
        return -1
    else:
        return 0



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
