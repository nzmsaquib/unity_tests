"""
Provides useful information about the 88 modern
constellations, including their boundaries, a method for
determining what constellation a given RA/Decl falls within,
and other useful methods for accessing constellation information.

Unless otherwise noted, RA is given in hours, Decl is given in
degrees, and both coordinates are w.r.t. Epoch 2000.0.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import os, math, io

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTFileIO as ASTFileIO
import ASTUtils.ASTMath as ASTMath
from ASTUtils.ASTMisc import ABORT_PROG, HIDE_ERRORS, UNKNOWN_RA_DECL, HMSFORMAT,DMSFORMAT
import ASTUtils.ASTMsg as ASTMsg
from ASTUtils.ASTPrt import CENTERTXT
import ASTUtils.ASTReal as ASTReal
import ASTUtils.ASTStr as ASTStr
import ASTUtils.ASTTime as ASTTime

NUM_CONSTELLATIONS = 88           # number of modern constellations



#=============================================================
# Define some other data items that intended to only be used
# in this module. These constants are used in the precession
# and findConstellation functions.
#=============================================================
CDR = 0.17453292519943e-01
CONVH = 0.2617993878
CONVD = 0.1745329251994e-01
PI4 = 6.28318530717948
E75 = 1875.0
RA_IDX = 0
DECL_IDX = 1



class __ConstException(Exception):
    """Define an exception for errors in reading the constellation data file"""
    pass



class ASTConstellation():
    """Defines a class for a constellation object."""
    #==================================================================
    # Class variables
    #
    # Note that we use a single underscore to name these class
    # variables so that they can be accessed by functions below that
    # are not class methods, yet indicate that they should be
    # considered as private to this module. Using a double underscore
    # to name them will not work because they would be private to the
    # class and not accessible outside the class.
    #==================================================================
    # we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
    _prt = None
    
    # Name of the data file containing the constellation data, its location,
    # and defaults in case the user must find it.
    _dataFilename = "ConstellationData.dat"        
    _fullFilename = os.path.realpath(os.path.join("..","ASTUtils",_dataFilename))
    _fileExtFilter = [("Data File","*.dat"),("All Files",".*")]
    _fallbackDir = os.path.realpath(os.path.join("..","..","Ast Data Files"))    

    _constDBReadIn = False             # avoid initializing more than once
            
    #===============================================================
    # Class instance variables
    #===============================================================
    # sName                           the constellation's name
    # sAbbrevName                     the constellation's international 3-character abbreviation
    # sMeaning                        what the constellation's name means (e.g., Andromeda means 'The Chained Maiden')
    # dCenterRA                       right ascension for the center of the constellation
    # dCenterDecl                     declination for the center of the constellation.
    # sBrightestStar                  name of the brightest star in the constellation
    # dStarRA                         right ascension for the brightest star in the constellation
    # dStarDecl                       declination for the brightest star in the constellation

    def __init__(self,name,abbrevname,meaning,centerRA,centerDecl,brightestStar,starRA,starDecl):
        """Constructor for this class"""
        self.sName = name
        self.sAbbrevName = abbrevname
        self.sMeaning = meaning
        self.dCenterRA = centerRA
        self.dCenterDecl = centerDecl
        self.sBrightestStar = brightestStar
        self.dStarRA = starRA
        self.dStarDecl = starDecl



class __BoundaryData():
    """
    Define an internal class for boundaries data, which define the boundaries
    for a constellation. The boundaries database can't be part of constDB because
    boundaries are not sorted in the same order as the constellations database.
    The boundaries database is sorted in a very specific way to make it easier
    to find what constellation a given RA/Decl falls within. Note that all RA/Decl
    coordinates in the boundaries database are defined w.r.t. the 1875.0 Epoch, not J2000.0.
    """
    #===============================================================
    # Class instance variables
    #===============================================================    
    # dRAlower            Lower RA boundary for a constellation
    # dRAupper            Upper RA boundary for this constellation
    # dDecl               Declination boundary for this constellation
    # idx                 Index into the constDB for this boundary
    
    def __init__(self,RAlower,RAupper,Decl,idx):
        self.dRAlower = RAlower
        self.dRAupper = RAupper
        self.dDecl = Decl
        self.idx = idx



__boundariesDB = []             # database for constellation boundaries

# Since these are private methods, we won't bother making sure idx is in range  
def __getNumBoundaries():
    return len(__boundariesDB)
def __getRAlower(idx):
    return __boundariesDB[idx].dRAlower
def __getRAupper(idx):
    return __boundariesDB[idx].dRAupper
def __getDecl(idx):
    return __boundariesDB[idx].dDecl
def __getConstIdx(idx):
    return __boundariesDB[idx].idx



#======================================================================
# Define a constellations database (__constDB). The only access to the
# constellations database that is allowed external to this module is to
# get elements within the database, which is provided through 'get'
# accessors defined below. The constellation database is an array of
# ASTConstellation objects.
#======================================================================

__constDB = []
 
# There is no need for 'set' accessors because only the
# functions in this module set class instance variables.
# Note that the constellations are stored as an array, so
# a 'get' must specify which constellation is desired from
# that array. 0-based indexing is assumed!
def getNumConstellations():
    return NUM_CONSTELLATIONS
def getConstName(idx):
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return None
    else:
        return __constDB[idx].sName
def getConstAbbrevName(idx):
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return None
    else:
        return __constDB[idx].sAbbrevName
def getConstMeaning(idx):   
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return None
    else:
        return __constDB[idx].sMeaning
def getConstCenterRA(idx):
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return UNKNOWN_RA_DECL
    else:
        return __constDB[idx].dCenterRA
def getConstCenterDecl(idx):
    """Gets the declination (Epoch J2000.0) for the constellation's center"""
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return UNKNOWN_RA_DECL
    else:
        return __constDB[idx].dCenterDecl
def getConstBrightestStarName(idx):
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return None
    else:
        return __constDB[idx].sBrightestStar
def getConstBrightestStarRA(idx):
    """Gets the right ascension (Epoch J2000.0) for the constellation's brightest star"""
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return UNKNOWN_RA_DECL
    else:
        return __constDB[idx].dStarRA
def getConstBrightestStarDecl(idx):
    """Gets the declination (Epoch J2000.0) for the constellation's brightest star"""
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        return UNKNOWN_RA_DECL
    else:
        return __constDB[idx].dStarDecl



#============================================================================
# Define functions for manipulating the constellations database
#============================================================================

def areConstellationsLoaded():
    """
    Gets whether the constellations database has been loaded.
    
    :return: true if constellations have been loaded.
    """
    return ASTConstellation._constDBReadIn



def displayAllConstellations():
    """Displays a list of all the constellations."""
    prt = ASTConstellation._prt
        
    __checkInitConstellationsDone()
    
    s = ASTStr.strFormat("%-20s ","Constellation Name") + ASTStr.strFormat("%5s ","Abrv") +\
        ASTStr.strFormat("%-30s ","   Meaning") + ASTStr.strFormat("%-25s","Brightest Star")
    prt.println(s)
    
    prt.println("="*90)
    for i in range(0,getNumConstellations()):
        s = ASTStr.strFormat("%-20s ",getConstName(i)) + ASTStr.strFormat("%5s ",getConstAbbrevName(i)) +\
            ASTStr.strFormat("%-30s ",getConstMeaning(i)) + ASTStr.strFormat("%-25s",getConstBrightestStarName(i))
        prt.println(s)



def displayConstellation(idx):
    """
    Displays data for a constellation.
    
    :param int idx: index into constDB for the constellation to display. 0-based indexing is assumed!
    """
    __checkInitConstellationsDone()
    
    prt = ASTConstellation._prt    
    
    if ((idx < 0) or (idx > (NUM_CONSTELLATIONS - 1))):
        prt.println("Constellation does not exist")
        return
    
    prt.setBoldFont(True)
    prt.println(getConstName(idx) + " (" + getConstAbbrevName(idx) +")",CENTERTXT)
    prt.setBoldFont(False)
    prt.println()
    
    prt.println("The Constellation's name means '" + getConstMeaning(idx) + "'")
    prt.printnoln("Its center is located at " + ASTTime.timeToStr_dec(getConstCenterRA(idx),HMSFORMAT) + " RA, ")
    prt.println(ASTAngle.angleToStr_dec(getConstCenterDecl(idx),DMSFORMAT) + " Decl (Epoch 2000.0)")
    prt.println()
    
    prt.printnoln("Its Brightest Star (" + getConstBrightestStarName(idx)+") ")
    prt.printnoln("is located at " + ASTTime.timeToStr_dec(getConstBrightestStarRA(idx),HMSFORMAT) + " RA, ")
    prt.println(ASTAngle.angleToStr_dec(getConstBrightestStarDecl(idx), DMSFORMAT) + " Decl (Epoch 2000.0)")
    prt.println()

    prt.println("The Constellation's boundaries (for Epoch 1875.0) are:")
    prt.setFixedWidthFont()
    
    s = ASTStr.strFormat("%-12s ","   Lower RA") + ASTStr.strFormat("%-12s ","   Upper RA") +\
        ASTStr.strFormat("%-16s","       Decl")
    prt.println(s)
    prt.println("="*43)
    
    for i in range(0,__getNumBoundaries()):
        n = __getConstIdx(i)
        if (n == idx):
            s = ASTStr.strFormat("%12s ",ASTTime.timeToStr_dec(__getRAlower(i),HMSFORMAT)) +\
                ASTStr.strFormat("%12s ",ASTTime.timeToStr_dec(__getRAupper(i),HMSFORMAT)) +\
                ASTStr.strFormat("%16s",ASTAngle.angleToStr_dec(__getDecl(i),DMSFORMAT))
            prt.println(s)            
    
    prt.setProportionalFont()        



def findConstellationByAbbrvName(targ):
    """
    Searches the constellations database and returns the index into the database for
    the requested constellation by its abbreviated name.
    
    The search performed is **not** case sensitive, but an exact match, ignoring white space,
    is required.

    :param str targ: Abbreviated name of the constellation to find
    :return: If successful, returns an index into the constellations database
             for the requested constellation. If unsuccessful, -1 is returned.
             0-based indexing is assumed!
    """
    __checkInitConstellationsDone()
    
    targStr = ASTStr.removeWhitespace(targ)
    
    for i in range(0,NUM_CONSTELLATIONS):
        if (ASTStr.compareIgnoreCase(targStr,getConstAbbrevName(i))):
            return i
        
    return -1               # not found



def findConstellationsByMeaning(targ):
    """
    Searches the constellations database and returns an index into the database for all
    constellations that contain the target substring in their 'meaning' field.
    
    The search performed is **not** case sensitive.

    :param str targ: target substring to search for
    :return: If successful, returns a list of indices into the constellations database
             for the requested constellations. If unsuccessful, the list is empty.
             0-based indexing is assumed for the indices!
    """
    result = []
    
    targStr = ASTStr.removeWhitespace(targ.lower())
    
    for i in range(0,NUM_CONSTELLATIONS):
        tmpStr = ASTStr.removeWhitespace(getConstMeaning(i).lower())
        if (tmpStr.find(targStr) >= 0):
            result.append(i)
    
    return result



def findConstellationByName(targ):
    """
    Searches the constellations database and returns the index into the database for
    the requested constellation by its name.
       
    The search performed is **not** case sensitive, but an exact match, ignoring white space,
    is required.

    :param str targ: Abbreviated name of the constellation to find
    :return: If successful, returns an index into the constellations database
             for the requested constellation. If unsuccessful, -1 is returned.
             0-based indexing is assumed!
    """
    __checkInitConstellationsDone()
    
    targStr = targ.strip()
    
    for i in range(0,NUM_CONSTELLATIONS):
        if (ASTStr.compareIgnoreCase(targStr,getConstName(i))):
            return i

    return -1            # not found



def findConstellationFromCoord(RAIn,DeclIn,EpochIn):
    """
    Find the constellation that a particular RA/Decl falls within.
    
    The code for this method was converted from the C and Fortran
    code found in the VizieR archives, http://cdsarc.u-strasbg.fr/viz-bin/Cat?VI/42. The
    code is translated somewhat literally, taking into account that array indexing in Python
    is 0 based whereas Fortran is not.

    :param float RAIn: right ascension in decimal hours (actually, hour angle)
    :param float DeclIn: declination in decimal degrees
    :param float EpochIn: epoch in which the RAIn/DeclIn are given
    :return: index into the constellations database for the constellation in which the
             RA/Decl falls, or -1 if there is a problem
    """
    # The author of the C program from which this translation came used a macro to adjust arrays
    # (e.g., translate X[i] to x[i-1]) to take the indexing base into account. The C author also
    # used macros to define the trig functions (e.g., #define DCOS    cos). Neither of these
    # conventions are preserved in the translation below. A major alteration to the original
    # algorithm is that the boundariesDB (boundaries database) is memory resident rather than
    # constantly opening and reading a datafile containing the constellation boundaries as
    # the original program did.

    iConstOut = -1
     
    __checkInitConstellationsDone()
     
    RAH = RAIn
    DECD = DeclIn

    # Convert to radians and then precess the position to the 1875.0 Epoch
    ARAD = CONVH * RAH
    DRAD = CONVD * DECD
    # precResult is a 2 element array with RA in element RA_IDX and decl in element DECL_IDX
    precResult = __HGTPrecession(ARAD,DRAD,EpochIn,E75)
    A = precResult[RA_IDX]
    D = precResult[DECL_IDX]
    if (A <  0.0):
        A=A + PI4
    if (A >= PI4):
        A=A - PI4
    
    # Convert radians back to degrees
    RA= A/CONVH
    DEC=D/CONVD

    # Now find the constellation such that the Declination entered is higher than
    # the lower boundary of the constellation when the upper and lower
    # Right Ascensions for the constellation bound the entered
    # Right Ascension
    for i in range(0,__getNumBoundaries()):
        RAL = __getRAlower(i)
        RAU = __getRAupper(i)
        DECL = __getDecl(i)
        
        if (DECL >  DEC):
            continue
        if (RAU <= RA):
            continue
        if (RAL >  RA):
            continue
        
        # If the constellation has been found, save the result and continue
        # to the next boundariesDB entry. Otherwise continue the search by
        # returning to RAU
        if ((RA >= RAL) and (RA <  RAU) and  (DECL <= DEC)):
            idx = __getConstIdx(i)
            iConstOut = idx
        elif (RAU <  RA):
            continue
        else:
            ASTMsg.errMsg("Constellation not found for RA/DECL" + str(RAIn) + "/" + str(DeclIn),
                       "Constellation not found")
        
        break

    return iConstOut



def initConstellations(prtInstance):
    """
    Does a one-time initialization by reading in the constellation data file.
    
    :param ASTPrt prtInstance: instance for performing output to the application's scrollable text output area
    """
    # See if we've already read in the constellations data file
    if (ASTConstellation._constDBReadIn):
        return
     
    if (prtInstance == None):
        ASTMsg.criticalErrMsg("An output text area cannot be null. Aborting program ...", ABORT_PROG)
         
    ASTConstellation._prt = prtInstance

    fileToRead = ASTConstellation._fullFilename

    # See if the file exists where it is expected. If not, ask the user to find it.
    if not (ASTFileIO.doesFileExist(ASTConstellation._fullFilename)):
        ASTMsg.errMsg("The constellations data file ("+ASTConstellation._dataFilename+") is missing.\n" +
                      "Please click 'OK' and then manually find the constellations data file",
                      "Missing Constellations Data File")
        
        fileToRead = ASTFileIO.getFileToRead("Load Constellation Data",ASTConstellation._fileExtFilter,
                                             ASTConstellation._fallbackDir,ASTConstellation._dataFilename)
        if (fileToRead == None) or (len(fileToRead) <= 0):
            ASTMsg.criticalErrMsg("Could not read the "+ASTConstellation._dataFilename+" file ... Aborting ...",ABORT_PROG)
        
    # If we get here, the constellation data file must exist
    # Note that we're using 'with open' so we don't have to explicitly close the file when we're done
    try:
        with io.open(fileToRead,"rt") as br:
            # Validate that the file is indeed a constellations data file
            strIn = ASTFileIO.readTillStr(br,"<Constellations>",HIDE_ERRORS)
            
            if (strIn == None):
                raise __ConstException
            
            # Now that we've found the constellations data file, read in the data.
            # Note that we must read the basic data and then the sorted data because
            # that's how the data file is structured.
            
            if (not __readBasicConstData(br)):
                ASTMsg.criticalErrMsg("The Constellations Data File is required but could\n" +
                                      "not be processed. Aborting program ...",ABORT_PROG)
                return                       
            
            # Although full initialization has not been completed, enough has been done
            # to have constellations in place to now read in the constellation boundaries.
            # We **must** set the flag here because reading in the sorted boundaries data assumes
            # that the basic constellations have already been successfully read in.

            ASTConstellation._constDBReadIn = True
                   
            if (not __readSortedBoundariesData(br)):
                ASTMsg.criticalErrMsg("The Constellations Data File is required but could\n" +
                                      "not be processed. Aborting program ...", ABORT_PROG)        
                return           
                    
        br.close()          # strictly speaking, this isn't necessary
    
    except __ConstException:
        ASTMsg.criticalErrMsg("The file specified is not a Constellations data\n" +
                              "file and could not be processed. Aborting program ...",ABORT_PROG)



#===================================================================
# Private functions only used internally to this module
#===================================================================

def __checkInitConstellationsDone():
    """
    Checks to see if the constellations have been properly initialized. This must
    be done to be sure none of the functions (e.g., displayAllConstellations)
    is invoked before the constellations have been initialized. This function
    doesn't return anything. It displays an error message and aborts the running
    program if there is a problem.
    """
    if (not ASTConstellation._constDBReadIn):
        ASTMsg.criticalErrMsg("The constellations database has not been loaded ... Aborting program ...",
                           ABORT_PROG)



def __HGTPrecession(RAIn,DeclIn,EpochIn,EpochOut):
    """
    This method performs a precession correction to convert an RA/Decl from one Epoch to another.
    
    The algorithm used here in the Herget Precession algorithm published on page 9
    of PUBL. CINCINNATI OBS. NO. 24. The code below was converted from the C and Fortran
    code found in the VizieR archives, http://cdsarc.u-strasbg.fr/viz-bin/Cat?VI/42. The
    code is translated almost literally, taking into account that array indexing in Python
    is 0 based whereas Fortran is not. The author of the C program from which this
    translation came used a macro to adjust arrays (e.g., translate X[i] to x[i-1])
    to take the indexing base into account. The C author also used macros to define
    the trig functions (e.g., #define DCOS    cos) which are not preserved
    in the translation below.

    :param float RAIn: Right Ascension in radians for EpochIn
    :param float DeclIn: Declination in radians for EpochOut
    :param float EpochIn: Epoch that RAIn/DeclIn is give in
    :param float EpochOut: Epoch to convert to
    :return: returns a vector with the precessed RA in item 0 and the
             precessed Decl in item 1 of the array
    """
    EP1 = 0.0
    EP2 = 0.0
    
    # Python arrays, especially multi-dimensional ones, are cumbersome. r will be 3x3,
    # result is a 2 element vector, x1 & x2 are 3 element vectors
    result = [0]*2          # RA is index RA_IDX, Decl is index DECL_IDX
    x1 = [0]*3
    x2 = [0]*3
    r = []
    for i in range(3):
        r.append([0]*3)
    # Now we can access r as r[row][col]

    # Compute input direction cosines
    A=math.cos(DeclIn)
    x1[0]=A*math.cos(RAIn)
    x1[1]=A*math.sin(RAIn)
    x1[2]=math.sin(DeclIn)
    
    # Set up rotation matrix (R)
    # Don't compare Epochs directly since equality comparison for real numbers is problematic due to roundoff errors
    if ((abs(EP1 - EpochIn) < ASTMath.EPS) and  (abs(EP2 - EpochOut) < ASTMath.EPS)):
        pass        # do nothing
    else:
        EP1 = EpochIn
        EP2 = EpochOut
        CSR=CDR/3600.0
        T=0.001*(EP2-EP1)
        ST=0.001*(EP1-1900.0)
        A=CSR*T*(23042.53+ST*(139.75+0.06*ST)+T*(30.23-0.27*ST+18.0*T))
        B=CSR*T*T*(79.27+0.66*ST+0.32*T)+A
        C=CSR*T*(20046.85-ST*(85.33+0.37*ST)+T*(-42.67-0.37*ST-41.8*T))
        SINA=math.sin(A)
        SINB=math.sin(B)
        SINC=math.sin(C)
        COSA=math.cos(A)
        COSB=math.cos(B)
        COSC=math.cos(C)
        r[0][0]=COSA*COSB*COSC-SINA*SINB
        r[0][1]=-COSA*SINB-SINA*COSB*COSC
        r[0][2]=-COSB*SINC
        r[1][0]=SINA*COSB+COSA*SINB*COSC
        r[1][1]=COSA*COSB-SINA*SINB*COSC
        r[1][2]=-SINB*SINC
        r[2][0]=COSA*SINC
        r[2][1]=-SINA*SINC
        r[2][2]=COSC
    
    # Perform the rotation to get the direction cosines at EpochOut
    for i in range(0,3):
        x2[i] = 0.0
        for j in range(0,3):
            x2[i] += r[i][j]*x1[j]

    result[0] = math.atan2(x2[1],x2[0])        # precessed RA
    if (result[0] <  0):
        result[0] = 6.28318530717948 + result[0]
    result[1] = math.asin(x2[2])               # precessed Decl
    
    return result



def __readBasicConstData(br):
    """
    Reads the constellations data file to extract basic info about the constellations
    
    :param stream br: file to read from
    :return: false if an error occurs
    """
    # count will be used to count the # of constellations actually read. If it differs from
    # NUM_CONSTELLATIONS, we have an error and must abort.
    count = 0

    strIn = ASTFileIO.readTillStr(br,"<Data>")
    if (strIn == None):
        return False

    for strIn in br:
        # Get rid of the line terminator, which differs between Windows (CRLF),
        # Unix (LF), MAC OS (CR).
        strIn = strIn.strip()
        
        str2 = ASTStr.removeWhitespace(strIn.lower())
        if (str2.find("</data>") >= 0):             # must use lower case since previous statement set it to LC!
            if (count == NUM_CONSTELLATIONS):
                return True
            else:
                ASTMsg.criticalErrMsg("The constellations data file ("+ASTConstellation._dataFilename+
                                      ") may be corrupted. It\n"+"contains "+str(count)+
                                      " Constellations but should contain "+str(NUM_CONSTELLATIONS)+
                                      ". Thus, we must abort ...",ABORT_PROG)
                return False
                
        count += 1
        if (count > NUM_CONSTELLATIONS):                # Make sure we don't try to store too much data in constDB
            continue
        
        parts = strIn.split(",")
        
        if (len(parts) != 8):
            ASTMsg.criticalErrMsg("Error while reading the constellation basic data\n" + "at string ["+strIn+"]")
            return False

        abbrevName = parts[0].strip()
        name = parts[1].strip()
        meaning = parts[7].strip()
        brightestStar = parts[4].strip()
        
        # Get the RA/Decl coordinates for the center of the constellation
        tmpReal = ASTReal.isValidReal(parts[2],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            centerRA = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid RA for the constellation center\n" + "at string ["+strIn+"]")
            return False
        
        tmpReal = ASTReal.isValidReal(parts[3],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            centerDecl = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid Decl for the constellation center \nat string ["+strIn+"]")
            return False 
        
        # Get the RA/Decl coordinates for the brightest star in the constellation
        tmpReal = ASTReal.isValidReal(parts[5],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            starRA = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid RA for the Brightest Star\nat string ["+strIn+"]")
            return False
        
        tmpReal = ASTReal.isValidReal(parts[6],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            starDecl = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid Decl for the Brightest Star\nat string ["+strIn+"]")
            return False
        
        __constDB.append(ASTConstellation(name,abbrevName,meaning,centerRA,centerDecl,
                                          brightestStar,starRA,starDecl))

    return True



def __readSortedBoundariesData(br):
    """
    Reads the sorted database of constellations and puts it into the boundariesDB structure.
    
    :param stream br: file to read from
    :return: false if an error occurs
    """
    # Look for the start of the constellation boundaries data
    strIn = ASTFileIO.readTillStr(br,"<SortedBoundaries>")
    if (strIn == None):
        return False
    strIn = ASTFileIO.readTillStr(br,"<Data>")
    if (str == None):
        return False
    
    for strIn in br:
        # Get rid of the line terminator, which differs between Windows (CRLF),
        # Unix (LF), MAC OS (CR).
        strIn = strIn.strip()
        
        str2 = ASTStr.removeWhitespace(strIn.lower())
        if (str2.find("</data>") >= 0):             # must use lower case since previous statement set it to LC!
            return True
        
        # Now parse the input string
        parts = strIn.split(",")
        
        if (len(parts) != 4):
            ASTMsg.criticalErrMsg("Error in the constellation boundaries data\nat string ["+strIn+"]")
            return False
        
        # Get the constellation boundaries (RA/Decl)
        tmpReal = ASTReal.isValidReal(parts[0],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            RAlower = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid Lower RA in the constellation boundaries data\nat string ["+strIn+"]")
            return False
        tmpReal=ASTReal.isValidReal(parts[1],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            RAupper = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid Upper RA in the constellation boundaries data\nat string ["+strIn+"]")
            return False
        tmpReal=ASTReal.isValidReal(parts[2],HIDE_ERRORS)
        if (tmpReal.isValidRealObj()):
            Decl = tmpReal.getRealValue()
        else:
            ASTMsg.criticalErrMsg("Invalid Declination in the constellation boundaries data\nat string ["+strIn+"]")
            return False

        # Find the index into the constDB for this boundary
        targ = parts[3].strip()
        idx = findConstellationByAbbrvName(targ)
        if (idx < 0):
            ASTMsg.criticalErrMsg("Could not match boundary with a Constellation\nat string ["+strIn+"]")

        __boundariesDB.append(__BoundaryData(RAlower,RAupper,Decl,idx))

    return True



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
