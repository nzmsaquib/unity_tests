"""
Provides the GUI to let a user choose a filename
and path for loading a data file, typically a star catalog
but also other files (e.g., constellations data file)
if the file is not where it was expected to be.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import os
from tkinter.filedialog import askopenfilename

from ASTUtils.ASTMisc import SHOW_ERRORS, HIDE_ERRORS
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTStr as ASTStr

def getFileSize(fName):
    """
    Get the size of a file
    
    :param str fName: file name, including path, whose size is desired
    :return: size of the file in bytes, or -1 if the file does not exist
    """
    if doesFileExist(fName):
        return os.path.getsize(fName)
    else:
        return -1     



def getFileToRead(title,extfilter,dName,fName=""):
    """
    Gets the file to open for reading.
    
    :param str title: title to display for the dialog window
    :param str extfilter: filter to use for displaying files by extension type
    :param str dName: initial directory to start in
    :param str fName: initial filename to look for
    :return: filename user chose or None if the user cancelled
    """
    # A filter is in the form [('Data Files','*.dat'), ('All Files','.*')] 

    return askopenfilename(title=title,filetypes=extfilter,initialdir=dName,initialfile=fName)



def doesFileExist(fullFilename):
    """
    Determines whether a specified file already exists. This function
    does not create the file if it does not exist.

    :param str fullFilename: full pathname and filename to be checked
    :return: true if file exists, else false
    """
    if (fullFilename == None) or (len(fullFilename) <= 0):
        return False
    else:
        return os.path.isfile(fullFilename)
    


def getNonBlankLine(br):
    """
    Reads the input file to get a non-blank line.
    
    :param stream br: file to read from
    :return: a non-blank line or null if reached end of file
    """
    for strIn in br:
        # Get rid of the line terminator, which differs between Windows (CRLF),
        # Unix (LF), MAC OS (CR).
        strIn = strIn.strip()
        if (len(strIn) > 0):
            return strIn

    # We only get here if EOF was reached
    return None



def getSimpleTaggedValue(br,tag):
    """
    Reads the input file looking for a tag, then returns the value
    found for that tag.
    
    For example, a tag might be <Epoch> and could exist in the
    file in multiple ways: (1) <Epoch>, next line has value, next line has </Epoch>,
    (2) <Epoch>value</Epoch> on a single line, (3) <Epoch>, next line has value</Epoch>,
    and so on. This function is only intended to be used for XML-style tags that have simple
    values and not for tags (such as <Data>) that will likely have multiple lines of input.
    If used for a multi-line tag, only the first line will be extracted as the tag value.

    :param stream br: file to read from.
    :param str tag: Simple "XML-like" tag to search for
    :return: string containing the value field for the given tag, a null if the
             tag does not exist, or "" if the tag exists but has no value field
    """
    endTag = tag.replace("<","</",1)
    
    strIn = readTillStr(br,tag,HIDE_ERRORS)                # readTillStr will worry about upper vs lowercase
    if ((strIn == None) or (len(strIn) <= 0)):
        return None
         
    strIn = strIn.replace(tag,"",1).strip()                #remove the tag from the line and see what's left
    if ((strIn == None) or (len(strIn) <= 0)):             # value must be on the next line
        strIn = getNonBlankLine(br)
        if (strIn == None) or (len(strIn) <= 0):
            strIn = ""
        else:
            strIn = strIn.strip()
     
    # Replace the endTag, if it's there, with a null string. Whatever is left over
    # is the result we need to return.
    strIn = strIn.replace(endTag,"").strip()

    return strIn



def readTillStr(br,target,showErrors=SHOW_ERRORS):
    """
    Reads the input file until the expected string is found or an error occurs.
    
    The string to search for is **not** case sensitive.

    :param stream br: file to read from
    :param str target: string to search for
    :param bool showErrors: true if error messages should be displayed
    :return: target string if it was found else a null string
    """
    targ = ASTStr.removeWhitespace(target.lower())
    
    for line in br:
        # Get rid of the line terminator, which differs between Windows (CRLF),
        # Unix (LF), MAC OS (CR).
        line = line.rstrip()
        
        str2In = ASTStr.removeWhitespace(line.lower())
        if (str2In.find(targ) >= 0):
            return line
        
    if (showErrors):
        ASTMsg.criticalErrMsg("Expected but did not find '" + target + "'")
        
    return None



#=========== Main entry point ===============
if __name__ == '__main__':
    pass

    