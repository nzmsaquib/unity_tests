"""
Provides several math-related utilities.

These functions are provided to be sure they perform the
calculations as required by the programs and to simplify
translating the code to another language. For example, the
Visual Basic function Truncate and the Python math.trunc
function already do what the Trunc function defined below
does and hence Trunc is unnecessary for Visual Basic
and Python. But, the Trunc function is defined here
because other languages, such as Java, do not have a
built-in Truncate function.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

import ASTUtils.ASTVect as ASTVect

# Define a value to use when comparing small real numbers to see if they are the same.
# Comparing reals for strict equality will often fail due to roundoff errors, so just
# check for "close enough" in those cases.
EPS = 0.00000005



#=====================================================
# Define some general math routines
#=====================================================

def cuberoot(x):
    """
    Find the cube root of a number.
    
    This function is provided to make it easier to convert to
    other languages, which may already have a cube root function.
    For example, the Java Math class has a built in cube root method.

    :param float x: value whose cube root is desired
    :return: cube root of x
    """
    return math.pow(x,(1.0/3.0))



def Frac(x):
    """
    Returns the fractional part of a number.
    
        Frac(1.5) = 0.5, Frac(-1.5) = 0.5

    :param float x: value whose decimal part is desired
    :return: the decimal part of the value passed
    """
    return abs(x) - abs(math.trunc(x))



def isClose(x,y):
    """
    Determine if the number x is 'close' to the number y
    
    :param float x: first number to compare
    :param float y: nd number to compare
    :return: true if x is 'close' to y
    """
    
    return abs(x-y) <= EPS



def isEvenlyDivisible(n,m):
    """
    Determines whether n is evenly divisible by m.
    That is, return true if (n MOD m) is 0, otherwise
    return false.

    :param int n: number to check
    :param int m: divisor
    :return: true if n is evenly divisible by m, else false
    """
    return ((n % m) == 0)



def Round(x,d=0):
    """
    Round a number to the specified number of digits.
    
    :param float x: number to round
    :param int d: number of digits after the decimal point
    :return: x rounded to d digits
    """
    y = round(x,int(d))
    if (d == 0):
        return int(y)
    else:
        return y



def signum(x):
    """
    Return the sign of a number.
    
    :param int/float x: number whose sign is desired
    :return: -1.0 if x < 0, 0.0 if x == 0, and 1.0 if x > 0.0
    """
    
    # Can't use math.copysign because math.copysign(1,0) returns 1.0 rather than 0.0
    if (x < 0):
        return -1.0
    elif (x > 0):
        return 1.0
    else:
        return 0.0
    


def Trunc(x):
    """
    Gets the integer part of a number.
    
        Trunc(1.5) = 1, Trunc(-1.5) = -1, etc.

    Some languages have built in functions that perform
    this operation, such as Visual Basic's Truncate function.
    Also, some languages have a FIX function, which is ***NOT***
    the same as the function defined here.

    :param float x: value whose integer part is desired
    :return: the integer part of the value passed
    """
    return math.trunc(x)



def xMOD(x,div):
    """
    Returns x modulo div. For example:
        xMOD(270,180) = 90, xMOD(-270.87,180) = 89.13,
        xMOD(-400,360) = 320, xMOD(-100,8) = 4.
        
    Some languages have built in functions that perform
    this operation, such as Visual Basic's MOD operator.
    However, they may not handle negative numbers the way
    required for this book. (Visual Basic will return a
    negative number if x is negative whereas this function
    always returns a positive number.)

    :param int/flaot x: x value in x MOD divisor
    :param int/float div: divisor
    :return:  x modulo div
    """
    return x % div



#================================================================================
# Define some useful trigonometric routines to use degrees rather than radians.
#================================================================================

def deg2rad(deg):
    """
    Converts degrees to radians
    
    :param float deg: angle in degrees
    :return: angle in radians
    """
    return math.radians(deg)



def rad2deg(rad):
    """
    Converts radians to degrees
    
    :param float rad: angle in radians
    :return: angle in degrees
    """
    return math.degrees(rad)

#================= Various trig functions to accept degrees rather than radians ===========

def SIN_D(deg):
    """
    Compute the sine of an angle
    
    :param float deg: angle in degrees
    :return: sine of the angle
    """
    return math.sin(deg2rad(deg))



def COS_D(deg):
    """
    Compute the cosine of an angle
    
    :param float deg: angle in degrees
    :return: cosine of the angle
    """
    return math.cos(deg2rad(deg))



def TAN_D(deg):
    """
    Compute the tangent of an angle
    
    :param float deg: angle in degrees
    :return: tangent of the angle
    """
    return math.tan(deg2rad(deg))



def INVSIN_D(x):
    """
    Computes the inverse sine of a value
    
    :param float x: value whose inverse sine is desired
    :return: angle (in degrees) whose sine is x
    """
    return rad2deg(math.asin(x))



def INVCOS_D(x):
    """
    Computes the inverse cosine of a value
    
    :param float x: value whose inverse cosine is desired
    :return: angle (in degrees) whose cosine is x
    """
    return rad2deg(math.acos(x))



def INVTAN_D(x):
    """
    Computes the inverse tangent of a value
    
    :param float x: value whose inverse tangent is desired
    :return: angle (in degrees) whose tangent is x
    """
    return rad2deg(math.atan(x))



def INVTAN2_D(y,x):
    """
    Computes the arctangent of y/x, corrected for the quadrant the resulting angle is in.
    
    :param float y: y coordinate
    :param float x: x coordinate
    :return: angle (in degrees) whose tangent is y/x
    """
    return INVTAN_D(y/x) + quadAdjust(y,x)



def quadAdjust(y,x):
    """
    Compute a quadrant adjustment for the inv tan
    function given x and y.

    :param float y: y coordinate
    :param float x: x coordinate
    :return: quadrant adjustment
    """    
    dQA = 0.0
    if ((y > 0.0) and (x < 0.0)):
        dQA = 180.0
    elif ((y < 0.0) and (x < 0.0)):
        dQA = 180.0
    elif ((y < 0.0) and (x > 0.0)):
        dQA = 360.0
        
    return dQA



#==============================================================
# Define some simple distance conversion routines
#==============================================================

def KM2Miles(km):
    """
    Convert kilometers to miles
    
    :param float km: kilometers to convert
    :return: miles
    """
    return km * 0.62137119



def Miles2KM(miles):
    """
    Convert miles to kilometers
    
    :param float miles: miles to convert
    :return: kilometers
    """
    return miles * 1.609344



def AU2KM(AU):
    """
    Convert AUs to kilometers
    
    :param float AU: AUs to convert
    :return: kilometers
    """
    return AU * 149597870.691



def KM2AU(KM):
    """
    Convert kilometers to AUs
    
    :param float KM: kms to convert
    :return: AUs
    """
    return KM / 149597870.691



#=============================================================================
# Define a couple of rotations that are needed for the satellites chapter.
#==============================================================================

def RotateX(theta,V):
    """
    Rotate about the x-axis. This is the book's 'f' family of functions.
    
    :param float theta: angle, in degrees, to rotate
    :param ASTVect V: vector to rotate
    :return: the input vector rotated by theta degrees about the x-axis
    """
    F = ASTVect.ASTVect()
    
    x1 = V.x()
    y1 = V.y() * COS_D(theta) + V.z() * SIN_D(theta)
    z1 = V.z() * COS_D(theta) - V.y() * SIN_D(theta)
    
    F.setVect(x1, y1, z1)
    return F



def RotateZ(theta,V):
    """
    Rotate about the z-axis. This is the book's 'g' family of functions.
    
    :param float theta: angle, in degrees, to rotate
    :param ASTVect V: vector to rotate
    :return: the input vector rotated by theta degrees about the z-axis
    """
    G = ASTVect.ASTVect()
    
    x1 = V.x() * COS_D(theta) + V.y() * SIN_D(theta)
    y1 = V.y() * COS_D(theta) - V.x() * SIN_D(theta)
    z1 = V.z()
    
    G.setVect(x1, y1, z1)
    return G



#=============================================================================
# Define some affine transformation manipulations for the satellites
# chapter. These are used to simplify graphics in ASTCharts. Python
# does not natively support matrices/arrays (except as lists) nor
# does it do matrix math. The '@' operator will eventually be implemented
# in Python to do matrix math. Also, packages such as numpy exist that
# do the manipulations we need, but the user would then have to locate,
# download, and install such packages. Because we need so few methods,
# we implement them here so that the user doesn't have to install
# anything else. For completeness sake, we provide more methods
# than are actually used in the programs.
#
#                    IMPORTANT NOTE
#
# These routines are specifically for 3x3 matrices and 1x3 vectors!
# They will not work for other dimensions.
#==============================================================================

def createAffineMatrix(row1,row2):
    """
    Create a 3x3 affine matrix from the 2 rows given
    
    :param row1,row2: 1x3 rows (e.g., [a,b,c], [e,f,g]
    :return: 3x3 affine matrix
    """
    return [row1,row2,[0.0, 0.0, 1.0]]
    
    
    

def createIdentityMatrix():
    """
    Initialize a 3x3 array to the identity matrix
    
    :return: 3x3 identity matrix
    """
    return [[1.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,1.0]]



def createScaleMatrix(xScale,yScale):
    """
    Create a matrix that scales by xScale, yScale
    
    :param float xScale: how much to scale in the x-dimension
    :param float yScale: how much to scale in the y-dimension
    :return: 3x3 affine transformation to scale a point
    """
    return [[xScale,0.0,0.0],[0.0,yScale,0.0],[0.0,0.0,1.0]] 



def createRotateCCWMatrix(theta):
    """
    Create a matrix that rotates by theta degrees counter-clockwise
    
    :param float theta: how much to rotate a point in the CCW direction
    :return: 3x3 affine transformation to rotate a point CCW by theta degrees
    """
    costheta = math.cos(math.radians(theta))
    sintheta = math.sin(math.radians(theta))
    return [[costheta,-sintheta,0.0],[sintheta,costheta,0.0],[0.0,0.0,1.0]]
 


def createRotateCWMatrix(theta):
    """
    Create a matrix that rotates by theta degrees clockwise
    
    :param float theta: how much to rotate a point in the CW direction
    :return: 3x3 affine transformation to rotate a point CW by theta degrees
    """ 
    costheta = math.cos(math.radians(theta))
    sintheta = math.sin(math.radians(theta))
    return [[costheta,sintheta,0.0],[-sintheta,costheta,0.0],[0.0,0.0,1.0]]



def createXlateMatrix(xnew,ynew):
    """
    Create a matrix that translates the point 0,0 to xnew,ynew
    
    :param float xnew,ynew: how much to move a point by in the
                            x and y directions
    :return: 3x3 affine transformation to translate (0,0) to (xnew,ynew)
    """
    return[[1.0,0.0,xnew],[0.0,1.0,ynew],[0.0,0.0,1.0]]
    

   
def mult3DMatrices(A,B):
    """
    Multiply matrices A and B (e.g., result=AB, not BA!).
    Both matrices must have 3 rows and 3 columns.
    
    :param matrix A,B: matrices to multiply
    :return: the 3x3 matrix AB 
    """
    result = [[0.0]*3, [0.0]*3, [0.0]*3]
    for i in range(len(A)):          # iterate through rows of A
        for j in range(len(B[0])):   # iterate through columns of B
            for k in range(len(B)):  # iterate through rows of B
                result[i][j] += A[i][k]*B[k][j]
                
    return result



def multMatrixVect(A,X):
    """
    Multiply matrix A and vector X (e.g., result = AX).
    A must be a 3x3 matrix and X must be a 3x1 vector (x=[a,b,c])
    
    :param matrix A, vect X: objects to multiply
    :return: the 3x1 vector AX
    """
    result = [0.0]*3
    for i in range(len(A)):         # iterate through rows of A
        for j in range(len(X)):     # iterate through rows of X
            result[i] += A[i][j]*X[j]
            
    return result
    


#=========== Main entry point ===============
if __name__ == '__main__':
    pass
