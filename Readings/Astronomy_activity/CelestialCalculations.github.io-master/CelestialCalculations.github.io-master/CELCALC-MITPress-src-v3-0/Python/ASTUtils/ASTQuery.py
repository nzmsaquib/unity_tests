"""
This module provides a GUI to allow a user to enter
multiple lines of data.

Although the code is general and allows an arbitrary
number of input data lines, it should only be used for
5-6 lines of data to keep the input dialog window
from being too big.
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import tkinter as tk
import tkinter.simpledialog as tkSimpleDialog

import ASTUtils.ASTStyle as ASTStyle

# Define OK and Cancel to make the response language independent
QUERY_OK = True
QUERY_CANCEL = not QUERY_OK



class ASTQuery(tkSimpleDialog.Dialog):
    """Class for multi-line input dialogs"""
    #==================================================================
    # Class variables
    #
    # __dataResults will be used to save the results that the user
    # entered. In Python one would normally do
    #    x=ASTQuery(parent,title,lines)
    # and check the result to see if x.result is None, meaning that
    # the user cancelled. Then one would immediately obtain the user
    # data from x.result[0], x.result[1], etc. However, in our case
    # the dialog object is destroyed before the calling routine gets
    # the data from the dialog. So, we'll save the most recent results
    # in __dataResults and get the data from there. This is done rather
    # than following Python practices to make porting to other
    # languages easier.
    #
    # THE RESULTS RETURNED WILL BE FOR THE LAST DIALOG DISPLAYED! THIS
    # APPROACH WILL NOT WORK IF THERE NEED TO BE TWO DIALOGS UP
    # SIMULTANEOUSLY.
    #==================================================================
    __dataResults = []
    
    # Save a reference to the application's parent so that we can put dialogs on top of the
    # parent frame without having to pass in the parent frame each time. This should
    # ***always*** be the top level application window
    __parentFrame = None
    
    #==================================================================
    # Class instance variables
    #==================================================================
    # lines                 list of query text for forming the dialog
    # entries               array of entries that user entered
    
    # Note: the parent class sets result = None, and we can't override
    # that very easily. So, we'll use entries instead and set the result
    # to be entries in the apply method below.
        
    def __init__(self,parent,title,lines):
        """
        Create a query instance
        
        :param tkwidget parent: parent widget for a query dialog
        :param str title: title for the query dialog window
        :param str list lines: list of lines for the queries. The length
                               of this list determines how many queries
                               to ask for at a time.
        """
        # Must save instance variables before super().init() because super calls body
        self.lines = lines
        self.entries = []
        ASTQuery.__dataResults = []         # delete the old query results
        super().__init__(parent=parent,title=title)  
        
    def body(self, parent):         
        self.e1 = None
         
        for line in self.lines:
            tk.Label(parent,text=line,font=ASTStyle.TEXT_BOLDFONT).pack()
            ent = tk.Entry(parent,font=ASTStyle.TEXT_FONT)
            self.entries.append(ent)
            if (self.e1 == None):
                self.e1 = ent
            # For some reason, we have to do the pack after the Entry is created
            # or else a get() on the entry will not work
            ent.pack(fill=tk.X)      
 
        return self.e1 # initial focus
         
    def apply(self):
        # If we get here, the user must have clicked on OK.
        # Change result from None to True since super().__init__() sets
        # result initially to None, and save the results for later use.     
        
        for i in self.entries:
            ASTQuery.__dataResults.append(i.get())
        self.result = True
    
    # These static methods are used so that __dataResults and __parentFrame
    # are accessible inside the module, but not readily visible outside it.
    # Can't use double underscore to name these or else they won't be
    # visible outside the class.
    
    @staticmethod
    def _getDResults():
        # Return the most recent data results
        return ASTQuery.__dataResults
    
    @staticmethod
    def _getClassParentFrame():
        return ASTQuery.__parentFrame
    
    @staticmethod
    def _setClassParentFrame(parent):
        ASTQuery.__parentFrame = parent
        


#==========================================================
# Define functions for manipulating queries
#==========================================================

def getData(i):
    """
    Get the data that the user entered.
    
    :param int i: which data entry to get (1 for the first entry, not 0!)
    :return: string that the user entered
    """
    dr = ASTQuery._getDResults()
    
    if (len(dr) <= 0):
        return None
    
    if ((i-1) < 0) or ((i-1) > len(dr)):
        return None
    return dr[i-1]
    


def setParentFrame(parent):
    """
    Saves a reference to the caller's parent frame so error messages are placed
    on top of the parent application's window. This **must** be called, typically
    from the code that creates the top level GUI, before any functions in this
    module are invoked.

    :param tkwidget parent: parent frame for the main application
    """
    ASTQuery._setClassParentFrame(parent)
    


def showQueryForm(lines,title=None):
    """
    Show the query form and get user results
    
    :param list lines: list (not a tuple!) of labels for the dialog
    :param str title: tile for the query window
    :return: QUERY_OK if user clicked on OK, else QUERY_CANCEL
    """
    # WARNING: lines must be a list, not a tuple. This only
    # causes problems if lines has a single entry.
    # lines = ("string") is a string, not a tuple nor a
    # list and WILL cause problems.
    
    if (title == None):
        winTitle = "Enter Data ..."
    else:
        winTitle = title
    
    if (ASTQuery(ASTQuery._getClassParentFrame(),winTitle,lines).result == None):
        return QUERY_CANCEL
    else:
        return QUERY_OK



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
