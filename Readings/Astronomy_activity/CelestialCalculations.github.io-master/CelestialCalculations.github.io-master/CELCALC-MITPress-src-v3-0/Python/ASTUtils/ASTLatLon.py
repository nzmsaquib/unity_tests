"""
This class implements a Lat/Lon object and related methods,
such as time zones and validating lat/lon strings.

In the validation routines, lat/lon can usually be entered in either
decimal (e.g., 95.63W) or DMS format (e.g., 9d 15m 33sN).
 
Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

from enum import Enum

import ASTUtils.ASTAngle as ASTAngle
import ASTUtils.ASTMath as ASTMath
import ASTUtils.ASTMsg as ASTMsg
import ASTUtils.ASTStr as ASTStr
from ASTUtils.ASTMisc import POS_DIRECTION, NEG_DIRECTION, SHOW_ERRORS, HIDE_ERRORS, DMSFORMAT
from ASTUtils.ASTStyle import LATLONFORMAT

# These flags are used just to make the code more readable
__DO_LATITUDE = True
__DO_LONGITUDE = not __DO_LATITUDE



class TimeZoneType(Enum):
    """
    The numeric value of the time zone type is the time zone adjustment. These are
    negative because West longitudes are negative.
    """
    PST = (-8,"PST")                    # use Pacific Standard Time zone    
    MST = (-7,"MST")                    # use Mountain Standard Time zone
    CST = (-6,"CST")                    # use Central Standard Time zone
    EST = (-5,"EST")                    # use Eastern Standard Time zone        
    LONGITUDE = (0,"Longitude")         # use longitude rather than a time zone
    
    def getAdjust(self):                # adjustment factor for this time zone
        return self.value[0]
    def toString(self):                 # printable string for this enumerated type
        return self.value[1]



def timeZoneAdjustment(tZone,lon):
    """
    Calculate a time zone adjustment
    
    :param TimeZoneType tZone: time zone
    :param float lon: longitude
    :return: a time zone adjustment
    """
    if (tZone == TimeZoneType.LONGITUDE):
        return ASTMath.Round((lon/15.0),0)
    else:
        return tZone.getAdjust()
    


def intToTZType(i):
    """
    Convert an integer value to a TimeZoneType
    
    :param int i: integer to convert
    """
    # If i is out of range, return LONGITUDE as the default
    for tz in TimeZoneType:
        if (tz.getAdjust() == i):
            return tz
    return TimeZoneType.LONGITUDE



def strToTimeZone(tzStr):
    """
    Convert a string to a time zone type
    
    :param str tzStr: string to convert
    :return: tzStr converted to a time zone type
    """
    tzStr = tzStr.strip()
    
    for zone in TimeZoneType:
        if (ASTStr.compareIgnoreCase(tzStr,zone.toString())):
            return zone
                
    return TimeZoneType.LONGITUDE



class ASTLatLon():
    """Define a class for latitude/longitude objects"""
    #===================================================================
    # Class instance variables
    #
    # Lat/lon objects hold both the DMS and decimal version of the
    # lat/lon. POS_DIRECTION is for North latitudes and East Longitudes
    #===================================================================

    # Latitude fields ------------------------------------------------------------------
    # bLatValid                true if the object contains a valid latitude
    # bLatDir                  North latitude if bLatDir = POS_DIRECTION, else South Latitude
    # latAngle                 actual value for the latitude
    # Longitude fields ------------------------------------------------------------------
    # bLonValid                true if the object contains a valid longitude
    # bLonDir                  East longitude if bLonDir = POS_DIRECTION, else West Longitude    
    # lonAngle                 actual value for the longitude

    def __init__(self):
        """
        Class constructor.
        
        Since much of the usage of this class involves validating that a string has a valid lat
        and/or lon, assume a new object is invalid until it is proven otherwise.
        """
        # latitude fields
        self.bLatValid = False
        self.bLatDir = POS_DIRECTION        # assume North latitude by default
        self.latAngle = ASTAngle.ASTAngle()
        # longitude fields
        self.bLonValid = False
        self.bLonDir = POS_DIRECTION        # assume East longitude by default    
        self.lonAngle = ASTAngle.ASTAngle()

    #========================================================
    # Define 'get/set' accessors for the object's fields
    #========================================================
    
    # Latitude fields ---------------------------------------
    def isLatValid(self):
        return self.bLatValid
    def isNorthLat(self):
        return self.bLatDir
    def getLatAngle(self):
        return self.latAngle
    def getLat(self):
        return self.latAngle.getDecAngle()
    def setLat(self,lat):
        """
        Sets the latitude value. This method assumes
        that the latitude is valid and does **not** check it

        :param float lat: latitude as a real number
        """
        self.bLatValid = True
        if (lat >= 0):
            self.bLatDir = POS_DIRECTION
        else:
            self.bLatDir = NEG_DIRECTION
        self.latAngle.setDecAngle(lat)   

    # Longitude fields ---------------------------------------
    def isLonValid(self):
        return self.bLonValid
    def isEastLon(self):
        return self.bLonDir
    def getLonAngle(self):
        return self.lonAngle
    def getLon(self):
        return self.lonAngle.getDecAngle()
    def setLon(self,lon):
        """
        Sets the longitude value. This method assumes
        that the longitude is valid and does **not** check it

        :param float lon: longitude as a real number
        """
        self.bLonValid = True
        if (lon >= 0):
            self.bLonDir = POS_DIRECTION
        else:
            self.bLonDir = NEG_DIRECTION
        self.lonAngle.setDecAngle(lon)
     


#====================================================================================
# Define some general methods for validating that a string contains a valid lat/lon
# and for converting a lat/lon to a displayable string
#====================================================================================

def lat_lonToStr(latlonObj,formatFlag):
    """
    Convert a lat/lon object to a string
    
    :param ASTLatLon latlonObj: object to convert to a string
    :param bool formatFlag: flag for how to format the result (DMSFORMAT or DECFORMAT)
    :return: a printable lat/lon string
    """
    return latToStr_obj(latlonObj,formatFlag) + ", " + lonToStr_obj(latlonObj,formatFlag)
    
# ================================== Latitude methods =============================================*/

def isValidLat(inputStr,flag=SHOW_ERRORS):
    """
    Checks a string to see if it contains a valid latitude value. The string format can be
    either DMS (###d ##m ##.##s[N or S]) or decimal (###.######[N or S]). If N/S is omitted,
    the latitude is assumed to be N. Specifying a sign in addition to, or in lieu of, N or S
    is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling routine
    display its own message.
    
    This routine returns the validated values in the latlonObj (latitude only is set). The 'degrees'
    field in the latitude angle will always be non-negative with bLatDir (returned by isNorthLat)
    being POS_DIRECTION if the latitude is a North latitude. The real value in the angle object
    (use getLat()) will be negative if the value is for a South latitude, and bLatDir will
    be set to NEG_DIRECTION.

    :param str inputStr: string to be checked for a valid latitude
    :param bool flag: whether to display error messages
    :return: a latlonObj object with the results (only latitude filled in)
    """
    return __validateLatOrLon(inputStr,__DO_LATITUDE,flag)



#================ IMPORTANT NOTE =================================
# Python doesn't allow overloading functions and named parameters
# would be too cumbersome in this case. single-dispatch generic 
# functions are in the latest Python 3, but if we used them, it 
# would make it more difficult to back port to Python 2. So, we're
# stuck with having unique functions for each case.
#================ IMPORTANT NOTE =================================

def latToStr_dms(pos,d,m,s,formatFlag):
    """
    Converts latitude to a printable string as determined by formatFlag
    
    :param bool pos: true if the latitude is positive
    :param int d: latitude degrees
    :param int m: latitude minutes
    :param float s: latitude seconds
    :param bool formatFlag: what format is desired (DMSFORMAT or DECFORMAT)
    :return: a printable string for the latitude
    """
    if (formatFlag == DMSFORMAT):
        return __latOrLonToDMSStr(__DO_LATITUDE, pos, d, m, s)
    else:
        # Note: have to do not pos because DMStoDEC uses true to indicate negative value
        return __latOrLonToDecStr(__DO_LATITUDE, ASTAngle.DMStoDec(not pos, d, m, s))



def latToStr_obj(latlonObj,formatFlag):
    """
    Converts latitude to a printable string as determined by formatFlag.
    
    :param ASTLatLon latlonObj: object to be converted to a string
    :param bool formatFlag: what format is desired (DMSFORMAT or DECFORMAT)
    :return: a printable string for the latitude
    """
    return latToStr_dms(latlonObj.bLatDir,latlonObj.latAngle.getDegrees(),
                        latlonObj.latAngle.getMinutes(),latlonObj.latAngle.getSeconds(),
                        formatFlag)
     
#=============================== Longitude methods ================================================*/

def isValidLon(inputStr,flag=SHOW_ERRORS): 
    """
    Checks a string to see if it contains a valid longitude value. The string format can be
    either DMS (###d ##m ##.##s[W or E]) or decimal (###.######[W or E]). If W/E is omitted,
    the longitude is assumed to be W. Specifying a sign in addition to, or in lieu of, W or D
    is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling
    routine display its own message.
    This routine returns the validated values in the latlonObj (longitude only is set). The 'degrees'
    field in the longitude angle will always be non-negative with bLonDir (returned by isEastLon)
    being POS_DIRECTION if the longitude is an East longitude. The real value in the angle object
    (use getLon()) will be negative if the value is for a West longitude, and bLonDir will
    be set to NEG_DIRECTION.

    :param str inputStr: string to be checked for a valid longitude
    :param bool flag: whether to display error messages
    :return: a latlonObj object with the results (only longitude filled in)
    """
    return __validateLatOrLon(inputStr,__DO_LONGITUDE,flag)



#================ IMPORTANT NOTE =================================
# Python doesn't allow overloading functions and named parameters
# would be too cumbersome in this case. single-dispatch generic 
# functions are in the latest Python 3, but if we used them, it 
# would make it more difficult to back port to Python 2. So, we're
# stuck with having unique functions for each case.
#================ IMPORTANT NOTE =================================

def lonToStr_dms(pos,d,m,s,formatFlag):
    """
    Converts longitude to a printable string as determined by
    formatFlag.

    :param bool pos: true if the longitude is positive
    :param int d: longitude degrees
    :param int m: longitude minutes
    :param float s: longitude seconds
    :param bool formatFlag: what format is desired (DMSFORMAT or DECFORMAT)
    :return: a printable string for the longitude
    """
    if (formatFlag == DMSFORMAT):
        return __latOrLonToDMSStr(__DO_LONGITUDE, pos, d, m, s)
    else:
        # Note: have to do not pos because DMStoDEC uses true to indicate negative value
        return __latOrLonToDecStr(__DO_LONGITUDE, ASTAngle.DMStoDec(not pos, d, m, s)) 



def lonToStr_obj(latlonObj,formatFlag):
    """
    Converts longitude to a printable string as determined by
    formatFlag.

    :param ASTLatLon latlonObj: object to be converted to a string
    :param bool formatFlag: what format is desired (DMSFORMAT or DECFORMAT)
    :return: a printable string for the longitude
    """
    return lonToStr_dms(latlonObj.bLonDir,latlonObj.lonAngle.getDegrees(),
                        latlonObj.lonAngle.getMinutes(),latlonObj.lonAngle.getSeconds(),
                        formatFlag)



#===================================================================
# Private functions only used internally to this module
#===================================================================

def __validateLatOrLon(inputStr,latlonFlag,flag):
    """
    Checks a string to see if it contains a valid latitude or longitude value. The
    string format can be either DMS (###d ##m ##.##s[N/S or W/E]) or decimal
    (###.######[N/S or W/E]). If the direction is omitted, a positive direction
    (i.e., N or E) is assumed. Specifying a sign in addition to, or in lieu of,
    N/S or W/E is invalid. Do not display any error message if flag is HIDE_ERRORS to
    let the calling routine display its own message. This routine is only used internally
    by isValidLat and isValidLon.

    :param str inputStr: string to validate
    :param bool latlonFlag: either __DO_LATITUDE or __DO_LONGITUDE
    :param bool flag: either HIDE_ERRORS or SHOW_ERRORS
    :return: ASTLatLon object containing the results, but the results will be
             **only** the latitude or longitude. The returned object will
             contain both decimal and DMS formats in the ASTLatLon angle object.
    """
    bDirection = POS_DIRECTION
    returnObj = ASTLatLon()
    
    if (latlonFlag == __DO_LATITUDE):
        strLatLon = "Latitude"
        cPos = 'N'
        cNeg = 'S'
        returnObj.bLatValid = False
    else:
        strLatLon = "Longitude"
        cPos = 'E'
        cNeg = 'W'
        returnObj.bLonValid = False

    strDir = "["+cPos+","+cNeg+"]"
    
    inputStr = ASTStr.removeWhitespace(inputStr)
    iLen = len(inputStr)
    if (iLen <= 0):
        ASTMsg.errMsg("Either nothing was entered or what was entered is invalid.\n" +
                "Please enter a valid "+strLatLon+" in the form ###d ##m ##.##s"+strDir+"\n" +
                "or the form ###.######"+strDir+".","Invalid "+strLatLon+" format",flag)
        return returnObj

    # See if the user specified a sign.
    if (inputStr.find('-') >= 0):
        ASTMsg.errMsg("Do not enter a sign [-,+]. Use either '"+cPos+"' or '"+cNeg+"' for "+strLatLon+".",
                "Invalid "+strLatLon+" format",flag)
        return returnObj
    if (inputStr.find('+') >= 0):
        ASTMsg.errMsg("Do not enter a sign [-,+]. Use either '"+cPos+"' or '"+cNeg+"' for "+strLatLon+".",
                "Invalid "+strLatLon+" format",flag)
        return returnObj
 
    # Figure out whether this is positive or negative direction. Note that we could have ##.##sS to contend with
    # since the user could express DMS format with S to indicate direction. Thus, we can't
    # just convert to upper case and check for 'S'.
    if (inputStr[iLen-1] == cNeg):
        bDirection = NEG_DIRECTION
        inputStr = inputStr[0:iLen-1]        # Get rid of the negative direction
        iLen = iLen - 1
    elif (inputStr[iLen-1] == cPos):
        bDirection = POS_DIRECTION
        inputStr = inputStr[0:iLen-1]        # Get rid of the positive direction
        iLen = iLen - 1

    # Let the Angle class do the work to validate that inputStr has a valid angle
    angleObj = ASTAngle.isValidAngle(inputStr,HIDE_ERRORS)
    if (not angleObj.isValidAngleObj()):
        ASTMsg.errMsg("The " + strLatLon + " entered is invalid. Please enter a\n" +
                "valid " + strLatLon+" in the form ###d ##m ##.##s"+strDir+"\n" +
                "or the form ###.######"+strDir+".","Invalid "+strLatLon+" format",flag)
        return returnObj

    if (latlonFlag == __DO_LATITUDE):
        if (abs(angleObj.getDecAngle()) > 90.0):
            ASTMsg.errMsg("The latitude value is out of range.", "Invalid Latitude",flag)
            return returnObj    
        returnObj.bLatDir= bDirection
        returnObj.bLatValid = True
         
        # Because the 'N/S' was stripped off, the angle is always positive. So, we must
        # check to see if it has to be converted to a negative angle.
        if (not returnObj.bLatDir):
            angleObj = ASTAngle.DecToDMS(-angleObj.getDecAngle())
         
        returnObj.latAngle = angleObj
    else:
        if (abs(angleObj.getDecAngle()) > 180.0):
            ASTMsg.errMsg("The longitude value is out of range.", "Invalid Longitude",flag)
            return returnObj    
        returnObj.bLonDir = bDirection
        returnObj.bLonValid = True
         
        # Because the 'W/E' was stripped off, the angle is always positive. So, we must
        # check to see if it has to be converted to a negative angle.
        if (not returnObj.bLonDir):
            angleObj = ASTAngle.DecToDMS(-angleObj.getDecAngle())
        returnObj.lonAngle = angleObj

    return returnObj



def __latOrLonToDecStr(bLat,LatOrLon):
    """
    Converts lat or lon decimal value to a displayable string.
    
    :param bool bLat: __DO_LATITUDE or __DO_LONGITUDE
    :param float LatOrLon: decimal latitude or longitude
    :return: latitude or longitude as a printable string
    """
    result = ASTStr.strFormat(LATLONFORMAT,abs(LatOrLon))
    if (bLat == __DO_LATITUDE):
        if (LatOrLon >= 0.0):
            result = result + "N"
        else:
            result = result + "S"
    else:
        if (LatOrLon >= 0.0):
            result = result + "E"
        else:
            result = result + "W"  
    return result



def __latOrLonToDMSStr(bLat,pos,d,m,s):
    """
    Converts lat or lon DMS values to a displayable DMS string.
    
    :param bool bLat:  __DO_LATITUDE or __DO_LONGITUDE
    :param bool pos: true if a positive latitude/longitude
    :param int d: degrees
    :param int m: minutes
    :param float s: seconds
    :return: latitude or longitude as a printable string
    """
    result = ASTStr.strFormat("%d",abs(d)) + "d " + ASTStr.strFormat("%2dm ",m) +\
             ASTStr.strFormat("%5.2fs",s)
    if (bLat == __DO_LATITUDE):
        if (pos == POS_DIRECTION):
            result = result + "N"
        else:
            result = result + "S"
    else:
        if (pos == POS_DIRECTION):
            result = result + "E"
        else:
            result=result + "W"
    return result



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
