"""
This implements a simple class for handling vectors with
3 elements. We could use intrinsic Python lists, but this
module simplifies porting to other languages.

Copyright (c) 2018
 
:author: J. L. Lawrence
:version 3.0, 2018
"""

import math

class ASTVect():
    """Define a vector as x ,y, z elements plus its length"""
    #=======================================================
    # Class instance variables
    #======================================================= 
    # Rx
    # Ry
    # Rz
    # Rmag 
    
    def __init__(self,x=0.0,y=0.0,z=0.0):
        """
        Constructor for creating vector objects.
        
        :param x: x value for the vector
        :param y: y value for the vector
        :param z: z value for the vector        
        """
        self.Rx = x
        self.Ry = y
        self.Rz = z
        self.Rmag = vectLen(x,y,z)
    
    #====================================================================
    # Define 'get' and 'set' accessors for the object's fields
    #====================================================================
    def x(self):
        return self.Rx
    def y(self):
        return self.Ry
    def z(self):
        return self.Rz
    def len(self):
        return self.Rmag
    def setVect(self,x,y,z):
        self.Rx = x
        self.Ry = y
        self.Rz = z
        self.Rmag = vectLen(x,y,z)



#==============================================================
# Define public functions for manipulating a vector
#==============================================================

def vectLen(x,y,z):
    """
    Calculate the length/magnitude of a vector
    
    :param x: vector's x value
    :param y: vector's y value
    :param z: vector's z value
    :return: the vector's length
    """
    return math.sqrt(x*x + y*y + z*z)



#=========== Main entry point ===============
if __name__ == '__main__':
    pass