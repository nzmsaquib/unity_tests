"""
Defines the fonts and other miscellaneous items that are used for the 
programs throughout the book. Style settings are collected here to ensure
uniformity across all the programs.

Copyright (c) 2018

:author: J. L. Lawrence
:version 3.0, 2018
"""

# Note: We can't use the tkinter.font and its methods to define fonts here
# because those methods require specifying a "root" window, but a root window
# may not have been created yet. So, we use the "old style" for defining fonts
# that does not require a root window to already be defined.



#=============================================================
# Style for the Book Panel
#=============================================================
BOOK_TITLE_BOLDFONT = "-family {Tahoma} -size 12 -weight bold"
BOOK_TITLE_COLOR = "white"
BOOK_TITLE_BKG = "#%02x%02x%02x" % (25,25,112)       # navy blue



#=============================================================
# Items for the GUI areas with which a user will interact
#=============================================================
MENU_FONT = "-family {Segoe UI Semibold} -size 9 -weight normal"  
BTN_FONT = "-family {Tahoma} -size 10 -weight normal"
TEXT_FONT = "-family {Tahoma} -size 10 -weight normal"
TEXT_BOLDFONT = "-family {Tahoma} -size 10 -weight bold"
TEXT_SMFONT = "-family {Tahoma} -size 9 -weight normal"
TEXT_SMBOLDFONT = "-family {Tahoma} -size 9 -weight bold"
TEXT_ITALICFONT = "-family {Tahoma} -size 10 -weight normal -slant italic"
TEXT_ITALICBOLDFONT = "-family {Tahoma} -size 10 -weight bold -slant italic" 
CBOX_FONT = "-family {Tahoma} -size 10 -weight normal"
CBOX_FONT_SMALL = "-family {Tahoma} -size 9 -weight normal"
RADBTN_FONT = "-family {Tahoma} -size 10 -weight normal"
RADBTN_FONT_SMALL = "-family {Tahoma} -size 9 -weight normal"
FW_OUT_TEXT_FONT = "-family {Courier} -size 10 -weight normal" # font when a fixed width font is needed



#==================================================
# Fonts used in the About Box
#==================================================
ABOUTAUTHOR_FONT = "-family {Tahoma} -size 10 -weight normal"
ABOUTBOOKTITLE_BOLDFONT = "-family {Tahoma} -size 10 -weight bold"
ABOUTBTN_FONT = "-family {Tahoma} -size 9 -weight normal"
ABOUTCAPTION_FONT = "-family {Tahoma} -size 8 -weight normal"
ABOUTCHAPTER_BOLDFONT = "-family {Tahoma} -size 10 -weight bold"
ABOUTCOPYRIGHT_FONT = "-family {Tahoma} -size 10 -weight normal"
ABOUTEDITION_BOLDFONT = "-family {Tahoma} -size 10 -weight bold"
ABOUTVERSION_FONT = "-family {Tahoma} -size 10 -weight normal"



#=================================================================
# Define format strings for various items to provide consistency
# across programs
#=================================================================     
mVFORMAT = "%.2f"                   # format for visual magnitude
DEC_DEG_FMT = "%.6f"                # decimal degrees
DEC_HRS_FMT = "%.6f"                # decimal hours
TEMPERATURE_FMT = "%.2f"            # temperature (C and F)
JDFormat = "%9.5f"                  # Julian day number format
LATLONFORMAT = "%.6f"               # latitude/longitude format    
EPOCHFORMAT = "%.1f"                # Format for dislaying the Epoch
GENFLOATFORMAT = "%.6f"             # general format for real numbers
GENFLOATFORMAT8 = "%.8f"            # extra digits for general format for real numbers



#=========== Main entry point ===============
if __name__ == '__main__':
    pass
