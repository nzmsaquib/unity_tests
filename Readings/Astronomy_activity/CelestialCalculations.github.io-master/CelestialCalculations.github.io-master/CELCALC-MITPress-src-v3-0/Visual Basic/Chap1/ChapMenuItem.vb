﻿'**********************************************************
'                     Copyright (c) 2018
'                   Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define possible menu items. These help in deciding what routine to
    ' execute when a menu item is selected
    Friend Enum CalculationType
        'Constellations menu
        LIST_CONST_BY_NAME
        LIST_CONST_BY_ABBREV_NAME
        LIST_CONST_BY_MEANING
        LIST_ALL_CONST
        FIND_CONST
        'Star Catalogs menu
        CLEAR_CATALOG
        LOAD_CATALOG
        SHOW_CATALOG
        ' Space Objects menu
        LIST_OBJ_BY_NAME
        LIST_OBJ_BY_ALT_NAME
        LIST_OBJ_BY_COMMENTS
        LIST_ALL_OBJS_IN_CAT
        LIST_ALL_OBJS_IN_RANGE
        LIST_ALL_OBJS_IN_CONST
        SORT_CAT_BY_CONST
        SORT_CAT_BY_CONST_AND_NAME
        SORT_CAT_BY_NAME
        SORT_CAT_BY_ALT_NAME
        SORT_CAT_BY_RA
        SORT_CAT_BY_DECL
        SORT_CAT_BY_MV
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub

End Class
