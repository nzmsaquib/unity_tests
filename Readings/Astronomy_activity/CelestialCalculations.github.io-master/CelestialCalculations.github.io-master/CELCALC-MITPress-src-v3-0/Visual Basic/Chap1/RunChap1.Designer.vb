﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chap1GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Chap1GUI))
        Me.ChapMenu = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuConstellations = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCatalogs = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSpaceObjects = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemInstructions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelBookTitle = New System.Windows.Forms.Panel()
        Me.lblBookTitle = New System.Windows.Forms.Label()
        Me.panelChkBox = New System.Windows.Forms.Panel()
        Me.chkboxSortOrder = New System.Windows.Forms.CheckBox()
        Me.panelCatalogInfo = New System.Windows.Forms.Panel()
        Me.lblEpoch = New System.Windows.Forms.Label()
        Me.lblFilename = New System.Windows.Forms.Label()
        Me.lblCatalogType = New System.Windows.Forms.Label()
        Me.panelTextOut = New System.Windows.Forms.Panel()
        Me.txtboxOutputArea = New System.Windows.Forms.RichTextBox()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.ChapMenu.SuspendLayout()
        Me.panelBookTitle.SuspendLayout()
        Me.panelChkBox.SuspendLayout()
        Me.panelCatalogInfo.SuspendLayout()
        Me.panelTextOut.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChapMenu
        '
        Me.ChapMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ChapMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuConstellations, Me.mnuCatalogs, Me.mnuSpaceObjects, Me.mnuHelp})
        Me.ChapMenu.Location = New System.Drawing.Point(0, 0)
        Me.ChapMenu.Name = "ChapMenu"
        Me.ChapMenu.Size = New System.Drawing.Size(949, 28)
        Me.ChapMenu.TabIndex = 0
        Me.ChapMenu.Text = "ChapMenu"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(44, 24)
        Me.mnuFile.Text = "File"
        '
        'mnuitemExit
        '
        Me.mnuitemExit.Name = "mnuitemExit"
        Me.mnuitemExit.Size = New System.Drawing.Size(108, 26)
        Me.mnuitemExit.Text = "Exit"
        '
        'mnuConstellations
        '
        Me.mnuConstellations.Name = "mnuConstellations"
        Me.mnuConstellations.Size = New System.Drawing.Size(114, 24)
        Me.mnuConstellations.Text = "Constellations"
        '
        'mnuCatalogs
        '
        Me.mnuCatalogs.Name = "mnuCatalogs"
        Me.mnuCatalogs.Size = New System.Drawing.Size(109, 24)
        Me.mnuCatalogs.Text = "Star Catalogs"
        '
        'mnuSpaceObjects
        '
        Me.mnuSpaceObjects.Name = "mnuSpaceObjects"
        Me.mnuSpaceObjects.Size = New System.Drawing.Size(115, 24)
        Me.mnuSpaceObjects.Text = "Space Objects"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemInstructions, Me.mnuitemAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(53, 24)
        Me.mnuHelp.Text = "Help"
        '
        'mnuitemInstructions
        '
        Me.mnuitemInstructions.Name = "mnuitemInstructions"
        Me.mnuitemInstructions.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemInstructions.Text = "Instructions"
        '
        'mnuitemAbout
        '
        Me.mnuitemAbout.Name = "mnuitemAbout"
        Me.mnuitemAbout.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemAbout.Text = "About"
        '
        'panelBookTitle
        '
        Me.panelBookTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBookTitle.BackColor = System.Drawing.Color.Navy
        Me.panelBookTitle.Controls.Add(Me.lblBookTitle)
        Me.panelBookTitle.Location = New System.Drawing.Point(0, 29)
        Me.panelBookTitle.Name = "panelBookTitle"
        Me.panelBookTitle.Size = New System.Drawing.Size(949, 49)
        Me.panelBookTitle.TabIndex = 1
        '
        'lblBookTitle
        '
        Me.lblBookTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblBookTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBookTitle.ForeColor = System.Drawing.Color.White
        Me.lblBookTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBookTitle.Location = New System.Drawing.Point(0, 14)
        Me.lblBookTitle.Name = "lblBookTitle"
        Me.lblBookTitle.Padding = New System.Windows.Forms.Padding(1)
        Me.lblBookTitle.Size = New System.Drawing.Size(949, 27)
        Me.lblBookTitle.TabIndex = 0
        Me.lblBookTitle.Text = "Book Title"
        Me.lblBookTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'panelChkBox
        '
        Me.panelChkBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelChkBox.Controls.Add(Me.chkboxSortOrder)
        Me.panelChkBox.Location = New System.Drawing.Point(2, 78)
        Me.panelChkBox.Name = "panelChkBox"
        Me.panelChkBox.Size = New System.Drawing.Size(946, 38)
        Me.panelChkBox.TabIndex = 2
        '
        'chkboxSortOrder
        '
        Me.chkboxSortOrder.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.chkboxSortOrder.AutoSize = True
        Me.chkboxSortOrder.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxSortOrder.Location = New System.Drawing.Point(368, 9)
        Me.chkboxSortOrder.Name = "chkboxSortOrder"
        Me.chkboxSortOrder.Size = New System.Drawing.Size(211, 24)
        Me.chkboxSortOrder.TabIndex = 0
        Me.chkboxSortOrder.TabStop = False
        Me.chkboxSortOrder.Text = "Sort in Ascending Order"
        Me.chkboxSortOrder.UseVisualStyleBackColor = True
        '
        'panelCatalogInfo
        '
        Me.panelCatalogInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelCatalogInfo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.panelCatalogInfo.Controls.Add(Me.lblEpoch)
        Me.panelCatalogInfo.Controls.Add(Me.lblFilename)
        Me.panelCatalogInfo.Controls.Add(Me.lblCatalogType)
        Me.panelCatalogInfo.Location = New System.Drawing.Point(0, 117)
        Me.panelCatalogInfo.Name = "panelCatalogInfo"
        Me.panelCatalogInfo.Size = New System.Drawing.Size(949, 63)
        Me.panelCatalogInfo.TabIndex = 1
        '
        'lblEpoch
        '
        Me.lblEpoch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEpoch.AutoSize = True
        Me.lblEpoch.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEpoch.Location = New System.Drawing.Point(783, 5)
        Me.lblEpoch.Name = "lblEpoch"
        Me.lblEpoch.Size = New System.Drawing.Size(134, 20)
        Me.lblEpoch.TabIndex = 2
        Me.lblEpoch.Text = "Epoch: 2000.0 "
        '
        'lblFilename
        '
        Me.lblFilename.AutoSize = True
        Me.lblFilename.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilename.Location = New System.Drawing.Point(4, 32)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Size = New System.Drawing.Size(91, 20)
        Me.lblFilename.TabIndex = 1
        Me.lblFilename.Text = "Filename:"
        '
        'lblCatalogType
        '
        Me.lblCatalogType.AutoSize = True
        Me.lblCatalogType.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCatalogType.Location = New System.Drawing.Point(4, 5)
        Me.lblCatalogType.Name = "lblCatalogType"
        Me.lblCatalogType.Size = New System.Drawing.Size(125, 20)
        Me.lblCatalogType.TabIndex = 0
        Me.lblCatalogType.Text = "Catalog Type:"
        '
        'panelTextOut
        '
        Me.panelTextOut.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelTextOut.Controls.Add(Me.txtboxOutputArea)
        Me.panelTextOut.Location = New System.Drawing.Point(1, 191)
        Me.panelTextOut.Name = "panelTextOut"
        Me.panelTextOut.Padding = New System.Windows.Forms.Padding(5)
        Me.panelTextOut.Size = New System.Drawing.Size(946, 618)
        Me.panelTextOut.TabIndex = 3
        '
        'txtboxOutputArea
        '
        Me.txtboxOutputArea.BackColor = System.Drawing.Color.White
        Me.txtboxOutputArea.Cursor = System.Windows.Forms.Cursors.No
        Me.txtboxOutputArea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtboxOutputArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxOutputArea.Location = New System.Drawing.Point(5, 5)
        Me.txtboxOutputArea.Name = "txtboxOutputArea"
        Me.txtboxOutputArea.ReadOnly = True
        Me.txtboxOutputArea.Size = New System.Drawing.Size(936, 608)
        Me.txtboxOutputArea.TabIndex = 0
        Me.txtboxOutputArea.TabStop = False
        Me.txtboxOutputArea.Text = ""
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.FileName = "OpenFileDialog"
        '
        'Chap1GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(949, 812)
        Me.Controls.Add(Me.panelTextOut)
        Me.Controls.Add(Me.panelCatalogInfo)
        Me.Controls.Add(Me.panelChkBox)
        Me.Controls.Add(Me.panelBookTitle)
        Me.Controls.Add(Me.ChapMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.ChapMenu
        Me.Name = "Chap1GUI"
        Me.Text = "Title"
        Me.ChapMenu.ResumeLayout(False)
        Me.ChapMenu.PerformLayout()
        Me.panelBookTitle.ResumeLayout(False)
        Me.panelChkBox.ResumeLayout(False)
        Me.panelChkBox.PerformLayout()
        Me.panelCatalogInfo.ResumeLayout(False)
        Me.panelCatalogInfo.PerformLayout()
        Me.panelTextOut.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChapMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuConstellations As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCatalogs As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSpaceObjects As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemInstructions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents panelBookTitle As System.Windows.Forms.Panel
    Friend WithEvents lblBookTitle As System.Windows.Forms.Label
    Friend WithEvents panelChkBox As System.Windows.Forms.Panel
    Friend WithEvents chkboxSortOrder As System.Windows.Forms.CheckBox
    Friend WithEvents panelCatalogInfo As System.Windows.Forms.Panel
    Friend WithEvents lblEpoch As System.Windows.Forms.Label
    Friend WithEvents lblFilename As System.Windows.Forms.Label
    Friend WithEvents lblCatalogType As System.Windows.Forms.Label
    Friend WithEvents panelTextOut As System.Windows.Forms.Panel
    Friend WithEvents txtboxOutputArea As System.Windows.Forms.RichTextBox
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog

End Class
