﻿'***********************************************************
'                 Chapter 1 - Introduction
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' for a program that will load and manipulate a star
' catalog. The main GUI was mostly built with the 
' Visual Basic form editor.
'**********************************************************

Option Explicit On
Option Strict On

Imports ASTUtils
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTCatalog
Imports ASTUtils.ASTConstellation
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap1.ChapMenuItem

Public Class Chap1GUI
    Private Const AboutBoxText = "Chapter 1 - Introduction"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' Create a reusable query form for user input
    Private queryForm As ASTQuery = New ASTQuery()

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt

    Private constellations As ASTConstellation
    Private catalog As ASTCatalog

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()

        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        prt.clearTextArea()

        ' Create all of the Constellations, Star Catalogs, and Space Objects menu items and
        ' perform all related initializations
        createChapMenus()

        setEpoch(DEFAULT_EPOCH)
        setCatalogType("")
        setFilename("")
        chkboxSortOrder.Checked = True

        constellations = New ASTConstellation(prt)
        catalog = New ASTCatalog(prt)

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------------------------------------
    ' Create the Constellations, Star Catalogs, and Space Objects menus.
    '---------------------------------------------------------------------------------------------

    '------------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler.
    '
    ' title         title for the menu item
    ' CalcType      what type of calculation to perform
    '               when this menu item is clicked
    '------------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '-------------------------------------------------
    ' Create the Constellations, Star Catalogs, and
    ' Space Objects menu subitems
    '-------------------------------------------------
    Private Sub createChapMenus()
        Dim tmpSubMenu As ToolStripMenuItem

        ' Create Constellations menu and submenu items
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "List a Constellation by ..."
        mnuConstellations.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("Name", CalculationType.LIST_CONST_BY_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Abbreviated Name", CalculationType.LIST_CONST_BY_ABBREV_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Meaning", CalculationType.LIST_CONST_BY_MEANING))
        mnuConstellations.DropDownItems.Add("-")
        mnuConstellations.DropDownItems.Add(createMenuItem("List All Constellations", CalculationType.LIST_ALL_CONST))
        mnuConstellations.DropDownItems.Add("-")
        mnuConstellations.DropDownItems.Add(createMenuItem("Find Constellation an Object is In",
                                                           CalculationType.FIND_CONST))

        ' Create Star Catalogs menu
        mnuCatalogs.DropDownItems.Add(createMenuItem("Clear Catalog", CalculationType.CLEAR_CATALOG))
        mnuCatalogs.DropDownItems.Add("-")
        mnuCatalogs.DropDownItems.Add(createMenuItem("Load a Star Catalog", CalculationType.LOAD_CATALOG))
        mnuCatalogs.DropDownItems.Add("-")
        mnuCatalogs.DropDownItems.Add(createMenuItem("Show Catalog Information", CalculationType.SHOW_CATALOG))

        ' Create Space Objects menu
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "List a Catalog Object by ..."
        mnuSpaceObjects.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("Name", CalculationType.LIST_OBJ_BY_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Alternate Name", CalculationType.LIST_OBJ_BY_ALT_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Catalog Comments", CalculationType.LIST_OBJ_BY_COMMENTS))
        mnuSpaceObjects.DropDownItems.Add("-")

        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "List All Space Objects ..."
        mnuSpaceObjects.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("In the Entire Catalog", CalculationType.LIST_ALL_OBJS_IN_CAT))
        tmpSubMenu.DropDownItems.Add(createMenuItem("In a Range", CalculationType.LIST_ALL_OBJS_IN_RANGE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("In a Constellation", CalculationType.LIST_ALL_OBJS_IN_CONST))
        mnuSpaceObjects.DropDownItems.Add("-")

        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Sort Catalog by ..."
        mnuSpaceObjects.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("Constellation", CalculationType.SORT_CAT_BY_CONST))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Constellation and Obj Name", CalculationType.SORT_CAT_BY_CONST_AND_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Object's Name", CalculationType.SORT_CAT_BY_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Object's Alternate Name", CalculationType.SORT_CAT_BY_ALT_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Object's Right Ascension", CalculationType.SORT_CAT_BY_RA))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Object's Declination", CalculationType.SORT_CAT_BY_DECL))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Object's Visual Magnitude", CalculationType.SORT_CAT_BY_MV))
    End Sub

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '----------------------------------------
    ' Handle clicks on the Constellations,
    ' Catalogs, and Space Objects menu items
    '----------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        Select Case calctype
            '************* Constellations menu
            Case CalculationType.LIST_CONST_BY_NAME
                listConstByName()
            Case CalculationType.LIST_CONST_BY_ABBREV_NAME
                listConstByAbbrevName()
            Case CalculationType.LIST_CONST_BY_MEANING
                listConstsByMeaning()
            Case CalculationType.LIST_ALL_CONST
                listAllConstellations()
            Case CalculationType.FIND_CONST
                findConstellationForRA_Decl()

                '************* Catalogs menu
            Case CalculationType.CLEAR_CATALOG
                clearCatalog()
            Case CalculationType.LOAD_CATALOG
                loadCatalog()
            Case CalculationType.SHOW_CATALOG
                showCatalogInfo()

                '************* Space Objects menu
            Case CalculationType.LIST_OBJ_BY_NAME
                listObjByName()
            Case CalculationType.LIST_OBJ_BY_ALT_NAME
                listObjByAltName()
            Case CalculationType.LIST_OBJ_BY_COMMENTS
                listObjByComments()
            Case CalculationType.LIST_ALL_OBJS_IN_CAT
                listAllObjsInCatalog()
            Case CalculationType.LIST_ALL_OBJS_IN_RANGE
                listAllObjsByRange()
            Case CalculationType.LIST_ALL_OBJS_IN_CONST
                listAllObjsByConst()
            Case CalculationType.SORT_CAT_BY_CONST
                sortCatalog(CatalogSortField.CONSTELLATION)
            Case CalculationType.SORT_CAT_BY_CONST_AND_NAME
                sortCatalog(CatalogSortField.CONST_AND_OBJNAME)
            Case CalculationType.SORT_CAT_BY_NAME
                sortCatalog(CatalogSortField.OBJNAME)
            Case CalculationType.SORT_CAT_BY_ALT_NAME
                sortCatalog(CatalogSortField.OBJ_ALTNAME)
            Case CalculationType.SORT_CAT_BY_RA
                sortCatalog(CatalogSortField.RA)
            Case CalculationType.SORT_CAT_BY_DECL
                sortCatalog(CatalogSortField.DECL)
            Case CalculationType.SORT_CAT_BY_MV
                sortCatalog(CatalogSortField.VISUAL_MAGNITUDE)
        End Select
    End Sub

    '---------------------------------------------------------------------
    ' Handle Constellations menu items
    '---------------------------------------------------------------------

    '------------------------------------------------------------
    ' Finds the constellation that a given RA/Decl falls within
    '------------------------------------------------------------
    Private Sub findConstellationForRA_Decl()
        Dim raObj As ASTTime = New ASTTime
        Dim declObj As ASTAngle = New ASTAngle
        Dim idx As Integer

        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)" & vbNewLine & "for Epoch 2000.0",
                                   "Enter Declination (xxxd yym zz.zzs)" & vbNewLine & "for Epoch 2000.0") =
                                    Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Exit Sub
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Exit Sub
            End If

            prt.clearTextArea()
            idx = findConstellationFromCoord(raObj.getDecTime(), declObj.getDecAngle(), DEFAULT_EPOCH)

            If idx < 0 Then
                CriticalErrMsg("Could not determine a constellation for the data entered.")
            Else
                prt.println("The location " & timeToStr(raObj, HMSFORMAT) & " RA, " &
                            angleToStr(declObj, DMSFORMAT) & " Decl " &
                            "is in the " & getConstName(idx) & " (" & getConstAbbrevName(idx) & ") constellation")
            End If
            prt.resetCursor()
        End If
    End Sub

    '----------------------------------------------
    ' Displays a list of all of the constellations.
    '----------------------------------------------
    Private Sub listAllConstellations()
        prt.clearTextArea()
        prt.setFixedWidthFont()
        displayAllConstellations()
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------
    ' Displays detailed information about a constellation when 
    ' given its abbreviated name.
    '----------------------------------------------------------
    Private Sub listConstByAbbrevName()
        Dim sAbbrevName As String
        Dim idx As Integer

        If queryForm.showQueryForm("Enter Constellation's 3 Character" & vbNewLine &
                                   "Abbreviated Name") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            sAbbrevName = queryForm.getData1()
            idx = findConstellationByAbbrvName(sAbbrevName.Trim())
            If idx < 0 Then
                prt.println("No Constellation whose abbreviated name is '" & sAbbrevName & "' was found")
            Else
                displayConstellation(idx)
            End If
            prt.resetCursor()
        End If
    End Sub

    '----------------------------------------------------------
    ' Displays detailed information about the constellations
    ' that contain a target substring in their "meaning" field.
    '----------------------------------------------------------
    Private Sub listConstsByMeaning()
        Dim targ As String = Nothing
        Dim n, i As Integer
        Dim iResult() As Integer = Nothing

        If queryForm.showQueryForm("Enter Substring to Search for in the" & vbNewLine &
                                   "Constellation's 'Meaning' field") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            targ = queryForm.getData1()
            n = findConstellationsByMeaning(targ.Trim(), iResult)
            If n < 0 Then
                prt.println("No Constellation(s) found with a 'Meaning' field containing the substring '" & targ & "'")
            Else
                ' There is at least one constellation to display
                If (n = 0) Then
                    prt.println("One Constellations has the substring '" & targ & "' in its 'Meaning' field")
                Else
                    prt.println(n + 1 & " Constellations have the substring '" & targ & "' in their 'Meaning' field")
                End If
                For i = 0 To n
                    prt.println()
                    prt.setFixedWidthFont()
                    prt.println(String.Format("{0,80:s}", "*").Replace(" ", "*"))
                    prt.setProportionalFont()
                    prt.println("Constellation # " & (i + 1))
                    displayConstellation(iResult(i))
                Next
            End If
            prt.resetCursor()
        End If
    End Sub

    '--------------------------------------------------------------------------
    ' Displays detailed information about a constellation when given its name.
    '--------------------------------------------------------------------------
    Private Sub listConstByName()
        Dim sConstName As String
        Dim idx As Integer

        If queryForm.showQueryForm("Enter Constellation Name to Display") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            sConstName = queryForm.getData1
            idx = findConstellationByName(sConstName.Trim())
            If idx < 0 Then
                prt.println("There is no Constellation with the name '" & sConstName & "'")
            Else
                displayConstellation(idx)
            End If
            prt.resetCursor()
        End If
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Catalog menu items
    '----------------------------------------------------------------------------------------------------

    '-----------------------------------------------------------------
    ' Shows the catalog information from the currently loaded catalog.
    '-----------------------------------------------------------------
    Private Sub showCatalogInfo()
        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
        Else
            prt.clearTextArea()
            displayCatalogInfo()
            prt.resetCursor()
        End If
    End Sub

    '--------------------------------------------
    ' Clears all catalog data currently loaded
    '--------------------------------------------
    Private Sub clearCatalog()

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded, so there is nothing to clear.", "No Catalog Loaded")
            Return
        End If

        prt.clearTextArea()
        If PleaseConfirm("Are you sure you want to clear all" & vbNewLine &
                         "currently loaded catalog data?", "Clear Catalog Data") Then
            clearCatalogAndSpaceObjects()
            setFilename("")
            setCatalogType("")
            setEpoch(DEFAULT_EPOCH)
            prt.println("All currently loaded catalog data was cleared ...")
        Else
            prt.println("Catalog data was not cleared ...")
        End If
        prt.resetCursor()
    End Sub

    '------------------------------------------------
    ' Loads star catalog from disk
    '------------------------------------------------
    Private Sub loadCatalog()
        Dim fullFilename As String = " "

        If Not getCatFileToOpen(fullFilename) Then Return

        setFilename(fullFilename)
        prt.clearTextArea()
        If ASTCatalog.loadFormattedStarCatalog(fullFilename) Then
            prt.println("Read in " & getCatNumConst() & " different Constellations with a total of " &
            getCatNumObjs() & " Objects")
            setCatalogType(getCatType())
            setEpoch(getCatEpoch())
        Else
            ErrMsg("Could not load the catalog data from " & fullFilename, "Catalog Load Failed")
            setFilename("")
            setCatalogType("")
            setEpoch(DEFAULT_EPOCH)
        End If
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Space Objects menu items
    '----------------------------------------------------------------------------------------------------

    '-------------------------------------------------------
    ' Shows all space objects within a given constellation.
    '-------------------------------------------------------
    Private Sub listAllObjsByConst()
        Dim idx As Integer
        Dim constAbbrevName As String

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter Constellation's 3 Character" & vbNewLine &
                           "Abbreviated Name") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            constAbbrevName = queryForm.getData1()
            idx = findConstellationByAbbrvName(constAbbrevName.Trim())
            If idx < 0 Then
                prt.println("No Constellation whose abbreviated name is '" & constAbbrevName & "' was found")
            Else
                prt.setFixedWidthFont()
                displayAllObjsByConstellation(idx, getSortOrder())
                prt.setProportionalFont()
            End If
            prt.resetCursor()
        End If
    End Sub

    '--------------------------------------------------------------
    ' Shows all space objects within a user-specified index range.
    '--------------------------------------------------------------
    Private Sub listAllObjsByRange()
        Dim iMaxNum As Integer = 100        ' default to 1st 100 entries
        Dim iStart, iEnd, iTemp As Integer

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter index for 1st object to list" & vbNewLine &
                                   "(ex: 1 for 1st object in the catalog)",
                                   "Enter number of objects to list" & vbNewLine &
                                   "(ex: 10 for total of 10 objects)") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            If isValidInt(queryForm.getData1(), iStart, HIDE_ERRORS) Then
                iStart = iStart - 1          ' 0 based indexing assumed!
            Else
                iStart = 0
            End If
            If isValidInt(queryForm.getData2(), iTemp, HIDE_ERRORS) Then
                iEnd = iStart + iTemp - 1
            Else
                iEnd = iStart + iMaxNum - 1
            End If
            prt.setFixedWidthFont()
            displayAllObjsByRange(iStart, iEnd)
            prt.setProportionalFont()
            prt.resetCursor()
        End If
    End Sub

    '---------------------------------------------------------
    ' Shows all catalog information, including space objects,
    ' in the currently loaded catalog.
    '---------------------------------------------------------
    Private Sub listAllObjsInCatalog()
        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        prt.clearTextArea()
        displayCatalogInfo()
        prt.setFixedWidthFont()
        prt.println(String.Format("{0,82:s}", "*").Replace(" ", "*"))
        displayAllCatalogObjects()
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '--------------------------------------------------
    ' Displays all catalog information about an object
    ' given its alternate name.
    '--------------------------------------------------
    Private Sub listObjByAltName()
        Dim idx As Integer
        Dim searchStr As String

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter the Object's Alternate Name") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            searchStr = queryForm.getData1()
            idx = findObjByAltName(searchStr.Trim())
            If idx < 0 Then
                prt.println("No object whose alternate name is '" & searchStr & "' was found in the catalog")
            Else
                displayFullObjInfo(idx)
            End If
            prt.resetCursor()
        End If
    End Sub

    '----------------------------------------------------------
    ' Displays detailed information about catalog objects that
    ' contain a target substring in their "comments" field.
    '----------------------------------------------------------
    Private Sub listObjByComments()
        Dim searchStr As String
        Dim n, i As Integer
        Dim iResult() As Integer = Nothing

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter Substring to search for in" & vbNewLine &
                                   "the Object's 'Comments' Field") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            searchStr = queryForm.getData1()
            n = findObjsByComments(searchStr.Trim(), iResult)
            If n < 0 Then
                prt.println("No Objects in the current Catalog have the substring '" & searchStr &
                            "' in their 'Comments' field")
            Else
                If n = 0 Then
                    prt.println("One Object has the substring '" & searchStr & "' in its 'Comments' field")
                Else
                    prt.println(n + 1 & " Objects have the substring '" & searchStr & "' in their 'Comments' field")
                End If
            End If
            For i = 0 To n
                prt.println()
                prt.setFixedWidthFont()
                prt.println(String.Format("{0,80:s}", "*").Replace(" ", "*"))
                prt.setProportionalFont()
                prt.println("Object # " & (i + 1))
                displayFullObjInfo(iResult(i))
            Next
            prt.resetCursor()
        End If
    End Sub

    '--------------------------------------------------
    ' Displays all catalog information about an object
    ' given its name.
    '--------------------------------------------------
    Private Sub listObjByName()
        Dim idx As Integer
        Dim searchStr As String

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter the Object's Name") = Windows.Forms.DialogResult.OK Then
            prt.clearTextArea()
            searchStr = queryForm.getData1()
            idx = findObjByName(searchStr.Trim())
            If idx < 0 Then
                prt.println("No object named '" & searchStr & "' was found in the catalog")
            Else
                displayFullObjInfo(idx)
            End If
            prt.resetCursor()
        End If
    End Sub

    '---------------------------------------------------------------
    ' Sorts the currently loaded catalog in ascending or descending
    ' order depending upon the sort checkbox in the GUI.
    '
    ' sortField             which field to sort on
    '---------------------------------------------------------------
    Private Sub sortCatalog(ByVal sortField As CatalogSortField)
        Dim sortOrder As Boolean

        sortOrder = getSortOrder()

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        prt.clearTextArea()
        prt.print("You have elected to sort the catalog in ")
        If (sortOrder = ASCENDING_ORDER) Then
            prt.print("ascending order")
        Else
            prt.print("descending order")
        End If
        prt.println("  by " & sortFieldToStr(sortField) & " ...")
        prt.println()

        sortStarCatalog(sortField, sortOrder)
        prt.println("The catalog has now been sorted ...")
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle the Instructions menu item
    '----------------------------------------------------------------------------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        prt.clearTextArea()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("This program allows you to examine constellations and objects in star catalogs in various ways." &
                    " If you want the lists of objects to be displayed in ascending order, check the 'Sort in Ascending Order' " &
                    " checkbox. If you want to sort in descending order, be sure the checkbox is unchecked.")
        prt.println()

        prt.println("The 'Constellations' menu allows you to display information about the 88 modern constellations. You can " &
                    "display a list of all the constellations and basic information about them, or use the " &
                    "'List a Constellation by ...' menu item to list individual constellations with more detailed information. " &
                    "The 'Find Constellation an Object is In' menu item allows you to enter the Right Ascension (in hours) and " &
                    "Declination (in degrees) for an object and determine what constellation the object lies within.")
        prt.println()

        prt.print("The 'Star Catalogs' menu item allows you to load a star catalog and see basic information about the " &
                  "catalog, such as where it came from, how many objects are in the catalog, and what constellations " &
                  "those objects are in. The catalogs ")
        prt.setBoldFont(True)
        prt.print("*MUST*")
        prt.setBoldFont(False)
        prt.println(" be in the format specifically designed for the programs in this book. Look at the source code for this " &
                    "program, the README.txt file, or any of the star catalog data files to understand the format of the " &
                    "star catalog data files. The star catalog data files are ordinary text files in a 'pseudo-XML' format. " &
                    "You can download publically available data from some astronomy/space organization to create your own " &
                    "star catalog files as long as the data is plain ASCII data in exactly the format this book requires.")
        prt.println()

        prt.print("The 'Space Objects' menu allows you to look at objects within an already loaded catalog. You can display " &
                  "individual objects by entering their name or alternate name. It is important to note that the name/alternate name " &
                  "entered must match a name in the")
        prt.setBoldFont(True)
        prt.print(" *currently loaded* ")
        prt.setBoldFont(False)
        prt.println("catalog because different catalogs (e.g., Messier versus Henry Draper) use different names " &
                    "for the same object. The 'List all Space Objects ...' menu entry allows you to " &
                    "look at all, or a subset of, the objects in the catalog. For example, if you wish to explore " &
                    "the objects within a particular constellation, choose the 'In a Constellation' sub-menu to see " &
                    "all objects within the currently loaded catalog that are in a given constellation. Be aware that " &
                    "some of the catalogs are quite large and may take considerable time to display all of their objects. " &
                    "The 'Sort Catalog by ...' menu item allows you to sort the catalog objects in various ways before " &
                    "displaying them.")
        prt.println()

        prt.print("Several menu options, such as those under 'List a Catalog Object by ...', allow you to enter a string, " &
                  "such as an object name, to search for in the currently loaded catalog. When searches are performed for " &
                  "a string that you enter, the search is not case sensitive. Thus, a search for 'm39' and 'M39' will yield " &
                  "the same results. Also, spaces are generally not important so that a search for 'M 39' will yield the " &
                  "same result as a search for 'M39'.")
        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '---------------------------------------------------------------------------------------------------------
    ' Routines below are miscellaneous helper routines used only in this class
    '---------------------------------------------------------------------------------------------------------

    '-----------------------------------------------
    ' Gets the sort order from the GUI check box
    '
    ' Returns ASCENDING_ORDER or DESCENDING_ORDER
    '-----------------------------------------------
    Private Function getSortOrder() As Boolean
        If chkboxSortOrder.Checked Then
            Return ASCENDING_ORDER
        Else
            Return DESCENDING_ORDER
        End If
    End Function

    '-------------------------------------------------------
    ' Sets the Catalog type in the GUI.
    '
    ' cat_type      String representing the catalog type
    '               which is read from the star catalog
    '               data file
    '-------------------------------------------------------
    Private Sub setCatalogType(ByVal cat_type As String)
        lblCatalogType.Text = "Catalog Type: " & cat_type
    End Sub

    '-----------------------------------
    ' Sets the Epoch label in the GUI.
    '
    ' epoch         Epoch to display
    '-----------------------------------
    Private Sub setEpoch(ByVal epoch As Double)
        lblEpoch.Text = "Epoch: " & Format(epoch, epochFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the filename label to be displayed in the GUI.
    '
    ' filename          star catalog filename, including pathname
    '---------------------------------------------------------------
    Private Sub setFilename(ByVal fname As String)
        lblFilename.Text = "Filename: " & fname
    End Sub

End Class
