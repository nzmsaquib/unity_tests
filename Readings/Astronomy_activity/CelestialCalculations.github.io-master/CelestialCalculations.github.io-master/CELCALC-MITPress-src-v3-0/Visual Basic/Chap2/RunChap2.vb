﻿'**********************************************************
'                Chapter 2 - Unit Conversions
'                     Copyright (c) 2018
'                  Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' to do unit conversions. The main GUI was mostly built
' with the Visual Basic form editor, so many "globals" are
' created external to the code here. An exception to using
' the Visual Basic form editor to create a GUI component are the
' Conversions menu items, which are created dynamically
' as ConvMenuItem objects. This is done to make it easier
' to figure out what to do when a user clicks on an
' entry because there is a lot of commonality to be
' taken advantage of (such as cross-multiplying).
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap2.ConvMenuItem

Public Class Chap2GUI
    Private Const AboutBoxText = "Chapter 2 - Unit Conversions"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt

    ' The current units being converted from/to are held as strings in lblFromUnits and lblToUnits.
    ' dFromDenom and dToDenom are used to know the denominators for doing a cross-multiply
    ' and other conversions. See the ConvMenuItem class below for more details.
    Private dFromDenom, dToDenom As Double
    Private strFromFormat, strToFormat As String
    Private eConvType As ConversionType                ' what type of conversion needs to be done

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        prt.clearTextArea()

        ' Create all of the Conversions menu items and perform all related initializations
        createConversionsMenu()

        ' Now initialize the rest of the GUI
        lblResult.Text = " "
        chkboxShowInterimCalcs.Checked = False

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------
    ' Create the Conversions menu items and perform other related 
    ' intializations
    '---------------------------------------------------------------
    Private Sub createConversionsMenu()
        Dim tmp As ConvMenuItem

        ' The first menu item is special in that we want to initialize the GUI
        ' as if that menu item were already selected.
        tmp = createMenuItem("mm", 25.4, "0.000000", "inches", 1.0, "0.000000", ConversionType.CROSS_MULTIPLY)
        lblFromUnits.Text = tmp.strFromUnits
        lblToUnits.Text = tmp.strToUnits
        dFromDenom = tmp.dFromDenom
        strFromFormat = tmp.strFromFormat
        dToDenom = tmp.dToDenom
        strToFormat = tmp.strToFormat
        eConvType = tmp.eConvType

        createMenuItem("m", 0.3048, "0.000000", "feet", 1.0, "0.000000", ConversionType.CROSS_MULTIPLY)
        mnuConversions.DropDownItems.Add("-")
        createMenuItem("km", 1.609344, "0.000000", "miles", 1.0, "0.000000", ConversionType.CROSS_MULTIPLY)
        createMenuItem("light years", 1.0, "0.0000E+00", "miles", 5870000000000.0, "0.000000E+00", ConversionType.CROSS_MULTIPLY)
        createMenuItem("light years", 1.0, "0.0000", "parsecs", 0.3068, "0.0000", ConversionType.CROSS_MULTIPLY)
        createMenuItem("AU", 1.0, "0.000000E+00", "miles", 92900000.0, "0.0000E+00", ConversionType.CROSS_MULTIPLY)
        mnuConversions.DropDownItems.Add("-")
        createMenuItem("degs", 180.0, decDegFormat, "radians", PI, decDegFormat, ConversionType.CROSS_MULTIPLY)
        createMenuItem("decimal hrs", 24.0, decHoursFormat, "decimal degs", 360.0, decDegFormat, ConversionType.CROSS_MULTIPLY)
        createMenuItem("HMS", -1.0, "@", "decimal hrs", 1.0, decHoursFormat, ConversionType.HorDMS_HRSorDEGS)
        createMenuItem("DMS", -1.0, "@", "decimal degs", 1.0, decDegFormat, ConversionType.HorDMS_HRSorDEGS)
        mnuConversions.DropDownItems.Add("-")
        createMenuItem("degs C", -1.0, temperatureFormat, "degs F", 1.0, temperatureFormat, ConversionType.CENTIGRADE_FAHRENHEIT)
    End Sub

    '---------------------------------------------------------------
    ' Create a Conversions menu item and set its handler
    '
    ' FromUnits     the units being converted from (e.g., mm)
    ' FromDenom     the value of the 'from' denominator (e.g., 5.0)
    ' FromFormat    how to format the 'from' data value
    ' ToUnits       the units to convert to (e.g., inches)
    ' ToDenom       the value of the 'to' denominator
    ' ToFormat      how to format the 'to' data value
    ' ConvType      the type of conversion to perform
    '---------------------------------------------------------------
    Private Function createMenuItem(ByVal FromUnits As String, ByVal FromDenom As Double,
                                    ByVal FromFormat As String, ByVal ToUnits As String,
                                    ByVal ToDenom As Double, ByVal ToFormat As String,
                                    ByVal ConvType As ConversionType) As ConvMenuItem

        Dim tmp As ConvMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the Conversion menu itself
        tmp = New ConvMenuItem(FromUnits, FromDenom, FromFormat, ToUnits, ToDenom, ToFormat, ConvType)
        tmp.Text = FromUnits & " - " & ToUnits
        AddHandler tmp.Click, AddressOf mnuConversionsItems_Clicks
        mnuConversions.DropDownItems.Add(tmp)

        Return tmp
    End Function

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuItemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '--------------------------------------------
    ' Handle Conversions menu items by setting
    ' the conversion units labels and other
    ' pertinent data items.
    '--------------------------------------------
    Private Sub mnuConversionsItems_Clicks(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim tmp As ConvMenuItem = CType(sender, ConvMenuItem)

        ' save the to/from units, denominators, and formats
        lblFromUnits.Text = tmp.strFromUnits
        dFromDenom = tmp.dFromDenom
        strFromFormat = tmp.strFromFormat

        lblToUnits.Text = tmp.strToUnits
        dToDenom = tmp.dToDenom
        strToFormat = tmp.strToFormat

        eConvType = tmp.eConvType

        lblResult.Text = " "   ' clear old results
    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        prt.clearTextArea()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " &
                    "intermediate results as the conversions are performed. Then select the " &
                    "conversion to perform from the 'Conversions' menu. For example, select the " &
                    "'mm - inches' menu item to convert between millimeters and inches.")
        prt.println()

        prt.println("The units on the left side of the menu (e.g., mm) will be converted to the units " &
                    "on the right side of the menu (e.g., inches). To perform a conversion the other " &
                    "way (e.g., inches to mm), click the 'Swap Units' button.")
        prt.println()

        prt.println("Enter the value to convert in the white input box to the right of the 'Convert' " &
                    "button. For all conversions except HMS/DMS, enter only digits, a decimal point, and " &
                    "a + or - sign, if needed. To convert HMS values, enter the HMS value in the form " &
                    "hh:mm:ss.ssss where hh is the hours, mm is the minutes, and ss.ssss is the seconds. (Note that " &
                    "time is expressed in 24 hour format, so 15:30:20 would be entered for 3:30:20 PM.) " &
                    "For DMS values, enter them in the form xxxd yym zz.zzzzs where xxx is the degrees, " &
                    "yy is the arcminutes, and zz.zzzzs is the arcseconds (e.g., 160d 8m 15.2s).")
        prt.println()

        prt.print("Finally, click the 'Convert' button to perform the conversion. The result of the " &
                  "conversion will be shown directly underneath the white input box. If the " &
                  "'Show Intermediate Calculations' checkbox is checked, intermediate calculation results " &
                  "will be shown in the scrollable text area at the bottom of the application window.")
        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '--------------------------------------------------------------------
    ' Routines below handle clicks on the GUI buttons
    '--------------------------------------------------------------------

    '--------------------------------------
    ' Handle the "Swap Units" button
    '--------------------------------------
    Private Sub btnSwapUnits_Click(sender As Object, e As System.EventArgs) Handles btnSwapUnits.Click
        Dim strTemp As String
        Dim dTemp As Double

        strTemp = lblToUnits.Text
        lblToUnits.Text = lblFromUnits.Text
        lblFromUnits.Text = strTemp

        strTemp = strToFormat
        strToFormat = strFromFormat
        strFromFormat = strTemp

        dTemp = dToDenom
        dToDenom = dFromDenom
        dFromDenom = dTemp

        ' Also make the current results the input before clearing the results
        txtboxUserInput.Text = lblResult.Text
        lblResult.Text = " "   ' clear old results
    End Sub

    '-----------------------------------------------
    ' Handle the "Convert" button by actually doing
    ' the conversion requested.
    '-----------------------------------------------
    Private Sub btnConvert_Click(sender As Object, e As System.EventArgs) Handles btnConvert.Click
        Dim strInput As String = txtboxUserInput.Text

        prt.clearTextArea()

        Select Case eConvType
            Case ConversionType.CROSS_MULTIPLY
                lblResult.Text = crossMultiply()
            Case ConversionType.HorDMS_HRSorDEGS
                lblResult.Text = convertHorDMS_decHrsorDegs()
            Case ConversionType.CENTIGRADE_FAHRENHEIT
                lblResult.Text = convertC_F()
        End Select
    End Sub

    '-------------------------------------------------------------------------------
    ' Routines below handle conversions depending upon which menu item was selected
    ' and returns a string with the computational results of the conversion.
    '-------------------------------------------------------------------------------

    '-------------------------------------------------------------
    ' Cross-multiply to do the requested conversion. The data
    ' to be converted is in the txtboxUserInput field in the GUI.
    '
    ' The cross-multiplication result is returned as a string.
    '-------------------------------------------------------------
    Private Function crossMultiply() As String
        Dim xTo, yFrom As Double
        Dim result = " "
        Dim strInput As String = removeWhitespace(txtboxUserInput.Text)

        If isValidReal(strInput, yFrom) Then
            prt.setBoldFont(True)
            printlnCond("Convert " & strInput & " " & lblFromUnits.Text & " to " & lblToUnits.Text, CENTERTXT)
            prt.setBoldFont(False)
            printlnCond()

            xTo = (yFrom * dToDenom) / dFromDenom
            result = Format(xTo, strToFormat)

            prt.setFixedWidthFont()
            printlnCond("1. Form an equation relating " & lblToUnits.Text & " and " & lblFromUnits.Text & ". Since")
            printlnCond("        " & Format(dToDenom, strToFormat) & " " & lblToUnits.Text & " = " &
                        Format(dFromDenom, strFromFormat) & " " & lblFromUnits.Text & ",")
            printlnCond("   the equation we must form is")
            printlnCond("        x / (" & Format(dToDenom, strToFormat) & " " & lblToUnits.Text & ") = " &
                        "(" & Format(yFrom, strFromFormat) & " " & lblFromUnits.Text & " / " &
                        Format(dFromDenom, strFromFormat) & " " & lblFromUnits.Text & ")")

            printlnCond()
            printlnCond("2. The units on the right side are the same, so cancel them out")
            printlnCond("   and then cross-multiply to get")
            printlnCond("        (x) * (" & Format(dFromDenom, strFromFormat) & ")" &
                        " = (" & Format(yFrom, strFromFormat) & ") * (" & Format(dToDenom, strToFormat) &
                        " " & lblToUnits.Text & "),")
            printlnCond("   which simplifies to")
            printlnCond("        " & Format(dFromDenom, strFromFormat) & "x = " &
                        Format(yFrom * dToDenom, strToFormat) & " " & lblToUnits.Text)

            printlnCond()
            printlnCond("3. Divide both sides of the equation by " & Format(dFromDenom, strFromFormat) &
                        " to get the answer.")
            printlnCond("   Doing so gives")
            printlnCond("        x = " & result & " " & lblToUnits.Text)

            printlnCond()
            printlnCond("Thus, " & Format(yFrom, strFromFormat) & " " & lblFromUnits.Text & " = " &
                        result & " " & lblToUnits.Text)
        Else
            prt.println("Invalid input data. Try again")
        End If

        prt.setProportionalFont()
        prt.resetCursor()
        crossMultiply = result
    End Function

    '-------------------------------------------------------------
    ' Convert between HMS and decimal hours, and DMS and decimal
    ' degrees. The process is the same with only the units being 
    ' different between the two.
    '
    ' The conversion result is returned as a string.
    '-------------------------------------------------------------
    Private Function convertHorDMS_decHrsorDegs() As String
        Dim strInput As String = LTrim(txtboxUserInput.Text)
        Dim resultDMSObj As ASTAngle = New ASTAngle
        Dim resultHMSObj As ASTTime = New ASTTime
        Dim SIGN, dORh, m As Integer
        Dim decValue, s As Double
        Dim result As String = " "
        Dim strDorH, strSIGN As String
        Dim doingHMS, bNeg As Boolean

        ' See if we're converting HMS/DMS to dec hrs/degs or vice versa
        If dFromDenom < 0.0 Then    ' convert HMS/DMS to decimal hrs/degs
            ' see if we're dealing with time or angles based on the menu entry
            If Strings.Left(LTrim(UCase(lblFromUnits.Text)), 1) = "D" Then
                If Not isValidDMSAngle(strInput, resultDMSObj) Then Return result
                strDorH = "Degrees"
                doingHMS = False
                bNeg = resultDMSObj.isNegAngle()
                dORh = resultDMSObj.getDegrees()
                m = resultDMSObj.getMinutes()
                s = resultDMSObj.getSeconds()
                strInput = angleToStr(bNeg, dORh, m, s, DMSFORMAT)
            Else
                If Not isValidHMSTime(strInput, resultHMSObj) Then Return result
                doingHMS = True
                strDorH = "Hours"
                bNeg = resultHMSObj.isNegTime()
                dORh = resultHMSObj.getHours()
                m = resultHMSObj.getMinutes()
                s = resultHMSObj.getSeconds()
                strInput = timeToStr(bNeg, dORh, m, s, HMSFORMAT)
            End If

            prt.setBoldFont(True)
            printlnCond("Convert " & strInput & " " & lblFromUnits.Text & " to " & lblToUnits.Text, CENTERTXT)
            prt.setBoldFont(False)
            printlnCond()

            If bNeg Then
                SIGN = -1
            Else
                SIGN = 1
            End If

            prt.setFixedWidthFont()
            printlnCond("1. If the " & lblFromUnits.Text & " value entered is negative, let SIGN = -1")
            printlnCond("   else let SIGN = 1. Thus, SIGN = " & Format(SIGN, "#0"))

            If (bNeg) Then dORh = -dORh ' Temporarily make it negative for the next step
            printlnCond()
            printlnCond("2. Let " & strDorH & " = ABS(" & strDorH & " entered) = ABS(" & Format(dORh, "###0") & ") = " &
                        Format(Abs(dORh), "###0"))
            dORh = Abs(dORh)

            decValue = s / 60
            printlnCond()
            printlnCond("3. Convert seconds entered to decimal minutes (i.e., dm)")
            printlnCond("   dm = seconds / 60 = " & Format(s, strToFormat) & " / 60 = " & Format(decValue, strToFormat))

            printlnCond()
            printlnCond("4. Add results of step 3 to minutes entered")
            printlnCond("   Total minutes = dm + m = " & Format(decValue, strToFormat) & " + " & Format(m, "#0") &
                        " = " & Format(decValue + m, strToFormat))
            decValue = decValue + m

            printlnCond()
            printlnCond("5. Convert total minutes to " & lblToUnits.Text)
            printlnCond("   " & lblToUnits.Text & " = (Total minutes) / 60 = (" &
                        Format(decValue, strToFormat) & ") / 60" &
                        " = " & Format(decValue / 60, strToFormat))
            decValue = decValue / 60

            printlnCond()
            printlnCond("6. Add results of step 5 to the " & strDorH & " from step 2")
            printlnCond("   " & lblToUnits.Text & " = " & lblToUnits.Text & " + " & strDorH &
                        " = " & Format(decValue, strToFormat) & " + " & Format(dORh, "##0") &
                        " = " & Format(decValue + dORh, strToFormat))
            decValue = decValue + dORh

            printlnCond()
            printlnCond("7. Account for the sign of the " & strDorH & " that were entered")
            printlnCond("   " & lblToUnits.Text & " = SIGN * (" & lblToUnits.Text & ") = " &
                        Format(SIGN, "#0") & " * " & Format(decValue, strToFormat) &
                        " = " & Format(SIGN * decValue, strToFormat))
            decValue = decValue * SIGN

            result = Format(decValue, strToFormat)

            printlnCond()
            printlnCond("Thus, " & strInput & " " & lblFromUnits.Text & " = " & result)
        Else                        ' convert decimal hrs/degs to HMS/DMS
            If Not isValidReal(strInput, decValue) Then Return result

            strInput = Format(decValue, strFromFormat)

            prt.setBoldFont(True)
            printlnCond("Convert " & strInput & " " & lblFromUnits.Text & " to " & lblToUnits.Text, CENTERTXT)
            prt.setBoldFont(False)
            printlnCond()

            ' Figure out if we're doing degrees or time
            If Strings.Left(LTrim(UCase(lblToUnits.Text)), 1) = "D" Then
                doingHMS = False
                strDorH = "Degrees"
            Else
                doingHMS = True
                strDorH = "Hours"
            End If

            prt.setFixedWidthFont()
            printlnCond("Dec is the decimal number to convert = " & Format(decValue, strFromFormat) & " " & strDorH)
            printlnCond()
            If decValue < 0 Then
                SIGN = -1
                bNeg = True
            Else
                SIGN = 1
                bNeg = False
            End If

            printlnCond("1. If Dec is negative, let SIGN = -1 else let SIGN = 1. Thus, SIGN = " & Format(SIGN, "#0"))
            printlnCond()
            printlnCond("2. Dec = ABS(Dec) = ABS(" & Format(decValue, strFromFormat) & ") = " &
                        Format(Abs(decValue), strFromFormat))
            decValue = Abs(decValue)

            dORh = Trunc(decValue)
            printlnCond()
            printlnCond("3. " & strDorH & " = INT(Dec) = INT(" & Format(decValue, strFromFormat) & ") = " &
                        Format(dORh, "###0"))

            m = CType(Int(60.0 * Frac(decValue)), Integer)
            printlnCond()
            printlnCond("4. Minutes = INT[ 60 * FRAC(Dec) ] = " & "INT[ 60 * FRAC(" & Format(decValue, strFromFormat) & ") ]")
            printlnCond("           = " & Format(m, "##0"))

            s = Frac(60 * Frac(decValue))
            printlnCond()
            printlnCond("5. Seconds = 60 * FRAC[ 60 * FRAC(Dec) ]")
            printlnCond("           = 60 * FRAC[ 60 * FRAC(" & Format(decValue, strFromFormat) & ") ]")
            printlnCond("           = " & Format(60 * s, strFromFormat))
            s = 60 * s

            printlnCond()
            printlnCond("6. To account for a negative decimal number being converted,")
            printlnCond("   use the SIGN from step 1. Multiply by SIGN if " & strDorH & " is > 0")
            printlnCond("   and merely append '-' if " & strDorH & " is 0. So,")
            If (dORh = 0) Then
                If (bNeg) Then
                    strSIGN = "-"
                Else
                    strSIGN = ""
                End If
                printlnCond("   " & strDorH & " = " & strSIGN & dORh)
            Else
                printlnCond("   " & strDorH & " = SIGN * " & strDorH & " = (" & Format(SIGN, "#0") & ") * (" &
                            Format(dORh, "###0") & ") = " & Format(SIGN * dORh, "###0"))
            End If

            If doingHMS Then
                result = timeToStr(bNeg, dORh, m, s, HMSFORMAT)
            Else
                result = angleToStr(bNeg, dORh, m, s, DMSFORMAT)
            End If

            printlnCond()
            printlnCond("Thus, " & strInput & " " & strDorH & " = " & result)
        End If

        prt.setProportionalFont()
        prt.resetCursor()
        Return result
    End Function

    '-------------------------------------------------------------
    ' Convert between degrees C and degrees F. The data
    ' to be converted is in the txtboxUserInput field in the GUI.
    '
    ' The temperature conversion result is returned as a string.
    '-------------------------------------------------------------
    Private Function convertC_F() As String
        Dim degOut, degIn As Double
        Dim strInput As String = removeWhitespace(txtboxUserInput.Text)

        If Not isValidReal(strInput, degIn) Then
            Return " "
        End If

        prt.setBoldFont(True)
        printlnCond("Convert " & strInput & " " & lblFromUnits.Text & " to " & lblToUnits.Text, CENTERTXT)
        prt.setBoldFont(False)
        printlnCond()

        prt.setFixedWidthFont()

        ' See if we're converting C to F or F to C
        If dFromDenom < 0.0 Then     ' convert C to F
            degOut = 32.0 + (degIn * 9.0) / 5.0
            printlnCond("1. The proper formula is deg F = 32 + (deg C * 9) / 5")

            printlnCond()
            printlnCond("2. Putting in the known values and performing the math, we have")
            printlnCond("   deg F = 32 + (" & Format(degIn, strFromFormat) & " deg * 9) / 5")
            printlnCond("         = " & Format(degOut, strToFormat) & " deg F")

            printlnCond()
            printlnCond("Thus, " & Format(degIn, strFromFormat) & " degrees C = " & Format(degOut, strToFormat) & " degrees F")
        Else                            ' convert F to C
            degOut = (degIn - 32.0) * (5.0 / 9.0)

            printlnCond("1. The proper formula is deg C = (deg F - 32) * (5/9)")

            printlnCond()
            printlnCond("2. Putting in the known values and performing the math, we have")
            printlnCond("   deg C = (" & Format(degIn, strFromFormat) & " deg - 32) * (5/9)")
            printlnCond("         = " & Format(degOut, strToFormat) & " deg C")

            printlnCond()
            printlnCond("Thus, " & Format(degIn, strFromFormat) & " degrees F = " & Format(degOut, strToFormat) & " degrees C")
        End If

        prt.setProportionalFont()
        prt.resetCursor()
        convertC_F = Format(degOut, strToFormat)
    End Function

    '----------------------------------------------------------
    ' Routines to handle sending output text to the scrollable
    ' output area. These are wrappers around ASTUtils.println
    ' that see if the 'Show Intermediate Calculations' checkbox
    ' is checked before invoking the println functions.
    '
    ' This method is overloaded to allow various combinations
    ' of what is passed as parameters.
    '
    ' txt           string to be printed
    ' centerTxt     true if the text is to be centered
    '----------------------------------------------------------
    Private Sub printlnCond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlnCond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlnCond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub
End Class