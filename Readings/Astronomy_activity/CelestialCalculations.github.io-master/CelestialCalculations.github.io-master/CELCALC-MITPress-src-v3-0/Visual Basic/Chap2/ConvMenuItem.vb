﻿'**********************************************************
'                      Copyright (c) 2018
'                    Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' Conversion menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ConvMenuItem

    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define values for the eConvType field in a ConvMenuItem class. These indicate how to do unit conversions.
    Friend Enum ConversionType
        CROSS_MULTIPLY
        HorDMS_HRSorDEGS
        CENTIGRADE_FAHRENHEIT
    End Enum

    ' Strictly speaking, these fields should be private and get/set methods should be
    ' used to access them. However, there's little real value in doing that in this case
    ' because the other components in the GUI that were created with the Visual Basic
    ' form builder are not accessed that way. Hence, get/set methods are omitted to
    ' be consistent with the rest of the GUI components.

    ' Info about data to convert from
    Friend strFromUnits As String       ' units
    Friend dFromDenom As Double         ' denominator
    Friend strFromFormat As String      ' how to format data
    ' Info about units to convert to
    Friend strToUnits As String         ' units
    Friend dToDenom As Double           ' denominator
    Friend strToFormat As String        ' how to format data

    Friend eConvType As ConversionType  ' what type of conversion to perform

    ' Note that the to/from denominators are actually overloaded in their usage.
    ' For menu items that are to be cross-multiplied, they are the denominators
    ' in the equation to form to do the corss multiplication. For other usages,
    ' such as C to F conversion, dToDenom is -1.0 while dFromDenom is 1.0. These values
    ' may be swapped when the switch units button is clicked so that they continue
    ' to track with the other "to/from" values (e.g., strToUnits/strFromUnits). When
    ' it is time to do a conversion, if the Chap2GUI class's private dFromDenom is
    ' negative, the conversion to perform is the in the direction indicated by the
    ' corresponding menu entry. For example, for the "degs C - degs F" menu entry,
    ' if fFronDenom is negative then convert degs C to degs F. If dFromDenom is
    ' positive, convert degs F to degs C.

    '***************************************************************************
    ' Initialize when an instance is created
    '
    ' FromUnits         text to display for the 'from' units (e.g., km)
    ' FromDenom         numeric value for the 'from' denominator (e.g, 5.0)
    ' FromFromat        how to format the 'from' denominator
    ' ToUnits           text to display for the 'to' units (e.g., miles)
    ' ToDenom           numeric value for the 'to' denominator (e.g, 5.0)
    ' ToFromat          how to format the 'to' denominator
    ' ConvType          what type of conversion is to be done
    '***************************************************************************
    Sub New(ByVal FromUnits As String, ByVal FromDenom As Double, ByVal FromFormat As String,
      ByVal ToUnits As String, ByVal ToDenom As Double, ByVal ToFormat As String,
      ByVal ConvType As ConversionType)

        strFromUnits = FromUnits
        dFromDenom = FromDenom
        strFromFormat = FromFormat

        strToUnits = ToUnits
        dToDenom = ToDenom
        strToFormat = ToFormat

        eConvType = ConvType
    End Sub
End Class
