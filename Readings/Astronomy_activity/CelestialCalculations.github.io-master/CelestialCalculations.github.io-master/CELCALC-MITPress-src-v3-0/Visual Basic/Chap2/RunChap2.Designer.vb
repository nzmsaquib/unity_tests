﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chap2GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Chap2GUI))
        Me.ChapMenu = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuConversions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemInstructions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelBookTitle = New System.Windows.Forms.Panel()
        Me.lblBookTitle = New System.Windows.Forms.Label()
        Me.panelChkBox = New System.Windows.Forms.Panel()
        Me.chkboxShowInterimCalcs = New System.Windows.Forms.CheckBox()
        Me.panelUserInput = New System.Windows.Forms.Panel()
        Me.lblToUnits = New System.Windows.Forms.Label()
        Me.lblFromUnits = New System.Windows.Forms.Label()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.txtboxUserInput = New System.Windows.Forms.TextBox()
        Me.btnSwapUnits = New System.Windows.Forms.Button()
        Me.btnConvert = New System.Windows.Forms.Button()
        Me.panelTextOut = New System.Windows.Forms.Panel()
        Me.txtboxOutputArea = New System.Windows.Forms.RichTextBox()
        Me.ChapMenu.SuspendLayout()
        Me.panelBookTitle.SuspendLayout()
        Me.panelChkBox.SuspendLayout()
        Me.panelUserInput.SuspendLayout()
        Me.panelTextOut.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChapMenu
        '
        Me.ChapMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ChapMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuConversions, Me.mnuHelp})
        Me.ChapMenu.Location = New System.Drawing.Point(0, 0)
        Me.ChapMenu.Name = "ChapMenu"
        Me.ChapMenu.Size = New System.Drawing.Size(747, 28)
        Me.ChapMenu.TabIndex = 0
        Me.ChapMenu.Text = "ChapMenu"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuItemExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(44, 24)
        Me.mnuFile.Text = "File"
        '
        'mnuItemExit
        '
        Me.mnuItemExit.Name = "mnuItemExit"
        Me.mnuItemExit.Size = New System.Drawing.Size(181, 26)
        Me.mnuItemExit.Text = "Exit"
        '
        'mnuConversions
        '
        Me.mnuConversions.Name = "mnuConversions"
        Me.mnuConversions.Size = New System.Drawing.Size(100, 24)
        Me.mnuConversions.Text = "Conversions"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemInstructions, Me.mnuitemAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(53, 24)
        Me.mnuHelp.Text = "Help"
        '
        'mnuitemInstructions
        '
        Me.mnuitemInstructions.Name = "mnuitemInstructions"
        Me.mnuitemInstructions.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemInstructions.Text = "Instructions"
        '
        'mnuitemAbout
        '
        Me.mnuitemAbout.Name = "mnuitemAbout"
        Me.mnuitemAbout.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemAbout.Text = "About"
        '
        'panelBookTitle
        '
        Me.panelBookTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBookTitle.BackColor = System.Drawing.Color.Navy
        Me.panelBookTitle.Controls.Add(Me.lblBookTitle)
        Me.panelBookTitle.Location = New System.Drawing.Point(0, 32)
        Me.panelBookTitle.Name = "panelBookTitle"
        Me.panelBookTitle.Size = New System.Drawing.Size(747, 49)
        Me.panelBookTitle.TabIndex = 1
        '
        'lblBookTitle
        '
        Me.lblBookTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblBookTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBookTitle.ForeColor = System.Drawing.Color.White
        Me.lblBookTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBookTitle.Location = New System.Drawing.Point(0, 13)
        Me.lblBookTitle.Name = "lblBookTitle"
        Me.lblBookTitle.Padding = New System.Windows.Forms.Padding(1)
        Me.lblBookTitle.Size = New System.Drawing.Size(747, 27)
        Me.lblBookTitle.TabIndex = 0
        Me.lblBookTitle.Text = "Book Title"
        Me.lblBookTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'panelChkBox
        '
        Me.panelChkBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelChkBox.Controls.Add(Me.chkboxShowInterimCalcs)
        Me.panelChkBox.Location = New System.Drawing.Point(0, 86)
        Me.panelChkBox.Name = "panelChkBox"
        Me.panelChkBox.Size = New System.Drawing.Size(746, 41)
        Me.panelChkBox.TabIndex = 2
        '
        'chkboxShowInterimCalcs
        '
        Me.chkboxShowInterimCalcs.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.chkboxShowInterimCalcs.AutoSize = True
        Me.chkboxShowInterimCalcs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxShowInterimCalcs.Location = New System.Drawing.Point(265, 9)
        Me.chkboxShowInterimCalcs.Name = "chkboxShowInterimCalcs"
        Me.chkboxShowInterimCalcs.Size = New System.Drawing.Size(266, 24)
        Me.chkboxShowInterimCalcs.TabIndex = 1
        Me.chkboxShowInterimCalcs.Text = "Show Intermediate Calculations"
        Me.chkboxShowInterimCalcs.UseVisualStyleBackColor = True
        '
        'panelUserInput
        '
        Me.panelUserInput.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelUserInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelUserInput.Controls.Add(Me.lblToUnits)
        Me.panelUserInput.Controls.Add(Me.lblFromUnits)
        Me.panelUserInput.Controls.Add(Me.lblResult)
        Me.panelUserInput.Controls.Add(Me.txtboxUserInput)
        Me.panelUserInput.Controls.Add(Me.btnSwapUnits)
        Me.panelUserInput.Controls.Add(Me.btnConvert)
        Me.panelUserInput.Location = New System.Drawing.Point(2, 131)
        Me.panelUserInput.Name = "panelUserInput"
        Me.panelUserInput.Size = New System.Drawing.Size(742, 82)
        Me.panelUserInput.TabIndex = 3
        '
        'lblToUnits
        '
        Me.lblToUnits.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblToUnits.AutoSize = True
        Me.lblToUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToUnits.Location = New System.Drawing.Point(534, 47)
        Me.lblToUnits.Name = "lblToUnits"
        Me.lblToUnits.Size = New System.Drawing.Size(28, 20)
        Me.lblToUnits.TabIndex = 7
        Me.lblToUnits.Text = "To"
        '
        'lblFromUnits
        '
        Me.lblFromUnits.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblFromUnits.AutoSize = True
        Me.lblFromUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFromUnits.Location = New System.Drawing.Point(534, 16)
        Me.lblFromUnits.Name = "lblFromUnits"
        Me.lblFromUnits.Size = New System.Drawing.Size(48, 20)
        Me.lblFromUnits.TabIndex = 6
        Me.lblFromUnits.Text = "From"
        '
        'lblResult
        '
        Me.lblResult.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblResult.AutoSize = True
        Me.lblResult.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResult.Location = New System.Drawing.Point(130, 50)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(55, 20)
        Me.lblResult.TabIndex = 5
        Me.lblResult.Text = "Label"
        '
        'txtboxUserInput
        '
        Me.txtboxUserInput.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtboxUserInput.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxUserInput.Location = New System.Drawing.Point(129, 10)
        Me.txtboxUserInput.Name = "txtboxUserInput"
        Me.txtboxUserInput.Size = New System.Drawing.Size(400, 27)
        Me.txtboxUserInput.TabIndex = 3
        '
        'btnSwapUnits
        '
        Me.btnSwapUnits.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSwapUnits.Location = New System.Drawing.Point(11, 43)
        Me.btnSwapUnits.Name = "btnSwapUnits"
        Me.btnSwapUnits.Size = New System.Drawing.Size(100, 30)
        Me.btnSwapUnits.TabIndex = 4
        Me.btnSwapUnits.Text = "Swap Units"
        Me.btnSwapUnits.UseVisualStyleBackColor = True
        '
        'btnConvert
        '
        Me.btnConvert.Location = New System.Drawing.Point(11, 6)
        Me.btnConvert.Name = "btnConvert"
        Me.btnConvert.Size = New System.Drawing.Size(100, 30)
        Me.btnConvert.TabIndex = 2
        Me.btnConvert.Text = "Convert"
        Me.btnConvert.UseVisualStyleBackColor = True
        '
        'panelTextOut
        '
        Me.panelTextOut.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelTextOut.Controls.Add(Me.txtboxOutputArea)
        Me.panelTextOut.Location = New System.Drawing.Point(2, 221)
        Me.panelTextOut.Margin = New System.Windows.Forms.Padding(5)
        Me.panelTextOut.Name = "panelTextOut"
        Me.panelTextOut.Size = New System.Drawing.Size(741, 492)
        Me.panelTextOut.TabIndex = 4
        '
        'txtboxOutputArea
        '
        Me.txtboxOutputArea.BackColor = System.Drawing.Color.White
        Me.txtboxOutputArea.Cursor = System.Windows.Forms.Cursors.No
        Me.txtboxOutputArea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtboxOutputArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxOutputArea.Location = New System.Drawing.Point(0, 0)
        Me.txtboxOutputArea.Name = "txtboxOutputArea"
        Me.txtboxOutputArea.ReadOnly = True
        Me.txtboxOutputArea.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtboxOutputArea.Size = New System.Drawing.Size(741, 492)
        Me.txtboxOutputArea.TabIndex = 0
        Me.txtboxOutputArea.TabStop = False
        Me.txtboxOutputArea.Text = ""
        '
        'Chap2GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(747, 715)
        Me.Controls.Add(Me.panelTextOut)
        Me.Controls.Add(Me.panelUserInput)
        Me.Controls.Add(Me.panelChkBox)
        Me.Controls.Add(Me.panelBookTitle)
        Me.Controls.Add(Me.ChapMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.ChapMenu
        Me.Name = "Chap2GUI"
        Me.Text = "Title"
        Me.ChapMenu.ResumeLayout(False)
        Me.ChapMenu.PerformLayout()
        Me.panelBookTitle.ResumeLayout(False)
        Me.panelChkBox.ResumeLayout(False)
        Me.panelChkBox.PerformLayout()
        Me.panelUserInput.ResumeLayout(False)
        Me.panelUserInput.PerformLayout()
        Me.panelTextOut.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChapMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuConversions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemInstructions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents panelBookTitle As System.Windows.Forms.Panel
    Private WithEvents lblBookTitle As System.Windows.Forms.Label
    Private WithEvents chkboxShowInterimCalcs As System.Windows.Forms.CheckBox
    Friend WithEvents panelUserInput As System.Windows.Forms.Panel
    Friend WithEvents panelChkBox As System.Windows.Forms.Panel
    Friend WithEvents btnSwapUnits As System.Windows.Forms.Button
    Friend WithEvents btnConvert As System.Windows.Forms.Button
    Friend WithEvents txtboxUserInput As System.Windows.Forms.TextBox
    Friend WithEvents lblResult As System.Windows.Forms.Label
    Friend WithEvents lblToUnits As System.Windows.Forms.Label
    Friend WithEvents lblFromUnits As System.Windows.Forms.Label
    Friend WithEvents panelTextOut As System.Windows.Forms.Panel
    Friend WithEvents txtboxOutputArea As System.Windows.Forms.RichTextBox

End Class
