﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define possible menu items. These help in deciding what routine to
    ' execute when a menu item is selected
    Friend Enum CalculationType
        ' Solar Info menu
        SUN_LOCATION
        SUN_RISE_SET
        EQUINOXES_SOLSTICES
        SUN_DIST_AND_ANG_DIAMETER
        EQ_OF_TIME
        TERM_CRITERIA
        ' Orbital Elements menu
        LOAD_ORBITAL_ELEMENTS
        SHOW_ORBITAL_ELEMENTS
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub

End Class
