﻿'******************************************************************
'                      Chapter 6 - The Sun
'                       Copyright (c) 2018
'                     Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' for a program that will calculate various items about
' the Sun, including its location and times for sunrise/set.
' The main GUI was mostly built with the Visual Basic form editor.
'******************************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTCoord
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTObserver
Imports ASTUtils.ASTOrbits
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap6.ChapMenuItem

Public Class Chap6GUI
    Private Const AboutBoxText = "Chapter 6 - The Sun"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt = Nothing
    Private orbElements As ASTOrbits = Nothing
    Private observer As ASTObserver = Nothing

    ' Create a reusable query form for user input
    Private queryForm As ASTQuery = New ASTQuery()

    ' Keep track of the index into the orbital elements database for the Sun
    ' so that we don't have to get it over and over
    Private idxSun As Integer

    ' Set a default termination criteria (in radians) for solving Kepler's equation
    Private termCriteria As Double = 0.000002

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearTextAreas()

        ' Create the menu items
        createChapMenus()

        ' Now initialize the rest of the GUI
        chkboxShowInterimCalcs.Checked = False
        chkboxDST.Checked = False
        radbtnLon.Checked = True
        radbtnEQofCenter.Checked = True

        ' Load the default orbital elements and observer location (if it exists)
        observer = New ASTObserver
        setObsDataInGUI(observer)
        orbElements = New ASTOrbits(prt)

        ' get the index into the orbital elements database for the Sun
        ' and the epoch to which the orbital elements are referenced
        idxSun = orbElements.getOEDBSunIndex()
        setEpoch(orbElements.getOEEpochDate)

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------------------------------------
    ' Create the Solar Info and Orbital Elements menus.
    '---------------------------------------------------------------------------------------------

    '------------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler.
    '
    ' title         title for the menu item
    ' CalcType      what type of calculation to perform
    '               when this menu item is clicked
    '------------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '--------------------------------------------
    ' Create the Solar Info and Orbital Elements
    ' menu subitems
    '--------------------------------------------
    Private Sub createChapMenus()
        ' Create Solar Info menu items
        mnuSolarInfo.DropDownItems.Add(createMenuItem("Sun's Position", CalculationType.SUN_LOCATION))
        mnuSolarInfo.DropDownItems.Add("-")
        mnuSolarInfo.DropDownItems.Add(createMenuItem("Sunrise/Sunset", CalculationType.SUN_RISE_SET))
        mnuSolarInfo.DropDownItems.Add(createMenuItem("Equinoxes and Solstices", CalculationType.EQUINOXES_SOLSTICES))
        mnuSolarInfo.DropDownItems.Add("-")
        mnuSolarInfo.DropDownItems.Add(createMenuItem("Solar Distance and Angular Diameter", CalculationType.SUN_DIST_AND_ANG_DIAMETER))
        mnuSolarInfo.DropDownItems.Add("-")
        mnuSolarInfo.DropDownItems.Add(createMenuItem("Equation of Time", CalculationType.EQ_OF_TIME))
        mnuSolarInfo.DropDownItems.Add("-")
        mnuSolarInfo.DropDownItems.Add(createMenuItem("Set Termination Criteria", CalculationType.TERM_CRITERIA))

        ' Create Orbital Elements menu
        mnuOrbitalElements.DropDownItems.Add(createMenuItem("Load Orbital Elements", CalculationType.LOAD_ORBITAL_ELEMENTS))
        mnuOrbitalElements.DropDownItems.Add(createMenuItem("Display Sun's Orbital Elements", CalculationType.SHOW_ORBITAL_ELEMENTS))
    End Sub

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '----------------------------------------
    ' Handle clicks on the Solar Info and
    ' Orbital Elements menu items
    '----------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        Select Case calctype
            '************* Solar Info menu
            Case CalculationType.SUN_LOCATION
                calcSunPosition()
            Case CalculationType.SUN_RISE_SET
                calcSunRiseSet()
            Case CalculationType.EQUINOXES_SOLSTICES
                calcEquinoxesSolstices()
            Case CalculationType.SUN_DIST_AND_ANG_DIAMETER
                calcDistAndAngDiameter()
            Case CalculationType.EQ_OF_TIME
                calcEqOfTime()
            Case CalculationType.TERM_CRITERIA
                setTerminationCriteria()

                '************* Orbital Elements menu
            Case CalculationType.LOAD_ORBITAL_ELEMENTS
                loadOrbitalElements()
            Case CalculationType.SHOW_ORBITAL_ELEMENTS
                showSunsOrbitalElements()
        End Select

    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " &
                    "be used in subsequent calculations. Not all of the observer information is required for every " &
                    "calculation (e.g., only the year is used to calculate the solstices and equinoxes), but all of " &
                    "data items must be entered anyway. You may find it convenient to enter an initial latitude, longitude, " &
                    "and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " &
                    "longitude, and time zone are already filled in with default values when the program starts. When this program " &
                    "begins, the date and time will default to the local date and time at which the program is started.")
        prt.println()
        prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " &
                    "various calculations are performed. Also, select the appropriate radio button to choose what method to " &
                    "use when it is necessary to determine the Sun's true anomaly. The radio button 'EQ of Center' will solve " &
                    "the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " &
                    "'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " &
                    "Newton/Raphson method. The menu entry 'Solar Info->Set Termination Critera' allows you to enter the " &
                    "termination criteria (in radians) for when to stop iterating to solve Kepler's equation.")
        prt.println()
        prt.println("The menu 'Solar Info' performs various calculations related to the Sun, such as determining its " &
                    "location for the current information in the 'Observer Location and Time' data area. The menu " &
                    "'Orbital Elements' allow you to load data files that contain the Sun's orbital elements referenced to " &
                    "a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " &
                    "starts up. The menu entry 'Orbital Elements->Load Orbital Elements' allows you to load and use orbital elements " &
                    "referenced to some other epoch.")

        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Solar Info menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Calculate the Sun's distance and angular diameter.
    '------------------------------------------------------------
    Private Sub calcDistAndAngDiameter()
        Dim eclCoord As ASTCoord
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim Msun, Vsun, dF, Ecc, dDist, dAngDiameter As Double

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Compute the Sun's Distance and Angular Diameter for the Current Date", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If
        Ecc = orbElements.getOEObjEccentricity(idxSun)

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                    observer.getObsDate.getYear, 0.0, Msun, Vsun, solveTrueAnomaly, termCriteria)
        printlncond("1.  Compute the Sun's true anomaly at 0 hours UT.")
        printlncond("    Vsun = " & angleToStr(Vsun, DECFORMAT) & " degrees")
        printlncond()

        dF = (1 + Ecc * COS_D(Vsun)) / (1 - Ecc * Ecc)
        printlncond("2.  Compute F = [1 + eccentricity*cos(Vsun)] / [1-eccentricity^2]")
        printlncond("              = [1 + " & Format(Ecc, genFloatFormat) & "*cos(" & angleToStr(Vsun, DECFORMAT) &
                    ")] / [1 - " & Format(Ecc, genFloatFormat) & "^2]")
        printlncond("              = " & Format(dF, genFloatFormat))
        printlncond()

        dDist = orbElements.getOEObjSemiMajAxisKM(idxSun) / dF
        printlncond("3.  Compute the distance to the Sun.")
        printlncond("    Dist = a0/F where a0 is the length of the Sun's semi-major axis in km")
        printlncond("    Dist = (" & insertCommas(orbElements.getOEObjSemiMajAxisKM(idxSun)) & ")/" & Format(dF, genFloatFormat))
        printlncond("         = " & insertCommas(dDist) & " km")
        printlncond()

        dAngDiameter = orbElements.getOEObjAngDiamDeg(idxSun) * dF
        printlncond("4.  Compute the Sun's angular diameter.")
        printlncond("    Theta_sun = Theta_0*F where Theta_0 is the Sun's angular diameter in degrees")
        printlncond("    when the Sun is distance a0 away.")
        printlncond("    Theta_sun = " & angleToStr(orbElements.getOEObjAngDiamDeg(idxSun), DECFORMAT) & "*" &
                    Format(dF, genFloatFormat) & " = " & angleToStr(dAngDiameter, DECFORMAT) & " degrees")
        printlncond()

        dDist = Math.Round(dDist, 2)
        printlncond("5.  Convert distance to miles and angular diameter to DMS format.")
        printlncond("    Dist = " & insertCommas(dDist) & " km = " & insertCommas(Math.Round(KM2Miles(dDist), 2)) & " miles")
        printlncond("    Theta_sun = " & angleToStr(dAngDiameter, DMSFORMAT))
        printlncond()

        printlncond("On " & dateToStr(observer.getObsDate) & ", the Sun is/was " & insertCommas(dDist) & " km away")
        printlncond("and its angular diameter is/was " & angleToStr(dAngDiameter, DMSFORMAT))

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Sun's Distance: " & insertCommas(dDist) & " km (" & insertCommas(Math.Round(KM2Miles(dDist), 2)) &
            " miles), Angular Diameter: " & angleToStr(dAngDiameter, DMSFORMAT)
    End Sub

    '------------------------------------------------------------
    ' Calculate the equation of time.
    '------------------------------------------------------------
    Private Sub calcEqOfTime()
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim eclCoord As ASTCoord
        Dim Msun, Vsun, dE, y, Lsun, dT, Ecc As Double

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Compute the Equation of Time for " & dateToStr(observer.getObsDate), CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If
        Ecc = orbElements.getOEObjEccentricity(idxSun)

        eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                    observer.getObsDate.getYear, 0.0, Msun, Vsun, solveTrueAnomaly, termCriteria)
        Lsun = eclCoord.getLonAngle.getDecAngle
        printlncond("1.  Compute the Sun's ecliptic longitude and mean anomaly for the given date.")
        printlncond("    Lsun = " & angleToStr(Lsun, DECFORMAT) & " degrees")
        printlncond("    Msun = " & angleToStr(Msun, DECFORMAT) & " degrees")
        printlncond()

        dE = calcEclipticObliquity(observer.getObsDate.getYear)
        printlncond("2.  Compute the obliquity of the ecliptic for the given year.")
        printlncond("    obliq = " & angleToStr(dE, DECFORMAT) & " degrees")
        printlncond()

        y = TAN_D(dE / 2.0)
        y = y * y
        printlncond("3.  Compute y = tan^2(obliq/2) = tan^2(" & angleToStr(dE, DECFORMAT) & "/2")
        printlncond("              = " & Format(y, genFloatFormat))
        printlncond()

        dT = y * SIN_D(2 * Lsun) - 2 * Ecc * SIN_D(Msun) + 4 * Ecc * y * SIN_D(Msun) * COS_D(2 * Lsun)
        dT = dT - (y * y / 2.0) * SIN_D(4 * Lsun) - ((5 * Ecc * Ecc) / 4) * SIN_D(2 * Msun)
        printlncond("4.  Calculate a delta T correction in radians from the interpolation formula")
        printlncond("    dT = y*sin(2*Lsun) - 2*eccentricity*sin(Msun) + ")
        printlncond("         4*eccentricity*y*sin(Msun)*cos(2*Lsun) -")
        printlncond("         (y^2/2.0)*sin(4*Lsun) - (5/4)*(eccentricity^2)*sin(2*Msun)")
        printlncond("       = " & Format(y, genFloatFormat) & "*sin(2*" & angleToStr(Lsun, DECFORMAT) &
                    ") - 2*" & Format(Ecc, genFloatFormat) & "*sin(" & angleToStr(Msun, DECFORMAT) & ") + ")
        printlncond("         4*" & Format(Ecc, genFloatFormat) & "*" & Format(y, genFloatFormat) & "sin(" &
                    angleToStr(Msun, DECFORMAT) & ")*cos(2*" & angleToStr(Lsun, DECFORMAT) & ") - ")
        printlncond("         (" & Format(y, genFloatFormat) & "^2/2.0)*sin(4*" & angleToStr(Lsun, DECFORMAT) &
                    ") - (5/4)*(" & Format(Ecc, genFloatFormat) & "^2)*sin(2*" & angleToStr(Msun, DECFORMAT) & ")")
        printlncond("       = " & Format(dT, genFloatFormat) & " radians")
        printlncond()

        dT = -dT
        printlncond("5.  Multiply by -1.")
        printlncond("    dT = -1 * dT = " & Format(dT, genFloatFormat) & " radians")
        printlncond()

        dT = rad2deg(dT)
        printlncond("6.  Convert dT from radians to degrees.")
        printlncond("    dT = (180/PI)*dT = " & angleToStr(dT, DECFORMAT) & " degrees")
        printlncond()

        dT = dT / 15.0
        printlncond("7.  Convert dT from degrees to hours by dividing by 15.")
        printlncond("    dT = " & timeToStr(dT, DECFORMAT) & " hours")
        printlncond()

        printlncond("8.  Convert dT to HMS format.")
        printlncond("    dT = " & timeToStr(dT, HMSFORMAT))
        printlncond()

        printlncond("For the given date, the equation of time correction is " & timeToStr(dT, HMSFORMAT))

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "For " & dateToStr(observer.getObsDate) & ", the equation of time is " &
            timeToStr(dT, HMSFORMAT)
    End Sub

    '------------------------------------------------------------
    ' Calculate the times of the equinoxes and solstices.
    '------------------------------------------------------------
    Private Sub calcEquinoxesSolstices()
        Dim dT, dT2, dT3, dY, JDm, JDj, JDs, JDd As Double
        Dim UTm, UTj, UTs, UTd As Double
        Dim marchDate, juneDate, septDate, decDate As ASTDate

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Compute the Equinoxes and Solstices for the Year", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        dY = observer.getObsDate.getYear
        dT = dY / 1000.0
        dT2 = dT * dT
        dT3 = dT2 * dT
        printlncond("1.  Calculate T = Year/1000 = " & dY & "/1000 = " & Format(dT, genFloatFormat))
        printlncond()

        JDm = 1721139.2855 + 365.2421376 * dY + 0.067919 * dT2 - 0.0027879 * dT3
        JDj = 1721233.2486 + 365.2417284 * dY - 0.053018 * dT2 + 0.009332 * dT3
        JDs = 1721325.6978 + 365.2425055 * dY - 0.126689 * dT2 + 0.0019401 * dT3
        JDd = 1721414.392 + 365.2428898 * dY - 0.010965 * dT2 - 0.0084885 * dT3
        printlncond("2.  Compute the Julian day numbers for the equinoxes and solstices.")
        printlncond("    JDm = 1721139.2855 + 365.2421376*Year + 0.067919*T^2 - 0.0027879*T^3")
        printlncond("        = 1721139.2855 + 365.2421376*" & dY & " + 0.067919*(" &
                    Format(dT, genFloatFormat) & ")^2 - 0.0027879*(" & Format(dT, genFloatFormat) & ")^3")
        printlncond("        = " & Format(JDm, JDFormat))
        printlncond("    JDj = 1721233.2486 + 365.2417284*Year - 0.053018*T^2 + 0.009332*T^3")
        printlncond("        = 1721233.2486 + 365.2417284*" & dY & " - 0.053018*(" &
                    Format(dT, genFloatFormat) & ")^2 + 0.009332*(" & Format(dT, genFloatFormat) & ")^3")
        printlncond("        = " & Format(JDj, JDFormat))
        printlncond("    JDs = 1721325.6978 + 365.2425055*Year - 0.126689*T^2 + 0.0019401*T^3")
        printlncond("        = 1721325.6978 + 365.2425055*" & dY & " - 0.126689*(" &
                    Format(dT, genFloatFormat) & ")^2 + 0.0019401*(" & Format(dT, genFloatFormat) & ")^3")
        printlncond("        = " & Format(JDs, JDFormat))
        printlncond("    JDd = 1721414.392 + 365.2428898*Year - 0.010965*T^2 - 0.0084885*T^3")
        printlncond("        = 1721414.392 + 365.2428898*" & dY & " - 0.010965*(" &
                    Format(dT, genFloatFormat) & ")^2 - 0.0084885*(" & Format(dT, genFloatFormat) & ")^3")
        printlncond("        = " & Format(JDd, JDFormat))
        printlncond()

        marchDate = JDtoDate(JDm)
        juneDate = JDtoDate(JDj)
        septDate = JDtoDate(JDs)
        decDate = JDtoDate(JDd)
        printlncond("3.  Convert the Julian day numbers from the previous step to calendar dates.")
        printlncond("    JDm gives " & marchDate.getMonth & "/" & Format(marchDate.getdDay, genFloatFormat) &
                    "/" & marchDate.getYear)
        printlncond("    JDj gives " & juneDate.getMonth & "/" & Format(juneDate.getdDay, genFloatFormat) &
            "/" & juneDate.getYear)
        printlncond("    JDs gives " & septDate.getMonth & "/" & Format(septDate.getdDay, genFloatFormat) &
            "/" & septDate.getYear)
        printlncond("    JDd gives " & decDate.getMonth & "/" & Format(decDate.getdDay, genFloatFormat) &
            "/" & decDate.getYear)
        printlncond()

        UTm = Frac(marchDate.getdDay) * 24.0
        UTj = Frac(juneDate.getdDay) * 24.0
        UTs = Frac(septDate.getdDay) * 24.0
        UTd = Frac(decDate.getdDay) * 24.0
        printlncond("4.  Convert the data part of the dates to a day and decimal UT.")
        printlncond("    March: day " & marchDate.getiDay & ", " & timeToStr(UTm, DECFORMAT) & " hours")
        printlncond("    June: day " & juneDate.getiDay & ", " & timeToStr(UTj, DECFORMAT) & " hours")
        printlncond("    Sept: day " & septDate.getiDay & ", " & timeToStr(UTs, DECFORMAT) & " hours")
        printlncond("    Dec: day " & decDate.getiDay & ", " & timeToStr(UTd, DECFORMAT) & " hours")
        printlncond()

        printlncond("5.  Convert the UT results to HMS format.")
        prt.println("    March equinox on " & dateToStr(marchDate) & " at " & timeToStr(UTm, HMSFORMAT) & " UT")
        prt.println("    June solstice on " & dateToStr(juneDate) & " at " & timeToStr(UTj, HMSFORMAT) & " UT")
        prt.println("    September equinox on " & dateToStr(septDate) & " at " & timeToStr(UTs, HMSFORMAT) & " UT")
        prt.println("    December equinox on " & dateToStr(decDate) & " at " & timeToStr(UTd, HMSFORMAT) & " UT")

        lblResults.Text = "Solstices and Equinoxes are listed in the text area below"
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------------------
    ' Calculate the position of the Sun, using the currently
    ' loaded orbital elements and the current observer position.
    '------------------------------------------------------------
    Private Sub calcSunPosition()
        Dim result As String
        Dim LCT, UT, LST, GST, JD, JDe As Double
        Dim De, Msun, Ec, Ea, Vsun, Lsun, Ecc As Double
        Dim Bsun As Double = 0.0
        Dim eqCoord, horizonCoord As ASTCoord
        Dim dateAdjust As Integer
        Dim iter As Integer
        Dim adjustedDate As ASTDate
        Dim prtObj As ASTPrt = Nothing

        ' Set up whether to display interim results when solving Kepler's equation
        If chkboxShowInterimCalcs.Checked Then
            prtObj = prt
        Else
            prtObj = Nothing
        End If

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Compute the Sun's Position for the Current Observer", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        Ecc = orbElements.getOEObjEccentricity(idxSun)

        ' Do all the time-related calculations
        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)

        printlncond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
        printlncond("    LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(observer.getObsDate) & ")")
        printcond("     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond("previous day ")
        ElseIf dateAdjust > 0 Then
            printcond("next day ")
        End If
        printlncond(dateToStr(adjustedDate) & ")")
        printlncond("    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond("    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        JDe = orbElements.getOEEpochJD
        printlncond("2.  Compute the Julian day number for the standard epoch.")
        printlncond("    Epoch: " & Format(orbElements.getOEEpochDate, epochFormat))
        printlncond("    JDe = " & Format(JDe, JDFormat))
        printlncond()

        JD = dateToJD(adjustedDate.getMonth, adjustedDate.getdDay + (UT / 24.0), adjustedDate.getYear)
        printlncond("3.  Compute the Julian day number for the desired date, being sure to use the")
        printlncond("    Greenwich date and UT from step 1, and including the fractional part of the day.")
        printlncond("    JD = " & Format(JD, JDFormat))
        printlncond()

        De = JD - JDe
        printlncond("4.  Compute the total number of elapsed days since the standard epoch.")
        printlncond("    De = JD - JDe = " & Format(JD, JDFormat) & " - " & Format(JDe, JDFormat))
        printlncond("       = " & Format(De, genFloatFormat) & " days")
        printlncond()

        Msun = ((360.0 * De) / 365.242191) + orbElements.getOEObjLonAtEpoch(idxSun) - orbElements.getOEObjLonAtPeri(idxSun)
        printlncond("5.  Compute the Sun's Mean Anomaly.")
        printlncond("    Msun = [(360.0 * De)/365.242191] + Eg - Wg")
        printlncond("         = [(360.0 * " & De & ")/365.242191] + " & orbElements.getOEObjLonAtEpoch(idxSun) &
                    " - " & orbElements.getOEObjLonAtPeri(idxSun))
        printlncond("         = " & angleToStr(Msun, DECFORMAT) & " degrees")
        printlncond()

        printlncond("6.  Use the MOD function to adjust Msun to the range [0, 360].")
        printlncond("    Msun = Msun MOD 360 = " & angleToStr(Msun, DECFORMAT) & " MOD 360")
        Msun = xMOD(Msun, 360.0)
        printlncond("         = " & angleToStr(Msun, DECFORMAT) & " degrees")
        printlncond()

        ' Find the true anomaly from equation of center or Kepler's equation
        If radbtnEQofCenter.Checked Then
            Ec = (360.0 / PI) * Ecc * SIN_D(Msun)
            printlncond("7.  Solve the equation of the center for the Sun.")
            printlncond("    Ec = (360/pi) * eccentricity * sin(Msun) = (360/3.1415927) * " &
                        Ecc & " * sin(" & angleToStr(Msun, DECFORMAT) & ")")
            printlncond("       = " & angleToStr(Ec, DECFORMAT) & " degrees")
            printlncond()

            Vsun = Ec + Msun
            printlncond("8.  Add Ec to Msun to get the true anomaly.")
            printlncond("    Vsun = Ec + Msun = " & angleToStr(Ec, DECFORMAT) & " + " & angleToStr(Msun, DECFORMAT))
            printlncond("         = " & angleToStr(Vsun, DECFORMAT) & " degrees")
        Else
            printlncond("7.  Solve Kepler's equation to get the eccentric anomaly.")
            If radbtnSimpleIteration.Checked Then
                Ea = calcSimpleKepler(prtObj, Msun, Ecc, termCriteria, iter)
            Else
                Ea = calcNewtonKepler(prtObj, Msun, Ecc, termCriteria, iter)
            End If
            printlncond("    Ea = E" & iter & " = " & angleToStr(Ea, DECFORMAT) & " degrees")
            printlncond()

            Vsun = (1 + Ecc) / (1 - Ecc)
            Vsun = Sqrt(Vsun) * TAN_D(Ea / 2.0)
            Vsun = 2.0 * INVTAN_D(Vsun)
            printlncond("8.  Vsun = 2 * inv tan[ sqrt[(1+eccentricity)/(1-eccentricity)] * tan(Ea/2) ]")
            printlncond("         = 2 * inv tan[ sqrt[(1+" & Ecc & ")/(1-" & Ecc & ")] * tan(" &
                        angleToStr(Ea, DECFORMAT) & "/2) ]")
            printlncond("         = " & angleToStr(Vsun, DECFORMAT) & " degrees")
        End If
        printlncond()

        printlncond("9.  Use the MOD function to adjust Vsun to the range [0, 360].")
        printlncond("    Vsun = Vsun MOD 360 = " & angleToStr(Vsun, DECFORMAT) & " MOD 360")
        Vsun = xMOD(Vsun, 360.0)
        printlncond("         = " & angleToStr(Vsun, DECFORMAT) & " degrees")
        printlncond()

        Lsun = Vsun + orbElements.getOEObjLonAtPeri(idxSun)
        printlncond("10. Add Vsun and Wg to get the ecliptic longitude.")
        printlncond("    Lsun = Vsun + Wg = " & angleToStr(Vsun, DECFORMAT) & " + " &
                    angleToStr(orbElements.getOEObjLonAtPeri(idxSun), DECFORMAT))
        printlncond("         = " & angleToStr(Lsun, DECFORMAT) & " degrees")
        printlncond()

        If Lsun > 360.0 Then Lsun = Lsun - 360.0
        printlncond("11. If Lsun > 360, subtract 360.")
        printlncond("    Lsun = " & angleToStr(Lsun, DECFORMAT) & " degrees")
        printlncond("    (Ecliptic latitude is 0 degrees)")
        printlncond()

        eqCoord = EclipticToEquatorial(Bsun, Lsun, orbElements.getOEEpochDate)
        printlncond("12. Convert ecliptic coordinates to equatorial coordinates.")
        printlncond("    RA = " & timeToStr(eqCoord.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl = " &
                    angleToStr(eqCoord.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        horizonCoord = RADecltoHorizon(eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle,
                                       observer.getObsLat, LST)
        printlncond("13. Convert equatorial coordinates to horizon coordinates.")
        printlncond("    Alt = " & angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & ", Az = " &
                    angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT))
        printlncond()

        result = angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & " Alt, " &
            angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT) & " Az"
        printlncond("Thus, for this observer location and date/time, the Sun")
        printlncond("is at " & result & ".")

        lblResults.Text = "Sun's Location is " & result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------------------
    ' Calculate the times of sunrise and sunset.
    '------------------------------------------------------------
    Private Sub calcSunRiseSet()
        Dim eclCoord, eqCoord1, eqCoord2 As ASTCoord
        Dim ST1r, ST1s, ST2r, ST2s, Tr, Ts, LCTr, LCTs, GST, UT As Double
        Dim Lsun2 As Double
        Dim LSTTimes(1) As Double
        Dim riseSet As Boolean = True
        Dim Msun, Vsun As Double
        Dim dateAdjust As Integer
        Dim solveTrueAnomaly As TrueAnomalyType

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Calculate Sunrise and Sunset for the Current Observer", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If

        eclCoord = orbElements.calcSunEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                    observer.getObsDate.getYear, 0.0, Msun, Vsun, solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the Sun's ecliptic location at midnight for the date in question.")
        printlncond("    For UT=0 hours on the date " &
                    dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear))
        printlncond("    The Sun's coordinates are Bsun1 = " &
                    angleToStr(eclCoord.getLatAngle.getDecAngle, DECFORMAT) &
                    " degrees, Lsun1 = " & angleToStr(eclCoord.getLonAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        eqCoord1 = EclipticToEquatorial(eclCoord.getLatAngle.getDecAngle, eclCoord.getLonAngle.getDecAngle,
                                        orbElements.getOEEpochDate)
        printlncond("2.  Convert the Sun's ecliptic coordinates to equatorial coordinates.")
        printlncond("    RA1 = " & timeToStr(eqCoord1.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl1 = " &
                    angleToStr(eqCoord1.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        LSTTimes = calcRiseSetTimes(eqCoord1.getRAAngle.getDecTime, eqCoord1.getDeclAngle.getDecAngle, observer.getObsLat, riseSet)
        ST1r = LSTTimes(0)
        ST1s = LSTTimes(1)
        printlncond("3.  Using the equatorial coordinates from step 2, compute the")
        printlncond("    LST rising and setting times.")
        If Not riseSet Then
            printlncond("    The Sun does not rise or set for this observer.")
            Return
        End If
        printlncond("    ST1r = " & timeToStr(ST1r, DECFORMAT) & " hours")
        printlncond("    ST1s = " & timeToStr(ST1s, DECFORMAT) & " hours")
        printlncond()

        Lsun2 = eclCoord.getLonAngle.getDecAngle + 0.985647
        printlncond("4.  Calculate the Sun's ecliptic coordinates 24 hours later.")
        printlncond("    24 hours later, the Sun's coordinates are")
        printlncond("    Bsun2 = " & angleToStr(eclCoord.getLatAngle.getDecAngle, DECFORMAT) &
                    " degrees (same as Bsun1)")
        printlncond("    Lsun2 = Lsun1 + 0.985647 = " & angleToStr(eclCoord.getLonAngle.getDecAngle, DECFORMAT) &
                    " + 0.985647")
        printlncond("          = " & angleToStr(Lsun2, DECFORMAT) & " degrees")
        printlncond()

        If Lsun2 > 360.0 Then Lsun2 = Lsun2 - 360.0
        printlncond("5.  If Lsun2 > 360.0, subtract 360 degrees.")
        printlncond("    Lsun2 = " & angleToStr(Lsun2, DECFORMAT) & " degrees")
        printlncond()

        eqCoord2 = EclipticToEquatorial(eclCoord.getLatAngle.getDecAngle, Lsun2, orbElements.getOEEpochDate)
        printlncond("6.  Convert the ecliptic coordinates from the previous step to equatorial coordinates.")
        printlncond("    RA2 = " & timeToStr(eqCoord2.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl2 = " &
                    angleToStr(eqCoord2.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        LSTTimes = calcRiseSetTimes(eqCoord2.getRAAngle.getDecTime, eqCoord2.getDeclAngle.getDecAngle, observer.getObsLat, riseSet)
        ST2r = LSTTimes(0)
        ST2s = LSTTimes(1)
        printlncond("7.  Using the equatorial coordinates from step 6, compute the")
        printlncond("    second set of LST rising and setting times.")
        If Not riseSet Then
            printlncond("    The Sun does not rise or set for this observer.")
            Return
        End If
        printlncond("    ST2r = " & timeToStr(ST2r, DECFORMAT) & " hours")
        printlncond("    ST2s = " & timeToStr(ST2s, DECFORMAT) & " hours")
        printlncond()

        Tr = (24.07 * ST1r) / (24.07 + ST1r - ST2r)
        printlncond("8.  Interpolate the two sets of LST rising times.")
        printlncond("    Tr = (24.07 * ST1r) / (24.07 + ST1r - ST2r)")
        printlncond("       = (24.07*" & timeToStr(ST1r, DECFORMAT) & ")/(24.07+" & timeToStr(ST1r, DECFORMAT) &
                    "-" & timeToStr(ST2r, DECFORMAT) & ")")
        printlncond("       = " & timeToStr(Tr, DECFORMAT) & " hours")
        printlncond()

        Ts = (24.07 * ST1s) / (24.07 + ST1s - ST2s)
        printlncond("9.  Interpolate the two sets of LST setting times.")
        printlncond("    Ts = (24.07 * ST1s) / (24.07 + ST1s - ST2s)")
        printlncond("       = (24.07*" & timeToStr(ST1s, DECFORMAT) & ")/(24.07+" & timeToStr(ST1s, DECFORMAT) &
                    "-" & timeToStr(ST2s, DECFORMAT) & ")")
        printlncond("       = " & timeToStr(Ts, DECFORMAT) & " hours")
        printlncond()

        GST = LSTtoGST(Tr, observer.getObsLon)
        UT = GSTtoUT(GST, observer.getObsDate)
        LCTr = UTtoLCT(UT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        GST = LSTtoGST(Ts, observer.getObsLon)
        UT = GSTtoUT(GST, observer.getObsDate)
        LCTs = UTtoLCT(UT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        printlncond("10. Convert the LST rising/setting times to their corresponding LCT times.")
        printlncond("    LCTr = " & timeToStr(LCTr, DECFORMAT) & " hours, LCTs = " &
                    timeToStr(LCTs, DECFORMAT) & " hours")
        printlncond()

        printlncond("11. Convert the LCT times to HMS format.")
        printlncond("    LCTr = " & timeToStr(LCTr, HMSFORMAT) & ", LCTs = " & timeToStr(LCTs, HMSFORMAT))
        printlncond()

        printlncond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                            observer.getObsDate.getYear) & " for the current observer")
        printlncond("the Sun will rise at " & timeToStr(LCTr, HMSFORMAT) & " LCT and set at " &
                    timeToStr(LCTs, HMSFORMAT) & " LCT")


        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "On " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                    observer.getObsDate.getYear) & ", the Sun will rise at " &
                               timeToStr(LCTr, HMSFORMAT) & " LCT and set at " & timeToStr(LCTs, HMSFORMAT) & " LCT"
    End Sub

    '------------------------------------------------------------
    ' Set the termination criteria for solving Kepler's equation
    '------------------------------------------------------------
    Private Sub setTerminationCriteria()
        Dim dTerm As Double = termCriteria

        clearTextAreas()
        prt.setBoldFont(True)
        prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        If queryForm.showQueryForm("Enter Termination Criteria in radians" & vbNewLine &
                                   "(ex: 0.000002)") <> Windows.Forms.DialogResult.OK Then
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        ' Validate the termination criteria
        If Not isValidReal(queryForm.getData1(), dTerm, HIDE_ERRORS) Then
            ErrMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        prt.println("Prior termination criteria was: " & termCriteria & " radians")
        If dTerm < KeplerMinCriteria Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too small, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        If dTerm > 1 Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too large, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        termCriteria = dTerm

        prt.println("Termination criteria is now set to " & termCriteria & " radians")
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Orbital Elements menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------
    ' Load orbital elements from disk
    '------------------------------------------------
    Private Sub loadOrbitalElements()
        Dim fullFilename As String = " "

        clearTextAreas()

        If checkOEDBLoaded(HIDE_ERRORS) Then
            If PleaseConfirm("Orbital Elements have already been loaded. Are you" & vbNewLine &
                              "sure you want to load a new set of orbital elements?", "Clear Orbital Elements Data") Then
                setEpoch(DEFAULT_EPOCH)
                idxSun = -1
                prt.println("The currently loaded orbital elements were cleared ...")
            Else
                prt.println("The currently loaded orbital elements were not cleared ...")
                Return
            End If
        End If

        If Not getOEDBFileToOpen(fullFilename) Then Return

        If orbElements.loadOEDB(fullFilename) Then
            prt.println("New orbital elements have been successfully loaded ...")
            setEpoch(orbElements.getOEEpochDate)
            idxSun = orbElements.findOrbElementObjIndex("Sun")
        End If
        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Shows the Sun's orbital elements and other data from whatever
    ' orbital elements database is currently loaded.
    '-----------------------------------------------------------------
    Private Sub showSunsOrbitalElements()
        If Not checkOEDBLoaded() Then Return

        prt.clearTextArea()
        orbElements.displayObjOrbElements(idxSun)
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '-----------------------------------------------------------
    ' Checks to see if an orbital elements database has been
    ' successfully loaded, and display an error message if not.
    '
    ' showErrors        display error message if true
    '-----------------------------------------------------------
    Private Function checkOEDBLoaded() As Boolean
        Return checkOEDBLoaded(SHOW_ERRORS)
    End Function
    Private Function checkOEDBLoaded(ByVal showErrors As Boolean) As Boolean
        If Not orbElements.isOrbElementsDBLoaded() Then
            If showErrors Then ErrMsg("No Orbital Elements data is currently loaded.", "No Orbital Elements Data Loaded")
            Return False
        End If
        Return True
    End Function

    '--------------------------------------
    ' Clear the text areas in the GUI
    '--------------------------------------
    Private Sub clearTextAreas()
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '----------------------------------------------------
    ' Determine what time zone radio button is selected
    '
    ' Returns a time zone type for whatever is the
    ' currently selected time zone radio button
    '----------------------------------------------------
    Private Function getSelectedRBStatus() As TimeZoneType
        If radbtnPST.Checked() Then
            Return TimeZoneType.PST
        ElseIf radbtnMST.Checked() Then
            Return TimeZoneType.MST
        ElseIf radbtnCST.Checked() Then
            Return TimeZoneType.CST
        ElseIf radbtnEST.Checked() Then
            Return TimeZoneType.EST
        Else
            Return TimeZoneType.LONGITUDE
        End If
    End Function

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '-----------------------------------
    ' Sets the Epoch label in the GUI.
    '
    ' epoch         Epoch to display
    '-----------------------------------
    Private Sub setEpoch(ByVal epoch As Double)
        lblEpoch.Text = "Epoch: " & Format(epoch, epochFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the observer location in the GUI. This is intended to be
    ' done one time only during the program initialization since a
    ' default location may be read from a data file.
    '
    ' obs           observer location object
    '---------------------------------------------------------------
    Private Sub setObsDataInGUI(ByVal obs As ASTObserver)
        txtboxLat.Text = latToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxLon.Text = lonToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxDate.Text = dateToStr(obs.getObsDate())
        txtboxLCT.Text = timeToStr(obs.getObsTime(), HMSFORMAT)

        If obs.getObsTimeZone = TimeZoneType.PST Then
            radbtnPST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.MST Then
            radbtnMST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.CST Then
            radbtnCST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.EST Then
            radbtnEST.Checked = True
        Else
            radbtnLon.Checked = True
        End If

    End Sub

    '----------------------------------------------
    ' See if the observer location, date, and time
    ' currently in the GUI is valid.
    '
    ' Returns true if valid, otherwise false.
    '----------------------------------------------
    Private Function validateGUIObsLoc() As Boolean
        Return isValidObsLoc(observer, txtboxLat.Text, txtboxLon.Text, getSelectedRBStatus().ToString,
                             txtboxDate.Text, txtboxLCT.Text)
    End Function
End Class
