﻿'**********************************************************
'                       Copyright (c) 2018
'                     Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define possible menu items. These help in deciding what routine to
    ' execute when a menu item is selected
    Friend Enum CalculationType
        ' Coordinate Systems menu
        EQ_TO_HORIZ
        HORIZ_TO_EQ
        CALC_PRECESSION
        STAR_RISE_SET
        ' Star Catalogs menu
        LOAD_CATALOG
        SHOW_CATALOG_INFO
        LIST_ALL_OBJS_IN_CAT
        LIST_ALL_CONST
        LIST_ALL_OBJS_IN_CONST
        FIND_CONST_OBJ_IN
        ' Star Charts menu
        SET_mV_FILTER
        DRAW_ALL_STARS
        DRAW_ALL_STARS_IN_CONST
        DRAW_ALL_CONST
        FIND_OBJ
        FIND_BRIGHTEST_OBJ
        FIND_EQ_LOC
        FIND_HORIZ_LOC
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub

End Class
