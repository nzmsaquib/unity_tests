﻿'**********************************************************
'               Chapter 5 - Stars in the Nighttime Sky
'                       Copyright (c) 2018
'                     Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' for a program that will plot star charts from a
' star catalog. The main GUI was mostly built
' with the Visual Basic form editor.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTCatalog
Imports ASTUtils.ASTCharts
Imports ASTUtils.ASTConstellation
Imports ASTUtils.ASTCoord
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTObserver
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap5.ChapMenuItem

Public Class Chap5GUI

    Private Const AboutBoxText = "Chapter 5 - Stars in the Nighttime Sky"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt

    Private constellations As ASTConstellation
    Private catalog As ASTCatalog
    Private observer As ASTObserver = Nothing

    ' Create a reusable query form for user input
    Private queryForm As ASTQuery = New ASTQuery()

    Private Const DEFAULT_mV_FILTER = mV_NAKED_EYE
    Private currentmVFilter As Double                   ' whatever the current mV filter in the GUI is

    ' Create a drawing canvas for the star charts
    Private starChart As ASTCharts = Nothing

    ' Color to use when highlighting an object, brightest object, and located object.
    ' Also define large mV values to use to display brightest and located objects.
    Private Shared HIGHLIGHT_COLOR As Brush = Brushes.Red
    Private Shared BRIGHTEST_OBJ_COLOR As Brush = Brushes.Blue
    Private Shared LOCATE_OBJ_COLOR As Brush = Brushes.Green
    Private Const BRIGHT_mV As Double = -8.0
    Private Const LOCATE_mV As Double = -12.0

    ' Flag indicating whether there is currently a star chart on the screen. This is
    ' used so that various stars (in a constellation, brightest, etc.) can be highlighted
    ' on an existing chart.
    Private chartOnScreen As Boolean = False

    ' Maximum length of a filename to display. Actual filename can be longer, but only a limited
    ' number of characters can be displayed in the GUI
    Private Const MAX_FNAME_DISPLAY = 70

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearTextAreas()
        ' Must create the starChart **after** InitializeComponent has been done
        starChart = New ASTCharts(txtboxOutputArea)

        ' Create the menu items
        createChapMenus()

        ' Now initialize the rest of the GUI
        chkboxShowInterimCalcs.Checked = False
        chkboxEQChart.Checked = True
        chkboxDST.Checked = False
        chkboxWhiteCharts.Checked = True
        chkboxLabelStars.Checked = True
        radbtnLon.Checked = True
        setFilename("")
        setCatalogType("")
        setEpoch(DEFAULT_EPOCH)
        setmVFilter(DEFAULT_mV_FILTER)
        chartOnScreen = False

        constellations = New ASTConstellation(prt)
        catalog = New ASTCatalog(prt)
        observer = New ASTObserver
        setObsDataInGUI(observer)

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------------------------------------
    ' Create the Coord Sys, Star Catalogs, and Star Charts menus.
    '---------------------------------------------------------------------------------------------

    '------------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler.
    '
    ' title         title for the menu item
    ' CalcType      what type of calculation to perform
    '               when this menu item is clicked
    '------------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '-------------------------------------------------
    ' Create the Coord Sys, Star Catalogs, and
    ' Star Charts menu subitems
    '-------------------------------------------------
    Private Sub createChapMenus()
        Dim tmpSubMenu As ToolStripMenuItem

        ' Create Coordinate Systems menu items
        mnuCoordSys.DropDownItems.Add(createMenuItem("Equatorial to Horizon", CalculationType.EQ_TO_HORIZ))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Horizon to Equatorial", CalculationType.HORIZ_TO_EQ))
        mnuCoordSys.DropDownItems.Add("-")
        mnuCoordSys.DropDownItems.Add(createMenuItem("Precession Correction", CalculationType.CALC_PRECESSION))
        mnuCoordSys.DropDownItems.Add("-")
        mnuCoordSys.DropDownItems.Add(createMenuItem("Star Rise/Set Times", CalculationType.STAR_RISE_SET))

        ' Create Star Catalogs menu
        mnuCatalogs.DropDownItems.Add(createMenuItem("Load a Star Catalog", CalculationType.LOAD_CATALOG))
        mnuCatalogs.DropDownItems.Add(createMenuItem("Show Catalog Information", CalculationType.SHOW_CATALOG_INFO))
        mnuCatalogs.DropDownItems.Add("-")
        mnuCatalogs.DropDownItems.Add(createMenuItem("List all Objects in the Catalog", CalculationType.LIST_ALL_OBJS_IN_CAT))
        mnuCatalogs.DropDownItems.Add("-")
        mnuCatalogs.DropDownItems.Add(createMenuItem("List all Constellations", CalculationType.LIST_ALL_CONST))
        mnuCatalogs.DropDownItems.Add(createMenuItem("List all Objects in a Constellation", CalculationType.LIST_ALL_OBJS_IN_CONST))
        mnuCatalogs.DropDownItems.Add(createMenuItem("Find Constellation an Object is In", CalculationType.FIND_CONST_OBJ_IN))

        ' Create Star Charts menu
        mnuCharts.DropDownItems.Add(createMenuItem("Set mV Filter", CalculationType.SET_mV_FILTER))
        mnuCharts.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Draw ..."
        mnuCharts.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("All Stars in the Catalog", CalculationType.DRAW_ALL_STARS))
        tmpSubMenu.DropDownItems.Add(createMenuItem("All Stars in a Constellation", CalculationType.DRAW_ALL_STARS_IN_CONST))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Brightest Star in each Constellation", CalculationType.DRAW_ALL_CONST))
        mnuCharts.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Locate ..."
        mnuCharts.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("An Object in the Catalog", CalculationType.FIND_OBJ))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Brightest Object in the Catalog", CalculationType.FIND_BRIGHTEST_OBJ))
        tmpSubMenu.DropDownItems.Add("-")
        tmpSubMenu.DropDownItems.Add(createMenuItem("An Equatorial Coordinate", CalculationType.FIND_EQ_LOC))
        tmpSubMenu.DropDownItems.Add(createMenuItem("A Horizon Coordinate", CalculationType.FIND_HORIZ_LOC))
    End Sub

    '------------------------------------------------------------------
    ' The following methods listen for changes, such as a checkbox
    ' update or a screen resize event, that may invalidate any
    ' chart that is currently on the screen. This is necessary because
    ' we want to keep track of whether a chart is already on the screen
    ' so that we can highlight specific objects on that chart. Changing
    ' the type of chart to display affects how the plotting is done, so
    ' we have to invalidate and assume that no chart is on the screen.
    '------------------------------------------------------------------
    Private Sub chkboxEQChart_click(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles chkboxEQChart.Click, chkboxWhiteCharts.Click
        chartOnScreen = False
    End Sub
    Private Sub Chap5GUI_resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        chartOnScreen = False
    End Sub

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '----------------------------------------
    ' Handle clicks on the Coord Sys, Star
    ' Catalogs, and Star Charts menu items
    '----------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        Select Case calctype
            '************* Coord Systems menu
            Case CalculationType.EQ_TO_HORIZ
                calcEQ2Horiz()
            Case CalculationType.HORIZ_TO_EQ
                calcHoriz2EQ()
            Case CalculationType.CALC_PRECESSION
                calcPrecession()
            Case CalculationType.STAR_RISE_SET
                calcStarRiseSet()

                '************* Star Catalogs menu
            Case CalculationType.LOAD_CATALOG
                loadCatalog()
            Case CalculationType.SHOW_CATALOG_INFO
                showCatalogInfo()
            Case CalculationType.LIST_ALL_OBJS_IN_CAT
                listAllObjsInCatalog()
            Case CalculationType.LIST_ALL_CONST
                listAllConstellations()
            Case CalculationType.LIST_ALL_OBJS_IN_CONST
                listAllObjsInConstellation()
            Case CalculationType.FIND_CONST_OBJ_IN
                findConstellationForRA_Decl()

                '************* Star Charts menu
            Case CalculationType.SET_mV_FILTER
                changemVFilter()
            Case CalculationType.DRAW_ALL_STARS
                drawAllStars()
            Case CalculationType.DRAW_ALL_STARS_IN_CONST
                drawAllStarsInConst()
            Case CalculationType.DRAW_ALL_CONST
                drawBrightestStarInAllConst()
            Case CalculationType.FIND_OBJ
                locateStarCatalogObj()
            Case CalculationType.FIND_BRIGHTEST_OBJ
                locateBrightestObjInCat()
            Case CalculationType.FIND_EQ_LOC
                locateRADecl()
            Case CalculationType.FIND_HORIZ_LOC
                locateAltAz()
        End Select
    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " &
                    "be used in subsequent calculations. (Note that the observer location and time are irrelevant for creating " &
                    "star charts of equatorial coordinates because equatorial coordinates are independent of an " &
                    "observer's location.) You may find it convenient to enter a default latitude, longitude, and time zone " &
                    "in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " &
                    "longitude, and time zone are already filled in with default values when the program starts. By default, " &
                    "the date and time will be set to the local date and time at which the program is started.")
        prt.println()
        prt.print("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " &
                    "various calculations are performed. This checkbox only applies to the 'Coord Systems' menu. Select the " &
                    "item you want from the 'Coord Systems' menu, enter the required data, and the results will be displayed " &
                    "immediately underneath the 'File' label and, if intermediate results are desired, in the scrollable text " &
                    "output area (i.e., where these instructions are being displayed). Note that the 'Coord Systems' menu " &
                    "provides an entry to adjust an equatorial coordinate for precession. This entry makes a precession " &
                    "correction for ")
        prt.setBoldFont(True)
        prt.print("**only**")
        prt.setBoldFont(False)
        prt.println(" the RA/Decl entered. It does not apply a precession correction to the coordinates in " &
                    "a star catalog or to the coordinates when a star chart is plotted.")
        prt.println()
        prt.println("The 'Star Catalogs' menu allows you to load a star catalog as was done in the program for " &
                    "Chapter 1. You may list the objects in the most recently loaded catalog as well as view basic information " &
                    "about the catalog and the constellations. The catalog objects that will be listed are limited to those " &
                    "objects that are at least as bright as the magnitude indicated by the 'mV Filter' data field near " &
                    "the top right of the application window. For example, if the mV filter is set to 6.5, then " &
                    "objects dimmer than magnitude 6.5 will be excluded from a catalog listing. Similarly, when " &
                    "plotting catalog objects, those objects that are dimmer than the mV filter will be " &
                    "excluded from the star chart that gets created. If a star catalog does not provide the visual " &
                    "magnitude for objects in the catalog, the mV filter will be ignored.")
        prt.println()
        prt.println("The first menu entry under the 'Star Charts' menu allows the mV filter to be set. The remaining items in " &
                    "this menu allow star charts to be created and displayed. The 'Equatorial Coords Charts' checkbox determines " &
                    "what type of star chart to create. If checked, the star chart will be a rectangular plot of right " &
                    "ascension and declination (i.e., equatorial coordinates) while if the checkbox " &
                    "is not checked, a circular plot of horizon coordinates will be generated. Resizing the application window will " &
                    "destroy any star chart that has been created. If this happens, after resizing the window " &
                    "to the size you want, simply go through the menu items to redraw the star chart that you want. It is " &
                    "typically better to resize the application window to the size you want before generating a star chart.")
        prt.println()
        prt.println("The 'White Bkg for Charts' checkbox allows you to produce charts with a white background (checkbox is checked) " &
                    "or a black background (checkbox is not checked). The remaining checkbox, ('Label Stars') indicates whether " &
                    "the name of an object should be displayed on the star charts. The menu items under 'Star Charts' provide the " &
                    "ability to plot all of the objects in the currently loaded star catalog, only those in a particular " &
                    "constellation, or just the brightest star in a constellation. These are provided as aids to help you find " &
                    "a constellatiion or to locate an object in a constellation.")
        prt.println()
        prt.println("The 'StarCharts->Locate ...->An Equatorial Coordinate' is particularly useful for finding an object on a " &
                    "star chart, even if the currently loaded star catalog does not contain the object. For example, if you know " &
                    "the equatorial coordinates for the planet Saturn (perhaps from an astronomy magazine), you can first draw a " &
                    "star chart from the currently loaded catalog, then select 'StarCharts->Locate ...->An Equatorial Coordinate' " &
                    "to enter Saturn's equatorial coordinates and see where the planet is in relation to the other objects in the " &
                    "star chart.")
        prt.println()
        prt.println("Two cautions are worth noting regarding displaying the names of stars on a star chart. First," &
                    "the star's name will appear below where the star is plotted. If a star is near one of the chart's sides," &
                    "the label may be only partially displayed. Second, you should plot star labels sparingly because plotting " &
                    "too many labels will clutter the star chart. For example, check the 'Label Stars' checkbox and then select the " &
                    "'Star Charts->Draw ...->Brightest Star in Each Constellation' menu entry to see how cluttered a chart can become!")
        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Coord System menu items
    '----------------------------------------------------------------------------------------------------

    '-------------------------------------------------------
    ' Convert equatorial coordinates to horizon coordinates
    '-------------------------------------------------------
    Private Sub calcEQ2Horiz()
        Dim result As String
        Dim raObj As ASTTime = New ASTTime
        Dim declObj As ASTAngle = New ASTAngle
        Dim adjustedDate As ASTDate
        Dim LCT, UT, GST, LST, HA As Double
        Dim horizCoord As ASTCoord = New ASTCoord
        Dim dateAdjust As Integer

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", "Enter Declination (xxxd yym zz.zzs)") =
                                 Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If
        Else
            Return
        End If

        clearTextAreas()

        result = timeToStr(raObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) & " Decl"

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to Horizon Coordinates", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        LCT = observer.getObsTime().getDecTime()
        printlncond("1.  Convert LCT to decimal format. LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) & " hours")
        printlncond()

        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        printcond("2.  Convert LCT to UT. UT = " & timeToStr(UT, DECFORMAT) & " hours")

        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        If dateAdjust < 0 Then
            printlncond(" (previous day " & dateToStr(adjustedDate) & ")")
        ElseIf dateAdjust > 0 Then
            printlncond(" (next day " & dateToStr(adjustedDate) & ")")
        Else
            printlncond()
        End If
        printlncond()

        GST = UTtoGST(UT, adjustedDate)
        printlncond("3.  Convert UT to GST. GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond()

        LST = GSTtoLST(GST, observer.getObsLon)
        printlncond("4.  Convert GST to LST. LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        HA = RAtoHA(raObj.getDecTime(), LST)
        printlncond("5.  Convert Right Ascension to Hour Angle. H = " & timeToStr(HA, DECFORMAT) & " hours")
        printlncond()

        horizCoord = HADecltoHorizon(HA, declObj.getDecAngle, observer.getObsLat)
        printlncond("6.  Convert Hour Angle, Declination to horizon coordinates.")
        printlncond("    Altitude = " & angleToStr(horizCoord.getAltAngle, DECFORMAT) & " degrees")
        printlncond("    Azimuth = " & angleToStr(horizCoord.getAzAngle, DECFORMAT) & " degrees")
        printlncond()

        printlncond("7.  Convert horizon coordinates to DMS format.")
        printlncond("    Altitude = " & angleToStr(horizCoord.getAltAngle, DMSFORMAT))
        printlncond("    Azimuth = " & angleToStr(horizCoord.getAzAngle, DMSFORMAT))
        printlncond()

        result = result & " = " & angleToStr(horizCoord.getAltAngle, DMSFORMAT) & " Alt, " &
            angleToStr(horizCoord.getAzAngle, DMSFORMAT) & " Az"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------
    ' Convert horizon coordinates to equatorial coordinates
    '-------------------------------------------------------
    Private Sub calcHoriz2EQ()
        Dim result As String
        Dim altObj As ASTAngle = New ASTAngle
        Dim azObj As ASTAngle = New ASTAngle
        Dim adjustedDate As ASTDate
        Dim LCT, UT, GST, LST, JD, RA As Double
        Dim eqCoord As ASTCoord = New ASTCoord
        Dim dateAdjust As Integer

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter Altitude (xxxd yym zz.zzs)", "Enter Azimuth (xxxd yym zz.zzs)") =
                                 Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidAngle(queryForm.getData1(), altObj, HIDE_ERRORS) Then
                ErrMsg("The Altitude entered is invalid - try again.", "Invalid Altitude")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), azObj, HIDE_ERRORS) Then
                ErrMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth")
                Return
            End If
        Else
            Return
        End If

        clearTextAreas()

        result = angleToStr(altObj, DMSFORMAT) & " Altitude, " & angleToStr(azObj, DMSFORMAT) & " Azimuth"

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to Horizon Coordinates", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        LCT = observer.getObsTime().getDecTime()
        printlncond("1.  Convert LCT to decimal format. LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) & " hours")
        printlncond()

        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        printcond("2.  Convert LCT to UT. UT = " & timeToStr(UT, DECFORMAT) & " hours")
        ' adjust the date, if needed, by converting date to JD, adding adjustment, and converting back to a date
        adjustedDate = observer.getObsDate
        If dateAdjust <> 0 Then
            JD = dateToJD(adjustedDate) + dateAdjust
            adjustedDate = JDtoDate(JD)
        End If
        If dateAdjust < 0 Then
            printlncond(" (previous day " & dateToStr(adjustedDate) & ")")
        ElseIf dateAdjust > 0 Then
            printlncond(" (next day " & dateToStr(adjustedDate) & ")")
        Else
            printlncond()
        End If
        printlncond()

        GST = UTtoGST(UT, adjustedDate)
        printlncond("3.  Convert UT to GST. GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond()

        LST = GSTtoLST(GST, observer.getObsLon)
        printlncond("4.  Convert GST to LST. LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        eqCoord = HorizontoHADecl(altObj.getDecAngle, azObj.getDecAngle, observer.getObsLat)
        printlncond("5.  Convert horizon coordinates to Hour Angle and Declination.")
        printlncond("    Hour Angle = " & timeToStr(eqCoord.getHAAngle(), DECFORMAT) & " hours")
        printlncond("    Decl = " & angleToStr(eqCoord.getDeclAngle, DECFORMAT) & " degrees")
        printlncond()

        RA = HAtoRA(eqCoord.getHAAngle.getDecTime, LST)
        printlncond("6.  Convert Hour Angle to Right Ascension. RA = " & timeToStr(RA, DECFORMAT) & " hours")
        printlncond()

        printlncond("7.  Convert equatorial coordinates to HMS & DMS format.")
        printlncond("    RA = " & timeToStr(RA, HMSFORMAT))
        printlncond("    Decl = " & angleToStr(eqCoord.getDeclAngle, DMSFORMAT))
        printlncond()

        result = result & " = " & timeToStr(RA, HMSFORMAT) & " RA, " &
            angleToStr(eqCoord.getDeclAngle, DMSFORMAT) & " Decl"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------------------------
    ' Precess equatorial coordinates from one epoch to another
    '---------------------------------------------------------
    Private Sub calcPrecession()
        Dim result As String
        Dim raObj As ASTTime = New ASTTime
        Dim declObj As ASTAngle = New ASTAngle
        Dim EpochFrom, EpochTo As Double
        Dim newCoord As ASTCoord = New ASTCoord

        ' Get the equatorial coordinates to convert
        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", "Enter Declination (xxxd yym zz.zzs)") =
                                  Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If
        Else
            Return
        End If

        ' Get the epochs to convert to and from
        If queryForm.showQueryForm("Epoch to Convert From (ex: 1950.0)", "Epoch to Convert To (ex: 2000.0)") =
                                  Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidReal(queryForm.getData1(), EpochFrom, HIDE_ERRORS) Then
                ErrMsg("The Epoch to convert from is invalid - try again.", "Invalid Epoch")
                Return
            End If
            If Not isValidReal(queryForm.getData2(), EpochTo, HIDE_ERRORS) Then
                ErrMsg("The Epoch to convert to is invalid - try again.", "Invalid Epoch ")
                Return
            End If
        Else
            Return
        End If

        clearTextAreas()

        result = timeToStr(raObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) & " Decl"

        prt.setBoldFont(True)
        printlncond("Precess " & result & " from Epoch " & EpochFrom & " to Epoch " & EpochTo, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        newCoord = PrecessEqCoord(raObj.getDecTime, declObj.getDecAngle, EpochFrom, EpochTo)

        printlncond("Thus, " & result & " (" & EpochFrom & ") =")
        printlncond("      " & timeToStr(newCoord.getRAAngle.getDecTime, HMSFORMAT) & " RA, " &
            angleToStr(newCoord.getDeclAngle.getDecAngle, DMSFORMAT) & " Decl (" & EpochTo & ")")

        result = result & "(" & EpochFrom & ") = " & timeToStr(newCoord.getRAAngle.getDecTime, HMSFORMAT) & " RA, " &
            angleToStr(newCoord.getDeclAngle.getDecAngle, DMSFORMAT) & " Decl (" & EpochTo & ")"

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()

    End Sub

    '-------------------------------------------------------
    ' Convert rising and setting times for a given eq coord.
    '-------------------------------------------------------
    Private Sub calcStarRiseSet()
        Dim result As String
        Dim riseSet As Boolean = True
        Dim raObj As ASTTime = New ASTTime
        Dim declObj As ASTAngle = New ASTAngle
        Dim Ar, R, S, H1, H2 As Double
        Dim LSTr, LSTs, LCTr, LCTs, tmp As Double
        Dim dateAdjust As Integer

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter Object Right Ascension (hh:mm:ss.ss)", "Enter Object Declination (xxxd yym zz.zzs)") =
                                 Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If
        Else
            Return
        End If

        clearTextAreas()

        result = timeToStr(raObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) & " Decl"

        prt.setBoldFont(True)
        printlncond("Find when location " & result & " will rise and set", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Convert RA to decimal format. RA = " & timeToStr(raObj, DECFORMAT) & " hours")
        printlncond()

        printlncond("2.  Convert Decl to decimal format. Decl = " & angleToStr(declObj, DECFORMAT) & " degrees")
        printlncond()

        printlncond("3.  Convert observer's latitude to decimal format. Lat = " &
                    angleToStr(observer.getObsLat, DECFORMAT) & " degrees")
        printlncond()

        Ar = SIN_D(declObj.getDecAngle) / COS_D(observer.getObsLat)
        printlncond("4.  Compute Ar = sin(Decl)/cos(lat) = sin(" & angleToStr(declObj, DECFORMAT) & ")/cos(" &
                    angleToStr(observer.getObsLat, DECFORMAT) & ")")
        printlncond("               = " & Format(Ar, genFloatFormat))
        printlncond()

        printlncond("5.  If ABS(Ar) > 1, the location doesn't rise or set.")
        If Abs(Ar) > 1 Then
            printlncond("    Location does not rise or set. No need to go further")
            riseSet = False
        Else
            printlncond("    Location may rise or set, so continue.")
        End If
        printlncond()

        If riseSet Then
            R = INVCOS_D(Ar)
            printlncond("6.  Compute R = inv cos(Ar) = inv cos(" & Format(Ar, genFloatFormat) & ")")
            printlncond("              = " & Format(R, genFloatFormat) & " degrees")
            printlncond()

            S = 360.0 - R
            printlncond("7.  Compute S = 360 - R = 360 - " & Format(R, genFloatFormat))
            printlncond("              = " & Format(S, genFloatFormat) & " degrees")
            printlncond()

            H1 = TAN_D(observer.getObsLat) * TAN_D(declObj.getDecAngle)
            printlncond("8.  Compute H1 = tan(Lat)*tan(Decl) = tan(" & angleToStr(observer.getObsLat, DECFORMAT) &
                        ")*tan(" & angleToStr(declObj, DECFORMAT) & ")")
            printlncond("               = " & Format(H1, genFloatFormat))
            printlncond()

            printlncond("9.  If ABS(H1) > 1, the location doesn't rise or set.")
            If Abs(H1) > 1 Then
                printlncond("    Location does not rise or set. No need to go further")
                riseSet = False
            Else
                printlncond("    Location will rise and set, so continue.")
            End If
            printlncond()
        End If

        If riseSet Then
            H2 = INVCOS_D(-H1) / 15.0
            printlncond("10. Calculate H2 = inv cos(-H1)/15 = inv cos[-(" & Format(H1, genFloatFormat) & ")]/15")
            printlncond("                 = " & Format(H2, genFloatFormat) & " hours")
            printlncond()

            LSTr = 24 + raObj.getDecTime - H2
            printlncond("11. Let LSTr = 24 + RA - H2 = 24 + " & timeToStr(raObj, DECFORMAT) & " - " &
                        Format(H2, genFloatFormat))
            printlncond("             = " & timeToStr(LSTr, DECFORMAT) & " hours")
            printlncond("    This is the LST for when the location rises above the observer's horizon.")
            printlncond()

            If LSTr > 24 Then LSTr = LSTr - 24.0
            printlncond("12. If LSTr > 24, subtract 24. LSTr = " & timeToStr(LSTr, DECFORMAT) & " hours")
            printlncond()

            LSTs = raObj.getDecTime + H2
            printlncond("13. Let LSTs = RA + H2 = " & timeToStr(raObj, DECFORMAT) & " + " &
                        Format(H2, genFloatFormat))
            printlncond("             = " & timeToStr(LSTs, DECFORMAT) & " hours")
            printlncond("    This is the LST for when the location sets below the observer's horizon.")
            printlncond()

            If LSTs > 24 Then LSTs = LSTs - 24.0
            printlncond("14. If LSTs > 24, subtract 24. LSTs = " & timeToStr(LSTs, DECFORMAT) & " hours")
            printlncond()

            printlncond("15. Convert LSTr and LSTs to LCTr and LCTs. These are the LCT rise and set times.")
            tmp = LSTtoGST(LSTr, observer.getObsLon)
            printcond("    GSTr = " & timeToStr(tmp, DECFORMAT) & ", ")
            tmp = GSTtoUT(tmp, observer.getObsDate)
            printcond("UTr = " & timeToStr(tmp, DECFORMAT) & ", ")
            LCTr = UTtoLCT(tmp, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
            printlncond("LCTr = " & timeToStr(LCTr, DECFORMAT) & " hours")

            tmp = LSTtoGST(LSTs, observer.getObsLon)
            printcond("    GSTs = " & timeToStr(tmp, DECFORMAT) & ", ")
            tmp = GSTtoUT(tmp, observer.getObsDate)
            printcond("UTs = " & timeToStr(tmp, DECFORMAT) & ", ")
            LCTs = UTtoLCT(tmp, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
            printlncond("LCTs = " & timeToStr(LCTs, DECFORMAT) & " hours")
            printlncond()

            printlncond("16. Convert LCTr and LCTs to HMS format.")
            printcond("    LCTr = " & timeToStr(LCTr, HMSFORMAT) & ", LCTs = " & timeToStr(LCTs, HMSFORMAT))
            printlncond()
        End If

        result = "Location " & result
        If riseSet Then
            result = result & " Rises at " & timeToStr(LCTr, HMSFORMAT) & " LCT, Sets at " &
                timeToStr(LCTs, HMSFORMAT) & " LCT"
        Else
            result = result & " does not rise or set"
        End If

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Star Catalog menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Finds the constellation that a given RA/Decl falls within
    '------------------------------------------------------------
    Private Sub findConstellationForRA_Decl()
        Dim raObj As ASTTime = New ASTTime
        Dim declObj As ASTAngle = New ASTAngle
        Dim idx As Integer
        Dim result As String

        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)" & vbNewLine & "for Epoch 2000.0",
                                   "Enter Declination (xxxd yym zz.zzs)" & vbNewLine & "for Epoch 2000.0") =
                                    Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If

            clearTextAreas()
            idx = findConstellationFromCoord(raObj.getDecTime(), declObj.getDecAngle(), DEFAULT_EPOCH)

            If idx < 0 Then
                CriticalErrMsg("Could not determine a constellation for the location entered.")
            Else
                result = "Location " & timeToStr(raObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) &
                    " Decl is in the " & getConstName(idx) & " (" & getConstAbbrevName(idx) & ") constellation"
                lblResults.Text = result
            End If
        End If
    End Sub

    '----------------------------------------------
    ' Displays a list of all of the constellations.
    '----------------------------------------------
    Private Sub listAllConstellations()
        clearTextAreas()
        prt.setFixedWidthFont()
        displayAllConstellations()
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------------------------
    ' Shows all catalog information, including space objects,
    ' in the currently loaded catalog.
    '---------------------------------------------------------
    Private Sub listAllObjsInCatalog()
        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        clearTextAreas()
        displayCatalogInfo()
        prt.println()
        prt.setBoldFont(True)
        prt.println("Only Those Objects at Least as Bright as mV = " & Format(currentmVFilter, mVFormat) &
                    " are listed", CENTERTXT)
        prt.setBoldFont(False)
        prt.setFixedWidthFont()
        prt.println(String.Format("{0,82:s}", "*").Replace(" ", "*"))
        displayAllCatalogObjects(currentmVFilter)
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------------------------
    ' Shows all space objects in the currently loaded catalog
    ' that fall within a user-specified constellation. The
    ' catalog must already be grouped by constellation.
    '---------------------------------------------------------
    Private Sub listAllObjsInConstellation()
        Dim idx As Integer
        Dim constAbbrevName As String

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter Constellation's 3 Character" & vbNewLine &
                           "Abbreviated Name") = Windows.Forms.DialogResult.OK Then
            clearTextAreas()
            constAbbrevName = queryForm.getData1()
            idx = findConstellationByAbbrvName(constAbbrevName.Trim())
            If idx < 0 Then
                prt.println("No Constellation whose abbreviated name is '" & constAbbrevName & "' was found")
            Else
                prt.setBoldFont(True)
                prt.println("Only Those Objects at Least as Bright as mV = " & Format(currentmVFilter, mVFormat) &
                            " are listed", CENTERTXT)
                prt.setBoldFont(False)
                prt.setFixedWidthFont()
                displayAllObjsByConstellation(idx, ASCENDING_ORDER, currentmVFilter)
                prt.setProportionalFont()
            End If
            prt.resetCursor()
        End If

    End Sub

    '------------------------------------------------
    ' Loads a star catalog from disk
    '------------------------------------------------
    Private Sub loadCatalog()
        Dim fullFilename As String = " "

        clearTextAreas()

        If isCatalogLoaded() Then
            If PleaseConfirm("A catalog is already loaded. Are you" & vbNewLine &
                              "sure you want to load a new one?", "Clear Catalog Data") Then
                clearCatalogAndSpaceObjects()
                setFilename("")
                setCatalogType("")
                setEpoch(DEFAULT_EPOCH)
                prt.println("The currently loaded star catalog was cleared ...")
            Else
                prt.println("The currently loaded star catalog was not cleared ...")
                Return
            End If
        End If

        If Not getCatFileToOpen(fullFilename) Then Return

        setFilename(fullFilename)
        clearTextAreas()
        If loadFormattedStarCatalog(fullFilename) Then
            prt.println("Read in " & getCatNumConst() & " different Constellations with a total of " &
            getCatNumObjs() & " Objects")
            setCatalogType(getCatType())
            setEpoch(getCatEpoch())
        Else
            ErrMsg("Could not load the catalog data from " & fullFilename, "Catalog Load Failed")
            setFilename("")
            setCatalogType("")
            setEpoch(DEFAULT_EPOCH)
        End If
        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Shows the catalog information from the currently loaded catalog.
    '-----------------------------------------------------------------
    Private Sub showCatalogInfo()
        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
        Else
            clearTextAreas()
            displayCatalogInfo()
            prt.resetCursor()
        End If
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Star Charts menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------
    ' Ask user for a filter and update the GUI
    '------------------------------------------
    Private Sub changemVFilter()
        Dim newmVFilter As Double

        clearTextAreas()

        If queryForm.showQueryForm("Enter desired visual magnitude filter") = Windows.Forms.DialogResult.OK Then
            ' validate data
            If isValidReal(queryForm.getData1(), newmVFilter, HIDE_ERRORS) Then
                setmVFilter(newmVFilter)
            Else
                ErrMsg("The mV Filter entered is invalid - try again.", "Invalid mV Filter")
            End If
        End If
    End Sub

    '--------------------------------------------------
    ' Draw all of the stars in the catalog that are at
    ' least as bright as the mV filter.
    '--------------------------------------------------
    Private Sub drawAllStars()
        Dim i, iConst, plotLimit, plotCount As Integer
        Dim RA, Decl, mV As Double
        Dim LST As Double
        Dim showObj As Boolean
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        chartOnScreen = True

        LST = computeLST()

        starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)

        ' See if we need to bother about pausing during plotting
        plotLimit = OBJS_LIMIT + 10          ' this really means to ignore pausing during display
        If getCatNumObjs() > OBJS_LIMIT Then
            If AbortMsg("Plotting " & getCatNumObjs() & " objects may take a long time.") Then Return

            If queryForm.showQueryForm("Enter Max # of Objs to Plot at a Time" & vbNewLine &
                             "(Recommend no More Than " & MAX_OBJS_TO_PLOT & " Objs)") = Windows.Forms.DialogResult.OK Then
                If Not isValidInt(queryForm.getData1(), plotLimit, HIDE_ERRORS) Then plotLimit = MAX_OBJS_TO_PLOT
            End If
        End If

        iConst = -1         ' set a bogus constellation
        plotCount = 0
        For i = 0 To getCatNumObjs() - 1

            plotCount = plotCount + 1
            If plotCount > plotLimit Then
                If AbortMsg("Plotted " & i & " of " & getCatNumObjs() & " objects") Then Exit For
                plotCount = 0
            End If

            RA = getCatObjRA(i)
            Decl = getCatObjDecl(i)
            If getCatObjConstIdx(i) <> iConst Then
                iConst = getCatObjConstIdx(i)
                lblResults.Text = "Plotting constellation " & getConstName(iConst)
                lblResults.Update()     ' force to update immediately so that the label is seen
            End If

            ' See if this object should be filtered out
            If getCatObjmVKnown(i) Then
                mV = getCatObjmV(i)
                showObj = (mV <= currentmVFilter)
            Else
                showObj = True      ' show object even if we don't know its mV value
                mV = mV_NAKED_EYE   ' default to what the naked eye can see
            End If

            If showObj Then
                If chkboxEQChart.Checked Then
                    starChart.plotRADecl(RA, Decl, mV)
                Else
                    horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                    starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                        horizonCoord.getAzAngle.getDecAngle, mV)
                End If
            End If
        Next

        lblResults.Text = " ... all done plotting ..."
    End Sub

    '--------------------------------------------------------------
    ' Draw all of the stars in the catalog within a user specified
    ' constellation that are at least as bright as the mV filter.
    ' Also draw the brightest star in the constellation.
    '--------------------------------------------------------------
    Private Sub drawAllStarsInConst()
        Dim i, iConst As Integer
        Dim RA, brightRA, Decl, brightDecl, mV As Double
        Dim LST As Double
        Dim showObj As Boolean
        Dim constAbbrevName As String
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        If queryForm.showQueryForm("Enter Constellation's 3 Character" & vbNewLine &
                                   "Abbreviated Name") = Windows.Forms.DialogResult.OK Then
            constAbbrevName = queryForm.getData1()
            iConst = findConstellationByAbbrvName(constAbbrevName.Trim())
            If iConst < 0 Then
                ErrMsg("No Constellation whose abbreviated name is '" & constAbbrevName & "' was found",
                       "Invalid Constellation")
                Return
            End If
        Else
            Return
        End If

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        lblResults.Text = "Plotting constellation " & getConstName(iConst)
        lblResults.Update()     ' force to update immediately so that the label is seen

        brightRA = getConstBrightestStarRA(iConst)
        brightDecl = getConstBrightestStarDecl(iConst)

        ' Cycle through the catalog and get only those objects in the specified constellation
        For i = 0 To getCatNumObjs() - 1
            RA = getCatObjRA(i)
            Decl = getCatObjDecl(i)
            If getCatObjConstIdx(i) = iConst Then
                ' See if this object should be filtered out
                If getCatObjmVKnown(i) Then
                    mV = getCatObjmV(i)
                    showObj = (mV <= currentmVFilter)
                Else
                    showObj = True      ' show object even if we don't know its mV value
                    mV = mV_NAKED_EYE   ' default to what the naked eye can see
                End If

                If showObj Then
                    If chkboxEQChart.Checked Then
                        starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
                    Else
                        horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                        starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                            horizonCoord.getAzAngle.getDecAngle, mV, HIGHLIGHT_COLOR)
                    End If
                End If
            End If
        Next

        ' Now draw the brightest star in the constellation
        If chkboxEQChart.Checked Then
            starChart.plotRADecl(brightRA, brightDecl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
            If chkboxLabelStars.Checked Then
                starChart.drawLabelRADecl(getConstBrightestStarName(iConst), brightRA, brightDecl, BRIGHTEST_OBJ_COLOR)
            End If
        Else
            ' plotAltAz and drawLabelAltAz ignore if coord is below the horizon
            horizonCoord = RADecltoHorizon(brightRA, brightDecl, observer.getObsLat, LST)
            starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                            horizonCoord.getAzAngle.getDecAngle, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
            If chkboxLabelStars.Checked Then
                starChart.drawLabelAltAz(getConstBrightestStarName(iConst),
                                         horizonCoord.getAltAngle.getDecAngle, horizonCoord.getAzAngle.getDecAngle,
                                         BRIGHTEST_OBJ_COLOR)
            End If
        End If

        lblResults.Text = "Brightest star in " & getConstName(iConst) &
                            " is " & getConstBrightestStarName(iConst)
    End Sub

    '--------------------------------------------------------------
    ' Draw the brightest star in each of the constellations.
    '--------------------------------------------------------------
    Private Sub drawBrightestStarInAllConst()
        Dim i As Integer
        Dim RA, Decl As Double
        Dim LST As Double
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        If chkboxLabelStars.Checked Then
            If Not PleaseConfirm("Labelling the stars may make the display   " & vbNewLine &
                                 "unreadable. Do you wish to continue anyway?", " ") Then Return
        End If

        For i = 0 To getNumConstellations() - 1
            lblResults.Text = "Plotting brightest star in constellation " & getConstName(i)

            RA = getConstBrightestStarRA(i)
            Decl = getConstBrightestStarDecl(i)

            If chkboxEQChart.Checked Then
                starChart.plotRADecl(RA, Decl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
                If chkboxLabelStars.Checked Then
                    starChart.drawLabelRADecl(getConstBrightestStarName(i), RA, Decl, BRIGHTEST_OBJ_COLOR)
                End If
            Else
                horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                ' plotAltAz and drawLabelAltAz ignore if coord is below the horizon
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                horizonCoord.getAzAngle.getDecAngle, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
                If chkboxLabelStars.Checked Then
                    starChart.drawLabelAltAz(getConstBrightestStarName(i),
                                             horizonCoord.getAltAngle.getDecAngle, horizonCoord.getAzAngle.getDecAngle,
                                             BRIGHTEST_OBJ_COLOR)
                End If
            End If
        Next

        lblResults.Text = "Finished plotting brightest stars in each constellation ..."
    End Sub

    '--------------------------------------------------------------
    ' Locate a horizon coordinate that the user specifies
    '--------------------------------------------------------------
    Private Sub locateAltAz()
        Dim LST As Double
        Dim altObj As ASTAngle = New ASTAngle
        Dim azObj As ASTAngle = New ASTAngle
        Dim eqCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        If queryForm.showQueryForm("Enter Altitude (xxxd yym zz.zzs)", "Enter Azimuth (xxxd yym zz.zzs)") =
                                 Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidAngle(queryForm.getData1(), altObj, HIDE_ERRORS) Then
                ErrMsg("The Altitude entered is invalid - try again.", "Invalid Altitude")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), azObj, HIDE_ERRORS) Then
                ErrMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth")
                Return
            End If
        Else
            Return
        End If

        If chkboxEQChart.Checked Then
            eqCoord = HorizontoRADecl(altObj.getDecAngle, azObj.getDecAngle, observer.getObsLat, LST)
            starChart.plotRADecl(eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
            If chkboxLabelStars.Checked Then
                starChart.drawLabelRADecl("LOC", eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle, LOCATE_OBJ_COLOR)
            End If
        Else
            If altObj.getDecAngle < 0 Then
                InfoMsg("The stated coordinate is below the observer's horizon")
            Else
                starChart.plotAltAz(altObj.getDecAngle, azObj.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
                If chkboxLabelStars.Checked Then
                    starChart.drawLabelAltAz("LOC", altObj.getDecAngle, azObj.getDecAngle, LOCATE_OBJ_COLOR)
                End If
            End If
        End If

        lblResults.Text = angleToStr(altObj.getDecAngle, DMSFORMAT) & " Alt, " & angleToStr(azObj.getDecAngle, DMSFORMAT) & " Az"
    End Sub

    '--------------------------------------------------------------
    ' Locate and highlight the brightest object in the catalog
    '--------------------------------------------------------------
    Private Sub locateBrightestObjInCat()
        Dim i, idx As Integer
        Dim LST As Double
        Dim RA, Decl, mV As Double
        Dim brightestmV = UNKNOWN_mV
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        If Not getCatmVProvided() Then
            InfoMsg("The currently loaded catalog does not provide magnitudes...")
            Return
        End If

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        ' Cycle through and find brightest object
        For i = 0 To getCatNumObjs() - 1
            mV = getCatObjmV(i)
            If mV < brightestmV Then
                brightestmV = mV
                idx = i
            End If
        Next

        RA = getCatObjRA(idx)
        Decl = getCatObjDecl(idx)
        mV = getCatObjmV(idx)

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
            If chkboxLabelStars.Checked Then
                starChart.drawLabelRADecl(getCatObjName(idx), RA, Decl, HIGHLIGHT_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle < 0 Then
                InfoMsg("The object '" & getCatObjName(idx) & "' is below the observer's horizon")
            Else
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, mV, HIGHLIGHT_COLOR)
                If chkboxLabelStars.Checked Then
                    starChart.drawLabelAltAz(getCatObjName(idx),
                                             horizonCoord.getAltAngle.getDecAngle,
                                             horizonCoord.getAzAngle.getDecAngle, HIGHLIGHT_COLOR)
                End If
            End If
        End If

        lblResults.Text = "Brightest Object is '" & getCatObjName(idx) & "' at " & timeToStr(RA, HMSFORMAT) &
            " RA, " & angleToStr(Decl, DMSFORMAT) & " Decl, " & Format(mV, mVFormat) & " mV"
    End Sub

    '--------------------------------------------------------------
    ' Locate an equatorial coordinate that the user specifies
    '--------------------------------------------------------------
    Private Sub locateRADecl()
        Dim raObj As ASTTime = New ASTTime
        Dim LST As Double
        Dim declObj As ASTAngle = New ASTAngle
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", "Enter Declination (xxxd yym zz.zzs)") =
                         Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If
        Else
            Return
        End If

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(raObj.getDecTime, declObj.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
            If chkboxLabelStars.Checked Then
                starChart.drawLabelRADecl("LOC", raObj.getDecTime, declObj.getDecAngle, LOCATE_OBJ_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(raObj.getDecTime, declObj.getDecAngle, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle < 0 Then
                InfoMsg("The stated coordinate is below the observer's horizon")
            Else
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
                If chkboxLabelStars.Checked Then
                    starChart.drawLabelAltAz("LOC", horizonCoord.getAltAngle.getDecAngle,
                                             horizonCoord.getAzAngle.getDecAngle, LOCATE_OBJ_COLOR)
                End If
            End If
        End If

        lblResults.Text = timeToStr(raObj.getDecTime, HMSFORMAT) & " RA, " &
            angleToStr(declObj.getDecAngle, DMSFORMAT) & " Decl"
    End Sub

    '--------------------------------------------------------------
    ' Locate and highlight a user-specified object from the catalog
    '--------------------------------------------------------------
    Private Sub locateStarCatalogObj()
        Dim idx As Integer
        Dim LST As Double
        Dim RA, Decl, mV As Double
        Dim searchStr As String
        Dim horizonCoord As ASTCoord = New ASTCoord

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter the Object's Name") = Windows.Forms.DialogResult.OK Then
            searchStr = queryForm.getData1()
            idx = findObjByName(searchStr.Trim())
            If idx < 0 Then
                ErrMsg("No Object with the name '" & searchStr & "' was found in the catalog",
                       "Invalid Object Name")
                Return
            End If
        Else
            Return
        End If

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        RA = getCatObjRA(idx)
        Decl = getCatObjDecl(idx)

        ' Show the object regardless of whether it is within the mV filter
        If getCatObjmVKnown(idx) Then
            mV = getCatObjmV(idx)
        Else
            mV = mV_NAKED_EYE   ' default to what the naked eye can see
        End If

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
            If chkboxLabelStars.Checked Then
                starChart.drawLabelRADecl(searchStr, RA, Decl, HIGHLIGHT_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle < 0 Then
                InfoMsg("The object '" & searchStr & "' is below the observer's horizon")
            Else
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, mV, HIGHLIGHT_COLOR)
                If chkboxLabelStars.Checked Then
                    starChart.drawLabelAltAz(searchStr,
                                             horizonCoord.getAltAngle.getDecAngle, horizonCoord.getAzAngle.getDecAngle,
                                             HIGHLIGHT_COLOR)
                End If
            End If
        End If

        lblResults.Text = "The object '" & searchStr & "' is at " & timeToStr(RA, HMSFORMAT) & " RA, " &
            angleToStr(Decl, DMSFORMAT) & " Decl"
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------
    ' Clear the text areas in the GUI
    '--------------------------------------
    Private Sub clearTextAreas()
        chartOnScreen = False
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '-----------------------------------------------------------------------
    ' This method computes the LST for the observer's current location
    ' and time. This is needed in order to convert equatorial coordinates
    ' to horizon coordinates. The observer's location is retrieved
    ' from the GUI.
    '
    ' NOTE: It is assumed that the observer's location/time has already
    ' been validated prior to this method being invoked.
    '-----------------------------------------------------------------------
    Private Function computeLST() As Double
        Dim LCT, UT, GST, LST As Double
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate

        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)
        Return LST
    End Function

    '----------------------------------------------------
    ' Determine what time zone radio button is selected
    '
    ' Returns a time zone type for whatever is the
    ' currently selected time zone radio button
    '----------------------------------------------------
    Private Function getSelectedRBStatus() As TimeZoneType
        If radbtnPST.Checked() Then
            Return TimeZoneType.PST
        ElseIf radbtnMST.Checked() Then
            Return TimeZoneType.MST
        ElseIf radbtnCST.Checked() Then
            Return TimeZoneType.CST
        ElseIf radbtnEST.Checked() Then
            Return TimeZoneType.EST
        Else
            Return TimeZoneType.LONGITUDE
        End If
    End Function

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '-------------------------------------------------------
    ' Sets the Catalog type in the GUI.
    '
    ' cat_type      String representing the catalog type
    '               which is read from the star catalog
    '               data file
    '-------------------------------------------------------
    Private Sub setCatalogType(ByVal cat_type As String)
        lblCatalogType.Text = "Catalog Type: " & cat_type
    End Sub

    '-----------------------------------
    ' Sets the Epoch label in the GUI.
    '
    ' epoch         Epoch to display
    '-----------------------------------
    Private Sub setEpoch(ByVal epoch As Double)
        lblEpoch.Text = "Epoch: " & Format(epoch, epochFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the filename label to be displayed in the GUI.
    '
    ' filename          star catalog filename, including pathname
    '---------------------------------------------------------------
    Private Sub setFilename(ByVal fname As String)
        lblFilename.Text = "File: " & abbrevRight(fname, MAX_FNAME_DISPLAY)
    End Sub

    '---------------------------------------------------------------
    ' Sets the mv Filter label to be displayed in the GUI.
    '
    ' mV            visual magnitude filter to display
    '---------------------------------------------------------------
    Private Sub setmVFilter(ByVal mV As Double)
        currentmVFilter = mV
        lblmVFilter.Text = "mV Filter: " & Format(mV, mVFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the observer location in the GUI. This is intended to be
    ' done one time only during the program initialization since a
    ' default location may be read from a data file.
    '
    ' obs           observer location object
    '---------------------------------------------------------------
    Private Sub setObsDataInGUI(ByVal obs As ASTObserver)
        txtboxLat.Text = latToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxLon.Text = lonToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxDate.Text = dateToStr(obs.getObsDate())
        txtboxLCT.Text = timeToStr(obs.getObsTime(), HMSFORMAT)

        If obs.getObsTimeZone = TimeZoneType.PST Then
            radbtnPST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.MST Then
            radbtnMST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.CST Then
            radbtnCST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.EST Then
            radbtnEST.Checked = True
        Else
            radbtnLon.Checked = True
        End If

    End Sub

    '----------------------------------------------
    ' See if the observer location, date, and time
    ' currently in the GUI is valid.
    '
    ' Returns true if valid, otherwise false.
    '----------------------------------------------
    Private Function validateGUIObsLoc() As Boolean
        Return isValidObsLoc(observer, txtboxLat.Text, txtboxLon.Text, getSelectedRBStatus().ToString,
                             txtboxDate.Text, txtboxLCT.Text)
    End Function

End Class
