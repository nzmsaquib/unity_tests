﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This class is used to allow the user to select the
' method for solving the true anomaly.
'**********************************************************

Option Explicit On
Option Strict On

Imports ASTUtils
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTOrbits

Friend Class TrueAnomalyRBs

    ' When a new instance is created, default to
    ' solving the equation of the center
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Me.radbtnEQofCenter.Checked = True
    End Sub

    ' Determine what the current setting is for which
    ' method the user wants to use to find the true anomaly.
    Friend Function getTrueAnomalyRB() As TrueAnomalyType
        If Me.radbtnEQofCenter.Checked Then
            Return TrueAnomalyType.SOLVE_EQ_OF_CENTER
        ElseIf Me.radbtnSimple.Checked Then
            Return TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        Else
            Return TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        End If
    End Function

    ' Display the radio button form and center it on the parent
    Friend Sub showRBForm()
        Me.StartPosition = FormStartPosition.CenterParent
        Me.ShowDialog()
    End Sub

    ' Handle closing the form
    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub

End Class