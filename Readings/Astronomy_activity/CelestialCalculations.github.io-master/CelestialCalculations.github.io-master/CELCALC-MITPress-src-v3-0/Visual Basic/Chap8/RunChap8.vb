﻿'******************************************************************
'               Chapter 8 - Our Solar System
'                   Copyright (c) 2018
'                  Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' for a program that will calculate various items about objects
' in our Solar System, including the location of the planets
' and when they will rise and set. The main GUI was mostly built 
' with the Visual Basic form editor.
'******************************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTCatalog
Imports ASTUtils.ASTConstellation
Imports ASTUtils.ASTCoord
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTObserver
Imports ASTUtils.ASTOrbits
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap8.ChapMenuItem

Public Class Chap8GUI
    Private Const AboutBoxText = "Chapter 8 - Our Solar System"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt = Nothing
    Private constellations As ASTConstellation
    Private catalog As ASTCatalog
    Private orbElements As ASTOrbits = Nothing
    Private observer As ASTObserver = Nothing

    ' Create a reusable query form for user input and for selecting 
    ' method for solving true anomaly
    Private queryForm As ASTQuery = New ASTQuery()
    Private rbForm As TrueAnomalyRBs = New TrueAnomalyRBs()

    ' Set a default termination criteria (in radians) for solving Kepler's equation
    Private termCriteria As Double = 0.000002

    Private Const DEFAULT_mV_FILTER = mV_NAKED_EYE
    Private currentmVFilter As Double                   ' whatever the current mV filter in the GUI is

    ' Create a drawing canvas for the star charts
    Private starChart As ASTCharts = Nothing

    ' Color to use when highlighting an object, brightest object, and located object.
    ' Also define large mV values to use to display brightest and located objects.
    Private Shared HIGHLIGHT_COLOR As Brush = Brushes.Red
    Private Shared BRIGHTEST_OBJ_COLOR As Brush = Brushes.Blue
    Private Shared LOCATE_OBJ_COLOR As Brush = Brushes.Green
    Private Shared SUN_COLOR As Brush = Brushes.OrangeRed
    Private Shared MOON_COLOR As Brush = Brushes.Silver
    Private Shared PLANET_COLOR As Brush = Brushes.Purple
    Private Const BRIGHT_mV As Double = -8.0
    Private Const LOCATE_mV As Double = -12.0

    ' Flag indicating whether there is currently a star chart on the screen. This is
    ' used so that various stars (in a constellation, brightest, etc.) can be highlighted
    ' on an existing chart.
    Private chartOnScreen As Boolean = False

    ' Maximum length of a filename to display. Actual filename can be longer, but only a limited
    ' number of characters can be displayed in the GUI
    Private Const MAX_FNAME_DISPLAY = 70

    ' Save the index into the orbital elements DB for the Moon, Earth, and Sun
    ' so that we don't have to constantly get them
    Private idxEarth As Integer = -1
    Private idxSun As Integer = -1
    Private idxMoon As Integer = -1

    ' Define the table values to use for determining perihelion and aphelion dates
    ' Column 0 is the planet's name, columns 1 and 2 are k0 and k1 respectively,
    ' and columns 3-5 are j0, j1, and j2 from the tables in the book
    Private Shared planetsTable As Object(,) = {{"Mercury", 4.15201, 2000.12, 2451590.257, 87.96934963, 0.0},
                                                {"Venus", 1.62549, 2000.53, 2451738.233, 224.7008188, -0.0000000327},
                                                {"Earth", 0.99997, 2000.01, 2451547.507, 365.2596358, 0.0000000156},
                                                {"Mars", 0.53166, 2001.78, 2452195.026, 686.9957857, -0.0000001187},
                                                {"Jupiter", 0.0843, 2011.2, 2455636.936, 4332.897065, 0.0001367},
                                                {"Saturn", 0.03393, 2003.52, 2452830.12, 10764.21676, 0.000827},
                                                {"Uranus", 0.0119, 2051.1, 2470213.5, 30694.8767, -0.00541},
                                                {"Neptune", 0.00607, 2047.5, 2468895.1, 60190.33, 0.03429}}
    Private Const NUM_PLANETS = 8

    ' These constants are used purely to make code more readable when
    ' asking the user for a Solar System object
    Private Const GETSUN As Boolean = True
    Private Const GETEARTH As Boolean = True
    Private Const GETMOON As Boolean = True

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearTextAreas()
        ' Must create the starChart **after** InitializeComponent has been done
        starChart = New ASTCharts(txtboxOutputArea)

        ' Create the menu items
        createChapMenus()

        ' Now initialize the rest of the GUI
        chkboxShowInterimCalcs.Checked = False
        chkboxDST.Checked = False
        radbtnLon.Checked = True
        chkboxWhiteCharts.Checked = True
        chkboxLabelObjs.Checked = True

        ' Load the default orbital elements and observer location (if it exists),
        ' and load the constellations
        constellations = New ASTConstellation(prt)
        catalog = New ASTCatalog(prt)
        observer = New ASTObserver
        setObsDataInGUI(observer)
        orbElements = New ASTOrbits(prt)
        idxEarth = orbElements.getOEDBEarthIndex()
        idxMoon = orbElements.getOEDBMoonIndex()
        idxSun = orbElements.getOEDBSunIndex()

        ' get the epoch to which the orbital elements are referenced
        setEpoch(orbElements.getOEEpochDate)
        setFilename("")
        setCatalogType("")
        setmVFilter(DEFAULT_mV_FILTER)
        chartOnScreen = False

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------------------------------------
    ' Create the Solar Sys Info, Data Files, and Star Charts menus.
    '---------------------------------------------------------------------------------------------

    '------------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler.
    '
    ' title         title for the menu item
    ' CalcType      what type of calculation to perform
    '               when this menu item is clicked
    '------------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '--------------------------------------------------------
    ' Create the Solar Sys Info, Data Files, and Star Charts
    ' menu subitems
    '--------------------------------------------------------
    Private Sub createChapMenus()
        Dim tmpSubMenu As ToolStripMenuItem

        ' Create Solar Sys Info menu items
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Find Object's Position", CalculationType.OBJ_LOCATION))
        mnuSolarSysInfo.DropDownItems.Add("-")
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Object Rise/Set Times", CalculationType.OBJ_RISE_SET))
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Object Distance and Angular Diameter", CalculationType.OBJ_DIST_AND_ANG_DIAMETER))
        mnuSolarSysInfo.DropDownItems.Add("-")
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Planetary Perihelion and Aphelion", CalculationType.PLANET_PERI_APHELION))
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Distance at Perihelion and Aphelion", CalculationType.OBJ_PERI_APH_DIST))
        mnuSolarSysInfo.DropDownItems.Add("-")
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Object Percent Illumination", CalculationType.OBJ_ILLUMINATION))
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Object Visual Magnitude", CalculationType.OBJ_MAGNITUDE))
        mnuSolarSysInfo.DropDownItems.Add("-")
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Miscellaneous Data", CalculationType.OBJ_MISC_DATA))
        mnuSolarSysInfo.DropDownItems.Add("-")
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Set Termination Criteria", CalculationType.TERM_CRITERIA))
        mnuSolarSysInfo.DropDownItems.Add(createMenuItem("Select True Anomaly Method", CalculationType.KEPLER_OR_EQOFCENTER))

        ' Create Data Files menu
        mnuDataFiles.DropDownItems.Add(createMenuItem("Load Orbital Elements", CalculationType.LOAD_ORBITAL_ELEMENTS))
        mnuDataFiles.DropDownItems.Add(createMenuItem("Display All Orbital Elements", CalculationType.SHOW_ALL_ORBITAL_ELEMENTS))
        mnuDataFiles.DropDownItems.Add(createMenuItem("Display Object's Orbital Elements", CalculationType.SHOW_OBJ_ORBITAL_ELEMENTS))
        mnuDataFiles.DropDownItems.Add("-")
        mnuDataFiles.DropDownItems.Add(createMenuItem("Load a Star Catalog", CalculationType.LOAD_CATALOG))
        mnuDataFiles.DropDownItems.Add(createMenuItem("Show Catalog Information", CalculationType.SHOW_CATALOG_INFO))
        mnuDataFiles.DropDownItems.Add(createMenuItem("List all Objects in the Catalog", CalculationType.LIST_ALL_OBJS_IN_CAT))
        mnuDataFiles.DropDownItems.Add("-")
        mnuDataFiles.DropDownItems.Add(createMenuItem("List All Constellations", CalculationType.LIST_ALL_CONST))
        mnuDataFiles.DropDownItems.Add(createMenuItem("List all Catalog Objects in a Constellation", CalculationType.LIST_ALL_CAT_OBJS_IN_CONST))
        mnuDataFiles.DropDownItems.Add("-")
        mnuDataFiles.DropDownItems.Add(createMenuItem("Find Constellation a RA/Decl is In", CalculationType.FIND_CONST_CAT_OBJ_IN))
        mnuDataFiles.DropDownItems.Add(createMenuItem("Find Constellation a Solar System Object is In", CalculationType.FIND_CONST_SOLAR_SYS_OBJ_IN))

        ' Create Star Charts menu
        mnuStarCharts.DropDownItems.Add(createMenuItem("Set mV Filter", CalculationType.SET_mV_FILTER))
        mnuStarCharts.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Draw ..."
        mnuStarCharts.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("All Stars in the Catalog", CalculationType.DRAW_ALL_STARS))
        tmpSubMenu.DropDownItems.Add(createMenuItem("All Stars in a Constellation", CalculationType.DRAW_ALL_STARS_IN_CONST))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Brightest Star in each Constellation", CalculationType.DRAW_ALL_CONST))
        tmpSubMenu.DropDownItems.Add("-")
        tmpSubMenu.DropDownItems.Add(createMenuItem("All Solar System Objects", CalculationType.DRAW_ALL_SOLAR_SYS_OBJS))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Sun, Moon, or a Planet", CalculationType.DRAW_SOLAR_SYS_OBJ))
        mnuStarCharts.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Locate ..."
        mnuStarCharts.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("An Object in the Catalog", CalculationType.FIND_OBJ))
        tmpSubMenu.DropDownItems.Add(createMenuItem("Brightest Object in the Catalog", CalculationType.FIND_BRIGHTEST_OBJ))
        tmpSubMenu.DropDownItems.Add("-")
        tmpSubMenu.DropDownItems.Add(createMenuItem("An Equatorial Coordinate", CalculationType.FIND_EQ_LOC))
        tmpSubMenu.DropDownItems.Add(createMenuItem("A Horizon Coordinate", CalculationType.FIND_HORIZ_LOC))
    End Sub

    '------------------------------------------------------------------
    ' The following methods listen for changes, such as a checkbox
    ' update or a screen resize event, that may invalidate any
    ' chart that is currently on the screen. This is necessary because
    ' we want to keep track of whether a chart is already on the screen
    ' so that we can highlight specific objects on that chart. Changing
    ' the type of chart to display affects how the plotting is done, so
    ' we have to invalidate and assume that no chart is on the screen.
    '------------------------------------------------------------------
    Private Sub chkboxEQChart_click(ByVal sender As Object, ByVal e As System.EventArgs) _
        Handles chkboxEQChart.Click, chkboxWhiteCharts.Click
        chartOnScreen = False
    End Sub
    Private Sub Chap8GUI_resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        chartOnScreen = False
    End Sub

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '------------------------------------------------------
    ' Handle clicks on the Solar Sys Info, Data Files, and
    ' Star Charts menu items
    '------------------------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        Select Case calctype
            '*************Solar Sys Info menu
            Case CalculationType.OBJ_LOCATION
                calcObjPosition()
            Case CalculationType.OBJ_RISE_SET
                calcObjRiseSet()
            Case CalculationType.OBJ_DIST_AND_ANG_DIAMETER
                calcDistAndAngDiameter()
            Case CalculationType.PLANET_PERI_APHELION
                calcPlanetPeriAphelion()
            Case CalculationType.OBJ_PERI_APH_DIST
                calcObjPeriAphDist()
            Case CalculationType.OBJ_ILLUMINATION
                calcObjIllumination()
            Case CalculationType.OBJ_MAGNITUDE
                calcObjMagnitude()
            Case CalculationType.OBJ_MISC_DATA
                calcObjMiscData()
            Case CalculationType.TERM_CRITERIA
                setTerminationCriteria()
            Case CalculationType.KEPLER_OR_EQOFCENTER
                rbForm.showRBForm()

                '************* Data Files menu
            Case CalculationType.LOAD_ORBITAL_ELEMENTS
                loadOrbitalElements()
            Case CalculationType.SHOW_ALL_ORBITAL_ELEMENTS
                showAllOrbitalElements()
            Case CalculationType.SHOW_OBJ_ORBITAL_ELEMENTS
                showObjOrbitalElements()
            Case CalculationType.LOAD_CATALOG
                loadCatalog()
            Case CalculationType.SHOW_CATALOG_INFO
                showCatalogInfo()
            Case CalculationType.LIST_ALL_OBJS_IN_CAT
                listAllObjsInCatalog()
            Case CalculationType.LIST_ALL_CONST
                listAllConstellations()
            Case CalculationType.LIST_ALL_CAT_OBJS_IN_CONST
                listAllObjsInConstellation()
            Case CalculationType.FIND_CONST_CAT_OBJ_IN
                findConstellationForRA_Decl()
            Case CalculationType.FIND_CONST_SOLAR_SYS_OBJ_IN
                findConstellationForObj()

                '************* Star Charts menu
            Case CalculationType.SET_mV_FILTER
                changemVFilter()
            Case CalculationType.DRAW_ALL_STARS
                drawAllStars()
            Case CalculationType.DRAW_ALL_STARS_IN_CONST
                drawAllStarsInConst()
            Case CalculationType.DRAW_ALL_CONST
                drawBrightestStarInAllConst()
            Case CalculationType.DRAW_ALL_SOLAR_SYS_OBJS
                drawAllSolarSysObjs()
            Case CalculationType.DRAW_SOLAR_SYS_OBJ
                drawSolarSysObj()
            Case CalculationType.FIND_OBJ
                locateStarCatalogObj()
            Case CalculationType.FIND_BRIGHTEST_OBJ
                locateBrightestObjInCat()
            Case CalculationType.FIND_EQ_LOC
                locateRADecl()
            Case CalculationType.FIND_HORIZ_LOC
                locateAltAz()
        End Select

    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click

        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " &
                    "be used in subsequent calculations. You may find it convenient to enter an initial latitude, longitude, " &
                    "and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " &
                    "longitude, and time zone are already filled in with default values when the program starts. When this program " &
                    "begins, the date and time will default to the local date and time at which the program is started.")
        prt.println()
        prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " &
                    "various calculations are performed. Also, select 'Solar Sys Info->Select True Anomaly Method' to indicate what " &
                    "method to use when finding the true anomaly for the Sun or a planet. This menu entry will bring up a window with " &
                    "radio buttons corresponding to the methods supported. The radio button 'Equation of Center' will solve " &
                    "the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " &
                    "'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " &
                    "Newton/Raphson method. The menu entry 'Solar Sys Info->Set Termination Critera' allows you to enter the " &
                    "termination criteria (in radians) for when to stop iterating to solve Kepler's equation. These radio buttons " &
                    "only affect how the true anomaly is computed for the Sun and planets. Finding the Moon's true anomaly is " &
                    "accomplished by solving the equation of the center regardless of which of these radio buttons is set.")
        prt.println()
        prt.println("The menu 'Solar Sys Info' performs various calculations related to objects in the Solar System, such as " &
                    "determining a planet's location for the current information in the 'Observer Location and Time' data area. The " &
                    "menu 'Data Files->Load Orbital Elements' allow you to load data files that contain the orbital elements referenced " &
                    "to a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " &
                   "starts up. The menu entries under 'Solar Sys Info' in general will work for any object in the orbital elements " &
                   "data file except for the Sun, Moon, and Earth. There are, however, some exceptions to this rule. For example, " &
                   "'Planetary Perihelion and Aphelion' will not work for Pluto because it is no longer considered to be a planet. It " &
                   "does work for Earth because Earth is a planet. As another example, 'Miscellaneous Data' allows any object in " &
                   "the orbital elements data file to be selected, but not all data items are computed for every object.")
        prt.println()
        prt.println("The menu 'Data Files' allows orbital elements data files and star catalogs to be loaded. The menu also provides " &
                    "entries for listing the constellations, stars in the currently loaded star catalog, and orbital elements " &
                    "from the currently loaded orbital elements data file. The last two entries will determine what constellation " &
                    "an equatorial coordinate (RA, Decl) falls within as well as what constellation a solar system object falls " &
                    "within for the current observer's location.")
        prt.println()
        prt.println("The first menu entry under the 'Star Charts' menu allows the mV filter to be set. This filter affects what stars " &
                    "are plotted; that is, only those stars that are at least as bright as the mV filter setting will be plotted. " &
                    "The Sun, Moon, and planets will be plotted (assuming they are visible) regardless of the mV filter setting. " &
                    "The remaining items in this menu allow star charts to be created and displayed. The 'Equatorial Coords Charts' " &
                    "checkbox determines what type of star chart to create. If checked, the star chart will be a rectangular plot of " &
                    "right ascension and declination (i.e., equatorial coordinates) while if the checkbox " &
                    "is not checked, a circular plot of horizon coordinates will be generated. Resizing the application window will " &
                    "destroy any star chart that has been created. If this happens, after resizing the window " &
                    "to the size you want, simply go through the menu items to redraw the star chart that you want. It is " &
                    "typically better to resize the application window to the size you want before generating a star chart.")
        prt.println()
        prt.println("The 'White Bkg for Charts' checkbox allows you to produce charts with a white background (checkbox is checked) " &
                    "or a black background (checkbox is not checked). The remaining checkbox, ('Label Objects') indicates whether " &
                    "the name of an object should be displayed on the star charts. The menu items under 'Star Charts' provide the " &
                    "ability to plot all of the objects in the currently loaded star catalog, only those in a particular " &
                    "constellation, just the brightest star in a constellation, or objects in the Solar System.")
        prt.println()
        prt.println("Two cautions are worth noting regarding displaying the names of objects on a star chart. First," &
                    "an object's name will appear below where the object is plotted. If an object is near one of the chart's sides," &
                    "the label may be only partially displayed. Second, you should plot labels sparingly because plotting " &
                    "too many labels will clutter the star chart. For example, check the 'Label Objects' checkbox and then select the " &
                    "'Star Charts->Draw ...->Brightest Star in Each Constellation' menu entry to see how cluttered a chart can become!")

        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Solar Sys Info menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Calculate an object's distance and angular diameter.
    '------------------------------------------------------------
    Private Sub calcDistAndAngDiameter()
        Dim objName As String
        Dim idx As Integer
        Dim R_p, R_e, L_e, L_p, dDist, dDist_km, Theta As Double
        Dim eclCoord As ASTCoord
        Dim solveTrueAnomaly As TrueAnomalyType

        clearTextAreas()

        idx = getValidObj(Not GETSUN, Not GETEARTH, Not GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        prt.setBoldFont(True)
        printlncond("Compute the Distance from Earth and the Angular Diameter for " & objName & " for the Observer's Date", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                       observer.getObsDate.getYear, 0.0, idx, L_p, R_p, L_e, R_e,
                                                       solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the radius vector length and heliocentric ecliptic longitude")
        printlncond("    for the Earth and " & objName & " at UT = 0.0 hours")
        printlncond("    Lp = " & angleToStr(L_p, DECFORMAT) & " degrees, Rp = " & Format(R_p, genFloatFormat) & " AUs")
        printlncond("    Le = " & angleToStr(L_e, DECFORMAT) & " degrees, Re = " & Format(R_e, genFloatFormat) & " AUs")
        printlncond()

        dDist = calcObjDistToEarth(R_e, L_e, R_p, L_p)
        printlncond("2.  Calculate the distance from " & objName & " to the Earth")
        printlncond("    Dist = sqrt[Re^2 + Rp^2 - 2*Re*Rp*cos(Lp - Le)]")
        printlncond("         = sqrt[" & Format(R_e, genFloatFormat) & "^2 + " & Format(R_p, genFloatFormat) & "^2 - 2*" &
                    Format(R_e, genFloatFormat) & "*" & Format(R_p, genFloatFormat) & "*cos(" &
                    angleToStr(L_p, DECFORMAT) & " - " & angleToStr(L_e, DECFORMAT) & ")]")
        printlncond("         = " & Format(dDist, genFloatFormat) & " AUs")
        printlncond()

        Theta = orbElements.getOEObjAngDiamArcSec(idx) / dDist
        printlncond("3.  Compute the angular diameter for " & objName & ".")
        printlncond("    Theta = Theta_p/Dist = " & angleToStr(orbElements.getOEObjAngDiamArcSec(idx), DECFORMAT) & "/" &
                    Format(dDist, genFloatFormat) & " = " & angleToStr(Theta, DECFORMAT) & " arcseconds")
        printlncond("    (Note: distance must be in AUs and Theta_p must be in arcseconds)")
        printlncond()

        dDist_km = Round(AU2KM(dDist), 0)
        Theta = Theta / 3600.0 ' Convert from arcseconds to degrees
        printlncond("4.  Convert the distance to km or miles, and angular diameter to DMS format.")
        printlncond("    Dist = " & insertCommas(dDist_km) & " km = " & insertCommas(Round(KM2Miles(dDist_km), 0)) & " miles")
        printlncond("    Theta = " & angleToStr(Theta, DMSFORMAT))
        printlncond()

        printlncond("On " & dateToStr(observer.getObsDate) & ", " & objName & " is/was " & insertCommas(dDist_km) & " km away")
        printlncond("and its angular diameter is/was " & angleToStr(Theta, DMSFORMAT))

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Distance to " & objName & ": " & Format(dDist, genFloatFormat) &
            " AUs (" & insertCommas(dDist_km) & " km, " & insertCommas(Round(KM2Miles(dDist_km), 0)) &
            " miles), Angular Diameter: " & angleToStr(Theta, DMSFORMAT)
    End Sub

    '------------------------------------------------------------
    ' Calculate how much of an object is illuminated.
    '------------------------------------------------------------
    Private Sub calcObjIllumination()
        Dim objName As String
        Dim idx As Integer
        Dim eclCoord As ASTCoord
        Dim R_p, R_e, L_e, L_p, dDist, dKpcnt As Double
        Dim solveTrueAnomaly As TrueAnomalyType

        clearTextAreas()

        idx = getValidObj(Not GETSUN, Not GETEARTH, Not GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        prt.setBoldFont(True)
        printlncond("Compute how much " & objName & " is illuminated as seen from Earth for the Observer's Date", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                       observer.getObsDate.getYear, 0.0, idx, L_p, R_p, L_e, R_e,
                                                       solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the radius vector length for the Earth and " & objName & " at UT = 0.0 hours")
        printlncond("    Rp = " & Format(R_p, genFloatFormat) & " AUs, Re = " & Format(R_e, genFloatFormat) & " AUs")
        printlncond()

        dDist = calcObjDistToEarth(R_e, L_e, R_p, L_p)
        printlncond("2.  Calculate the distance from " & objName & " to the Earth")
        printlncond("    Dist = " & Format(dDist, genFloatFormat) & " AUs")
        printlncond()

        dKpcnt = ((R_p + dDist) ^ 2 - R_e ^ 2) / (4 * R_p * dDist)
        dKpcnt = 100 * dKpcnt
        printlncond("3.  Calculate the percent illumination.")
        printlncond("    K% = 100*[((Rp + Dist)^2 - Re^2)/(4*Rp*Dist)]")
        printlncond("       = 100*[((" & Format(R_p, genFloatFormat) & " + " & Format(dDist, genFloatFormat) &
                    ")^2 - " & Format(R_e, genFloatFormat) & "^2)/(4*" & Format(R_p, genFloatFormat) & "*" &
                    Format(dDist, genFloatFormat) & ")]")
        printlncond("       = " & Format(dKpcnt, genFloatFormat))
        printlncond()

        printlncond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear) &
                    ", " & objName & " is/was " & Round(dKpcnt, 1) & "% illuminated as seen from Earth")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "On " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear) &
            ", " & objName & " is/was " & Round(dKpcnt, 1) & "% illuminated."
    End Sub

    '------------------------------------------------------------
    ' Calculate an object's (apparent) visual magnitude.
    '------------------------------------------------------------
    Private Sub calcObjMagnitude()
        Dim objName As String
        Dim idx As Integer
        Dim eclCoord As ASTCoord
        Dim R_p, R_e, L_e, L_p, Lon_p, Vp, mV, dDist, dPA As Double
        Dim solveTrueAnomaly As TrueAnomalyType

        clearTextAreas()

        idx = getValidObj(Not GETSUN, Not GETEARTH, Not GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        prt.setBoldFont(True)
        printlncond("Compute the visual magnitude for " & objName, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                       observer.getObsDate.getYear, 0.0, idx, L_p, R_p, L_e, R_e,
                                                       solveTrueAnomaly, termCriteria)
        Lon_p = eclCoord.getLonAngle.getDecAngle
        printlncond("1.  Calculate the object's position at UT = 0.0 hours to get its heliocentric (Lp)")
        printlncond("    and geocentric longitude (Lon_p), and its radius vector length (Rp).")
        printlncond("    Lp = " & angleToStr(L_p, DECFORMAT) & " degrees")
        printlncond("    Lon_p = " & angleToStr(Lon_p, DECFORMAT) & " degrees")
        printlncond("    Rp = " & Format(R_p, genFloatFormat) & " AUs")
        printlncond()

        dDist = calcObjDistToEarth(R_e, L_e, R_p, L_p)
        printlncond("2.  Calculate the distance from " & objName & " to the Earth")
        printlncond("    Dist = " & Format(dDist, genFloatFormat) & " AUs")
        printlncond()

        dPA = (1 + COS_D(Lon_p - L_p)) / 2.0
        printlncond("3.  Calculate the object's phase angle.")
        printlncond("    PA = [1 + cos(Lon_p - Lp)]/2 = [1 + cos(" & angleToStr(Lon_p, DECFORMAT) & " - " &
                    angleToStr(L_p, DECFORMAT) & ")]/2]")
        printlncond("       = " & angleToStr(dPA, DECFORMAT) & " degrees")
        printlncond("    (The elongation is Lon_p - Lp = " & angleToStr(Lon_p - L_p, DECFORMAT) & " degrees)")
        printlncond()

        Vp = orbElements.getOEObjmV(idx)
        mV = Vp + 5 * Log10((R_p * dDist) / Sqrt(dPA))
        printlncond("4.  Calculate the object's visual magnitude.")
        printlncond("    mV = Vp + 5*Log10[(Rp*Dist)/sqrt(PA)]")
        printlncond("       = " & Format(Vp, genFloatFormat) & " + 5*Log10[(" & Format(R_p, genFloatFormat) &
                    "*" & Format(dDist, genFloatFormat) & ")/sqrt(" & angleToStr(dPA, DECFORMAT) & ")]")
        printlncond("       = " & Format(mV, mVFormat))
        printlncond()

        printcond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                            observer.getObsDate.getYear) & ", the visual magnitude for ")
        printlncond(objName & " is/was " & Format(mV, mVFormat))

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "On " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear) &
            ", for " & objName & " mV = " & Format(mV, mVFormat) & " at 1 AU"
    End Sub

    '------------------------------------------------------------
    ' Calculate miscellaneous data items about an object.
    '------------------------------------------------------------
    Private Sub calcObjMiscData()
        Dim objName, result As String
        Dim idx As Integer
        Dim saveCBStatus As Boolean

        clearTextAreas()

        idx = getValidObj(GETSUN, GETEARTH, GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        prt.setBoldFont(True)
        printlncond("Compute miscellaneous data items for " & objName, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        ' Show how calculations were done, but only if needed
        If chkboxShowInterimCalcs.Checked Then calcMiscData(idx, False)

        ' Now do the summary, but temporarily set checkbox to false
        ' so that we don't do detailed display again
        saveCBStatus = chkboxShowInterimCalcs.Checked
        chkboxShowInterimCalcs.Checked = False
        prt.setProportionalFont()
        calcMiscData(idx, True)
        chkboxShowInterimCalcs.Checked = saveCBStatus

        prt.setProportionalFont()
        prt.resetCursor()

        result = "Misc data items for "
        If (idx = idxMoon) Or (idx = idxSun) Then result = result & "the "
        lblResults.Text = result & objName & " are given in the text area below"
    End Sub

    '------------------------------------------------------------
    ' Calculate the distance an object will be from the Sun when
    ' it passes through perhelion/aphelion.
    '------------------------------------------------------------
    Private Sub calcObjPeriAphDist()
        Dim objName As String
        Dim idx As Integer
        Dim a_p, e_p, Distper, Distaph, Distper_km, Distaph_km As Double

        clearTextAreas()

        idx = getValidObj(Not GETSUN, GETEARTH, Not GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        prt.setBoldFont(True)
        printlncond("Determine distance from the Sun when " & objName & " passes through Perihelion/Aphelion", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        a_p = orbElements.getOEObjSemiMajAxisAU(idx)
        e_p = orbElements.getOEObjEccentricity(idx)

        Distper = a_p * (1 - e_p)
        printlncond("1.  Compute the object's distance at perihelion.")
        printlncond("    Dist_per = a_p * (1 - eccentricity_p) = " & a_p & " * (1 - " &
                    e_p & ")")
        printlncond("             = " & Format(Distper, genFloatFormat) & " AUs")
        printlncond()

        Distaph = a_p * (1 + e_p)
        printlncond("2.  Compute the object's distance at aphelion.")
        printlncond("    Dist_aph = a_p * (1 + eccentricity_p) = " & a_p & " * (1 + " &
                    e_p & ")")
        printlncond("             = " & Format(Distaph, genFloatFormat) & " AUs")
        printlncond()

        Distper_km = Round(AU2KM(Distper), 0)
        Distaph_km = Round(AU2KM(Distaph), 0)
        printlncond("3.  Convert the distances to km or miles, if desired.")
        printlncond("    Dist_per = " & insertCommas(Distper_km) & " km = " &
                    insertCommas(Round(KM2Miles(Distper_km), 0)) & " miles")
        printlncond("    Dist_aph = " & insertCommas(Distaph_km) & " km = " &
                    insertCommas(Round(KM2Miles(Distaph_km), 0)) & " miles")
        printlncond()

        printlncond(objName & " is " & Format(Distper, genFloatFormat) & " AUs (" &
                    insertCommas(Distper_km) & " km) from the Sun at perihelion")
        printlncond("and " & Format(Distaph, genFloatFormat) & " AUs (" &
                    insertCommas(Distaph_km) & " km) from the Sun at aphelion")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = objName & " Perihelion Dist: " & Format(Distper, genFloatFormat) &
            " AUs (" & insertCommas(Distper_km) & " km), Aphelion Dist: " & Format(Distaph, genFloatFormat) &
            " AUs (" & insertCommas(Distaph_km) & " km)"
    End Sub

    '------------------------------------------------------------
    ' Calculate the position of an object, using the currently
    ' loaded orbital elements and the current observer position.
    ' The object can be anything in the orbital elements file
    ' except the Sun, Moon, or Earth.
    '------------------------------------------------------------
    Private Sub calcObjPosition()
        Dim prtObj As ASTPrt = Nothing
        Dim objName As String
        Dim idx, iter As Integer
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim result As String
        Dim horizonCoord, eqCoord As ASTCoord
        Dim LCT, UT, LST, GST, JD, JDe, De As Double
        Dim Ecc_p, inclin_p, M_p, v_p, Ec_p, Ea_p, L_p, H_p, R_p, Lp_p, Lon_p, Lat_p As Double
        Dim Ecc_e, inclin_e, M_e, v_e, Ec_e, Ea_e, L_e, H_e, R_e As Double
        Dim x, y, dT As Double
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate

        clearTextAreas()

        idx = getValidObj(Not GETSUN, Not GETEARTH, Not GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        ' Set up whether to display interim results when solving Kepler's equation
        If chkboxShowInterimCalcs.Checked Then
            prtObj = prt
        Else
            prtObj = Nothing
        End If

        prt.setBoldFont(True)
        printlncond("Compute the Position of " & objName & " for the Current Observer", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        Ecc_p = orbElements.getOEObjEccentricity(idx)
        inclin_p = orbElements.getOEObjInclination(idx)
        Ecc_e = orbElements.getOEObjEccentricity(idxEarth)
        inclin_e = orbElements.getOEObjInclination(idxEarth)

        ' Do all the time-related calculations
        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)

        printlncond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
        printlncond("    LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(observer.getObsDate) & ")")
        printcond("     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond("previous day ")
        ElseIf dateAdjust > 0 Then
            printcond("next day ")
        End If
        printlncond(dateToStr(adjustedDate) & ")")
        printlncond("    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond("    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        JDe = orbElements.getOEEpochJD
        printlncond("2.  Compute the Julian day number for the standard epoch.")
        printlncond("    Epoch: " & Format(orbElements.getOEEpochDate, epochFormat))
        printlncond("    JDe = " & Format(JDe, JDFormat))
        printlncond()

        JD = dateToJD(adjustedDate.getMonth, adjustedDate.getdDay + (UT / 24.0), adjustedDate.getYear)
        printlncond("3.  Compute the Julian day number for the desired date, being sure to use the")
        printlncond("    Greenwich date and UT from step 1, and including the fractional part of the day.")
        printlncond("    JD = " & Format(JD, JDFormat))
        printlncond()

        De = JD - JDe
        printlncond("4.  Compute the total number of elapsed days, including fractional days,")
        printlncond("    since the standard epoch.")
        printlncond("    De = JD - JDe = " & Format(JD, JDFormat) & " - " & Format(JDe, JDFormat))
        printlncond("       = " & Format(De, genFloatFormat) & " days")
        printlncond()

        ' Calculate the object's mean and true anomalies
        M_p = (360 * De) / (365.242191 * orbElements.getOEObjPeriod(idx)) + orbElements.getOEObjLonAtEpoch(idx) -
            orbElements.getOEObjLonAtPeri(idx)
        printlncond("5.  Compute the mean anomaly for " & objName)
        printlncond("    Mp = (360*De)/(365.242191*Tp) + Ep - Wp")
        printlncond("       = (360*" & Format(De, genFloatFormat) & ")/(365.242191*" &
                    Format(orbElements.getOEObjPeriod(idx), genFloatFormat) & " + " &
                    angleToStr(orbElements.getOEObjLonAtEpoch(idx), DECFORMAT) & " - " &
                    angleToStr(orbElements.getOEObjLonAtPeri(idx), DECFORMAT))
        printlncond("       = " & angleToStr(M_p, DECFORMAT) & " degrees")
        printlncond()

        printlncond("6.  If necessary, adjust Mp so that it falls in the range [0,360]")
        printcond("    Mp = Mp MOD 360 = " & angleToStr(M_p, DECFORMAT) & " MOD 360 = ")
        M_p = xMOD(M_p, 360.0)
        printlncond(angleToStr(M_p, DECFORMAT) & " degrees")
        printlncond()

        ' Find the true anomaly from equation of center or Kepler's equation
        If solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER Then
            Ec_p = (360.0 / PI) * Ecc_p * SIN_D(M_p)
            printlncond("7.  Solve the equation of the center for " & objName & ".")
            printlncond("    Ep = (360/pi) * eccentricity_p * sin(Mp)")
            printlncond("       = (360/3.1415927) * " &
                        Ecc_p & " * sin(" & angleToStr(M_p, DECFORMAT) & ")")
            printlncond("       = " & angleToStr(Ec_p, DECFORMAT) & " degrees")
            printlncond()

            v_p = M_p + Ec_p
            printlncond("8.  Add Ep to Mp to get the true anomaly.")
            printlncond("    vp = Mp + Ep = " & angleToStr(M_p, DECFORMAT) & " + " & angleToStr(Ec_p, DECFORMAT))
            printlncond("       = " & angleToStr(v_p, DECFORMAT) & " degrees")
        Else
            printlncond("7.  Solve Kepler's equation to get the eccentric anomaly.")
            If solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
                Ea_p = calcSimpleKepler(prtObj, M_p, Ecc_p, termCriteria, iter)
            Else
                Ea_p = calcNewtonKepler(prtObj, M_p, Ecc_p, termCriteria, iter)
            End If
            printlncond("    Eap = E" & iter & " = " & angleToStr(Ea_p, DECFORMAT) & " degrees")
            printlncond()

            v_p = (1 + Ecc_p) / (1 - Ecc_p)
            v_p = Sqrt(v_p) * TAN_D(Ea_p / 2.0)
            v_p = 2.0 * INVTAN_D(v_p)
            printlncond("8.  vp = 2 * inv tan[ sqrt[(1+eccentricity_p)/(1-eccentricity_p)] * tan(Eap/2) ]")
            printlncond("       = 2 * inv tan[ sqrt[(1+" & Ecc_p & ")/(1-" & Ecc_p & ")] * tan(" &
                        angleToStr(Ea_p, DECFORMAT) & "/2) ]")
            printlncond("       = " & angleToStr(v_p, DECFORMAT) & " degrees")
        End If
        printlncond()

        ' Calculate the object's heliocentric ecliptic coordinates (L_p, H_p)
        ' and radius vector length (R_p)
        L_p = v_p + orbElements.getOEObjLonAtPeri(idx)
        printlncond("9.  Calculate the object's heliocentric longitude.")
        printlncond("    Lp = vp + Wp = " & angleToStr(v_p, DECFORMAT) & " + " & orbElements.getOEObjLonAtPeri(idx))
        printlncond("       = " & angleToStr(L_p, DECFORMAT) & " degrees")
        printlncond()

        printlncond("10. If necessary, adjust Lp to be in the range [0,360]")
        printcond("    Lp = Lp MOD 360 = " & angleToStr(L_p, DECFORMAT) & " MOD 360 = ")
        L_p = xMOD(L_p, 360.0)
        printlncond(angleToStr(L_p, DECFORMAT) & " degrees")
        printlncond()

        H_p = SIN_D(L_p - orbElements.getOEObjLonAscNode(idx)) * SIN_D(inclin_p)
        H_p = INVSIN_D(H_p)
        printlncond("11. Compute the object's heliocentric latitude.")
        printlncond("    Hp = inv sin[sin(Lp-Omega_p)*sin(inclination_p)]")
        printlncond("       = inv sin[sin(" & angleToStr(L_p, DECFORMAT) & " - " &
                    angleToStr(orbElements.getOEObjLonAscNode(idx), DECFORMAT) & ")*sin(" &
                    angleToStr(inclin_p, DECFORMAT) & ")]")
        printlncond("       = " & angleToStr(H_p, DECFORMAT) & " degrees")
        printlncond()

        printlncond("12. If necessary, adjust Hp to be in the range [0,360]")
        printcond("    Hp = Hp MOD 360 = " & angleToStr(H_p, DECFORMAT) & " MOD 360 = ")
        H_p = xMOD(H_p, 360.0)
        printlncond(angleToStr(H_p, DECFORMAT) & " degrees")
        printlncond()

        R_p = orbElements.getOEObjSemiMajAxisAU(idx) * (1 - Ecc_p * Ecc_p)
        R_p = R_p / (1 + Ecc_p * COS_D(v_p))
        printlncond("13. Compute the object's radius vector length")
        printlncond("    Rp = [a_p*(1-eccentricity_p^2)]/[1+eccentricity_p*cos(vp)]")
        printlncond("       = [" & orbElements.getOEObjSemiMajAxisAU(idx) & "*(1-" & Format(Ecc_p, genFloatFormat) & "^2)]/[1+" &
                    Format(Ecc_p, genFloatFormat) & "*cos(" & angleToStr(v_p, DECFORMAT) & ")]")
        printlncond("       = " & Format(R_p, genFloatFormat) & " AUs")
        printlncond()

        ' Repeat the steps just done for the object for the Earth

        'Calculate the Earth's mean and true anomalies
        M_e = (360 * De) / (365.242191 * orbElements.getOEObjPeriod(idxEarth)) + orbElements.getOEObjLonAtEpoch(idxEarth) -
            orbElements.getOEObjLonAtPeri(idxEarth)
        printlncond("14. Compute the mean anomaly for the Earth")
        printlncond("    Me = (360*De)/(365.242191*Te) + Ee - We")
        printlncond("       = (360*" & Format(De, genFloatFormat) & ")/(365.242191*" &
                    Format(orbElements.getOEObjPeriod(idxEarth), genFloatFormat) & " + " &
                    angleToStr(orbElements.getOEObjLonAtEpoch(idxEarth), DECFORMAT) & " - " &
                    angleToStr(orbElements.getOEObjLonAtPeri(idxEarth), DECFORMAT))
        printlncond("       = " & angleToStr(M_e, DECFORMAT) & " degrees")
        printlncond()

        printlncond("15. If necessary, adjust Me so that it falls in the range [0,360]")
        printcond("    Me = Me MOD 360 = " & angleToStr(M_e, DECFORMAT) & " MOD 360 = ")
        M_e = xMOD(M_e, 360.0)
        printlncond(angleToStr(M_e, DECFORMAT) & " degrees")
        printlncond()

        ' Find the Earth's true anomaly from equation of center or Kepler's equation
        If solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER Then
            Ec_e = (360.0 / PI) * Ecc_e * SIN_D(M_e)
            printlncond("16. Solve the equation of the center for the Earth.")
            printlncond("    Ee = (360/pi) * eccentricity_e * sin(Me) = (360/3.1415927) * " &
                        Ecc_e & " * sin(" & angleToStr(M_e, DECFORMAT) & ")")
            printlncond("       = " & angleToStr(Ec_e, DECFORMAT) & " degrees")
            printlncond()

            v_e = M_e + Ec_e
            printlncond("17. Add Ee to Me to get the Earth's true anomaly.")
            printlncond("    ve = Me + Ee = " & angleToStr(M_e, DECFORMAT) & " + " & angleToStr(Ec_e, DECFORMAT))
            printlncond("       = " & angleToStr(v_e, DECFORMAT) & " degrees")
        Else
            printlncond("16. Solve Kepler's equation to get the Earth's eccentric anomaly.")
            If solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
                Ea_e = calcSimpleKepler(prtObj, M_e, Ecc_e, termCriteria, iter)
            Else
                Ea_e = calcNewtonKepler(prtObj, M_e, Ecc_e, termCriteria, iter)
            End If
            printlncond("    Eae = E" & iter & " = " & angleToStr(Ea_e, DECFORMAT) & " degrees")
            printlncond()

            v_e = (1 + Ecc_e) / (1 - Ecc_e)
            v_e = Sqrt(v_e) * TAN_D(Ea_e / 2.0)
            v_e = 2.0 * INVTAN_D(v_e)
            printlncond("17. ve = 2 * inv tan[ sqrt[(1+eccentricity_e)/(1-eccentricity_e)] * tan(Eae/2) ]")
            printlncond("       = 2 * inv tan[ sqrt[(1+" & Ecc_e & ")/(1-" & Ecc_e & ")] * tan(" &
                        angleToStr(Ea_e, DECFORMAT) & "/2) ]")
            printlncond("       = " & angleToStr(v_e, DECFORMAT) & " degrees")
        End If
        printlncond()

        ' Calculate the Earth's heliocentric ecliptic coordinates (L_e, H_e)
        ' and radius vector length (R_e)
        L_e = v_e + orbElements.getOEObjLonAtPeri(idxEarth)
        printlncond("18. Calculate the Earth's heliocentric longitude.")
        printlncond("    Le = ve + We = " & angleToStr(v_e, DECFORMAT) & " + " & orbElements.getOEObjLonAtPeri(idxEarth))
        printlncond("       = " & angleToStr(L_e, DECFORMAT) & " degrees")
        printlncond()

        printlncond("19. If necessary, adjust Le to be in the range [0,360]")
        printcond("    Le = Le MOD 360 = " & angleToStr(L_e, DECFORMAT) & " MOD 360 = ")
        L_e = xMOD(L_e, 360.0)
        printlncond(angleToStr(L_e, DECFORMAT) & " degrees")
        printlncond()

        H_e = SIN_D(L_e - orbElements.getOEObjLonAscNode(idxEarth)) * SIN_D(inclin_e)
        H_e = INVSIN_D(H_e)
        printlncond("20. Compute the Earth's heliocentric latitude.")
        printlncond("    He = inv sin[sin(Le-Omega_e)*sin(inclination_e)]")
        printlncond("       = inv sin[sin(" & angleToStr(L_e, DECFORMAT) & " - " &
                    angleToStr(orbElements.getOEObjLonAscNode(idxEarth), DECFORMAT) & ")*sin(" &
                    angleToStr(inclin_e, DECFORMAT) & ")]")
        printlncond("       = " & angleToStr(H_e, DECFORMAT) & " degrees")
        printlncond()

        printlncond("21. If necessary, adjust He to be in the range [0,360]")
        printcond("    He = He MOD 360 = " & angleToStr(H_e, DECFORMAT) & " MOD 360 = ")
        H_e = xMOD(H_e, 360.0)
        printlncond(angleToStr(H_e, DECFORMAT) & " degrees")
        printlncond()

        R_e = orbElements.getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e)
        R_e = R_e / (1 + Ecc_e * COS_D(v_e))
        printlncond("22. Compute the Earth's radius vector length")
        printlncond("    Re = [a_e*(1-eccentricity_e^2)]/[1+eccentricity_e*cos(ve)]")
        printlncond("       = [" & orbElements.getOEObjSemiMajAxisAU(idxEarth) & "*(1-" & Format(Ecc_e, genFloatFormat) & "^2)]/[1+" &
                    Format(Ecc_e, genFloatFormat) & "*cos(" & angleToStr(v_e, DECFORMAT) & ")]")
        printlncond("       = " & Format(R_e, genFloatFormat) & " AUs")
        printlncond()

        ' Given the heliocentric location and radius vector length for both the Earth
        ' and the object, project the object's location onto the ecliptic plane
        ' with respect to the Earth

        y = SIN_D(L_p - orbElements.getOEObjLonAscNode(idx)) * COS_D(inclin_p)
        x = COS_D(L_p - orbElements.getOEObjLonAscNode(idx))
        printlncond("23. Calculate an adjustment to the object's ecliptic longitude.")
        printlncond("    y = sin(Lp - Omega_p)*cos(inclination_p)")
        printlncond("      = sin(" & angleToStr(L_p, DECFORMAT) & " - " &
                    angleToStr(orbElements.getOEObjLonAscNode(idx), DECFORMAT) & ")*cos(" & angleToStr(inclin_p, DECFORMAT) & ")")
        printlncond("      = " & Format(y, genFloatFormat))
        printlncond("    x = cos(Lp - Omega_p) = cos(" & angleToStr(L_p, DECFORMAT) & " - " &
                    angleToStr(orbElements.getOEObjLonAscNode(idx), DECFORMAT) & ")")
        printlncond("      = " & Format(x, genFloatFormat))
        printlncond()

        dT = INVTAN_D(y / x)
        printlncond("24. Compute T = inv tan(y/x) = inv tan(" & Format(y, genFloatFormat) & "/" & Format(x, genFloatFormat) &
                    ") = " & angleToStr(dT, DECFORMAT) & " degrees")
        printlncond()

        x = quadAdjust(y, x)
        printlncond("25. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
        printcond("    T = T + Adjustment = " & angleToStr(dT, DECFORMAT) & " + " & angleToStr(x, DECFORMAT) & " = ")
        dT = dT + x
        printlncond(angleToStr(dT, DECFORMAT) & " degrees")
        printlncond()

        Lp_p = orbElements.getOEObjLonAscNode(idx) + dT
        printlncond("26. Finish computing the object's adjustment for the ecliptic longitude.")
        printlncond("    Lp_prime = Omega_p + T = " & angleToStr(orbElements.getOEObjLonAscNode(idx), DECFORMAT) & " + " &
                    angleToStr(dT, DECFORMAT) & " = " & angleToStr(Lp_p, DECFORMAT) & " degrees")
        printlncond()

        printlncond("27. If necessary, adjust Lp_prime to be in the range [0,360]")
        printcond("    Lp_prime = Lp_prime MOD 360 = " & angleToStr(Lp_p, DECFORMAT) & " MOD 360 = ")
        Lp_p = xMOD(Lp_p, 360.0)
        printlncond(angleToStr(Lp_p, DECFORMAT) & " degrees")
        printlncond()

        ' See if this is an inferior or superior object and compute accordingly
        If orbElements.getOEObjInferior(idx) Then
            y = R_p * COS_D(H_p) * SIN_D(L_e - Lp_p)
            x = R_e - R_p * COS_D(H_p) * COS_D(L_e - Lp_p)
            printlncond("28. This is an inferior object, so compute its geocentric ecliptic longitude.")
            printlncond("    y = Rp*cos(Hp)*sin(Le - Lp_prime)")
            printlncond("      = " & Format(R_p, genFloatFormat) & "*cos(" &
                        angleToStr(H_p, DECFORMAT) & ")*sin(" & angleToStr(L_e, DECFORMAT) & " - " &
                        angleToStr(Lp_p, DECFORMAT) & ")")
            printlncond("      = " & Format(y, genFloatFormat))
            printlncond("    x = Re - Rp*cos(Hp)*cos(Le - Lp_prime)")
            printlncond("      = " & Format(R_e, genFloatFormat) & " - " &
                        Format(R_p, genFloatFormat) & "*cos(" & angleToStr(H_p, DECFORMAT) & ")*cos(" &
                        angleToStr(L_e, DECFORMAT) & " - " & angleToStr(Lp_p, DECFORMAT) & ")")
            printlncond("      = " & Format(x, genFloatFormat))
            printlncond()

            dT = INVTAN_D(y / x)
            printlncond("29. Compute T = inv tan(y/x) = inv tan(" & Format(y, genFloatFormat) & "/" & Format(x, genFloatFormat) &
                        ") = " & angleToStr(dT, DECFORMAT) & " degrees")
            printlncond()

            x = quadAdjust(y, x)
            printlncond("30. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
            printcond("    T = T + Adjustment = " & angleToStr(dT, DECFORMAT) & " + " & angleToStr(x, DECFORMAT) & " = ")
            dT = dT + x
            printlncond(angleToStr(dT, DECFORMAT) & " degrees")
            printlncond()

            Lon_p = 180 + L_e + dT
            printlncond("31. Finish computing the inferior object's geocentric ecliptic longitude.")
            printlncond("    Lon_p = 180 + Le + T = 180 + " & angleToStr(L_e, DECFORMAT) & " + " & angleToStr(dT, DECFORMAT) &
                        " = " & angleToStr(Lon_p, DECFORMAT) & " degrees")
            printlncond()

            printlncond("32. If necessary, adjust Lon_p to be in the range [0,360]")
            printcond("    Lon_p = Lon_p MOD 360 = " & angleToStr(Lon_p, DECFORMAT) & " MOD 360 = ")
            Lon_p = xMOD(Lon_p, 360.0)
            printlncond(angleToStr(Lon_p, DECFORMAT) & " degrees")
        Else
            y = R_e * SIN_D(Lp_p - L_e)
            x = R_p * COS_D(H_p) - R_e * COS_D(L_e - Lp_p)
            printlncond("28. This is a superior object, so compute its geocentric ecliptic longitude.")
            printlncond("    y = Re*sin(Lp_prime - Le) = " & Format(R_e, genFloatFormat) & "*sin(" &
                        angleToStr(Lp_p, DECFORMAT) & " - " &
                        angleToStr(L_e, DECFORMAT) & ")")
            printlncond("      = " & Format(y, genFloatFormat))
            printlncond("    x = Rp*cos(Hp) - Re*cos(Le - Lp_prime)")
            printlncond("      = " & Format(R_p, genFloatFormat) & "*cos(" &
                        angleToStr(H_p, DECFORMAT) & ") - " & Format(R_e, genFloatFormat) & "*cos(" &
                        angleToStr(L_e, DECFORMAT) & " - " & angleToStr(Lp_p, DECFORMAT) & ")")
            printlncond("      = " & Format(x, genFloatFormat))
            printlncond()

            dT = INVTAN_D(y / x)
            printlncond("29. Compute T = inv tan(y/x) = inv tan(" & Format(y, genFloatFormat) & "/" & Format(x, genFloatFormat) &
                        ") = " & angleToStr(dT, DECFORMAT) & " degrees")
            printlncond()

            x = quadAdjust(y, x)
            printlncond("30. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
            printcond("    T = T + Adjustment = " & angleToStr(dT, DECFORMAT) & " + " & angleToStr(x, DECFORMAT) & " = ")
            dT = dT + x
            printlncond(angleToStr(dT, DECFORMAT) & " degrees")
            printlncond()

            Lon_p = Lp_p + dT
            printlncond("31. Finish computing the superior object's geocentric ecliptic longitude.")
            printlncond("    Lon_p = Lp_prime + T = " & angleToStr(Lp_p, DECFORMAT) & " + " & angleToStr(dT, DECFORMAT) &
                        " = " & angleToStr(Lon_p, DECFORMAT) & " degrees")
            printlncond()

            printlncond("32. If necessary, adjust Lon_p to be in the range [0,360]")
            printcond("    Lon_p = Lon_p MOD 360 = " & angleToStr(Lon_p, DECFORMAT) & " MOD 360 = ")
            Lon_p = xMOD(Lon_p, 360.0)
            printlncond(angleToStr(Lon_p, DECFORMAT) & " degrees")
        End If
        printlncond()

        Lat_p = R_p * COS_D(H_p) * TAN_D(H_p) * SIN_D(Lon_p - Lp_p)
        Lat_p = Lat_p / (R_e * SIN_D(Lp_p - L_e))
        Lat_p = INVTAN_D(Lat_p)
        printlncond("33. Compute the object's geocentric ecliptic latitude.")
        printlncond("    Lat_p = inv tan[(Rp*cos(Hp)*tan(Hp)*sin(Lon_p-Lp_prime))/(Re*sin(Lp_prime-Le))]")
        printlncond("          = inv tan[(" & Format(R_p, genFloatFormat) & "*cos(" & angleToStr(H_p, DECFORMAT) & ")*tan(" &
                    angleToStr(H_p, DECFORMAT) & ")*sin(" & angleToStr(Lon_p, DECFORMAT) & " - " & angleToStr(Lp_p, DECFORMAT) & "))/")
        printlncond("                    (" & Format(R_e, genFloatFormat) & "*sin(" & angleToStr(Lp_p, DECFORMAT) & " - " &
                    angleToStr(L_e, DECFORMAT) & "))]")
        printlncond("          = " & angleToStr(Lat_p, DECFORMAT) & " degrees")
        printlncond()

        ' All that remains now is to convert (Lat_p,Lon_p) to equatorial and horizon coordinates

        eqCoord = EclipticToEquatorial(Lat_p, Lon_p, orbElements.getOEEpochDate)
        printlncond("34. Convert the object's ecliptic coordinates (Lat_p, Lon_p) to equatorial coordinates.")
        printlncond("    RA = " & timeToStr(eqCoord.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl = " &
                    angleToStr(eqCoord.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        horizonCoord = RADecltoHorizon(eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle,
                                observer.getObsLat, LST)
        printlncond("35. Convert the object's equatorial coordinates to horizon coordinates.")
        printlncond("    Alt = " & angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & ", Az = " &
                    angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT))
        printlncond()

        result = angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & " Alt, " &
            angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT) & " Az"
        printlncond("Thus, for this observer location and date/time, " & objName)
        printlncond("is at " & result & ".")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = objName & " is located at " & result
    End Sub

    '------------------------------------------------------------
    ' Calculate the times that an object will rise and set.
    '------------------------------------------------------------
    Private Sub calcObjRiseSet()
        Dim objName, result As String
        Dim idx, dateAdjust As Integer
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim eclCoord, eqCoord As ASTCoord
        Dim LSTTimes(1) As Double
        Dim L_p, R_p, L_e, R_e As Double
        Dim LSTr, LSTs, LCTr, LCTs, UT, GST As Double
        Dim riseSet As Boolean = True

        clearTextAreas()

        idx = getValidObj(Not GETSUN, Not GETEARTH, Not GETMOON)
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        prt.setBoldFont(True)
        printlncond("Calculate when " & objName & " will rise and set for the Current Observer", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                       observer.getObsDate.getYear, 0.0, idx, L_p, R_p, L_e, R_e,
                                                       solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the ecliptic location for " & objName & " at midnight for the date in question.")
        printlncond("    For UT=0 hours on the date " &
                    dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear))
        printlncond("    " & objName & " has eclipitic coordinates Lat_p = " &
                    angleToStr(eclCoord.getLatAngle.getDecAngle, DECFORMAT) &
                    " degrees, Lon_p = " & angleToStr(eclCoord.getLonAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        eqCoord = EclipticToEquatorial(eclCoord.getLatAngle.getDecAngle, eclCoord.getLonAngle.getDecAngle,
                                       orbElements.getOEEpochDate)
        printlncond("2.  Convert the object's geocentric ecliptic coordinates to equatorial coordinates.")
        printlncond("    RA = " & timeToStr(eqCoord.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl = " &
                    angleToStr(eqCoord.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        LSTTimes = calcRiseSetTimes(eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle, observer.getObsLat, riseSet)
        LSTr = LSTTimes(0)
        LSTs = LSTTimes(1)
        printlncond("3.  Using these equatorial coordinates, compute the LST rising and setting times.")
        If riseSet Then
            printlncond("    LSTr = " & timeToStr(LSTr, DECFORMAT) & " hours")
            printlncond("    LSTs = " & timeToStr(LSTs, DECFORMAT) & " hours")
            printlncond()

            GST = LSTtoGST(LSTr, observer.getObsLon)
            UT = GSTtoUT(GST, observer.getObsDate)
            LCTr = UTtoLCT(UT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)

            GST = LSTtoGST(LSTs, observer.getObsLon)
            UT = GSTtoUT(GST, observer.getObsDate)
            LCTs = UTtoLCT(UT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)

            printlncond("4.  Convert the LST rising/setting times to their corresponding LCT times.")
            printlncond("    LCTr = " & timeToStr(LCTr, DECFORMAT) & " hours, LCTs = " &
                        timeToStr(LCTs, DECFORMAT) & " hours")
            printlncond()

            printlncond("5. Convert the LCT times to HMS format.")
            printlncond("   LCTr = " & timeToStr(LCTr, HMSFORMAT) & ", LCTs = " & timeToStr(LCTs, HMSFORMAT))
            printlncond()

            printlncond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                    observer.getObsDate.getYear) & " for the current observer")
            result = objName & " will rise at " & timeToStr(LCTr, HMSFORMAT) & " LCT and set at " & timeToStr(LCTs, HMSFORMAT) & " LCT"
        Else
            printlncond()
            result = objName & " does not rise or set for this observer."
        End If
        printlncond(result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------------------
    ' Calculate when a planet, including the Earth, will pass
    ' through perihelion and aphelieon.
    '------------------------------------------------------------
    Private Sub calcPlanetPeriAphelion()
        Dim objName, result As String
        Dim idx, iTab, iKper, iTmp As Integer
        Dim dK, dK0, dK1, dJ0, dJ1, dJ2, dKaph, JDper, JDaph As Double
        Dim UTperi, UTaph As Double
        Dim iDays As Integer
        Dim periDate, aphDate As ASTDate
        Dim dY As Double

        clearTextAreas()

        ' This only works for the planets, so make sure the user specifies a planet
        ' The orbital elements file could have non-planets in it (e.g, Pluto, Ceres)
        idx = getValidPlanet()
        If idx < 0 Then Return

        objName = orbElements.getOEObjName(idx)

        ' Find the index into the planetsTable for the requested planet
        ' and the coefficients
        For iTab = 0 To NUM_PLANETS - 1
            If String.Compare(planetsTable(iTab, 0).ToString, objName, True) = 0 Then Exit For
        Next
        dK0 = CDbl(planetsTable(iTab, 1))
        dK1 = CDbl(planetsTable(iTab, 2))
        dJ0 = CDbl(planetsTable(iTab, 3))
        dJ1 = CDbl(planetsTable(iTab, 4))
        dJ2 = CDbl(planetsTable(iTab, 5))

        prt.setBoldFont(True)
        printlncond("Determine when " & objName & " will pass through Perihelion/Aphelion", CENTERTXT)
        printlncond("closest to the date " & dateToStr(observer.getObsDate()), CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        iDays = daysIntoYear(observer.getObsDate.getMonth(), observer.getObsDate.getiDay(), observer.getObsDate.getYear())
        printlncond("1.  Compute the number of days into the year for the given date.")
        printlncond("    Days = " & iDays)
        printlncond()

        dY = observer.getObsDate.getYear() + (iDays / 365.25)
        printlncond("2.  Compute Y = Year + (Days/365.25) = " & observer.getObsDate.getYear() & " + (" &
                    iDays & "/365.25) = " & Format(dY, genFloatFormat))
        printlncond()

        dK = dK0 * (dY - dK1)
        printlncond("3.  Using the appropriate 'k' coefficients for " & objName & ",")
        printlncond("    Compute K = k0*(Y - k1) = " & dK0 & "*(" &
                    Format(dY, genFloatFormat) & " - (" & dK1 & "))")
        printlncond("              = " & Format(dK, genFloatFormat))
        printlncond()

        iKper = Trunc(Abs(dK) + 0.5)
        If dK < 0 Then iKper = -iKper
        iTmp = Sign(dK - iKper)
        If iTmp = 0 Then iTmp = 1
        dKaph = iKper + 0.5 * iTmp
        printlncond("4.  Let Kper be the integer value closest to K and let Kaph")
        printlncond("    be the fraction ending in 0.5 that is closest to K.")
        printlncond("    Kper = " & iKper & ", Kaph = " & Format(dKaph, genFloatFormat))
        printlncond()

        JDper = dJ0 + dJ1 * iKper + dJ2 * iKper * iKper
        JDaph = dJ0 + dJ1 * dKaph + dJ2 * dKaph * dKaph
        printlncond("5.  Compute JDper = j0 + j1*Kper + j2*Kper^2. This is the Julian day number for perihelion.")
        printlncond("                  = " & dJ0 & " + (" & dJ1 & ")*(" & iKper & ") + (" & dJ2 & ")*(" & iKper & ")^2")
        printlncond("                  = " & Format(JDper, JDFormat))
        printlncond()

        printlncond("6.  Compute JDaph = j0 + j1*Kaph + j2*Kaph^2. This is the Julian day number for aphelion.")
        printlncond("                  = " & dJ0 & " + (" & dJ1 & ")*(" & dKaph & ") + (" & dJ2 & ")*(" & dKaph & ")^2")
        printlncond("                  = " & Format(JDaph, JDFormat))
        printlncond()

        periDate = JDtoDate(JDper)
        aphDate = JDtoDate(JDaph)
        printlncond("7.  Convert JDper and JDaph to calendar dates.")
        printlncond("    JDper gives " & periDate.getMonth & "/" & Format(periDate.getdDay, genFloatFormat) &
                    "/" & periDate.getYear)
        printlncond("    JDaph gives " & aphDate.getMonth & "/" & Format(aphDate.getdDay, genFloatFormat) &
                    "/" & aphDate.getYear)
        printlncond()

        UTperi = Frac(periDate.getdDay) * 24.0
        UTaph = Frac(aphDate.getdDay) * 24.0
        printlncond("8.  Convert the fractional part of the days to a decimal UT.")
        printlncond("    Perihelion: day " & periDate.getiDay & ", " & timeToStr(UTperi, DECFORMAT) & " hours")
        printlncond("    Aphelion: day " & aphDate.getiDay & ", " & timeToStr(UTaph, DECFORMAT) & " hours")
        printlncond()

        result = objName & " will pass through "
        printlncond("9.  Convert the UT results to HMS format.")
        prt.println("    " & result & "Perihelion on " & dateToStr(periDate) & " at " & timeToStr(UTperi, HMSFORMAT) & " UT")
        prt.println("    " & result & "Aphelion on " & dateToStr(aphDate) & " at " & timeToStr(UTaph, HMSFORMAT) & " UT")

        lblResults.Text = result & "Perihelion/Aphelion as listed in the text area below"
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------------------
    ' Set the termination criteria for solving Kepler's equation
    '------------------------------------------------------------
    Private Sub setTerminationCriteria()
        Dim dTerm As Double = termCriteria

        clearTextAreas()
        prt.setBoldFont(True)
        prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        If queryForm.showQueryForm("Enter Termination Criteria in radians" & vbNewLine &
                                   "(ex: 0.000002)") <> Windows.Forms.DialogResult.OK Then
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        ' Validate the termination criteria
        If Not isValidReal(queryForm.getData1(), dTerm, HIDE_ERRORS) Then
            ErrMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        prt.println("Prior termination criteria was: " & termCriteria & " radians")
        If dTerm < KeplerMinCriteria Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too small, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        If dTerm > 1 Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too large, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        termCriteria = dTerm

        prt.println("Termination criteria is now set to " & termCriteria & " radians")
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Data Files menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Finds the constellation that an object is currently in.
    '------------------------------------------------------------
    Private Sub findConstellationForObj()
        Dim objName As String
        Dim eclCoord As ASTCoord
        Dim RA, Decl As Double
        Dim idxObj, idxConst As Integer
        Dim result As String = ""

        clearTextAreas()

        idxObj = getValidObj(GETSUN, Not GETEARTH, GETMOON)
        If idxObj < 0 Then Return

        objName = orbElements.getOEObjName(idxObj)

        eclCoord = getSolarSysEqCoord(idxObj)
        RA = eclCoord.getRAAngle.getDecTime()
        Decl = eclCoord.getDeclAngle.getDecAngle()

        idxConst = findConstellationFromCoord(RA, Decl, DEFAULT_EPOCH)

        If idxConst < 0 Then
            CriticalErrMsg("Could not determine a constellation for the location entered.")
        Else
            If (idxObj = idxSun) Or (idxObj = idxMoon) Then result = "The "
            result = result & objName & " at " & timeToStr(RA, HMSFORMAT) & " RA, " & angleToStr(Decl, DMSFORMAT) &
                " Decl is in the " & getConstName(idxConst) & " (" & getConstAbbrevName(idxConst) & ") constellation"
            lblResults.Text = result
        End If
    End Sub

    '------------------------------------------------------------
    ' Finds the constellation that a given RA/Decl falls within
    '------------------------------------------------------------
    Private Sub findConstellationForRA_Decl()
        Dim raObj As ASTTime = New ASTTime
        Dim declObj As ASTAngle = New ASTAngle
        Dim idx As Integer
        Dim result As String

        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)" & vbNewLine & "for Epoch 2000.0",
                                   "Enter Declination (xxxd yym zz.zzs)" & vbNewLine & "for Epoch 2000.0") =
                                    Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If

            clearTextAreas()
            idx = findConstellationFromCoord(raObj.getDecTime(), declObj.getDecAngle(), DEFAULT_EPOCH)

            If idx < 0 Then
                CriticalErrMsg("Could not determine a constellation for the location entered.")
            Else
                result = "Location " & timeToStr(raObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) &
                    " Decl is in the " & getConstName(idx) & " (" & getConstAbbrevName(idx) & ") constellation"
                lblResults.Text = result
            End If
        End If
    End Sub

    '----------------------------------------------
    ' Displays a list of all of the constellations.
    '----------------------------------------------
    Private Sub listAllConstellations()
        clearTextAreas()
        prt.setFixedWidthFont()
        displayAllConstellations()
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------------------------
    ' Shows all catalog information, including space objects,
    ' in the currently loaded catalog.
    '---------------------------------------------------------
    Private Sub listAllObjsInCatalog()
        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        clearTextAreas()
        displayCatalogInfo()
        prt.println()
        prt.setBoldFont(True)
        prt.println("Only Those Objects at Least as Bright as mV = " & Format(currentmVFilter, mVFormat) &
                    " are listed", CENTERTXT)
        prt.setBoldFont(False)
        prt.setFixedWidthFont()
        prt.println(String.Format("{0,82:s}", "*").Replace(" ", "*"))
        displayAllCatalogObjects(currentmVFilter)
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------------------------
    ' Shows all space objects in the currently loaded catalog
    ' that fall within a user-specified constellation. The
    ' catalog must already be grouped by constellation.
    '---------------------------------------------------------
    Private Sub listAllObjsInConstellation()
        Dim idx As Integer
        Dim constAbbrevName As String

        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
            Return
        End If

        If queryForm.showQueryForm("Enter Constellation's 3 Character" & vbNewLine &
                           "Abbreviated Name") = Windows.Forms.DialogResult.OK Then
            clearTextAreas()
            constAbbrevName = queryForm.getData1()
            idx = findConstellationByAbbrvName(constAbbrevName.Trim())
            If idx < 0 Then
                prt.println("No Constellation whose abbreviated name is '" & constAbbrevName & "' was found")
            Else
                prt.setBoldFont(True)
                prt.println("Only Those Objects at Least as Bright as mV = " & Format(currentmVFilter, mVFormat) &
                            " are listed", CENTERTXT)
                prt.setBoldFont(False)
                prt.setFixedWidthFont()
                displayAllObjsByConstellation(idx, ASCENDING_ORDER, currentmVFilter)
                prt.setProportionalFont()
            End If
            prt.resetCursor()
        End If

    End Sub

    '------------------------------------------------
    ' Loads a star catalog from disk
    '------------------------------------------------
    Private Sub loadCatalog()
        Dim fullFilename As String = " "

        clearTextAreas()

        If isCatalogLoaded() Then
            If PleaseConfirm("A catalog is already loaded. Are you" & vbNewLine &
                              "sure you want to load a new one?", "Clear Catalog Data") Then
                clearCatalogAndSpaceObjects()
                setFilename("")
                setCatalogType("")
                setEpoch(DEFAULT_EPOCH)
                prt.println("The currently loaded star catalog was cleared ...")
            Else
                prt.println("The currently loaded star catalog was not cleared ...")
                Return
            End If
        End If

        If Not getCatFileToOpen(fullFilename) Then Return

        setFilename(fullFilename)
        clearTextAreas()
        If loadFormattedStarCatalog(fullFilename) Then
            prt.println("Read in " & getCatNumConst() & " different Constellations with a total of " &
            getCatNumObjs() & " Objects")
            setCatalogType(getCatType())
            setEpoch(getCatEpoch())
        Else
            ErrMsg("Could not load the catalog data from " & fullFilename, "Catalog Load Failed")
            setFilename("")
            setCatalogType("")
            setEpoch(DEFAULT_EPOCH)
        End If
        prt.resetCursor()
    End Sub

    '------------------------------------------------
    ' Load orbital elements from disk
    '------------------------------------------------
    Private Sub loadOrbitalElements()
        Dim fullFilename As String = " "

        clearTextAreas()

        If checkOEDBLoaded(HIDE_ERRORS) Then
            If PleaseConfirm("Orbital Elements have already been loaded. Are you" & vbNewLine &
                              "sure you want to load a new set of orbital elements?", "Clear Orbital Elements Data") Then
                setEpoch(DEFAULT_EPOCH)
                prt.println("The currently loaded orbital elements were cleared ...")
                idxEarth = -1
                idxMoon = -1
                idxSun = -1
            Else
                prt.println("The currently loaded orbital elements were not cleared ...")
                Return
            End If
        End If

        If Not getOEDBFileToOpen(fullFilename) Then Return

        If orbElements.loadOEDB(fullFilename) Then
            prt.println("New orbital elements have been successfully loaded ...")
            setEpoch(orbElements.getOEEpochDate)
        End If

        idxEarth = orbElements.getOEDBEarthIndex()
        idxMoon = orbElements.getOEDBMoonIndex()
        idxSun = orbElements.getOEDBSunIndex()

        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Shows all of the orbital elements and other data from whatever
    ' orbital elements database is currently loaded.
    '-----------------------------------------------------------------
    Private Sub showAllOrbitalElements()
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()
        orbElements.displayAllOrbitalElements()
        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Shows the orbital elements and other data for an object from 
    ' whatever orbital elements database is currently loaded.
    '-----------------------------------------------------------------
    Private Sub showObjOrbitalElements()
        Dim idx As Integer
        Dim objName As String

        If Not checkOEDBLoaded() Then Return

        If queryForm.showQueryForm("Enter Object's Name") = Windows.Forms.DialogResult.OK Then
            ' validate data
            objName = queryForm.getData1()
            idx = orbElements.findOrbElementObjIndex(objName.Trim())
            If idx < 0 Then
                ErrMsg("No Object with the name '" & objName & "' was found",
                       "Invalid Object Name")
                Return
            End If
        Else
            Return
        End If

        clearTextAreas()
        orbElements.displayObjOrbElements(idx)
        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Shows the catalog information from the currently loaded catalog.
    '-----------------------------------------------------------------
    Private Sub showCatalogInfo()
        If Not isCatalogLoaded() Then
            ErrMsg("No catalog is currently loaded.", "No Catalog Loaded")
        Else
            clearTextAreas()
            displayCatalogInfo()
            prt.resetCursor()
        End If
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Star Charts menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------
    ' Ask user for a filter and update the GUI
    '------------------------------------------
    Private Sub changemVFilter()
        Dim newmVFilter As Double

        clearTextAreas()

        If queryForm.showQueryForm("Enter desired visual magnitude filter") = Windows.Forms.DialogResult.OK Then
            ' validate data
            If isValidReal(queryForm.getData1(), newmVFilter, HIDE_ERRORS) Then
                setmVFilter(newmVFilter)
            Else
                ErrMsg("The mV Filter entered is invalid - try again.", "Invalid mV Filter")
            End If
        End If
    End Sub

    '--------------------------------------------------
    ' Draw all of the objects in the orbital elements
    ' file, except for the Earth
    '--------------------------------------------------
    Private Sub drawAllSolarSysObjs()
        Dim idx As Integer
        Dim objName As String
        Dim RA, Decl, mV As Double
        Dim LST As Double
        Dim horizonCoord As ASTCoord = New ASTCoord
        Dim eqCoord As ASTCoord
        Dim plotColor As Brush

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        ' Cycle through all the objects in the orbital elements data file
        For idx = 0 To orbElements.getNumOEDBObjs() - 1
            ' Skip the Earth
            If idx = idxEarth Then Continue For

            objName = orbElements.getOEObjName(idx)
            eqCoord = getSolarSysEqCoord(idx)
            RA = eqCoord.getRAAngle.getDecTime()
            Decl = eqCoord.getDeclAngle.getDecAngle()
            mV = orbElements.getOEObjmV(idx)
            If (idx = idxSun) Then
                plotColor = SUN_COLOR
            ElseIf (idx = idxMoon) Then
                plotColor = MOON_COLOR
            Else
                plotColor = PLANET_COLOR
            End If

            If chkboxEQChart.Checked Then
                starChart.plotRADecl(RA, Decl, mV, plotColor)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelRADecl(objName, RA, Decl, plotColor)
                End If
            Else
                horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                        horizonCoord.getAzAngle.getDecAngle, mV, plotColor)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz(objName, horizonCoord.getAltAngle.getDecAngle,
                                             horizonCoord.getAzAngle.getDecAngle, plotColor)
                End If
            End If
        Next

        lblResults.Text = " ... all done plotting Solar System objects ..."
    End Sub

    '--------------------------------------------------
    ' Draw all of the stars in the catalog that are at
    ' least as bright as the mV filter.
    '--------------------------------------------------
    Private Sub drawAllStars()
        Dim i, iConst, plotLimit, plotCount As Integer
        Dim RA, Decl, mV As Double
        Dim LST As Double
        Dim showObj As Boolean
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        chartOnScreen = True

        LST = computeLST()

        starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)

        ' See if we need to bother about pausing during plotting
        plotLimit = OBJS_LIMIT + 10          ' this really means to ignore pausing during display
        If getCatNumObjs() > OBJS_LIMIT Then
            If AbortMsg("Plotting " & getCatNumObjs() & " objects may take a long time.") Then Return

            If queryForm.showQueryForm("Enter Max # of Objs to Plot at a Time" & vbNewLine &
                             "(Recommend no More Than " & MAX_OBJS_TO_PLOT & " Objs)") = Windows.Forms.DialogResult.OK Then
                If Not isValidInt(queryForm.getData1(), plotLimit, HIDE_ERRORS) Then plotLimit = MAX_OBJS_TO_PLOT
            End If
        End If

        iConst = -1         ' set a bogus constellation
        plotCount = 0
        For i = 0 To getCatNumObjs() - 1

            plotCount = plotCount + 1
            If plotCount > plotLimit Then
                If AbortMsg("Plotted " & i & " of " & getCatNumObjs() & " objects") Then Exit For
                plotCount = 0
            End If

            RA = getCatObjRA(i)
            Decl = getCatObjDecl(i)
            If getCatObjConstIdx(i) <> iConst Then
                iConst = getCatObjConstIdx(i)
                lblResults.Text = "Plotting constellation " & getConstName(iConst)
                lblResults.Update()     ' force to update immediately so that the label is seen
            End If

            ' See if this object should be filtered out
            If getCatObjmVKnown(i) Then
                showObj = (getCatObjmV(i) <= currentmVFilter)
                mV = getCatObjmV(i)
            Else
                showObj = True      ' show object even if we don't know its mV value
                mV = mV_NAKED_EYE   ' default to what the naked eye can see
            End If

            If showObj Then
                If chkboxEQChart.Checked Then
                    starChart.plotRADecl(RA, Decl, mV)
                Else
                    horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                    starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                        horizonCoord.getAzAngle.getDecAngle, mV)
                End If
            End If
        Next

        lblResults.Text = " ... all done plotting ..."
    End Sub

    '--------------------------------------------------------------
    ' Draw all of the stars in the catalog within a user specified
    ' constellation that are at least as bright as the mV filter.
    ' Also draw the brightest star in the constellation.
    '--------------------------------------------------------------
    Private Sub drawAllStarsInConst()
        Dim i, iConst As Integer
        Dim RA, brightRA, Decl, brightDecl, mV As Double
        Dim LST As Double
        Dim showObj As Boolean
        Dim constAbbrevName As String
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        If queryForm.showQueryForm("Enter Constellation's 3 Character" & vbNewLine &
                                   "Abbreviated Name") = Windows.Forms.DialogResult.OK Then
            constAbbrevName = queryForm.getData1()
            iConst = findConstellationByAbbrvName(constAbbrevName.Trim())
            If iConst < 0 Then
                ErrMsg("No Constellation whose abbreviated name is '" & constAbbrevName & "' was found",
                       "Invalid Constellation")
                Return
            End If
        Else
            Return
        End If

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        lblResults.Text = "Plotting constellation " & getConstName(iConst)
        lblResults.Update()     ' force to update immediately so that the label is seen

        brightRA = getConstBrightestStarRA(iConst)
        brightDecl = getConstBrightestStarDecl(iConst)

        For i = 0 To getCatNumObjs() - 1
            RA = getCatObjRA(i)
            Decl = getCatObjDecl(i)
            If getCatObjConstIdx(i) = iConst Then
                ' See if this object should be filtered out
                If getCatObjmVKnown(i) Then
                    showObj = (getCatObjmV(i) <= currentmVFilter)
                    mV = getCatObjmV(i)
                Else
                    showObj = True      ' show object even if we don't know its mV value
                    mV = mV_NAKED_EYE   ' default to what the naked eye can see
                End If

                If showObj Then
                    If chkboxEQChart.Checked Then
                        starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
                    Else
                        horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                        starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                            horizonCoord.getAzAngle.getDecAngle, mV, HIGHLIGHT_COLOR)
                    End If
                End If
            End If
        Next

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(brightRA, brightDecl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelRADecl(getConstBrightestStarName(iConst), brightRA, brightDecl, BRIGHTEST_OBJ_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(brightRA, brightDecl, observer.getObsLat, LST)
            starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                horizonCoord.getAzAngle.getDecAngle, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelAltAz(getConstBrightestStarName(iConst),
                                         horizonCoord.getAltAngle.getDecAngle, horizonCoord.getAzAngle.getDecAngle,
                                         BRIGHTEST_OBJ_COLOR)
            End If
        End If

        lblResults.Text = "Brightest star in " & getConstName(iConst) &
                            " is " & getConstBrightestStarName(iConst)
    End Sub

    '--------------------------------------------------------------
    ' Draw the brightest star in each of the constellations.
    '--------------------------------------------------------------
    Private Sub drawBrightestStarInAllConst()
        Dim i As Integer
        Dim RA, Decl As Double
        Dim LST As Double
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        If chkboxLabelObjs.Checked Then
            If Not PleaseConfirm("Labelling the stars may make the display   " & vbNewLine &
                                 "unreadable. Do you wish to continue anyway?", " ") Then Return
        End If

        For i = 0 To getNumConstellations() - 1
            lblResults.Text = "Plotting brightest star in constellation " & getConstName(i)

            RA = getConstBrightestStarRA(i)
            Decl = getConstBrightestStarDecl(i)

            If chkboxEQChart.Checked Then
                starChart.plotRADecl(RA, Decl, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelRADecl(getConstBrightestStarName(i), RA, Decl, BRIGHTEST_OBJ_COLOR)
                End If
            Else
                horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, BRIGHT_mV, BRIGHTEST_OBJ_COLOR)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz(getConstBrightestStarName(i),
                                             horizonCoord.getAltAngle.getDecAngle, horizonCoord.getAzAngle.getDecAngle,
                                             BRIGHTEST_OBJ_COLOR)
                End If
            End If
        Next

        lblResults.Text = "Finished plotting brightest stars in each constellation ..."
    End Sub

    '--------------------------------------------------
    ' Draw an object in the orbital elements
    ' file, except for the Earth
    '--------------------------------------------------
    Private Sub drawSolarSysObj()
        Dim idx As Integer
        Dim objName, result As String
        Dim RA, Decl, mV As Double
        Dim LST As Double
        Dim horizonCoord As ASTCoord = New ASTCoord
        Dim eqCoord As ASTCoord
        Dim plotColor As Brush

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        idx = getValidObj(GETSUN, Not GETEARTH, GETMOON)
        If idx < 0 Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        objName = orbElements.getOEObjName(idx)
        eqCoord = getSolarSysEqCoord(idx)
        RA = eqCoord.getRAAngle.getDecTime()
        Decl = eqCoord.getDeclAngle.getDecAngle()
        mV = orbElements.getOEObjmV(idx)
        If (idx = idxSun) Then
            plotColor = SUN_COLOR
        ElseIf (idx = idxMoon) Then
            plotColor = MOON_COLOR
        Else
            plotColor = PLANET_COLOR
        End If

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(RA, Decl, mV, plotColor)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelRADecl(objName, RA, Decl, plotColor)
            End If
        Else
            horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle >= 0 Then
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, mV, plotColor)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz(objName, horizonCoord.getAltAngle.getDecAngle,
                                             horizonCoord.getAzAngle.getDecAngle, plotColor)
                End If
            Else
                InfoMsg("The object '" & objName & "' is below the observer's horizon")
            End If
        End If

        result = ""
        If (idx = idxSun) Or (idx = idxMoon) Then result = "The "
        lblResults.Text = result & objName & " is at " & timeToStr(RA, HMSFORMAT) & " RA, " & angleToStr(Decl, DMSFORMAT) & " Decl"

    End Sub

    '--------------------------------------------------------------
    ' Locate a horizon coordinate that the user specifies
    '--------------------------------------------------------------
    Private Sub locateAltAz()
        Dim LST As Double
        Dim altObj As ASTAngle = New ASTAngle
        Dim azObj As ASTAngle = New ASTAngle
        Dim eqCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        If queryForm.showQueryForm("Enter Altitude (xxxd yym zz.zzs)", "Enter Azimuth (xxxd yym zz.zzs)") =
                                 Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidAngle(queryForm.getData1(), altObj, HIDE_ERRORS) Then
                ErrMsg("The Altitude entered is invalid - try again.", "Invalid Altitude")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), azObj, HIDE_ERRORS) Then
                ErrMsg("The Azimuth entered is invalid - try again.", "Invalid Azimuth")
                Return
            End If
        Else
            Return
        End If

        If chkboxEQChart.Checked Then
            eqCoord = HorizontoRADecl(altObj.getDecAngle, azObj.getDecAngle, observer.getObsLat, LST)
            starChart.plotRADecl(eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelRADecl("LOC", eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle, LOCATE_OBJ_COLOR)
            End If
        Else
            If altObj.getDecAngle < 0 Then
                InfoMsg("The stated coordinate is below the observer's horizon")
            Else
                starChart.plotAltAz(altObj.getDecAngle, azObj.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz("LOC", altObj.getDecAngle, azObj.getDecAngle, LOCATE_OBJ_COLOR)
                End If
            End If
        End If

        lblResults.Text = angleToStr(altObj.getDecAngle, DMSFORMAT) & " Alt, " & angleToStr(azObj.getDecAngle, DMSFORMAT) & " Az"
    End Sub

    '--------------------------------------------------------------
    ' Locate and highlight the brightest object in the catalog
    '--------------------------------------------------------------
    Private Sub locateBrightestObjInCat()
        Dim i, idx As Integer
        Dim LST As Double
        Dim RA, Decl, mV As Double
        Dim brightestmV = UNKNOWN_mV
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        If Not getCatmVProvided() Then
            InfoMsg("The currently loaded catalog does not provide magnitudes...")
            Return
        End If

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        ' Cycle through and find brightest object
        For i = 0 To getCatNumObjs() - 1
            mV = getCatObjmV(i)
            If mV < brightestmV Then
                brightestmV = mV
                idx = i
            End If
        Next

        RA = getCatObjRA(idx)
        Decl = getCatObjDecl(idx)
        mV = getCatObjmV(idx)

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelRADecl(getCatObjName(idx), RA, Decl, HIGHLIGHT_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle < 0 Then
                InfoMsg("The object '" & getCatObjName(idx) & "' is below the observer's horizon")
            Else
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, mV, HIGHLIGHT_COLOR)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz(getCatObjName(idx),
                                             horizonCoord.getAltAngle.getDecAngle,
                                             horizonCoord.getAzAngle.getDecAngle, HIGHLIGHT_COLOR)
                End If
            End If
        End If

        lblResults.Text = "Brightest Object is '" & getCatObjName(idx) & "' at " & timeToStr(RA, HMSFORMAT) &
            " RA, " & angleToStr(Decl, DMSFORMAT) & " Decl, " & Format(mV, mVFormat) & " mV"
    End Sub

    '--------------------------------------------------------------
    ' Locate an equatorial coordinate that the user specifies
    '--------------------------------------------------------------
    Private Sub locateRADecl()
        Dim raObj As ASTTime = New ASTTime
        Dim LST As Double
        Dim declObj As ASTAngle = New ASTAngle
        Dim horizonCoord As ASTCoord = New ASTCoord

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        If queryForm.showQueryForm("Enter Right Ascension (hh:mm:ss.ss)", "Enter Declination (xxxd yym zz.zzs)") =
                         Windows.Forms.DialogResult.OK Then
            ' validate data
            If Not isValidTime(queryForm.getData1(), raObj, HIDE_ERRORS) Then
                ErrMsg("The RA entered is invalid - try again.", "Invalid RA")
                Return
            End If
            If Not isValidAngle(queryForm.getData2(), declObj, HIDE_ERRORS) Then
                ErrMsg("The Declination entered is invalid - try again.", "Invalid Decl")
                Return
            End If
        Else
            Return
        End If

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(raObj.getDecTime, declObj.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelRADecl("LOC", raObj.getDecTime, declObj.getDecAngle, LOCATE_OBJ_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(raObj.getDecTime, declObj.getDecAngle, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle < 0 Then
                InfoMsg("The stated coordinate is below the observer's horizon")
            Else
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, LOCATE_mV, LOCATE_OBJ_COLOR)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz("LOC", horizonCoord.getAltAngle.getDecAngle,
                                             horizonCoord.getAzAngle.getDecAngle, LOCATE_OBJ_COLOR)
                End If
            End If
        End If

        lblResults.Text = timeToStr(raObj.getDecTime, HMSFORMAT) & " RA, " &
            angleToStr(declObj.getDecAngle, DMSFORMAT) & " Decl"
    End Sub

    '--------------------------------------------------------------
    ' Locate and highlight a user-specified object from the catalog
    '--------------------------------------------------------------
    Private Sub locateStarCatalogObj()
        Dim idx As Integer
        Dim LST As Double
        Dim RA, Decl, mV As Double
        Dim searchStr As String
        Dim horizonCoord As ASTCoord = New ASTCoord

        If Not isCatalogLoaded() Then
            ErrMsg("Warning - No catalog is currently loaded.", "No Catalog Loaded")
            Return      ' No need to go any further since there is nothing to plot
        End If

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter the Object's Name") = Windows.Forms.DialogResult.OK Then
            searchStr = queryForm.getData1()
            idx = findObjByName(searchStr.Trim())
            If idx < 0 Then
                ErrMsg("No Object with the name '" & searchStr & "' was found in the catalog",
                       "Invalid Object Name")
                Return
            End If
        Else
            Return
        End If

        LST = computeLST()

        ' If there's already a plot on the screen, don't erase it
        If Not chartOnScreen Then
            starChart.initDrawingCanvas(chkboxWhiteCharts.Checked, chkboxEQChart.Checked)
        End If

        chartOnScreen = True

        RA = getCatObjRA(idx)
        Decl = getCatObjDecl(idx)

        ' Show the object regardless of whether it is within the mV filter
        If getCatObjmVKnown(idx) Then
            mV = getCatObjmV(idx)
        Else
            mV = mV_NAKED_EYE   ' default to what the naked eye can see
        End If

        If chkboxEQChart.Checked Then
            starChart.plotRADecl(RA, Decl, mV, HIGHLIGHT_COLOR)
            If chkboxLabelObjs.Checked Then
                starChart.drawLabelRADecl(searchStr, RA, Decl, HIGHLIGHT_COLOR)
            End If
        Else
            horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
            If horizonCoord.getAltAngle.getDecAngle < 0 Then
                InfoMsg("The object '" & searchStr & "' is below the observer's horizon")
            Else
                starChart.plotAltAz(horizonCoord.getAltAngle.getDecAngle,
                                    horizonCoord.getAzAngle.getDecAngle, mV, HIGHLIGHT_COLOR)
                If chkboxLabelObjs.Checked Then
                    starChart.drawLabelAltAz(searchStr,
                                             horizonCoord.getAltAngle.getDecAngle, horizonCoord.getAzAngle.getDecAngle,
                                             HIGHLIGHT_COLOR)
                End If
            End If
        End If

        lblResults.Text = "The object '" & searchStr & "' is at " & timeToStr(RA, HMSFORMAT) & " RA, " &
            angleToStr(Decl, DMSFORMAT) & " Decl"
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Calculate miscellaneous data items about an object. This
    ' method is intended to be called twice: 1st to show how the
    ' calculations are done (if user wants to see intermediate
    ' results) and then to do a summary. It is done this way
    ' because mixing the summary and details makes for a
    ' messy combination of summary and details for the user
    ' to weed through.
    '
    ' idx               which object is of interest
    ' summary           true if this is the summary pass
    '------------------------------------------------------------
    Private Sub calcMiscData(ByVal idx As Integer, ByVal summary As Boolean)
        Dim objName As String
        Dim iTmp As Integer
        Dim m_p, r_p, m_e, r_e, dDist, dT, L_p, L_e, dTmp, Ecc_p, a_p, Tp As Double
        Dim RVL_p, RVL_e, RVL_x As Double              ' radius vector length for object and Earth
        Dim eclCoord As ASTCoord
        Dim solveTrueAnomaly As TrueAnomalyType

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        ' Note that if idx = idxEarth, the "_p" variables are actually the Earth
        objName = orbElements.getOEObjName(idx)
        Ecc_p = orbElements.getOEObjEccentricity(idx)
        a_p = orbElements.getOEObjSemiMajAxisAU(idx)
        Tp = orbElements.getOEObjPeriod(idx)
        m_p = orbElements.getOEObjMass(idx)
        r_p = orbElements.getOEObjRadius(idx)
        RVL_p = 1.0   'This is actually calculated below, but initialized here to avoid compiler warning
        m_e = orbElements.getOEObjMass(idxEarth)
        r_e = orbElements.getOEObjRadius(idxEarth)
        RVL_e = orbElements.calcEarthRVLength(observer.getObsDate().getMonth(), observer.getObsDate().getiDay(),
                observer.getObsDate().getYear(), 0.0, solveTrueAnomaly, termCriteria)

        If summary Then
            prt.setBoldFont(True)
            prt.println("Summary of miscellaneous data items for " & objName, CENTERTXT)
            prt.setBoldFont(False)
            prt.println()
        End If

        prt.setFixedWidthFont()

        ' Calculate weight on any object except the Earth
        If (idx <> idxEarth) Then
            printlncond("**************************************")
            dT = (m_p * r_e ^ 2) / (r_p ^ 2 * m_e)
            printcond("Calculate the weight of an object on ")
            If (idx = idxSun) Or (idx = idxMoon) Then printcond("the ")
            printlncond(objName)
            printlncond("  Weight = (m_p*r_e^2)/(r^p*m_e) = (" & Format(m_p, genFloatFormat) & " * " &
                        Format(r_e, genFloatFormat) & "^2) / (" & Format(r_p, genFloatFormat) & "^2 * " &
                        Format(m_e, genFloatFormat) & ")")
            printlncond("         = " & Format(dT, genFloatFormat))
            printlncond()
            If summary Then
                prt.println("Weight on " & objName & ": Wp = " & Format(dT, genFloatFormat) & " * We")
            End If
        End If

        ' Calculate transmissions delays for any object except the Sun, Earth, or Moon
        If (idx <> idxEarth) And (idx <> idxSun) And (idx <> idxMoon) Then
            printlncond("**************************************")
            ' Find the object's location and then its distance.
            eclCoord = orbElements.calcObjEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                               observer.getObsDate.getYear, 0.0, idx, L_p, RVL_p, L_e, RVL_e,
                                               solveTrueAnomaly, termCriteria)
            dDist = calcObjDistToEarth(RVL_e, L_e, RVL_p, L_p)
            dT = 0.1384 * dDist
            printcond("Calculate the transmission delay between ")
            If (idx = idxSun) Then printcond("the ")
            printlncond(objName & " and the Earth.")
            printlncond("  Delay = 0.1384*Dist = 0.1384*" & Format(dDist, genFloatFormat))
            printlncond("        = " & timeToStr(dT, DECFORMAT) & " hours")
            printlncond()
            If summary Then
                prt.println("Transmission delay: " & timeToStr(dT, HMSFORMAT))
            End If
        End If

        ' Find length of a year relative to Earth for any object except the Earth, Moon, or Sun
        If (idx <> idxEarth) And (idx <> idxMoon) And (idx <> idxSun) Then
            printlncond("**************************************")
            dT = 365.242191 * Tp
            printlncond("Calculate length of a year on " & objName & " relative to Earth")
            printlncond("  Year = 365.242191*Tp = 365.242191 * " & Tp)
            printlncond("       = " & insertCommas(dT) & " Earth days")
            printlncond()
            If summary Then
                dT = dT / 365.25 ' convert to years
                iTmp = Trunc(dT)
                prt.print("Length of Year: " & iTmp & " years, ")
                dTmp = (dT - CDbl(iTmp)) * 365.25  ' days
                iTmp = Trunc(dTmp)
                prt.print(iTmp & " days, ")
                dTmp = (dTmp - CDbl(iTmp)) * 24 ' hours
                prt.println(timeToStr(dTmp, HMSFORMAT))
            End If
        End If

        ' Calculate orbital period given the semi-major axis
        printlncond("**************************************")
        dT = Sqrt(a_p ^ 3)
        printcond("Calculate the orbital period for ")
        If (idx = idxSun) Or (idx = idxMoon) Then printcond("the ")
        printlncond(objName & " given its semi-major axis in AUs.")
        printlncond("  Tp = sqrt(a_p^3) = sqrt(" & Format(a_p, genFloatFormat) & "^3)")
        printlncond("     = " & Format(dT, genFloatFormat) & " tropical years")
        printlncond()
        If summary Then
            prt.println("Orbital period calculated from semi-major axis: " & Format(dT, genFloatFormat) & " tropical years")
        End If

        ' Calculate the semi-major axis given the orbital period
        ' for all but the Sun and Moon
        If (idx <> idxMoon) And (idx <> idxSun) Then
            printlncond("**************************************")
            dT = cuberoot(Tp * Tp)
            printlncond("Calculate the semi-major axis for " & objName)
            printlncond("given its orbital period in tropical years.")
            printlncond("  a_p = cuberoot(Tp^2) = cuberoot(" & Format(Tp, genFloatFormat) & "^2)")
            printlncond("      = " & Format(dT, genFloatFormat) & " AUs")
            printlncond()
            If summary Then
                prt.println("Semi-major axis calculated from orbital period: " & Format(dT, genFloatFormat) & " AUs")
            End If
        End If

        ' Calculate orbital velocities for all except the Sun and Moon
        If (idx <> idxSun) And (idx <> idxMoon) Then
            printlncond("**************************************")
            dTmp = orbElements.getOEObjGravParm(idxSun)
            dT = dTmp * (1 + Ecc_p) / (a_p * (1 - Ecc_p) * 150000000.0)
            dT = Sqrt(dT)
            printlncond("Calculate the velocity at perihelion for " & objName)
            printlncond("  Vper = sqrt[(Mu_sun*(1+eccentricity_p))/(a_p*(1-eccentricity_p)*1.5E08)]")
            printlncond("       = sqrt[(" & Format(dTmp, genFloatFormat) & "*(1+" & Format(Ecc_p, genFloatFormat) & "))/(" &
                        Format(a_p, genFloatFormat) & "*(1-" & Format(Ecc_p, genFloatFormat) & ")*1.5E08)]")
            printlncond("       = " & Format(dT, genFloatFormat) & " km/s")
            printlncond()
            If summary Then
                prt.println("Vper = " & Round(dT, 2) & " km/s (" & Round(KM2Miles(dT), 2) & " miles/s)")
            End If

            dT = 29.865958 * ((a_p * Sqrt(1 - Ecc_p ^ 2)) / Tp)
            printlncond("Calculate the average orbital velocity for " & objName)
            printlncond("  Vavg = 29.86598*[(a_p*sqrt(1-eccentricity_p^2)/Tp]")
            printlncond("       = 29.86598*[(" & Format(a_p, genFloatFormat) & "*sqrt(1-" &
                        Format(Ecc_p, genFloatFormat) & "^2)/(" &
                        Format(Tp, genFloatFormat) & "]")
            printlncond("       = " & Format(dT, genFloatFormat) & " km/s")
            printlncond()
            If summary Then
                prt.println("Vavg = " & Round(dT, 2) & " km/s (" & Round(KM2Miles(dT), 2) & " miles/s)")
            End If

            dT = dTmp * (1 - Ecc_p) / (a_p * (1 + Ecc_p) * 150000000.0)
            dT = Sqrt(dT)
            printlncond("Calculate the velocity at aphelion for " & objName)
            printlncond("  Vaph = sqrt[(Mu_sun*(1-eccentricity_p))/(a_p*(1+eccentricity_p)*1.5E08)]")
            printlncond("       = sqrt[(" & Format(dTmp, genFloatFormat) & "*(1-" & Format(Ecc_p, genFloatFormat) & "))/(" &
                        Format(a_p, genFloatFormat) & "*(1+" & Format(Ecc_p, genFloatFormat) & ")*1.5E08)]")
            printlncond("       = " & Format(dT, genFloatFormat) & " km/s")
            printlncond()
            If summary Then
                prt.println("Vaph = " & Round(dT, 2) & " km/s (" & Round(KM2Miles(dT), 2) & " miles/s)")
            End If

            If idx = idxEarth Then
                RVL_x = RVL_e
            Else
                RVL_x = RVL_p
            End If
            dT = (dTmp / 150000000.0) * ((2 / RVL_x) - (1 / a_p))
            dT = Sqrt(dT)
            printlncond("Calculate the velocity for " & objName & " for the current observer's date")
            printlncond("  Vdate = sqrt[(Mu_sun/1.5E8)*((2/Rp) - (1/a_p))]")
            printlncond("        = sqrt[(" & Format(dTmp, genFloatFormat) & "/1.5E8)*((2/" & Format(RVL_x, genFloatFormat) &
                        ") - (1/" & Format(a_p, genFloatFormat) & "))]")
            printlncond("        = " & Format(dT, genFloatFormat) & " km/s")
            If summary Then
                prt.println("Vdate = " & Round(dT, 2) & " km/s (" & Round(KM2Miles(dT), 2) & " miles/s)" & " Date is " &
                            dateToStr(observer.getObsDate))
            End If
        End If

        ' Calculate the escape velocity
        printlncond("**************************************")
        dT = orbElements.getOEObjGravParm(idx)
        printcond("Calculate the escape velocity for ")
        If (idx = idxSun) Or (idx = idxMoon) Then printcond("the ")
        printlncond(objName)
        printlncond("  Vescape = sqrt[(2*Mu_p)/(r_p)] = sqrt[(2*" & Format(dT, genFloatFormat) & ")/(" &
                    Format(r_p, genFloatFormat) & ")]")
        dT = Sqrt(2 * dT / r_p)
        printlncond("          = " & Format(dT, genFloatFormat) & " km/s")
        printlncond()
        If summary Then
            prt.println("Vescape = " & Round(dT, 2) & " km/s (" & Round(KM2Miles(dT), 2) & " miles/s)")
        End If

    End Sub

    '-----------------------------------------------------------
    ' Checks to see if an orbital elements database has been
    ' successfully loaded, and display an error message if not.
    '
    ' showErrors        display error message if true
    '-----------------------------------------------------------
    Private Function checkOEDBLoaded() As Boolean
        Return checkOEDBLoaded(SHOW_ERRORS)
    End Function
    Private Function checkOEDBLoaded(ByVal showErrors As Boolean) As Boolean

        If Not orbElements.isOrbElementsDBLoaded() Then
            If showErrors Then ErrMsg("No Orbital Elements data is currently loaded.", "No Orbital Elements Data Loaded")
            Return False
        End If
        Return True
    End Function

    '--------------------------------------
    ' Clear the text areas in the GUI
    '--------------------------------------
    Private Sub clearTextAreas()
        chartOnScreen = False
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '-----------------------------------------------------------------------
    ' This method computes the LST for the observer's current location
    ' and time. This is needed in order to convert equatorial coordinates
    ' to horizon coordinates. The observer's location is retrieved
    ' from the GUI.
    '
    ' NOTE: It is assumed that the observer's location/time has already
    ' been validated prior to this method being invoked.
    '-----------------------------------------------------------------------
    Private Function computeLST() As Double
        Dim LCT, UT, GST, LST As Double
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate

        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)
        Return LST
    End Function

    '----------------------------------------------------
    ' Determine what time zone radio button is selected
    '
    ' Returns a time zone type for whatever is the
    ' currently selected time zone radio button
    '----------------------------------------------------
    Private Function getSelectedRBStatus() As TimeZoneType
        If radbtnPST.Checked() Then
            Return TimeZoneType.PST
        ElseIf radbtnMST.Checked() Then
            Return TimeZoneType.MST
        ElseIf radbtnCST.Checked() Then
            Return TimeZoneType.CST
        ElseIf radbtnEST.Checked() Then
            Return TimeZoneType.EST
        Else
            Return TimeZoneType.LONGITUDE
        End If
    End Function

    '-------------------------------------------------------------------
    ' Get the equatorial coordinates for a solar system object
    ' for the current observer.
    '
    ' idx           index into orbital elements for the desired object,
    '               which cannot be the Earth but can be any other
    '               object in the orbital elements data file
    '
    ' Returns the equatorial coordinates for the object.
    '-------------------------------------------------------------------
    Private Function getSolarSysEqCoord(ByVal idx As Integer) As ASTCoord
        Dim eclCoord As ASTCoord
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim UT, LCT As Double
        Dim Lp, Le, Rp, Re As Double                    ' variables for a Solar Sys object
        Dim Ltrue, Omega_p, Ca, Mm, Vmoon As Double     ' variables for the Moon
        Dim Msun, Vsun As Double                        ' variables for the Sun
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate

        solveTrueAnomaly = rbForm.getTrueAnomalyRB()

        ' Get the observer's UT time
        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)

        If (idx = idxSun) Then
            eclCoord = orbElements.calcSunEclipticCoord(adjustedDate.getMonth, adjustedDate.getiDay, adjustedDate.getYear,
                                                        UT, Msun, Vsun, solveTrueAnomaly, termCriteria)

        ElseIf idx = idxMoon Then
            eclCoord = orbElements.calcMoonEclipticCoord(adjustedDate.getMonth, adjustedDate.getiDay, adjustedDate.getYear,
                                                         UT, Ltrue, Omega_p, Ca, Mm, Vmoon, solveTrueAnomaly, termCriteria)
        Else
            eclCoord = orbElements.calcObjEclipticCoord(adjustedDate.getMonth, adjustedDate.getiDay, adjustedDate.getYear,
                                                        UT, idx, Lp, Rp, Le, Re, solveTrueAnomaly, termCriteria)
        End If

        Return EclipticToEquatorial(eclCoord.getLatAngle.getDecAngle, eclCoord.getLonAngle.getDecAngle, orbElements.getOEEpochDate)
    End Function

    '---------------------------------------------------------------------------
    ' Ask user for a Solar System object's name and return its index.
    '
    ' Sun, Moon, Earth      booleans which are True if the object is allowed
    '
    ' Returns -1 if object is invalid or not allowed, else returns index
    ' into the orbital elements array.
    '---------------------------------------------------------------------------
    Private Function getValidObj(ByVal Sun As Boolean, ByVal Earth As Boolean,
                                 ByVal Moon As Boolean) As Integer
        Dim idx As Integer = -1
        Dim objName As String
        Dim exclusions As String = ""

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return -1
        If Not checkOEDBLoaded() Then Return -1

        ' See if any objects are to be excluded
        If (Not Sun) Or (Not Moon) Or (Not Earth) Then
            If Not Sun Then exclusions = "Sun"
            If Not Earth Then
                If exclusions.Length > 0 Then
                    exclusions = exclusions & ", Earth"
                Else
                    exclusions = "Earth"
                End If
            End If
            If Not Moon Then
                If exclusions.Length > 0 Then
                    exclusions = exclusions & ", Moon"
                Else
                    exclusions = "Moon"
                End If
            End If
            exclusions = "(cannot choose " & exclusions & ")"
        Else
            exclusions = "(any Solar System object can be used)"
        End If

        If queryForm.showQueryForm("Enter Solar System object's name" & vbNewLine &
                                   exclusions) <> Windows.Forms.DialogResult.OK Then Return -1

        ' validate data
        objName = queryForm.getData1()
        idx = orbElements.findOrbElementObjIndex(objName.Trim())
        If idx < 0 Then
            ErrMsg("No Object with the name '" & objName & "' was found", "Invalid Object Name")
            Return -1
        End If

        If (idx = idxSun) And Not Sun Then
            ErrMsg("Cannot choose the Sun", "Invalid Choice")
            Return -1
        End If
        If (idx = idxEarth) And Not Earth Then
            ErrMsg("Cannot choose the Earth", "Invalid Choice")
            Return -1
        End If
        If (idx = idxMoon) And Not Moon Then
            ErrMsg("Cannot choose the Moon", "Invalid Choice")
            Return -1
        End If

        Return idx
    End Function

    '---------------------------------------------------------------------------
    ' Ask user for a planet's name and return its index. Pluto is **not**
    ' considered to be a planet in this context.
    '
    ' Returns -1 if object is invalid or not allowed, else returns index
    ' into the orbital elements array.
    '---------------------------------------------------------------------------
    Private Function getValidPlanet() As Integer
        Dim idx As Integer = -1
        Dim objName As String

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return -1
        If Not checkOEDBLoaded() Then Return -1

        If queryForm.showQueryForm("Enter Object's Name (must be a planet!)") <> Windows.Forms.DialogResult.OK Then Return -1

        ' validate data
        objName = queryForm.getData1()
            idx = orbElements.findOrbElementObjIndex(objName.Trim())
        If idx < 0 Then
            ErrMsg("No Planet with the name '" & objName & "' was found",
                       "Invalid Planet Name")
            Return -1
        End If

        ' This only works for the planets, so make sure the user specified a planet
        ' The orbital elements file could have non-planets in it (e.g, Pluto, Ceres)
        If Not isAPlanet(idx) Then
            ErrMsg("This method only works for Solar System planets" & vbNewLine &
                   "(Remember, Pluto is not a planet)", "Invalid Choice")
            Return -1
        End If

        Return idx

    End Function

    '-----------------------------------------------------------
    ' Determines whether an object is a planet. This method
    ' does **not** count Pluto as a planet.
    '
    ' idx           index into orbital elements for the object
    '               under consideration
    '
    ' Returns true if the object is a planet in the currently
    ' loaded orbital elements.
    '-----------------------------------------------------------
    Private Function isAPlanet(ByVal idx As Integer) As Boolean
        If (idx = orbElements.findOrbElementObjIndex("Mercury")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Venus")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Earth")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Mars")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Jupiter")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Saturn")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Uranus")) Then
            Return True
        ElseIf (idx = orbElements.findOrbElementObjIndex("Neptune")) Then
            Return True
        End If
        Return False
    End Function

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '-------------------------------------------------------
    ' Sets the Catalog type in the GUI.
    '
    ' cat_type      String representing the catalog type
    '               which is read from the star catalog
    '               data file
    '-------------------------------------------------------
    Private Sub setCatalogType(ByVal cat_type As String)
        lblCatalogType.Text = "Catalog Type: " & cat_type
    End Sub

    '-----------------------------------
    ' Sets the Epoch label in the GUI.
    '
    ' epoch         Epoch to display
    '-----------------------------------
    Private Sub setEpoch(ByVal epoch As Double)
        lblEpoch.Text = "Epoch: " & Format(epoch, epochFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the filename label to be displayed in the GUI.
    '
    ' filename          star catalog filename, including pathname
    '---------------------------------------------------------------
    Private Sub setFilename(ByVal fname As String)
        lblFilename.Text = "File: " & abbrevRight(fname, MAX_FNAME_DISPLAY)
    End Sub

    '---------------------------------------------------------------
    ' Sets the mv Filter label to be displayed in the GUI.
    '
    ' mV            visual magnitude filter to display
    '---------------------------------------------------------------
    Private Sub setmVFilter(ByVal mV As Double)
        currentmVFilter = mV
        lblmVFilter.Text = "mV Filter: " & Format(mV, mVFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the observer location in the GUI. This is intended to be
    ' done one time only during the program initialization since a
    ' default location may be read from a data file.
    '
    ' obs           observer location object
    '---------------------------------------------------------------
    Private Sub setObsDataInGUI(ByVal obs As ASTObserver)
        txtboxLat.Text = latToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxLon.Text = lonToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxDate.Text = dateToStr(obs.getObsDate())
        txtboxLCT.Text = timeToStr(obs.getObsTime(), HMSFORMAT)

        If obs.getObsTimeZone = TimeZoneType.PST Then
            radbtnPST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.MST Then
            radbtnMST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.CST Then
            radbtnCST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.EST Then
            radbtnEST.Checked = True
        Else
            radbtnLon.Checked = True
        End If

    End Sub

    '----------------------------------------------
    ' See if the observer location, date, and time
    ' currently in the GUI is valid.
    '
    ' Returns true if valid, otherwise false.
    '----------------------------------------------
    Private Function validateGUIObsLoc() As Boolean
        Return isValidObsLoc(observer, txtboxLat.Text, txtboxLon.Text, getSelectedRBStatus().ToString,
                             txtboxDate.Text, txtboxLCT.Text)
    End Function

End Class
