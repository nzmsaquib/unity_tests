﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chap8GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Chap8GUI))
        Me.ChapMenu = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSolarSysInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDataFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStarCharts = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemInstructions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelBookTitle = New System.Windows.Forms.Panel()
        Me.lblBookTitle = New System.Windows.Forms.Label()
        Me.lblObs = New System.Windows.Forms.Label()
        Me.obsPanel = New System.Windows.Forms.Panel()
        Me.lblLCT = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.txtboxLCT = New System.Windows.Forms.TextBox()
        Me.txtboxDate = New System.Windows.Forms.TextBox()
        Me.chkboxDST = New System.Windows.Forms.CheckBox()
        Me.groupboxTimeZone = New System.Windows.Forms.GroupBox()
        Me.radbtnLon = New System.Windows.Forms.RadioButton()
        Me.radbtnMST = New System.Windows.Forms.RadioButton()
        Me.radbtnCST = New System.Windows.Forms.RadioButton()
        Me.radbtnEST = New System.Windows.Forms.RadioButton()
        Me.radbtnPST = New System.Windows.Forms.RadioButton()
        Me.lblLongitude = New System.Windows.Forms.Label()
        Me.lblLatitude = New System.Windows.Forms.Label()
        Me.txtboxLon = New System.Windows.Forms.TextBox()
        Me.txtboxLat = New System.Windows.Forms.TextBox()
        Me.chkboxLabelObjs = New System.Windows.Forms.CheckBox()
        Me.chkboxWhiteCharts = New System.Windows.Forms.CheckBox()
        Me.chkboxEQChart = New System.Windows.Forms.CheckBox()
        Me.chkboxShowInterimCalcs = New System.Windows.Forms.CheckBox()
        Me.panelInfo = New System.Windows.Forms.Panel()
        Me.lblmVFilter = New System.Windows.Forms.Label()
        Me.lblEpoch = New System.Windows.Forms.Label()
        Me.lblFilename = New System.Windows.Forms.Label()
        Me.lblCatalogType = New System.Windows.Forms.Label()
        Me.lblResults = New System.Windows.Forms.Label()
        Me.panelTextOut = New System.Windows.Forms.Panel()
        Me.txtboxOutputArea = New System.Windows.Forms.RichTextBox()
        Me.ChapMenu.SuspendLayout()
        Me.panelBookTitle.SuspendLayout()
        Me.obsPanel.SuspendLayout()
        Me.groupboxTimeZone.SuspendLayout()
        Me.panelInfo.SuspendLayout()
        Me.panelTextOut.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChapMenu
        '
        Me.ChapMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ChapMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuSolarSysInfo, Me.mnuDataFiles, Me.mnuStarCharts, Me.mnuitemHelp})
        Me.ChapMenu.Location = New System.Drawing.Point(0, 0)
        Me.ChapMenu.Name = "ChapMenu"
        Me.ChapMenu.Size = New System.Drawing.Size(982, 28)
        Me.ChapMenu.TabIndex = 0
        Me.ChapMenu.Text = "ChapMenu"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(44, 24)
        Me.mnuFile.Text = "File"
        '
        'mnuitemExit
        '
        Me.mnuitemExit.Name = "mnuitemExit"
        Me.mnuitemExit.Size = New System.Drawing.Size(108, 26)
        Me.mnuitemExit.Text = "Exit"
        '
        'mnuSolarSysInfo
        '
        Me.mnuSolarSysInfo.Name = "mnuSolarSysInfo"
        Me.mnuSolarSysInfo.Size = New System.Drawing.Size(110, 24)
        Me.mnuSolarSysInfo.Text = "Solar Sys Info"
        '
        'mnuDataFiles
        '
        Me.mnuDataFiles.Name = "mnuDataFiles"
        Me.mnuDataFiles.Size = New System.Drawing.Size(86, 24)
        Me.mnuDataFiles.Text = "Data Files"
        '
        'mnuStarCharts
        '
        Me.mnuStarCharts.Name = "mnuStarCharts"
        Me.mnuStarCharts.Size = New System.Drawing.Size(92, 24)
        Me.mnuStarCharts.Text = "Star Charts"
        '
        'mnuitemHelp
        '
        Me.mnuitemHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemInstructions, Me.mnuitemAbout})
        Me.mnuitemHelp.Name = "mnuitemHelp"
        Me.mnuitemHelp.Size = New System.Drawing.Size(53, 24)
        Me.mnuitemHelp.Text = "Help"
        '
        'mnuitemInstructions
        '
        Me.mnuitemInstructions.Name = "mnuitemInstructions"
        Me.mnuitemInstructions.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemInstructions.Text = "Instructions"
        '
        'mnuitemAbout
        '
        Me.mnuitemAbout.Name = "mnuitemAbout"
        Me.mnuitemAbout.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemAbout.Text = "About"
        '
        'panelBookTitle
        '
        Me.panelBookTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBookTitle.BackColor = System.Drawing.Color.Navy
        Me.panelBookTitle.Controls.Add(Me.lblBookTitle)
        Me.panelBookTitle.Location = New System.Drawing.Point(0, 29)
        Me.panelBookTitle.Name = "panelBookTitle"
        Me.panelBookTitle.Size = New System.Drawing.Size(982, 36)
        Me.panelBookTitle.TabIndex = 2
        '
        'lblBookTitle
        '
        Me.lblBookTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblBookTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBookTitle.ForeColor = System.Drawing.Color.White
        Me.lblBookTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBookTitle.Location = New System.Drawing.Point(0, 8)
        Me.lblBookTitle.Name = "lblBookTitle"
        Me.lblBookTitle.Padding = New System.Windows.Forms.Padding(1)
        Me.lblBookTitle.Size = New System.Drawing.Size(982, 27)
        Me.lblBookTitle.TabIndex = 0
        Me.lblBookTitle.Text = "Book Title"
        Me.lblBookTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblObs
        '
        Me.lblObs.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblObs.AutoSize = True
        Me.lblObs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObs.Location = New System.Drawing.Point(368, 68)
        Me.lblObs.Name = "lblObs"
        Me.lblObs.Size = New System.Drawing.Size(247, 20)
        Me.lblObs.TabIndex = 3
        Me.lblObs.Text = "Observer Location and Time"
        '
        'obsPanel
        '
        Me.obsPanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.obsPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.obsPanel.Controls.Add(Me.lblLCT)
        Me.obsPanel.Controls.Add(Me.lblDate)
        Me.obsPanel.Controls.Add(Me.txtboxLCT)
        Me.obsPanel.Controls.Add(Me.txtboxDate)
        Me.obsPanel.Controls.Add(Me.chkboxDST)
        Me.obsPanel.Controls.Add(Me.groupboxTimeZone)
        Me.obsPanel.Controls.Add(Me.lblLongitude)
        Me.obsPanel.Controls.Add(Me.lblLatitude)
        Me.obsPanel.Controls.Add(Me.txtboxLon)
        Me.obsPanel.Controls.Add(Me.txtboxLat)
        Me.obsPanel.Location = New System.Drawing.Point(84, 91)
        Me.obsPanel.Name = "obsPanel"
        Me.obsPanel.Size = New System.Drawing.Size(835, 81)
        Me.obsPanel.TabIndex = 7
        '
        'lblLCT
        '
        Me.lblLCT.AutoSize = True
        Me.lblLCT.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLCT.Location = New System.Drawing.Point(760, 46)
        Me.lblLCT.Name = "lblLCT"
        Me.lblLCT.Size = New System.Drawing.Size(44, 20)
        Me.lblLCT.TabIndex = 0
        Me.lblLCT.Text = "LCT"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDate.Location = New System.Drawing.Point(760, 12)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(49, 20)
        Me.lblDate.TabIndex = 0
        Me.lblDate.Text = "Date"
        '
        'txtboxLCT
        '
        Me.txtboxLCT.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxLCT.Location = New System.Drawing.Point(635, 43)
        Me.txtboxLCT.Name = "txtboxLCT"
        Me.txtboxLCT.Size = New System.Drawing.Size(123, 27)
        Me.txtboxLCT.TabIndex = 5
        Me.txtboxLCT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtboxDate
        '
        Me.txtboxDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxDate.Location = New System.Drawing.Point(635, 9)
        Me.txtboxDate.Name = "txtboxDate"
        Me.txtboxDate.Size = New System.Drawing.Size(123, 27)
        Me.txtboxDate.TabIndex = 4
        Me.txtboxDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkboxDST
        '
        Me.chkboxDST.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkboxDST.AutoSize = True
        Me.chkboxDST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxDST.Location = New System.Drawing.Point(399, 5)
        Me.chkboxDST.Name = "chkboxDST"
        Me.chkboxDST.Size = New System.Drawing.Size(189, 24)
        Me.chkboxDST.TabIndex = 3
        Me.chkboxDST.Text = "Daylight Saving Time"
        Me.chkboxDST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkboxDST.UseVisualStyleBackColor = True
        '
        'groupboxTimeZone
        '
        Me.groupboxTimeZone.Controls.Add(Me.radbtnLon)
        Me.groupboxTimeZone.Controls.Add(Me.radbtnMST)
        Me.groupboxTimeZone.Controls.Add(Me.radbtnCST)
        Me.groupboxTimeZone.Controls.Add(Me.radbtnEST)
        Me.groupboxTimeZone.Controls.Add(Me.radbtnPST)
        Me.groupboxTimeZone.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.groupboxTimeZone.Location = New System.Drawing.Point(258, 21)
        Me.groupboxTimeZone.Name = "groupboxTimeZone"
        Me.groupboxTimeZone.Size = New System.Drawing.Size(363, 51)
        Me.groupboxTimeZone.TabIndex = 0
        Me.groupboxTimeZone.TabStop = False
        Me.groupboxTimeZone.Text = " Time Zone "
        '
        'radbtnLon
        '
        Me.radbtnLon.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnLon.AutoSize = True
        Me.radbtnLon.CausesValidation = False
        Me.radbtnLon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnLon.Location = New System.Drawing.Point(270, 20)
        Me.radbtnLon.Name = "radbtnLon"
        Me.radbtnLon.Size = New System.Drawing.Size(93, 24)
        Me.radbtnLon.TabIndex = 0
        Me.radbtnLon.Text = "Use Lon"
        Me.radbtnLon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnLon.UseVisualStyleBackColor = True
        '
        'radbtnMST
        '
        Me.radbtnMST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnMST.AutoSize = True
        Me.radbtnMST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnMST.Location = New System.Drawing.Point(70, 20)
        Me.radbtnMST.Name = "radbtnMST"
        Me.radbtnMST.Size = New System.Drawing.Size(65, 24)
        Me.radbtnMST.TabIndex = 4
        Me.radbtnMST.Text = "MST"
        Me.radbtnMST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnMST.UseVisualStyleBackColor = True
        '
        'radbtnCST
        '
        Me.radbtnCST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnCST.AutoSize = True
        Me.radbtnCST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnCST.Location = New System.Drawing.Point(141, 20)
        Me.radbtnCST.Name = "radbtnCST"
        Me.radbtnCST.Size = New System.Drawing.Size(63, 24)
        Me.radbtnCST.TabIndex = 9
        Me.radbtnCST.Text = "CST"
        Me.radbtnCST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnCST.UseVisualStyleBackColor = True
        '
        'radbtnEST
        '
        Me.radbtnEST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnEST.AutoSize = True
        Me.radbtnEST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnEST.Location = New System.Drawing.Point(206, 20)
        Me.radbtnEST.Name = "radbtnEST"
        Me.radbtnEST.Size = New System.Drawing.Size(62, 24)
        Me.radbtnEST.TabIndex = 10
        Me.radbtnEST.Text = "EST"
        Me.radbtnEST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnEST.UseVisualStyleBackColor = True
        '
        'radbtnPST
        '
        Me.radbtnPST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnPST.AutoSize = True
        Me.radbtnPST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnPST.Location = New System.Drawing.Point(6, 20)
        Me.radbtnPST.Name = "radbtnPST"
        Me.radbtnPST.Size = New System.Drawing.Size(62, 24)
        Me.radbtnPST.TabIndex = 3
        Me.radbtnPST.Text = "PST"
        Me.radbtnPST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnPST.UseVisualStyleBackColor = True
        '
        'lblLongitude
        '
        Me.lblLongitude.AutoSize = True
        Me.lblLongitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLongitude.Location = New System.Drawing.Point(154, 46)
        Me.lblLongitude.Name = "lblLongitude"
        Me.lblLongitude.Size = New System.Drawing.Size(91, 20)
        Me.lblLongitude.TabIndex = 3
        Me.lblLongitude.Text = "Longitude"
        '
        'lblLatitude
        '
        Me.lblLatitude.AutoSize = True
        Me.lblLatitude.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLatitude.Location = New System.Drawing.Point(154, 12)
        Me.lblLatitude.Name = "lblLatitude"
        Me.lblLatitude.Size = New System.Drawing.Size(77, 20)
        Me.lblLatitude.TabIndex = 0
        Me.lblLatitude.Text = "Latitude"
        '
        'txtboxLon
        '
        Me.txtboxLon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxLon.Location = New System.Drawing.Point(7, 43)
        Me.txtboxLon.Name = "txtboxLon"
        Me.txtboxLon.Size = New System.Drawing.Size(145, 27)
        Me.txtboxLon.TabIndex = 2
        Me.txtboxLon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtboxLat
        '
        Me.txtboxLat.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxLat.Location = New System.Drawing.Point(7, 9)
        Me.txtboxLat.Name = "txtboxLat"
        Me.txtboxLat.Size = New System.Drawing.Size(145, 27)
        Me.txtboxLat.TabIndex = 1
        Me.txtboxLat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkboxLabelObjs
        '
        Me.chkboxLabelObjs.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxLabelObjs.AutoSize = True
        Me.chkboxLabelObjs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxLabelObjs.Location = New System.Drawing.Point(784, 174)
        Me.chkboxLabelObjs.Name = "chkboxLabelObjs"
        Me.chkboxLabelObjs.Size = New System.Drawing.Size(135, 24)
        Me.chkboxLabelObjs.TabIndex = 13
        Me.chkboxLabelObjs.Text = "Label Objects"
        Me.chkboxLabelObjs.UseVisualStyleBackColor = True
        '
        'chkboxWhiteCharts
        '
        Me.chkboxWhiteCharts.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxWhiteCharts.AutoSize = True
        Me.chkboxWhiteCharts.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxWhiteCharts.Location = New System.Drawing.Point(589, 174)
        Me.chkboxWhiteCharts.Name = "chkboxWhiteCharts"
        Me.chkboxWhiteCharts.Size = New System.Drawing.Size(188, 24)
        Me.chkboxWhiteCharts.TabIndex = 12
        Me.chkboxWhiteCharts.Text = "White Bkg for Charts"
        Me.chkboxWhiteCharts.UseVisualStyleBackColor = True
        '
        'chkboxEQChart
        '
        Me.chkboxEQChart.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxEQChart.AutoSize = True
        Me.chkboxEQChart.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxEQChart.Location = New System.Drawing.Point(356, 174)
        Me.chkboxEQChart.Name = "chkboxEQChart"
        Me.chkboxEQChart.Size = New System.Drawing.Size(220, 24)
        Me.chkboxEQChart.TabIndex = 11
        Me.chkboxEQChart.Text = "Equatorial Coords Charts"
        Me.chkboxEQChart.UseVisualStyleBackColor = True
        '
        'chkboxShowInterimCalcs
        '
        Me.chkboxShowInterimCalcs.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxShowInterimCalcs.AutoSize = True
        Me.chkboxShowInterimCalcs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxShowInterimCalcs.Location = New System.Drawing.Point(81, 174)
        Me.chkboxShowInterimCalcs.Name = "chkboxShowInterimCalcs"
        Me.chkboxShowInterimCalcs.Size = New System.Drawing.Size(266, 24)
        Me.chkboxShowInterimCalcs.TabIndex = 10
        Me.chkboxShowInterimCalcs.Text = "Show Intermediate Calculations"
        Me.chkboxShowInterimCalcs.UseVisualStyleBackColor = True
        '
        'panelInfo
        '
        Me.panelInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelInfo.Controls.Add(Me.lblmVFilter)
        Me.panelInfo.Controls.Add(Me.lblEpoch)
        Me.panelInfo.Controls.Add(Me.lblFilename)
        Me.panelInfo.Controls.Add(Me.lblCatalogType)
        Me.panelInfo.Location = New System.Drawing.Point(0, 204)
        Me.panelInfo.Name = "panelInfo"
        Me.panelInfo.Size = New System.Drawing.Size(982, 52)
        Me.panelInfo.TabIndex = 14
        '
        'lblmVFilter
        '
        Me.lblmVFilter.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblmVFilter.AutoSize = True
        Me.lblmVFilter.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblmVFilter.Location = New System.Drawing.Point(824, 28)
        Me.lblmVFilter.Name = "lblmVFilter"
        Me.lblmVFilter.Size = New System.Drawing.Size(150, 20)
        Me.lblmVFilter.TabIndex = 3
        Me.lblmVFilter.Text = "mV Filter: -20.55"
        '
        'lblEpoch
        '
        Me.lblEpoch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblEpoch.AutoSize = True
        Me.lblEpoch.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEpoch.Location = New System.Drawing.Point(824, 6)
        Me.lblEpoch.Name = "lblEpoch"
        Me.lblEpoch.Size = New System.Drawing.Size(113, 20)
        Me.lblEpoch.TabIndex = 2
        Me.lblEpoch.Text = "Epoch: 2000"
        '
        'lblFilename
        '
        Me.lblFilename.AutoSize = True
        Me.lblFilename.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilename.Location = New System.Drawing.Point(11, 28)
        Me.lblFilename.Name = "lblFilename"
        Me.lblFilename.Size = New System.Drawing.Size(46, 20)
        Me.lblFilename.TabIndex = 1
        Me.lblFilename.Text = "File:"
        '
        'lblCatalogType
        '
        Me.lblCatalogType.AutoSize = True
        Me.lblCatalogType.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCatalogType.Location = New System.Drawing.Point(11, 6)
        Me.lblCatalogType.Name = "lblCatalogType"
        Me.lblCatalogType.Size = New System.Drawing.Size(125, 20)
        Me.lblCatalogType.TabIndex = 0
        Me.lblCatalogType.Text = "Catalog Type:"
        '
        'lblResults
        '
        Me.lblResults.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblResults.AutoSize = True
        Me.lblResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResults.Location = New System.Drawing.Point(13, 260)
        Me.lblResults.Name = "lblResults"
        Me.lblResults.Size = New System.Drawing.Size(73, 20)
        Me.lblResults.TabIndex = 15
        Me.lblResults.Text = "Results"
        '
        'panelTextOut
        '
        Me.panelTextOut.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelTextOut.Controls.Add(Me.txtboxOutputArea)
        Me.panelTextOut.Location = New System.Drawing.Point(10, 283)
        Me.panelTextOut.Name = "panelTextOut"
        Me.panelTextOut.Padding = New System.Windows.Forms.Padding(5)
        Me.panelTextOut.Size = New System.Drawing.Size(964, 505)
        Me.panelTextOut.TabIndex = 16
        '
        'txtboxOutputArea
        '
        Me.txtboxOutputArea.BackColor = System.Drawing.Color.White
        Me.txtboxOutputArea.Cursor = System.Windows.Forms.Cursors.No
        Me.txtboxOutputArea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtboxOutputArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxOutputArea.Location = New System.Drawing.Point(5, 5)
        Me.txtboxOutputArea.Name = "txtboxOutputArea"
        Me.txtboxOutputArea.ReadOnly = True
        Me.txtboxOutputArea.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtboxOutputArea.Size = New System.Drawing.Size(954, 495)
        Me.txtboxOutputArea.TabIndex = 0
        Me.txtboxOutputArea.TabStop = False
        Me.txtboxOutputArea.Text = ""
        '
        'Chap8GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(982, 789)
        Me.Controls.Add(Me.panelTextOut)
        Me.Controls.Add(Me.lblResults)
        Me.Controls.Add(Me.panelInfo)
        Me.Controls.Add(Me.chkboxLabelObjs)
        Me.Controls.Add(Me.chkboxWhiteCharts)
        Me.Controls.Add(Me.chkboxEQChart)
        Me.Controls.Add(Me.chkboxShowInterimCalcs)
        Me.Controls.Add(Me.obsPanel)
        Me.Controls.Add(Me.lblObs)
        Me.Controls.Add(Me.panelBookTitle)
        Me.Controls.Add(Me.ChapMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.ChapMenu
        Me.Name = "Chap8GUI"
        Me.Text = "Title"
        Me.ChapMenu.ResumeLayout(False)
        Me.ChapMenu.PerformLayout()
        Me.panelBookTitle.ResumeLayout(False)
        Me.obsPanel.ResumeLayout(False)
        Me.obsPanel.PerformLayout()
        Me.groupboxTimeZone.ResumeLayout(False)
        Me.groupboxTimeZone.PerformLayout()
        Me.panelInfo.ResumeLayout(False)
        Me.panelInfo.PerformLayout()
        Me.panelTextOut.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChapMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSolarSysInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDataFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStarCharts As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemInstructions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents panelBookTitle As System.Windows.Forms.Panel
    Friend WithEvents lblBookTitle As System.Windows.Forms.Label
    Friend WithEvents lblObs As System.Windows.Forms.Label
    Friend WithEvents obsPanel As System.Windows.Forms.Panel
    Friend WithEvents lblLCT As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents txtboxLCT As System.Windows.Forms.TextBox
    Friend WithEvents txtboxDate As System.Windows.Forms.TextBox
    Friend WithEvents chkboxDST As System.Windows.Forms.CheckBox
    Friend WithEvents groupboxTimeZone As System.Windows.Forms.GroupBox
    Friend WithEvents radbtnLon As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnMST As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnCST As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnEST As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnPST As System.Windows.Forms.RadioButton
    Friend WithEvents lblLongitude As System.Windows.Forms.Label
    Friend WithEvents lblLatitude As System.Windows.Forms.Label
    Friend WithEvents txtboxLon As System.Windows.Forms.TextBox
    Friend WithEvents txtboxLat As System.Windows.Forms.TextBox
    Friend WithEvents chkboxLabelObjs As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxWhiteCharts As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxEQChart As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowInterimCalcs As System.Windows.Forms.CheckBox
    Friend WithEvents panelInfo As System.Windows.Forms.Panel
    Friend WithEvents lblmVFilter As System.Windows.Forms.Label
    Friend WithEvents lblEpoch As System.Windows.Forms.Label
    Friend WithEvents lblFilename As System.Windows.Forms.Label
    Friend WithEvents lblCatalogType As System.Windows.Forms.Label
    Friend WithEvents lblResults As System.Windows.Forms.Label
    Friend WithEvents panelTextOut As System.Windows.Forms.Panel
    Friend WithEvents txtboxOutputArea As System.Windows.Forms.RichTextBox

End Class
