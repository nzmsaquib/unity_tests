﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TrueAnomalyRBs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.radbtnEQofCenter = New System.Windows.Forms.RadioButton()
        Me.radbtnSimple = New System.Windows.Forms.RadioButton()
        Me.radbtnNewton = New System.Windows.Forms.RadioButton()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.trueAnomalyGroupBox = New System.Windows.Forms.GroupBox()
        Me.trueAnomalyGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'radbtnEQofCenter
        '
        Me.radbtnEQofCenter.AutoSize = True
        Me.radbtnEQofCenter.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnEQofCenter.Location = New System.Drawing.Point(14, 26)
        Me.radbtnEQofCenter.Name = "radbtnEQofCenter"
        Me.radbtnEQofCenter.Size = New System.Drawing.Size(169, 24)
        Me.radbtnEQofCenter.TabIndex = 0
        Me.radbtnEQofCenter.TabStop = True
        Me.radbtnEQofCenter.Text = "Equation of Center"
        Me.radbtnEQofCenter.UseVisualStyleBackColor = True
        '
        'radbtnSimple
        '
        Me.radbtnSimple.AutoSize = True
        Me.radbtnSimple.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnSimple.Location = New System.Drawing.Point(14, 57)
        Me.radbtnSimple.Name = "radbtnSimple"
        Me.radbtnSimple.Size = New System.Drawing.Size(146, 24)
        Me.radbtnSimple.TabIndex = 1
        Me.radbtnSimple.TabStop = True
        Me.radbtnSimple.Text = "Simple Iteration"
        Me.radbtnSimple.UseVisualStyleBackColor = True
        '
        'radbtnNewton
        '
        Me.radbtnNewton.AutoSize = True
        Me.radbtnNewton.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnNewton.Location = New System.Drawing.Point(14, 90)
        Me.radbtnNewton.Name = "radbtnNewton"
        Me.radbtnNewton.Size = New System.Drawing.Size(217, 24)
        Me.radbtnNewton.TabIndex = 2
        Me.radbtnNewton.TabStop = True
        Me.radbtnNewton.Text = "Newton/Raphson Method"
        Me.radbtnNewton.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Location = New System.Drawing.Point(104, 168)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 34)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'trueAnomalyGroupBox
        '
        Me.trueAnomalyGroupBox.CausesValidation = False
        Me.trueAnomalyGroupBox.Controls.Add(Me.radbtnNewton)
        Me.trueAnomalyGroupBox.Controls.Add(Me.radbtnSimple)
        Me.trueAnomalyGroupBox.Controls.Add(Me.radbtnEQofCenter)
        Me.trueAnomalyGroupBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.trueAnomalyGroupBox.Location = New System.Drawing.Point(16, 20)
        Me.trueAnomalyGroupBox.Name = "trueAnomalyGroupBox"
        Me.trueAnomalyGroupBox.Size = New System.Drawing.Size(241, 127)
        Me.trueAnomalyGroupBox.TabIndex = 4
        Me.trueAnomalyGroupBox.TabStop = False
        Me.trueAnomalyGroupBox.Text = "True Anomaly Methods"
        '
        'TrueAnomalyRBs
        '
        Me.AcceptButton = Me.btnOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(282, 222)
        Me.Controls.Add(Me.trueAnomalyGroupBox)
        Me.Controls.Add(Me.btnOK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MinimizeBox = False
        Me.Name = "TrueAnomalyRBs"
        Me.ShowIcon = False
        Me.Text = "Choose Method ..."
        Me.trueAnomalyGroupBox.ResumeLayout(False)
        Me.trueAnomalyGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents radbtnEQofCenter As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnSimple As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnNewton As System.Windows.Forms.RadioButton
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents trueAnomalyGroupBox As System.Windows.Forms.GroupBox
End Class
