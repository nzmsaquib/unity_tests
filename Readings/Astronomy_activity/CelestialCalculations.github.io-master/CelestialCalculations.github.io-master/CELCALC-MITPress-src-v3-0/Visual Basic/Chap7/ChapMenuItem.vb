﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define possible menu items. These help in deciding what routine to
    ' execute when a menu item is selected
    Friend Enum CalculationType
        ' Lunar Info menu
        MOON_LOCATION
        MOON_RISE_SET
        MOON_DIST_AND_ANG_DIAMETER
        MOON_PHASES_AND_AGE
        MOON_PERCENT_ILLUMINATED
        TERM_CRITERIA
        ' Orbital Elements menu
        LOAD_ORBITAL_ELEMENTS
        SHOW_ORBITAL_ELEMENTS
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub

End Class
