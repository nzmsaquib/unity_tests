﻿'******************************************************************
'                  Chapter 7 - The Moon
'                   Copyright (c) 2018
'                  Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' for a program that will calculate various items about
' the Moon, including its location and times for moonrise/set.
' The main GUI was mostly built with the Visual Basic form editor.
'******************************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTCoord
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTObserver
Imports ASTUtils.ASTOrbits
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap7.ChapMenuItem

Public Class Chap7GUI
    Private Const AboutBoxText = "Chapter 7 - The Moon"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt = Nothing
    Private orbElements As ASTOrbits = Nothing
    Private observer As ASTObserver = Nothing

    ' Create a reusable query form for user input
    Private queryForm As ASTQuery = New ASTQuery()

    ' Keep track of the index into the orbital elements database for the Moon
    ' so that we don't have to get it over and over
    Private idxMoon As Integer

    ' Set a default termination criteria (in radians) for solving Kepler's equation
    Private termCriteria As Double = 0.000002

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearTextAreas()

        ' Create the menu items
        createChapMenus()

        ' Now initialize the rest of the GUI
        chkboxShowInterimCalcs.Checked = False
        chkboxDST.Checked = False
        radbtnLon.Checked = True
        radbtnEQofCenter.Checked = True

        ' Load the default orbital elements and observer location (if it exists)
        observer = New ASTObserver
        setObsDataInGUI(observer)
        orbElements = New ASTOrbits(prt)

        ' get the index into the orbital elements database for the Moon
        ' and the epoch to which the orbital elements are referenced
        idxMoon = orbElements.getOEDBMoonIndex()
        setEpoch(orbElements.getOEEpochDate)

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------------------------------------
    ' Create the Solar Info and Orbital Elements menus.
    '---------------------------------------------------------------------------------------------

    '------------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler.
    '
    ' title         title for the menu item
    ' CalcType      what type of calculation to perform
    '               when this menu item is clicked
    '------------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '--------------------------------------------
    ' Create the Lunar Info and Orbital Elements
    ' menu subitems
    '--------------------------------------------
    Private Sub createChapMenus()
        ' Create Lunar Info menu items
        mnuLunarInfo.DropDownItems.Add(createMenuItem("Moon's Position", CalculationType.MOON_LOCATION))
        mnuLunarInfo.DropDownItems.Add("-")
        mnuLunarInfo.DropDownItems.Add(createMenuItem("Moonrise/Moonset", CalculationType.MOON_RISE_SET))
        mnuLunarInfo.DropDownItems.Add("-")
        mnuLunarInfo.DropDownItems.Add(createMenuItem("Lunar Distance and Angular Diameter", CalculationType.MOON_DIST_AND_ANG_DIAMETER))
        mnuLunarInfo.DropDownItems.Add(createMenuItem("Phase of the Moon", CalculationType.MOON_PHASES_AND_AGE))
        mnuLunarInfo.DropDownItems.Add(createMenuItem("Percent of Moon Illuminated", CalculationType.MOON_PERCENT_ILLUMINATED))
        mnuLunarInfo.DropDownItems.Add("-")
        mnuLunarInfo.DropDownItems.Add(createMenuItem("Set Termination Criteria", CalculationType.TERM_CRITERIA))

        ' Create Orbital Elements menu
        mnuOrbitalElements.DropDownItems.Add(createMenuItem("Load Orbital Elements", CalculationType.LOAD_ORBITAL_ELEMENTS))
        mnuOrbitalElements.DropDownItems.Add(createMenuItem("Display Moon's Orbital Elements", CalculationType.SHOW_ORBITAL_ELEMENTS))
    End Sub

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '----------------------------------------
    ' Handle clicks on the Solar Info and
    ' Orbital Elements menu items
    '----------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        Select Case calctype
            '*************Lunar Info menu
            Case CalculationType.MOON_LOCATION
                calcMoonPosition()
            Case CalculationType.MOON_RISE_SET
                calcMoonRiseSet()
            Case CalculationType.MOON_DIST_AND_ANG_DIAMETER
                calcDistAndAngDiameter()
            Case CalculationType.MOON_PHASES_AND_AGE
                calcMoonPhase()
            Case CalculationType.MOON_PERCENT_ILLUMINATED
                calcMoonPercentIllum()
            Case CalculationType.TERM_CRITERIA
                setTerminationCriteria()

                '************* Orbital Elements menu
            Case CalculationType.LOAD_ORBITAL_ELEMENTS
                loadOrbitalElements()
            Case CalculationType.SHOW_ORBITAL_ELEMENTS
                showMoonsOrbitalElements()
        End Select

    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " &
                    "be used in subsequent calculations. You may find it convenient to enter an initial latitude, longitude, " &
                    "and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " &
                    "longitude, and time zone are already filled in with default values when the program starts. When this program " &
                    "begins, the date and time will default to the local date and time at which the program is started.")
        prt.println()
        prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " &
                    "various calculations are performed. Also, select the appropriate radio button to choose what method to " &
                    "use when it is necessary to determine the Sun's true anomaly. The radio button 'EQ of Center' will solve " &
                    "the equation of the center while the other two radio buttons solve Kepler's equation. The radio button " &
                    "'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " &
                    "Newton/Raphson method. The menu entry 'Lunar Info->Set Termination Critera' allows you to enter the " &
                    "termination criteria (in radians) for when to stop iterating to solve Kepler's equation. This radio button " &
                    "only affects how the Sun's true anomaly is computed. Finding the Moon's true anomaly is accomplished by " &
                    "solving the equation of the center regardless of which of these radio buttons is set.")
        prt.println()
        prt.println("The menu 'Lunar Info' performs various calculations related to the Moon, such as determining its " &
                    "location for the current information in the 'Observer Location and Time' data area. The menu " &
                    "'Orbital Elements' allow you to load data files that contain the Sun and Moon's orbital elements referenced to " &
                    "a standard epoch. By default, orbital elements for the standard epoch J2000 are loaded when this program " &
                    "starts up. The menu entry 'Orbital Elements->Load Orbital Elements' allows you to load and use orbital elements " &
                    "referenced to some other epoch.")
        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Lunar Info menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Calculate the Moon's distance and angular diameter.
    '------------------------------------------------------------
    Private Sub calcDistAndAngDiameter()
        Dim eclCoord As ASTCoord
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim Ecc, dF, dDist, dAngDiameter As Double
        Dim L_t1, Omega_p1, Ca1, Mmoon, Vmoon As Double

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Compute the Moon's Distance and Angular Diameter for the Current Date", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If
        Ecc = orbElements.getOEObjEccentricity(idxMoon)

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoord = orbElements.calcMoonEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                     observer.getObsDate.getYear, 0.0, L_t1, Omega_p1, Ca1, Mmoon, Vmoon,
                                                     solveTrueAnomaly, termCriteria)
        printlncond("1.  Compute the Moon's true anomaly at 0 hours UT for the given date.")
        printlncond("    Vmoon = " & angleToStr(Vmoon, DECFORMAT) & " degrees")
        printlncond()

        dF = (1 + Ecc * COS_D(Vmoon)) / (1 - Ecc * Ecc)
        printlncond("2.  Compute F = [1 + eccentricity*cos(Vmoon)] / [1-eccentricity^2]")
        printlncond("              = [1 + " & Format(Ecc, genFloatFormat) & "*cos(" & angleToStr(Vmoon, DECFORMAT) &
                    ")] / [1 - " & Format(Ecc, genFloatFormat) & "^2]")
        printlncond("              = " & Format(dF, genFloatFormat))
        printlncond()

        dDist = orbElements.getOEObjSemiMajAxisKM(idxMoon) / dF
        dDist = Round(dDist, 2)
        printlncond("3.  Compute the distance to the Moon.")
        printlncond("    Dist = a0/F where a0 is the length of the Moon's semi-major axis in km")
        printlncond("    Dist = (" & insertCommas(orbElements.getOEObjSemiMajAxisKM(idxMoon)) & ")/" & Format(dF, genFloatFormat))
        printlncond("         = " & insertCommas(dDist) & " km")
        printlncond()

        dAngDiameter = orbElements.getOEObjAngDiamDeg(idxMoon) * dF
        printlncond("4.  Compute the Moon's angular diameter.")
        printlncond("    Theta_Moon = Theta_0*F where Theta_0 is the Moon's angular diameter in degrees")
        printlncond("    when the Moon is distance a0 away.")
        printlncond("    Theta_Moon = " & angleToStr(orbElements.getOEObjAngDiamDeg(idxMoon), DECFORMAT) & "*" &
                    Format(dF, genFloatFormat) & " = " & angleToStr(dAngDiameter, DECFORMAT) & " degrees")
        printlncond()

        printlncond("5.  Convert distance to miles and angular diameter to DMS format.")
        printlncond("    Dist = " & insertCommas(dDist) & " km = " & insertCommas(Round(KM2Miles(dDist), 2)) & " miles")
        printlncond("    Theta_Moon = " & angleToStr(dAngDiameter, DMSFORMAT))
        printlncond()

        printlncond("On " & dateToStr(observer.getObsDate) & ", the Moon is/was " & insertCommas(dDist) & " km away")
        printlncond("and its angular diameter is/was " & angleToStr(dAngDiameter, DMSFORMAT))

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Moon's Distance: " & insertCommas(dDist) & " km (" & insertCommas(Round(KM2Miles(dDist), 2)) &
            " miles), Angular Diameter: " & angleToStr(dAngDiameter, DMSFORMAT)
    End Sub

    '--------------------------------------------
    ' Calculate percentage of the Moon's surface
    ' that is illuminated as seen from Earth.
    '--------------------------------------------
    Private Sub calcMoonPercentIllum()
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim eclCoordSun, eclCoordMoon As ASTCoord
        Dim Vsun, Msun As Double
        Dim L_t, Omega_p, Ca, Vmoon, Mmoon, d, dPA, dKpcnt As Double
        Dim iKpcnt As Integer

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Calculate the Percentage Illumination for the Moon as seen from Earth", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        ' How to find the Sun's true anomaly
        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoordSun = orbElements.calcSunEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                            observer.getObsDate.getYear, 0.0, Msun, Vsun, solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the Sun's ecliptic longitude for the date in question.")
        printlncond("    Lsun = " & angleToStr(eclCoordSun.getLonAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        eclCoordMoon = orbElements.calcMoonEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                         observer.getObsDate.getYear, 0.0, L_t, Omega_p, Ca, Mmoon, Vmoon,
                                                         solveTrueAnomaly, termCriteria)
        printlncond("2.  Calculate the Moon's mean anomaly and ecliptic coordinates for the date in question.")
        printlncond("       Mm = " & angleToStr(Mmoon, DECFORMAT) & " degrees")
        printlncond("    Bmoon = " & angleToStr(eclCoordMoon.getLatAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond("    Lmoon = " & angleToStr(eclCoordMoon.getLonAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        d = COS_D(eclCoordMoon.getLonAngle.getDecAngle - eclCoordSun.getLonAngle.getDecAngle) * COS_D(eclCoordMoon.getLatAngle.getDecAngle)
        d = INVCOS_D(d)
        printlncond("3.  Calculate the Moon's age in degrees.")
        printlncond("    d = inv cos[cos(Lmoon - Lsun)*cos(Bmoon)]")
        printlncond("      = inv cos[cos(" & angleToStr(eclCoordMoon.getLonAngle.getDecAngle, DECFORMAT) & " - " &
                    angleToStr(eclCoordSun.getLonAngle.getDecAngle, DECFORMAT) & ")*cos(" &
                    angleToStr(eclCoordMoon.getLatAngle.getDecAngle, DECFORMAT) & ")]")
        printlncond("      = " & angleToStr(d, DECFORMAT) & " degrees")
        printlncond()

        dPA = (1 - 0.0549 * SIN_D(Mmoon)) / (1 - 0.0167 * SIN_D(Mmoon))
        dPA = 180.0 - d - 0.1468 * dPA * SIN_D(d)
        printlncond("4.  Calculate the Moon's phase angle.")
        printlncond("    PA = 180 - d - 0.1468*[(1-0.0549*sin(Mm)) / (1-0.0167*sin(Mm))]*sin(d)")
        printlncond("       = 180-" & angleToStr(d, DECFORMAT) & "-0.1468*[(1-0.0549*sin(" & angleToStr(Mmoon, DECFORMAT) &
                    "))/(1-0.0167*sin(" & angleToStr(Mmoon, DECFORMAT) & "))]*sin(" & angleToStr(d, DECFORMAT) & ")")
        printlncond("       = " & angleToStr(dPA, DECFORMAT) & " degrees")
        printlncond()

        dKpcnt = 50 * (1 + COS_D(dPA))
        printlncond("5.  Calculate the percent illumination.")
        printlncond("    K% = 100*[(1 - cos(PA))/2] = 0*[(1 - cos(" & angleToStr(dPA, DECFORMAT) & "))/2]")
        printlncond("       = " & Format(dKpcnt, genFloatFormat))
        printlncond()
        iKpcnt = CInt(Round(dKpcnt))

        printlncond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear) &
                    ", the Moon is " & iKpcnt & "% illuminated as seen from Earth")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "On " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear) &
            ", the Moon is " & iKpcnt & "% illuminated."
    End Sub

    '------------------------------------
    ' Calculate the Moon's phase and age
    '------------------------------------
    Private Sub calcMoonPhase()
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim eclCoordSun, eclCoordMoon As ASTCoord
        Dim Vsun, Msun As Double
        Dim L_t, Omega_p, Ca, Mmoon, Vmoon, Age, dF As Double

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Calculate the Moon's Phase and Age", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        ' How to find the Sun's true anomaly
        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If

        ' Technically, we should be using the UT for the observer rather than UT=0, but
        ' the difference is so small that it isn't worth converting LCT to UT
        eclCoordSun = orbElements.calcSunEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                            observer.getObsDate.getYear, 0.0, Msun, Vsun, solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the Sun's ecliptic longitude for the date in question.")
        printlncond("    Lsun = " & angleToStr(eclCoordSun.getLonAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        eclCoordMoon = orbElements.calcMoonEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                         observer.getObsDate.getYear, 0.0, L_t, Omega_p, Ca, Mmoon, Vmoon,
                                                         solveTrueAnomaly, termCriteria)
        printlncond("2.  Calculate the Moon's true ecliptic longitude for the date in question.")
        printlncond("    L_t = " & angleToStr(L_t, DECFORMAT) & " degrees")
        printlncond()

        Age = L_t - eclCoordSun.getLonAngle.getDecAngle
        printlncond("3.  Calculate the Moon's age in degrees.")
        printlncond("    Age = L_t - Lsun = " & angleToStr(L_t, DECFORMAT) & " - " & angleToStr(eclCoordSun.getLonAngle.getDecAngle, DECFORMAT) &
                    " = " & angleToStr(Age, DECFORMAT) & " degrees")
        printlncond()

        Age = xMOD(Age, 360)
        printlncond("4.  If necessary, adjust Age to be in the range [0, 360]")
        printlncond("    Age = " & angleToStr(Age, DECFORMAT) & " degrees")
        printlncond("    Note: The Moon's age in days is Age/12.1907 = " & Format(Age / 12.1907, genFloatFormat) & " days")
        printlncond()

        dF = (1 - COS_D(Age)) / 2.0
        dF = Round(dF, 2)
        Age = Round(Age, 2)
        printlncond("5.  Calculate the Moon's phase.")
        printlncond("    F = (1 - cos(Age))/2 = (1 - cos(" & angleToStr(Age, DECFORMAT) & "))/2 = " & Format(dF, genFloatFormat))
        printlncond()

        printlncond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                            observer.getObsDate.getYear) & " for the current observer")
        printlncond("the Moon's age is " & Format(Age, genFloatFormat) & " degrees (F = " & Format(dF, genFloatFormat) &
                    "), which is closest to a " & ageToStr(Age) & " Moon.")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "On " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear) &
            ", Age = " & Format(Age, genFloatFormat) & " deg, F = " & Format(dF, genFloatFormat) &
            " (closest to a " & ageToStr(Age) & " Moon)"

    End Sub

    '------------------------------------------------------------
    ' Calculate the position of the Moon, using the currently
    ' loaded orbital elements and the current observer position.
    '------------------------------------------------------------
    Private Sub calcMoonPosition()
        Dim result As String
        Dim LCT, UT, LST, GST, TT, JD, JDe As Double
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate
        Dim De, Msun, Vsun, Lsun As Double
        Dim L_uncor, L_p, L_t, Omega, Omega_p, Mm, Ae, Ev, Ca, Vmoon, V As Double
        Dim x, y, dT, inclin, Lmoon, Bmoon As Double
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim eqCoord, eclCoord, horizonCoord As ASTCoord

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Compute the Moon's Position for the Current Observer", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If
        inclin = orbElements.getOEObjInclination(idxMoon)

        ' Do all the time-related calculations
        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)

        printlncond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
        printlncond("    LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(observer.getObsDate) & ")")
        printcond("     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond("previous day ")
        ElseIf dateAdjust > 0 Then
            printcond("next day ")
        End If
        printlncond(dateToStr(adjustedDate) & ")")
        printlncond("    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond("    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        TT = UT + (63.8 / 3600.0)
        printlncond("2.  Compute TT = UT + 63.8s = " & timeToStr(UT, DECFORMAT) & " + 63.8s = " &
                    timeToStr(TT, DECFORMAT) & " hours")
        printlncond()

        JDe = orbElements.getOEEpochJD
        printlncond("3.  Compute the Julian day number for the standard epoch.")
        printlncond("    Epoch: " & Format(orbElements.getOEEpochDate, epochFormat))
        printlncond("    JDe = " & Format(JDe, JDFormat))
        printlncond()

        JD = dateToJD(adjustedDate.getMonth, adjustedDate.getdDay + (TT / 24.0), adjustedDate.getYear)
        printlncond("4.  Compute the Julian day number for the desired date, being sure to use the")
        printlncond("    Greenwich date and TT from steps 1 and 2, and including the fractional part of the day.")
        printlncond("    JD = " & Format(JD, JDFormat))
        printlncond()

        De = JD - JDe
        printlncond("5.  Compute the total number of elapsed days, including fractional days,")
        printlncond("    since the standard epoch.")
        printlncond("    De = JD - JDe = " & Format(JD, JDFormat) & " - " & Format(JDe, JDFormat))
        printlncond("       = " & Format(De, genFloatFormat) & " days")
        printlncond()

        ' Use UT to get the Sun's position, not TT
        eclCoord = orbElements.calcSunEclipticCoord(adjustedDate.getMonth, adjustedDate.getiDay,
                                            adjustedDate.getYear, UT, Msun, Vsun, solveTrueAnomaly, termCriteria)
        Lsun = eclCoord.getLonAngle.getDecAngle
        printlncond("6.  Calculate the Sun's ecliptic longitude and mean anomaly for the UT date and UT time.")
        printlncond("    Lsun = " & angleToStr(Lsun, DECFORMAT) & " degrees")
        printlncond("    Msun = " & angleToStr(Msun, DECFORMAT) & " degrees")
        printlncond()

        L_uncor = 13.176339686 * De + orbElements.getOEObjLonAtEpoch(idxMoon)
        printlncond("7.  Calculate the Moon's uncorrected mean ecliptic longitude.")
        printlncond("    L_uncor = 13.176339686 * De + L_0 = 13.176339686 * " & Format(De, genFloatFormat) &
                    " + " & angleToStr(orbElements.getOEObjLonAtEpoch(idxMoon), DECFORMAT))
        printlncond("            = " & angleToStr(L_uncor, DECFORMAT) & " degrees")
        printlncond()

        printlncond("8.  If necessary, use the MOD function to adjust L_uncor to the range [0, 360].")
        printcond("    L_uncor = L_uncor MOD 360 = " & angleToStr(L_uncor, DECFORMAT) & " MOD 360 = ")
        L_uncor = xMOD(L_uncor, 360.0)
        printlncond(angleToStr(L_uncor, DECFORMAT) & " degrees")
        printlncond()

        Omega = orbElements.getOEObjLonAscNode(idxMoon) - 0.0529539 * De
        printlncond("9.  Calculate the Moon's uncorrected mean longitude of the ascending node.")
        printlncond("    Omega = Omega_0 - 0.0529539 * De = " &
                    angleToStr(orbElements.getOEObjLonAscNode(idxMoon), DECFORMAT) & " - 0.0529539 * " &
                    Format(De, genFloatFormat))
        printlncond("          = " & angleToStr(Omega, DECFORMAT) & " degrees")
        printlncond()

        printlncond("10. If necessary, adjust Omega to be in the range [0, 360].")
        printcond("    Omega = Omega MOD 360 = " & angleToStr(Omega, DECFORMAT) & " MOD 360 = ")
        Omega = xMOD(Omega, 360.0)
        printlncond(angleToStr(Omega, DECFORMAT) & " degrees")
        printlncond()

        Mm = L_uncor - 0.1114041 * De - orbElements.getOEObjLonAtPeri(idxMoon)
        printlncond("11. Compute the Moon's uncorrected mean anomaly.")
        printlncond("    Mm = L_uncor - 0.1114041*De - W_0 = " & angleToStr(L_uncor, DECFORMAT) & "-0.1114041*(" &
                    Format(De, genFloatFormat) & ")-" & angleToStr(orbElements.getOEObjLonAtPeri(idxMoon), DECFORMAT))
        printlncond("       = " & angleToStr(Mm, DECFORMAT) & " degrees")
        printlncond()

        printlncond("12. Adjust Mm if necessary to be in the range [0, 360].")
        printcond("    Mm = Mm MOD 360 = " & angleToStr(Mm, DECFORMAT) & " MOD 360 = ")
        Mm = xMOD(Mm, 360.0)
        printlncond(angleToStr(Mm, DECFORMAT) & " degrees")
        printlncond()

        Ae = 0.1858 * SIN_D(Msun)
        printlncond("13. Calculate the annual equation correction.")
        printlncond("    Ae = 0.1858 * sin(Msun) = 0.1858 * sin(" & angleToStr(Msun, DECFORMAT) & ") = " &
                    angleToStr(Ae, DECFORMAT) & " degrees")
        printlncond()

        Ev = 1.2739 * SIN_D(2 * (L_uncor - Lsun) - Mm)
        printlncond("14. Calculate the evection correction.")
        printlncond("    Ev = 1.2739 * sin[2 * (L_uncor - Lsun) - Mm]")
        printlncond("       = 1.2739 * sin[2 * (" & angleToStr(L_uncor, DECFORMAT) & " - " &
                    angleToStr(Lsun, DECFORMAT) & ") - " & angleToStr(Mm, DECFORMAT) & "]")
        printlncond("       = " & angleToStr(Ev, DECFORMAT) & " degrees")
        printlncond()

        Ca = Mm + Ev - Ae - 0.37 * SIN_D(Msun)
        printlncond("15. Calculate the mean anomaly correction.")
        printlncond("    Ca = Mm + Ev - Ae - 0.37 * sin(Msun)")
        printlncond("       = " & angleToStr(Mm, DECFORMAT) & " + " & angleToStr(Ev, DECFORMAT) &
                    " - " & angleToStr(Ae, DECFORMAT) & " - 0.37 * sin(" & angleToStr(Msun, DECFORMAT) & ")")
        printlncond("       = " & angleToStr(Ca, DECFORMAT) & " degrees")
        printlncond()

        Vmoon = 6.2886 * SIN_D(Ca) + 0.214 * SIN_D(2 * Ca)
        printlncond("16. Calculate the Moon's true anomaly.")
        printlncond("    Vmoon = 6.2886 * sin(Ca) + 0.214 * sin(2*Ca)")
        printlncond("          = 6.2886 * sin(" & angleToStr(Ca, DECFORMAT) & ") + 0.214 * sin(2*" & angleToStr(Ca, DECFORMAT) & ")")
        printlncond("          = " & angleToStr(Vmoon, DECFORMAT) & " degrees")
        printlncond()

        L_p = L_uncor + Ev + Vmoon - Ae
        printlncond("17. Calculate the corrected ecliptic longitude.")
        printlncond("    L_p = L_uncor + Ev + Vmoon - Ae = " & angleToStr(L_uncor, DECFORMAT) & "+" &
                    angleToStr(Ev, DECFORMAT) & "+(" & angleToStr(Vmoon, DECFORMAT) & ")-(" & angleToStr(Ae, DECFORMAT) & ")")
        printlncond("        = " & angleToStr(L_p, DECFORMAT) & " degrees")
        printlncond()

        V = 0.6583 * SIN_D(2 * (L_p - Lsun))
        printlncond("18. Compute the variation correction.")
        printlncond("    V = 0.6583 * sin[2 * (L_p - Lsun)] = 0.6583 * sin[2 * (" &
                    angleToStr(L_p, DECFORMAT) & " - " & angleToStr(Lsun, DECFORMAT) & ")]")
        printlncond("      = " & angleToStr(V, DECFORMAT) & " degrees")
        printlncond()

        L_t = L_p + V
        printlncond("19. Calculate the Moon's true ecliptic longitude.")
        printlncond("    L_t = L_p + V = " & angleToStr(L_p, DECFORMAT) & " + " &
                    angleToStr(V, DECFORMAT) & " = " & angleToStr(L_t, DECFORMAT) & " degrees")
        printlncond()

        Omega_p = Omega - 0.16 * SIN_D(Msun)
        printlncond("20. Compute a corrected ecliptic longitude of the ascending node.")
        printlncond("    Omega_p = Omega - 0.16 * sin(Msun) = " & angleToStr(Omega, DECFORMAT) & " - 0.16 * sin(" &
                    angleToStr(Msun, DECFORMAT) & ")")
        printlncond("            = " & angleToStr(Omega_p, DECFORMAT) & " degrees")
        printlncond()

        y = SIN_D(L_t - Omega_p) * COS_D(inclin)
        printlncond("21. Calculate y = sin(L_t - Omega_p) * cos(inclination)")
        printlncond("                = sin(" & angleToStr(L_t, DECFORMAT) & " - " &
                    angleToStr(Omega_p, DECFORMAT) & ") * cos( " & angleToStr(inclin, DECFORMAT) & ")")
        printlncond("                = " & Format(y, genFloatFormat))
        printlncond()

        x = COS_D(L_t - Omega_p)
        printlncond("22. Calculate x = cos(L_t - Omega_p) = cos(" & angleToStr(L_t, DECFORMAT) & " - " &
                    angleToStr(Omega_p, DECFORMAT) & ") = " & Format(x, genFloatFormat))
        printlncond()

        dT = INVTAN_D(y / x)
        printlncond("23. Compute T = inv tan(y/x) = inv tan(" & Format(y, genFloatFormat) & " / " &
                    Format(x, genFloatFormat) & ") = " & angleToStr(dT, DECFORMAT) & " degrees")
        printlncond()

        x = quadAdjust(y, x)
        printlncond("24. Use the algebraic signs of y and x to determine a quadrant adjustment and apply it to T.")
        printcond("    T = T + Adjustment = " & angleToStr(dT, DECFORMAT) & " + " & angleToStr(x, DECFORMAT) & " = ")
        dT = dT + x
        printlncond(angleToStr(dT, DECFORMAT) & " degrees")
        printlncond()

        Lmoon = Omega_p + dT
        printlncond("25. Calculate the Moon's ecliptic longitude.")
        printlncond("    Lmoon = Omega_p + T = " & angleToStr(Omega_p, DECFORMAT) & " + " & angleToStr(dT, DECFORMAT) &
                    " = " & angleToStr(Lmoon, DECFORMAT) & " degrees")
        printlncond()

        If Lmoon > 360 Then Lmoon = Lmoon - 360
        printlncond("26. If Lmoon > 360, then subtract 360.")
        printlncond("    Lmoon = " & angleToStr(Lmoon, DECFORMAT) & " degrees")
        printlncond()

        Bmoon = SIN_D(L_t - Omega_p) * SIN_D(inclin)
        Bmoon = INVSIN_D(Bmoon)
        printlncond("27. Calculate the Moon's ecliptic latitude.")
        printlncond("    Bmoon = inv sin[sin(L_t - Omega_p) * sin(inclination)]")
        printlncond("          = inv sin[sin(" & angleToStr(L_t, DECFORMAT) & " - " &
                    angleToStr(Omega_p, DECFORMAT) & ") * sin(" & angleToStr(inclin, DECFORMAT) & ")]")
        printlncond("          = " & angleToStr(Bmoon, DECFORMAT) & " degrees")
        printlncond()

        eqCoord = EclipticToEquatorial(Bmoon, Lmoon, orbElements.getOEEpochDate)
        printlncond("28. Convert the Moon's ecliptic coordinates (Bmoon, Lmoon) to equatorial coordinates.")
        printlncond("    RA = " & timeToStr(eqCoord.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl = " &
                    angleToStr(eqCoord.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        horizonCoord = RADecltoHorizon(eqCoord.getRAAngle.getDecTime, eqCoord.getDeclAngle.getDecAngle,
                                observer.getObsLat, LST)
        printlncond("29. Convert the Moon's equatorial coordinates to horizon coordinates.")
        printlncond("    Alt = " & angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & ", Az = " &
                    angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT))
        printlncond()

        result = angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & " Alt, " &
            angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT) & " Az"
        printlncond("Thus, for this observer location and date/time, the Moon")
        printlncond("is at " & result & ".")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Moon's Location is " & result
    End Sub

    '------------------------------------------------------------
    ' Calculate the times of moonrise and moonset.
    '------------------------------------------------------------
    Private Sub calcMoonRiseSet()
        Dim dateAdjust As Integer
        Dim LCTr, LCTs, GST, UT As Double
        Dim L_t1, Omega_p1, Ca1 As Double
        Dim Bmoon2, Lmoon2, Mmoon, Vmoon As Double
        Dim eclCoord, eqCoord1, eqCoord2 As ASTCoord
        Dim solveTrueAnomaly As TrueAnomalyType
        Dim LSTTimes(1) As Double
        Dim ST1r, ST1s, ST2r, ST2s, Tr, Ts As Double
        Dim riseSet As Boolean = True
        Dim crossedDate As Boolean = False
        Dim result As String

        ' Validate the observer location data and be sure orbital elements are loaded
        If Not validateGUIObsLoc() Then Return
        If Not checkOEDBLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Calculate Moonrise and Moonset for the Current Observer", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        ' How to find the Sun's true anomaly
        If radbtnSimpleIteration.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        ElseIf radbtnNewtonMethod.Checked Then
            solveTrueAnomaly = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        Else
            solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER
        End If

        eclCoord = orbElements.calcMoonEclipticCoord(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                                     observer.getObsDate.getYear, 0.0, L_t1, Omega_p1, Ca1, Mmoon, Vmoon,
                                                     solveTrueAnomaly, termCriteria)
        printlncond("1.  Calculate the Moon's ecliptic location at midnight for the date in question,")
        printlncond("    as well as the true ecliptic longitude (L_t1), corrected ecliptic longitude of")
        printlncond("    the ascending node(Omega_p1), and mean anomaly correction (Ca1)")
        printlncond("    For UT=0 hours on the date " &
                    dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay, observer.getObsDate.getYear))
        printlncond("    The Moon's coordinates are Bmoon1 = " & angleToStr(eclCoord.getLatAngle.getDecAngle, DECFORMAT) &
                    " degrees, Lmoon1 = " & angleToStr(eclCoord.getLonAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond("    The Moon's true ecliptic longitude L_t1 = " & angleToStr(L_t1, DECFORMAT) & " degrees")
        printlncond("    The corrected longitude of the ascending node Omega_p1 = " & angleToStr(Omega_p1, DECFORMAT) & " degrees")
        printlncond("    The mean anomaly correction Ca1 = " & angleToStr(Ca1, DECFORMAT) & " degrees")
        printlncond()

        eqCoord1 = EclipticToEquatorial(eclCoord.getLatAngle.getDecAngle, eclCoord.getLonAngle.getDecAngle,
                                        orbElements.getOEEpochDate)
        printlncond("2.  Convert the Moon's ecliptic coordinates to equatorial coordinates.")
        printlncond("    RA1 = " & timeToStr(eqCoord1.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl1 = " &
                    angleToStr(eqCoord1.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        LSTTimes = calcRiseSetTimes(eqCoord1.getRAAngle.getDecTime, eqCoord1.getDeclAngle.getDecAngle, observer.getObsLat, riseSet)
        ST1r = LSTTimes(0)
        ST1s = LSTTimes(1)
        printlncond("3.  Using the equatorial coordinates from step 2, compute the")
        printlncond("    LST rising and setting times.")
        If Not riseSet Then
            printlncond("    The Moon does not rise or set for this observer.")
            Return
        End If
        printlncond("    ST1r = " & timeToStr(ST1r, DECFORMAT) & " hours")
        printlncond("    ST1s = " & timeToStr(ST1s, DECFORMAT) & " hours")
        printlncond()

        Bmoon2 = eclCoord.getLatAngle.getDecAngle + 0.05 * COS_D(L_t1 - Omega_p1) * 12.0
        printlncond("4.  Use L_t1, Omega_p1, and Bmoon1 to compute the Moon's ecliptic latitude 12 hours later.")
        printlncond("    Bmoon2 = Bmoon1 + 0.05*cos(L_t1 - Omega_p1)*12")
        printlncond("           = " & angleToStr(eclCoord.getLatAngle.getDecAngle, DECFORMAT) &
                    " + 0.05*cos(" & angleToStr(L_t1, DECFORMAT) & " - " & angleToStr(Omega_p1, DECFORMAT) & ")*12")
        printlncond("           = " & angleToStr(Bmoon2, DECFORMAT) & " degrees")
        printlncond()

        Lmoon2 = eclCoord.getLonAngle.getDecAngle + (0.55 + 0.06 * COS_D(Ca1)) * 12.0
        printlncond("5.  Use Ca1 and Lmoon1 to compute the Moon's ecliptic longitude 12 hours later.")
        printlncond("    Lmoon2 = Lmoon1 + [0.55 + 0.06*cos(Ca1)]*12")
        printlncond("           = " & angleToStr(eclCoord.getLonAngle.getDecAngle, DECFORMAT) &
                    " + [0.55 + 0.06*cos(" & angleToStr(Ca1, DECFORMAT) & ")]*12")
        printlncond("           = " & angleToStr(Lmoon2, DECFORMAT) & " degrees")
        printlncond()

        If Lmoon2 > 360.0 Then Lmoon2 = Lmoon2 - 360.0
        printlncond("6.  If Lmoon2 > 360.0, subtract 360 degrees.")
        printlncond("    Lmoon2 = " & angleToStr(Lmoon2, DECFORMAT) & " degrees")
        printlncond()

        eqCoord2 = EclipticToEquatorial(Bmoon2, Lmoon2, orbElements.getOEEpochDate)
        printlncond("7.  Convert the ecliptic coordinates from the previous step to equatorial coordinates.")
        printlncond("    RA2 = " & timeToStr(eqCoord2.getRAAngle.getDecTime, DECFORMAT) & " hours, Decl2 = " &
                    angleToStr(eqCoord2.getDeclAngle.getDecAngle, DECFORMAT) & " degrees")
        printlncond()

        LSTTimes = calcRiseSetTimes(eqCoord2.getRAAngle.getDecTime, eqCoord2.getDeclAngle.getDecAngle, observer.getObsLat, riseSet)
        ST2r = LSTTimes(0)
        ST2s = LSTTimes(1)
        printlncond("8.  Using the equatorial coordinates from step 7, compute the")
        printlncond("    second set of LST rising and setting times.")
        If Not riseSet Then
            printlncond("    The Moon does not rise or set for this observer.")
            Return
        End If
        printlncond("    ST2r = " & timeToStr(ST2r, DECFORMAT) & " hours")
        printlncond("    ST2s = " & timeToStr(ST2s, DECFORMAT) & " hours")
        printlncond()

        Tr = (12.03 * ST1r) / (12.03 + ST1r - ST2r)
        printlncond("9.  Interpolate the two sets of LST rising times.")
        printlncond("    Tr = (12.03 * ST1r) / (12.03 + ST1r - ST2r)")
        printlncond("       = (12.03*" & timeToStr(ST1r, DECFORMAT) & ")/(12.03+" & timeToStr(ST1r, DECFORMAT) &
                    "-" & timeToStr(ST2r, DECFORMAT) & ")")
        printlncond("       = " & timeToStr(Tr, DECFORMAT) & " hours")
        printlncond()

        Ts = (12.03 * ST1s) / (12.03 + ST1s - ST2s)
        printlncond("10. Interpolate the two sets of LST setting times.")
        printlncond("    Ts = (12.03 * ST1s) / (12.03 + ST1s - ST2s)")
        printlncond("       = (12.03*" & timeToStr(ST1s, DECFORMAT) & ")/(12.03+" & timeToStr(ST1s, DECFORMAT) &
                    "-" & timeToStr(ST2s, DECFORMAT) & ")")
        printlncond("       = " & timeToStr(Ts, DECFORMAT) & " hours")
        printlncond()

        GST = LSTtoGST(Tr, observer.getObsLon)
        UT = GSTtoUT(GST, observer.getObsDate)
        LCTr = UTtoLCT(UT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        crossedDate = (dateAdjust <> 0)

        GST = LSTtoGST(Ts, observer.getObsLon)
        UT = GSTtoUT(GST, observer.getObsDate)
        LCTs = UTtoLCT(UT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        crossedDate = crossedDate Or (dateAdjust <> 0)

        printlncond("11. Convert the LST rising/setting times to their corresponding LCT times.")
        printlncond("    LCTr = " & timeToStr(LCTr, DECFORMAT) & " hours, LCTs = " &
                    timeToStr(LCTs, DECFORMAT) & " hours")
        If (crossedDate) Then
            printlncond("    WARNING: Converting LST to LCT crossed a date boundary, so the results are likely")
            printlncond("             inaccurate. Try the date before and the date after the given date,")
            printlncond("             then average the results to get a more accurate estimate.")
        End If
        printlncond()

        printlncond("12. Convert the LCT times to HMS format.")
        printlncond("    LCTr = " & timeToStr(LCTr, HMSFORMAT) & ", LCTs = " & timeToStr(LCTs, HMSFORMAT))
        printlncond()

        printlncond("Thus, on " & dateToStr(observer.getObsDate.getMonth, observer.getObsDate.getiDay,
                                            observer.getObsDate.getYear) & " for the current observer")
        printlncond("the Moon will rise at " & timeToStr(LCTr, HMSFORMAT) & " LCT and set at " &
                    timeToStr(LCTs, HMSFORMAT) & " LCT")


        prt.setProportionalFont()
        prt.resetCursor()

        result = "Moonrise: " & timeToStr(LCTr, HMSFORMAT) & " LCT, Moonset: " & timeToStr(LCTs, HMSFORMAT) & " LCT"
        If crossedDate Then
            result = result & " (Moonset on day before Moonrise-Try new dates)"
        ElseIf (LCTr > LCTs) Then
            result = result & " (next day)"
        End If
        lblResults.Text = result
    End Sub

    '------------------------------------------------------------
    ' Set the termination criteria for solving Kepler's equation
    '------------------------------------------------------------
    Private Sub setTerminationCriteria()
        Dim dTerm As Double = termCriteria

        clearTextAreas()
        prt.setBoldFont(True)
        prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        If queryForm.showQueryForm("Enter Termination Criteria in radians" & vbNewLine &
                                   "(ex: 0.000002)") <> Windows.Forms.DialogResult.OK Then
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        ' Validate the termination criteria
        If Not isValidReal(queryForm.getData1(), dTerm, HIDE_ERRORS) Then
            ErrMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        prt.println("Prior termination criteria was: " & termCriteria & " radians")
        If dTerm < KeplerMinCriteria Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too small, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        If dTerm > 1 Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too large, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        termCriteria = dTerm

        prt.println("Termination criteria is now set to " & termCriteria & " radians")
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Orbital Elements menu items
    '----------------------------------------------------------------------------------------------------

    '------------------------------------------------
    ' Load orbital elements from disk
    '------------------------------------------------
    Private Sub loadOrbitalElements()
        Dim fullFilename As String = " "

        clearTextAreas()

        If checkOEDBLoaded(HIDE_ERRORS) Then
            If PleaseConfirm("Orbital Elements have already been loaded. Are you" & vbNewLine &
                              "sure you want to load a new set of orbital elements?", "Clear Orbital Elements Data") Then
                setEpoch(DEFAULT_EPOCH)
                idxMoon = -1
                prt.println("The currently loaded orbital elements were cleared ...")
            Else
                prt.println("The currently loaded orbital elements were not cleared ...")
                Return
            End If
        End If

        If Not getOEDBFileToOpen(fullFilename) Then Return

        If orbElements.loadOEDB(fullFilename) Then
            prt.println("New orbital elements have been successfully loaded ...")
            setEpoch(orbElements.getOEEpochDate)
            idxMoon = orbElements.findOrbElementObjIndex("Moon")
        End If
        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Shows the Moon's orbital elements and other data from whatever
    ' orbital elements database is currently loaded.
    '-----------------------------------------------------------------
    Private Sub showMoonsOrbitalElements()
        If Not checkOEDBLoaded() Then Return

        prt.clearTextArea()
        orbElements.displayObjOrbElements(idxMoon)
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------------------
    ' Convert the Moon's age into a printable string
    '
    ' Age           Moon's age in degrees
    '
    ' Returns New Moon, First Quarter, etc. as a string
    '---------------------------------------------------
    Private Function ageToStr(ByVal Age As Double) As String
        If Age < 45 - 0.5 * 45 Then
            Return "New"
        ElseIf Age < 90 - 0.5 * (90 - 45) Then
            Return "Waxing Crescent"
        ElseIf Age < 135 - 0.5 * (135 - 90) Then
            Return "First Quarter"
        ElseIf Age < 180 - 0.5 * (180 - 135) Then
            Return "Waxing Gibbous"
        ElseIf Age < 225 - 0.5 * (225 - 180) Then
            Return "Full"
        ElseIf Age < 270 - 0.5 * (270 - 225) Then
            Return "Waning Gibbous"
        ElseIf Age < 315 - 0.5 * (315 - 270) Then
            Return "Last Quarter"
        ElseIf Age < 360 - 0.5 * (360 - 315) Then
            Return "Waning Crescent"
        Else
            Return "New"
        End If

    End Function

    '-----------------------------------------------------------
    ' Checks to see if an orbital elements database has been
    ' successfully loaded, and display an error message if not.
    '
    ' showErrors        display error message if true
    '-----------------------------------------------------------
    Private Function checkOEDBLoaded() As Boolean
        Return checkOEDBLoaded(SHOW_ERRORS)
    End Function
    Private Function checkOEDBLoaded(ByVal showErrors As Boolean) As Boolean
        If Not orbElements.isOrbElementsDBLoaded() Then
            If showErrors Then ErrMsg("No Orbital Elements data is currently loaded.", "No Orbital Elements Data Loaded")
            Return False
        End If
        Return True
    End Function

    '--------------------------------------
    ' Clear the text areas in the GUI
    '--------------------------------------
    Private Sub clearTextAreas()
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '----------------------------------------------------
    ' Determine what time zone radio button is selected
    '
    ' Returns a time zone type for whatever is the
    ' currently selected time zone radio button
    '----------------------------------------------------
    Private Function getSelectedRBStatus() As TimeZoneType
        If radbtnPST.Checked() Then
            Return TimeZoneType.PST
        ElseIf radbtnMST.Checked() Then
            Return TimeZoneType.MST
        ElseIf radbtnCST.Checked() Then
            Return TimeZoneType.CST
        ElseIf radbtnEST.Checked() Then
            Return TimeZoneType.EST
        Else
            Return TimeZoneType.LONGITUDE
        End If
    End Function

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '-----------------------------------
    ' Sets the Epoch label in the GUI.
    '
    ' epoch         Epoch to display
    '-----------------------------------
    Private Sub setEpoch(ByVal epoch As Double)
        lblEpoch.Text = "Epoch: " & Format(epoch, epochFormat)
    End Sub

    '---------------------------------------------------------------
    ' Sets the observer location in the GUI. This is intended to be
    ' done one time only during the program initialization since a
    ' default location may be read from a data file.
    '
    ' obs           observer location object
    '---------------------------------------------------------------
    Private Sub setObsDataInGUI(ByVal obs As ASTObserver)
        txtboxLat.Text = latToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxLon.Text = lonToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxDate.Text = dateToStr(obs.getObsDate())
        txtboxLCT.Text = timeToStr(obs.getObsTime(), HMSFORMAT)

        If obs.getObsTimeZone = TimeZoneType.PST Then
            radbtnPST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.MST Then
            radbtnMST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.CST Then
            radbtnCST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.EST Then
            radbtnEST.Checked = True
        Else
            radbtnLon.Checked = True
        End If

    End Sub

    '----------------------------------------------
    ' See if the observer location, date, and time
    ' currently in the GUI is valid.
    '
    ' Returns true if valid, otherwise false.
    '----------------------------------------------
    Private Function validateGUIObsLoc() As Boolean
        Return isValidObsLoc(observer, txtboxLat.Text, txtboxLon.Text, getSelectedRBStatus().ToString,
                             txtboxDate.Text, txtboxLCT.Text)
    End Function

End Class
