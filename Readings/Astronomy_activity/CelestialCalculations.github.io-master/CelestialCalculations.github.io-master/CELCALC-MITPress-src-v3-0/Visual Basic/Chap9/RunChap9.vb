﻿'***********************************************************************
'                         Chapter 9 - Satellites
'                            Copyright (c) 2018
'                          Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' for a program that will calculate various items about
' satellites, including their location and times for rising/setting.
' The main GUI was mostly built with the Visual Basic form editor.
'***********************************************************************

Option Explicit On
Option Strict On

Imports System.IO
Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTCoord
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTObserver
Imports ASTUtils.ASTOrbits
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTSites
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime
Imports ASTUtils.ASTTLE
Imports ASTUtils.ASTVect

Imports RunChap9.ChapMenuItem

Public Class Chap9GUI
    Private Const AboutBoxText = "Chapter 9 - Satellites"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt = Nothing
    Private observer As ASTObserver = Nothing

    ' Create reusable query forms for user input
    Private queryForm As ASTQuery = New ASTQuery()
    Private coords3Form As Form3Coords = New Form3Coords()
    Private coords6Form As Form6Coords = New Form6Coords()

    ' Set a default termination criteria (in radians) for solving Kepler's equation
    Private termCriteria As Double = 0.000002

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearTextAreas()

        ' Create the menu items
        createChapMenus()

        ' Set up the TLEs and launch sites databases
        clearTLEData()
        clearLaunchSites()

        ' Now initialize the rest of the GUI
        chkboxShowInterimCalcs.Checked = False
        chkboxDST.Checked = False
        radbtnLon.Checked = True
        radbtnSimpleIteration.Checked = True

        ' Load the default observer location (if it exists)
        observer = New ASTObserver
        setObsDataInGUI(observer)

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '---------------------------------------------------------------------------------------------
    ' Create the menus for this chapter
    '---------------------------------------------------------------------------------------------

    '------------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler.
    '
    ' title         title for the menu item
    ' CalcType      what type of calculation to perform
    '               when this menu item is clicked
    '------------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '--------------------------------------------
    ' Create the Coord Sys, Launch Sites, TLEs,
    ' and Satellites menu subitems
    '--------------------------------------------
    Private Sub createChapMenus()
        Dim tmpSubMenu As ToolStripMenuItem

        ' Create Coord Sys menu items
        mnuCoordSys.DropDownItems.Add(createMenuItem("Cartesian to Spherical", CalculationType.CART_TO_SPHERICAL))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Spherical to Cartesian", CalculationType.SPHERICAL_TO_CART))
        mnuCoordSys.DropDownItems.Add("-")
        mnuCoordSys.DropDownItems.Add(createMenuItem("ECI to Horizon Coordinates", CalculationType.ECI_TO_HORIZON))
        mnuCoordSys.DropDownItems.Add(createMenuItem("ECI to Topocentric Coordinates", CalculationType.ECI_TO_TOPO))
        mnuCoordSys.DropDownItems.Add("-")
        mnuCoordSys.DropDownItems.Add(createMenuItem("State Vector to Keplerian Elements", CalculationType.STATEVECTOR_TO_KEPLERIAN))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Keplerian Elements to State Vector", CalculationType.KEPLERIAN_TO_STATEVECTOR))

        ' Create TLEs menu items
        mnuTLEs.DropDownItems.Add(createMenuItem("Load TLE Data", CalculationType.LOAD_TLE_DATA))
        mnuTLEs.DropDownItems.Add(createMenuItem("Display Raw TLE Data", CalculationType.DISPLAY_RAW_TLE_DATA))
        mnuTLEs.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Find TLE ..."
        mnuTLEs.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("By Object Name", CalculationType.SEARCH_TLE_BY_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("By Catalog ID", CalculationType.SEARCH_TLE_BY_CATID))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Decode TLE Data ..."
        mnuTLEs.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("By Object Name", CalculationType.DECODE_TLE_BY_NAME))
        tmpSubMenu.DropDownItems.Add(createMenuItem("By Catalog ID", CalculationType.DECODE_TLE_BY_CATID))
        tmpSubMenu.DropDownItems.Add(createMenuItem("By Data Set Number", CalculationType.DECODE_TLE_BY_SETNUM))
        mnuTLEs.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Mean Motion to Semi-Major Axis ..."
        mnuTLEs.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.AXIS_FROM_MMOTION_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.AXIS_FROM_MMOTION_FROM_INPUT))
        mnuTLEs.DropDownItems.Add(createMenuItem("Semi-Major Axis to Mean Motion", CalculationType.MEAN_MOTION_FROM_AXIS))
        mnuTLEs.DropDownItems.Add(createMenuItem("Extract Keplerian Elements from TLE", CalculationType.KEPLERIAN_FROM_TLE))

        ' Launch Sites menu
        mnuLaunchSites.DropDownItems.Add(createMenuItem("Load Launch Sites", CalculationType.LOAD_LAUNCH_SITES))
        mnuLaunchSites.DropDownItems.Add(createMenuItem("Display All Launch Sites", CalculationType.DISPLAY_ALL_LAUNCH_SITES))
        mnuLaunchSites.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Calculate Orbital Inclination ..."
        mnuLaunchSites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From Sites File", CalculationType.INCLINATION_FROM_FILE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.INCLINATION_FROM_INPUT))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Calculate Launch Azimuth ..."
        mnuLaunchSites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From Sites File", CalculationType.LAUNCH_AZIMUTH_FROM_FILE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.LAUNCH_AZIMUTH_FROM_INPUT))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Calculate Launch Velocity ..."
        mnuLaunchSites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From Sites File", CalculationType.VELOCITY_FROM_FILE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.VELOCITY_FROM_INPUT))

        'Satellite menu
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Locate Satellite ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.LOCATE_SAT_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.LOCATE_SAT_FROM_INPUT))
        mnuSatellites.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Rise/Set Times ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.RISE_SET_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.RISE_SET_FROM_INPUT))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Satellite Distance ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.SAT_DISTANCE_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.SAT_DISTANCE_FROM_INPUT))
        mnuSatellites.DropDownItems.Add("-")
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Perigee/Apogee ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.SAT_PERI_APOGEE_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.SAT_PERI_APOGEE_FROM_INPUT))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Orbital Period ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.SAT_PERIOD_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.SAT_PERIOD_FROM_INPUT))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Axis from Orbital Period ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("From TLE Data Set", CalculationType.AXIS_FROM_PERIOD_FROM_TLE))
        tmpSubMenu.DropDownItems.Add(createMenuItem("From User Input", CalculationType.AXIS_FROM_PERIOD_FROM_INPUT))
        tmpSubMenu = New ToolStripMenuItem()
        tmpSubMenu.Text = "Orbital Velocity ..."
        mnuSatellites.DropDownItems.Add(tmpSubMenu)
        tmpSubMenu.DropDownItems.Add(createMenuItem("For Circular Orbit", CalculationType.SAT_VELOCITY_CIRCULAR))
        tmpSubMenu.DropDownItems.Add(createMenuItem("For Elliptical Orbit", CalculationType.SAT_VELOCITY_ELLIPTICAL))
        mnuSatellites.DropDownItems.Add("-")
        mnuSatellites.DropDownItems.Add(createMenuItem("Satellite Footprint", CalculationType.SAT_FOOTPRINT))
        mnuSatellites.DropDownItems.Add("-")
        mnuSatellites.DropDownItems.Add(createMenuItem("Set Termination Criteria", CalculationType.TERM_CRITERIA))
    End Sub

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '-----------------------------------------------------
    ' Handle clicks on the Coord Sys, Launch Sites, TLEs,
    ' and Satellites menu items
    '-----------------------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        Select Case calctype
            '***************** Coord Sys menu
            Case CalculationType.CART_TO_SPHERICAL
                calcCartesian2Spherical()
            Case CalculationType.SPHERICAL_TO_CART
                calcSpherical2Cartesian()
            Case CalculationType.ECI_TO_HORIZON
                calcECI2Horizon()
            Case CalculationType.ECI_TO_TOPO
                calcECI2Topocentric()
            Case CalculationType.STATEVECTOR_TO_KEPLERIAN
                calcState2Keplerian()
            Case CalculationType.KEPLERIAN_TO_STATEVECTOR
                calcKeplerian2State()
                '***************** TLEs menu
            Case CalculationType.LOAD_TLE_DATA
                loadTLEData()
            Case CalculationType.DISPLAY_RAW_TLE_DATA
                displayRawTLEData()
            Case CalculationType.SEARCH_TLE_BY_NAME
                searchForTLE(calctype)
            Case CalculationType.SEARCH_TLE_BY_CATID
                searchForTLE(calctype)
            Case CalculationType.DECODE_TLE_BY_NAME
                decodeTLEData(calctype)
            Case CalculationType.DECODE_TLE_BY_CATID
                decodeTLEData(calctype)
            Case CalculationType.DECODE_TLE_BY_SETNUM
                decodeTLEData(calctype)
            Case CalculationType.AXIS_FROM_MMOTION_FROM_TLE
                calcAxisFromMMotion(calctype)
            Case CalculationType.AXIS_FROM_MMOTION_FROM_INPUT
                calcAxisFromMMotion(calctype)
            Case CalculationType.MEAN_MOTION_FROM_AXIS
                calcMeanMotionFromAxis()
            Case CalculationType.KEPLERIAN_FROM_TLE
                extractKeplerianFromTLE()
                '***************** Launch Sites menu
            Case CalculationType.LOAD_LAUNCH_SITES
                loadLaunchSites()
            Case CalculationType.DISPLAY_ALL_LAUNCH_SITES
                displayAllLaunchSites()
            Case CalculationType.INCLINATION_FROM_FILE
                calcInclination(calctype)
            Case CalculationType.INCLINATION_FROM_INPUT
                calcInclination(calctype)
            Case CalculationType.LAUNCH_AZIMUTH_FROM_FILE
                calcLaunchAzimuth(calctype)
            Case CalculationType.LAUNCH_AZIMUTH_FROM_INPUT
                calcLaunchAzimuth(calctype)
            Case CalculationType.VELOCITY_FROM_FILE
                calcLaunchVelocity(calctype)
            Case CalculationType.VELOCITY_FROM_INPUT
                calcLaunchVelocity(calctype)
                '***************** Satellites menu
            Case CalculationType.LOCATE_SAT_FROM_TLE
                calcSatLocation(calctype)
            Case CalculationType.LOCATE_SAT_FROM_INPUT
                calcSatLocation(calctype)
            Case CalculationType.RISE_SET_FROM_TLE
                calcSatRiseSet(calctype)
            Case CalculationType.RISE_SET_FROM_INPUT
                calcSatRiseSet(calctype)
            Case CalculationType.SAT_DISTANCE_FROM_TLE
                calcSatDistance(calctype)
            Case CalculationType.SAT_DISTANCE_FROM_INPUT
                calcSatDistance(calctype)
            Case CalculationType.SAT_PERI_APOGEE_FROM_TLE
                calcSatPerigeeApogee(calctype)
            Case CalculationType.SAT_PERI_APOGEE_FROM_INPUT
                calcSatPerigeeApogee(calctype)
            Case CalculationType.SAT_PERIOD_FROM_TLE
                calcSatOrbitalPeriod(calctype)
            Case CalculationType.SAT_PERIOD_FROM_INPUT
                calcSatOrbitalPeriod(calctype)
            Case CalculationType.AXIS_FROM_PERIOD_FROM_TLE
                calcAxisFromPeriod(calctype)
            Case CalculationType.AXIS_FROM_PERIOD_FROM_INPUT
                calcAxisFromPeriod(calctype)
            Case CalculationType.SAT_VELOCITY_CIRCULAR
                calcSatVelocityCircular()
            Case CalculationType.SAT_VELOCITY_ELLIPTICAL
                calcSatVelocityElliptical()
            Case CalculationType.SAT_FOOTPRINT
                calcSatFootprint()
            Case CalculationType.TERM_CRITERIA
                setTerminationCriteria()
        End Select

    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click

        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("Enter the 'Observer Location and Time' information in the area indicated. This location/time will " &
                    "be used in subsequent calculations. You may find it convenient to enter an initial latitude, longitude, " &
                    "and time zone in the file DefaultObsLoc.dat in the 'Ast Data Files' directory so that the latitude, " &
                    "longitude, and time zone are already filled in with default values when the program starts. When this program " &
                    "begins, the date and time will default to the local date and time at which the program is started.")
        prt.println()
        prt.println("Check the 'Show Intermediate Calculations' checkbox if you want to see intermediate results as the " &
                     "various calculations are performed. Also, select the appropriate radio button to choose what method to " &
                     "use when it is necessary to determine an object's true anomaly. The radio button " &
                     "'Simple' uses a simple iteration method while the radio button 'Newton' solves Kepler's equation via the " &
                     "Newton/Raphson method. The menu entry 'Satellites->Set Termination Critera' allows you to enter the " &
                     "termination criteria (in radians) for when to stop iterating to solve Kepler's equation.")
        prt.println()

        prt.println("The menu 'Coord Sys' performs various coordinate system conversions related to satellites, such as converting " &
                    "Keplerian elements to/from a state vector. The menu 'TLEs' allows TLE data files to be loaded and examined. There are " &
                    "also menu entries that decode a TLE data set and convert between Mean Motion and the length of the semi-major axis. " &
                    "The 'Launch Sites' menu provides the ability to load a collection of launch sites and their latitude for performing " &
                    "various calculations about launching rockets where the choice of the launch site is important.")
        prt.println()

        prt.println("The 'Satellites' menu uses the algorithms from the 'Coord Sys' and 'TLEs' menus to provide information about satellite " &
                    "orbits. Since only a very simple propagator is implemented, the results will not be very accurate if the time at which " &
                    "the information is calculated is more than a few hours after the epoch at which the Keplerian elements were captured. The " &
                    "'Satellites->Rise/Set Times' produces a table of a satellite's location at various times. In addition to the observer's " &
                    "location and LCT, the algorithm asks for the number of hours after the LCT an estimation is desired and a time increment. " &
                    "The algorithm then estimates the satellite's location for the observer's LCT, increments the LCT by the time increment " &
                    "entered, recomputes the esimated satellite location for that new time, and continues the process until the requsted " &
                    "number of hours have been reached. Within the time interval generated by this method, the algorithm estimates when a " &
                    "satellite will rise or set. For each 'Satellites' menu entry, the required Keplerian elements can be either entered " &
                    "directly from the keyboard or extracted from a TLE data set that the user selects. When time must be entered, " &
                    "such as when entering an object's orbital period, you must enter decimal minutes if the period " &
                    "is greater than 59 minutes because in HMS format, hours must be less than 24, minutes must be less than 60, and " &
                    "seconds must be less than 60.")
        prt.println()

        prt.setBoldFont(True)
        prt.println("**Important Notes**", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()
        prt.println("In the context of this program, the word 'satellite' is used rather loosely to mean any manmade object that is " &
                    "orbiting the Earth (satellite, rocket body, space station, space telescope, etc.), and not as literally " &
                    "meaning only a satellite. The various calculations performed by this program will work for any object " &
                    "orbiting the Earth whose Keplerian elements are known.")
        prt.println()

        prt.println("This program allows Keplerian elements to be entered into the program in multiple ways. They may be entered " &
                    "directly from the keyboard, or they may be read from a TLE data file. When read from a TLE data file, you may generally " &
                    "specify which object is of interest in one of up to three different ways: (1) by object name, (2) by catalog ID, or " &
                    "(3) by TLE data set. (1) When an object name is specified, a partial match is all that is required and the search " &
                    "is not case sensitive. Thus, a search for an object named 'is' or 'IS' will find every object in the currently loaded " &
                    "TLE data file that has 'IS' in its name. Searching by object name requires that object names actually exist in the " &
                    "TLE data file (names are optional), and such a search may return multiple entries. (2) A catalog ID is a required " &
                    "field in the TLE format. The catalog ID is a 5 digit number and all 5 digits must be entered for the search. As with " &
                    "searching by object name, searching by catalog ID may return multiple results because the TLE data file could contain " &
                    "several entries (but usually at different times) for the same object. (3) Searching by TLE data set number means to retrieve " &
                    "a specific TLE data set from the currently loaded TLE data file. For example, if the currently loaded data file has " &
                    "14 sets of TLE data lines, then searching for data set number 4 will retrieve the 4th data set in the TLE data file.")
        prt.println()

        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Coord Sys menu items
    '----------------------------------------------------------------------------------------------------

    '---------------------------------------------
    ' Convert cartesian coordinates to spherical
    '---------------------------------------------
    Private Sub calcCartesian2Spherical()
        Dim x, y, z, r, alpha, phi, dT As Double
        Dim result As String

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Convert Cartesian Coordinates to Spherical", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If coords3Form.showQueryForm("x", "y", "z", "Enter Cartesian coordinates ...") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(coords3Form.getData1(), x, HIDE_ERRORS) Then
            ErrMsg("Invalid x value - try again", "Invalid x")
            Return
        End If
        If Not isValidReal(coords3Form.getData2(), y, HIDE_ERRORS) Then
            ErrMsg("Invalid y value - try again", "Invalid y")
            Return
        End If
        If Not isValidReal(coords3Form.getData3(), z, HIDE_ERRORS) Then
            ErrMsg("Invalid z value - try again", "Invalid z")
            Return
        End If

        printlncond("Convert x= " & x & ", y = " & y & ", z = " & z & " to spherical coordinates")
        printlncond()

        r = Sqrt(x ^ 2 + y ^ 2 + z ^ 2)
        printlncond("1.  r = sqrt[ x^2 + y^2 + z^2 ]")
        printlncond("      = sqrt[(" & Format(x, genFloatFormat8) & ")^2 + (" & Format(y, genFloatFormat8) &
                    ")^2 + (" & Format(z, genFloatFormat8) & ")^2]")
        printlncond("      = " & r)
        printlncond()

        alpha = INVTAN_D(y / x)
        printlncond("2.  alpha = inv tan(y/x) = inv tan(" & y & "/" & x & ")")
        printlncond("          = " & Format(alpha, genFloatFormat8) & " degrees")
        printlncond()

        dT = quadAdjust(y, x)
        printlncond("3.  Use the algebraic signs of y and x to determine a quadrant adjustment")
        printlncond("    and apply it to alpha.")
        printcond("    alpha = alpha + Adjustment = " & Format(alpha, genFloatFormat8) & " + " &
                  angleToStr(dT, DECFORMAT) & " = ")
        alpha = alpha + dT
        printlncond(Format(alpha, genFloatFormat8) & " degrees")
        printlncond()

        phi = INVCOS_D(z / r)
        printlncond("4.  phi = inv cos(z/r) = inv cos(" & z & "/" & r & ")")
        printlncond("        = " & Format(phi, genFloatFormat8) & " degrees")
        printlncond()

        result = "[x=" & Format(x, genFloatFormat8) & ", y=" & Format(y, genFloatFormat8) & ", z=" &
            Format(z, genFloatFormat8) & "] = [r=" & Format(r, genFloatFormat8) & ", alpha=" &
            Format(alpha, genFloatFormat8) & ", phi=" & Format(phi, genFloatFormat8) & "]"
        printlncond("Thus, [x=" & Format(x, genFloatFormat8) & ", y=" & Format(y, genFloatFormat8) & ", z=" &
                    Format(z, genFloatFormat8) & "]")
        printlncond("    = [r=" & Format(r, genFloatFormat8) & ", alpha=" & Format(alpha, genFloatFormat8) &
                    ", phi=" & Format(phi, genFloatFormat8) & "]")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = result
    End Sub

    '------------------------------------------------
    ' Convert ECI (cartesian) coordinates to horizon
    '------------------------------------------------
    Private Sub calcECI2Horizon()
        Dim LCT, UT, LST, GST As Double
        Dim x, y, z, RA, Decl As Double
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate
        Dim spherCoord, horizonCoord As ASTCoord
        Dim result As String

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Convert ECI (Cartesian) Coordinates to Horizon", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If coords3Form.showQueryForm("x", "y", "z", "Enter Cartesian coordinates ...") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(coords3Form.getData1(), x, HIDE_ERRORS) Then
            ErrMsg("Invalid x value - try again", "Invalid x")
            Return
        End If
        If Not isValidReal(coords3Form.getData2(), y, HIDE_ERRORS) Then
            ErrMsg("Invalid y value - try again", "Invalid y")
            Return
        End If
        If Not isValidReal(coords3Form.getData3(), z, HIDE_ERRORS) Then
            ErrMsg("Invalid z value - try again", "Invalid z")
            Return
        End If

        ' Do all the time-related calculations
        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)

        printlncond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
        printlncond("    LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(observer.getObsDate) & ")")
        printcond("     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond("previous day ")
        ElseIf dateAdjust > 0 Then
            printcond("next day ")
        End If
        printlncond(dateToStr(adjustedDate) & ")")
        printlncond("    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond("    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        spherCoord = CartesianToSpherical(x, y, z)
        printlncond("2.  Convert ECI (cartesian) coordinates to spherical coordinates")
        printlncond("    r = " & Format(spherCoord.getSpherR.getDecAngle(), genFloatFormat8))
        printlncond("    alpha = " & Format(spherCoord.getSpherAlpha.getDecAngle(), genFloatFormat8) & " degrees")
        printlncond("    phi = " & Format(spherCoord.getSpherPhi.getDecAngle(), genFloatFormat8) & " degrees")
        printlncond()
        printlncond("    Note: r is the distance above the center of the Earth.")
        printlncond()

        Decl = 90.0 - spherCoord.getSpherPhi.getDecAngle()
        printlncond("3.  Convert phi to declination.")
        printlncond("    Decl = 90 - phi = 90 - " & Format(spherCoord.getSpherPhi.getDecAngle(), genFloatFormat8) &
                    " = " & Format(Decl, genFloatFormat8) & " degrees")
        printlncond()

        RA = spherCoord.getSpherAlpha.getDecAngle() / 15.0
        printlncond("4.  Convert alpha to hours.")
        printlncond("    RA = alpha/15.0 = " & Format(spherCoord.getSpherAlpha.getDecAngle(), genFloatFormat8) &
                    "/15.0 = " & Format(RA, genFloatFormat8) & " hours")
        printlncond()

        horizonCoord = RADecltoHorizon(RA, Decl, observer.getObsLat, LST)
        printlncond("5.  Convert the equatorial coordinates (RA, Decl) to horizon coordinates")
        printlncond("    Alt = " & angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) & ", Az = " &
                    angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT))
        printlncond()

        result = "[x=" & Format(x, genFloatFormat8) & ", y=" & Format(y, genFloatFormat8) & ", z=" &
            Format(z, genFloatFormat8) & "] =  " & angleToStr(horizonCoord.getAltAngle.getDecAngle, DMSFORMAT) &
            " Alt, " & angleToStr(horizonCoord.getAzAngle.getDecAngle, DMSFORMAT) & " Az, dist=" &
            Format(spherCoord.getSpherR.getDecAngle(), genFloatFormat)
        printlncond("Thus, for this observer location and date/time,")
        printlncond("  " & result)

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = result
    End Sub

    '------------------------------------------------
    ' Convert ECI (cartesian) to Topocentric
    '------------------------------------------------
    Private Sub calcECI2Topocentric()
        Dim LCT, UT, LST, LSTd, GST As Double
        Dim x, y, z As Double
        Dim R As ASTVect = New ASTVect()            ' Satellite's position in ECI coordinates
        Dim R_obs As ASTVect = New ASTVect()        ' Observer's position in ECI coordinates
        Dim Rp As ASTVect = New ASTVect()           ' Range vector
        Dim Rpp As ASTVect = New ASTVect()          ' Rotated range vector
        Dim obsLat As Double
        Dim h_sea As Double                         ' Observer's distance above sea level (meters)
        Dim rp_e, r_eq, r_dist As Double
        Dim Az, Alt As Double
        Dim dateAdjust As Integer
        Dim adjustedDate As ASTDate
        Dim result As String

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        obsLat = observer.getObsLat()

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Convert ECI (Cartesian) Coordinates to Topocentric", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If queryForm.showQueryForm("Enter Observer's height" & vbNewLine & "above sea level (in meters)") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(queryForm.getData1(), h_sea, HIDE_ERRORS) Then
            ErrMsg("Invalid Observer Height - try again", "Invalid Height")
            Return
        End If

        If coords3Form.showQueryForm("x", "y", "z", "Enter Cartesian coordinates ...") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(coords3Form.getData1(), x, HIDE_ERRORS) Then
            ErrMsg("Invalid x value - try again", "Invalid x")
            Return
        End If
        If Not isValidReal(coords3Form.getData2(), y, HIDE_ERRORS) Then
            ErrMsg("Invalid y value - try again", "Invalid y")
            Return
        End If
        If Not isValidReal(coords3Form.getData3(), z, HIDE_ERRORS) Then
            ErrMsg("Invalid z value - try again", "Invalid z")
            Return
        End If
        R.setVect(x, y, z)

        ' Do all the time-related calculations
        LCT = observer.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)

        printlncond("1.  Convert LCT to UT, GST, and LST and adjust date if needed.")
        printlncond("    LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(observer.getObsDate) & ")")
        printcond("     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond("previous day ")
        ElseIf dateAdjust > 0 Then
            printcond("next day ")
        End If
        printlncond(dateToStr(adjustedDate) & ")")
        printlncond("    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond("    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        LSTd = LST * 15.0
        printlncond("2.  Convert the observer's LST to an angle")
        printlncond("    LSTd = LST*15 = " & timeToStr(LST, DECFORMAT) & "*15 = " &
                    angleToStr(LSTd, DECFORMAT) & " degrees")
        printlncond()

        rp_e = EarthRadius + (h_sea / 1000)
        r_eq = rp_e * COS_D(obsLat)
        R_obs.setVect(r_eq * COS_D(LSTd), r_eq * SIN_D(LSTd), rp_e * SIN_D(obsLat))
        printlncond("3.  Convert the observer's location to ECI coordinates")
        printlncond("    rp_e = r_e + (h_obs/1000) = " & EarthRadius & " + (" & h_sea & "/1000)")
        printlncond("         = " & Format(rp_e, genFloatFormat8) & " km")
        printlncond("    r_eq = rp_e * cos(lat) = " & Format(rp_e, genFloatFormat8) & " * cos(" &
                    angleToStr(obsLat, DECFORMAT) & ")")
        printlncond("         = " & Format(r_eq, genFloatFormat8) & " km")
        printlncond("    x_obs = r_eq * cos(LSTd) = " & Format(r_eq, genFloatFormat8) & " * cos(" &
                    Format(LSTd, genFloatFormat8) & ")")
        printlncond("          = " & Format(R_obs.x, genFloatFormat8))
        printlncond("    y_obs = r_eq * sin(LSTd) = " & Format(r_eq, genFloatFormat8) & " * sin(" &
                    Format(LSTd, genFloatFormat8) & ")")
        printlncond("          = " & Format(R_obs.y, genFloatFormat8))
        printlncond("    z_obs = r_eq * sin(lat) = " & Format(r_eq, genFloatFormat8) & " * sin(" &
                    angleToStr(obsLat, DECFORMAT) & ")")
        printlncond("          = " & Format(R_obs.z, genFloatFormat8))
        printlncond()

        Rp.setVect(R.x - R_obs.x, R.y - R_obs.y, R.z - R_obs.z)
        printlncond("4.  Compute the range vector")
        printlncond("    x' = x - x_obs = " & Format(R.x, genFloatFormat8) & " - " & Format(R_obs.x, genFloatFormat8))
        printlncond("       = " & Format(Rp.x, genFloatFormat8))
        printlncond("    y' = y - y_obs = " & Format(R.y, genFloatFormat8) & " - " & Format(R_obs.y, genFloatFormat8))
        printlncond("       = " & Format(Rp.y, genFloatFormat8))
        printlncond("    z' = z - z_obs = " & Format(R.z, genFloatFormat8) & " - " & Format(R_obs.z, genFloatFormat8))
        printlncond("       = " & Format(Rp.z, genFloatFormat8))
        printlncond()

        x = Rp.x * SIN_D(obsLat) * COS_D(LSTd) + Rp.y * SIN_D(obsLat) * SIN_D(LSTd) - Rp.z * COS_D(obsLat)
        y = -Rp.x * SIN_D(LSTd) + Rp.y * COS_D(LSTd)
        z = Rp.x * COS_D(obsLat) * COS_D(LSTd) + Rp.y * COS_D(obsLat) * SIN_D(LSTd) + Rp.z * SIN_D(obsLat)
        Rpp.setVect(x, y, z)
        printlncond("5.  Rotate the range vector by LSTd and latitude")
        printlncond("    x'' = x'*sin(lat)*cos(LSTd) + y'*sin(lat)*sin(LSTd) - z'*cos(lat)")
        printlncond("        = " & Format(Rp.x, genFloatFormat8) & "*sin(" & angleToStr(obsLat, DECFORMAT) & ")*cos(" &
                    angleToStr(LSTd, DECFORMAT) & ") + ")
        printlncond("          " & Format(Rp.y, genFloatFormat8) & "*sin(" & angleToStr(obsLat, DECFORMAT) & ")*sin(" &
                    angleToStr(LSTd, DECFORMAT) & ") - ")
        printlncond("          " & Format(Rp.z, genFloatFormat8) & "*cos(" & angleToStr(obsLat, DECFORMAT) & ")")
        printlncond("        = " & Format(Rpp.x, genFloatFormat8))
        printlncond("    y'' = -[x'*sin(LSTd)] + y'*cos(LSTd)")
        printlncond("        = -[" & Format(Rp.x, genFloatFormat8) & "*sin(" & angleToStr(LSTd, DECFORMAT) & ")] + " &
                    Format(Rp.y, genFloatFormat8) & "*cos(" & angleToStr(LSTd, DECFORMAT) & ")")
        printlncond("        = " & Format(Rpp.y, genFloatFormat8))
        printlncond("    z'' = x'*cos(lat)*cos(LSTd) + y'*cos(lat)*sin(LSTd) + z'*sin(lat)")
        printlncond("        = " & Format(Rp.x, genFloatFormat8) & "*cos(" & angleToStr(obsLat, DECFORMAT) & ")*cos(" &
                    angleToStr(LSTd, DECFORMAT) & ") + ")
        printlncond("          " & Format(Rp.y, genFloatFormat8) & "*cos(" & angleToStr(obsLat, DECFORMAT) & ")*sin(" &
                    angleToStr(LSTd, DECFORMAT) & ") + ")
        printlncond("          " & Format(Rp.z, genFloatFormat8) & "*sin(" & angleToStr(obsLat, DECFORMAT) & ")")
        printlncond("        = " & Format(Rpp.z, genFloatFormat8))
        printlncond()

        r_dist = Rpp.len()
        printlncond("6.  Compute the rotated range vector's magnitude")
        printlncond("    r_dist = " & insertCommas(Round(r_dist, 8)) & " km")
        printlncond()

        Az = INVTAN2_D(-Rpp.y, Rpp.x)
        printlncond("7.  Compute the topocentric azimuth and adjust if necessary to put in the correct quadrant")
        printlncond("    Az_topo = inv tan(-y'' / x'') = inv tan(-" & Format(Rp.y, genFloatFormat8) & "/" &
                    Format(Rp.x, genFloatFormat8) & ")")
        printlncond("            = " & Format(INVTAN_D(-Rp.y / Rp.x), genFloatFormat) & " degrees")
        printlncond("    After adjusting to the correct quadrant,")
        printlncond("    Az_topo = " & Format(Az, genFloatFormat) & " degrees")
        printlncond()

        Alt = INVSIN_D(Rpp.z / r_dist)
        printlncond("8.  Compute the topocentric altitude")
        printlncond("    Alt_topo = inv sin(z'' / r_dist) = inv sin(" & Format(Rpp.z, genFloatFormat8) & "/" &
                    Format(r_dist, genFloatFormat8) & ")")
        printlncond("             = " & Format(Alt, genFloatFormat) & " degrees")
        printlncond()

        printlncond("9.  Finally, convert Altitude and Azimuth to DMS format. Thus,")
        printlncond("    Alt_topo = " & angleToStr(Alt, DMSFORMAT) & ", Az_topo = " & angleToStr(Az, DMSFORMAT))
        printlncond()

        result = "[x=" & Format(R.x, genFloatFormat8) & ", y=" & Format(R.y, genFloatFormat8) & ", z=" &
            Format(R.z, genFloatFormat8) & "] =  " & angleToStr(Alt, DMSFORMAT) &
            " Alt_topo, " & angleToStr(Az, DMSFORMAT) & " Az_topo"
        printlncond("Thus, for this observer location and date/time,")
        printlncond("  " & result)

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = result
    End Sub

    '----------------------------------------------
    ' Convert Keplerian elements to a state vector
    '----------------------------------------------
    Private Sub calcKeplerian2State()
        Dim prtObj As ASTPrt = Nothing
        Dim iter As Integer
        Dim axis, inclin, RAAN, argofperi, M0, ecc As Double
        Dim RAANp, argofperip As Double
        Dim rho, EA, dv, dLen, x, y, z, A As Double
        Dim R As ASTVect = New ASTVect()
        Dim V As ASTVect = New ASTVect()
        Dim angleObj As ASTAngle = New ASTAngle()
        Dim Otype As Integer

        clearTextAreas()

        If Not coords6Form.validateKeplerianElements(inclin, ecc, axis, RAAN, argofperi, M0) Then Return

        prt.setBoldFont(True)
        printlncond("Convert Keplerian Elements to the corresponding State Vector", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        ' Set up whether to display interim results when solving Kepler's equation
        If chkboxShowInterimCalcs.Checked Then
            prtObj = prt
        Else
            prtObj = Nothing
        End If

        printlncond("Convert these Keplerian Elements to a State Vector:")
        printlncond("  Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
        printlncond("  Eccentricity = " & Format(ecc, genFloatFormat8))
        printlncond("  Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
        printlncond("  RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
        printlncond("  Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
        printlncond("  Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")
        printlncond()

        rho = axis * (1.0 - ecc ^ 2)
        printlncond("1.  Compute the semi-latus rectum from the semi-major axis and eccentricity")
        printlncond("    p = a*(1 - e^2) = " & insertCommas(axis) & "*(1 - " & Format(ecc, genFloatFormat8) & "^2)")
        printlncond("      = " & insertCommas(rho))
        printlncond()

        printlncond("2.  Solve Kepler's equation to get the eccentric anomaly from M0")
        If radbtnSimpleIteration.Checked Then
            EA = calcSimpleKepler(prtObj, M0, ecc, termCriteria, iter)
        Else
            EA = calcNewtonKepler(prtObj, M0, ecc, termCriteria, iter)
        End If
        printlncond("    E = " & Format(EA, genFloatFormat8) & " degrees")
        printlncond()

        dv = Sqrt((1 + ecc) / (1 - ecc)) * TAN_D(EA / 2.0)
        dv = 2.0 * INVTAN_D(dv)
        printlncond("3.  Compute the object's true anomaly")
        printlncond("    v = 2 * inv tan[ sqrt[(1 + e)/(1 - e)] * tan(E/2) ]")
        printlncond("      = 2 * inv tan[ sqrt[(1+" & ecc & ")/(1-" & ecc & ")] * tan(" &
                    Format(EA, genFloatFormat8) & "/2) ]")
        printlncond("      = " & Format(dv, genFloatFormat8) & " degrees")
        printlncond()

        dLen = rho / (1 + ecc * COS_D(dv))
        printlncond("4.  Compute the length of the positional vector")
        printlncond("    Rlen = p/(1 + e*cos(v)) = " & insertCommas(rho) & "/(1 + " & Format(ecc, genFloatFormat8) &
                    "*cos(" & Format(dv, genFloatFormat8) & "))")
        printlncond("         = " & insertCommas(dLen))
        printlncond()

        x = dLen * COS_D(dv)
        y = dLen * SIN_D(dv)
        z = 0.0
        R.setVect(x, y, z)
        printlncond("5.  Compute the positional vector R' in the perifocal coordinate system")
        printlncond("    x' = Rlen*cos(v) = " & insertCommas(dLen) & "*cos(" & Format(dv, genFloatFormat8) & ")")
        printlncond("       = " & insertCommas(x))
        printlncond("    y' = Rlen*sin(v) = " & insertCommas(dLen) & "*sin(" & Format(dv, genFloatFormat8) & ")")
        printlncond("       = " & insertCommas(y))
        printlncond("    z' = 0.0")
        printlncond()

        A = Sqrt(muEarth / rho)
        printlncond("6.  Compute a temporary variable A")
        printlncond("    A = sqrt(muEarth/p) = sqrt(" & Format(muEarth, genFloatFormat8) & "/" & Format(rho, genFloatFormat8) & ")")
        printlncond("      = " & Format(A, genFloatFormat8))
        printlncond()

        x = -A * SIN_D(dv)
        y = A * (ecc + COS_D(dv))
        z = 0.0
        V.setVect(x, y, z)
        printlncond("7.  Compute the velocity vector V' in the perifocal coordinate system")
        printlncond("    Vx' = -A*sin(v) = -(" & Format(A, genFloatFormat8) & ")*sin(" & Format(dv, genFloatFormat8) & ")")
        printlncond("        = " & insertCommas(x))
        ' E in the next statement is e, the base of the natural log
        printlncond("    Vy' = A*(e + cos(v)) = (" & Format(A, genFloatFormat8) & ")*(" & Format(E, genFloatFormat8) &
                    " + cos(" & Format(dv, genFloatFormat8) & "))")
        printlncond("        = " & insertCommas(y))
        printlncond("    Vz' = 0.0")
        printlncond()

        printlncond("8.  Compute the length of the velocity vector to get the velocity")
        printlncond("    Vlen' = " & Format(V.len, genFloatFormat8))
        printlncond()

        Otype = getSatOrbitType(ecc, inclin)
        If (Otype < 0) Then
            ErrMsg("Orbit must be elliptical. The computed eccentricity" & vbNewLine & "and inclination are not supported.",
                   "Invalid Orbit type")
            Return
        End If
        printlncond("9.  Determine the orbit type based on orbital eccentricity and inclination")
        printlncond("    Otype = " & Otype)
        printlncond()

        printlncond("10. Calculate RAAN' based on the orbit type")
        If (Otype = 1) Or (Otype = 2) Then
            RAANp = 0.0
            printlncond("    For orbit types 1 and 2, RAAN' = 0.0 degrees")
        Else
            RAANp = RAAN
            printlncond("    For orbit types other than 1 and 2, RAAN' = RAAN")
        End If
        printlncond("    Thus, RAAN' = " & Format(RAANp, genFloatFormat8) & " degrees")
        printlncond()

        printlncond("11. Compute w' based on the orbit type")
        If (Otype = 1) Or (Otype = 3) Then
            printlncond("    For orbit types 1 and 3, w' = 0.0 degrees")
            argofperip = 0.0
        Else
            argofperip = argofperi
            printlncond("    For orbit types other than 1 and 3, w' = w")
        End If
        printlncond("    Thus, w' = " & Format(argofperip, genFloatFormat8) & " degrees")
        printlncond()

        printlncond("12. Use the 'g' functions to rotate the positional")
        printlncond("    vector R' by -w' degrees about the z-axis")
        R = RotateZ(-argofperip, R)
        printlncond("    Rotating R' by " & Format(-argofperip, genFloatFormat8) & " degrees about the z-axis gives")
        printlncond("    x' = " & insertCommas(R.x))
        printlncond("    y' = " & insertCommas(R.y))
        printlncond("    z' = " & insertCommas(R.z))
        printlncond()

        printlncond("13. Use the 'f' functions to rotate the vector from the previous step")
        printlncond("    by -inclination degrees about the x-axis")
        R = RotateX(-inclin, R)
        printlncond("    Rotating R' by " & Format(-inclin, genFloatFormat8) & " degrees about the x-axis gives")
        printlncond("    x' = " & insertCommas(R.x))
        printlncond("    y' = " & insertCommas(R.y))
        printlncond("    z' = " & insertCommas(R.z))
        printlncond()

        printlncond("14. Use the 'g' functions to rotate the vector from the previous step")
        printlncond("    by -RAANp degrees about the z-axis to get the positional vector")
        R = RotateZ(-RAANp, R)
        printlncond("    Rotating R' by " & Format(-RAANp, genFloatFormat8) & " degrees about the z-axis gives")
        printlncond("    x = " & insertCommas(R.x))
        printlncond("    y = " & insertCommas(R.y))
        printlncond("    z = " & insertCommas(R.z))
        printlncond()

        printlncond("15. Use the 'g' functions to rotate the velocity")
        printlncond("    vector V' by -w' degrees about the z-axis")
        V = RotateZ(-argofperip, V)
        printlncond("    Rotating V' by " & Format(-argofperip, genFloatFormat8) & " degrees about the z-axis gives")
        printlncond("    Vx' = " & insertCommas(V.x))
        printlncond("    Vy' = " & insertCommas(V.y))
        printlncond("    Vz' = " & insertCommas(V.z))
        printlncond()

        printlncond("16. Use the 'f' functions to rotate the vector from the previous step")
        printlncond("    by -inclination degrees about the x-axis")
        V = RotateX(-inclin, V)
        printlncond("    Rotating V' by " & Format(-inclin, genFloatFormat8) & " degrees about the x-axis gives")
        printlncond("    Vx' = " & insertCommas(V.x))
        printlncond("    Vy' = " & insertCommas(V.y))
        printlncond("    Vz' = " & insertCommas(V.z))
        printlncond()

        printlncond("17. Use the 'g' functions to rotate the vector from the previous step")
        printlncond("    by -RAANp degrees about the z-axis to get the velocity vector")
        V = RotateZ(-RAANp, V)
        printlncond("    Rotating V' by " & Format(-RAANp, genFloatFormat8) & " degrees about the z-axis gives")
        printlncond("    Vx = " & insertCommas(V.x))
        printlncond("    Vy = " & insertCommas(V.y))
        printlncond("    Vz = " & insertCommas(V.z))
        printlncond()

        prt.println("Input Keplerian Elements (assuming units are km and km/s):")
        prt.println("  Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
        prt.println("  Eccentricity = " & Format(ecc, genFloatFormat8))
        prt.println("  Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
        prt.println("  RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
        prt.println("  Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
        prt.println("  Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")
        prt.println()

        prt.println("Gives the State Vector:")
        prt.println("  [" & insertCommas(Round(R.x, 3)) & ", " & insertCommas(Round(R.y, 3)) &
                    ", " & insertCommas(Round(R.z, 3)) & "] (positional vector)")
        prt.println("  [" & insertCommas(Round(V.x, 3)) & ", " & insertCommas(Round(V.y, 3)) &
                    ", " & insertCommas(Round(V.z, 3)) & "] (velocity vector)")
        prt.println()
        prt.println("State Vector gives a velocity of " & Round(V.len, 3) & " km/s at a distance")
        prt.println("of " & insertCommas(Round(R.len, 3)) & " km (" & insertCommas(Round(KM2Miles(R.len), 3)) &
                    " miles) above the center of the Earth.")
        prt.println()

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "State Vector components are listed in the text area below"
    End Sub

    '---------------------------------------------
    ' Convert spherical coordinates to cartesian
    '---------------------------------------------
    Private Sub calcSpherical2Cartesian()
        Dim x, y, z, r, alpha, phi As Double
        Dim result As String

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Convert Spherical Coordinates to Cartesian", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If coords3Form.showQueryForm("r", "alpha", "phi",
                                     "Enter spherical coordinates ...") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(coords3Form.getData1(), r, HIDE_ERRORS) Then
            ErrMsg("Invalid r value - try again", "Invalid r")
            Return
        End If
        If Not isValidReal(coords3Form.getData2(), alpha, HIDE_ERRORS) Then
            ErrMsg("Invalid alpha value - try again", "Invalid alpha")
            Return
        End If
        If Not isValidReal(coords3Form.getData3(), phi, HIDE_ERRORS) Then
            ErrMsg("Invalid phi value - try again", "Invalid phi")
            Return
        End If

        printlncond("Convert r= " & r & ", alpha = " & alpha & ", phi = " & phi & " to cartesian coordinates")
        printlncond()

        x = r * COS_D(alpha) * SIN_D(phi)
        printlncond("1.  x = r * cos(alpha) * sin(phi)")
        printlncond("      = " & Format(r, genFloatFormat8) & " * cos(" & Format(alpha, genFloatFormat8) &
                    ") * sin(" & Format(phi, genFloatFormat8) & ")")
        printlncond("      = " & x)
        printlncond()

        y = r * SIN_D(alpha) * SIN_D(phi)
        printlncond("2.  y = r * sin(alpha) * sin(phi)")
        printlncond("      = " & Format(r, genFloatFormat8) & " * sin(" & Format(alpha, genFloatFormat8) &
                    ") * sin(" & Format(phi, genFloatFormat8) & ")")
        printlncond("      = " & y)
        printlncond()

        z = r * COS_D(phi)
        printlncond("3.  z = r * cos(phi) = " & r & " * cos(" & Format(phi, genFloatFormat8) & ")")
        printlncond("        = " & z)
        printlncond()

        result = "[r=" & Format(r, genFloatFormat8) & ", alpha=" & Format(alpha, genFloatFormat8) &
            ", phi=" & Format(phi, genFloatFormat8) & "] = [x=" & Format(x, genFloatFormat8) &
            ", y=" & Format(y, genFloatFormat8) & ", z=" & Format(z, genFloatFormat8) & "]"
        printlncond("Thus, [r=" & Format(r, genFloatFormat8) & ", alpha=" & Format(alpha, genFloatFormat8) &
                    ", phi=" & Format(phi, genFloatFormat8) & "]")
        printlncond("    = [x=" & Format(x, genFloatFormat8) & ", y=" & Format(y, genFloatFormat8) &
                    ", z=" & Format(z, genFloatFormat8) & "]")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = result
    End Sub

    '----------------------------------------------
    ' Convert a state vector to Keplerian elements
    '----------------------------------------------
    Private Sub calcState2Keplerian()
        Dim R As ASTVect = New ASTVect()
        Dim V As ASTVect = New ASTVect()
        Dim H As ASTVect = New ASTVect()
        Dim N As ASTVect = New ASTVect()
        Dim e As ASTVect = New ASTVect()
        Dim x, y, z, A, B, rho, axis, inclin, RAAN, argofperi, w_t As Double
        Dim dv, L_t, u, EA, MA, M0 As Double
        Dim Otype As Integer

        ' Note dv Is the satellite's true anomaly. It is undefined for circular
        ' orbits. For all other orbits, the true anomaly Is calculated below.
        ' However, it Is initialized here to avoid compiler warnings about
        ' a potentially uninitialized variable.
        dv = 1.0

        clearTextAreas()

        If Not coords6Form.validateStateVect(R, V) Then Return

        prt.setBoldFont(True)
        printlncond("Convert a State Vector to its Keplerian Elements", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        printlncond("Convert the state vector")
        printlncond("  [" & Format(R.x, genFloatFormat8) & ", " & Format(R.y, genFloatFormat8) &
                    ", " & Format(R.z, genFloatFormat8) & "] (positional vector)")
        printlncond("  [" & Format(V.x, genFloatFormat8) & ", " & Format(V.y, genFloatFormat8) &
                    ", " & Format(V.z, genFloatFormat8) & "] (velocity vector)")
        printlncond("to its equivalent Keplerian elements")
        printlncond()

        printlncond("1.  Compute the length of the position vector R")
        printlncond("    Rlen = sqrt[x^2 + y^2 + z^2]")
        printlncond("         = sqrt[(" & Format(R.x, genFloatFormat8) & ")^2 + (" & Format(R.y, genFloatFormat8) &
                    ")^2 + (" & Format(R.z, genFloatFormat8) & ")^2]")
        printlncond("         = " & insertCommas(R.len))
        printlncond()

        printlncond("2.  Compute the length of the velocity vector V")
        printlncond("    Vlen = sqrt[Vx^2 + Vy^2 + Vz^2]")
        printlncond("         = sqrt[(" & Format(V.x, genFloatFormat8) & ")^2 + (" & Format(V.y, genFloatFormat8) &
                    ")^2 + (" & Format(V.z, genFloatFormat8) & ")^2]")
        printlncond("         = " & insertCommas(V.len))
        printlncond()

        x = R.y * V.z - R.z * V.y
        y = R.z * V.x - R.x * V.z
        z = R.x * V.y - R.y * V.x
        H.setVect(x, y, z)
        printlncond("3.  Compute the angular momentum vector H=[Hx Hy Hz]")
        printlncond("    Hx = yVz - zVy = (" & Format(R.y, genFloatFormat8) & ")*(" & Format(V.z, genFloatFormat8) & ") - (" &
                    Format(R.z, genFloatFormat8) & ")*(" & Format(V.y, genFloatFormat8) & ")")
        printlncond("       = " & insertCommas(H.x))
        printlncond("    Hy = zVx - xVz = (" & Format(R.z, genFloatFormat8) & ")*(" & Format(V.x, genFloatFormat8) & ") - (" &
                    Format(R.x, genFloatFormat8) & ")*(" & Format(V.z, genFloatFormat8) & ")")
        printlncond("       = " & insertCommas(H.y))
        printlncond("    Hz = xVy - yVx = (" & Format(R.x, genFloatFormat8) & ")*(" & Format(V.y, genFloatFormat8) & ") - (" &
                    Format(R.y, genFloatFormat8) & ")*(" & Format(V.x, genFloatFormat8) & ")")
        printlncond("       = " & insertCommas(H.z))
        printlncond()

        printlncond("4.  Compute the length of the angular momentum vector H")
        printlncond("    Hlen = sqrt[Hx^2 + Hy^2 + Hz^2]")
        printlncond("         = sqrt[(" & Format(H.x, genFloatFormat8) & ")^2 + (" & Format(H.y, genFloatFormat8) &
                    ")^2 + (" & Format(H.z, genFloatFormat8) & ")^2]")
        printlncond("         = " & insertCommas(H.len))
        printlncond()

        x = -H.y
        y = H.x
        z = 0.0
        N.setVect(x, y, z)
        printlncond("5.  Compute the node vector.")
        printlncond("    Nx = -Hy = -(" & H.y & ") = " & insertCommas(N.x))
        printlncond("    Ny = Hx = " & insertCommas(N.y))
        printlncond("    Nz = 0.0")
        printlncond()

        printlncond("6.  Compute the length of the node vector N")
        printlncond("    Nlen = " & insertCommas(N.len))
        printlncond()

        A = V.len ^ 2 - (muEarth / R.len)
        B = R.x * V.x + R.y * V.y + R.z * V.z
        printlncond("7.  Calculate temporary variables A and B")
        printlncond("    A = Vlen^2 - (muEarth/Rlen) = " & insertCommas(V.len) & "^2 - (" &
                    muEarth & "/" & insertCommas(R.len) & ")")
        printlncond("      = " & Format(A, genFloatFormat8))
        printlncond("    B = xVx + yVy + zVz")
        printlncond("      = (" & insertCommas(R.x) & ")*(" & insertCommas(V.x) & ") + (" &
                    insertCommas(R.y) & ")*(" & insertCommas(V.y) & ") + (" & insertCommas(R.z) &
                    ")*(" & insertCommas(V.z) & ")")
        printlncond("      = " & Format(B, genFloatFormat8))
        printlncond()

        x = (R.x * A - B * V.x) / muEarth
        y = (R.y * A - B * V.y) / muEarth
        z = (R.z * A - B * V.z) / muEarth
        e.setVect(x, y, z)
        printlncond("8.  Compute the eccentricity vector")
        printlncond("    ex = [xA - BVx]/muEarth")
        printlncond("       = [(" & insertCommas(R.x) & ")*(" & Format(A, genFloatFormat8) & ") - (" &
                    Format(B, genFloatFormat8) & ")*(" & insertCommas(V.x) & ")]/" & muEarth)
        printlncond("       = " & Format(x, genFloatFormat8))
        printlncond("    ey = [yA - BVy]/muEarth")
        printlncond("       = [(" & insertCommas(R.y) & ")*(" & Format(A, genFloatFormat8) & ") - (" &
                    Format(B, genFloatFormat8) & ")*(" & insertCommas(V.y) & ")]/" & muEarth)
        printlncond("       = " & Format(y, genFloatFormat8))
        printlncond("    ez = [zA - BVz]/muEarth")
        printlncond("       = [(" & insertCommas(R.z) & ")*(" & Format(A, genFloatFormat8) & ") - (" &
                    Format(B, genFloatFormat8) & ")*(" & insertCommas(V.z) & ")]/" & muEarth)
        printlncond("       = " & Format(z, genFloatFormat8))
        printlncond()

        printlncond("9.  Compute the length of the eccentricity vector to get the orbital eccentricity")
        printlncond("    e = " & Format(e.len, genFloatFormat8))
        printlncond()

        rho = (H.len ^ 2) / muEarth
        printlncond("10. Compute the semi-latus rectum")
        printlncond("    p = (Hlen^2)/muEarth = (" & insertCommas(H.len) & "^2)/" & muEarth)
        printlncond("      = " & insertCommas(rho))
        printlncond()

        axis = rho / (1 - e.len ^ 2)
        printlncond("11. Use the semi-latus rectum and eccentricity to compute the semi-major axis")
        printlncond("    a = p/(1 - e^2) = " & insertCommas(rho) & "/(1 - " & Format(e.len, genFloatFormat8) & "^2)")
        printlncond("      = " & insertCommas(axis))
        printlncond()

        inclin = INVCOS_D(H.z / H.len)
        printlncond("12. Compute the orbital inclination")
        printlncond("    inclin = inv cos(Hz/Hlen) = inv cos(" & insertCommas(H.z) & "/" & insertCommas(H.len) & ")")
        printlncond("           = " & Format(inclin, genFloatFormat8) & " degrees")
        printlncond()

        Otype = getSatOrbitType(e.len, inclin)
        If (Otype < 0) Then
            ErrMsg("Orbit must be elliptical. The computed eccentricity" & vbNewLine & "and inclination are not supported.",
                   "Invalid Orbit type")
            Return
        End If
        printlncond("13. Determine the orbit type based on orbital eccentricity and inclination")
        printlncond("    Otype = " & Otype)
        printlncond()

        printlncond("14. Calculate the right ascension of the ascending node")
        If (Otype = 1) Or (Otype = 2) Then
            RAAN = 0.0
            printlncond("    RAAN = 0.0 degrees for orbit types 1 and 2")
        Else
            RAAN = INVCOS_D(-H.y / N.len)
            printlncond("    RAAN = inv cos(-Hy/Nlen) = inv cos[-(" & insertCommas(H.y) & ")/" & insertCommas(N.len) & "]")
            printlncond("         = " & Format(RAAN, genFloatFormat8) & " degrees")
            printlncond("    If Hx < 0, subtract RAAN from 360 to put it into the correct quadrant")
            If (H.x < 0.0) Then
                RAAN = 360.0 - RAAN
                printlncond("    RAAN = 360 - RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
            Else
                printlncond("    No adjustment required.")
            End If
        End If
        printlncond()

        argofperi = 0.0
        printlncond("15. Calculate the argument of perigee")
        If (Otype = 1) Or (Otype = 3) Then
            argofperi = 0.0
            printlncond("    w = 0.0 degrees for orbit types 1 and 3. Skip to step 18.")
        Else
            printlncond("    For orbit types 2 and 4, proceed to step 16.")
        End If
        printlncond()

        w_t = INVCOS_D(e.x / e.len)
        If (e.y < 0.0) Then w_t = 360.0 - w_t
        printlncond("16. Calculate the argument of perigee")
        If (Otype = 1) Or (Otype = 3) Then
            printlncond("    For orbit types 1 and 3, skip to step 18.")
        ElseIf Otype = 2 Then
            printlncond("    For orbit type 2, set w = w_t and skip to step 18.")
            argofperi = INVCOS_D(e.x / e.len)
            printlncond("    w = w_t = inv cos(ex/e) = inv cos(" & Format(e.x, genFloatFormat8) & "/" & Format(e.len, genFloatFormat8) & ")")
            printlncond("      = " & Format(argofperi, genFloatFormat8) & " degrees")
            printlncond("    If ey < 0, subtract w from 360 to put it into the correct quadrant")
            If (e.y < 0.0) Then
                argofperi = 360.0 - argofperi
                printlncond("    w = 360 - w = " & Format(argofperi, genFloatFormat8) & " degrees")
            Else
                printlncond("    No adjustment required.")
            End If
        Else
            printlncond("    For orbit type 4, must proceed to step 17, but")
            printlncond("    w_t = " & Format(w_t, genFloatFormat8) & " degrees")
        End If
        printlncond()

        printlncond("17. Calculate the argument of perigee")
        If (Otype <> 4) Then
            printlncond("   Skip to step 18 for this orbit type. w was calculated in a prior step.")
        Else
            argofperi = INVCOS_D((e.y * H.x - e.x * H.y) / (e.len * N.len))
            printlncond("    w = inv cos[(eyHx-exHy)/(eNlen)]")
            printlncond("      = inv cos[((" & Format(e.y, genFloatFormat8) & ")*(" & insertCommas(H.x) & ") - (" &
                        Format(e.x, genFloatFormat8) & ")*(" & insertCommas(H.y) & "))/")
            printlncond("                (" & Format(e.len, genFloatFormat8) & ")*(" & insertCommas(N.len) & "))]")
            printlncond("      = " & Format(argofperi, genFloatFormat8) & " degrees")
            printlncond("     If ez < 0, subtract w from 360 to put it into the correct quadrant")
            If (e.z < 0.0) Then
                argofperi = 360.0 - argofperi
                printlncond("     w = 360 - w = " & Format(argofperi, genFloatFormat8) & " degrees")
            Else
                printlncond("     No adjustment required.")
            End If
        End If
        printlncond()

        printlncond("18. Compute the object's true anomaly")
        If (Otype = 1) Then
            L_t = INVCOS_D(R.x / R.len)
            dv = L_t
            printlncond("    For orbit type 1, v = L_t and skip to step 21.")
            printlncond("    v = L_t = inv cos(x/Rlen) = inv cos(" & insertCommas(R.x) & "/" & insertCommas(R.len) & ")")
            printlncond("      = " & Format(L_t, genFloatFormat8) & " degrees")
            printlncond("    If y < 0, subtract v from 360 to put it into the correct quadrant")
            If (R.y < 0.0) Then
                dv = 360.0 - dv
                printlncond("    v = 360 - v = " & Format(dv, genFloatFormat8) & " degrees")
            Else
                printlncond("    No adjustment required.")
            End If
        Else
            printlncond("    For orbit types other than 1, proceed to next step")
        End If
        printlncond()

        u = INVCOS_D((R.x * N.x + R.y * N.y) / (N.len * R.len))
        If R.z < 0 Then u = 360.0 - u
        printlncond("19. Compute the object's true anomaly.")
        If (Otype = 1) Then
            printlncond("    For orbit type 1, proceed to step 21.")
        ElseIf (Otype = 3) Then
            dv = INVCOS_D((R.x * N.x + R.y * N.y) / (N.len * R.len))
            printlncond("    For orbit type 3, set v = u and skip to step 21.")
            printlncond("    u = inv cos[(xNx + yNy)/(Nlen*Rlen)])")
            printlncond("      = inv cos[((" & insertCommas(R.x) & ")*(" & insertCommas(N.x) & ") + (" &
                        insertCommas(R.y) & ")*(" & insertCommas(N.y) & "))/(" & insertCommas(N.len) & "*" &
                        insertCommas(R.len) & ")]")
            printlncond("      = " & Format(dv, genFloatFormat8) & " degrees")
            printlncond("    If z < 0, subtract v from 360 to put it into the correct quadrant")
            If (R.z < 0.0) Then
                dv = 360.0 - dv
                printlncond("    v = 360 - v = " & Format(dv, genFloatFormat8) & " degrees")
            Else
                printlncond("    No adjustment required.")
            End If
        Else
            printlncond("    For orbit types other than 3, proceed to the next step, but")
            printlncond("    u = " & Format(u, genFloatFormat8) & " degrees")
        End If
        printlncond()

        printlncond("20. Compute the object's true anomaly.")
        If (Otype = 1) Or (Otype = 3) Then
            printlncond("    For orbit types 1 and 3, skip to step 21.")
        Else
            dv = INVCOS_D((e.x * R.x + e.y * R.y + e.z * R.z) / (e.len * R.len))
            printlncond("    v = inv cos[(ex*x + ey*y + ez*z)/(e*Rlen)]")
            printlncond("      = inv cos[((" & Format(e.x, genFloatFormat8) & ")*(" & insertCommas(R.x) & ") + (" &
                        Format(e.y, genFloatFormat8) & ")*(" & insertCommas(R.y) & ") + (" &
                        Format(e.z, genFloatFormat8) & ")*(" & insertCommas(R.z) & "))/")
            printlncond("                (" & Format(e.len, genFloatFormat8) & "*" & insertCommas(R.len) & ")]")
            printlncond("      = " & Format(dv, genFloatFormat8) & " degrees")
            printlncond("    If B < 0, subtract v from 360 to put it into the correct quadrant")
            If (B < 0.0) Then
                dv = 360.0 - dv
                printlncond("    v = 360 - v = " & Format(dv, genFloatFormat8) & " degrees")
            Else
                printlncond("    No adjustment required.")
            End If
        End If
        printlncond()

        EA = INVCOS_D((e.len + COS_D(dv)) / (1 + e.len * COS_D(dv)))
        printlncond("21. Use the true anomaly to calculate the eccentric anomaly")
        printlncond("    E = inv cos[(e + cos(v))/(1 + e*cos(v))]")
        printlncond("      = inv cos[(" & Format(e.len, genFloatFormat8) & " + cos(" & Format(dv, genFloatFormat8) & "))/(1 + " &
                    Format(e.len, genFloatFormat8) & "*cos(" & Format(dv, genFloatFormat8) & "))]")
        printlncond("      = " & Format(EA, genFloatFormat8) & " degrees")
        printlncond("    If v > 180, then subtract E from 360 to put it into the correct quadrant")
        If (dv > 180.0) Then
            EA = 360 - EA
            printlncond("    E = 360 - E = " & Format(EA, genFloatFormat8) & " degrees")
        Else
            printlncond("    No adjustment necessary")
        End If
        printlncond()

        MA = deg2rad(EA) - e.len * SIN_D(EA)
        printlncond("22. Use E to compute the mean anomaly at the epoch.")
        printlncond("    M = E - e*sin(E) where E **must** be in radians")
        printlncond("      = " & Format(deg2rad(EA), genFloatFormat8) & " - " & Format(e.len, genFloatFormat8) & "*sin(" &
                    Format(deg2rad(EA), genFloatFormat8) & ")")
        printlncond("      = " & Format(MA, genFloatFormat8) & " radians")
        printlncond()

        M0 = rad2deg(MA)
        printlncond("23. Convert M to degrees")
        printlncond("    M0 = (180*M)/PI = " & Format(M0, genFloatFormat8) & " degrees")
        printlncond()

        prt.println("Input State Vector (assuming units are km and km/s):")
        prt.println("  [" & Format(R.x, genFloatFormat8) & ", " & Format(R.y, genFloatFormat8) &
                    ", " & Format(R.z, genFloatFormat8) & "] (positional vector)")
        prt.println("  [" & Format(V.x, genFloatFormat8) & ", " & Format(V.y, genFloatFormat8) &
                    ", " & Format(V.z, genFloatFormat8) & "] (velocity vector)")
        prt.println()
        prt.println("State Vector gives a velocity of " & Round(V.len, 3) & " km/s at a distance")
        prt.println("of " & insertCommas(Round(R.len, 3)) & " km (" & insertCommas(Round(KM2Miles(R.len), 3)) &
                    " miles) above the center of the Earth.")
        prt.println()
        prt.println("Keplerian Elements are:")
        prt.println("  Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
        prt.println("  Eccentricity = " & Format(e.len, genFloatFormat8))
        prt.println("  Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
        prt.println("  RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
        prt.println("  Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
        prt.println("  Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Keplerian Elements are listed in the text area below"
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle TLEs menu items
    '----------------------------------------------------------------------------------------------------

    '-----------------------------------------------------------------
    ' Calculate length of the semi-major axis from the mean motion
    '
    ' calctype          AXIS_FROM_MMOTION_FROM_TLE or
    '                   AXIS_FROM_MMOTION_FROM_INPUT
    '-----------------------------------------------------------------
    Private Sub calcAxisFromMMotion(ByVal calctype As CalculationType)
        Dim idx As Integer = 0
        Dim MMotion, axis As Double
        Dim strTmp As String

        If calctype = CalculationType.AXIS_FROM_MMOTION_FROM_INPUT Then
            If queryForm.showQueryForm("Enter Mean Motion (revs/day)") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidReal(queryForm.getData1(), MMotion, HIDE_ERRORS) Then
                ErrMsg("Invalid Mean Motion - try again", "Invalid Mean Motion")
                Return
            End If
            If MMotion < 0.0 Then
                ErrMsg("Invalid Mean Motion - try again", "Invalid Mean Motion")
                Return
            End If
        Else           ' calculationtype.AXIS_FROM_MMOTION_FROM_TLE
            If Not getValidTLENum(idx) Then Return
        End If

        clearTextAreas()

        prt.setBoldFont(True)
        If calctype = CalculationType.AXIS_FROM_MMOTION_FROM_INPUT Then
            printlncond("Calculate the Length of the Semi-Major Axis from Mean Motion", CENTERTXT)
        Else
            printlncond("Calculate the Length of the Semi-Major Axis from TLE Data Set " &
                        (idx + 1), CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.AXIS_FROM_MMOTION_FROM_INPUT Then
            printlncond("Mean Motion entered: " & Format(MMotion, genFloatFormat8) & " revolutions per day")
        Else
            If chkboxShowInterimCalcs.Checked Then
                displayTLEHeader(prt)
                displayTLEItem(prt, idx)
                printlncond()
            End If
            If Not getTLEMeanMotion(idx, MMotion) Then
                ErrMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
                Return
            End If
            printlncond("Mean Motion extracted from TLE Data Set: " & Format(MMotion, genFloatFormat8) &
                        " revolutions per day")
        End If

        printlncond()

        axis = MeanMotion2Axis(MMotion, True)

        strTmp = "Mean Motion of " & Format(MMotion, genFloatFormat8) & " rev/day gives a = " &
            Format(axis, genFloatFormat8) & " km"

        printlncond("Thus, " & strTmp)

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = strTmp
    End Sub

    '-----------------------------------------------------------------
    ' Calculate the mean motion from the length of the semi-major axis
    '-----------------------------------------------------------------
    Private Sub calcMeanMotionFromAxis()
        Dim MMotion, axis As Double
        Dim result As String

        If queryForm.showQueryForm("Enter Length of Semi-Major Axis (in km)") <> Windows.Forms.DialogResult.OK Then Return

        If Not isValidReal(queryForm.getData1(), axis, HIDE_ERRORS) Then
            ErrMsg("Invalid Axis - try again", "Invalid Axis")
            Return
        End If

        If axis < 0.0 Then
            ErrMsg("Invalid Axis - try again", "Invalid Axis")
            Return
        End If

        clearTextAreas()

        prt.setBoldFont(True)
        printlncond("Calculate Mean Motion from Length of the Semi-Major Axis", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        printlncond("Axis entered: " & Format(axis, genFloatFormat8) & " km")
        printlncond()

        MMotion = Axis2MeanMotion(axis, True)
        printlncond()

        result = "a = " & Format(axis, genFloatFormat8) & " km gives Mean Motion = " &
            Format(MMotion, genFloatFormat8) & " rev/day"

        printlncond("Thus, " & result)

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = result
    End Sub

    '-------------------------------------------------------------------
    ' Decode a TLE data set to show what its elements are. There may 
    ' be multiple data sets to display based on how the selection
    ' of what to display is done.
    ' 
    ' decodeBy                  DECODE_TLE_BY_NAME, DECODE_TLE_BY_CATID,
    '                           or DECODE_TLE_BY_SETNUM
    '-------------------------------------------------------------------
    Private Sub decodeTLEData(ByVal decodeBy As CalculationType)
        Dim idx As Integer = 0
        Dim obj As String = ""

        If Not areTLEsLoaded() Then
            ErrMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
            Return
        End If

        If decodeBy = CalculationType.DECODE_TLE_BY_CATID Then
            If queryForm.showQueryForm("Enter Object's Catalog ID") <> Windows.Forms.DialogResult.OK Then Return
            obj = queryForm.getData1()
            obj = obj.Trim()
            idx = findTLECatID(obj, idx)
            If idx < 0 Then
                ErrMsg("No Object with the Catalog ID '" & obj & "' was found", "Invalid Catalog ID")
                Return
            End If
        ElseIf decodeBy = CalculationType.DECODE_TLE_BY_NAME Then
            If queryForm.showQueryForm("Enter Object's Name (or partial name)") <> Windows.Forms.DialogResult.OK Then Return
            obj = queryForm.getData1()
            obj = obj.Trim()
            idx = findTLEName(obj, idx)
            If idx < 0 Then
                ErrMsg("No Object with the name '" & obj & "' was found", "Invalid Object Name")
                Return
            End If
        Else            ' DECODE_TLE_BY_SETNUM
            If Not getValidTLENum(idx) Then Return
        End If

        clearTextAreas()

        If decodeBy = CalculationType.DECODE_TLE_BY_SETNUM Then
            decodeTLE(prt, idx, "TLE Data for Data Set " & (idx + 1))
        Else
            Do While idx >= 0
                If decodeBy = CalculationType.DECODE_TLE_BY_CATID Then
                    decodeTLE(prt, idx, "TLE Data for Catlog ID '" & obj & "', Data Set " & (idx + 1))
                    idx = findTLECatID(obj, idx + 1)
                Else
                    decodeTLE(prt, idx, "TLE Data for Object '" & obj & "', Data Set " & (idx + 1))
                    idx = findTLEName(obj, idx + 1)
                End If
            Loop
        End If

        prt.resetCursor()
    End Sub

    '-----------------------------------------------------------------
    ' Display all of the currently loaded TLE data.
    '-----------------------------------------------------------------
    Private Sub displayRawTLEData()
        Dim i As Integer

        If Not areTLEsLoaded() Then
            ErrMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
            Return
        End If

        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Currently Loaded TLE Data Sets", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("TLE Data Filename: " & getTLEFilename())
        prt.println("Number of TLE data sets: " & getNumTLEDBObjs())
        prt.println()

        prt.setFixedWidthFont()
        displayTLEHeader(prt)

        For i = 0 To getNumTLEDBObjs() - 1
            displayTLEItem(prt, i)
        Next

        prt.setProportionalFont()
        prt.resetCursor()
    End Sub


    '-----------------------------------------------------------------
    ' Extract Keplerian elements from a TLE data set
    '-----------------------------------------------------------------
    Private Sub extractKeplerianFromTLE()
        Dim inclin, ecc, axis, RAAN, argofperi, M0, MMotion As Double
        Dim daysIntoYear, iYear As Integer
        Dim epochDate As ASTDate = New ASTDate()
        Dim epochUT, dT As Double
        Dim padding As String
        Dim strTmp As String = " "
        Dim strTmp2 As String = " "
        Dim idx As Integer

        If Not areTLEsLoaded() Then
            ErrMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
            Return
        End If

        padding = String.Format("{0,60:s}", " ")        ' In case TLE data lines are not long enough

        If Not getValidTLENum(idx) Then Return

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Extract Keplerian Elements from TLE Data Set " & (idx + 1), CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        If chkboxShowInterimCalcs.Checked Then
            displayTLEHeader(prt)
            displayTLEItem(prt, idx)
            printlncond()
        End If

        getTLELine1(idx, strTmp)
        strTmp = strTmp & padding
        printlncond("1.  Get the epoch year from columns 19-20 of TLE Line 1")
        printlncond("    Epoch year from TLE is " & strTmp.Substring(18, 2))
        If Not isValidInt(strTmp.Substring(18, 2), iYear, HIDE_ERRORS) Then
            ErrMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year")
            Return
        End If
        printlncond("    Years < 57 are in the 2000s. Years > 56 are in the 1900s")
        If (iYear < 57) Then
            iYear = iYear + 2000
            printlncond("    Thus, Epoch Year = Year + 2000 = " & iYear)
        Else
            iYear = iYear + 1900
            printlncond("    Thus, Epoch Year = Year + 1900 = " & iYear)
        End If
        printlncond()

        printlncond("2.  Get the epoch day of year from columns 21-32 of TLE Line 1")
        printlncond("    Epoch day of year from TLE is " & strTmp.Substring(20, 12))
        If Not isValidReal(strTmp.Substring(20, 12), dT, HIDE_ERRORS) Then
            ErrMsg("Invalid Epoch Day of Year in TLE Data Set", "Invalid Epoch Day of Year")
            Return
        End If
        printlncond()

        daysIntoYear = Trunc(dT)
        printlncond("3.  Extract days into year as first 3 digits from " & strTmp.Substring(20, 12))
        printlncond("    days into year = INT(" & strTmp.Substring(20, 12) & ")")
        printlncond("                   = " & daysIntoYear)
        printlncond()

        epochDate = daysIntoYear2Date(iYear, daysIntoYear)
        printlncond("4.  Convert days into year to a calendar date for the year " & iYear)
        printlncond("    Epoch date = " & dateToStr(epochDate))
        printlncond()

        dT = dT - daysIntoYear
        printlncond("5.  Extract fractional part of the day from " & strTmp.Substring(20, 12))
        printlncond("    Frac Day = " & Round(dT, 8))
        printlncond()

        epochUT = Round(dT, 8) * 24.0
        printlncond("6.  Convert fractional part of day to UT")
        printlncond("    UT = (Frac Day)*24 = " & Round(dT, 8) & "*24")
        printlncond("       = " & Format(epochUT, genFloatFormat8) & " = " & timeToStr(epochUT, HMSFORMAT))
        printlncond()

        getTLELine2(idx, strTmp)
        strTmp = strTmp & padding
        printlncond("7.  Get the orbital inclination from columns 9-16 of TLE Line 2")
        printlncond("    Inclination = " & strTmp.Substring(8, 8).Trim() & " degrees")
        If Not isValidReal(strTmp.Substring(8, 8).Trim(), inclin, HIDE_ERRORS) Then
            ErrMsg("Invalid Inclination in TLE Data Set", "Invalid Inclination")
            Return
        End If
        printlncond("                = " & angleToStr(inclin, DMSFORMAT))
        printlncond()

        printlncond("8.  Get the RAAN from columns 18-25 of TLE Line 2")
        printlncond("    RAAN = " & strTmp.Substring(17, 8).Trim() & " degrees")
        If Not isValidReal(strTmp.Substring(17, 8).Trim(), RAAN, HIDE_ERRORS) Then
            ErrMsg("Invalid RAAN in TLE Data Set", "Invalid RAAN")
            Return
        End If
        printlncond("         = " & angleToStr(RAAN, DMSFORMAT))
        printlncond()

        printlncond("9.  Get the orbital eccentricity from columns 27-33 of TLE Line 2")
        printlncond("    (A leading decimal point is assumed)")
        strTmp2 = "0." & strTmp.Substring(26, 7).Trim()
        printlncond("    e = 0." & strTmp.Substring(26, 7).Trim())
        If Not isValidReal(strTmp2, ecc, HIDE_ERRORS) Then
            ErrMsg("Invalid Eccentricity in TLE Data Set", "Invalid Eccentricity")
            Return
        End If
        printlncond()

        printlncond("10. Get the argument of perigee from columns 35-42 of TLE Line 2")
        printlncond("    w = " & strTmp.Substring(34, 8).Trim() & " degrees")
        If Not isValidReal(strTmp.Substring(34, 8).Trim(), argofperi, HIDE_ERRORS) Then
            ErrMsg("Invalid Argument of Perigee in TLE Data Set", "Invalid Arg of Perigee")
            Return
        End If
        printlncond("      = " & angleToStr(argofperi, DMSFORMAT))
        printlncond()

        printlncond("11. Get the mean anomaly from columns 44-51 of TLE Line 2")
        printlncond("    M0 = " & strTmp.Substring(43, 8).Trim() & " degrees")
        If Not isValidReal(strTmp.Substring(43, 8).Trim(), M0, HIDE_ERRORS) Then
            ErrMsg("Invalid Mean Anomaly in TLE Data Set", "Invalid Mean Anomaly")
            Return
        End If
        printlncond("       = " & angleToStr(M0, DMSFORMAT))
        printlncond()

        printlncond("12. Get the mean motion from columns 53-63 of TLE Line 2")
        printlncond("    Mean Motion = " & strTmp.Substring(52, 11).Trim() & " rev/day")
        If Not isValidReal(strTmp.Substring(52, 11).Trim(), MMotion, HIDE_ERRORS) Then
            ErrMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
            Return
        End If
        printlncond()

        printlncond("13. Convert the mean motion to length of the semi-major axis")
        axis = MeanMotion2Axis(MMotion, True)

        prt.println("Epoch Date " & dateToStr(epochDate) & " at " & timeToStr(epochUT, HMSFORMAT) & " UT")
        prt.println("Keplerian Elements are:")
        prt.println("  Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
        prt.println("  Eccentricity = " & Format(ecc, genFloatFormat8))
        prt.println("  Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
        prt.println("  RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
        prt.println("  Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
        prt.println("  Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Keplerian Elements extracted from TLE Data Set " & (idx + 1) &
            " are listed in the text area below"
    End Sub

    '-----------------------------------------------------------------
    ' Search the TLE database by catalog ID or name and display the
    ' raw data for that object. Note that the TLE database may have
    ' multiple entries with the same catalog ID/name. An exact match
    ' is required for catalog IDs, but a partial match is all that
    ' is required when done by name.
    '
    ' searchBy              SEARCH_TLE_BY_NAME or SEARCH_TLE_BY_CATID
    '-----------------------------------------------------------------
    Private Sub searchForTLE(ByVal searchBy As CalculationType)
        Dim idx As Integer = 0
        Dim obj As String = ""

        If Not areTLEsLoaded() Then
            ErrMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
            Return
        End If

        If searchBy = CalculationType.SEARCH_TLE_BY_CATID Then
            If queryForm.showQueryForm("Enter Object's Catalog ID") <> Windows.Forms.DialogResult.OK Then Return
            obj = queryForm.getData1()
            obj = obj.Trim()
            idx = findTLECatID(obj, idx)
            If idx < 0 Then
                ErrMsg("No Object with the catalog ID '" & obj & "' was found", "Invalid Catalog ID")
                Return
            End If
        Else
            If queryForm.showQueryForm("Enter Object's Name (or partial name)") <> Windows.Forms.DialogResult.OK Then Return
            obj = queryForm.getData1()
            obj = obj.Trim()
            idx = findTLEName(obj, idx)
            If idx < 0 Then
                ErrMsg("No Object with the name '" & obj & "' was found", "Invalid Object Name")
                Return
            End If
        End If

        clearTextAreas()
        prt.setFixedWidthFont()
        displayTLEHeader(prt)

        Do While idx >= 0
            displayTLEItem(prt, idx)
            If searchBy = CalculationType.SEARCH_TLE_BY_CATID Then
                idx = findTLECatID(obj, idx + 1)
            Else
                idx = findTLEName(obj, idx + 1)
            End If

        Loop

        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Launch Sites menu items
    '----------------------------------------------------------------------------------------------------

    '--------------------------------------------------
    ' Calculate the orbital inclination when launched
    ' from a launch site.
    '
    ' calctype              INCLINATION_FROM_FILE or
    '                       INCLINATION_FROM_INPUT
    '--------------------------------------------------
    Private Sub calcInclination(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim Az As ASTAngle = New ASTAngle()
        Dim latObj As ASTLatLon = New ASTLatLon()
        Dim dLat, inclin As Double
        Dim latStr As String

        If calctype = CalculationType.INCLINATION_FROM_INPUT Then
            If queryForm.showQueryForm("Enter Launch Site latitude",
                                       "Enter Launch Azimuth") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidLat(queryForm.getData1(), latObj) Then
                ErrMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude")
                Return
            End If
            dLat = latObj.getLat()
        Else                ' INCLINATION_FROM_FILE
            If Not areSitesLoaded() Then Return
            If queryForm.showQueryForm("Enter Name of Launch Site",
                                       "Enter Launch Azimuth") <> Windows.Forms.DialogResult.OK Then Return
            idx = findSiteName(queryForm.getData1())
            If idx < 0 Then
                ErrMsg("No Launch Site named '" & queryForm.getData1() & "' exists" & vbNewLine &
                       "in the currently loaded launch sites database", "Invalid Site name")
                Return
            End If
            dLat = getSiteLat(idx)
            ' Next two lines get a latObj so that we can use latToStr later
            latStr = Format(dLat, genFloatFormat8)
            isValidLat(latStr, latObj, HIDE_ERRORS)
        End If
        If Not isValidAngle(queryForm.getData2(), Az, HIDE_ERRORS) Then
            ErrMsg("Invalid Azimuth - try again", "Invalid Azimuth")
            Return
        End If
        If (Az.getDecAngle < 0.0) Or (Az.getDecAngle > 360.0) Then
            ErrMsg("Invalid Azimuth - try again", "Invalid Azimuth")
            Return
        End If

        clearTextAreas()

        prt.setBoldFont(True)
        If calctype = CalculationType.INCLINATION_FROM_INPUT Then
            printlncond("Calculate the Orbital Inclination when launched from " & latToStr(latObj, DECFORMAT) &
                        " degrees", CENTERTXT)
        Else
            printlncond("Calculate the Orbital Inclination when launched from " & getSiteName(idx), CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.INCLINATION_FROM_INPUT Then
            printlncond("The Launch Site latitude entered was " & latToStr(latObj, DECFORMAT) &
                        " degrees (" & latToStr(latObj, DMSFORMAT) & ")")
            printlncond("The Azimuth entered was A = " & Az.getDecAngle & " degrees (" & angleToStr(Az, DMSFORMAT) & ")")
        Else
            printlncond("The selected launch site is at latitude " & dLat & " degrees (" & latToStr(latObj, DMSFORMAT) & ")")
            printlncond("The Azimuth entered was A = " & Az.getDecAngle & " degrees (" & angleToStr(Az, DMSFORMAT) & ")")
        End If
        printlncond()

        inclin = SIN_D(Az.getDecAngle()) * COS_D(dLat)
        inclin = INVCOS_D(inclin)
        printlncond("inclination = inv cos[sin(A)*cos(Lat)]")
        printlncond("            = inv cos[sin(" & Format(Az.getDecAngle, genFloatFormat8) &
                    ")*cos(" & Format(dLat, genFloatFormat8) & ")]")
        printlncond("            = " & Format(inclin, genFloatFormat8) & " degrees")
        printlncond()

        If calctype = CalculationType.INCLINATION_FROM_INPUT Then
            printlncond("Launching from " & angleToStr(dLat, DMSFORMAT) &
                        " at an Azimuth of " & angleToStr(Az, DMSFORMAT))
        Else
            printlncond("Launching from " & getSiteName(idx) & " at an Azimuth of " & angleToStr(Az, DMSFORMAT))
        End If
        printlncond("results in an orbital inclination of " & angleToStr(inclin, DMSFORMAT) & " (" &
            angleToStr(inclin, DECFORMAT) & " deg)")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Lat = " & latToStr(latObj, DMSFORMAT) & ", A = " & angleToStr(Az, DMSFORMAT) &
            " results in inclination = " & angleToStr(inclin, DMSFORMAT) & " (" &
            angleToStr(inclin, DECFORMAT) & " deg)"

    End Sub

    '--------------------------------------------------
    ' Calculate the launch azimuth required to achieve
    ' a specific orbital inclination.
    '
    ' calctype          LAUNCH_AZIMUTH_FROM_FILE or
    '                   LAUNCH_AZIMUTH_FROM_INPUT
    '--------------------------------------------------
    Private Sub calcLaunchAzimuth(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim inclin As ASTAngle = New ASTAngle()
        Dim latObj As ASTLatLon = New ASTLatLon()
        Dim dLat, Az As Double

        If calctype = CalculationType.LAUNCH_AZIMUTH_FROM_INPUT Then
            If queryForm.showQueryForm("Enter Launch Site latitude",
                                       "Enter Desired Orbital Inclination") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidLat(queryForm.getData1(), latObj) Then
                ErrMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude")
                Return
            End If
            dLat = latObj.getLat()
        Else
            If Not areSitesLoaded() Then Return
            If queryForm.showQueryForm("Enter Name of Launch Site",
                                       "Enter Desired Orbital Inclination") <> Windows.Forms.DialogResult.OK Then Return
            idx = findSiteName(queryForm.getData1())
            If idx < 0 Then
                ErrMsg("No Launch Site named '" & queryForm.getData1() & "' exists" & vbNewLine &
                       "in the currently loaded launch sites database", "Invalid Site name")
                Return
            End If
            dLat = getSiteLat(idx)
        End If

        ' If too close to polar, the cos function "blows up"
        If (dLat < -85.5) Or (dLat > 85.5) Then
            ErrMsg("Launch site latitude is out of range [-85.5, 85.5] - try again", "Invalid Site Latitude")
            Return
        End If

        If Not isValidAngle(queryForm.getData2(), inclin, HIDE_ERRORS) Then
            ErrMsg("Invalid inclination - try again", "Invalid inclination")
            Return
        End If
        If (inclin.getDecAngle < -90.0) Or (inclin.getDecAngle > 90.0) Then
            ErrMsg("Invalid Inclination - try again", "Invalid Inclination")
            Return
        End If

        clearTextAreas()

        prt.setBoldFont(True)
        If calctype = CalculationType.LAUNCH_AZIMUTH_FROM_INPUT Then
            printlncond("Calculate the required launch azimuth when launched from " & latToStr(latObj, DECFORMAT) &
                        " degrees", CENTERTXT)
        Else
            printlncond("Calculate the required launch azimuth when launched from " & getSiteName(idx), CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        printlncond("The selected launch site is at latitude " & dLat & " degrees (" &
                    latToStr(latObj, DMSFORMAT) & ")")
        printlncond("The desired inclination is " & inclin.getDecAngle() & " degrees (" &
                    angleToStr(inclin, DMSFORMAT) & ")")
        printlncond()

        If (Abs(inclin.getDecAngle()) < Abs(dLat)) Then
            printlncond("Error: Orbital Inclination must be greater than the launch latitude")
            lblResults.Text = "Error: Orbital Inclination must be greater than the launch latitude"
            Return
        End If

        Az = COS_D(inclin.getDecAngle) / COS_D(dLat)
        Az = INVSIN_D(Az)
        printlncond("A = inv sin[cos(inclination)/cos(Lat)]")
        printlncond("  = inv sin[cos(" & Format(inclin.getDecAngle, genFloatFormat8) &
                    ")/cos(" & Format(dLat, genFloatFormat8) & ")]")
        printlncond("  = " & Format(Az, genFloatFormat8) & " degrees")
        printlncond()

        printlncond("To achieve an orbital inclination of " & angleToStr(inclin.getDecAngle(), DECFORMAT) & " deg (" &
                    angleToStr(inclin.getDecAngle(), DMSFORMAT) & ")")
        If calctype = CalculationType.LAUNCH_AZIMUTH_FROM_INPUT Then
            printlncond("when launched from a site at " & dLat & " degrees (" &
                        latToStr(latObj, DMSFORMAT) & ") requires a")
        Else
            printlncond("when launching from " & getSiteName(idx) & " requires a")
        End If
        printlncond("launch azimuth of " & angleToStr(Az, DMSFORMAT) & " (" & angleToStr(Az, DECFORMAT) & " deg)")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Lat = " & latToStr(latObj, DMSFORMAT) & ", inclination = " &
                angleToStr(inclin, DMSFORMAT) & " requires azimuth of " & angleToStr(Az, DMSFORMAT) &
                " (" & angleToStr(Az, DECFORMAT) & " deg)"
    End Sub

    '---------------------------------------------------------
    ' Calculate the launch velocity that a rocket must provide
    ' when launched from a given launch site.
    '
    ' calctype      VELOCITY_FROM_FILE or VELOCITY_FROM_INPUT
    '---------------------------------------------------------
    Private Sub calcLaunchVelocity(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim latObj As ASTLatLon = New ASTLatLon()
        Dim dLat, dVelocity, deltaV As Double
        Dim latStr, tmpStr As String

        If calctype = CalculationType.VELOCITY_FROM_INPUT Then
            If queryForm.showQueryForm("Enter Launch Site latitude",
                                       "Enter Desired Velocity (km/hr)") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidLat(queryForm.getData1(), latObj) Then
                ErrMsg("Launch site latitude is invalid - try again", "Invalid Site Latitude")
                Return
            End If
        Else
            If Not areSitesLoaded() Then Return
            If queryForm.showQueryForm("Enter Name of Launch Site",
                                       "Enter Desired Velocity (km/hr)") <> Windows.Forms.DialogResult.OK Then Return
            idx = findSiteName(queryForm.getData1())
            If idx < 0 Then
                ErrMsg("No Launch Site named '" & queryForm.getData1() & "' exists" & vbNewLine &
                       "in the currently loaded launch sites database", "Invalid Site name")
                Return
            End If
            dLat = getSiteLat(idx)
            ' Next two lines get a latObj
            latStr = Format(dLat, genFloatFormat8)
            isValidLat(latStr, latObj, HIDE_ERRORS)
        End If
        dLat = latObj.getLatAngle.getDecAngle()
        If (dLat < -85.5) Or (dLat > 85.5) Then
            ErrMsg("Launch site latitude is out of range [-85.5, 85.5] - try again", "Invalid Site Latitude")
            Return
        End If

        If Not isValidReal(queryForm.getData2(), dVelocity, HIDE_ERRORS) Then
            ErrMsg("Invalid Velocity - try again", "Invalid Velocity")
            Return
        End If
        If dVelocity < 0.0 Then
            ErrMsg("Invalid Velocity - try again", "Invalid Velocity")
            Return
        End If

        clearTextAreas()

        prt.setBoldFont(True)
        If calctype = CalculationType.VELOCITY_FROM_INPUT Then
            printlncond("Calculate the Velocity a rocket must provide when launched from " &
                        latToStr(latObj, DECFORMAT) & " degrees", CENTERTXT)
        Else
            printlncond("Calculate the Velocity a rocket must provide when launched from " &
                        getSiteName(idx), CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        printlncond("The selected launch site is at latitude " & dLat & " degrees (" & latToStr(latObj, DMSFORMAT) & ")")
        printlncond("The required veclocity is " & insertCommas(Round(dVelocity, 2)) & " km/hr (" &
                    insertCommas(Round(KM2Miles(dVelocity), 2)) & " mph)")
        printlncond()

        deltaV = 1669.81 * COS_D(dLat)
        printlncond("'Free' velocity = 1669.81*cos(Lat) = 1669.81*cos(" & Format(dLat, genFloatFormat8) & ")")
        printlncond("                = " & Round(deltaV, 2) & " km/hr")
        printlncond()

        printlncond("Rocket must provide difference in velocities")
        printlncond("deltaV = Vel - 'free' = " & insertCommas(Round(dVelocity, 2)) & " - " & Round(deltaV, 2))
        deltaV = dVelocity - deltaV
        printlncond("       = " & insertCommas(Round(deltaV, 2)) & " km/hr")
        printlncond()

        printlncond("To achieve an orbital velocity of " & insertCommas(Round(dVelocity, 2)) & " km/hr (" &
            insertCommas(Round(KM2Miles(dVelocity), 2)) & " mph) when launched")
        If calctype = CalculationType.VELOCITY_FROM_INPUT Then
            tmpStr = "from " & latToStr(latObj, DMSFORMAT)
        Else
            tmpStr = "from " & getSiteName(idx)
        End If
        printlncond(tmpStr & ", a rocket must supply a")
        printlncond("velocity of " & insertCommas(Round(deltaV, 2)) & " km/hr (" &
                    insertCommas(Round(KM2Miles(deltaV), 2)) & " mph)")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Launch " & tmpStr & " requires " & insertCommas(Round(deltaV, 2)) &
            " km/hr to achieve " & insertCommas(Round(dVelocity, 2)) & " km/hr orbital velocity"
    End Sub

    '-----------------------------------------------------------------
    ' Display all of the currently loaded launch sites
    '-----------------------------------------------------------------
    Private Sub displayAllLaunchSites()
        Dim i As Integer
        Dim latObj As ASTLatLon = New ASTLatLon()
        Dim latStr As String

        If Not areSitesLoaded() Then Return

        clearTextAreas()

        prt.setBoldFont(True)
        prt.println("Currently Loaded Launch Sites", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("Launch Sites Data Filename: " & getSitesFilename())
        prt.println("Number of Launch Sites: " & getNumSites())
        prt.println()

        prt.setFixedWidthFont()

        For i = 0 To getNumSites() - 1
            ' Next two lines get a latObj so that we can use latToStr
            latStr = Format(getSiteLat(i), genFloatFormat8)
            isValidLat(latStr, latObj, HIDE_ERRORS)
            prt.println("Site " & i + 1 & ": " & getSiteName(i) & " at latitude " & getSiteLat(i) &
                        " degrees (" & latToStr(latObj, DMSFORMAT) & ")")
        Next

        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------------------------------------------------
    ' Handle Satellites menu items
    '----------------------------------------------------------------------------------------------------

    '---------------------------------------------------------
    ' Calculate a satellite's semi-major axis from its
    ' orbital period
    '
    ' calctype                  AXIS_FROM_PERIOD_FROM_TLE or
    '                           AXIS_FROM_PERIOD_FROM_INPUT
    '---------------------------------------------------------
    Private Sub calcAxisFromPeriod(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim inclin, ecc, axis, RAAN, M0, argofperi, MMotion, epochUT As Double
        Dim period As Double
        Dim timeObj As ASTTime = New ASTTime()
        Dim epochDate As ASTDate = New ASTDate()
        Dim tmpStr As String

        If calctype = CalculationType.AXIS_FROM_PERIOD_FROM_INPUT Then
            If queryForm.showQueryForm("Enter orbital period" & vbNewLine &
                                       "(in decimal minutes)") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidReal(queryForm.getData1(), period, HIDE_ERRORS) Then
                ErrMsg("Invalid orbital period - try again", "Invalid Orbital Period")
                Return
            End If
            If period < 0.0 Then
                ErrMsg("Invalid orbital period - try again", "Invalid Orbital Period")
                Return
            End If
            tmpStr = "manually entered data"
        Else
            If Not getValidTLENum(idx) Then Return
            If Not getKeplerianElementsFromTLE(idx, epochDate, epochUT, inclin, ecc, RAAN, argofperi, M0, MMotion) Then Return
            axis = MeanMotion2Axis(MMotion)
            tmpStr = "TLE data set " & (idx + 1)
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Use " & tmpStr & " to calculate the Semi-Major Axis from the Orbital Period", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.AXIS_FROM_PERIOD_FROM_INPUT Then
            printlncond("Orbital Period as entered: " & period & " minutes")
        Else
            printlncond("Mean Motion from TLE data: " & Format(MMotion, genFloatFormat8) & " revs/day")
            printlncond()

            period = 1440.0 / MMotion
            printlncond("Convert Mean Motion to orbital period")
            printlncond("    tau = 1440/(Mean Motion) = 1440/" & Format(MMotion, genFloatFormat8) & ")")
            printlncond("        = " & period & " minutes")
        End If
        printlncond()

        axis = 331.253274 * cuberoot(period ^ 2)
        printlncond("Compute the length of the object's semi-major axis")
        printlncond("   a = 331.253274*cuberoot(tau^2) = 331.253274*cuberoot(" & period & "^2)")
        printlncond("     = " & axis & " km")
        printlncond()

        printlncond("Object's orbital semi-major axis is " & insertCommas(Round(axis, 4)) & " km")

        lblResults.Text = "a=" & insertCommas(Round(axis, 4)) & " km for tau = " & Round(period, 4) & " minutes"

        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------------------------
    ' Calculate a satellite's distance from the center of the
    ' Earth at a given time.
    '
    ' calctype          SAT_DISTANCE_FROM_TLE or
    '                   SAT_DISTANCE_FROM_INPUT
    '---------------------------------------------------------
    Private Sub calcSatDistance(ByVal calctype As CalculationType)
        Dim idx, dateAdjust, iter As Integer
        Dim inclin, ecc, RAAN, argofperi, axis, M0, MMotion, Mt As Double
        Dim angleObj As ASTAngle = New ASTAngle()
        Dim epochDate As ASTDate = New ASTDate()
        Dim adjustedDate As ASTDate = New ASTDate()
        Dim epochUT, LCT, UT, GST, LST, JD, JDe, deltaT, EA, dv, dDist As Double
        Dim timeObj As ASTTime = New ASTTime()
        Dim solveKepler As TrueAnomalyType
        Dim prtObj As ASTPrt = Nothing

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        ' Figure out which method to use for solving Kepler's equation
        If radbtnSimpleIteration.Checked Then
            solveKepler = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        Else
            solveKepler = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        End If

        ' Set up whether to display interim results when solving Kepler's equation
        If chkboxShowInterimCalcs.Checked Then
            prtObj = prt
        Else
            prtObj = Nothing
        End If

        If calctype = CalculationType.SAT_DISTANCE_FROM_INPUT Then
            If coords3Form.showQueryForm("eccentricity", "axis", "M0", "Enter Keplerian elements ...") <>
                Windows.Forms.DialogResult.OK Then Return
            If Not isValidReal(coords3Form.txtData1.Text, ecc, HIDE_ERRORS) Then
                ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
                Return
            End If
            If (ecc < 0.0) Or (ecc > 1.0) Then
                ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
                Return
            End If
            If Not isValidReal(coords3Form.txtData2.Text, axis, HIDE_ERRORS) Then
                ErrMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
                Return
            End If
            If Not isValidAngle(coords3Form.txtData3.Text, angleObj, HIDE_ERRORS) Then
                ErrMsg("Invalid Mean Anomaly - try again", "Invalid Mean Anomaly")
                Return
            End If
            M0 = angleObj.getDecAngle()

            If queryForm.showQueryForm("Enter Epoch date (mm/dd/yyyy)",
                                       "Enter UT time for the Epoch") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidDate(queryForm.getData1(), epochDate, HIDE_ERRORS) Then
                ErrMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date")
                Return
            End If
            If Not isValidTime(queryForm.getData2(), timeObj, HIDE_ERRORS) Then
                ErrMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time")
                Return
            End If
            epochUT = timeObj.getDecTime()
            MMotion = Axis2MeanMotion(axis)
        Else
            If Not getValidTLENum(idx) Then Return
            If Not getKeplerianElementsFromTLE(idx, epochDate, epochUT, inclin, ecc, RAAN, argofperi, M0, MMotion) Then Return
            axis = MeanMotion2Axis(MMotion)
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        If calctype = CalculationType.SAT_DISTANCE_FROM_INPUT Then
            printlncond("Use manually entered Keplerian Elements to calculate distance", CENTERTXT)
        Else
            printlncond("Use TLE data set " & (idx + 1) & " to calculate distance", CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.SAT_DISTANCE_FROM_INPUT Then
            printlncond("1.  Calculate mean motion from the semi-major axis")
            printlncond("    Mean Motion = " & Format(MMotion, genFloatFormat8) & " revs/day")
        Else
            printlncond("1.  Calculate length of semi-major axis from mean motion")
            printlncond("    axis = " & insertCommas(Round(axis, 8)) & " km")
        End If
        printlncond()
        printlncond("    Resulting data items to use are ...")
        printlncond("    Epoch Date: " & dateToStr(epochDate) & " at " & timeToStr(epochUT, HMSFORMAT) & " UT")
        printlncond("    Keplerian Elements:")
        printlncond("       Eccentricity = " & Format(ecc, genFloatFormat8))
        printlncond("       Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
        printlncond("       Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")
        printlncond("       Mean Motion = " & Format(MMotion, genFloatFormat8) & " revs/day")
        printlncond()

        ' Do all the time-related calculations
        LCT = observer.getObsTime.getDecTime
        UT = LCTtoUT(LCT, chkboxDST.Checked, observer.getObsTimeZone, observer.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(observer.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, observer.getObsLon)

        printlncond("2.  Convert the observer LCT to UT, GST, and LST and adjust date if needed.")
        printlncond("    LCT = " & timeToStr(observer.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(observer.getObsDate))
        printcond("     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond("previous day ")
        ElseIf dateAdjust > 0 Then
            printcond("next day ")
        End If
        printlncond(dateToStr(adjustedDate) & ")")
        printlncond("    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond("    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond()

        JDe = dateToJD(epochDate.getMonth, epochDate.getdDay + (epochUT / 24.0), epochDate.getYear)
        JD = dateToJD(adjustedDate.getMonth, adjustedDate.getdDay + (UT / 24.0), adjustedDate.getYear)
        deltaT = JD - JDe
        printlncond("3.  Compute the total number of elapsed days, including fractional days,")
        printlncond("    since the epoch.")
        printlncond("    deltaT = " & Format(deltaT, genFloatFormat8) & " days")
        printlncond()

        Mt = M0 + 360 * deltaT * MMotion
        printlncond("4.  Propagate the mean anomaly by deltaT days")
        printlncond("    Mt = M0 + 360*deltaT*(Mean Motion) = " & Format(M0, genFloatFormat8) & " + 360*" &
                    Format(deltaT, genFloatFormat8) & "*(" & Format(MMotion, genFloatFormat8) & ")")
        printlncond("       = " & Format(Mt, genFloatFormat8) & " degrees")
        printlncond()

        printlncond("5.  If necessary, adjust Mt to be in the range [0,360]")
        printcond("    Mt = Mt MOD 360 = " & Format(Mt, genFloatFormat8) & " MOD 360 = ")
        Mt = xMOD(Mt, 360.0)
        printlncond(Format(Mt, genFloatFormat8) & " degrees")
        printlncond()

        printlncond("6.  Solve Kepler's equation to get the eccentric anomaly from Mt")
        If radbtnSimpleIteration.Checked Then
            EA = calcSimpleKepler(prtObj, M0, ecc, termCriteria, iter)
        Else
            EA = calcNewtonKepler(prtObj, M0, ecc, termCriteria, iter)
        End If
        printlncond("    E = " & Format(EA, genFloatFormat8) & " degrees")
        printlncond()

        dv = Sqrt((1 + ecc) / (1 - ecc)) * TAN_D(EA / 2.0)
        dv = 2.0 * INVTAN_D(dv)
        printlncond("7.  Compute the object's true anomaly")
        printlncond("    v = 2 * inv tan[ sqrt[(1 + e)/(1 - e)] * tan(E/2) ]")
        printlncond("      = 2 * inv tan[ sqrt[(1+" & ecc & ")/(1-" & ecc & ")] * tan(" &
                    Format(EA, genFloatFormat8) & "/2) ]")
        printlncond("      = " & Format(dv, genFloatFormat8) & " degrees")
        printlncond()

        dDist = (axis * (1 - ecc ^ 2)) / (1 + ecc * COS_D(dv))
        printlncond("8.  Compute the distance from the center of the Earth")
        printlncond("    Dist = a*(1-e^2)/(1 + e*cos(v))")
        printlncond("         = " & Format(axis, genFloatFormat8) & "*(1-" & Format(ecc, genFloatFormat8) &
                    "^2)/(1+" & Format(ecc, genFloatFormat8) & "*cos(" & Format(dv, genFloatFormat8) & "))")
        printlncond("         = " & insertCommas(dDist))
        printlncond()

        printlncond("On " & dateToStr(observer.getObsDate) & ", object is/was " & insertCommas(Round(dDist, 3)) & " km")
        printlncond("(" & insertCommas(Round(KM2Miles(dDist), 3)) & " miles) from the center of the Earth")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Distance: " & insertCommas(Round(dDist, 3)) & " km (" &
            insertCommas(Round(KM2Miles(dDist), 3)) & " miles) from the center of the Earth"
    End Sub

    '-----------------------------------------------------------
    ' Calculate the size of a satellite's footprint, what %
    ' of the Earth its footprint covers, and how long it will
    ' be in the footprint.
    '-----------------------------------------------------------
    Private Sub calcSatFootprint()
        Dim dist, V, r_fp, Fpct, Fpt As Double

        If queryForm.showQueryForm("Distance from center" & vbNewLine & "of Earth in km",
                                   "Orbital Velocity (km/s)") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(queryForm.getData1(), dist, HIDE_ERRORS) Then
            ErrMsg("Invalid orbital distance - try again", "Invalid Orbital Distance")
            Return
        End If
        If (dist < EarthRadius) Then
            ErrMsg("Orbital distance must be > Radius of Earth", "Invalid Orbital Distance")
            Return
        End If
        If Not isValidReal(queryForm.getData2(), V, HIDE_ERRORS) Then
            ErrMsg("Invalid orbital distance - try again", "Invalid Orbital Distance")
            Return
        End If
        If (V < 0.0) Then
            ErrMsg("Invalid orbital velocity - try again", "Invalid Orbital Velocity")
            Return
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Calculate Basic Data about a Satellite's Footprint", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        r_fp = EarthRadius * deg2rad(INVCOS_D(EarthRadius / dist))
        printlncond("Compute the max radius of the satellite's footprint")
        printlncond("   r_fp = (pi/180)*r_e*invcos(r_e/r) = (pi/180)*" & EarthRadius & "*invcos(" & EarthRadius & "/" & dist & ")")
        printlncond("        = " & r_fp & " km")
        printlncond()

        Fpct = 50 * (dist - EarthRadius) / dist
        printlncond("Compute the fraction of the Earth's surface that is in the footprint")
        printlncond("   F = 50*(r - r_e)/r = 50*(" & dist & " - " & EarthRadius & ")/" & dist)
        printlncond("     = " & Round(Fpct, 1) & "%")
        printlncond()

        Fpt = r_fp / (30 * V)
        printlncond("Calculate how long the satellite will be in the footprint")
        printlncond("   FPt = r_fp/(30*V) = " & r_fp & "/(30*" & V & ")")
        printlncond("       = " & Fpt & " minutes")
        printlncond()

        printlncond("A satellite at a distance of " & insertCommas(Round(dist, 2)) & " km that is")
        printlncond("travelling at " & Round(V, 2) & " km/s has a footprint radius of " & Round(r_fp, 2) & " km")
        printlncond("and covers " & Round(Fpct, 1) & "% of the Earth's surface. It will remain in the")
        printlncond("footprint for " & Round(Fpt, 2) & " minutes")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Footprint radius=" & Round(r_fp, 2) & " km, " & Round(Fpct, 1) & "% coverage, in footprint for " &
            Round(Fpt, 2) & " minutes"

    End Sub

    '----------------------------------------------
    ' Calculate a satellite's location.
    '
    ' calctype          LOCATE_SAT_FROM_TLE or
    '                   LOCATE_SAT_FROM_INPUT
    '----------------------------------------------
    Private Sub calcSatLocation(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim solveKepler As TrueAnomalyType
        Dim inclin, ecc, axis, RAAN, argofperi, M0, MMotion As Double
        Dim epochDate As ASTDate = New ASTDate()
        Dim epochUT As Double
        Dim timeObj As ASTTime = New ASTTime()
        Dim topoCoord As ASTCoord
        Dim h_sea As Double

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter Observer's height" & vbNewLine & "above sea level (in meters)") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(queryForm.getData1(), h_sea, HIDE_ERRORS) Then
            ErrMsg("Invalid Observer Height - try again", "Invalid Height")
            Return
        End If

        ' Figure out which method to use for solving Kepler's equation
        If radbtnSimpleIteration.Checked Then
            solveKepler = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        Else
            solveKepler = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        End If

        If calctype = CalculationType.LOCATE_SAT_FROM_INPUT Then
            If Not coords6Form.validateKeplerianElements(inclin, ecc, axis, RAAN, argofperi, M0) Then Return
            If queryForm.showQueryForm("Enter Epoch date (mm/dd/yyyy)",
                                       "Enter UT time for the Epoch") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidDate(queryForm.getData1(), epochDate, HIDE_ERRORS) Then
                ErrMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date")
                Return
            End If
            If Not isValidTime(queryForm.getData2(), timeObj, HIDE_ERRORS) Then
                ErrMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time")
                Return
            End If
            epochUT = timeObj.getDecTime()
        Else
            If Not getValidTLENum(idx) Then Return

            If Not getTLEMeanMotion(idx, MMotion) Then
                ErrMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
                Return
            End If
            axis = MeanMotion2Axis(MMotion)
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        If calctype = CalculationType.LOCATE_SAT_FROM_INPUT Then
            printlncond("Find a satellite's location from manually entered data", CENTERTXT)
        Else
            printlncond("Find a satellite's location from TLE data set " & (idx + 1), CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.LOCATE_SAT_FROM_INPUT Then
            printlncond("1.  Data entered was")
            printlncond("    Epoch Date: " & dateToStr(epochDate) & " at " & timeToStr(epochUT, HMSFORMAT) & " UT")
            printlncond("    Keplerian Elements are:")
            printlncond("       Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
            printlncond("       Eccentricity = " & Format(ecc, genFloatFormat8))
            printlncond("       Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
            printlncond("       RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
            printlncond("       Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
            printlncond("       Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")
            printlncond()
            MMotion = Axis2MeanMotion(axis)
            printlncond("2.  Convert Mean Motion to semi-major axis")
            printlncond("    Mean Motion = " & Format(MMotion, genFloatFormat8) & " revs/day")
        Else
            printlncond("1.  Extract epoch date and Keplerian Elements from TLE data set " & (idx + 1))
            If chkboxShowInterimCalcs.Checked Then
                displayTLEHeader(prt)
                displayTLEItem(prt, idx)
                printlncond()
            End If
            If Not getKeplerianElementsFromTLE(idx, epochDate, epochUT, inclin, ecc, RAAN,
                                               argofperi, M0, MMotion) Then Return
            printlncond("    Epoch Date: " & dateToStr(epochDate) & " at " & timeToStr(epochUT, HMSFORMAT) & " UT")
            printlncond("    Keplerian Elements are:")
            printlncond("       Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
            printlncond("       Eccentricity = " & Format(ecc, genFloatFormat8))
            printlncond("       RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
            printlncond("       Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
            printlncond("       Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")
            printlncond("       Mean Motion = " & Format(MMotion, genFloatFormat8) & " revs/day")
            printlncond()
            axis = MeanMotion2Axis(MMotion)
            printlncond("2.  Convert Mean Motion to semi-major axis")
            printlncond("    axis = " & insertCommas(Round(axis, 8)) & " km")
        End If
        printlncond()

        topoCoord = Keplerian2Topo(3, observer, h_sea, chkboxDST.Checked, epochDate,
                                         epochUT, inclin, ecc, axis, RAAN,
                                         argofperi, M0, MMotion, solveKepler,
                                         termCriteria, prt)

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "For this observer, the satellite is at " &
            angleToStr(topoCoord.getAltAngle.getDecAngle, DMSFORMAT) & " Alt_topo, " &
            angleToStr(topoCoord.getAzAngle.getDecAngle, DMSFORMAT) & " Az_topo"
    End Sub

    '---------------------------------------------------------
    ' Calculate a satellite's orbital period.
    '
    ' calctype          SAT_PERIOD_FROM_TLE or
    '                   SAT_PERIOD_FROM_INPUT
    '---------------------------------------------------------
    Private Sub calcSatOrbitalPeriod(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim inclin, ecc, axis, RAAN, M0, argofperi, MMotion, epochUT As Double
        Dim period As Double
        Dim epochDate As ASTDate = New ASTDate()
        Dim tmpStr As String

        If calctype = CalculationType.SAT_PERIOD_FROM_INPUT Then
            If queryForm.showQueryForm("Enter length of semi-major" & vbNewLine &
                                       "axis (in km)") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidReal(queryForm.getData1(), axis, HIDE_ERRORS) Then
                ErrMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
                Return
            End If
            If axis < 0.0 Then
                ErrMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
                Return
            End If
            tmpStr = "manually entered data"
        Else
            If Not getValidTLENum(idx) Then Return
            If Not getKeplerianElementsFromTLE(idx, epochDate, epochUT, inclin, ecc, RAAN, argofperi, M0, MMotion) Then Return
            axis = MeanMotion2Axis(MMotion)
            tmpStr = "TLE data set " & (idx + 1)
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Use " & tmpStr & " to calculate the Orbital Period", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.SAT_PERIOD_FROM_INPUT Then
            printlncond("Length of semi-major axis as entered: " & insertCommas(Round(axis, 8)) & " km")
        Else
            printlncond("Mean Motion from TLE data: " & Format(MMotion, genFloatFormat8) & " revs/day")
            printlncond("Semi-major axis computed from Mean Motion: " & insertCommas(Round(axis, 8)) & " km")
        End If
        printlncond()

        period = 0.00016587 * Sqrt(axis ^ 3)
        printlncond("Compute the object's orbital period")
        printlncond("   tau = 0.00016587*sqrt(a^3) = 0.00016587*sqrt(" & axis & "^3)")
        printlncond("       = " & period & " minutes")
        printlncond()

        printlncond("Object's orbital period is " & Round(period, 4) & " minutes (" &
                    Round(period / 60.0, 4) & " hours)")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "tau=" & Round(period, 4) & " minutes (" &
            Round(period / 60.0, 4) & " hours) for a = " & insertCommas(Round(axis, 4)) & " km"
    End Sub

    '---------------------------------------------------------
    ' Calculate a satellite's perigee and apogee distances.
    '
    ' calctype          SAT_PERI_APOGEE_FROM_TLE or
    '                   SAT_PERI_APOGEE_FROM_INPUT
    '---------------------------------------------------------
    Private Sub calcSatPerigeeApogee(ByVal calctype As CalculationType)
        Dim idx As Integer
        Dim inclin, ecc, axis, RAAN, M0, argofperi, MMotion, epochUT As Double
        Dim apogeeDist, perigeeDist As Double
        Dim epochDate As ASTDate = New ASTDate()
        Dim tmpStr As String

        If calctype = CalculationType.SAT_PERI_APOGEE_FROM_INPUT Then
            If queryForm.showQueryForm("Enter orbital eccentricity", "Enter length of semi-major" & vbNewLine &
                                       "axis (in km)") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidReal(queryForm.getData1(), ecc, HIDE_ERRORS) Then
                ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
                Return
            End If
            If (ecc < 0.0) Or (ecc > 1.0) Then
                ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
                Return
            End If
            If Not isValidReal(queryForm.getData2(), axis, HIDE_ERRORS) Then
                ErrMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
                Return
            End If
            tmpStr = "manually entered data"
        Else
            If Not getValidTLENum(idx) Then Return
            If Not getKeplerianElementsFromTLE(idx, epochDate, epochUT, inclin, ecc, RAAN, argofperi, M0, MMotion) Then Return
            axis = MeanMotion2Axis(MMotion)
            tmpStr = "TLE data set " & (idx + 1)
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Use " & tmpStr & " to calculate Perigee/Apogee distances", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        If calctype = CalculationType.SAT_PERI_APOGEE_FROM_INPUT Then
            printlncond("Eccentricity as entered: " & Format(ecc, genFloatFormat8))
            printlncond("Length of semi-major axis as entered: " & insertCommas(Round(axis, 8)) & " km")
        Else
            printlncond("Eccentricity from TLE data: " & Format(ecc, genFloatFormat8))
            printlncond("Mean Motion from TLE data: " & Format(MMotion, genFloatFormat8) & " revs/day")
            printlncond("Semi-major axis computed from Mean Motion: " & insertCommas(Round(axis, 8)) & " km")
        End If
        printlncond()

        perigeeDist = axis * (1 - ecc)
        printlncond("Compute the object's distance at perigee.")
        printlncond("   r_p = a * (1 - e) = " & axis & " * (1 - " & ecc & ")")
        printlncond("       = " & insertCommas(Round(perigeeDist, 8)) & " km (" &
                    insertCommas(Round(KM2Miles(perigeeDist), 8)) & " miles) from the center of the Earth")
        printlncond()

        apogeeDist = axis * (1 + ecc)
        printlncond("Compute the object's distance at apogee.")
        printlncond("   r_a = a * (1 + e) = " & axis & " * (1 + " & ecc & ")")
        printlncond("       = " & insertCommas(Round(apogeeDist, 8)) & " km (" &
                    insertCommas(Round(KM2Miles(apogeeDist), 8)) & " miles) from the center of the Earth")
        printlncond()


        printlncond("At perigee, object is " & insertCommas(Round(perigeeDist, 2)) & " km (" &
                    insertCommas(Round(KM2Miles(perigeeDist), 2)) & " miles) from the center of the Earth.")
        printlncond("At apogee, object is " & insertCommas(Round(apogeeDist, 2)) & " km (" &
                    insertCommas(Round(KM2Miles(apogeeDist), 2)) & " miles) from the center of the Earth")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "Perigee Dist: " & insertCommas(Round(perigeeDist, 2)) & " km (" &
                    insertCommas(Round(KM2Miles(perigeeDist), 2)) & " miles), Apogee Dist: " &
                    insertCommas(Round(apogeeDist, 2)) & " km (" &
                    insertCommas(Round(KM2Miles(apogeeDist), 2)) & " miles)"
    End Sub

    '---------------------------------------------------------
    ' Calculate a satellite's orbital velocity when in a
    ' circular orbit.
    '---------------------------------------------------------
    Private Sub calcSatVelocityCircular()
        Dim axis, period, V, V2 As Double

        ' For circular orbits, the radius and the semi-major axis are the same
        If queryForm.showQueryForm("Enter orbital radius in km" & vbNewLine &
                                   "(i.e., distance above center of Earth)") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(queryForm.getData1(), axis, HIDE_ERRORS) Then
            ErrMsg("Invalid orbital radius - try again", "Invalid Orbital Radius")
            Return
        End If
        If (axis < EarthRadius) Then
            ErrMsg("Orbital radius must be > Radius of Earth", "Invalid Orbital Radius")
            Return
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Compute Orbital Velocity for a Circular Orbit", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        period = 0.00016587 * Sqrt(axis ^ 3)
        printlncond("Compute the object's orbital period")
        printlncond("   tau = 0.00016587*sqrt(r^3) = 0.00016587*sqrt(" & axis & "^3)")
        printlncond("       = " & period & " minutes")
        printlncond()

        V = (PI * axis) / (30 * period)
        printlncond("Compute the object's orbital velocity")
        printlncond("   V = (pi*r)/(30*tau) = (pi*" & axis & ")/(30*" & period & ")")
        printlncond("     = " & Format(V, genFloatFormat8) & " km/s")
        printlncond()

        V2 = 631.348115 * Sqrt(1 / axis)
        printlncond("Using vis-viva law instead,")
        printlncond("   V = 631.348115*sqrt(1/r) = 631.348115*sqrt(1/" & axis & ")")
        printlncond("     = " & Format(V2, genFloatFormat8) & " km/s")
        printlncond()

        printlncond("An object in a circular orbit that is " & insertCommas(Round(axis, 4)) & " km")
        printlncond("above the center of the Earth has an orbital velocity")
        printlncond("of " & Round(V, 2) & " km/s (" & Round(KM2Miles(V), 2) & " miles/s, or " &
                    insertCommas(Round(3600 * KM2Miles(V), 2)) & " mph)")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "r=" & insertCommas(Round(axis, 2)) & " km, tau=" & Round(period, 2) & " minutes, V=" &
            Round(V, 2) & " km/s (" & Round(KM2Miles(V), 2) & " miles/s)"
    End Sub

    '---------------------------------------------------------
    ' Calculate a satellite's orbital velocity when in an
    ' elliptical orbit.
    '---------------------------------------------------------
    Private Sub calcSatVelocityElliptical()
        Dim axis, dist, perigeeDist, apogeeDist, ecc, V, Vap, Vperi As Double

        If coords3Form.showQueryForm("eccentricity", "distance", "axis", "Enter orbital data ...") <>
            Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(coords3Form.txtData1.Text, ecc, HIDE_ERRORS) Then
            ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            Return
        End If
        If (ecc < 0.0) Or (ecc >= 1.0) Then
            ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            Return
        End If
        If Not isValidReal(coords3Form.txtData2.Text, dist, HIDE_ERRORS) Then
            ErrMsg("Invalid orbital distance - try again", "Invalid Orbital Distance")
            Return
        End If
        If (dist < EarthRadius) Then
            ErrMsg("Orbital distance must be > Radius of Earth", "Invalid Orbital Distance")
            Return
        End If
        If Not isValidReal(coords3Form.txtData3.Text, axis, HIDE_ERRORS) Then
            ErrMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
            Return
        End If
        If (axis < EarthRadius) Then
            ErrMsg("Semi-major axis must be > Radius of Earth", "Invalid Semi-Major Axis")
            Return
        End If

        ' Be sure that 2/r > 1/a
        If (1 / axis) >= (2 / dist) Then
            ErrMsg("Semi-major axis or Dist is incorrect", "Invalid axis or distance")
            Return
        End If

        clearTextAreas()
        prt.setBoldFont(True)
        printlncond("Compute Orbital Velocity for an Elliptical Orbit", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        V = 631.348115 * Sqrt((2 / dist) - (1 / axis))
        printlncond("Use the vis-viva law to compute the object's orbital velocity")
        printlncond("   V = 631.348115*sqrt[(2/r)-(1/a)] = 631.348115*sqrt[(2/" & dist & ")-(1/" & axis & ")]")
        printlncond("     = " & Format(V, genFloatFormat8) & " km/s")
        printlncond()

        perigeeDist = axis * (1 - ecc)
        printlncond("Compute the object's distance at perigee.")
        printlncond("   r_p = a * (1 - e) = " & axis & " * (1 - " & ecc & ")")
        printlncond("       = " & insertCommas(Round(perigeeDist, 8)) & " km (" &
                    insertCommas(Round(KM2Miles(perigeeDist), 8)) & " miles) from the center of the Earth")
        printlncond()

        Vperi = 631.348115 * Sqrt((2 / perigeeDist) - (1 / axis))
        printlncond("Use the vis-viva law to compute the object's orbital velocity at perigee")
        printlncond("   Vperigee = 631.348115*sqrt[(2/r_p)-(1/a)] = 631.348115*sqrt[(2/" & perigeeDist & ")-(1/" & axis & ")]")
        printlncond("            = " & Format(Vperi, genFloatFormat8) & " km/s")
        printlncond()

        apogeeDist = axis * (1 + ecc)
        printlncond("Compute the object's distance at apogee.")
        printlncond("   r_a = a * (1 + e) = " & axis & " * (1 + " & ecc & ")")
        printlncond("       = " & insertCommas(Round(apogeeDist, 8)) & " km (" &
                    insertCommas(Round(KM2Miles(apogeeDist), 8)) & " miles) from the center of the Earth")
        printlncond()

        Vap = 631.348115 * Sqrt((2 / apogeeDist) - (1 / axis))
        printlncond("Use the vis-viva law to compute the object's orbital velocity at apogee")
        printlncond("   Vapogee = 631.348115*sqrt[(2/r_a)-(1/a)] = 631.348115*sqrt[(2/" & apogeeDist & ")-(1/" & axis & ")]")
        printlncond("           = " & Format(Vap, genFloatFormat8) & " km/s")
        printlncond()

        printlncond("An object in an elliptical orbit that is " & insertCommas(Round(dist, 4)) & " km")
        printlncond("above the center of the Earth has an orbital velocity")
        printlncond("of " & Round(V, 2) & " km/s (" & Round(KM2Miles(V), 2) & " miles/s, or " &
                    insertCommas(Round(3600 * KM2Miles(V), 2)) & " mph)")
        printlncond()
        printlncond("Distance at perigee is " & insertCommas(Round(perigeeDist, 4)) & " km and")
        printlncond("orbital velocity is " & Round(Vperi, 2) & " km/s (" & Round(KM2Miles(Vperi), 2) & " miles/s, or " &
                    insertCommas(Round(3600 * KM2Miles(Vperi), 2)) & " mph)")
        printlncond()
        printlncond("Distance at apogee is " & insertCommas(Round(apogeeDist, 4)) & " km and")
        printlncond("orbital velocity is " & Round(Vap, 2) & " km/s (" & Round(KM2Miles(Vap), 2) & " miles/s, or " &
                    insertCommas(Round(3600 * KM2Miles(Vap), 2)) & " mph)")

        prt.setProportionalFont()
        prt.resetCursor()

        lblResults.Text = "V=" & Round(V, 2) & " km/s, Vperigee=" & Round(Vperi, 2) & " km/s, Vapogee=" &
            Round(Vap, 2) & " km/s"
    End Sub

    '---------------------------------------------------------
    ' Calculate a satellite's rise/set times. This method
    ' only provides an estimate. It does so by periodically
    ' calculating the horizon coordinates over some
    ' interval of time. The results are rough estimates
    ' at best, due in part to using such a simple
    ' orbit propagator
    '
    ' The starting time is assumed to be the observer LCT. As
    ' the LCT is incremented, it is rounded to 2 decimal seconds
    ' so that what is printed as the LCT will actually match
    ' what is used for the calculations. Failure to do this
    ' means that the coordinates calculated by a decimal
    ' LCT value (e.g., 7.5043138888) may differ greatly 
    ' from that calculated using a less precise HMS
    ' value (e.g., 7:30:15.53 vs 7:30:15.52999968).
    '
    ' calctype          LOCATE_SAT_FROM_TLE or
    '                   LOCATE_SAT_FROM_INPUT
    '---------------------------------------------------------
    Private Sub calcSatRiseSet(ByVal calctype As CalculationType)
        Dim i, idx As Integer
        Dim inclin, ecc, axis, RAAN, argofperi, M0, MMotion As Double
        Dim epochDate As ASTDate = New ASTDate()
        Dim tmpDate As ASTDate = New ASTDate()
        Dim saveDay, saveSign As Integer
        Dim epochUT, LCT, saveJD, maxHours, incrementT, deltaT, JD, dT As Double
        Dim timeObj As ASTTime = New ASTTime()
        Dim topoCoord As ASTCoord
        Dim h_sea As Double
        Dim tmpStr As String
        Dim solveKepler As TrueAnomalyType

        ' Validate the observer location data
        If Not validateGUIObsLoc() Then Return

        If queryForm.showQueryForm("Enter Observer's height" & vbNewLine & "above sea level (in meters)") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(queryForm.getData1(), h_sea, HIDE_ERRORS) Then
            ErrMsg("Invalid Observer Height - try again", "Invalid Height")
            Return
        End If

        ' Figure out which method to use for solving Kepler's equation
        If radbtnSimpleIteration.Checked Then
            solveKepler = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER
        Else
            solveKepler = TrueAnomalyType.SOLVE_NEWTON_METHOD_KEPLER
        End If

        If calctype = CalculationType.RISE_SET_FROM_INPUT Then
            If Not coords6Form.validateKeplerianElements(inclin, ecc, axis, RAAN, argofperi, M0) Then Return
            If queryForm.showQueryForm("Enter Epoch date (mm/dd/yyyy)",
                                       "Enter UT time for the Epoch") <> Windows.Forms.DialogResult.OK Then Return
            If Not isValidDate(queryForm.getData1(), epochDate, HIDE_ERRORS) Then
                ErrMsg("The Epoch Date specified is invalid - try again", "Invalid Epoch Date")
                Return
            End If
            If Not isValidTime(queryForm.getData2(), timeObj, HIDE_ERRORS) Then
                ErrMsg("The Epoch UT time specified is invalid - try again", "Invalid Epoch Time")
                Return
            End If
            epochUT = timeObj.getDecTime()
            MMotion = Axis2MeanMotion(axis)
        Else
            If Not getValidTLENum(idx) Then Return
            If Not getKeplerianElementsFromTLE(idx, epochDate, epochUT, inclin, ecc, RAAN, argofperi, M0, MMotion) Then Return
            axis = MeanMotion2Axis(MMotion)
        End If

        If queryForm.showQueryForm("Maximum Hours from observer LCT" & vbNewLine & "(enter decimal hours, NOT HMS!)",
                                   "Increment (decimal hours or HMS)") <> Windows.Forms.DialogResult.OK Then Return
        If Not isValidReal(queryForm.getData1(), maxHours, HIDE_ERRORS) Then
            ErrMsg("Invalid Max Hours - try again", "Invalid Max Hours")
            Return
        End If
        If maxHours < 0 Then
            ErrMsg("Invalid Max Hours - try again", "Invalid Max Hours")
            Return
        End If
        If Not isValidTime(queryForm.getData2(), timeObj, HIDE_ERRORS) Then
            ErrMsg("Invalid Time Increment- try again", "Invalid Time Increment")
            Return
        End If
        deltaT = timeObj.getDecTime()

        clearTextAreas()
        prt.setBoldFont(True)
        If calctype = CalculationType.RISE_SET_FROM_INPUT Then
            printlncond("Use manually entered Keplerian Elements to estimate rise/set times", CENTERTXT)
        Else
            printlncond("Use TLE data set " & (idx + 1) & " to estimate rise/set times", CENTERTXT)
        End If
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()

        printlncond("    Epoch Date: " & dateToStr(epochDate) & " at " & timeToStr(epochUT, HMSFORMAT) & " UT")
        printlncond("    Keplerian Elements are:")
        printlncond("       Inclination = " & Format(inclin, genFloatFormat8) & " degrees")
        printlncond("       Eccentricity = " & Format(ecc, genFloatFormat8))
        printlncond("       Length of Semi-major axis = " & insertCommas(Round(axis, 8)) & " km")
        printlncond("       RAAN = " & Format(RAAN, genFloatFormat8) & " degrees")
        printlncond("       Argument of Perigee = " & Format(argofperi, genFloatFormat8) & " degrees")
        printlncond("       Mean anomaly at the epoch = " & Format(M0, genFloatFormat8) & " degrees")
        printlncond("       Mean Motion = " & Format(MMotion, genFloatFormat8) & " revs/day")
        printlncond()

        LCT = observer.getObsTime.getDecTime()
        prt.println("Start at " & timeToStr(LCT, HMSFORMAT) & " on " & dateToStr(observer.getObsDate))
        prt.println("for a maximum of " & maxHours & " hours, incremented by " & timeToStr(deltaT, HMSFORMAT) & " (" &
                    timeToStr(deltaT, DECFORMAT) & " hours)")
        prt.println()

        i = 1
        incrementT = 0.0
        tmpDate = observer.getObsDate()

        prt.println(String.Format("{0,5:s} {1,10:s} {2,11:s} {3,16:s} {4,15:s}", " ", "Date   ", "LCT Time ",
                                  "Topo Alt    ", "Topo Az     "))
        prt.println(String.Format("{0,65:s}", "=").Replace(" ", "="))

        ' Show location at the start time and save the day to determine when a new day starts,
        ' save the sign of the altitude to determine rise/set when the sign changes, and
        ' save the JD to interpolate a rise/set time. JD is needed rather than saving the LCT
        ' because we may cross a date boundary
        prt.print(String.Format("{0,-5:d} {1,10:s} {2,11:s} ", i, dateToStr(tmpDate), timeToStr(LCT, HMSFORMAT)))

        topoCoord = Keplerian2Topo(observer, h_sea, chkboxDST.Checked, epochDate, epochUT, inclin,
                                         ecc, axis, RAAN, argofperi, M0, MMotion,
                                         solveKepler, termCriteria)
        prt.println(String.Format("{0,16:s} {1,15:s}", angleToStr(topoCoord.getAltAngle.getDecAngle(), DMSFORMAT),
                                  angleToStr(topoCoord.getAzAngle.getDecAngle(), DMSFORMAT)))

        saveDay = tmpDate.getiDay()
        ' Use LCT even though it really should be UT. This allows us to interpolate the LCT times
        ' w/o the need to interpolate UTs and then convert to the LCT
        saveJD = dateToJD(tmpDate.getMonth, tmpDate.getiDay + (LCT / 24.0), tmpDate.getYear)
        saveSign = Sign(topoCoord.getAltAngle.getDecAngle())

        ' Loop until maxHours is reached
        Do While incrementT < maxHours
            i = i + 1

            ' add deltaT to the time to get a new LCT - use JDs to do so and round
            ' to nearest 100ths of a second so that times printed for the user
            ' match the actual times used for determining the horizon coordinates
            JD = saveJD + deltaT / 24.0
            tmpDate = JDtoDate(JD)
            LCT = Frac(tmpDate.getdDay) * 24.0
            LCT = Round(LCT, 2)
            ' Update the observer object with the new time and potentially new date. This
            ' will not affect the values displayed in the GUI. We could create a new observer
            ' object and use it, but other methods in this program use the global
            ' observer object. So, we will just update the global observer object.
            observer.setObsDateTime(tmpDate.getMonth, tmpDate.getiDay, tmpDate.getYear, LCT)

            If saveDay = tmpDate.getiDay Then
                tmpStr = " "
            Else
                tmpStr = dateToStr(tmpDate)
                saveDay = tmpDate.getiDay
            End If
            prt.print(String.Format("{0,-5:d} {1,10:s} {2,11:s} ", i, tmpStr, timeToStr(LCT, HMSFORMAT)))

            ' propagate to the new time
            topoCoord = Keplerian2Topo(observer, h_sea, chkboxDST.Checked, epochDate, epochUT, inclin,
                                         ecc, axis, RAAN, argofperi, M0, MMotion,
                                         solveKepler, termCriteria)
            prt.print(String.Format("{0,16:s} {1,15:s}", angleToStr(topoCoord.getAltAngle.getDecAngle(), DMSFORMAT),
                                    angleToStr(topoCoord.getAzAngle.getDecAngle(), DMSFORMAT)))

            ' See if the object has transition above or below the horizon
            If Sign(topoCoord.getAltAngle.getDecAngle()) <> saveSign Then
                dT = saveJD + (JD - saveJD) / 2.0  ' This is the JD halfway between the previous and current time
                dT = Frac(JDtoDate(dT).getdDay) * 24.0  ' Convert JD to a date and get the LCT time from it
                prt.print(" " & timeToStr(dT, HMSFORMAT) & " est. ")
                If saveSign < 0 Then
                    prt.print("rise")
                Else
                    prt.print("set")
                End If
            End If
            prt.println()

            saveSign = Sign(topoCoord.getAltAngle.getDecAngle())
            saveJD = JD
            incrementT = incrementT + deltaT
        Loop

        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------------------
    ' Set the termination criteria for solving Kepler's equation
    '------------------------------------------------------------
    Private Sub setTerminationCriteria()
        Dim dTerm As Double = termCriteria

        clearTextAreas()
        prt.setBoldFont(True)
        prt.println("Set Termination Criteria for use in solving Kepler's Equation", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        If queryForm.showQueryForm("Enter Termination Criteria in radians" & vbNewLine &
                                   "(ex: 0.000002)") <> Windows.Forms.DialogResult.OK Then
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        ' Validate the termination criteria
        If Not isValidReal(queryForm.getData1(), dTerm, HIDE_ERRORS) Then
            ErrMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
            prt.println("Termination criteria was not changed from " & termCriteria & " radians")
            Return
        End If

        prt.println("Prior termination criteria was: " & termCriteria & " radians")
        If dTerm < KeplerMinCriteria Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too small, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        If dTerm > 1 Then
            dTerm = KeplerMinCriteria
            prt.println("Criteria entered was too large, so it has been reset to " & dTerm & " radians")
            prt.println()
        End If
        termCriteria = dTerm

        prt.println("Termination criteria is now set to " & termCriteria & " radians")
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Routines for dealing with TLE data files
    '-------------------------------------------------------------------------------------------------------

    '----------------------------------------------------------
    ' Convert length of the semi-major axis to mean motion
    '
    ' axis                  length of semi-major axis in km
    ' showInterim           show intermediate results if
    '                       true and checkbox is true
    '
    ' Returns mean motion in revs/day
    '----------------------------------------------------------
    Private Function Axis2MeanMotion(ByVal axis As Double) As Double
        Return Axis2MeanMotion(axis, False)
    End Function
    Private Function Axis2MeanMotion(ByVal axis As Double, ByVal showInterim As Boolean) As Double
        Dim N, MMotion As Double

        N = (1 / (2 * PI * axis)) * Sqrt(muEarth / axis)
        If showInterim Then
            printlncond("    Convert length of semi-major axis to rev/s")
            printlncond("    N = [1/(2*pi*a)]*sqrt(muEarth/a)")
            printlncond("      = [1/(2*pi*" & axis & ")]*sqrt(" & insertCommas(muEarth) & "/" & axis & ")")
            printlncond("      = " & N & " rev/s")
            printlncond()
        End If

        MMotion = N * 86400.0
        If showInterim Then
            printlncond("    Convert N from rev/s to rev/day")
            printlncond("    Mean Motion = 86,400*N = 86,400*" & N)
            printlncond("                = " & Format(MMotion, genFloatFormat8) & " rev/day")
            printlncond()
        End If

        Return MMotion
    End Function

    '----------------------------------------------------------
    ' Convert mean motion to length of the semi-major axis
    '
    ' MMotion               mean motion in revs/day
    ' showInterim           show intermediate results if
    '                       true and checkbox is true
    '
    ' Returns length of the semi-major axis
    '----------------------------------------------------------
    Private Function MeanMotion2Axis(ByVal MMotion As Double) As Double
        Return MeanMotion2Axis(MMotion, False)
    End Function
    Private Function MeanMotion2Axis(ByVal MMotion As Double, ByVal showInterim As Boolean) As Double
        Dim N, axis As Double

        N = MMotion / 86400.0
        If showInterim Then
            printlncond("    Convert mean motion from rev/day to rev/s")
            printlncond("    N = (Mean Motion)/86,400 = " & Format(MMotion, genFloatFormat8) & "/86,400")
            printlncond("      = " & N & " rev/s")
            printlncond()
        End If

        axis = 2.0 * PI * N
        axis = muEarth / (axis ^ 2)
        axis = cuberoot(axis)
        If showInterim Then
            printlncond("    Calculate length of the semi-major axis")
            printlncond("    a = cuberoot[muEarth/((2*pi*N)^2)]")
            printlncond("      = cuberoot[" & insertCommas(muEarth) & "/(2*pi*" & N & ")^2)]")
            printlncond("      = " & axis & " km")
            printlncond()
        End If

        Return axis
    End Function

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------
    ' Clear the text areas in the GUI
    '--------------------------------------
    Private Sub clearTextAreas()
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '-------------------------------------------
    ' Ask user for a valid TLE data set number
    '
    ' idx           indx into TLE data set
    '
    ' Returns true if the user enters a valid
    ' TLE data set number and sets the value
    ' of idx to that number. This assumes
    ' 0-based indexing!
    '-------------------------------------------
    Private Function getValidTLENum(ByRef idx As Integer) As Boolean
        Dim result As Boolean = False

        idx = -1
        If Not areTLEsLoaded() Then
            ErrMsg("No TLE data sets are currently loaded.", "No TLE Data Sets Loaded")
            Return False
        End If

        If queryForm.showQueryForm("Enter desired TLE Data set" & vbNewLine &
                                   "number (1-" & getNumTLEDBObjs() & ")") <> Windows.Forms.DialogResult.OK Then Return False

        If isValidInt(queryForm.getData1(), idx, HIDE_ERRORS) Then
            If (idx < 1) Or (idx > getNumTLEDBObjs()) Then
                ErrMsg("No TLE Data Set number " & idx & " exists", "Invalid TLE Data Set Number")
                idx = -1
                Return False
            Else
                idx = idx - 1               ' adjust for 0-based indexing!
            End If
        Else
            idx = -1
            ErrMsg("Invalid TLE data set number - try again", "Invalid TLE Data Set Number")
            Return False
        End If

        Return True
    End Function

    '----------------------------------------------------
    ' Determine what time zone radio button is selected
    '
    ' Returns a time zone type for whatever is the
    ' currently selected time zone radio button
    '----------------------------------------------------
    Private Function getSelectedRBStatus() As TimeZoneType
        If radbtnPST.Checked() Then
            Return TimeZoneType.PST
        ElseIf radbtnMST.Checked() Then
            Return TimeZoneType.MST
        ElseIf radbtnCST.Checked() Then
            Return TimeZoneType.CST
        ElseIf radbtnEST.Checked() Then
            Return TimeZoneType.EST
        Else
            Return TimeZoneType.LONGITUDE
        End If
    End Function

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '---------------------------------------------------------------
    ' Set the observer location in the GUI. This is intended to be
    ' done one time only during the program initialization since a
    ' default location may be read from a data file.
    '
    ' obs           observer location object
    '---------------------------------------------------------------
    Private Sub setObsDataInGUI(ByVal obs As ASTObserver)
        txtboxLat.Text = latToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxLon.Text = lonToStr(obs.getObsLocation(), DMSFORMAT)
        txtboxDate.Text = dateToStr(obs.getObsDate())
        txtboxLCT.Text = timeToStr(obs.getObsTime(), HMSFORMAT)

        If obs.getObsTimeZone = TimeZoneType.PST Then
            radbtnPST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.MST Then
            radbtnMST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.CST Then
            radbtnCST.Checked = True
        ElseIf obs.getObsTimeZone = TimeZoneType.EST Then
            radbtnEST.Checked = True
        Else
            radbtnLon.Checked = True
        End If

    End Sub

    '----------------------------------------------
    ' See if the observer location, date, and time
    ' currently in the GUI is valid.
    '
    ' Returns true if valid, otherwise false.
    '----------------------------------------------
    Private Function validateGUIObsLoc() As Boolean
        Return isValidObsLoc(observer, txtboxLat.Text, txtboxLon.Text, getSelectedRBStatus().ToString,
                             txtboxDate.Text, txtboxLCT.Text)
    End Function

End Class
