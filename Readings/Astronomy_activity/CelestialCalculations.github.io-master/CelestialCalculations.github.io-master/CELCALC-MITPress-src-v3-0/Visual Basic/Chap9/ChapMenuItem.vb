﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define possible menu items. These help in deciding what routine to
    ' execute when a menu item is selected
    Friend Enum CalculationType
        ' Coord Sys menu
        CART_TO_SPHERICAL
        SPHERICAL_TO_CART
        ECI_TO_HORIZON
        ECI_TO_TOPO
        STATEVECTOR_TO_KEPLERIAN
        KEPLERIAN_TO_STATEVECTOR
        'TLEs menu
        LOAD_TLE_DATA
        DISPLAY_RAW_TLE_DATA
        SEARCH_TLE_BY_NAME
        SEARCH_TLE_BY_CATID
        DECODE_TLE_BY_NAME
        DECODE_TLE_BY_CATID
        DECODE_TLE_BY_SETNUM
        AXIS_FROM_MMOTION_FROM_TLE
        AXIS_FROM_MMOTION_FROM_INPUT
        MEAN_MOTION_FROM_AXIS
        KEPLERIAN_FROM_TLE
        ' Launch Sites menu
        LOAD_LAUNCH_SITES
        DISPLAY_ALL_LAUNCH_SITES
        INCLINATION_FROM_FILE
        INCLINATION_FROM_INPUT
        LAUNCH_AZIMUTH_FROM_FILE
        LAUNCH_AZIMUTH_FROM_INPUT
        VELOCITY_FROM_FILE
        VELOCITY_FROM_INPUT
        'Satellite menu
        LOCATE_SAT_FROM_TLE
        LOCATE_SAT_FROM_INPUT
        RISE_SET_FROM_TLE
        RISE_SET_FROM_INPUT
        SAT_DISTANCE_FROM_TLE
        SAT_DISTANCE_FROM_INPUT
        SAT_PERI_APOGEE_FROM_TLE
        SAT_PERI_APOGEE_FROM_INPUT
        SAT_PERIOD_FROM_TLE
        SAT_PERIOD_FROM_INPUT
        AXIS_FROM_PERIOD_FROM_TLE
        AXIS_FROM_PERIOD_FROM_INPUT
        SAT_VELOCITY_CIRCULAR
        SAT_VELOCITY_ELLIPTICAL
        SAT_FOOTPRINT
        TERM_CRITERIA
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub

End Class
