﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This class provides a GUI to allow a user to enter
' 3 data items for a coordinate system.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Windows.Forms

Public Class Form3Coords
    '********************************************************
    ' Create a query form instance and initialize everything
    '********************************************************
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Me.lblLabel1.Text = ""
        Me.txtData1.Text = ""
        Me.lblLabel2.Text = ""
        Me.txtData2.Text = ""
        Me.lblLabel3.Text = ""
        Me.txtData3.Text = ""
    End Sub

    '********************************************************************************************
    ' Public methods that calling routines can use
    '********************************************************************************************

    '*****************************************************
    ' Display the query form and center it on the parent.
    '
    ' sLabel1       1st label to display
    ' sLabel2       2nd label to display
    ' sLabel3       3rd label to display
    ' txt           title to display
    '*****************************************************
    Public Function showQueryForm(ByVal sLabel1 As String, ByVal sLabel2 As String,
                                  ByVal sLabel3 As String) As DialogResult
        Return showQueryForm(sLabel1, sLabel2, sLabel3, "Enter Data ...")
    End Function
    Public Function showQueryForm(ByVal sLabel1 As String, ByVal sLabel2 As String,
                                  ByVal sLabel3 As String, ByVal txt As String) As DialogResult
        Me.lblLabel1.Text = sLabel1
        Me.txtData1.Text = ""
        Me.lblLabel2.Text = sLabel2
        Me.txtData2.Text = ""
        Me.lblLabel3.Text = sLabel3
        Me.txtData3.Text = ""
        Me.Text = txt

        Me.StartPosition = FormStartPosition.CenterParent
        Return Me.ShowDialog()
    End Function

    '****************************************************************
    ' Provide 'get' functions to return the data from the query form
    '****************************************************************
    Public Function getData1() As String
        Return txtData1.Text
    End Function
    Public Function getData2() As String
        Return txtData2.Text
    End Function
    Public Function getData3() As String
        Return txtData3.Text
    End Function

    '-------------------------------------------------------------------------------------------
    ' Private methods used only in this class
    '-------------------------------------------------------------------------------------------

    '---------------------------------
    ' Handle closing the query dialog
    '---------------------------------
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub
End Class