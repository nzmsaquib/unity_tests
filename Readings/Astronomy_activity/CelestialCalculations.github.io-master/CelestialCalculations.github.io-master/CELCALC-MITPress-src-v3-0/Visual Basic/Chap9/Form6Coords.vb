﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This class provides a GUI to allow a user to enter
' 6 data items for a state vector or Keplerian
' elements.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Windows.Forms

Imports ASTUtils
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTVect

Public Class Form6Coords
    '********************************************************
    ' Create a query form instance and initialize everything
    '********************************************************
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Me.lblLabel1.Text = ""
        Me.txtData1.Text = ""
        Me.lblLabel2.Text = ""
        Me.txtData2.Text = ""
        Me.lblLabel3.Text = ""
        Me.txtData3.Text = ""
        Me.lblLabel4.Text = ""
        Me.txtData4.Text = ""
        Me.lblLabel5.Text = ""
        Me.txtData5.Text = ""
        Me.lblLabel6.Text = ""
        Me.txtData6.Text = ""
    End Sub

    '********************************************************************************************
    ' Public methods that calling routines can use
    '********************************************************************************************

    '************************************************************
    ' Display a form to get Keplerian elements and validate the
    ' data values the user enters
    '
    ' inclin, ecc, axis, RAAN, argofperi, M0   Keplerian elements
    '
    ' Returns true if the Keplerian elements are valid and also
    ' returns the Keplerian elements
    '************************************************************
    Public Function validateKeplerianElements(ByRef inclin As Double, ByRef ecc As Double, ByRef axis As Double,
                                              ByRef RAAN As Double, ByRef argofperi As Double,
                                              ByRef M0 As Double) As Boolean
        Dim angleObj As ASTAngle = New ASTAngle(0.0)

        inclin = 0.0
        ecc = 0.0
        axis = 0.0
        RAAN = 0.0
        argofperi = 0.0
        M0 = 0.0

        If Me.showQueryForm(False) <> Windows.Forms.DialogResult.OK Then Return False

        If Not isValidAngle(Me.txtData1.Text, angleObj, HIDE_ERRORS) Then
            ErrMsg("Invalid inclination - try again", "Invalid Inclination")
            Return False
        End If
        inclin = angleObj.getDecAngle()
        If Not isValidReal(Me.txtData2.Text, ecc, HIDE_ERRORS) Then
            ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            Return False
        End If
        If (ecc < 0.0) Or (ecc > 1.0) Then
            ErrMsg("Invalid eccentricity - try again", "Invalid Eccentricity")
            Return False
        End If
        If Not isValidReal(Me.txtData3.Text, axis, HIDE_ERRORS) Then
            ErrMsg("Invalid semi-major axis - try again", "Invalid Semi-Major Axis")
            Return False
        End If
        If Not isValidAngle(Me.txtData4.Text, angleObj, HIDE_ERRORS) Then
            ErrMsg("Invalid RAAN - try again", "Invalid RAAN")
            Return False
        End If
        RAAN = angleObj.getDecAngle()
        If Not isValidAngle(Me.txtData5.Text, angleObj, HIDE_ERRORS) Then
            ErrMsg("Invalid Argument of Perigee - try again", "Invalid Arg of Peri")
            Return False
        End If
        argofperi = angleObj.getDecAngle()
        If Not isValidAngle(Me.txtData6.Text, angleObj, HIDE_ERRORS) Then
            ErrMsg("Invalid Mean Anomaly - try again", "Invalid Mean Anomaly")
            Return False
        End If
        M0 = angleObj.getDecAngle()

        Return True
    End Function

    '*******************************************************
    ' Display a form to get a state vector and validate the
    ' data values the user enters
    '
    ' R                 positional vector
    ' V                 velocity vector
    '
    ' Returns true if the state vector is valid, and also
    ' returns R and V
    '*******************************************************
    Public Function validateStateVect(ByRef R As ASTVect, ByRef V As ASTVect) As Boolean
        Dim x, y, z As Double

        R.setVect(0.0, 0.0, 0.0)
        V.setVect(0.0, 0.0, 0.0)

        If Me.showQueryForm(True) <> Windows.Forms.DialogResult.OK Then Return False

        ' Validate the postional (R) vector
        If Not isValidReal(Me.txtData1.Text, x, HIDE_ERRORS) Then
            ErrMsg("Invalid positional Rx value - try again", "Invalid Rx")
            Return False
        End If
        If Not isValidReal(Me.txtData2.Text, y, HIDE_ERRORS) Then
            ErrMsg("Invalid positional Ry value - try again", "Invalid Ry")
            Return False
        End If
        If Not isValidReal(Me.txtData3.Text, z, HIDE_ERRORS) Then
            ErrMsg("Invalid positional Rz value - try again", "Invalid Rz")
            Return False
        End If
        R.setVect(x, y, z)

        ' Validate the velocity (V) vector
        If Not isValidReal(Me.txtData4.Text, x, HIDE_ERRORS) Then
            ErrMsg("Invalid positional Vx value - try again", "Invalid Vx")
            Return False
        End If
        If Not isValidReal(Me.txtData5.Text, y, HIDE_ERRORS) Then
            ErrMsg("Invalid positional Vy value - try again", "Invalid Vy")
            Return False
        End If
        If Not isValidReal(Me.txtData6.Text, z, HIDE_ERRORS) Then
            ErrMsg("Invalid positional Vz value - try again", "Invalid Vz")
            Return False
        End If
        V.setVect(x, y, z)
        Return True
    End Function

    '-------------------------------------------------------------------------------------------
    ' Private methods used only in this class
    '-------------------------------------------------------------------------------------------

    '---------------------------------
    ' Handle closing the query dialog
    '---------------------------------
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub

    '--------------------------------------------------------
    ' Display the query form and center it on the parent.
    '
    ' stateVect         true if state vector, else Keplerian
    '--------------------------------------------------------
    Private Function showQueryForm(ByVal stateVect As Boolean) As DialogResult
        If stateVect Then
            Me.lblLabel1.Text = "Rx"
            Me.txtData1.Text = ""
            Me.lblLabel2.Text = "Ry"
            Me.txtData2.Text = ""
            Me.lblLabel3.Text = "Rz"
            Me.txtData3.Text = ""

            Me.lblLabel4.Text = "Vx"
            Me.txtData4.Text = ""
            Me.lblLabel5.Text = "Vy"
            Me.txtData5.Text = ""
            Me.lblLabel6.Text = "Vz"
            Me.txtData6.Text = ""
            Me.Text = "Enter State Vector components ..."
        Else
            Me.lblLabel1.Text = "inclination"
            Me.txtData1.Text = ""
            Me.lblLabel2.Text = "eccentricity"
            Me.txtData2.Text = ""
            Me.lblLabel3.Text = "axis"
            Me.txtData3.Text = ""
            Me.lblLabel4.Text = "RAAN"
            Me.txtData4.Text = ""
            Me.lblLabel5.Text = "Arg of Perigee"
            Me.txtData5.Text = ""
            Me.lblLabel6.Text = "M0"
            Me.txtData6.Text = ""
            Me.Text = "Enter Keplerian Elements ..."
        End If

        Me.StartPosition = FormStartPosition.CenterParent
        Return Me.ShowDialog()
    End Function

End Class