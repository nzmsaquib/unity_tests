﻿'**********************************************************
'            Chapter 4 - Coordinate System Conversions
'                      Copyright (c) 2018
'                    Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' to do coordinate system conversions. The main GUI was
' built with the Visual Basic form editor, so many
' "globals" are created external to the code here.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap4.ChapMenuItem

Public Class Chap4GUI
    Private Const AboutBoxText = "Chapter 4 - Coordinate System Conversions"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' Each menu item selection sets this variable to know what to do when the calculation button is pressed
    Private calculationToDo As CalculationType = CalculationType.NO_CALC_SELECTED

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt

    ' Galactic coordinates for Epoch 1950.0
    Private Const GalEpoch1950 = 1950.0
    Private Const GalRA1950 = 192.25            ' in degrees
    Private Const GalDecl1950 = 27.4            ' in degrees
    Private Const GalAscNode1950 = 33.0         ' in degrees

    ' Galactic coordinates for Epoch J2000
    Private Const GalEpoch2000 = 2000.0
    Private Const GalRA2000 = 192.8598            ' in degrees
    Private Const GalDecl2000 = 27.128027         ' in degrees
    Private Const GalAscNode2000 = 32.9319        ' in degrees

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearAllTextAreas()

        ' Create the menu items for the Coordinate Systems and Kepler's Equation menus
        createChapMenus()

        ' Now initialize the rest of the GUI
        clearAllTextAreas()
        chkboxShowInterimCalcs.Checked = False
        calculationToDo = CalculationType.NO_CALC_SELECTED

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '-------------------------------------------------------
    ' Create all of the menu items for the Chapter menus
    '-------------------------------------------------------
    ' Create all of the menu items for the Coordinate Systems menus
    Private Sub createChapMenus()
        ' Coordinate System Conversions menu
        mnuCoordSys.DropDownItems.Add(createMenuItem("Hour Angle to Right Ascension", CalculationType.HA_RA))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Right Ascension to Hour Angle", CalculationType.RA_HA))
        mnuCoordSys.DropDownItems.Add("-")

        mnuCoordSys.DropDownItems.Add(createMenuItem("Horizon to Equatorial", CalculationType.HORIZON_EQUATORIAL))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Equatorial to Horizon", CalculationType.EQUATORIAL_HORIZON))
        mnuCoordSys.DropDownItems.Add("-")

        mnuCoordSys.DropDownItems.Add(createMenuItem("Ecliptic to Equatorial", CalculationType.ECLIPTIC_EQUATORIAL))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Equatorial to Ecliptic", CalculationType.EQUATORIAL_ECLIPTIC))
        mnuCoordSys.DropDownItems.Add("-")

        mnuCoordSys.DropDownItems.Add(createMenuItem("Galactic to Equatorial", CalculationType.GALACTIC_EQUATORIAL))
        mnuCoordSys.DropDownItems.Add(createMenuItem("Equatorial to Galactic", CalculationType.EQUATORIAL_GALACTIC))
        mnuCoordSys.DropDownItems.Add("-")

        mnuCoordSys.DropDownItems.Add(createMenuItem("Precession Correction", CalculationType.PRECESSION_CORR))

        ' Kepler's Equation menu
        mnuKeplersEq.DropDownItems.Add(createMenuItem("Simple Iteration", CalculationType.KEPLER_SIMPLE))
        mnuKeplersEq.DropDownItems.Add(createMenuItem("Newton/Raphson", CalculationType.KEPLER_NEWTON))
    End Sub

    '-----------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler
    '-----------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '------------------------------------------------------------------------------------------------
    ' Routines below handle all menu items
    '------------------------------------------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '--------------------------------------------------------------
    ' This handles all 'Coordinate System' and 'Kepler's Equation'
    ' menu items. All we do here is capture what menu item is to be
    ' done when the 'Calculate' button is clicked.
    '--------------------------------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType

        prt.clearTextArea()

        Select Case calctype
            ' Coord Systems Menu
            Case CalculationType.HA_RA
                setDataLabels("Hour Angle (hh:mm:ss.ss)", "UT (hh:mm:ss.ss)",
                              "Longitude (ex: 96.52E, 30.4W)", "Date (mm/dd/yyyy)")
                lblResults.Text = "Convert Hour Angle to Right Ascension"
            Case CalculationType.RA_HA
                setDataLabels("Right Ascension (hh:mm:ss.ss)", "UT (hh:mm:ss.ss)",
                              "Longitude (ex: 96.52E, 30.4W)", "Date (mm/dd/yyyy)")
                lblResults.Text = "Convert Right Ascension to Hour Angle"
            Case CalculationType.HORIZON_EQUATORIAL
                setDataLabels("Altitude (xxxd yym zz.zzs)", "Azimuth (xxxd yym zz.zzs)",
                              "Latitude (ex: 38.5N, 20.5S)")
                lblResults.Text = "Convert Horizon Coordinates to Equatorial Coordinates"
            Case CalculationType.EQUATORIAL_HORIZON
                setDataLabels("Hour Angle (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                              "Latitude (ex: 38.5N, 20.5S)")
                lblResults.Text = "Convert Equatorial Coordinates to Horizon Coordinates"
            Case CalculationType.ECLIPTIC_EQUATORIAL
                setDataLabels("Ecliptic Lat (xxxd yym zz.zzs)", "Ecliptic Lon (xxxd yym zz.zzs)",
                              "Epoch Coordinates are in (ex: 2000.0)")
                lblResults.Text = "Convert Ecliptic Coordinates to Equatorial Coordinates"
            Case CalculationType.EQUATORIAL_ECLIPTIC
                setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                              "Epoch Coordinates are in (ex: 2000.0)")
                lblResults.Text = "Convert Equatorial Coordinates to Ecliptic Coordinates"
            Case CalculationType.GALACTIC_EQUATORIAL
                setDataLabels("Galactic Lat (xxxd yym zz.zzs)", "Galactic Lon (xxxd yym zz.zzs)",
                              "Epoch (1950.0 or 2000.0)")
                lblResults.Text = "Convert Galactic Coordinates to Equatorial Coordinates"
            Case CalculationType.EQUATORIAL_GALACTIC
                setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                              "Epoch (1950.0 or 2000.0)")
                lblResults.Text = "Convert Equatorial Coordinates to Galactic Coordinates"
            Case CalculationType.PRECESSION_CORR
                setDataLabels("Right Ascension (hh:mm:ss.ss)", "Declination (xxxd yym zz.zzs)",
                              "Epoch to Convert From (ex: 1950.0)", "Epoch to Convert To (ex: 2000.0)")
                lblResults.Text = "Compute Precession Correction"

                ' Kepler's Equation menu
            Case CalculationType.KEPLER_SIMPLE
                setDataLabels("Mean Anomaly (in degrees)", "Stop Criteria in Radians (ex: 0.000002)",
                              "Orbital Eccentricity")
                lblResults.Text = "Solve Kepler's Equation via Simple Iteration"
            Case CalculationType.KEPLER_NEWTON
                setDataLabels("Mean Anomaly (in degrees)", "Stop Criteria in Radians (ex: 0.000002)",
                              "Orbital Eccentricity")
                lblResults.Text = "Solve Kepler's Equation via Newton/Raphson Method"
        End Select

        calculationToDo = calctype
    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        clearAllTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " &
                    "intermediate results as the various calculations are performed. Next, select the desired " &
                    "calculation to perform from the 'Coord Systems' or 'Kepler's Equation' menu. " &
                    "Once a menu item is selected, you will be asked to enter one to four lines of data in the text " &
                    "boxes beneath the 'Show Intermediate Calculations' checkbox. The values to be entered " &
                    "depend upon what menu item was selected. Finally, after entering all required data, click the " &
                    "'Calculate' button at the bottom right of the data input area. The selected computation " &
                    "(e.g., Precession Correction) will be done.")
        prt.println()

        prt.println("If you checked the 'Show Intermediate Calculations' checkbox, the steps " &
                    "required to find a solution will be shown in the scrollable text output area (i.e., where " &
                    "these instructions are being displayed). Regardless of whether intermediate results are shown, " &
                    "the final result will be shown just below the data input text boxes.")
        prt.println()

        prt.setBoldFont(True)
        prt.println("Notes:")
        prt.setBoldFont(False)
        prt.println("1. Angles can be entered in either decimal or DMS format (e.g., xxxd yym zz.zzs). Time-related " &
                    "(e.g., Right Ascension, Hour Angle) values can be entered in decimal or HMS format (i.e., hh:mm:ss.ss).")
        prt.println()

        prt.print("2. The formulas used in the calculations assume that angles are expressed in degrees. However, " &
                  "the trig functions provided in most computer languages and on pocket calculators assume angles " &
                  "are in radians. So, if necessary, convert degrees to radians before invoking a trig function, " &
                  "and convert radians to degrees when inverse trig functions are used.")

        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------
    ' Handle a click on the calculate button
    '----------------------------------------
    Private Sub btnCalculate_Click(sender As System.Object, e As System.EventArgs) Handles btnCalculate.Click
        Select Case calculationToDo
            Case CalculationType.NO_CALC_SELECTED
                ErrMsg("No appropriate menu item has been selected. Choose an item" & vbNewLine &
                       "from the 'Coord Systems' or 'Kepler's Equation' menu and try again.", "No Menu Item Selected")
            Case CalculationType.HA_RA
                calcHA_RA()
            Case CalculationType.RA_HA
                calcRA_HA()
            Case CalculationType.HORIZON_EQUATORIAL
                calcHorizon_Equatorial()
            Case CalculationType.EQUATORIAL_HORIZON
                calcEquatorial_Horizon()
            Case CalculationType.ECLIPTIC_EQUATORIAL
                calcEcliptic_Equatorial()
            Case CalculationType.EQUATORIAL_ECLIPTIC
                calcEquatorial_Ecliptic()
            Case CalculationType.GALACTIC_EQUATORIAL
                calcGalactic_Equatorial()
            Case CalculationType.EQUATORIAL_GALACTIC
                calcEquatorial_Galactic()
            Case CalculationType.PRECESSION_CORR
                calcPrecessionCorr()
            Case CalculationType.KEPLER_SIMPLE
                calcKeplerSimple()
            Case CalculationType.KEPLER_NEWTON
                calcKeplerNewton()
        End Select
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Routines to do the actual work for the Coord System conversions menu
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------------------------
    ' Convert Ecliptic Coordinates to Equatorial Coordinates
    '--------------------------------------------------------
    Private Sub calcEcliptic_Equatorial()
        Dim EclLatInput As String = removeWhitespace(txtboxData1.Text)
        Dim EclLonInput As String = removeWhitespace(txtboxData2.Text)
        Dim EpochInput As String = removeWhitespace(txtboxData3.Text)
        Dim Epoch As Double
        Dim result, strtmp As String
        Dim eclLatObj As ASTAngle = New ASTAngle()
        Dim eclLonObj As ASTAngle = New ASTAngle()
        Dim dEclLat, dEclLon, dE, dT, dDecl, dY, dX, dR, dQA, dRA As Double

        prt.clearTextArea()

        ' Validate the Ecliptic Latitude and Longitude
        If isValidAngle(EclLatInput, eclLatObj, HIDE_ERRORS) Then
            strtmp = angleToStr(eclLatObj, DMSFORMAT) & " Ecl Lat"
            dEclLat = eclLatObj.getDecAngle()
        Else
            ErrMsg("Invalid Ecliptic Latitude was entered - try again", "Invalid Ecl Lat")
            Return
        End If
        If isValidAngle(EclLonInput, eclLonObj, HIDE_ERRORS) Then
            strtmp = strtmp & ", " & angleToStr(eclLonObj, DMSFORMAT) & " Ecl Lon"
            dEclLon = eclLonObj.getDecAngle()
        Else
            ErrMsg("Invalid Ecliptic Longitude was entered - try again", "Invalid Ecl Lon")
            Return
        End If

        ' Validate the Epoch
        If isValidReal(EpochInput, Epoch, HIDE_ERRORS) Then
            strtmp = strtmp & ", Epoch " & Epoch
        Else
            ErrMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Convert " & strtmp & " to Equatorial Coordinates", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        dE = computeEclipticObliquity(Epoch)

        printlncond("5.  Convert Ecliptic Latitude to decimal format.")
        printlncond("    Ecl Lat = " & angleToStr(dEclLat, DECFORMAT) & " degrees")
        printlncond()

        printlncond("6.  Convert Ecliptic Longitude to decimal format.")
        printlncond("    Ecl Lon = " & angleToStr(dEclLon, DECFORMAT) & " degrees")
        printlncond()

        dT = SIN_D(dEclLat) * COS_D(dE) + COS_D(dEclLat) * SIN_D(dE) * SIN_D(dEclLon)
        printlncond("7.  Compute T = sin(Ecl Lat) * cos(E) + cos(Ecl Lat) * sin(E) * sin(Ecl Lon)")
        printlncond("    where E is the obliquity of the ecliptic for the given Epoch")
        printlncond("    T = sin(" & Format(dEclLat, decDegFormat) & ")*cos(" & Format(dE, decDegFormat) &
                    ") + cos(" & Format(dEclLat, decDegFormat) & ")*sin(" & Format(dE, decDegFormat) &
                    ")*sin(" & Format(dEclLon, decDegFormat) & ")")
        printlncond("      = " & Format(dT, genFloatFormat))
        printlncond()

        dDecl = INVSIN_D(dT)
        printlncond("8.  Decl = inv sin(T) = inv sin(" & Format(dT, genFloatFormat) & ") = " &
                    Format(dDecl, decDegFormat) & " degrees")
        printlncond()

        printlncond("9.  Y = sin(Ecl Lon) * cos(E) - tan(Ecl Lat) * sin(E)")
        printlncond("      = sin(" & Format(dEclLon, decDegFormat) & ")*cos(" & Format(dE, decDegFormat) &
                    ") - tan(" & Format(dEclLat, decDegFormat) & ")*sin(" & Format(dE, decDegFormat) & ")")
        dY = SIN_D(dEclLon) * COS_D(dE) - TAN_D(dEclLat) * SIN_D(dE)
        printlncond("      = " & Format(dY, genFloatFormat))
        printlncond()

        dX = COS_D(dEclLon)
        printlncond("10. X = cos(Ecl Lon) = cos(" & Format(dEclLon, decDegFormat) & ") = " & Format(dX, genFloatFormat))
        printlncond()

        dR = INVTAN_D(dY / dX)
        printlncond("11. R = inv tan(Y / X) = inv tan(" & Format(dY, genFloatFormat) &
                    " / " & Format(dX, genFloatFormat) & ") = " & Format(dR, decDegFormat))
        printlncond()

        dQA = quadAdjust(dY, dX)
        dRA = dR + dQA
        printlncond("12. RAdeg = R + quadrant adjustment factor")
        printlncond("    Since X = " & Format(dX, genFloatFormat) & " and Y = " & Format(dY, genFloatFormat) & ",")
        printlncond("    the quadrant adjustment factor is " & Format(dQA, decDegFormat))
        printlncond("    Thus, RAdeg = " & Format(dRA, decDegFormat) & " degrees")
        printlncond()

        printlncond("13. Divide RAdeg by 15 to convert to hours, so RA = " &
                    Format(dRA, decDegFormat) & "/15 = " & Format(dRA / 15.0, genFloatFormat) & " hours")
        dRA = dRA / 15.0
        printlncond()

        printlncond("14. Finally, convert RA to HMS format and Decl to DMS format.")
        printlncond("    RA = " & timeToStr(dRA, HMSFORMAT) & ", Decl = " & angleToStr(dDecl, DMSFORMAT))
        printlncond()

        result = angleToStr(eclLatObj, DMSFORMAT) & " Ecl Lat, " & angleToStr(eclLonObj, DMSFORMAT) & " Ecl Lon = "
        result = result & timeToStr(dRA, HMSFORMAT) & " RA, " & angleToStr(dDecl, DMSFORMAT) & " Decl"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '--------------------------------------------------------
    ' Convert Equatorial Coordinates to Ecliptic Coordinates
    '--------------------------------------------------------
    Private Sub calcEquatorial_Ecliptic()
        Dim RAInput As String = removeWhitespace(txtboxData1.Text)
        Dim DeclInput As String = removeWhitespace(txtboxData2.Text)
        Dim EpochInput As String = removeWhitespace(txtboxData3.Text)
        Dim Epoch As Double
        Dim result, strtmp As String
        Dim RAObj As ASTTime = New ASTTime()
        Dim declObj As ASTAngle = New ASTAngle()
        Dim dRA, dDecl, dEclLat, dEclLon, dE, dT, dY, dX, dQA, dR As Double

        prt.clearTextArea()

        ' Validate RA and Declination
        If isValidTime(RAInput, RAObj, HIDE_ERRORS) Then
            strtmp = "RA " & timeToStr(RAObj, HMSFORMAT)
            dRA = RAObj.getDecTime()
        Else
            ErrMsg("Invalid Right Ascension was entered - try again", "Invalid RA")
            Return
        End If
        If isValidAngle(DeclInput, declObj, HIDE_ERRORS) Then
            strtmp = strtmp & ", Decl " & angleToStr(declObj, DMSFORMAT)
            dDecl = declObj.getDecAngle()
        Else
            ErrMsg("Invalid Declination was entered - try again", "Invalid Declination")
            Return
        End If

        ' Validate the Epoch
        If isValidReal(EpochInput, Epoch, HIDE_ERRORS) Then
            strtmp = strtmp & ", Epoch " & Epoch
        Else
            ErrMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Convert " & strtmp & " to Ecliptic Coordinates", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        dE = computeEclipticObliquity(Epoch)

        printlncond("5.  Convert RA to decimal format. RA = " & timeToStr(dRA, DECFORMAT) & " hours")
        printlncond()

        printlncond("6.  Multiply RA by 15 to convert to degrees.")
        printlncond("    RAdeg = " & Format(dRA, genFloatFormat) & " * 15 = " &
                    Format(dRA * 15.0, genFloatFormat) & " degrees")
        dRA = dRA * 15.0
        printlncond()

        printlncond("7.  Convert Decl to decimal format. Decl = " & angleToStr(dDecl, DECFORMAT) & " degrees")
        printlncond()

        dT = SIN_D(dDecl) * COS_D(dE) - COS_D(dDecl) * SIN_D(dE) * SIN_D(dRA)
        printlncond("8.  Compute T = sin(Decl) * cos(E) - cos(Decl) * sin(E) * sin(RAdeg)")
        printlncond("    where E is the obliquity of the ecliptic for the given Epoch")
        printlncond("    T = sin(" & Format(dDecl, genFloatFormat) & ")*cos(" & Format(dE, decDegFormat) &
                    ") - cos(" & Format(dDecl, genFloatFormat) & ")*sin(" & Format(dE, decDegFormat) &
                    ")*sin(" & Format(dRA, genFloatFormat) & ")")
        printlncond("      = " & Format(dT, genFloatFormat))
        printlncond()

        dEclLat = INVSIN_D(dT)
        printlncond("9.  Ecl Lat = inv sin(T) = inv sin(" & Format(dT, genFloatFormat) & ") = " &
                    Format(dEclLat, decDegFormat) & " degrees")
        printlncond()

        dY = SIN_D(dRA) * COS_D(dE) + TAN_D(dDecl) * SIN_D(dE)
        printlncond("10. Y = sin(RAdeg) * cos(E) + tan(Decl) * sin(E)")
        printlncond("      = sin(" & Format(dRA, decDegFormat) & ") * cos(" &
                    Format(dE, genFloatFormat) & ") + tan(" & Format(dDecl, decDegFormat) & ") * sin(" &
                    Format(dE, genFloatFormat) & ")")
        printlncond("      = " & Format(dY, genFloatFormat))
        printlncond()

        dX = COS_D(dRA)
        printlncond("11. X = cos(RAdeg) = cos(" & Format(dRA, decDegFormat) & ") = " & Format(dX, genFloatFormat))
        printlncond()

        dR = INVTAN_D(dY / dX)
        printlncond("12. R = inv tan (Y / X) = inv tan(" & Format(dY, genFloatFormat) & " / " &
                    Format(dX, genFloatFormat) & ") = " & Format(dR, genFloatFormat) & " degrees")
        printlncond()

        dQA = quadAdjust(dY, dX)
        dEclLon = dR + dQA
        printlncond("13. Ecl Lon = R + quadrant adjustment factor")
        printlncond("    Since X = " & Format(dX, genFloatFormat) & " and Y = " & Format(dY, genFloatFormat) & ",")
        printlncond("    the quadrant adjustment factor is " & Format(dQA, genFloatFormat))
        printlncond("    Thus, Ecl Lon = " & Format(dEclLon, decDegFormat) & " degrees")
        printlncond()

        printlncond("14. Finally, convert Ecl Lat and Ecl Lon to DMS format.")
        printlncond("    Ecl Lat = " & angleToStr(dEclLat, DMSFORMAT) & ", Ecl Lon = " & angleToStr(dEclLon, DMSFORMAT))
        printlncond()

        result = timeToStr(RAObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) & " Decl = "
        result = result & angleToStr(dEclLat, DMSFORMAT) & " Ecl Lat, " & angleToStr(dEclLon, DMSFORMAT) & " Ecl Lon"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '--------------------------------------------------------
    ' Convert Equatorial Coordinates to Galactic Coordinates
    '--------------------------------------------------------
    Private Sub calcEquatorial_Galactic()
        Dim RAInput As String = removeWhitespace(txtboxData1.Text)
        Dim DeclInput As String = removeWhitespace(txtboxData2.Text)
        Dim GalEpochInput As String = removeWhitespace(txtboxData3.Text)
        Dim result As String
        Dim RAObj As ASTTime = New ASTTime()
        Dim declObj As ASTAngle = New ASTAngle()
        Dim dRA, dDecl, dGalLat, dGalLon, dT, dY, dX, dQA As Double
        Dim GalEpoch, GalRA, GalDecl, GalAscNode As Double

        prt.clearTextArea()

        ' Validate RA and Declination
        If isValidTime(RAInput, RAObj, HIDE_ERRORS) Then
            result = timeToStr(RAObj, HMSFORMAT) & " RA, "
            dRA = RAObj.getDecTime()
        Else
            ErrMsg("Invalid Right Ascension was entered - try again", "Invalid RA")
            Return
        End If
        If isValidAngle(DeclInput, declObj, HIDE_ERRORS) Then
            result = result & angleToStr(declObj, DMSFORMAT) & " Decl"
            dDecl = declObj.getDecAngle()
        Else
            ErrMsg("Invalid Declination was entered - try again", "Invalid Declination")
            Return
        End If

        ' Validate Epoch
        If Not isValidReal(GalEpochInput, GalEpoch, HIDE_ERRORS) Then
            ErrMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
            Return
        End If
        If GalEpoch = GalEpoch1950 Then         ' use Epoch 1950.0 values
            GalRA = GalRA1950
            GalDecl = GalDecl1950
            GalAscNode = GalAscNode1950
        ElseIf GalEpoch = GalEpoch2000 Then     ' use Epoch J2000 values
            GalRA = GalRA2000
            GalDecl = GalDecl2000
            GalAscNode = GalAscNode2000
        Else
            ErrMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Convert " & result & " (Epoch " & Format(GalEpoch, epochFormat) & ")", CENTERTXT)
        printlncond("to Galactic Coordinates (Epoch " & Format(GalEpoch, epochFormat) & ")", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Convert RA to decimal format. RA = " & timeToStr(dRA, DECFORMAT) & " hours")
        printlncond()

        dRA = dRA * 15.0
        printlncond("2.  Multiply RA by 15 to convert to degrees. RAdeg = " & angleToStr(dRA, DECFORMAT) & " degrees")
        printlncond()

        printlncond("3.  Convert Decl to decimal format. Decl = " & angleToStr(dDecl, DECFORMAT) & " degrees")
        printlncond()

        dGalLat = COS_D(dDecl) * COS_D(GalDecl) * COS_D(dRA - GalRA) + SIN_D(dDecl) * SIN_D(GalDecl)
        printlncond("4.  Compute T0 = cos(Decl) * cos(" & GalDecl & " * cos(RAdeg - " & GalRA &
                    ") + sin(Decl) * sin(" & GalDecl & ")")
        printlncond("               = cos(" & Format(dDecl, decDegFormat) & ")*cos(" & GalDecl &
                    ")*cos(" & Format(dRA, decDegFormat) & "-" & GalRA & ")+sin(" &
                    Format(dDecl, decDegFormat) & ")*sin(" & GalDecl & ")")
        printlncond("               = " & Format(dGalLat, genFloatFormat))
        printlncond()

        printcond("5.  Gal Lat = inv sin(T0) = inv sin(" & Format(dGalLat, genFloatFormat) & ") = ")
        dGalLat = INVSIN_D(dGalLat)
        printlncond(Format(dGalLat, decDegFormat) & " degrees")
        printlncond()

        dY = SIN_D(dDecl) - SIN_D(dGalLat) * SIN_D(GalDecl)
        printlncond("6.  Y = sin(Decl) - sin(Gal Lat) * sin(" & GalDecl & ") = sin(" &
                    Format(dDecl, decDegFormat) & ") - sin(" & Format(dGalLat, decDegFormat) & ")*sin(" & GalDecl & ")")
        printlncond("      = " & Format(dY, genFloatFormat))
        printlncond()

        dX = COS_D(dDecl) * SIN_D(dRA - GalRA) * COS_D(GalDecl)
        printlncond("7.  X = cos(Decl) * sin(RAdeg - " & GalRA & ") * cos(27.4)")
        printlncond("      = cos(" & Format(dDecl, decDegFormat) & ")*sin(" & Format(dRA, decDegFormat) &
                    " - " & GalRA & ")*cos(" & GalDecl & ")")
        printlncond("      = " & Format(dX, genFloatFormat))
        printlncond()

        dT = INVTAN_D(dY / dX)
        printlncond("8.  T1 = inv tan(Y / X) = inv tan(" & Format(dY, genFloatFormat) & " / " &
                    Format(dX, genFloatFormat) & ") = " & Format(dT, genFloatFormat) & " degrees")
        printlncond()

        dQA = quadAdjust(dY, dX)
        dGalLon = dT + dQA + GalAscNode
        printlncond("9.  Gal Lon = T1 + quadrant adjustment + " & GalAscNode)
        printlncond("    Since X = " & Format(dX, genFloatFormat) & " and Y = " & Format(dY, genFloatFormat) & ",")
        printlncond("    the quadrant adjustment factor is " & Format(dQA, genFloatFormat))
        printlncond("    Thus, Gal Lon = " & Format(dGalLon, decDegFormat) & " degrees")
        printlncond()

        If (dGalLon > 360.0) Then dGalLon = dGalLon - 360.0
        printlncond("10. If Gal Lon is greater than 360, subtract 360. Gal Lon = " & Format(dGalLon, decDegFormat) & " degrees")
        printlncond()

        printlncond("11. Finally, convert Gal Lat and Gal Lon to DMS format.")
        printlncond("    Gal Lat = " & angleToStr(dGalLat, DMSFORMAT) & ", Gal Lon = " & angleToStr(dGalLon, DMSFORMAT))
        printlncond()

        result = result & " = " & angleToStr(dGalLat, DMSFORMAT) & " Gal Lat, " & angleToStr(dGalLon, DMSFORMAT) &
            " Gal Lon (" & Format(GalEpoch, epochFormat) & ")"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------
    ' Convert Equatorial Coordinates to Horizon Coordinates
    '-------------------------------------------------------
    Private Sub calcEquatorial_Horizon()
        Dim HAInput As String = removeWhitespace(txtboxData1.Text)
        Dim DeclInput As String = removeWhitespace(txtboxData2.Text)
        Dim latInput As String = removeWhitespace(txtboxData3.Text)
        Dim result, strtmp As String
        Dim HAObj As ASTTime = New ASTTime()
        Dim declObj As ASTAngle = New ASTAngle()
        Dim dLatObj As ASTLatLon = New ASTLatLon()
        Dim dLat, dAlt, dAz, dDecl, dHA, dT, dT1, dT2 As Double

        prt.clearTextArea()

        ' Validate HA and Declination
        If isValidTime(HAInput, HAObj, HIDE_ERRORS) Then
            result = "HA " & timeToStr(HAObj, HMSFORMAT)
            dHA = HAObj.getDecTime()
        Else
            ErrMsg("Invalid Hour Angle was entered - try again", "Invalid HA")
            Return
        End If
        If isValidAngle(DeclInput, declObj, HIDE_ERRORS) Then
            result = result & ", Declination " & angleToStr(declObj, DMSFORMAT)
            dDecl = declObj.getDecAngle()
        Else
            ErrMsg("Invalid Declination was entered - try again", "Invalid Declination")
            Return
        End If

        ' Validate latitude
        If Not isValidLat(latInput, dLatObj) Then Return
        dLat = dLatObj.getLat()
        strtmp = "for " & latToStr(dLatObj, DECFORMAT) & " latitude"

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to Horizon Coordinates", CENTERTXT)
        printlncond(strtmp, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Convert Hour Angle to decimal format. HA = " & timeToStr(HAObj, DECFORMAT) & " hours")
        printlncond()

        dHA = dHA * 15.0
        printlncond("2.  Multiply step 1 by 15 to convert to degrees. HAdeg = " & angleToStr(dHA, DECFORMAT) & " degrees")
        printlncond()

        printlncond("3.  Convert Declination to decimal format. Decl = " & angleToStr(dDecl, DECFORMAT) & " degrees")
        printlncond()

        printlncond("4.  Compute T0 = sin(Decl) * sin(Lat) + cos(Decl) * cos(Lat) * cos(HAdeg)")
        printlncond("               = sin(" & Format(dDecl, genFloatFormat) & ")*sin(" & Format(dLat, genFloatFormat) &
                    ") + cos(" & Format(dDecl, genFloatFormat) & ")*cos(" & Format(dLat, genFloatFormat) & ")*cos(" &
                    Format(dHA, genFloatFormat) & ")")
        dT = SIN_D(dDecl) * SIN_D(dLat) + COS_D(dDecl) * COS_D(dLat) * COS_D(dHA)
        printlncond("               = " & Format(dT, genFloatFormat))
        printlncond()

        dAlt = INVSIN_D(dT)
        printlncond("5.  Alt = inv sin(T0) = inv sin(" & Format(dT, genFloatFormat) & ") = " &
                    Format(dAlt, genFloatFormat) & " degrees")
        printlncond()

        dT1 = SIN_D(dDecl) - SIN_D(dLat) * SIN_D(dAlt)
        printlncond("6.  T1 = sin(Decl) - sin(Lat) * sin(Alt) = sin(" & Format(dDecl, genFloatFormat) & ") - sin(" &
                    Format(dLat, genFloatFormat) & ")*sin(" & Format(dAlt, genFloatFormat) & ")")
        printlncond("       = " & Format(dT1, genFloatFormat))
        printlncond()

        dT2 = COS_D(dLat) * COS_D(dAlt)
        printlncond("7.  T2 = T1 / [cos(Lat) * cos(Alt)] = " & Format(dT1, genFloatFormat) & " / [cos(" &
                    Format(dLat, genFloatFormat) & ") * cos(" & Format(dAlt, genFloatFormat) & ")]")
        dT2 = dT1 / dT2
        printlncond("       = " & Format(dT2, genFloatFormat))
        printlncond()

        dAz = INVCOS_D(dT2)
        printlncond("8.  Az = inv cos(T2) = inv cos(" & Format(dT2, genFloatFormat) & ") = " &
                    Format(dAz, genFloatFormat) & " degrees")
        printlncond()

        dT1 = SIN_D(dHA)
        printlncond("9.  Compute sin(HAdeg) = sin(" & Format(dHA, genFloatFormat) & ") = " & Format(dT1, genFloatFormat))
        printlncond()

        printlncond("10. If the result of step 9 is positive, then Az = 360 - Az.")
        If (dT1 > 0) Then dAz = 360.0 - dAz
        printlncond("    Az = " & Format(dAz, genFloatFormat) & " degrees")
        printlncond()

        printlncond("11. Finally, convert Altitude and Azimuth to DMS format. Thus,")
        printlncond("    Alt = " & angleToStr(dAlt, DMSFORMAT) & ", Az = " & angleToStr(dAz, DMSFORMAT))
        printlncond()

        result = timeToStr(HAObj, DMSFORMAT) & " HA, " & angleToStr(declObj, DMSFORMAT) & " Decl = "
        result = result & angleToStr(dAlt, DMSFORMAT) & " Alt, " & angleToStr(dAz, DMSFORMAT) & " Az"
        printlncond("Thus, " & result)
        printlncond(strtmp)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '--------------------------------------------------------
    ' Convert Galactic Coordinates to Equatorial Coordinates
    '--------------------------------------------------------
    Private Sub calcGalactic_Equatorial()
        Dim GalLatInput As String = removeWhitespace(txtboxData1.Text)
        Dim GalLonInput As String = removeWhitespace(txtboxData2.Text)
        Dim GalEpochInput As String = removeWhitespace(txtboxData3.Text)
        Dim result As String
        Dim latObj As ASTAngle = New ASTAngle()
        Dim lonObj As ASTAngle = New ASTAngle()
        Dim dLat, dLon, dDecl, dY, dX, dR, dQA, dRA As Double
        Dim GalEpoch, GalRA, GalDecl, GalAscNode As Double

        prt.clearTextArea()

        ' Validate Galactic lat/lon
        If Not isValidAngle(GalLatInput, latObj) Then Return
        dLat = latObj.getDecAngle()
        If Not isValidAngle(GalLonInput, lonObj) Then Return
        dLon = lonObj.getDecAngle()

        ' Validate Epoch
        If Not isValidReal(GalEpochInput, GalEpoch, HIDE_ERRORS) Then
            ErrMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
            Return
        End If
        If GalEpoch = GalEpoch1950 Then         ' use Epoch 1950.0 values
            GalRA = GalRA1950
            GalDecl = GalDecl1950
            GalAscNode = GalAscNode1950
        ElseIf GalEpoch = GalEpoch2000 Then     ' use Epoch J2000 values
            GalRA = GalRA2000
            GalDecl = GalDecl2000
            GalAscNode = GalAscNode2000
        Else
            ErrMsg("Invalid Epoch was entered - try again", "Invalid Epoch")
            Return
        End If

        result = angleToStr(dLat, DMSFORMAT) & " Gal Lat, " & angleToStr(dLon, DMSFORMAT) & " Gal Lon"

        prt.setBoldFont(True)
        printlncond("Convert " & result & " (Epoch " & Format(GalEpoch, epochFormat) & ")", CENTERTXT)
        printlncond("to Equatorial Coordinates " & "(Epoch " & Format(GalEpoch, epochFormat) & ")", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Convert Galactic Lat to decimal format. Gal Lat = " & angleToStr(dLat, DECFORMAT) & " degrees")
        printlncond()

        printlncond("2.  Convert Galactic Lon to decimal format. Gal Lon = " & angleToStr(dLon, DECFORMAT) & " degrees")
        printlncond()

        dDecl = COS_D(dLat) * COS_D(GalDecl) * SIN_D(dLon - GalAscNode) + SIN_D(dLat) * SIN_D(GalDecl)
        printlncond("3.  Compute T = cos(Gal Lat) * cos(" & GalDecl & ") * sin(Gal Lon - " & GalAscNode & ") + " &
                    "sin(Gal Lat) * sin(" & GalDecl & ")")
        printlncond("              = cos(" & Format(dLat, decDegFormat) & ")*cos(" & GalDecl & ")*sin(" &
                    Format(dLon, decDegFormat) & " - " & GalAscNode & ") + sin(" & Format(dLat, decDegFormat) &
                    ") * sin(" & GalDecl & ")")
        printlncond("              = " & Format(dDecl, genFloatFormat))
        printlncond()

        printcond("4.  Decl = inv sin(T) = inv sin(" & Format(dDecl, genFloatFormat) & ") = ")
        dDecl = INVSIN_D(dDecl)
        printlncond(Format(dDecl, decDegFormat) & " degrees")
        printlncond()

        dY = COS_D(dLat) * COS_D(dLon - GalAscNode)
        printlncond("5.  Y = cos(Gal Lat) * cos(Gal Lon - " & GalAscNode & ") = cos(" &
                    Format(dLat, decDegFormat) & ")*cos(" & Format(dLon, decDegFormat) & " - " & GalAscNode & ")")
        printlncond("      = " & Format(dY, genFloatFormat))
        printlncond()

        dX = SIN_D(dLat) * COS_D(GalDecl) - COS_D(dLat) * SIN_D(GalDecl) * SIN_D(dLon - GalAscNode)
        printlncond("6.  X = sin(Gal Lat) * cos(" & GalDecl & ") - cos(Gal Lat) * sin(" & GalDecl & ") * " &
                    "sin(Gal Lon - " & GalAscNode & ")")
        printlncond("      = sin(" & Format(dLat, decDegFormat) & ")*cos(" & GalDecl & ") - cos(" &
                    Format(dLat, decDegFormat) & ") * sin(" & GalDecl & ")*sin(" &
                    Format(dLon, decDegFormat) & " - " & GalAscNode & ")")
        printlncond("      = " & Format(dX, genFloatFormat))
        printlncond()

        dR = INVTAN_D(dY / dX)
        printlncond("7.  R = inv tan (Y / X) = inv tan(" & Format(dY, genFloatFormat) & " / " &
                    Format(dX, genFloatFormat) & ") = " & Format(dR, genFloatFormat) & " degrees")
        printlncond()

        dQA = quadAdjust(dY, dX)
        dRA = dR + dQA + GalRA
        printlncond("8.  RAdeg = R + quadrant adjustment factor + " & GalRA)
        printlncond("    Since X = " & Format(dX, genFloatFormat) & " and Y = " & Format(dY, genFloatFormat) & ",")
        printlncond("    the quadrant adjustment factor is " & Format(dQA, genFloatFormat))
        printlncond("    Thus, RAdeg = " & Format(dRA, decDegFormat) & " degrees")
        printlncond()

        If (dRA > 360) Then dRA = dRA - 360.0
        printlncond("9.  If RAdeg is > 360, subtract 360. RAdeg = " & Format(dRA, decDegFormat) & " degrees")
        printlncond()

        dRA = dRA / 15.0
        printlncond("10. Divide RAdeg by 15 to convert it to hours, giving RA = " & Format(dRA, decHoursFormat) & " hours")
        printlncond()

        printlncond("11. Finally, convert Right Ascension to HMS format and Declination to DMS format.")
        printlncond("    RA = " & timeToStr(dRA, HMSFORMAT) & ", Decl = " & angleToStr(dDecl, DMSFORMAT))
        printlncond()

        result = result & " = " & timeToStr(dRA, HMSFORMAT) & " RA, " & angleToStr(dDecl, DMSFORMAT) & " Decl (" &
            Format(GalEpoch, epochFormat) & ")"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------
    ' Convert Hour Angle to Right Ascension
    '---------------------------------------
    Private Sub calcHA_RA()
        Dim HAInput As String = removeWhitespace(txtboxData1.Text)
        Dim UTInput As String = removeWhitespace(txtboxData2.Text)
        Dim lonInput As String = removeWhitespace(txtboxData3.Text)
        Dim dateInput As String = removeWhitespace(txtboxData4.Text)
        Dim result, strtmp As String
        Dim HAtimeObj As ASTTime = New ASTTime()
        Dim UTtimeObj As ASTTime = New ASTTime()
        Dim dLongObj As ASTLatLon = New ASTLatLon()
        Dim dateObj As ASTDate = New ASTDate()
        Dim LST, dLongitude, RA As Double

        prt.clearTextArea()

        ' Validate Hour Angle and UT
        If isValidTime(HAInput, HAtimeObj, HIDE_ERRORS) Then
            result = timeToStr(HAtimeObj, HMSFORMAT)
        Else
            ErrMsg("Invalid Hour Angle was entered - try again", "Invalid Hour Angle")
            Return
        End If

        If isValidTime(UTInput, UTtimeObj, HIDE_ERRORS) Then
            strtmp = "at " & timeToStr(UTtimeObj, HMSFORMAT) & " UT"
        Else
            ErrMsg("Invalid UT was entered - try again", "Invalid UT")
            Return
        End If

        ' Validate longitude and date
        If Not isValidLon(lonInput, dLongObj) Then Return
        dLongitude = dLongObj.getLon()
        strtmp = strtmp & " for " & lonToStr(dLongObj, DECFORMAT) & " longitude"

        If Not isValidDate(dateInput, dateObj) Then Return
        strtmp = strtmp & " on " & dateToStr(dateObj)

        prt.setBoldFont(True)
        printlncond("Convert Hour Angle " & result & " to Right Ascension", CENTERTXT)
        printlncond(strtmp, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        LST = GSTtoLST(UTtoGST(UTtimeObj.getDecTime(), dateObj), dLongitude)
        printlncond("1. Convert UT to LST. Thus, for date " & dateToStr(dateObj) & " at longitude " & lonToStr(dLongObj, DECFORMAT))
        printlncond("   " & timeToStr(UTtimeObj, HMSFORMAT) & " UT = " & timeToStr(LST, HMSFORMAT) &
                    " = " & timeToStr(LST, DECFORMAT) & " LST")
        printlncond()

        printlncond("2. Convert Hour Angle to decimal format. Thus, HA = " &
                    timeToStr(HAtimeObj, DECFORMAT) & " hours")
        printlncond()

        RA = LST - HAtimeObj.getDecTime()
        printlncond("3. RA = LST - Hour Angle = " & timeToStr(LST, DECFORMAT) & " - " & timeToStr(HAtimeObj, DECFORMAT) &
                    " = " & timeToStr(RA, DECFORMAT) & " hours")
        printlncond()

        If RA < 0.0 Then RA = RA + 24.0
        printlncond("4. If RA is negative add 24. Thus, RA = " & timeToStr(RA, DECFORMAT) & " hours")
        printlncond()

        printlncond("5. Finally, convert RA to hours, minutes, seconds format, giving RA = " & timeToStr(RA, HMSFORMAT))
        printlncond()

        result = timeToStr(HAtimeObj, HMSFORMAT) & " HA = " & timeToStr(RA, HMSFORMAT) & " RA"
        printlncond("Thus, " & result & " at " & timeToStr(UTtimeObj, HMSFORMAT) & " UT")
        printlncond(strtmp)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------
    ' Convert Horizon Coordinates to Equatorial Coordinates
    '-------------------------------------------------------
    Private Sub calcHorizon_Equatorial()
        Dim altInput As String = removeWhitespace(txtboxData1.Text)
        Dim azInput As String = removeWhitespace(txtboxData2.Text)
        Dim latInput As String = removeWhitespace(txtboxData3.Text)
        Dim result, strtmp As String
        Dim altObj As ASTAngle = New ASTAngle()
        Dim azObj As ASTAngle = New ASTAngle()
        Dim dLatObj As ASTLatLon = New ASTLatLon()
        Dim dLat, dAlt, dAz, dDecl, dHA, dTmp As Double

        prt.clearTextArea()

        ' Validate Altitude and Azimuth
        If isValidAngle(altInput, altObj, HIDE_ERRORS) Then
            result = "Altitude " & angleToStr(altObj, DMSFORMAT)
            dAlt = altObj.getDecAngle()
        Else
            ErrMsg("Invalid Altitude was entered - try again", "Invalid Altitude")
            Return
        End If
        If isValidAngle(azInput, azObj, HIDE_ERRORS) Then
            result = result & ", Azimuth " & angleToStr(azObj, DMSFORMAT)
            dAz = azObj.getDecAngle()
        Else
            ErrMsg("Invalid Azimuth was entered - try again", "Invalid Azimuth")
            Return
        End If

        ' Validate latitude
        If Not isValidLat(latInput, dLatObj) Then Return
        dLat = dLatObj.getLat()
        strtmp = "for " & latToStr(dLatObj, DECFORMAT) & " latitude"

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to Equatorial Coordinates", CENTERTXT)
        printlncond(strtmp, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Convert Altitude to decimal format. Alt = " &
                    angleToStr(altObj, DECFORMAT) & " degrees")
        printlncond()

        printlncond("2.  Convert Azimuth to decimal format. Az = " &
                    angleToStr(azObj, DECFORMAT) & " degrees")
        printlncond()

        printlncond("3.  Compute T0 = sin(Alt) * sin(Lat) + cos(Alt) * cos(Lat) * cos(Az)")
        printlncond("               = sin(" & Format(dAlt, genFloatFormat) & ") * sin(" & Format(dLat, genFloatFormat) &
                    ") + cos(" & Format(dAlt, genFloatFormat) & ") * cos(" & Format(dLat, genFloatFormat) &
                    ") * cos(" & Format(dAz, genFloatFormat) & ")")
        dTmp = SIN_D(dAlt) * SIN_D(dLat) + COS_D(dAlt) * COS_D(dLat) * COS_D(dAz)
        printlncond("               = " & Format(dTmp, genFloatFormat))
        printlncond()

        dDecl = INVSIN_D(dTmp)
        printlncond("4.  Decl = inverse sin(T0) = inv sin(" & Format(dTmp, genFloatFormat) & ") = " &
                    Format(dDecl, genFloatFormat) & " degrees")
        printlncond()

        printlncond("5.  T1 = sin(Alt) - sin(Lat) * sin(Decl)")
        printlncond("       = sin(" & Format(dAlt, genFloatFormat) & ") - sin(" &
                    Format(dLat, genFloatFormat) & ") * sin(" & Format(dDecl, genFloatFormat) & ")")
        dTmp = SIN_D(dAlt) - SIN_D(dLat) * SIN_D(dDecl)
        printlncond("       = " & Format(dTmp, genFloatFormat))
        printlncond()

        dHA = COS_D(dLat) * COS_D(dDecl)
        printlncond("6.  cos(H) = T1 / [cos(Lat) * cos(Decl)]")
        printlncond("           = " & Format(dTmp, genFloatFormat) & " / [cos(" &
                    Format(dLat, genFloatFormat) & ") * cos(" & Format(dDecl, genFloatFormat) & ")]")
        dHA = dTmp / dHA
        printlncond("           = " & Format(dHA, genFloatFormat))
        printlncond()

        printlncond("7.  H = inv cos(H) = inv cos( " & Format(dHA, genFloatFormat) & ") = " &
                    Format(INVCOS_D(dHA), genFloatFormat) & " degrees")
        dHA = INVCOS_D(dHA)
        printlncond()

        dTmp = SIN_D(dAz)
        printlncond("8.  Compute sin(Az) = sin(" & Format(dAz, genFloatFormat) & ") = " & Format(dTmp, genFloatFormat))
        printlncond()

        printlncond("9.  If sin(Az) is positive, then subtract the result of step 7 from 360.")
        If (dTmp > 0) Then dHA = 360 - dHA
        printlncond("    H = " & Format(dHA, genFloatFormat) & " degrees")
        printlncond()

        printcond("10. Hour Angle = HA = H / 15 = " & Format(dHA, genFloatFormat) & " / 15 = ")
        dHA = dHA / 15.0
        printlncond(Format(dHA, genFloatFormat) & " hours")
        printlncond()

        printlncond("11. Finally, convert Declination to DMS format and HA to HMS format.")
        printlncond("    HA = " & timeToStr(dHA, HMSFORMAT) & ", Decl = " & angleToStr(dDecl, DMSFORMAT))
        printlncond()

        result = angleToStr(altObj, DMSFORMAT) & " Alt, " & angleToStr(azObj, DMSFORMAT) & " Az = "
        result = result & timeToStr(dHA, HMSFORMAT) & " HA, " & angleToStr(dDecl, DMSFORMAT) & " Decl"
        printlncond("Thus, " & result)
        printlncond(strtmp)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------
    ' Compute a precession correction
    '---------------------------------
    Private Sub calcPrecessionCorr()
        Dim RAInput As String = removeWhitespace(txtboxData1.Text)
        Dim DeclInput As String = removeWhitespace(txtboxData2.Text)
        Dim strEpochFrom As String = removeWhitespace(txtboxData3.Text)
        Dim strEpochTo As String = removeWhitespace(txtboxData4.Text)
        Dim result As String
        Dim RAObj As ASTTime = New ASTTime()
        Dim declObj As ASTAngle = New ASTAngle()
        Dim dRA, dRAdeg, dDecl, deltaRA, deltaDecl, dT, dM, dNd, dNt As Double
        Dim EpochTo, EpochFrom, dDiff As Double

        prt.clearTextArea()

        ' Validate RA and Declination
        If isValidTime(RAInput, RAObj, HIDE_ERRORS) Then
            result = timeToStr(RAObj, HMSFORMAT) & " RA, "
            dRA = RAObj.getDecTime()
        Else
            ErrMsg("Invalid Right Ascension was entered - try again", "Invalid RA")
            Return
        End If
        If isValidAngle(DeclInput, declObj, HIDE_ERRORS) Then
            result = result & angleToStr(declObj, DMSFORMAT) & " Decl"
            dDecl = declObj.getDecAngle()
        Else
            ErrMsg("Invalid Declination was entered - try again", "Invalid Declination")
            Return
        End If

        ' Validate the to and from Epochs
        If isValidReal(strEpochFrom, EpochFrom, HIDE_ERRORS) Then
            result = result & " in Epoch " & EpochFrom
        Else
            ErrMsg("Invalid 'From' Epoch was entered - try again", "Invalid 'From' Epoch")
            Return
        End If
        If Not isValidReal(strEpochTo, EpochTo, HIDE_ERRORS) Then
            ErrMsg("Invalid 'To' Epoch was entered - try again", "Invalid 'To' Epoch")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to Epoch " & EpochTo, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Convert RA to decimal format. RA = " & timeToStr(dRA, DECFORMAT) & " hours")
        printlncond()

        dRAdeg = dRA * 15.0
        printlncond("2.  Multiply RA by 15 to convert to degrees. RAdeg = " & angleToStr(dRAdeg, DECFORMAT) & " degrees")
        printlncond()

        printlncond("3.  Convert Decl to decimal format. Decl = " & angleToStr(dDecl, DECFORMAT) & " degrees")
        printlncond()

        dT = (EpochTo - 1900.0) / 100.0
        printlncond("4.  T = (Epoch to convert to - 1900) / 100 = (" & EpochTo & " - 1900) / 100 = " & Format(dT, genFloatFormat))
        printlncond()

        dM = 3.07234 + 0.00186 * dT
        printlncond("5.  M = 3.07234 + 0.00186 * T = 3.07234 + 0.00186 * " & Format(dT, genFloatFormat) & " = " &
                    Format(dM, genFloatFormat) & " arcseconds")
        printlncond()

        dNd = 20.0468 - 0.0085 * dT
        printlncond("6.  Nd = 20.0468 - 0.0085 * T = 20.0468 - 0.0085 * " & Format(dT, genFloatFormat) & " = " &
                    Format(dNd, genFloatFormat) & " arcseconds")
        printlncond()

        dNt = dNd / 15.0
        printlncond("7.  Nt = Nd / 15 = " & Format(dNd, genFloatFormat) & " / 15 = " & Format(dNt, genFloatFormat) &
                    " seconds (of time)")
        printlncond()

        dDiff = (EpochTo - EpochFrom)
        printlncond("8.  Diff = (Epoch to convert to - Epoch to convert from) = (" & EpochTo & " - " &
                    EpochFrom & ") = " & dDiff)
        printlncond()

        deltaRA = (dM + dNt * SIN_D(dRAdeg) * TAN_D(dDecl)) * dDiff
        printlncond("9.  D.ra = [M + Nt * sin(RAdeg) * tan(Decl)] * Diff")
        printlncond("         = [" & Format(dM, genFloatFormat) & " + " & Format(dNt, genFloatFormat) &
                    " * sin(" & Format(dRAdeg, decDegFormat) & ") * tan(" & Format(dDecl, decDegFormat) & "]) * " & dDiff)
        printlncond("         = " & Format(deltaRA, genFloatFormat) & " seconds (of time)")
        printlncond()

        deltaDecl = dNd * COS_D(dRAdeg) * dDiff
        printlncond("10. D.decl = Nd * cos(RAdeg) * Diff = " & Format(dNd, genFloatFormat) & " * cos(" &
                    Format(dRAdeg, decDegFormat) & ") * " & dDiff)
        printlncond("           = " & Format(deltaDecl, genFloatFormat) & " arcseconds")
        printlncond()

        deltaRA = deltaRA / 3600.0
        deltaDecl = deltaDecl / 3600.0
        printlncond("11. Divide D.ra by 3600 to convert to hours and divide D.decl by 3600 to convert to degrees.")
        printlncond("    D.ra = " & Format(deltaRA, genFloatFormat) & " hours and D.decl = " &
                    Format(deltaDecl, genFloatFormat) & " degrees")
        printlncond()

        dRA = dRA + deltaRA
        dDecl = dDecl + deltaDecl
        printlncond("12. Add D.ra to RA and D.decl to Decl, giving an")
        printlncond("    adjusted RA = " & Format(dRA, decHoursFormat) & " hours and Decl = " &
                    Format(dDecl, decDegFormat) & " degrees")
        printlncond()

        printlncond("13. Finally, convert RA to HMS format and Decl to DMS format. Thus, in Epoch " & EpochTo & ",")
        printlncond("    RA = " & timeToStr(dRA, HMSFORMAT) & " hours, Decl = " & angleToStr(dDecl, DMSFORMAT) & " degrees")
        printlncond()

        result = timeToStr(RAObj, HMSFORMAT) & " RA, " & angleToStr(declObj, DMSFORMAT) & " Decl (" & EpochFrom & ") = "
        result = result & timeToStr(dRA, HMSFORMAT) & " RA, " & angleToStr(dDecl, DMSFORMAT) & " Decl (" & EpochTo & ")"
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '---------------------------------------
    ' Convert Right Ascension to Hour Angle
    '---------------------------------------
    Private Sub calcRA_HA()
        Dim RAInput As String = removeWhitespace(txtboxData1.Text)
        Dim UTInput As String = removeWhitespace(txtboxData2.Text)
        Dim lonInput As String = removeWhitespace(txtboxData3.Text)
        Dim dateInput As String = removeWhitespace(txtboxData4.Text)
        Dim result, strtmp As String
        Dim RAtimeObj As ASTTime = New ASTTime()
        Dim UTtimeObj As ASTTime = New ASTTime()
        Dim dLonObj As ASTLatLon = New ASTLatLon()
        Dim dateObj As ASTDate = New ASTDate()
        Dim LST, dLongitude, HA As Double

        prt.clearTextArea()

        ' Validate Right Ascension and UT
        If isValidTime(RAInput, RAtimeObj, HIDE_ERRORS) Then
            result = timeToStr(RAtimeObj, HMSFORMAT)
        Else
            ErrMsg("Invalid Right Ascension was entered - try again", "Invalid Right Ascension")
            Return
        End If

        If isValidTime(UTInput, UTtimeObj, HIDE_ERRORS) Then
            strtmp = "at " & timeToStr(UTtimeObj, HMSFORMAT) & " UT"
        Else
            ErrMsg("Invalid UT was entered - try again", "Invalid UT")
            Return
        End If

        ' Validate longitude and date
        If Not isValidLon(lonInput, dLonObj) Then Return
        dLongitude = dLonObj.getLon()
        strtmp = strtmp & " for " & lonToStr(dLonObj, DECFORMAT) & " longitude"

        If Not isValidDate(dateInput, dateObj) Then Return
        strtmp = strtmp & " on " & dateToStr(dateObj)

        prt.setBoldFont(True)
        printlncond("Convert Right Ascension " & result & " to Hour Angle (HA)", CENTERTXT)
        printlncond(strtmp, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        LST = GSTtoLST(UTtoGST(UTtimeObj.getDecTime(), dateObj), dLongitude)
        printlncond("1. Convert UT to LST. Thus, for date " & dateToStr(dateObj) & " at longitude " & lonToStr(dLonObj, DECFORMAT))
        printlncond("   " & timeToStr(UTtimeObj, HMSFORMAT) & " UT = " & timeToStr(LST, HMSFORMAT) &
                    " = " & timeToStr(LST, DECFORMAT) & " LST")
        printlncond()

        printlncond("2. Convert Right Ascension to decimal format. Thus, RA = " &
                    timeToStr(RAtimeObj, DECFORMAT) & " hours")
        printlncond()

        HA = LST - RAtimeObj.getDecTime()
        printlncond("3. HA = LST - RA = " & timeToStr(LST, DECFORMAT) & " - " & timeToStr(RAtimeObj, DECFORMAT) &
                    " = " & timeToStr(HA, DECFORMAT) & " hours")
        printlncond()

        If (HA < 0.0) Then HA = HA + 24.0
        printlncond("4. If HA is negative add 24. Thus, HA = " & timeToStr(HA, DECFORMAT) & " hours")
        printlncond()

        printlncond("5. Finally, convert HA to hours, minutes, seconds format, giving HA = " & timeToStr(HA, HMSFORMAT))
        printlncond()

        result = timeToStr(RAtimeObj, HMSFORMAT) & " RA = " & timeToStr(HA, HMSFORMAT) & " HA"
        printlncond("Thus, " & result)
        printlncond(strtmp)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Routines to do the actual work for the Kepler's Equation menu
    '-------------------------------------------------------------------------------------------------------

    '---------------------------------------
    ' Solve Kepler's equation via a simple
    ' iteration method
    '---------------------------------------
    Private Sub calcKeplerSimple()
        Dim MAInput As String = removeWhitespace(txtboxData1.Text)
        Dim TermInput As String = removeWhitespace(txtboxData2.Text)
        Dim EccInput As String = removeWhitespace(txtboxData3.Text)
        Dim tmpAngle As ASTAngle = New ASTAngle()
        Dim dTerm, dMA, dMARad, dEA, dEANext, dEcc, delta As Double
        Dim iterCnt As Integer = 0
        Dim result As String

        prt.clearTextArea()

        ' Validate the mean anomaly
        If isValidAngle(MAInput, tmpAngle, HIDE_ERRORS) Then
            dMA = tmpAngle.getDecAngle()
        Else
            ErrMsg("Invalid Mean Anomaly was entered - try again", "Invalid Mean Anomaly")
            Return
        End If

        ' Validate the termination criteria
        If Not isValidReal(TermInput, dTerm, HIDE_ERRORS) Then
            ErrMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
            Return
        End If
        If dTerm < KeplerMinCriteria Then dTerm = KeplerMinCriteria

        ' Validate the orbital eccentricity
        If Not isValidReal(EccInput, dEcc, HIDE_ERRORS) Then
            ErrMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
            Return
        End If
        If dEcc < 0 Then
            ErrMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Solve Kepler's Equation for Mean Anomaly " & angleToStr(dMA, DMSFORMAT) &
                    " and Orbital Eccentricity = " & dEcc, CENTERTXT)
        printlncond("Iterate until difference is less than " & dTerm & " radians (" &
                    Format(rad2deg(dTerm) * 3600.0, genFloatFormat) & " arcseconds)", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        dMARad = deg2rad(dMA)
        printlncond("1. Convert mean anomaly to radians.")
        printlncond("   MA_rad = " & Format(dMARad, genFloatFormat) & " radians")
        printlncond()

        dEA = dMARad
        printlncond("2. Set initial estimate E0 = MA_rad = " & Format(dEA, genFloatFormat) & " radians")
        printlncond()

        printlncond("3. Using the iteration scheme E_i = MA_rad + e*sin(E_(i-1)),")
        printlncond("   iterate until the difference between successive estimates is")
        printlncond("   less than the termination criteria (" & dTerm & " radians)")
        printlncond()

        For iterCnt = 1 To KeplerMaxIterations
            ' Note that the sine function here uses radians, not degrees!
            dEANext = dMARad + dEcc * Sin(dEA)
            delta = Abs(dEANext - dEA)
            printlncond("   E" & iterCnt & " = " & Format(dEANext, genFloatFormat) & ", Delta = " &
                        Format(delta, genFloatFormat))
            dEA = dEANext
            If delta <= dTerm Then Exit For
        Next
        If iterCnt >= KeplerMaxIterations Then
            printlncond("WARNING: Did not converge, so stopped after " & KeplerMaxIterations & " iterations.")
        End If
        printlncond()

        printlncond("4. The eccentric anomaly (in radians) is the last result. So,")
        printlncond("   Eccentric Anomaly = E" & iterCnt & " = " & Format(dEA, genFloatFormat) & " radians")
        printlncond()

        dEA = rad2deg(dEA)
        printlncond("5. Convert the eccentric anomaly from radians to degrees.")
        printlncond("   Eccentric Anomaly = " & angleToStr(dEA, DECFORMAT))
        printlncond()

        printlncond("6. Convert the Eccentric Anomaly to DMS format.")
        printlncond("   Eccentric Anomaly = " & angleToStr(dEA, DMSFORMAT))
        printlncond()

        result = "After " & iterCnt & " iterations (Simple Iteration), Eccentric Anomaly = " & angleToStr(dEA, DMSFORMAT)
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------------
    ' Solve Kepler's equation by the Newton/Raphson method
    '------------------------------------------------------
    Private Sub calcKeplerNewton()
        Dim MAInput As String = removeWhitespace(txtboxData1.Text)
        Dim TermInput As String = removeWhitespace(txtboxData2.Text)
        Dim EccInput As String = removeWhitespace(txtboxData3.Text)
        Dim tmpAngle As ASTAngle = New ASTAngle()
        Dim dTerm, dMA, dMARad, dEA, dEANext, dEcc, delta As Double
        Dim iterCnt As Integer = 0
        Dim result As String

        prt.clearTextArea()

        ' Validate the mean anomaly
        If isValidAngle(MAInput, tmpAngle, HIDE_ERRORS) Then
            dMA = tmpAngle.getDecAngle()
        Else
            ErrMsg("Invalid Mean Anomaly was entered - try again", "Invalid Mean Anomaly")
            Return
        End If

        ' Validate the termination criteria
        If Not isValidReal(TermInput, dTerm, HIDE_ERRORS) Then
            ErrMsg("Invalid Termination Criteria was entered - try again", "Invalid Termination Criteria")
            Return
        End If
        If dTerm < KeplerMinCriteria Then dTerm = KeplerMinCriteria

        ' Validate the orbital eccentricity
        If Not isValidReal(EccInput, dEcc, HIDE_ERRORS) Then
            ErrMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
            Return
        End If
        If dEcc < 0 Then
            ErrMsg("Invalid Orbital Eccentricity was entered - try again", "Invalid Orbital Eccentricity")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Solve Kepler's Equation for Mean Anomaly " & angleToStr(dMA, DMSFORMAT) &
                    " and Orbital Eccentricity = " & dEcc, CENTERTXT)
        printlncond("Iterate until difference is less than " & dTerm & " radians (" &
                    Format(rad2deg(dTerm) * 3600.0, genFloatFormat) & " arcseconds)", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        dMARad = deg2rad(dMA)
        printlncond("1. Convert mean anomaly to radians.")
        printlncond("   MA_rad = " & Format(dMARad, genFloatFormat) & " radians")
        printlncond()

        If dEcc < 0.75 Then
            dEA = dMARad
        Else
            dEA = PI
        End If
        printlncond("2. Set an initial estimate for E0. If the orbital eccentricity is < 0.75,")
        printlncond("   E0 = MA_rad, otherwise E0 = pi radians. Thus, ")
        printlncond("   E0 = " & Format(dEA, genFloatFormat) & " radians")
        printlncond()

        printlncond("3. Using the iteration scheme")
        printlncond("   E_i = E_(i-1) - [E_(i-1) - e*sin(E_(i-1)-MA_rad]/[1-e*cos(E_(i-1))],")
        printlncond("   iterate until the difference between successive estimates is")
        printlncond("   less than the termination criteria (" & dTerm & " radians)")
        printlncond()

        For iterCnt = 1 To KeplerMaxIterations
            ' Note that the sine & cosine functions here uses radians, not degrees!
            dEANext = dEA - (dEA - dEcc * Sin(dEA) - dMARad) / (1 - dEcc * Cos(dEA))
            delta = Abs(dEANext - dEA)
            printlncond("   E" & iterCnt & " = " & Format(dEANext, genFloatFormat) & ", Delta = " &
                        Format(delta, genFloatFormat))
            dEA = dEANext
            If delta <= dTerm Then Exit For
        Next
        If iterCnt >= KeplerMaxIterations Then
            printlncond("WARNING: Did not converge, so stopped after " & KeplerMaxIterations & " iterations.")
        End If
        printlncond()

        printlncond("4. The eccentric anomaly (in radians) is the last result. So,")
        printlncond("   Eccentric Anomaly = E" & iterCnt & " = " & Format(dEA, genFloatFormat) & " radians")
        printlncond()

        dEA = rad2deg(dEA)
        printlncond("5. Convert the eccentric anomaly from radians to degrees.")
        printlncond("   Eccentric Anomaly = " & angleToStr(dEA, DECFORMAT))
        printlncond()

        printlncond("6. Convert the Eccentric Anomaly to DMS format.")
        printlncond("   Eccentric Anomaly = " & angleToStr(dEA, DMSFORMAT))
        printlncond()

        result = "After " & iterCnt & " iterations (Newton/Raphson method), Eccentric Anomaly = " & angleToStr(dEA, DMSFORMAT)
        printlncond("Thus, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------
    ' Clear all the text areas in the GUI
    '    ' Clear all text areas
    '--------------------------------------
    Private Sub clearAllTextAreas()
        setDataLabels("", "", "", "")
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '--------------------------------------
    ' Set the GUI Data Labels. This method
    ' is overloaded to allow setting only
    ' one or multiple data labels.
    '
    ' d1        text for 1st data label
    ' d2        text for 2nd data label
    ' d3        text for 3rd data label
    ' d4        text for 4th data label
    '-------------------------------------
    Private Sub setDataLabels(ByVal d1 As String)
        setDataLabels(d1, "", "", "")
    End Sub
    Private Sub setDataLabels(ByVal d1 As String, ByVal d2 As String)
        setDataLabels(d1, d2, "", "")
    End Sub
    Private Sub setDataLabels(ByVal d1 As String, ByVal d2 As String, ByVal d3 As String)
        setDataLabels(d1, d2, d3, "")
    End Sub
    Private Sub setDataLabels(ByVal d1 As String, ByVal d2 As String, ByVal d3 As String, ByVal d4 As String)
        lblData1.Text = d1
        lblData2.Text = d2
        lblData3.Text = d3
        lblData4.Text = d4
        txtboxData1.Text = ""
        txtboxData2.Text = ""
        txtboxData3.Text = ""
        txtboxData4.Text = ""
    End Sub

    '---------------------------------------------------------
    ' Compute the obliquity of the ecliptic for a given epoch
    '
    ' Epoch             epoch for which obliquity is desired
    '
    ' Returns obliquity of the ecliptic calculated for the
    ' stated epoch.
    '---------------------------------------------------------
    Private Function computeEclipticObliquity(ByVal Epoch As Double) As Double
        Dim JD, dT, dDE, dE As Double
        Dim iEpoch As Integer = Trunc(Epoch)

        printlncond("Steps 1-4 compute the Obliquity of the Ecliptic for Epoch " & Format(Epoch, "#0.0#"))
        printlncond()

        JD = dateToJD(1, 0.0, iEpoch)
        printlncond("1.  Calculate the Julian Day Number for January 0.0 of the given Epoch")
        printlncond("    JD = " & dateToStr(1, 0.0, iEpoch) & " = " & Format(JD, JDFormat))
        printlncond()

        dT = (JD - 2451545.0) / 36525.0
        printlncond("2.  T = (JD - 2451545.0) / 36525 = (" & Format(JD, JDFormat) & " - 2451545.0) / 36525")
        printlncond("      = " & Format(dT, genFloatFormat) & " centuries")
        printlncond()

        dDE = 46.815 * dT + 0.0006 * dT * dT - 0.00181 * dT * dT * dT
        printlncond("3.  DE = 46.815 * T + 0.0006 * T * T - 0.00181 * T * T * T")
        printlncond("       = 46.815*" & Format(dT, genFloatFormat) & " + 0.0006*" & Format(dT * dT, genFloatFormat) &
                    " - 0.00181*" & Format(dT * dT * dT, genFloatFormat))
        printlncond("       = " & Format(dDE, decDegFormat) & " arcsecs")
        printlncond()

        dE = 23.439292 - (dDE / 3600.0)
        printlncond("4.  E = 23.439292 - (DE / 3600) = 23.439292 - (" & Format(dDE, decDegFormat) & " / 3600)")
        printlncond("      = " & Format(dE, decDegFormat) & " degrees")
        printlncond()

        Return dE
    End Function

End Class
