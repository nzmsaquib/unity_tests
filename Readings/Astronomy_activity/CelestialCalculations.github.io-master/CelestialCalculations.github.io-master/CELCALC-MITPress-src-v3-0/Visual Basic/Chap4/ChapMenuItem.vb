﻿'**********************************************************
'                     Copyright (c) 2018
'                   Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define what menu item has been selected to know what to do when the calculate button is pressed
    Friend Enum CalculationType
        NO_CALC_SELECTED
        HA_RA
        RA_HA
        HORIZON_EQUATORIAL
        EQUATORIAL_HORIZON
        ECLIPTIC_EQUATORIAL
        EQUATORIAL_ECLIPTIC
        GALACTIC_EQUATORIAL
        EQUATORIAL_GALACTIC
        PRECESSION_CORR
        KEPLER_SIMPLE
        KEPLER_NEWTON
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub

End Class
