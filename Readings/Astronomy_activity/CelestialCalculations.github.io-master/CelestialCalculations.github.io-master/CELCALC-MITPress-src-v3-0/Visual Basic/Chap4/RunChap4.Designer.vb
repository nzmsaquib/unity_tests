﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chap4GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Chap4GUI))
        Me.ChapMenu = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCoordSys = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuKeplersEq = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemInstructions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelBookTitle = New System.Windows.Forms.Panel()
        Me.lblBookTitle = New System.Windows.Forms.Label()
        Me.panelTextOut = New System.Windows.Forms.Panel()
        Me.txtboxOutputArea = New System.Windows.Forms.RichTextBox()
        Me.chkboxShowInterimCalcs = New System.Windows.Forms.CheckBox()
        Me.txtboxData1 = New System.Windows.Forms.TextBox()
        Me.txtboxData2 = New System.Windows.Forms.TextBox()
        Me.lblData1 = New System.Windows.Forms.Label()
        Me.lblData2 = New System.Windows.Forms.Label()
        Me.lblResults = New System.Windows.Forms.Label()
        Me.lblData4 = New System.Windows.Forms.Label()
        Me.lblData3 = New System.Windows.Forms.Label()
        Me.txtboxData4 = New System.Windows.Forms.TextBox()
        Me.txtboxData3 = New System.Windows.Forms.TextBox()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.ChapMenu.SuspendLayout()
        Me.panelBookTitle.SuspendLayout()
        Me.panelTextOut.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChapMenu
        '
        Me.ChapMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ChapMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuCoordSys, Me.mnuKeplersEq, Me.mnuHelp})
        Me.ChapMenu.Location = New System.Drawing.Point(0, 0)
        Me.ChapMenu.Name = "ChapMenu"
        Me.ChapMenu.Size = New System.Drawing.Size(993, 28)
        Me.ChapMenu.TabIndex = 0
        Me.ChapMenu.Text = "ChapMenu"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(44, 24)
        Me.mnuFile.Text = "File"
        '
        'mnuitemExit
        '
        Me.mnuitemExit.Name = "mnuitemExit"
        Me.mnuitemExit.Size = New System.Drawing.Size(108, 26)
        Me.mnuitemExit.Text = "Exit"
        '
        'mnuCoordSys
        '
        Me.mnuCoordSys.Name = "mnuCoordSys"
        Me.mnuCoordSys.Size = New System.Drawing.Size(119, 24)
        Me.mnuCoordSys.Text = "Coord Systems"
        '
        'mnuKeplersEq
        '
        Me.mnuKeplersEq.Name = "mnuKeplersEq"
        Me.mnuKeplersEq.Size = New System.Drawing.Size(136, 24)
        Me.mnuKeplersEq.Text = "Kepler's Equation"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemInstructions, Me.mnuitemAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(53, 24)
        Me.mnuHelp.Text = "Help"
        '
        'mnuitemInstructions
        '
        Me.mnuitemInstructions.Name = "mnuitemInstructions"
        Me.mnuitemInstructions.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemInstructions.Text = "Instructions"
        '
        'mnuitemAbout
        '
        Me.mnuitemAbout.Name = "mnuitemAbout"
        Me.mnuitemAbout.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemAbout.Text = "About"
        '
        'panelBookTitle
        '
        Me.panelBookTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBookTitle.BackColor = System.Drawing.Color.Navy
        Me.panelBookTitle.Controls.Add(Me.lblBookTitle)
        Me.panelBookTitle.Location = New System.Drawing.Point(0, 32)
        Me.panelBookTitle.Name = "panelBookTitle"
        Me.panelBookTitle.Size = New System.Drawing.Size(993, 50)
        Me.panelBookTitle.TabIndex = 1
        '
        'lblBookTitle
        '
        Me.lblBookTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblBookTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBookTitle.ForeColor = System.Drawing.Color.White
        Me.lblBookTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBookTitle.Location = New System.Drawing.Point(0, 14)
        Me.lblBookTitle.Name = "lblBookTitle"
        Me.lblBookTitle.Size = New System.Drawing.Size(993, 25)
        Me.lblBookTitle.TabIndex = 0
        Me.lblBookTitle.Text = "Book Title"
        Me.lblBookTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'panelTextOut
        '
        Me.panelTextOut.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelTextOut.Controls.Add(Me.txtboxOutputArea)
        Me.panelTextOut.Location = New System.Drawing.Point(0, 221)
        Me.panelTextOut.Name = "panelTextOut"
        Me.panelTextOut.Padding = New System.Windows.Forms.Padding(5)
        Me.panelTextOut.Size = New System.Drawing.Size(993, 491)
        Me.panelTextOut.TabIndex = 4
        '
        'txtboxOutputArea
        '
        Me.txtboxOutputArea.BackColor = System.Drawing.Color.White
        Me.txtboxOutputArea.Cursor = System.Windows.Forms.Cursors.No
        Me.txtboxOutputArea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtboxOutputArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxOutputArea.Location = New System.Drawing.Point(5, 5)
        Me.txtboxOutputArea.Name = "txtboxOutputArea"
        Me.txtboxOutputArea.ReadOnly = True
        Me.txtboxOutputArea.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtboxOutputArea.Size = New System.Drawing.Size(983, 481)
        Me.txtboxOutputArea.TabIndex = 0
        Me.txtboxOutputArea.TabStop = False
        Me.txtboxOutputArea.Text = ""
        '
        'chkboxShowInterimCalcs
        '
        Me.chkboxShowInterimCalcs.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxShowInterimCalcs.AutoSize = True
        Me.chkboxShowInterimCalcs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxShowInterimCalcs.Location = New System.Drawing.Point(363, 87)
        Me.chkboxShowInterimCalcs.Name = "chkboxShowInterimCalcs"
        Me.chkboxShowInterimCalcs.Size = New System.Drawing.Size(266, 24)
        Me.chkboxShowInterimCalcs.TabIndex = 1
        Me.chkboxShowInterimCalcs.Text = "Show Intermediate Calculations"
        Me.chkboxShowInterimCalcs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkboxShowInterimCalcs.UseVisualStyleBackColor = True
        '
        'txtboxData1
        '
        Me.txtboxData1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxData1.Location = New System.Drawing.Point(11, 116)
        Me.txtboxData1.Name = "txtboxData1"
        Me.txtboxData1.Size = New System.Drawing.Size(148, 27)
        Me.txtboxData1.TabIndex = 2
        Me.txtboxData1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtboxData2
        '
        Me.txtboxData2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxData2.Location = New System.Drawing.Point(11, 151)
        Me.txtboxData2.Name = "txtboxData2"
        Me.txtboxData2.Size = New System.Drawing.Size(148, 27)
        Me.txtboxData2.TabIndex = 3
        Me.txtboxData2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblData1
        '
        Me.lblData1.AutoSize = True
        Me.lblData1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData1.Location = New System.Drawing.Point(162, 121)
        Me.lblData1.Name = "lblData1"
        Me.lblData1.Size = New System.Drawing.Size(105, 20)
        Me.lblData1.TabIndex = 10
        Me.lblData1.Text = "Data Label 1"
        Me.lblData1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblData2
        '
        Me.lblData2.AutoSize = True
        Me.lblData2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData2.Location = New System.Drawing.Point(162, 156)
        Me.lblData2.Name = "lblData2"
        Me.lblData2.Size = New System.Drawing.Size(105, 20)
        Me.lblData2.TabIndex = 11
        Me.lblData2.Text = "Data Label 2"
        Me.lblData2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblResults
        '
        Me.lblResults.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblResults.AutoSize = True
        Me.lblResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResults.Location = New System.Drawing.Point(9, 191)
        Me.lblResults.Name = "lblResults"
        Me.lblResults.Size = New System.Drawing.Size(63, 20)
        Me.lblResults.TabIndex = 9
        Me.lblResults.Text = "Result"
        Me.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblData4
        '
        Me.lblData4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblData4.AutoSize = True
        Me.lblData4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData4.Location = New System.Drawing.Point(643, 161)
        Me.lblData4.Name = "lblData4"
        Me.lblData4.Size = New System.Drawing.Size(105, 20)
        Me.lblData4.TabIndex = 13
        Me.lblData4.Text = "Data Label 4"
        Me.lblData4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblData3
        '
        Me.lblData3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblData3.AutoSize = True
        Me.lblData3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData3.Location = New System.Drawing.Point(643, 128)
        Me.lblData3.Name = "lblData3"
        Me.lblData3.Size = New System.Drawing.Size(105, 20)
        Me.lblData3.TabIndex = 12
        Me.lblData3.Text = "Data Label 3"
        Me.lblData3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtboxData4
        '
        Me.txtboxData4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtboxData4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxData4.Location = New System.Drawing.Point(493, 156)
        Me.txtboxData4.Name = "txtboxData4"
        Me.txtboxData4.Size = New System.Drawing.Size(148, 27)
        Me.txtboxData4.TabIndex = 5
        Me.txtboxData4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtboxData3
        '
        Me.txtboxData3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.txtboxData3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxData3.Location = New System.Drawing.Point(493, 121)
        Me.txtboxData3.Name = "txtboxData3"
        Me.txtboxData3.Size = New System.Drawing.Size(148, 27)
        Me.txtboxData3.TabIndex = 4
        Me.txtboxData3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnCalculate
        '
        Me.btnCalculate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCalculate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalculate.Location = New System.Drawing.Point(880, 189)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(107, 31)
        Me.btnCalculate.TabIndex = 6
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'Chap4GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(993, 713)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.lblData4)
        Me.Controls.Add(Me.lblData3)
        Me.Controls.Add(Me.txtboxData4)
        Me.Controls.Add(Me.txtboxData3)
        Me.Controls.Add(Me.lblResults)
        Me.Controls.Add(Me.lblData2)
        Me.Controls.Add(Me.lblData1)
        Me.Controls.Add(Me.txtboxData2)
        Me.Controls.Add(Me.txtboxData1)
        Me.Controls.Add(Me.chkboxShowInterimCalcs)
        Me.Controls.Add(Me.panelTextOut)
        Me.Controls.Add(Me.panelBookTitle)
        Me.Controls.Add(Me.ChapMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.ChapMenu
        Me.Name = "Chap4GUI"
        Me.Text = "Title"
        Me.ChapMenu.ResumeLayout(False)
        Me.ChapMenu.PerformLayout()
        Me.panelBookTitle.ResumeLayout(False)
        Me.panelTextOut.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChapMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCoordSys As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuKeplersEq As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemInstructions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents panelBookTitle As System.Windows.Forms.Panel
    Friend WithEvents lblBookTitle As System.Windows.Forms.Label
    Friend WithEvents panelTextOut As System.Windows.Forms.Panel
    Friend WithEvents txtboxOutputArea As System.Windows.Forms.RichTextBox
    Friend WithEvents chkboxShowInterimCalcs As System.Windows.Forms.CheckBox
    Friend WithEvents txtboxData1 As System.Windows.Forms.TextBox
    Friend WithEvents txtboxData2 As System.Windows.Forms.TextBox
    Friend WithEvents lblData1 As System.Windows.Forms.Label
    Friend WithEvents lblData2 As System.Windows.Forms.Label
    Friend WithEvents lblResults As System.Windows.Forms.Label
    Friend WithEvents lblData4 As System.Windows.Forms.Label
    Friend WithEvents lblData3 As System.Windows.Forms.Label
    Friend WithEvents txtboxData4 As System.Windows.Forms.TextBox
    Friend WithEvents txtboxData3 As System.Windows.Forms.TextBox
    Friend WithEvents btnCalculate As System.Windows.Forms.Button

End Class
