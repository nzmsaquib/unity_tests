﻿'********************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class implements methods to display messages
' to the user. These methods are Shared so that a class
' instance does not have to be created prior to usage.
' These methods simplify translating the code to other 
' languages, and they help enforce consistency in usage and style.
'********************************************************************

Option Explicit On
Option Strict On

Imports ASTUtils.ASTMisc

Public Class ASTMsg

    '***************************************************************************
    ' Asks the user if they wish to abort
    '
    ' msg          text message to display as a prompt
    '***************************************************************************
    Public Shared Function AbortMsg(ByVal msg As String) As Boolean
        Return PleaseConfirm(msg & vbNewLine & "Abort processing?", "")
    End Function

    '*****************************************************************************
    ' Display a critical error message
    '
    ' msg          text message to display
    ' abort        If ABORT_PROG, abort the program. The default is not to abort.
    '*****************************************************************************
    Public Shared Sub CriticalErrMsg(ByVal msg As String)
        MsgBox("Severe Error: " & msg, vbCritical, "Severe Error")
    End Sub
    Public Shared Sub CriticalErrMsg(ByVal msg As String, ByVal abort As Boolean)
        CriticalErrMsg(msg)
        If abort = ABORT_PROG Then Environment.Exit(-1)
    End Sub

    '****************************************************************************
    ' Conditionally display an error message, but only if errFlag is SHOW_ERRORS.
    '
    ' This method is overloaded to allow errFlag to be optional. The default
    ' is to show errors.
    '
    ' msg          text message to display
    ' title        title for the message window
    ' errFlag      whether to display error messages at all
    '****************************************************************************
    Public Shared Sub ErrMsg(ByVal msg As String, ByVal title As String, ByVal errFlag As Boolean)
        If (errFlag = SHOW_ERRORS) Then MsgBox(msg, vbExclamation, title)
    End Sub
    Public Shared Sub ErrMsg(ByVal msg As String, ByVal title As String)
        ErrMsg(msg, title, SHOW_ERRORS)
    End Sub

    '****************************************************************************
    ' Display a simple informational message.
    '
    ' msg          text message to display
    ' title        title for the message window
    '****************************************************************************
    Public Shared Sub InfoMsg(ByVal msg As String)
        InfoMsg(msg, " ")
    End Sub
    Public Shared Sub InfoMsg(ByVal msg As String, ByVal title As String)
        MsgBox(msg, vbInformation, title)
    End Sub

    '***************************************************************************
    ' Asks the user to confirm something (such as whether to exit)
    '
    ' msg          text message to display as a prompt
    ' title        title for the message window
    '***************************************************************************
    Public Shared Function PleaseConfirm(ByVal msg As String, ByVal title As String) As Boolean
        If (MsgBox(msg, CType(vbYesNo + vbQuestion, MsgBoxStyle), title) = vbYes) Then Return True
        Return False
    End Function

End Class
