﻿'****************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides the ability to load orbital elements
' from a data file and to store them in an accessible
' structure. This is done so that changing from one epoch
' to another only requires that the orbital elements
' be loaded from a different data file. The default
' data file is J2000.dat under the data files 
' directory.
'
' An orbital elements data  file **MUST** be in the format
' specifically designed for this book. The format can be
' gleaned by opening the J2000.dat data file and examining
' its contents. The format is straightforward and is
' documented in comments within the data file itself.
'****************************************************************

Option Explicit On
Option Strict On

Imports System.IO
Imports System.Math

Imports ASTUtils.ASTDate
Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Public Class ASTOrbits
    Private Const NA_VALUE = -1.0       ' value to use when something is N/A as an orbital element

    ' Define the class instance variables.
    Private epochDate As Double         ' epoch as a date, such as 2000.0 or 1980.0
    Private epochJD As Double           ' epoch as a Julian day number

    ' Define the Earth's standard gravitational parameter. This is only
    ' used in the satellites chapter for a bit of extra accuracy when dealing with
    ' satellites. For other chapters, the value is taken from the orbital
    ' elements data file.
    Public Const muEarth = 398600.4418

    ' Define radius of the Earth. We could read this from a data file, but this is easier.
    Public Const EarthRadius = 6378.135

    ' The Earth, Sun, and Moon are mandatory and require special handling,
    ' so save their index into the orbital elements db
    Private idxSun As Integer = -1
    Private idxMoon As Integer = -1
    Private idxEarth As Integer = -1

    ' Prompts and other data for allowing the user to find an orbital elements data file
    ' and to load the default orbital elements data file at startup
    Private Const dataPrompt = "Open an Orbital Elements data file ..."
    Private Const dataFilename = "J2000.dat"            ' this is the default orbital elements file
    Private Const dataPathname = "..\Ast Data Files\"
    Private Const dataExt = ".dat"
    Private Const dataFilter = "Orbital Elements|*.*"

    ' we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
    Private prt As ASTPrt = Nothing

    Private orbitalElementsLoaded As Boolean = False

    '---------------------------------------------------------------
    ' Define a private class and database for the orbital elements.
    '---------------------------------------------------------------
    Friend Class OrbElemObj
        Friend sName As String          ' the name of the object to which this orbital element data applies
        Friend bInferior As Boolean     ' true if this is an object whose orbit is between the Earth and Sun
        Friend dPeriod As Double        ' orbital period in tropical years
        Friend dMass As Double          ' object's mass relative to the Earth
        Friend dRadius As Double        ' object's radius in km
        Friend dDay As Double           ' length of day relative to Earth
        Friend dEccentricity As Double  ' orbital eccentricity
        Friend dSemiMajAxisAU As Double ' length of the semi-major axis in AUs
        Friend dSemiMajAxisKM As Double ' length of the semi-major axis in km (Sun and Moon only)
        Friend dAngDiamArcSec As Double ' angular diameter in arcseconds
        Friend dAngDiamDeg As Double    ' angular diameter in degrees (Sun and Moon only)
        Friend dmV As Double            ' visual magnitude
        Friend dGravParm As Double      ' gravitational parameter in km^3/s^2
        Friend dInclination As Double   ' orbital inclination in degrees
        Friend dLonAtEpoch As Double    ' longitude at the epoch in degrees
        Friend dLonAtPeri As Double     ' longitude at perihelion in degrees
        Friend dLonAscNode As Double    ' longitude of the ascending node
    End Class

    ' This array holds the orbital elements
    Private Shared orbElementsDB As List(Of OrbElemObj) = New List(Of OrbElemObj)

    '***********************************************************
    ' Initialize from the default orbital elements data file, if
    ' it exists. If it doesn't, require the user to find it.
    ' This method should only be called once and then methods
    ' below should be used to load a new orbital elements data
    ' file under the calling application's control.
    '***********************************************************
    Sub New(ByVal prtInstance As ASTPrt)
        initOrbitalElements(prtInstance)
    End Sub

    '********************************************************************
    ' Define 'get' accessors for the instance data for this class and
    ' for the elements in the orbital elements database.
    '
    ' Warning: No check is made to be sure idx is valid. Also, note that
    ' if an item is N/A (e.g., angular diameter of the Earth), the
    ' value NA_VALUE is returned.
    '********************************************************************
    Public Function getNumOEDBObjs() As Integer
        Return orbElementsDB.Count()
    End Function
    Public Function getOEEpochDate() As Double
        Return epochDate
    End Function
    Public Function getOEEpochJD() As Double
        Return epochJD
    End Function
    Public Function getOEDBSunIndex() As Integer
        Return idxSun
    End Function
    Public Function getOEDBMoonIndex() As Integer
        Return idxMoon
    End Function
    Public Function getOEDBEarthIndex() As Integer
        Return idxEarth
    End Function
    Public Function getOEObjName(ByVal idx As Integer) As String
        Return orbElementsDB(idx).sName
    End Function
    Public Function getOEObjInferior(ByVal idx As Integer) As Boolean
        Return orbElementsDB(idx).bInferior
    End Function
    Public Function getOEObjPeriod(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dPeriod
    End Function
    Public Function getOEObjMass(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dMass
    End Function
    Public Function getOEObjRadius(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dRadius
    End Function
    Public Function getOEObjDay(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dDay
    End Function
    Public Function getOEObjEccentricity(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dEccentricity
    End Function
    Public Function getOEObjSemiMajAxisAU(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dSemiMajAxisAU
    End Function
    Public Function getOEObjSemiMajAxisKM(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dSemiMajAxisKM
    End Function
    Public Function getOEObjAngDiamArcSec(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dAngDiamArcSec
    End Function
    Public Function getOEObjAngDiamDeg(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dAngDiamDeg
    End Function
    Public Function getOEObjmV(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dmV
    End Function
    Public Function getOEObjGravParm(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dGravParm
    End Function
    Public Function getOEObjInclination(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dInclination
    End Function
    Public Function getOEObjLonAtEpoch(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dLonAtEpoch
    End Function
    Public Function getOEObjLonAtPeri(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dLonAtPeri
    End Function
    Public Function getOEObjLonAscNode(ByVal idx As Integer) As Double
        Return orbElementsDB(idx).dLonAscNode
    End Function

    '******************************************************************
    ' Determine an object's orbit type based on orbital
    ' inclination and eccentricity.
    '
    ' e             orbital eccentricity
    ' inclin        orbital inclination in degrees
    '
    ' Returns orbit type or -1 for an error
    '
    ' Note: orbit types are as defined in the Satellites chapter.
    '*******************************************************************
    Public Shared Function getSatOrbitType(ByVal e As Double, ByVal inclin As Double) As Integer
        If e < 0.0 Then Return -1 ' negative eccentricity is impossible
        If e >= 1 Then Return -1 ' we don't handle parabolic or hyperbolic orbits

        If isClose(e, 0.0) Then     ' circular orbits
            If isClose(inclin, 0.0) Or isClose(inclin, 180.0) Then
                Return 1            ' circular orbit in the equatorial plane
            End If
            Return 3                ' circular orbit inclined w.r.t. equatorial plane
        End If
        If e > 0.0 Then             ' non-circular orbits
            If isClose(inclin, 0.0) Or isClose(inclin, 180.0) Then
                Return 2            ' elliptical orbit in the equatorial plane
            End If
            Return 4                ' elliptical orbit inclined w.r.t. equatorial plane
        End If
        Return -1       ' An error condition!
    End Function

    '************************************************************************
    ' Calculate the Earth's radius vector length (i.e., distance to the Sun)
    ' at the stated time and date.
    '
    ' month, day, year          date at which Earth's distance is desired
    ' UT                        time (UT) at which distance is desired
    ' solvetrueAnomaly          how to compute the true anomaly
    ' termCriteria              termination criteria if Kepler's equation
    '                           is solved
    '
    ' returns the Earth's radius vector length (R_e)
    '************************************************************************
    Public Function calcEarthRVLength(ByVal month As Integer, ByVal day As Integer, ByVal year As Integer,
                                          ByVal UT As Double, ByVal solveTrueAnomaly As TrueAnomalyType,
                                          ByVal termCriteria As Double) As Double
        Dim iter As Integer
        Dim R_e As Double = 1.0
        Dim obsDate As ASTDate = New ASTDate()
        Dim JD, JDe, De As Double
        Dim Ecc_e, M_e, v_e, Ec_e, Ea_e As Double

        If Not orbitalElementsLoaded Then
            ErrMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
            Return R_e
        End If

        obsDate.setMonth(month)
        obsDate.setYear(year)
        obsDate.setiDay(day)
        Ecc_e = getOEObjEccentricity(idxEarth)

        JDe = getOEEpochJD()
        JD = dateToJD(obsDate.getMonth(), obsDate.getdDay() + (UT / 24.0), obsDate.getYear())
        De = JD - JDe

        ' Calculate the Earth's mean and true anomalies
        M_e = (360 * De) / (365.242191 * getOEObjPeriod(idxEarth)) + getOEObjLonAtEpoch(idxEarth) - getOEObjLonAtPeri(idxEarth)
        M_e = xMOD(M_e, 360.0)

        ' Find the Earth's true anomaly from equation of center or Kepler's equation
        If solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER Then
            Ec_e = (360.0 / PI) * Ecc_e * SIN_D(M_e)
            v_e = M_e + Ec_e
        Else
            If solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
                Ea_e = calcSimpleKepler(M_e, Ecc_e, termCriteria, iter)
            Else
                Ea_e = calcNewtonKepler(M_e, Ecc_e, termCriteria, iter)
            End If

            v_e = (1 + Ecc_e) / (1 - Ecc_e)
            v_e = Sqrt(v_e) * TAN_D(Ea_e / 2.0)
            v_e = 2.0 * INVTAN_D(v_e)
        End If

        ' Calculate the Earth's radius vector length (R_e)
        R_e = getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e)
        R_e = R_e / (1 + Ecc_e * COS_D(v_e))

        Return R_e
    End Function

    '***************************************************************
    ' Calculate the Moon's ecliptic coordinates using the
    ' currently loaded orbital elements.
    '
    ' month, day, year      date at which Moon's position is desired
    ' UT                    time (UT) at which position is desired
    ' Ltrue                 Moon's true ecliptic longitude
    ' Omega_p               Moon's corrected longitude of the asc node
    ' Ca                    mean anomaly correction
    ' Mm                    Moon's mean anomaly
    ' Vmoon                 Moon's true anomaly
    ' solveTrueAnomaly      how to compute the true anomaly for the Sun
    ' termCriteria          termination criteria if Kepler's equation
    '                       is solved
    '
    ' Returns the ecliptic latitude/longitude as well as the Moon's
    ' true longitude, corrected longitude of the ascending node, 
    ' mean anomaly correction, and true anomaly.
    '***************************************************************
    Public Function calcMoonEclipticCoord(ByVal month As Integer, ByVal day As Integer, ByVal year As Integer,
                                          ByVal UT As Double, ByRef Ltrue As Double, ByRef Omega_p As Double,
                                          ByRef Ca As Double, ByRef Mm As Double, ByRef Vmoon As Double,
                                          ByVal solveTrueAnomaly As TrueAnomalyType,
                                          ByVal termCriteria As Double) As ASTCoord
        Dim eclCoord As ASTCoord = New ASTCoord(0.0, 0.0)
        Dim obsDate As ASTDate = New ASTDate()
        Dim TT, JDe, JD, De As Double
        Dim inclin, L_uncor, L_p, Omega, Ev, Ae, V As Double
        Dim Lmoon, Bmoon As Double
        Dim x, y, dT As Double
        Dim Msun, Vsun, Lsun As Double

        If Not orbitalElementsLoaded Then
            ErrMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
            Return eclCoord
        End If

        obsDate.setMonth(month)
        obsDate.setYear(year)
        obsDate.setiDay(day)
        inclin = getOEObjInclination(idxMoon)

        ' Adjust the time at which the Moon's position is required
        TT = UT + (63.8 / 3600.0)
        JDe = getOEEpochJD()
        JD = dateToJD(month, day + (TT / 24.0), year)
        De = JD - JDe

        ' Get the Sun's position
        eclCoord = calcSunEclipticCoord(month, day, year, UT, Msun, Vsun, solveTrueAnomaly, termCriteria)
        Lsun = eclCoord.getLonAngle.getDecAngle

        ' Compute various corrections
        L_uncor = 13.176339686 * De + getOEObjLonAtEpoch(idxMoon)
        L_uncor = xMOD(L_uncor, 360.0)
        Omega = getOEObjLonAscNode(idxMoon) - 0.0529539 * De
        Omega = xMOD(Omega, 360.0)
        Mm = L_uncor - 0.1114041 * De - getOEObjLonAtPeri(idxMoon)
        Mm = xMOD(Mm, 360.0)
        Ae = 0.1858 * SIN_D(Msun)
        Ev = 1.2739 * SIN_D(2 * (L_uncor - Lsun) - Mm)
        Ca = Mm + Ev - Ae - 0.37 * SIN_D(Msun)

        ' Calculate Moon's true anomaly
        Vmoon = 6.2886 * SIN_D(Ca) + 0.214 * SIN_D(2 * Ca)

        L_p = L_uncor + Ev + Vmoon - Ae
        V = 0.6583 * SIN_D(2 * (L_p - Lsun))
        Ltrue = L_p + V
        Omega_p = Omega - 0.16 * SIN_D(Msun)
        y = SIN_D(Ltrue - Omega_p) * COS_D(inclin)
        x = COS_D(Ltrue - Omega_p)
        dT = INVTAN_D(y / x)
        x = quadAdjust(y, x)
        dT = dT + x

        Lmoon = Omega_p + dT
        If Lmoon > 360 Then Lmoon = Lmoon - 360
        Bmoon = SIN_D(Ltrue - Omega_p) * SIN_D(inclin)
        Bmoon = INVSIN_D(Bmoon)
        eclCoord.setAngle1(Bmoon)
        eclCoord.setAngle2(Lmoon)

        Return eclCoord
    End Function

    '**************************************************************
    ' Calculate the distance (in AUs) from an object to the Earth.
    '
    ' Re            Earth's radius vector in AUs
    ' Le            Earth's heliocentric longitude
    ' Rp            object's radius vector in AUs
    ' Lp            object's heliocentric longitude
    '
    ' Returns distance from Earth to object in AUs.
    '**************************************************************
    Public Shared Function calcObjDistToEarth(ByVal Re As Double, ByVal Le As Double,
                                              ByVal Rp As Double, ByVal Lp As Double) As Double
        Return Sqrt(Re * Re + Rp * Rp - 2 * Re * Rp * COS_D(Lp - Le))
    End Function

    '***************************************************************
    ' Calculate an object's ecliptic coordinates using the
    ' currently loaded orbital elements. The object can be anything
    ' in the orbital elements file except the Sun, Moon, or Earth.
    '
    ' month, day, year      date at which object's position is desired
    ' UT                    time (UT) at which position is desired
    ' idx                   index into the orbital elements database
    '                       for the object whose position is desired
    ' L_p                   the object's heliocentric latitude
    ' R_p                   the object's radius vector length
    ' L_e                   the Earth's heliocentric latitude
    ' R_e                   the Earth's radius vector length
    ' solveTrueAnomaly      how to compute the true anomaly for the Sun,
    '                       Earth, and object
    ' termCriteria          termination criteria if Kepler's equation
    '                       is solved
    '
    ' Returns the ecliptic latitude/longitude for the object, and
    ' the radius vector length and heliocentric latitude for both
    ' the Earth and the object.
    '
    ' Caution: No check is made to be sure idx is valid.
    '***************************************************************
    Public Function calcObjEclipticCoord(ByVal month As Integer, ByVal day As Integer, ByVal year As Integer,
                                         ByVal UT As Double, ByVal idx As Integer,
                                         ByRef L_p As Double, ByRef R_p As Double,
                                         ByRef L_e As Double, ByRef R_e As Double,
                                         ByVal solveTrueAnomaly As TrueAnomalyType,
                                         ByVal termCriteria As Double) As ASTCoord

        Dim eclCoord As ASTCoord = New ASTCoord(0.0, 0.0)
        Dim iter As Integer
        Dim obsDate As ASTDate = New ASTDate()
        Dim JD, JDe, De As Double
        Dim Ecc_p, inclin_p, M_p, v_p, Ec_p, Ea_p, H_p, Lp_p, Lon_p, Lat_p As Double
        Dim Ecc_e, inclin_e, M_e, v_e, Ec_e, Ea_e, H_e As Double
        Dim x, y, dT As Double

        If Not orbitalElementsLoaded Then
            ErrMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
            Return eclCoord
        End If

        obsDate.setMonth(month)
        obsDate.setYear(year)
        obsDate.setiDay(day)
        Ecc_p = getOEObjEccentricity(idx)
        inclin_p = getOEObjInclination(idx)
        Ecc_e = getOEObjEccentricity(idxEarth)
        inclin_e = getOEObjInclination(idxEarth)

        JDe = getOEEpochJD()
        JD = dateToJD(obsDate.getMonth, obsDate.getdDay + (UT / 24.0), obsDate.getYear)
        De = JD - JDe

        ' Calculate the object's mean and true anomalies
        M_p = (360 * De) / (365.242191 * getOEObjPeriod(idx)) + getOEObjLonAtEpoch(idx) - getOEObjLonAtPeri(idx)
        M_p = xMOD(M_p, 360.0)

        ' Find the true anomaly from equation of center or Kepler's equation
        If solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER Then
            Ec_p = (360.0 / PI) * Ecc_p * SIN_D(M_p)
            v_p = M_p + Ec_p
        Else
            If solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
                Ea_p = calcSimpleKepler(M_p, Ecc_p, termCriteria, iter)
            Else
                Ea_p = calcNewtonKepler(M_p, Ecc_p, termCriteria, iter)
            End If
            v_p = (1 + Ecc_p) / (1 - Ecc_p)
            v_p = Sqrt(v_p) * TAN_D(Ea_p / 2.0)
            v_p = 2.0 * INVTAN_D(v_p)
        End If

        ' Calculate the object's heliocentric ecliptic coordinates (L_p, H_p)
        ' and radius vector length (R_p)
        L_p = v_p + getOEObjLonAtPeri(idx)
        L_p = xMOD(L_p, 360.0)
        H_p = SIN_D(L_p - getOEObjLonAscNode(idx)) * SIN_D(inclin_p)
        H_p = INVSIN_D(H_p)
        H_p = xMOD(H_p, 360.0)

        R_p = getOEObjSemiMajAxisAU(idx) * (1 - Ecc_p * Ecc_p)
        R_p = R_p / (1 + Ecc_p * COS_D(v_p))

        ' Repeat the steps just done for the object for the Earth

        'Calculate the Earth's mean and true anomalies
        M_e = (360 * De) / (365.242191 * getOEObjPeriod(idxEarth)) + getOEObjLonAtEpoch(idxEarth) - getOEObjLonAtPeri(idxEarth)
        M_e = xMOD(M_e, 360.0)

        ' Find the Earth's true anomaly from equation of center or Kepler's equation
        If solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER Then
            Ec_e = (360.0 / PI) * Ecc_e * SIN_D(M_e)
            v_e = M_e + Ec_e
        Else
            If solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
                Ea_e = calcSimpleKepler(M_e, Ecc_e, termCriteria, iter)
            Else
                Ea_e = calcNewtonKepler(M_e, Ecc_e, termCriteria, iter)
            End If

            v_e = (1 + Ecc_e) / (1 - Ecc_e)
            v_e = Sqrt(v_e) * TAN_D(Ea_e / 2.0)
            v_e = 2.0 * INVTAN_D(v_e)
        End If

        ' Calculate the Earth's heliocentric ecliptic coordinates (L_e, H_e)
        ' and radius vector length (R_e)
        L_e = v_e + getOEObjLonAtPeri(idxEarth)
        L_e = xMOD(L_e, 360.0)
        H_e = SIN_D(L_e - getOEObjLonAscNode(idxEarth)) * SIN_D(inclin_e)
        H_e = INVSIN_D(H_e)
        H_e = xMOD(H_e, 360.0)

        R_e = getOEObjSemiMajAxisAU(idxEarth) * (1 - Ecc_e * Ecc_e)
        R_e = R_e / (1 + Ecc_e * COS_D(v_e))

        ' Given the heliocentric location and radius vector length for both the Earth
        ' and the object, project the object's location onto the ecliptic plane
        ' with respect to the Earth

        y = SIN_D(L_p - getOEObjLonAscNode(idx)) * COS_D(inclin_p)
        x = COS_D(L_p - getOEObjLonAscNode(idx))
        dT = INVTAN_D(y / x)
        x = quadAdjust(y, x)
        dT = dT + x

        Lp_p = getOEObjLonAscNode(idx) + dT
        Lp_p = xMOD(Lp_p, 360.0)

        ' See if this is an inferior or superior object and compute accordingly
        If getOEObjInferior(idx) Then
            y = R_p * COS_D(H_p) * SIN_D(L_e - Lp_p)
            x = R_e - R_p * COS_D(H_p) * COS_D(L_e - Lp_p)
            dT = INVTAN_D(y / x)
            x = quadAdjust(y, x)
            dT = dT + x

            Lon_p = 180 + L_e + dT
            Lon_p = xMOD(Lon_p, 360.0)
        Else
            y = R_e * SIN_D(Lp_p - L_e)
            x = R_p * COS_D(H_p) - R_e * COS_D(L_e - Lp_p)
            dT = INVTAN_D(y / x)
            x = quadAdjust(y, x)
            dT = dT + x

            Lon_p = Lp_p + dT
            Lon_p = xMOD(Lon_p, 360.0)
        End If

        Lat_p = R_p * COS_D(H_p) * TAN_D(H_p) * SIN_D(Lon_p - Lp_p)
        Lat_p = Lat_p / (R_e * SIN_D(Lp_p - L_e))
        Lat_p = INVTAN_D(Lat_p)

        eclCoord.setAngle1(Lat_p)
        eclCoord.setAngle2(Lon_p)

        Return eclCoord
    End Function

    '*******************************************************
    ' Calculate the LST rising and setting times for a given
    ' equatorial coordinate.
    '
    ' RA, Decl      equatorial coordinates
    ' Lat           observer's latitude
    ' riseSet       set to true if coordinates rise/set for
    '               the given latitude
    '
    ' Returns LST rise time as result(0) and LST set time
    ' as result(1). The flag riseSet is set to true if the
    ' location rises/sets, otherwise it is false.
    '
    ' Note: Rising and setting azimuth are calculated although the setting
    ' azimuth isn't needed.
    '*******************************************************
    Public Shared Function calcRiseSetTimes(ByVal RA As Double, ByVal Decl As Double, ByVal Lat As Double,
                                            ByRef riseSet As Boolean) As Double()
        Dim result(1) As Double
        Dim Ar, R, S, H1, H2 As Double

        result(0) = 0.0         ' LST Rise time
        result(1) = 0.0         ' LST Set time
        riseSet = True
        Ar = SIN_D(Decl) / COS_D(Lat)
        If Abs(Ar) > 1 Then
            riseSet = False
            Return result
        End If

        R = INVCOS_D(Ar)                    ' rising azimuth
        S = 360.0 - R                       ' setting azimuth
        H1 = TAN_D(Lat) * TAN_D(Decl)
        If Abs(H1) > 1 Then
            riseSet = False
            Return result
        End If

        H2 = INVCOS_D(-H1) / 15.0
        result(0) = 24 + RA - H2
        If result(0) > 24 Then result(0) = result(0) - 24.0
        result(1) = RA + H2
        If result(1) > 24 Then result(1) = result(1) - 24.0
        Return result
    End Function

    '***************************************************************
    ' Calculate the Sun's ecliptic coordinates using the
    ' currently loaded orbital elements.
    '
    ' month, day, year      date at which Sun's position is desired
    ' UT                    time (UT) at which position is desired
    ' Msun                  Sun's mean anomaly as computed in
    '                       this method
    ' Vsun                  Sun's true anomaly as computed
    ' solveTrueAnomaly      how to compute the true anomaly
    ' termCriteria          termination criteria if Kepler's equation
    '                       is solved
    '
    ' Returns the ecliptic latitude/longitude as well as the mean
    ' and true anomalies.
    '***************************************************************
    Public Function calcSunEclipticCoord(ByVal month As Integer, ByVal day As Integer, ByVal year As Integer,
                                         ByVal UT As Double, ByRef Msun As Double, ByRef Vsun As Double,
                                         ByVal solveTrueAnomaly As TrueAnomalyType, ByVal termCriteria As Double) As ASTCoord

        Dim eclCoord As ASTCoord = New ASTCoord(0.0, 0.0)
        Dim Ecc, Lsun, JDe, JD, De, Ec, Ea As Double
        Dim iter As Integer
        Dim obsDate As ASTDate = New ASTDate()

        If Not orbitalElementsLoaded Then
            ErrMsg("ERROR: No orbital elements have been loaded", "No Orbital Elements")
            Return eclCoord
        End If

        Ecc = getOEObjEccentricity(idxSun)
        obsDate.setMonth(month)
        obsDate.setYear(year)
        obsDate.setiDay(day)

        JDe = getOEEpochJD()
        JD = dateToJD(month, day + (UT / 24.0), year)
        De = JD - JDe

        Msun = ((360.0 * De) / 365.242191) + getOEObjLonAtEpoch(idxSun) - getOEObjLonAtPeri(idxSun)
        Msun = xMOD(Msun, 360.0)

        ' Find the true anomaly from equation of center or Kepler's equation
        If solveTrueAnomaly = TrueAnomalyType.SOLVE_EQ_OF_CENTER Then
            Ec = (360.0 / PI) * Ecc * SIN_D(Msun)
            Vsun = Ec + Msun
        Else
            If solveTrueAnomaly = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
                Ea = calcSimpleKepler(Msun, Ecc, termCriteria, iter)
            Else
                Ea = calcNewtonKepler(Msun, Ecc, termCriteria, iter)
            End If
            Vsun = (1 + Ecc) / (1 - Ecc)
            Vsun = Sqrt(Vsun) * TAN_D(Ea / 2.0)
            Vsun = 2.0 * INVTAN_D(Vsun)
        End If

        Vsun = xMOD(Vsun, 360.0)
        Lsun = Vsun + getOEObjLonAtPeri(idxSun)
        If Lsun > 360.0 Then Lsun = Lsun - 360.0
        eclCoord.setAngle1(0.0)
        eclCoord.setAngle2(Lsun)

        Return eclCoord
    End Function

    '**************************************************
    ' Displays a list in the user's scrollable output
    ' text area of all the orbital elements for all
    ' of the objects in the database.
    '**************************************************
    Public Sub displayAllOrbitalElements()
        Dim i As Integer

        If Not isOrbElementsDBLoaded() Then
            ErrMsg("No Orbital Elements have been loaded", "No Orbital Elements")
            Return
        End If
        prt.setBoldFont(True)
        prt.println("Currently Loaded Orbital Elements", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()
        prt.println("Orbital Elements are for Epoch " & Format(epochDate, epochFormat) &
                    " (Julian Day Number " & Format(epochJD, JDFormat) & ")")
        prt.println()

        For i = 0 To getNumOEDBObjs() - 1
            prt.println(String.Format("{0,80:s}", "=").Replace(" ", "="))
            displayObjOrbElements(i)
            prt.println()
        Next
    End Sub

    '***********************************************
    ' Display the orbital elements for an object.
    '***********************************************
    Public Sub displayObjOrbElements(ByVal idx As Integer)
        Dim st As String = "Orbital Elements and Other Data for "

        If Not isOrbElementsDBLoaded() Then Return
        If (idx < 0) Or (idx > getNumOEDBObjs()) Then Return

        If (idx = getOEDBEarthIndex()) Or (idx = getOEDBMoonIndex()) Or (idx = getOEDBSunIndex()) Then
            st = st & "the "
        End If
        prt.setBoldFont(True)
        prt.println(st & orbElementsDB(idx).sName & " (Epoch: " & getOEEpochDate() & ")", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        If (idx <> getOEDBEarthIndex()) And (idx <> getOEDBMoonIndex()) And (idx <> getOEDBSunIndex()) Then
            prt.print("Object's orbit is ")
            If Not getOEObjInferior(idx) Then
                prt.print("not ")
            End If
            prt.println("between the Earth and the Sun")
            prt.println()
        End If

        prt.println("Mass: " & insertCommas(getOEObjMass(idx)) & " times the mass of the Earth")
        prt.println("Gravitational Parameter: " & insertCommas(getOEObjGravParm(idx)) & " km^3 per s^2")
        prt.println("Radius: " & insertCommas(getOEObjRadius(idx)) & " km")
        prt.println("Day: " & getOEObjDay(idx) & " Earth days")
        If (idx = getOEDBMoonIndex()) Then
            prt.println("Visual Magnitude (Full Moon): " & getOEObjmV(idx) & " when 1 AU from the Earth")
        ElseIf (idx <> getOEDBEarthIndex()) Then
            prt.println("Visual Magnitude: " & getOEObjmV(idx) & " when 1 AU from the Earth")
        End If
        If (idx <> getOEDBEarthIndex()) And (idx <> getOEDBMoonIndex()) Then
            prt.println("Angular Diameter: " & insertCommas(getOEObjAngDiamArcSec(idx)) &
                        " arc seconds when length of the semi-major axis away")
        End If
        If (idx = getOEDBSunIndex()) Or (idx = getOEDBMoonIndex()) Then
            prt.println("Angular Diameter: " & getOEObjAngDiamDeg(idx) &
                        " degrees when length of the semi-major axis away")
        End If

        prt.println()

        If (idx <> getOEDBSunIndex()) And (idx <> getOEDBMoonIndex()) Then
            prt.println("Orbital Period: " & getOEObjPeriod(idx) & " tropical years")
        End If
        prt.println("Orbital Inclination: " & getOEObjInclination(idx) & " degrees")
        prt.println("Orbital Eccentricity: " & getOEObjEccentricity(idx))
        prt.print("Length of Orbital Semi-Major Axis: " & insertCommas(getOEObjSemiMajAxisAU(idx)) &
                    " AUs")
        If (idx = getOEDBSunIndex()) Or (idx = getOEDBMoonIndex()) Then
            prt.print(" (which is approximately " & insertCommas(getOEObjSemiMajAxisKM(idx)) & " km)")
        End If
        prt.println()
        prt.println("Longitude at the Epoch: " & getOEObjLonAtEpoch(idx) & " degrees")
        prt.print("Longitude at ")
        If (idx = getOEDBMoonIndex()) Then
            prt.print("Perigee")
        Else prt.print("Perihelion")
        End If
        prt.println(": " & getOEObjLonAtPeri(idx) & " degrees")
        If (idx <> getOEDBSunIndex()) Then
            prt.println("Longitude of the Ascending Node: " & getOEObjLonAscNode(idx) & " degrees")
        End If
    End Sub

    '***********************************************************************
    ' Searches the orbital elements database and returns an index into the
    ' database for the requested object. The search performed is **not**
    ' case sensitive, but an exact match, ignoring white space, is required.
    '
    ' name                  name of the object to find
    '
    ' If successful, returns an index into the orbital elements database
    ' for the object requested. If unsuccessful, -1 is returned. Note that
    ' this assumes 0-based indexing!
    '***********************************************************************
    Public Function findOrbElementObjIndex(ByVal name As String) As Integer
        Dim i As Integer

        If Not isOrbElementsDBLoaded() Then Return -1

        For i = 0 To getNumOEDBObjs() - 1
            ' In Visual Basic, the 'True' parameter means to ignore case
            If String.Compare(name, getOEObjName(i), True) = 0 Then Return i
        Next

        Return -1
    End Function

    '************************************************************************
    ' Puts up a browser window and gets an orbital elements DB filename.
    '
    ' fullFilename  full name (file and path) of the orbital elements file
    '
    ' Returns true if user selected a file. The file selected is returned
    ' in fullFileName. This routine does **not** check to see if the file is
    ' a valid orbital elements db file, but it **does** check to see that
    ' the file exists.
    '************************************************************************
    Public Shared Function getOEDBFileToOpen(ByRef fullFilename As String) As Boolean
        Dim fChooser As ASTFileIO

        fChooser = New ASTFileIO(dataPrompt, dataFilter, dataExt)
        If Not fChooser.fileBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then Return False

        fullFilename = fChooser.fileBrowser.FileName
        ' See if the file exists
        If Not doesFileExist(fullFilename) Then
            ErrMsg("The Orbital Elements data file specified does not exist", "Orbital Elements File Does Not Exist")
            Return False
        End If

        Return True
    End Function

    '****************************************************
    ' Determine whether there is currently an orbital
    ' elements database loaded.
    '
    ' Returns true if a db is loaded, else false
    '****************************************************
    Public Function isOrbElementsDBLoaded() As Boolean
        If (idxSun < 0) Or (idxMoon < 0) Or (idxEarth < 0) Then Return False
        Return orbitalElementsLoaded
    End Function

    '*************************************************************
    ' Loads orbital elements database from disk.
    '
    ' filename      Full filename (including path) to load
    '
    ' Returns true if successful, else false.
    '*************************************************************
    Public Function loadOEDB(ByVal filename As String) As Boolean
        Dim br As StreamReader

        br = New System.IO.StreamReader(filename)

        If Not readOrbitalElementsData(br) Then
            br.Close()
            ErrMsg("An Orbital Elements data file is required but could" & vbNewLine &
                   "not be processed. Try another data file.", "Invalid Orbital Elements Data File")
            Return False
        End If

        br.Close()

        orbitalElementsLoaded = True
        idxEarth = findOrbElementObjIndex("Earth")
        idxMoon = findOrbElementObjIndex("Moon")
        idxSun = findOrbElementObjIndex("Sun")

        Return orbitalElementsLoaded
    End Function

    '----------------------------------------------------------------------------------------------
    ' Private methods used only in this class.
    '----------------------------------------------------------------------------------------------

    '------------------------------------------------------------
    ' Clears all the currently loaded orbital elements.
    '------------------------------------------------------------
    Private Sub clearOrbitalElementObjects()
        idxSun = -1
        idxMoon = -1
        idxEarth = -1

        ' Now delete all of the orbital element objects.
        ' In some languages, we'd need to loop through the db
        ' and delete objects individually.
        orbElementsDB.Clear()

        orbElementsDB = New List(Of OrbElemObj)

        orbitalElementsLoaded = False
    End Sub

    '-----------------------------------------------------------
    ' Does a one-time initialization by reading in the default
    ' orbital elements data file.
    '-----------------------------------------------------------
    Private Sub initOrbitalElements(ByVal prtInstance As ASTPrt)
        Dim fullFilename As String = IO.Path.Combine(dataPathname, dataFilename)
        Dim fChooser As ASTFileIO
        Dim br As StreamReader

        orbitalElementsLoaded = False

        If IsNothing(prtInstance) Then
            CriticalErrMsg("An output text area cannot be null. Aborting program ...", ABORT_PROG)
        End If

        prt = prtInstance

        ' See if the file exists where it is expected. If not, ask the user to find it.
        If doesFileExist(fullFilename) <> True Then
            ErrMsg("The Orbital Elements data file (" & dataFilename & ")" & vbNewLine &
                   "is missing. Please click 'OK' and then manually" & vbNewLine &
                   "find an orbital elements data file.", "Missing Orbital Elements Data File")

            ' Create a file browser form instance
            fChooser = New ASTFileIO(dataPrompt, dataFilter, dataFilename, dataExt)
            If fChooser.fileBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then
                fullFilename = fChooser.fileBrowser.FileName
            Else
                CriticalErrMsg("An Orbital Elements data file is required but was" & vbNewLine &
                               "not found. Aborting program ...", ABORT_PROG)
            End If
        End If

        ' Now that we've found the orbital elements data file, read in the data.
        br = New System.IO.StreamReader(fullFilename)

        If Not readOrbitalElementsData(br) Then
            br.Close()
            CriticalErrMsg("An Orbital Elements data file is required but could" & vbNewLine &
                           "not be processed. Aborting program ...", ABORT_PROG)
        End If

        'Close the file.
        br.Close()

        orbitalElementsLoaded = True
        idxEarth = findOrbElementObjIndex("Earth")
        idxMoon = findOrbElementObjIndex("Moon")
        idxSun = findOrbElementObjIndex("Sun")
    End Sub

    '-------------------------------------------------------------------------
    ' This method reads the already opened orbital data file, looks for a
    ' specific tag, and returns its value. This only handles tags whose
    ' value is a real number.
    '
    ' br            buffered stream to read from
    ' tag           tag to look for
    ' readError     set to true if a read error occurs (e.g., tag not found)
    ' dataError     set to true the data format is invalid.
    '
    ' Note that the dataError flag passed in may already be true, so the OR
    ' statement below avoids overwriting its previous value in case no
    ' errors were found in this invocation of this function. This allows a
    ' calling routine to perform multiple reads and then check the error
    ' only after the last one, which simplifies the coding logic in the
    ' calling routine.
    '-------------------------------------------------------------------------
    Private Function readElement(ByVal br As StreamReader, ByVal tag As String, ByRef readError As Boolean,
                                 ByRef dataError As Boolean) As Double
        Dim strIn As String
        Dim dResult As Double

        strIn = removeWhitespace(getSimpleTaggedValue(br, tag, readError))
        If IsNothing(strIn) Then Return NA_VALUE
        dataError = readError Or dataError Or (Not isValidReal(strIn, dResult, HIDE_ERRORS))
        If dataError Then
            Return NA_VALUE
        Else
            Return dResult
        End If
    End Function

    '------------------------------------------------------------------------
    ' Reads an orbital elements data file to extract and validate its data.
    ' The data tags **must** be in the order specified by this code.
    '
    ' br        buffered stream to read from
    '
    ' Returns true if successfully processed the orbital elements data.
    '------------------------------------------------------------------------
    Private Function readOrbitalElementsData(ByVal br As StreamReader) As Boolean
        Dim strIn, objName As String
        Dim i As Integer = -1
        Dim readError As Boolean = False
        Dim dataError As Boolean = False

        clearOrbitalElementObjects()

        ' Validate that this is an orbital elements data file
        If IsNothing(readTillStr(br, "<OrbitalElements>", HIDE_ERRORS)) Then Return False

        ' Get the epoch before reading in the individual orbital elements
        strIn = getSimpleTaggedValue(br, "<Epoch>", readError)
        If IsNothing(strIn) Then strIn = ""
        If Not isValidReal(strIn, epochDate, HIDE_ERRORS) Then
            ErrMsg("Invalid Epoch Date at string [" & strIn & "]", "Invalid Epoch")
            Return False
        End If
        strIn = getSimpleTaggedValue(br, "<JD>", readError)
        If IsNothing(strIn) Then strIn = ""
        If Not isValidReal(strIn, epochJD, HIDE_ERRORS) Then
            ErrMsg("Invalid Julian Day Number at string [" & strIn & "]", "Invalid JD")
            Return False
        End If

        ' Look for the beginning of the data section with the orbital elements,
        ' then cycle through and extract each object and its orbital elements.
        If IsNothing(readTillStr(br, "<Data>")) Then Return False

        Do While br.Peek() <> -1
            readError = False
            dataError = False

            strIn = getNonBlankLine(br, readError)
            If readError Then Exit Do

            strIn = removeWhitespace(strIn)
            If InStr(LCase(strIn), "</data>") > 0 Then Exit Do

            ' Convert the current tag to be an object's name
            strIn = replaceStr(strIn, "<", "")
            objName = replaceStr(strIn, ">", "")
            i = i + 1

            orbElementsDB.Add(New OrbElemObj)
            orbElementsDB(i).sName = objName

            ' In Visual Basic, the 'TRUE' parameter means to ignore case
            If String.Compare(objName, "Sun", True) = 0 Then
                idxSun = i
            ElseIf String.Compare(objName, "Moon", True) = 0 Then
                idxMoon = i
            ElseIf String.Compare(objName, "Earth", True) = 0 Then
                idxEarth = i
            End If

            strIn = removeWhitespace(getSimpleTaggedValue(br, "<Inferior>", readError))
            orbElementsDB(i).bInferior = (String.Compare(strIn, "true", True) = 0)
            strIn = removeWhitespace(getSimpleTaggedValue(br, "<Period>", readError))
            If (i = idxSun) Or (i = idxMoon) Then
                orbElementsDB(i).dPeriod = NA_VALUE
            Else
                dataError = dataError Or (Not isValidReal(strIn, orbElementsDB(i).dPeriod, HIDE_ERRORS))
            End If
            orbElementsDB(i).dMass = readElement(br, "<Mass>", readError, dataError)
            orbElementsDB(i).dRadius = readElement(br, "<Radius>", readError, dataError)
            orbElementsDB(i).dDay = readElement(br, "<Day>", readError, dataError)
            orbElementsDB(i).dEccentricity = readElement(br, "<Eccentricity>", readError, dataError)
            orbElementsDB(i).dSemiMajAxisAU = readElement(br, "<SemiMajorAxisAU>", readError, dataError)
            If (i = idxSun) Or (i = idxMoon) Then
                orbElementsDB(i).dSemiMajAxisKM = readElement(br, "<SemiMajorAxisKM>", readError, dataError)
            Else ' NA for all the rest of the objects
                orbElementsDB(i).dAngDiamArcSec = NA_VALUE
                strIn = getSimpleTaggedValue(br, "<SemiMajorAxisKM>", readError) ' Throw away; value is irrelevant
            End If
            If (i = idxEarth) Or (i = idxMoon) Then
                orbElementsDB(i).dAngDiamArcSec = NA_VALUE
                strIn = getSimpleTaggedValue(br, "<AngDiamArcSec>", readError)
            Else
                orbElementsDB(i).dAngDiamArcSec = readElement(br, "<AngDiamArcSec>", readError, dataError)
            End If
            If (i = idxSun) Or (i = idxMoon) Then
                orbElementsDB(i).dAngDiamDeg = readElement(br, "<AngDiamDeg>", readError, dataError)
            Else
                orbElementsDB(i).dAngDiamDeg = NA_VALUE
                strIn = getSimpleTaggedValue(br, "<AngDiamDeg>", readError)
            End If
            If (i = idxEarth) Then
                orbElementsDB(i).dmV = NA_VALUE
                strIn = getSimpleTaggedValue(br, "<VisualMagnitude>", readError)
            Else
                orbElementsDB(i).dmV = readElement(br, "<VisualMagnitude>", readError, dataError)
            End If
            orbElementsDB(i).dGravParm = readElement(br, "<GravParm>", readError, dataError)
            orbElementsDB(i).dInclination = readElement(br, "<Inclination>", readError, dataError)
            orbElementsDB(i).dLonAtEpoch = readElement(br, "<LonAtEpoch>", readError, dataError)
            orbElementsDB(i).dLonAtPeri = readElement(br, "<LonAtPeri>", readError, dataError)
            If (i = idxSun) Then
                orbElementsDB(i).dLonAscNode = NA_VALUE
                strIn = getSimpleTaggedValue(br, "<LonAscNode>", readError)
            Else
                orbElementsDB(i).dLonAscNode = readElement(br, "<LonAscNode>", readError, dataError)
            End If

            If readError Or dataError Then
                ErrMsg("The Orbital Elements data file has one or more" & vbNewLine &
                       "errors for the object [" & objName & "] Try another file.", "Orbital Elements Error")
                Return False
            End If

            ' Throw away the end tag
            readTillStr(br, "</" & objName & ">")
        Loop

        If (idxSun < 0) Or (idxMoon < 0) Or (idxEarth < 0) Then
            ErrMsg("Orbital Elements for the Sun, Moon, and Earth" & vbNewLine &
                   "are required but one or more are missing.", "Missing Orbital Elements")
            Return False
        End If

        orbitalElementsLoaded = True
        Return True
    End Function

End Class
