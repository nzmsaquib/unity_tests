﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class implements an observer class so that an
' object can be created that represents a default observer
' location and time.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.IO

Imports ASTUtils.ASTDate
Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTTime

Public Class ASTObserver
    Private obsLatLon As ASTLatLon = New ASTLatLon        ' Observer's lat/lon location
    Private tZone As TimeZoneType                           ' time zone as an enumerated type
    Private obsDate As ASTDate = New ASTDate              ' default date
    Private obsTime As ASTTime = New ASTTime              ' default obs time

    ' Location for the optional data file with the default observer location
    Private Const dataFullFilename = "..\Ast Data Files\DefaultObsLoc.dat"

    '***********************************************************
    ' Initialize from the default observer data file, if it
    ' exists. If it doesn't, do some reasonable default values.
    '***********************************************************
    Sub New()
        Dim latStr As String = "0N"
        Dim lonStr As String = "0E"
        Dim zoneStr As String = "LON"
        Dim dateStr As String = getCurrentDate()
        Dim timeStr As String = getCurrentTime()
        Dim strIn As String
        Dim errorOccurred As Boolean = False
        Dim br As StreamReader

        ' If default observer location data file exists, read it in.
        ' Otherwise, use the defaults set above
        If doesFileExist(dataFullFilename) Then
            br = New System.IO.StreamReader(dataFullFilename)

            ' Validate that this is an observer location data file. If it is,
            ' continue processing. Otherwise, use defaults set up in the
            ' DIM statements above.
            If Not IsNothing(readTillStr(br, "<ObserverLocation>", HIDE_ERRORS)) Then
                strIn = getSimpleTaggedValue(br, "<Latitude>", errorOccurred)
                If Not errorOccurred Then latStr = strIn
                strIn = getSimpleTaggedValue(br, "<Longitude>", errorOccurred)
                If Not errorOccurred Then lonStr = strIn
                strIn = getSimpleTaggedValue(br, "<TimeZone>", errorOccurred)
                If Not errorOccurred Then zoneStr = strIn
                br.Close()
            End If
        End If

        If isValidObsLoc(Me, latStr, lonStr, zoneStr, dateStr, timeStr) Then
            ' do nothing - we just want the observer location object updated
            ' which happens as a side effect of isValidObsLoc
        End If

    End Sub

    '********************************************************************
    ' Define 'get' accessors for the object's fields
    '********************************************************************
    ' Location fields
    Public Function getObsLocation() As ASTLatLon
        Return Me.obsLatLon
    End Function
    Public Function getObsLat() As Double
        Return Me.obsLatLon.getLat()
    End Function
    Public Function getObsLon() As Double
        Return Me.obsLatLon.getLon()
    End Function
    ' Time zone
    Public Function getObsTimeZone() As TimeZoneType
        Return Me.tZone
    End Function
    ' Time/Data fields
    Public Function getObsDate() As ASTDate
        Return Me.obsDate
    End Function
    Public Function getObsTime() As ASTTime
        Return Me.obsTime
    End Function

    Public Sub setObsDateTime(ByVal iMonth As Integer, iDay As Integer, iYear As Integer, dTime As Double)
        ' In an observer, the time is not included in the dDay data field
        Me.obsDate.setMonth(iMonth)
        Me.obsDate.setiDay(iDay)
        Me.obsDate.setYear(iYear)
        Me.obsDate.setdDay(iDay)
        Me.obsTime.setDecTime(dTime)
    End Sub

    '**********************************************************************
    ' Checks a collection of data strings to see if the data they contain
    ' specify a valid observer location, date, and time.
    '
    ' obsLoc            object for returning an observer's location data
    ' latStr            string with a latitude
    ' lonStr            string with a longitude
    ' tzStr             string with a time zone (PST, MST, CST,
    '                   EST, or LON)
    ' dateStr           string with a date
    ' timeStr           string with a time
    '
    ' Returns true if the strings represent a valid location. Also, if
    ' valid, obsLoc is updated. If not valid, obsLoc is undefined.
    '**********************************************************************
    Public Shared Function isValidObsLoc(ByRef obsLoc As ASTObserver, ByVal latStr As String,
                                         ByVal lonStr As String, ByVal tzStr As String,
                                         ByVal datestr As String, ByVal timestr As String) As Boolean

        If Not isValidLat(latStr, obsLoc.obsLatLon, HIDE_ERRORS) Then
            ErrMsg("The observer latitude specified is invalid - try again", "Invalid Latitude")
            Return False
        End If
        If Not isValidLon(lonStr, obsLoc.obsLatLon, HIDE_ERRORS) Then
            ErrMsg("The observer longitude specified is invalid - try again", "Invalid Longitude")
            Return False
        End If

        obsLoc.tZone = strToTimeZone(tzStr)

        If Not isValidDate(datestr, obsLoc.obsDate, HIDE_ERRORS) Then
            ErrMsg("The observer date specified is invalid - try again", "Invalid Date")
            Return False
        End If

        If Not isValidTime(timestr, obsLoc.obsTime, HIDE_ERRORS) Then
            ErrMsg("The observer time specified is invalid - try again", "Invalid Time")
            Return False
        End If

        Return True
    End Function

End Class
