﻿'******************************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides a class for angle objects and related methods.
'
' This class is primarily used for determining whether a string has a valid
' angle. Methods provided include validation routines in which an angle can
' be expressed in DMS format (###d ##m ##.##s) or decimal format. When
' expressed in DMS format, the d, m, and s values are optional as long as one
' is provided. They must be given in the correct order (i.e., 5m 30d is
' invalid because degrees must be specified before minutes). The angle object
' maintains the object's value in both DMS and decimal format so that either
' format can be readily obtained directly from the object.
'
' Note that to handle situations where the angle is negative but the integer
' degrees is 0 (e.g., -0d 13m 14.45s), the bNeg field is used to indicate whether
' the DMS format of the value is negative. The decimal value of the angle will be
' negative or positive as required and kept in synch with the bNeg field. Also
' note that due to roundoff errors, it is possible that minutes and seconds can
' exceed the value 60.
'******************************************************************************

Option Explicit On
Option Strict On
Imports System.Math
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle

Public Class ASTAngle
    ' Class instance variables. Save object value in both decimal and d,m,s formats
    Private bValid As Boolean           ' true if object is a valid angle
    Private dDecValue As Double         ' decimal value for the angle
    Private bNeg As Boolean             ' true if the angle is negative
    Private iDegrees As Integer
    Private iMinutes As Integer
    Private dSeconds As Double

    '*****************************************************
    ' Method for creating an angle object. Since much of 
    ' the usage of this class involves validating that a
    ' string has a valid angle, assume a new object is
    ' invalid until it is proven otherwise.
    '
    ' This function is overloaded to allow passing in
    ' an initial value or taking only the defaults.
    '
    ' decValue      decimal value for the angle object
    '               to be created
    '*****************************************************
    Sub New()
        bValid = False
        dDecValue = 0.0
        bNeg = False
        iDegrees = 0
        iMinutes = 0
        dSeconds = 0.0
    End Sub
    Sub New(ByVal decValue As Double)
        Me.setDecAngle(decValue)
    End Sub

    '********************************************************************
    ' Define 'get' accessors for an angle object's fields
    '********************************************************************
    Public Function isValidAngleObj() As Boolean
        Return Me.bValid
    End Function
    Public Function getDecAngle() As Double
        Return Me.dDecValue
    End Function
    Public Function isNegAngle() As Boolean
        Return Me.bNeg
    End Function
    Public Function getDegrees() As Integer
        Return Me.iDegrees
    End Function
    Public Function getMinutes() As Integer
        Return Me.iMinutes
    End Function
    Public Function getSeconds() As Double
        Return Me.dSeconds
    End Function

    '********************************************************************
    ' Define 'set' accessors for an angle object's fields. We actually
    ' will only need one.
    '********************************************************************
    Public Sub setDecAngle(ByVal decValue As Double)
        Dim tmp As Double

        tmp = Abs(decValue)

        Me.bValid = True
        Me.bNeg = (decValue < 0.0)
        Me.dDecValue = decValue
        Me.iDegrees = Trunc(tmp)
        Me.iMinutes = Trunc(Frac(tmp) * 60.0)
        Me.dSeconds = 60.0 * Frac(60 * Frac(tmp))
    End Sub

    '********************************************************************************
    ' Define some general methods for validating that a string has a valid angle
    ' expressed in either decimal format or DMS format.
    '********************************************************************************

    '********************************************************
    ' Validate a string that may be in either DMS or decimal
    ' format. Do not display any error messages unless flag
    ' is SHOW_ERRORS.
    '
    ' This function is overloaded to make the flag
    ' input parameter optional. The default is to
    ' show error messages.
    '
    ' inputStr          string to be validated
    ' angleObj          angle object to hold the result
    ' flag              flag indicating whether to display
    '                   error messages
    '
    ' Returns true if the angle is valid, else returns false.
    ' Also returns the angle in angleObj if valid.
    '********************************************************
    Public Shared Function isValidAngle(ByVal inputStr As String, ByRef angleObj As ASTAngle) As Boolean
        Return isValidAngle(inputStr, angleObj, SHOW_ERRORS)
    End Function
    Public Shared Function isValidAngle(ByVal inputStr As String, ByRef angleObj As ASTAngle, ByVal flag As Boolean) As Boolean
        Dim bValid As Boolean = False

        inputStr = removeWhitespace(inputStr)
        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter an angle as a valid decimal or DMS (###d ##m ##.##s) value",
                   "Invalid Angle Format", flag)
            Return False
        End If

        ' if 'd', 'm', or 's' is in the string, it is in DMS format
        If (InStr(inputStr, "d") > 0) Or (InStr(inputStr, "m") > 0) Or (InStr(inputStr, "s") > 0) Then
            bValid = isValidDMSAngle(inputStr, angleObj, flag)
        Else
            bValid = isValidDecAngle(inputStr, angleObj, flag)
        End If

        Return bValid
    End Function

    '***************************************************************
    ' Validate that a string contains a valid decimal format value
    ' Don't display any error messages unless flag is SHOW_ERRORS.
    '
    ' This function is overloaded to allow the flag input
    ' parameter to be optional. The default is to show errors.
    '
    ' inputStr         string to be validated
    ' angelObj         angle object to hold the results
    ' flag             whether to display error messages
    '
    ' Returns true if the angle is valid, else returns false.
    ' Also returns the angle in angleObj if valid.
    '***************************************************************
    Public Shared Function isValidDecAngle(ByVal inputStr As String, ByRef angleObj As ASTAngle) As Boolean
        Return isValidDecAngle(inputStr, angleObj, SHOW_ERRORS)
    End Function
    Public Shared Function isValidDecAngle(ByVal inputStr As String, ByRef angleObj As ASTAngle, ByVal flag As Boolean) As Boolean
        angleObj.bValid = False             ' assume invalid till we parse and prove otherwise
        angleObj.bNeg = False
        angleObj.dDecValue = 0.0
        angleObj.iDegrees = 0
        angleObj.iMinutes = 0
        angleObj.dSeconds = 0.0

        inputStr = removeWhitespace(inputStr)
        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter an angle as a valid decimal value", "Invalid Angle Format", flag)
            Return False
        End If

        If isValidReal(inputStr, angleObj.dDecValue, HIDE_ERRORS) Then
            angleObj = DecToDMS(angleObj.dDecValue)
        Else
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter an angle as a valid decimal value", "Invalid Angle Format", flag)
            Return False
        End If

        Return True
    End Function

    '**************************************************************
    ' Validate that a string contains a valid DMS format value.
    ' Don't display any error messages unless flag is SHOW_ERRORS.
    '
    ' This function is overloaded to allow the flag input
    ' parameter to be optional. The default is to show errors.
    '
    ' inputStr         string to be validated
    ' angleObj         angle object to hold the result
    ' flag             whether to display error messages
    '
    ' Returns true if the angle is valid, else returns false.
    ' Also returns the angle in angleObj if valid.
    '***************************************************************
    Public Shared Function isValidDMSAngle(ByVal inputStr As String, ByRef angleObj As ASTAngle) As Boolean
        Return isValidDMSAngle(inputStr, angleObj, SHOW_ERRORS)
    End Function
    Public Shared Function isValidDMSAngle(ByVal inputStr As String, ByRef angleObj As ASTAngle, ByVal flag As Boolean) As Boolean
        Dim temp As String
        Dim posD, posM, posS As Integer
        Dim bValidData As Boolean

        angleObj.bValid = False             ' assume invalid till we parse and prove otherwise
        angleObj.bNeg = False
        angleObj.iDegrees = 0
        angleObj.iMinutes = 0
        angleObj.dSeconds = 0.0

        inputStr = removeWhitespace(inputStr)
        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag)
            Return False
        End If

        inputStr = LCase(inputStr)
        If (inputStr.Substring(0, 1) = "-") Then angleObj.bNeg = True

        ' Figure out where the d, m, and s values are located
        posD = InStr(inputStr, "d")
        posM = InStr(inputStr, "m")
        posS = InStr(inputStr, "s")

        ' First, be sure that the delimiters d, m, and s appear no more than once in the input string
        If ((countChars(inputStr, CChar("d")) > 1) Or (countChars(inputStr, CChar("m")) > 1) Or (countChars(inputStr, CChar("s")) > 1)) Then
            ErrMsg("Only one value for Degrees, Minutes, and Seconds can be entered." & vbNewLine &
                   "Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag)
            Return False
        End If

        ' Now be sure they're in the correct order (i.e., dms) if they exist
        bValidData = True
        If posD > 0 Then                ' if true, the user specified degrees
            If (posM > 0) Then bValidData = (posM > posD) And bValidData
            If (posS > 0) Then bValidData = (posS > posD) And bValidData
        End If
        If posM > 0 Then                ' if true, the user specified minutes
            If (posS > 0) Then bValidData = (posS > posM) And bValidData
        End If

        If Not bValidData Then
            ErrMsg("The DMS values must be entered in the correct order." & vbNewLine &
                   "Please enter an angle as a valid DMS (###d ##m ##.##s) value", "Invalid Angle Format", flag)
            Return False
        End If

        ' At this point everything is in the correct order and appears no more than once.
        ' Get the degrees, which is the first in the string if the user entered degrees.
        If (posD > 0) Then
            temp = Strings.Left(inputStr, posD - 1)
            ' Now throw away the degrees. No need to worry if the result is null since that would mean
            ' that the user didn't enter minutes or seconds. This will be correctly caught below with posM and/or posS.
            inputStr = Strings.Right(inputStr, Len(inputStr) - posD)
            If isValidInt(temp, angleObj.iDegrees, HIDE_ERRORS) Then
                angleObj.iDegrees = Abs(angleObj.iDegrees)      ' angleObj.bNeg already contains the sign
            Else
                ErrMsg("The value for Degrees that was entered is invalid." & vbNewLine &
                       "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                       "Invalid Angle Format (Degrees)", flag)
                Return False
            End If
        End If

        ' Get minutes if they were entered, and they must be after the degrees, which we've already checked
        If (posM > 0) Then
            ' Have to update the value of posM since inputStr may have changed in the previous step for getting degrees
            posM = InStr(inputStr, "m")
            temp = Strings.Left(inputStr, posM - 1)
            inputStr = Strings.Right(inputStr, Len(inputStr) - posM)
            If Not isValidInt(temp, angleObj.iMinutes, HIDE_ERRORS) Then
                ErrMsg("The value for Minutes that was entered is invalid." & vbNewLine &
                       "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                       "Invalid Angle Format (Minutes)", flag)
                Return False
            End If

            If ((angleObj.iMinutes < 0) Or (angleObj.iMinutes > 59)) Then
                ErrMsg("The value entered for Minutes is out of range." & vbNewLine &
                       "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                       "Invalid Angle Format (Minutes)", flag)
                Return False
            End If
        End If

        ' Finally, get seconds if they were entered.
        If (posS > 0) Then
            posS = InStr(inputStr, "s")
            temp = Strings.Left(inputStr, posS - 1)
            If Not isValidReal(temp, angleObj.dSeconds, HIDE_ERRORS) Then
                ErrMsg("The value for Seconds that was entered is invalid." & vbNewLine &
                       "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                       "Invalid Angle Format (Seconds)", flag)
                Return False
            End If

            If Not ((angleObj.dSeconds >= 0.0) And (angleObj.dSeconds < 60.0)) Then
                ErrMsg("The value entered for Seconds is out of range." & vbNewLine &
                       "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                       "Invalid Angle Format (Seconds)", flag)
                Return False
            End If
        End If

        ' Handle the pathological case where the user didn't specify d, m, or s. Default to seconds
        If ((posD <= 0) And (posM <= 0) And (posS <= 0)) Then
            If Not isValidReal(inputStr, angleObj.dSeconds, HIDE_ERRORS) Then
                ErrMsg("The value for Seconds that was entered is invalid." & vbNewLine &
                       "Please enter an angle as a valid DMS (###d ##m ##.##s) value",
                       "Invalid Angle Format (Seconds)", flag)
                Return False
            End If
        End If

        angleObj.dDecValue = DMStoDec(angleObj.bNeg, angleObj.iDegrees, angleObj.iMinutes, angleObj.dSeconds)
        angleObj.bValid = True
        Return True
    End Function

    '********************************************************************************
    ' Define some general methods for converting an angle to a string format
    '********************************************************************************

    '*********************************************************
    ' Convert an angle object to DMS or decimal string format
    ' depending upon the formatFlag.
    '
    ' This function is overloaded to allow flexibility in
    ' how the angle is passed: as an object, as a decimal
    ' value, or by neg, d, m, s.
    '
    ' angleObj         angle object to be converted
    ' angle            angle as a decimal value
    ' neg              true if angle is negative
    ' d                degrees
    ' m                minutes
    ' s                seconds
    ' formatFlag       either DMSFORMAT or DECFORMAT
    '
    ' Returns angle as a string
    '*********************************************************
    Public Shared Function angleToStr(ByVal angleObj As ASTAngle, ByVal formatFlag As Boolean) As String
        If (formatFlag = DMSFORMAT) Then
            Return DMStoStr(angleObj)
        Else
            Return Format(angleObj.dDecValue, decDegFormat)
        End If
    End Function
    Public Shared Function angleToStr(ByVal angle As Double, ByVal formatFlag As Boolean) As String
        Return angleToStr(DecToDMS(angle), formatFlag)
    End Function
    Public Shared Function angleToStr(ByVal neg As Boolean, ByVal d As Integer, ByVal m As Integer,
                                      ByVal s As Double, ByVal formatFlag As Boolean) As String

        If (formatFlag = DMSFORMAT) Then
            Return DMStoStr(neg, d, m, s)
        Else
            Return Format(DMStoDec(neg, d, m, s), decHoursFormat)
        End If
    End Function

    '********************************************************************************
    ' Convert an angle to/from DMS and to/from decimal
    '********************************************************************************

    '****************************************************
    ' Convert a decimal angle to DMS format
    '
    ' decValue         decimal angle to be converted
    '
    ' Returns an angle object
    '****************************************************
    Public Shared Function DecToDMS(ByVal decValue As Double) As ASTAngle
        Dim angle As ASTAngle = New ASTAngle()
        Dim tmp As Double = Abs(decValue)

        angle.bValid = True
        angle.dDecValue = decValue
        angle.bNeg = (decValue < 0.0)
        angle.iDegrees = Trunc(tmp)
        angle.iMinutes = Trunc(Frac(tmp) * 60.0)
        angle.dSeconds = 60.0 * Frac(60 * Frac(tmp))
        Return angle
    End Function

    '***************************************************
    ' Convert individual DMS values to a decimal value.
    ' The neg flag is required because the integer
    ' degrees could be 0 and we may want -0d angles.
    '
    ' neg          true if the angle is negative
    ' d            degrees
    ' m            minutes
    ' s            seconds
    '
    ' Returns DMS value converted to a decimal value.
    '***************************************************
    Public Shared Function DMStoDec(ByVal neg As Boolean, ByVal d As Integer, ByVal m As Integer, ByVal s As Double) As Double
        Dim sign As Integer = 1

        If neg Then sign = -1
        Return sign * (d + CDbl(m / 60.0) + (s / 3600.0))
    End Function

    '--------------------------------------------------------------------------------
    ' Define some private methods used only in this class
    '--------------------------------------------------------------------------------

    '----------------------------------------------------
    ' Convert DMS format to a string.
    '
    ' This function is overloaded to allow the angle
    ' to be passed as an object or by neg, d, m, s.
    '
    ' angleObj     angle as an object
    ' neg          true if the angle is negative
    ' d            degrees
    ' m            minutes
    ' s            seconds
    '
    ' Returns the angle as a DMS string (xxd yym zz.zzs)
    '----------------------------------------------------
    ' Convert an angle object to a DMS string
    Private Shared Function DMStoStr(ByVal angleObj As ASTAngle) As String
        Return DMStoStr(angleObj.bNeg, angleObj.iDegrees, angleObj.iMinutes, angleObj.dSeconds)
    End Function
    Private Shared Function DMStoStr(ByVal neg As Boolean, ByVal d As Integer, ByVal m As Integer, ByVal s As Double) As String
        Dim result, str As String
        Dim n As Integer
        Dim x As Double

        ' Due to round off and truncation errors, build up the result starting with s, then m, then d and worry
        ' about m, and s values being out of bounds. (Allow d to be > 360 degrees) So, convert s to a string,
        ' convert the string back to a number, and adjust both s and m if required. Similarly, handle m and do d last.
        str = Format(s, "00.00")
        x = Double.Parse(str)
        If x >= 60.0 Then
            str = "00.00"
            m = m + 1
        End If
        result = str & "s"

        str = Format(m, "00")
        n = Integer.Parse(str)
        If n >= 60 Then
            str = "00"
            n = Abs(d) + 1
        Else
            n = Abs(d)
        End If
        result = str & "m " & result

        result = Format(n, "##0") & "d " & result
        If neg Then result = "-" & result
        Return result
    End Function

End Class