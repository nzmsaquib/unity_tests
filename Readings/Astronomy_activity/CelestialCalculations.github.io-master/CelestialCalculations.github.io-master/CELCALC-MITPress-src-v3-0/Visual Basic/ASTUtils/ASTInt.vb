﻿'***************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides a class for creating integer objects and validating
' that a string has a valid integer value.
'**************************************************************

Option Explicit On
Option Strict On

Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg

Public Class ASTInt

    '********************************************************************************
    ' Define some general methods for validating that a string has a valid integer
    ' in it.
    '********************************************************************************

    '*********************************************************
    ' Check to see if a valid integer was entered, but don't
    ' display any error messages unless flag is SHOW_ERRORS.
    '
    ' This function is overloaded to allow the flag for
    ' whether to display error messages to be optional.
    ' The default is to show error messages.
    '
    ' inputStr         string to be validated
    ' result           validated string as an integer
    ' flag             whether to display error messages
    '
    ' Returns true if string has a valid integer, otherwise
    ' false. Also returns string value as an integer in
    ' result if the string was valid.
    '*********************************************************
    Public Shared Function isValidInt(ByVal inputStr As String, ByRef result As Integer) As Boolean
        Return isValidInt(inputStr, result, SHOW_ERRORS)
    End Function
    Public Shared Function isValidInt(ByVal inputStr As String, ByRef result As Integer, ByVal flag As Boolean) As Boolean
        inputStr = Trim(inputStr)

        If Not Integer.TryParse(inputStr, result) Then
            ErrMsg("Either nothing was entered or what was entered" & vbNewLine & "is invalid. Please enter a valid integer.",
                   "Invalid Integer", flag)
            Return False
        End If

        Return True
    End Function

End Class
