﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ASTAboutBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ASTAboutBox))
        Me.BookTitlePanel = New System.Windows.Forms.Panel()
        Me.BookTitle = New System.Windows.Forms.Label()
        Me.PicInfoPanel = New System.Windows.Forms.Panel()
        Me.OKButton = New System.Windows.Forms.Button()
        Me.PicInfoLabel = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.AboutPicBox = New System.Windows.Forms.PictureBox()
        Me.BookCopyright = New System.Windows.Forms.Label()
        Me.BookEdition = New System.Windows.Forms.Label()
        Me.BookAuthor = New System.Windows.Forms.Label()
        Me.ChapterTitle = New System.Windows.Forms.Label()
        Me.BookTitlePanel.SuspendLayout()
        Me.PicInfoPanel.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.AboutPicBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BookTitlePanel
        '
        Me.BookTitlePanel.Controls.Add(Me.BookTitle)
        Me.BookTitlePanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.BookTitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.BookTitlePanel.Name = "BookTitlePanel"
        Me.BookTitlePanel.Size = New System.Drawing.Size(636, 55)
        Me.BookTitlePanel.TabIndex = 2
        '
        'BookTitle
        '
        Me.BookTitle.BackColor = System.Drawing.Color.Navy
        Me.BookTitle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BookTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BookTitle.ForeColor = System.Drawing.Color.White
        Me.BookTitle.Location = New System.Drawing.Point(0, 0)
        Me.BookTitle.Name = "BookTitle"
        Me.BookTitle.Size = New System.Drawing.Size(636, 55)
        Me.BookTitle.TabIndex = 1
        Me.BookTitle.Text = "Book Title"
        Me.BookTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PicInfoPanel
        '
        Me.PicInfoPanel.Controls.Add(Me.OKButton)
        Me.PicInfoPanel.Controls.Add(Me.PicInfoLabel)
        Me.PicInfoPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PicInfoPanel.Location = New System.Drawing.Point(0, 347)
        Me.PicInfoPanel.Name = "PicInfoPanel"
        Me.PicInfoPanel.Size = New System.Drawing.Size(636, 122)
        Me.PicInfoPanel.TabIndex = 3
        '
        'OKButton
        '
        Me.OKButton.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OKButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.OKButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OKButton.Location = New System.Drawing.Point(522, 73)
        Me.OKButton.Margin = New System.Windows.Forms.Padding(4)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(100, 33)
        Me.OKButton.TabIndex = 9
        Me.OKButton.Text = "&OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'PicInfoLabel
        '
        Me.PicInfoLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PicInfoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PicInfoLabel.Location = New System.Drawing.Point(0, 5)
        Me.PicInfoLabel.Name = "PicInfoLabel"
        Me.PicInfoLabel.Size = New System.Drawing.Size(279, 108)
        Me.PicInfoLabel.TabIndex = 0
        Me.PicInfoLabel.Text = "'Pillars of Creation' in the Eagle Nebula taken in 2014 through the Hubble Space " &
    "Telescope. (Image courtesy NASA/ESA/Hubble Heritage Team)"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 55)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.AboutPicBox)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.BookCopyright)
        Me.SplitContainer1.Panel2.Controls.Add(Me.BookEdition)
        Me.SplitContainer1.Panel2.Controls.Add(Me.BookAuthor)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ChapterTitle)
        Me.SplitContainer1.Size = New System.Drawing.Size(636, 292)
        Me.SplitContainer1.SplitterDistance = 279
        Me.SplitContainer1.TabIndex = 4
        '
        'AboutPicBox
        '
        Me.AboutPicBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.AboutPicBox.Image = CType(resources.GetObject("AboutPicBox.Image"), System.Drawing.Image)
        Me.AboutPicBox.Location = New System.Drawing.Point(0, 0)
        Me.AboutPicBox.Name = "AboutPicBox"
        Me.AboutPicBox.Size = New System.Drawing.Size(279, 292)
        Me.AboutPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.AboutPicBox.TabIndex = 0
        Me.AboutPicBox.TabStop = False
        '
        'BookCopyright
        '
        Me.BookCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BookCopyright.Location = New System.Drawing.Point(1, 36)
        Me.BookCopyright.Margin = New System.Windows.Forms.Padding(8, 0, 4, 0)
        Me.BookCopyright.Name = "BookCopyright"
        Me.BookCopyright.Size = New System.Drawing.Size(351, 33)
        Me.BookCopyright.TabIndex = 9
        Me.BookCopyright.Text = "Copyright"
        Me.BookCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BookEdition
        '
        Me.BookEdition.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BookEdition.Location = New System.Drawing.Point(1, 5)
        Me.BookEdition.Margin = New System.Windows.Forms.Padding(8, 0, 4, 0)
        Me.BookEdition.Name = "BookEdition"
        Me.BookEdition.Size = New System.Drawing.Size(351, 33)
        Me.BookEdition.TabIndex = 8
        Me.BookEdition.Text = "Edition"
        Me.BookEdition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BookAuthor
        '
        Me.BookAuthor.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BookAuthor.Location = New System.Drawing.Point(2, 119)
        Me.BookAuthor.Margin = New System.Windows.Forms.Padding(8, 0, 4, 0)
        Me.BookAuthor.Name = "BookAuthor"
        Me.BookAuthor.Size = New System.Drawing.Size(351, 35)
        Me.BookAuthor.TabIndex = 6
        Me.BookAuthor.Text = "Author"
        Me.BookAuthor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ChapterTitle
        '
        Me.ChapterTitle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ChapterTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChapterTitle.Location = New System.Drawing.Point(11, 165)
        Me.ChapterTitle.Name = "ChapterTitle"
        Me.ChapterTitle.Size = New System.Drawing.Size(330, 123)
        Me.ChapterTitle.TabIndex = 7
        Me.ChapterTitle.Text = "Chapter Title"
        Me.ChapterTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ASTAboutBox
        '
        Me.AcceptButton = Me.OKButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(636, 469)
        Me.ControlBox = False
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.PicInfoPanel)
        Me.Controls.Add(Me.BookTitlePanel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "ASTAboutBox"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "About ..."
        Me.BookTitlePanel.ResumeLayout(False)
        Me.PicInfoPanel.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.AboutPicBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BookTitlePanel As Windows.Forms.Panel
    Friend WithEvents PicInfoPanel As Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As Windows.Forms.SplitContainer
    Friend WithEvents AboutPicBox As Windows.Forms.PictureBox
    Friend WithEvents BookTitle As Windows.Forms.Label
    Friend WithEvents PicInfoLabel As Windows.Forms.Label
    Friend WithEvents BookAuthor As Windows.Forms.Label
    Friend WithEvents ChapterTitle As Windows.Forms.Label
    Friend WithEvents OKButton As Windows.Forms.Button
    Friend WithEvents BookEdition As Windows.Forms.Label
    Friend WithEvents BookCopyright As Windows.Forms.Label
End Class
