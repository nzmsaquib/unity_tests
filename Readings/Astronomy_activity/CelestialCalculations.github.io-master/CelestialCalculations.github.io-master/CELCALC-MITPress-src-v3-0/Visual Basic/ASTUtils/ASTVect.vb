﻿'**********************************************************
'                   Copyright (c) 2018
'                 Author: J. L. Lawrence
'
' This implements a simple class for handling vectors with
' 3 elements.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math

Public Class ASTVect

    ' Define a vector as x, y, z elements plus its length.
    Private Rx As Double
    Private Ry As Double
    Private Rz As Double
    Private Rmag As Double

    '************************************
    ' Create an instance.
    '
    ' x,y,z         vector components
    '************************************
    Sub New()
        Me.Rx = 0.0
        Me.Ry = 0.0
        Me.Rz = 0.0
        Me.Rmag = 0.0
    End Sub
    Sub New(ByVal x As Double, ByVal y As Double, ByVal z As Double)
        Me.Rx = x
        Me.Ry = y
        Me.Rz = z
        Me.Rmag = vectLen(x, y, z)
    End Sub

    '********************************************************************
    ' Define 'get' and 'set' accessors for the object's fields
    '********************************************************************
    Public Function x() As Double
        Return Me.Rx
    End Function
    Public Function y() As Double
        Return Me.Ry
    End Function
    Public Function z() As Double
        Return Me.Rz
    End Function
    Public Function len() As Double
        Return Me.Rmag
    End Function

    Public Sub setVect(ByVal x As Double, ByVal y As Double, ByVal z As Double)
        Me.Rx = x
        Me.Ry = y
        Me.Rz = z
        Me.Rmag = vectLen(x, y, z)
    End Sub

    '***********************************************
    ' Calculate the length/magnitude of a vector
    '
    ' x,y,z                     vector components
    '
    ' Returns the vector's length
    '***********************************************
    Public Shared Function vectLen(ByVal x As Double, ByVal y As Double, ByVal z As Double) As Double
        Return Sqrt(x ^ 2 + y ^ 2 + z ^ 2)
    End Function

End Class
