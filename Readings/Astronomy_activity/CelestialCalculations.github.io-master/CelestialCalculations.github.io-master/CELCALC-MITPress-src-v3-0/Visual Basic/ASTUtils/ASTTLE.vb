﻿'****************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides the ability to load TLEs
' from a data file and to store them in an accessible
' structure. Only one TLE data file can be
' loaded at a time.
'
' A TLE data file **MUST** be in the format
' specifically designed for this book. The format can be
' gleaned by opening the sample TLE-TABLE9.dat data file,
' which is under the data files directory, and examining
' its contents. The data file format is straightforward 
' and is documented in comments within the data file itself.
'****************************************************************

Option Explicit On
Option Strict On

Imports System.IO

Imports ASTUtils.ASTDate
Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr

Public Class ASTTLE

    ' Note: TLE data bases are likely fairly small, so they aren't sorted by this class.
    ' This means that any searches must be brute force through all the elements.

    ' file types typically associated with TLE data files in this program
    Private Const TLEdataPrompt = "Open a TLE data file ..."
    Private Const TLEdataFilename = "TLE-TABLE9.dat"            ' this is the default TLE data file
    Private Const TLEdataPathname = "..\Ast Data Files\"
    Private Const TLEdataExt = ".dat"
    Private Const TLEdataFilter = "TLE Data|*.*"

    Private Shared TLEsLoaded As Boolean = False
    Private Shared TLEsFilename As String = ""                  ' The currently loaded TLE data file

    ' Provide some padding to add to the data lines to avoid possible substring errors
    Private Const padding As String = "                                        "

    '----------------------------------------------------------------
    ' Define a private class and database for the raw TLE data lines
    '
    ' Note: We could also parse the data lines and store results,
    ' but for our purposes we can just calculate them when needed.
    '----------------------------------------------------------------
    Friend Class RawTLE
        Friend bNamed As Boolean                               ' true if set has a name
        Friend sName As String                                 ' optional name for 2 TLE lines
        Friend sLine1 As String
        Friend sLine2 As String
    End Class

    ' This array holds lines of TLE data
    Private Shared TLEdb As List(Of RawTLE) = New List(Of RawTLE)

    '********************************************************
    ' Define 'getter' methods to return information about
    ' the TLE database.
    '********************************************************
    Public Shared Function getTLEArgofPeri(ByVal idx As Integer, ByRef argofperi As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Argument of perigee is on the 2nd data line, columns 35-42
        strTmp = TLEdb(idx).sLine2 & padding
        strTmp = strTmp.Substring(34, 8).Trim()
        Return isValidReal(strTmp, argofperi, HIDE_ERRORS)
    End Function
    Public Shared Function getTLECatID(ByVal idx As Integer, ByRef CatID As String) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Cat ID is on the 1st data line, columns 3-7
        strTmp = TLEdb(idx).sLine1 & padding
        CatID = strTmp.Substring(2, 5).Trim()
        Return True
    End Function
    Public Shared Function getTLEEccentricity(ByVal idx As Integer, ByRef ecc As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Eccentricity is on the 2nd data line, columns 27-33
        ' with a leading decimal point assumed.
        strTmp = TLEdb(idx).sLine2 & padding
        strTmp = "0." & strTmp.Substring(26, 7).Trim()
        Return isValidReal(strTmp, ecc, HIDE_ERRORS)
    End Function
    Public Shared Function getTLEEpochDate(ByVal idx As Integer, ByRef epochDate As ASTDate) As Boolean
        Dim iYear, daysIntoYear As Integer
        Dim dT As Double

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        If Not getTLEEpochYear(idx, iYear) Then Return False
        If (iYear < 57) Then
            iYear = iYear + 2000
        Else
            iYear = iYear + 1900
        End If

        If Not getTLEEpochDay(idx, dT) Then Return False
        daysIntoYear = Trunc(dT)
        epochDate = daysIntoYear2Date(iYear, daysIntoYear)

        Return True
    End Function
    Public Shared Function getTLEEpochDay(ByVal idx As Integer, ByRef epochDay As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Epoch day is on the 1st data line, columns 21-32
        strTmp = TLEdb(idx).sLine1 & padding
        strTmp = strTmp.Substring(20, 12).Trim()
        Return isValidReal(strTmp, epochDay, HIDE_ERRORS)
    End Function
    Public Shared Function getTLEEpochUT(ByVal idx As Integer, ByRef epochUT As Double) As Boolean
        Dim epochDay As Double

        If ((idx < 0) Or (idx >= getNumTLEDBObjs())) Then Return False

        If Not getTLEEpochDay(idx, epochDay) Then Return False

        epochUT = (epochDay - Trunc(epochDay)) * 24.0

        Return True
    End Function
    Public Shared Function getTLEEpochYear(ByVal idx As Integer, ByRef epochYear As Integer) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Epoch year is on the 1st data line, columns 19-20
        strTmp = TLEdb(idx).sLine1 & padding
        strTmp = strTmp.Substring(18, 2).Trim()
        Return isValidInt(strTmp, epochYear, HIDE_ERRORS)
    End Function
    Public Shared Function getTLEInclination(ByVal idx As Integer, ByRef inclin As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Inclination is on the 2nd data line, columns 9-16
        strTmp = TLEdb(idx).sLine2 & padding
        strTmp = strTmp.Substring(8, 8).Trim()
        Return isValidReal(strTmp, inclin, HIDE_ERRORS)
    End Function
    Public Shared Function getTLEFilename() As String
        Return TLEsFilename
    End Function
    Public Shared Function getTLELine1(ByVal idx As Integer, ByRef line1 As String) As Boolean
        If ((idx < 0) Or (idx >= getNumTLEDBObjs())) Then Return False
        line1 = TLEdb(idx).sLine1
        Return True
    End Function
    Public Shared Function getTLELine2(ByVal idx As Integer, ByRef line2 As String) As Boolean
        If ((idx < 0) Or (idx >= getNumTLEDBObjs())) Then Return False
        line2 = TLEdb(idx).sLine2
        Return True
    End Function
    Public Shared Function getTLEMeanAnomaly(ByVal idx As Integer, ByRef M0 As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Mean anomaly is on the 2nd data line, columns 44-51
        strTmp = TLEdb(idx).sLine2 & padding
        strTmp = strTmp.Substring(43, 8).Trim()
        Return isValidReal(strTmp, M0, HIDE_ERRORS)
    End Function
    Public Shared Function getTLEMeanMotion(ByVal idx As Integer,
                                            ByRef MMotion As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' Mean motion is on the 2nd data line, columns 53-63
        strTmp = TLEdb(idx).sLine2 & padding
        strTmp = strTmp.Substring(52, 11).Trim()
        Return isValidReal(strTmp, MMotion, HIDE_ERRORS)
    End Function
    Public Shared Function getNumTLEDBObjs() As Integer
        Return TLEdb.Count
    End Function
    Public Shared Function getTLERAAN(ByVal idx As Integer, ByRef RAAN As Double) As Boolean
        Dim strTmp As String

        If (idx < 0) Or (idx >= getNumTLEDBObjs()) Then Return False

        ' RAAN is on the 2nd data line, columns 18-25
        strTmp = TLEdb(idx).sLine2 & padding
        strTmp = strTmp.Substring(17, 8).Trim()
        Return isValidReal(strTmp, RAAN, HIDE_ERRORS)
    End Function

    '********************************************************
    ' Public methods for manipulating the TLE database
    '********************************************************

    '*********************************************************
    ' Check to see if a TLE database has 
    ' been loaded.
    '
    ' Returns true if TLE db is loaded, else false
    '*********************************************************
    Public Shared Function areTLEsLoaded() As Boolean
        Return TLEsLoaded
    End Function

    '*********************************************
    ' Clear all the currently loaded TLE data
    '*********************************************
    Public Shared Sub clearTLEData()

        ' Delete all of the TLE data. In some languages, we'd need
        ' to loop through the database and and delete objects individually.
        TLEdb.Clear()
        TLEdb = New List(Of RawTLE)
        TLEsLoaded = False
        TLEsFilename = ""
    End Sub

    '*********************************************************
    ' Decode a TLE data set to show what its elements are
    '
    ' prt               area for printing results
    ' idx               index into TLE db for set to display,
    '                   assuming 0-based indexing!
    ' txt               text to display as printout heading
    '*********************************************************
    Public Shared Sub decodeTLE(ByVal prt As ASTPrt, ByVal idx As Integer, ByVal txt As String)
        Dim padding, strTmp As String

        padding = String.Format("{0,60:s}", " ")        ' In case TLE data lines are not long enough

        prt.setBoldFont(True)
        prt.println(txt, CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.setFixedWidthFont()
        displayTLEHeader(prt)
        displayTLEItem(prt, idx)

        ' Decode TLE data line 1
        strTmp = TLEdb(idx).sLine1 & padding
        prt.setBoldFont(True)
        prt.println("TLE Data Line 1")
        prt.setBoldFont(False)
        prt.println("Line ID (Always 1)    (Col 1)    : " & strTmp.Substring(0, 1))
        prt.println("Catlog Number         (Col 3-7)  : " & strTmp.Substring(2, 5))
        prt.println("Classification        (Col 8)    : " & strTmp.Substring(7, 1))
        prt.println("Launch Year           (Col 10-11): " & strTmp.Substring(9, 2))
        prt.println("Launch Number         (Col 12-14): " & strTmp.Substring(11, 3))
        prt.println("Piece of the Launch   (Col 15-17): " & strTmp.Substring(14, 3))
        prt.println("Epoch Year            (Col 19-20): " & strTmp.Substring(18, 2))
        prt.println("Epoch Day of Year     (Col 21-32): " & strTmp.Substring(20, 12))
        prt.println("1st Deriv Mean Motion (Col 34-43): " & strTmp.Substring(33, 10))
        prt.println("2nd Deriv Mean Motion (Col 45-52): " & strTmp.Substring(44, 8))
        prt.println("Drag Term             (Col 54-61): " & strTmp.Substring(53, 8))
        prt.println("Ephemeris Type        (Col 63)   : " & strTmp.Substring(62, 1))
        prt.println("Element Number        (Col 65-68): " & strTmp.Substring(64, 4))
        prt.println("Checksum              (Col 69)   : " & strTmp.Substring(68, 1))
        prt.println()

        strTmp = TLEdb(idx).sLine2 & padding
        prt.setBoldFont(True)
        prt.println("TLE Data Line 2")
        prt.setBoldFont(False)
        prt.println("Line ID (Always 2)    (Col 1)    : " & strTmp.Substring(0, 1))
        prt.println("Catlog Number         (Col 3-7)  : " & strTmp.Substring(2, 5))
        prt.println("Inclination (degrees) (Col 9-16) : " & strTmp.Substring(8, 8))
        prt.println("RAAN (degrees)        (Col 18-25): " & strTmp.Substring(17, 8))
        prt.println("Eccentricity          (Col 27-33): " & strTmp.Substring(26, 7))
        prt.println("Arg of Perigee (deg)  (Col 35-42): " & strTmp.Substring(34, 8))
        prt.println("Mean Anomaly (deg)    (Col 44-51): " & strTmp.Substring(43, 8))
        prt.println("Mean Motion (rev/day) (Col 53-63): " & strTmp.Substring(52, 11))
        prt.println("Num of Orbits         (Col 64-68): " & strTmp.Substring(63, 5))
        prt.println("Checksum              (Col 69)   : " & strTmp.Substring(68, 1))
        prt.println()

        prt.setProportionalFont()
    End Sub

    '****************************************
    ' Display a header for the TLE data
    '
    ' prt           area to display results
    '****************************************
    Public Shared Sub displayTLEHeader(ByVal prt As ASTPrt)
        prt.println("TLE            1         2         3         4         5         6")
        prt.println("Set   123456789012345678901234567890123456789012345678901234567890123456789")
        prt.println(String.Format("{0,76:s}", "=").Replace(" ", "="))
    End Sub

    '*******************************************
    ' Display a single TLE data set
    '
    ' prt       area in which to display results
    ' idx       which data set to display
    '********************************************
    Public Shared Sub displayTLEItem(ByVal prt As ASTPrt, ByVal idx As Integer)
        If TLEdb(idx).bNamed Then prt.println("      " & TLEdb(idx).sName)
        prt.print(String.Format("{0,-5:d} ", idx + 1))
        prt.println(TLEdb(idx).sLine1)
        prt.println("      " & TLEdb(idx).sLine2)
    End Sub

    '***************************************************************************
    ' Search the currently loaded TLE database and return an index into the
    ' database for the requested catalog ID. An exact match is required. Note
    ' that there may be several items with the same name, so this routine
    ' must be invoked until all are found. Also note that the catalog ID is
    ' an integer, but we'll do a string search rather than converting the
    ' raw TLE data to an integer. The catalog ID is characters 3-7 on both
    ' TLE line 1 and line 2.
    '
    ' catID                 catalog ID of the object to find
    ' iStart                where to start searching in the db
    '
    ' If successful, returns an index into the currently loaded TLE database
    ' for the object requested. If unsuccessful, -1 is returned. Note that
    ' this assumes 0-based indexing!
    '***************************************************************************
    Public Shared Function findTLECatID(ByVal catID As String, ByVal iStart As Integer) As Integer
        Dim i As Integer
        Dim targ, strTmp As String

        If (iStart >= TLEdb.Count()) Then Return -1

        targ = removeWhitespace(catID)
        For i = iStart To TLEdb.Count() - 1
            ' We can use either Line1 or Line2 - no need to check both
            ' as they had better be the same catID! Since VB starts
            ' indexing at 0, we want the substring that starts at
            ' 2 and goes for 5 characters
            strTmp = TLEdb(i).sLine1.Substring(2, 5)
            If String.Compare(targ, strTmp) = 0 Then Return i
        Next

        Return -1
    End Function

    '**************************************************************************
    ' Search the currently loaded TLE database and return an index into the
    ' database for the requested object. The search performed is **not**
    ' case sensitive, and a partial match is counted as successful. Note
    ' that there may be several items with the same name, so this routine
    ' must be invoked until all are found.
    '
    ' name                  name of the object to find
    ' iStart                where to start searching in the db
    '
    ' If successful, returns an index into the currently loaded TLE database
    ' for the object requested. If unsuccessful, -1 is returned. Note that
    ' this assumes 0-based indexing!
    '**************************************************************************
    Public Shared Function findTLEName(ByVal name As String, ByVal iStart As Integer) As Integer
        Dim i As Integer
        Dim targ, strTmp As String

        If (iStart >= TLEdb.Count()) Then Return -1

        targ = LCase(removeWhitespace(name))
        For i = iStart To TLEdb.Count() - 1
            If TLEdb(i).bNamed Then
                strTmp = LCase(removeWhitespace(TLEdb(i).sName))
                If InStr(strTmp, targ) <> 0 Then Return i
            End If
        Next

        Return -1
    End Function

    '****************************************************************
    ' Extract the Keplerian elements and epoch from a TLE
    ' data set.
    '
    ' idx               which data set to use (0-based indexing)
    ' epochDate         calendar date for the epoch
    ' epochUT           UT time for the epoch
    ' inclin            inclination
    ' ecc               eccentricity
    ' RAAN              RAAN
    ' argofperi         argument of perigee
    ' M0                mean anomaly
    ' MMotion           mean motion
    '
    ' Returns true if extraction was successful and returns
    ' date elements in the appropriate variables
    '****************************************************************
    Public Shared Function getKeplerianElementsFromTLE(ByVal idx As Integer,
                                                       ByRef epochDate As ASTDate,
                                                       ByRef epochUT As Double,
                                                       ByRef inclin As Double,
                                                       ByRef ecc As Double,
                                                       ByRef RAAN As Double,
                                                       ByRef argofperi As Double,
                                                       ByRef M0 As Double,
                                                       ByRef MMotion As Double) As Boolean
        If ((idx < 0) Or (idx >= getNumTLEDBObjs())) Then
            ErrMsg("No TLE Data Set number " & idx & " exists", "Invalid Data Set Number")
            Return False
        End If

        ' Validate epoch year and epoch UT
        If Not getTLEEpochDate(idx, epochDate) Then
            ErrMsg("Invalid Epoch Year in TLE Data Set", "Invalid Epoch Year")
            Return False
        End If
        If Not getTLEEpochUT(idx, epochUT) Then
            ErrMsg("Invalid Epoch Day of Year in TLE Data Set", "Invalid Epoch Day of Year")
            Return False
        End If

        ' Validate Keplerian elements
        If Not getTLEInclination(idx, inclin) Then
            ErrMsg("Invalid Inclination in TLE Data Set", "Invalid Inclination")
            Return False
        End If
        If Not getTLERAAN(idx, RAAN) Then
            ErrMsg("Invalid RAAN in TLE Data Set", "Invalid RAAN")
            Return False
        End If
        If Not getTLEEccentricity(idx, ecc) Then
            ErrMsg("Invalid Eccentricity in TLE Data Set", "Invalid Eccentricity")
            Return False
        End If
        If Not getTLEArgofPeri(idx, argofperi) Then
            ErrMsg("Invalid Argument of Perigee in TLE Data Set", "Invalid Arg of Perigee")
            Return False
        End If
        If Not getTLEMeanAnomaly(idx, M0) Then
            ErrMsg("Invalid Mean Anomaly in TLE Data Set", "Invalid Mean Anomaly")
            Return False
        End If
        If Not getTLEMeanMotion(idx, MMotion) Then
            ErrMsg("Invalid Mean Motion in TLE Data Set", "Invalid Mean Motion")
            Return False
        End If

        Return True
    End Function

    '******************************************
    ' Load TLEs database from disk
    '
    ' Returns true if successful, else false.
    '******************************************
    Public Shared Function loadTLEData() As Boolean
        Dim fChooser As ASTFileIO
        Dim fullFilename As String = ""
        Dim br As StreamReader
        Dim c As Char
        Dim i As Integer = -1
        Dim count As Integer = 0
        Dim strIn, name, line1, line2 As String
        Dim named As Boolean = False
        Dim readError As Boolean = False

        clearTLEData()

        fChooser = New ASTFileIO(TLEdataPrompt, TLEdataFilter, TLEdataExt)

        If Not fChooser.showFileBrowser(fullFilename) Then Return False

        ' See if the file exists
        If Not doesFileExist(fullFilename) Then
            ErrMsg("The TLE data file specified does not exist", "TLE Data File Does Not Exist")
            Return False
        End If

        ' Open and read in the data file
        br = New System.IO.StreamReader(fullFilename)

        ' Validate that this is a TLE data file
        If IsNothing(readTillStr(br, "<TLEs>", HIDE_ERRORS)) Then
            ErrMsg("The specified file does not appear to" & vbNewLine &
                   "be a valid TLE data file - try again.", "Invalid TLE Date File")
            br.Close()
            Return False
        End If

        ' Look for the beginning of the data section, then cycle through
        ' and extract each TLE data line.
        If IsNothing(readTillStr(br, "<Data>")) Then
            ErrMsg("The specified file does not appear to" & vbNewLine &
                   "be a valid TLE data file - try again.", "Invalid TLE Date File")
            br.Close()
            Return False
        End If

        Do While br.Peek() <> -1
            name = ""
            named = False
            count = count + 1
            readError = False
            strIn = getNonBlankLine(br, readError)
            If readError Then Exit Do

            strIn = Trim(strIn)
            If InStr(LCase(strIn), "</data>") > 0 Then Exit Do

            ' See if this line is an object name
            c = strIn.Chars(0)
            If (c <> "1") And (c <> "2") Then
                name = strIn
                named = True
                strIn = getNonBlankLine(br, readError)
                If readError Then Exit Do
                strIn = Trim(strIn)
            End If

            ' If there's any error, may as well quit because we'll get out of synch
            ' with the fact that line 1 must precede a line 2
            c = strIn.Chars(0)
            If (c <> "1"c) Then
                readError = True
                Exit Do
            Else
                line1 = strIn
            End If

            strIn = getNonBlankLine(br, readError)
            If readError Then Exit Do
            strIn = Trim(strIn)
            c = strIn.Chars(0)
            If (c <> "2"c) Then
                readError = True
                Exit Do
            Else
                line2 = strIn
            End If

            i = i + 1
            TLEdb.Add(New RawTLE)
            TLEdb(i).sName = name
            TLEdb(i).bNamed = named
            TLEdb(i).sLine1 = line1
            TLEdb(i).sLine2 = line2
        Loop

        br.Close()

        If Not readError Then
            TLEsLoaded = True
            TLEsFilename = fullFilename
        End If

        Return TLEsLoaded
    End Function
End Class

