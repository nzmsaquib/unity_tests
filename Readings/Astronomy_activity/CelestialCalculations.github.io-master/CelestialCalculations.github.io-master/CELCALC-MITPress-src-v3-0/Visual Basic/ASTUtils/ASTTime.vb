﻿'*******************************************************************************
'                               Copyright (c) 2018
'                             Author: J. L. Lawrence
'
' Provides a class for time objects and time-related methods, including
' validating strings in the form h:m:s.s where h is required but m and s.s
' are optional.
'
' The code allows for the possibility that h is negative as well as situations
' where the time is negative but the integer hours is 0 (e.g., -0:13:14.45).
' Time objects maintain the time in both HMS and decimal format so that either
' format can be readily obtained directly from the object.
' 
' Different programming languages have time-related routines, but they are not
' used here because (a) we need to allow time to be negative and (b) implementing
' the time-related routines needed makes it easier to translate to a different
' programming language.
'*******************************************************************************

Option Explicit On
Option Strict On
Imports System.Math

Imports ASTUtils.ASTDate
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle

Public Class ASTTime
    ' Class instance variables
    Private bValid As Boolean           ' true if the object contains a valid time
    Private bNeg As Boolean             ' true if the time is negative
    Private dDecValue As Double         ' decimal value for the time
    Private iHours As Integer
    Private iMinutes As Integer
    Private dSeconds As Double

    '***********************************************************
    ' Creates and initializes a new time object. Since much of
    ' the usage of this class involves validating that a string
    ' has a valid time, assume a new object is invalid until it
    ' is proven otherwise.
    '***********************************************************
    Sub New()
        bValid = False
        bNeg = False
        dDecValue = 0.0
        iHours = 0
        iMinutes = 0
        dSeconds = 0.0
    End Sub
    Sub New(ByVal decValue As Double)
        Me.setDecTime(decValue)
    End Sub

    '**********************************************************
    ' Define 'get' accessors for the object's fields
    '**********************************************************
    Public Function isValidTimeObj() As Boolean
        Return Me.bValid
    End Function
    Public Function isNegTime() As Boolean
        Return Me.bNeg
    End Function
    Public Function getDecTime() As Double
        Return Me.dDecValue
    End Function
    Public Function getHours() As Integer
        Return Me.iHours
    End Function
    Public Function getMinutes() As Integer
        Return Me.iMinutes
    End Function
    Public Function getSeconds() As Double
        Return Me.dSeconds
    End Function

    '********************************************************************
    ' Define 'set' accessors for a time object's fields
    '********************************************************************
    Public Sub setDecTime(ByVal decValue As Double)
        Dim tmp As Double

        tmp = Abs(decValue)

        Me.bValid = True
        Me.bNeg = (decValue < 0.0)
        Me.dDecValue = decValue
        Me.iHours = Trunc(tmp)
        Me.iMinutes = Trunc(Frac(tmp) * 60.0)
        Me.dSeconds = 60.0 * Frac(60 * Frac(tmp))
    End Sub

    '********************************************************************
    ' Define a method for returning the current time. This is
    ' easy to do, but is provided as a method to ease portability to
    ' other languages.
    '********************************************************************
    Public Shared Function getCurrentTime() As String
        Return TimeString()
    End Function

    '***********************************************************************************
    ' Define some general methods for validating that a string contains a valid time
    '***********************************************************************************

    '*******************************************************
    ' Checks a string to see if it contains a valid decimal
    ' format value. Don't display any error messages unless
    ' flag is SHOW_ERRORS.
    '
    ' This function is overloaded to allow flag to be an
    ' optional parameter. The default is to display error
    ' messages.
    '
    ' inputStr         string to validate
    ' timeObj          validated string as a time object
    ' flag             whether to display error messages
    '
    ' Returns true if the string has a valid time in it.
    ' Also returns the time itself in timeObj.
    '*******************************************************
    Public Shared Function isValidDecTime(ByVal inputStr As String, ByRef timeObj As ASTTime) As Boolean
        Return isValidDecTime(inputStr, timeObj, SHOW_ERRORS)
    End Function
    Public Shared Function isValidDecTime(ByVal inputStr As String, ByRef timeObj As ASTTime, ByVal flag As Boolean) As Boolean

        timeObj.bValid = False              ' assume invalid till we parse and prove otherwise

        inputStr = removeWhitespace(inputStr)
        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter time as a valid decimal value", "Invalid Time Format", flag)
            Return False
        End If

        If isValidReal(inputStr, timeObj.dDecValue, HIDE_ERRORS) Then
            timeObj = DecToHMS(timeObj.dDecValue)
            If (timeObj.iHours > 23) Then
                ErrMsg("The Hours value entered is not a valid value." & vbNewLine &
                       "Please enter a valid decimal value", "Invalid Time Format (Hours)", flag)
                Return False
            End If
        Else
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter time as a valid decimal value", "Invalid Time Format", flag)
            Return False
        End If

        timeObj.bValid = True
        Return True
    End Function

    '**********************************************************
    ' Checks a string to see if contains a valid HMS value.
    ' The string format is h:m:s.s where h is required but m
    ' and s.s are optional. Do not display any error message
    ' if flag is HIDE_ERRORS to let the calling routine display
    ' its own messages.
    '
    ' This function is overloaded to allow flag to be an
    ' optional parameter. The default is to display error
    ' messages.
    '
    ' inputStr         string to validate
    ' timeObj          validated string as a time object
    ' flag             whether to display error messages
    '
    ' Returns true if the string has a valid time in it.
    ' Also returns the time itself in timeObj.
    '
    ' The custom code here is needed because we want to allow for
    ' the possibility that h could be negative, and to allow both
    ' minutes and seconds to be optional. Also note that to
    ' handle a situation where the time is negative but h is 0
    ' (e.g., -0:13:15.45), the bNeg field is required.
    '**********************************************************
    Public Shared Function isValidHMSTime(ByVal inputStr As String, ByVal timeObj As ASTTime) As Boolean
        Return isValidHMSTime(inputStr, timeObj, SHOW_ERRORS)
    End Function
    Public Shared Function isValidHMSTime(ByVal inputStr As String, ByRef timeObj As ASTTime, ByVal flag As Boolean) As Boolean
        Dim parts() As String           ' This will contain the tokens extracted from inputStr

        timeObj.bValid = False          ' assume invalid till we parse and prove otherwise

        inputStr = removeWhitespace(inputStr)

        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format", flag)
            Return False
        End If

        If (inputStr.Substring(0, 1) = "-") Then timeObj.bNeg = True
        parts = Split(inputStr, ":")

        ' Ubound is 1 less than the number of tokens since Visual Basic starts array indices at 0
        If UBound(parts) > 2 Then
            ErrMsg("The HMS value entered is not in a valid format." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format", flag)
            Return False
        End If

        ' Get hours, which must have been entered if we get this far
        If isValidInt(parts(0), timeObj.iHours, HIDE_ERRORS) Then
            timeObj.iHours = Abs(timeObj.iHours)      ' timeObj.bNeg will contain the sign
        Else
            ErrMsg("The Hours value entered is not a valid value." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format (Hours)", flag)
            Return False
        End If
        If (timeObj.iHours > 23) Then
            ErrMsg("The Hours value entered is not a valid value." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format (Hours)", flag)
            Return False
        End If

        ' Get minutes, if they were entered
        If UBound(parts) < 1 Then           ' if true, neither m or s were entered, but still need to set dec value
            timeObj.dDecValue = HMStoDec(timeObj.bNeg, timeObj.iHours, 0, 0.0)
            timeObj.bValid = True
            Return True
        End If

        If Not isValidInt(parts(1), timeObj.iMinutes, HIDE_ERRORS) Then
            ErrMsg("The Minutes value entered is not a valid value." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format (Minutes)", flag)
            Return False
        End If
        If ((timeObj.iMinutes > 59) Or (timeObj.iMinutes < 0)) Then
            ErrMsg("The Minutes value entered is not a valid value." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format (Minutes)", flag)
            Return False
        End If

        ' Get seconds, if they were entered
        If UBound(parts) < 2 Then                ' if true, s was not entered
            timeObj.dDecValue = HMStoDec(timeObj.bNeg, timeObj.iHours, timeObj.iMinutes, 0.0)
            timeObj.bValid = True
            Return True
        End If

        If Not isValidReal(parts(2), timeObj.dSeconds, HIDE_ERRORS) Then
            ErrMsg("The Seconds value entered is not a valid value." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format (Seconds)", flag)
            Return False
        End If
        If ((timeObj.dSeconds >= 60.0) Or (timeObj.dSeconds < 0.0)) Then
            ErrMsg("The Seconds value entered is not a valid value." & vbNewLine &
                   "Please enter a valid time in the form hh:mm:ss.ss", "Invalid HMS Format (Seconds)", flag)
            Return False
        End If

        timeObj.dDecValue = HMStoDec(timeObj.bNeg, timeObj.iHours, timeObj.iMinutes, timeObj.dSeconds)
        timeObj.bValid = True
        Return True
    End Function

    '********************************************************
    ' Validate a string that may be in either HMS or decimal
    ' format. Do not display any error messages unless flag
    ' is SHOW_ERRORS.
    '
    ' This function is overloaded to allow flag to be an
    ' optional parameter. The default is to display error
    ' messages.
    '
    ' inputStr         string to be validated
    ' timeObj          time object to hold the result
    ' flag             whether to display error messages
    '
    ' Returns true if the string has a valid time in it.
    ' Returns the time itself in timeObj.
    '********************************************************
    Public Shared Function isValidTime(ByVal inputStr As String, ByRef timeObj As ASTTime) As Boolean
        Return isValidTime(inputStr, timeObj, SHOW_ERRORS)
    End Function
    Public Shared Function isValidTime(ByVal inputStr As String, ByRef timeObj As ASTTime, ByVal flag As Boolean) As Boolean
        Dim bValid As Boolean = False

        inputStr = removeWhitespace(inputStr)
        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter time as a valid decimal or HMS (hh:mm:ss.ss) value", "Invalid Time Format", flag)
            Return False
        End If

        If (InStr(inputStr, ":") > 0) Then
            bValid = isValidHMSTime(inputStr, timeObj, flag)
        Else
            bValid = isValidDecTime(inputStr, timeObj, flag)
        End If

        Return bValid
    End Function

    '***********************************************************************************
    ' Define some methods to convert time to a string
    '***********************************************************************************

    '*************************************************
    ' Convert time to an HMS or decimal string format
    ' depending upon the formatFlag, which can be
    ' HMSFORMAT or DECFORMAT. This function is
    ' overloaded to allow passing time as an object,
    ' as a decimal value, or by its individual
    ' neg,h,m,s values.
    '
    ' This function is overloaded to allow the time
    ' to be passed as a decimal value, as an object,
    ' or by neg, h, m, s.
    '
    ' decTime      decimal value to be converted
    ' timeObj      time object to be converted
    ' neg          true if the time is negative
    ' h            integer hours
    ' m            integer minutes
    ' s            decimal seconds
    ' formatFlag   what format to return
    '
    ' Returns a string with the converted time.
    '*************************************************
    Public Shared Function timeToStr(ByVal decTime As Double, ByVal formatFlag As Boolean) As String
        Return timeToStr(DecToHMS(decTime), formatFlag)
    End Function
    Public Shared Function timeToStr(ByVal timeObj As ASTTime, ByVal formatFlag As Boolean) As String
        If (formatFlag = HMSFORMAT) Then
            Return HMStoStr(timeObj)
        Else
            Return Format(timeObj.dDecValue, decHoursFormat)
        End If
    End Function
    Public Shared Function timeToStr(ByVal neg As Boolean, ByVal h As Integer, ByVal m As Integer,
                                     ByVal s As Double, ByVal formatFlag As Boolean) As String

        Dim decValue As Double

        If (formatFlag = HMSFORMAT) Then
            Return HMStoStr(neg, h, m, s)
        Else
            decValue = HMStoDec(neg, h, m, s)
            Return Format(decValue, decHoursFormat)
        End If
    End Function

    '*************************************************************************************
    ' Methods to convert time to/from HMS and to/from decimal
    '*************************************************************************************

    '***********************************************
    ' Convert a decimal time to an ASTTime object
    '
    ' decValue     decimal value to be converted
    '
    ' Returns a time object.
    '***********************************************
    Public Shared Function DecToHMS(ByVal decValue As Double) As ASTTime
        Dim timeObj As ASTTime = New ASTTime()
        Dim tmp As Double = Abs(decValue)

        timeObj.bValid = True
        timeObj.dDecValue = decValue
        timeObj.bNeg = (decValue < 0.0)
        timeObj.iHours = Trunc(tmp)
        timeObj.iMinutes = Trunc(Frac(tmp) * 60.0)
        timeObj.dSeconds = 60.0 * Frac(60.0 * Frac(tmp))

        Return timeObj
    End Function

    '********************************************
    ' Convert HMS values to decimal
    '
    ' neg      true if the time is negative
    ' h        integer hours (always non-negative)
    ' m        integer minutes
    ' s        decimal seconds
    '
    ' Returns the time as a decimal value
    '********************************************
    Public Shared Function HMStoDec(ByVal neg As Boolean, ByVal h As Integer, ByVal m As Integer, ByVal s As Double) As Double
        Dim result As Double = CDbl(h) + (CDbl(m) / 60.0) + (s / 3600.0)

        If (neg) Then result = -result

        Return result
    End Function

    '************************************************************************************
    ' These methods perform various time conversions
    '************************************************************************************

    '************************************************
    ' Adjust the date by some number of days
    '
    ' dateObj           original date
    ' days              number of days by which to
    '                   adjust the date
    '
    ' Returns adjusted date
    '************************************************
    Public Shared Function adjustDatebyDays(ByVal dateObj As ASTDate, ByVal days As Double) As ASTDate
        Dim JD As Double

        If days = 0 Then Return dateObj

        JD = dateToJD(dateObj) + days
        Return JDtoDate(JD)
    End Function

    '************************************************
    ' Convert GST to LST
    '
    ' GST          GST time to convert
    ' dLongitude   observer's longitude
    '
    ' Returns LST time for the given GST & longitude
    '************************************************
    Public Shared Function GSTtoLST(ByVal GST As Double, ByVal dLongitude As Double) As Double
        Dim LST As Double

        LST = GST + (dLongitude / 15.0)

        If (LST < 0.0) Then LST = LST + 24.0
        If (LST > 24.0) Then LST = LST - 24.0

        Return LST
    End Function

    '************************************************
    ' Convert GST to UT
    '
    ' GST          GST time to convert
    ' dateObj      date for which UT is desired
    '
    ' Returns UT time for the given GST & date
    '************************************************
    Public Shared Function GSTtoUT(ByVal GST As Double, ByVal dateObj As ASTDate) As Double
        Dim UT As Double
        Dim JD, JD0, dT, dR, dB, dA, T0 As Double
        Dim days As Integer

        ' must use iDay rather than dDay because dDay may contain time of day
        ' which will throw the results off
        JD = dateToJD(dateObj.getMonth, CDbl(dateObj.getiDay), dateObj.getYear)
        JD0 = dateToJD(1, 0, dateObj.getYear)

        days = CType(JD - JD0, Integer)
        dT = (JD0 - 2415020.0) / 36525.0
        dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
        dB = 24.0 - dR + 24.0 * (dateObj.getYear - 1900)
        T0 = 0.0657098 * (days) - dB
        If (T0 < 0) Then T0 = T0 + 24

        dA = GST - T0
        If (dA < 0) Then dA = dA + 24
        UT = dA * 0.99727
        Return UT
    End Function

    '***********************************************
    ' Convert LCT to UT
    '
    ' LCT           time to convert
    ' DST           true if on daylight saving time
    ' timeZone      observer's time zone
    ' lon           observer's longitude
    ' dateAdjust    amount by which to adjust date
    '
    ' Returns UT and also dateAdjust
    '***********************************************
    Public Shared Function LCTtoUT(ByVal LCT As Double, ByVal DST As Boolean, ByVal timeZone As TimeZoneType,
                                   ByVal lon As Double, ByRef dateAdjust As Integer) As Double
        Dim UT As Double = LCT

        dateAdjust = 0      ' amount to adjust date by if the conversion results in next or prev day

        ' Adjust for daylight saving time
        If DST Then UT = LCT - 1

        UT = UT - timeZoneAdjustment(timeZone, lon)
        If (UT > 24) Then
            UT = UT - 24.0
            dateAdjust = 1
        ElseIf (UT < 0) Then
            UT = UT + 24.0
            dateAdjust = -1
        End If

        Return UT
    End Function

    '**************************************
    ' Convert LST to GST
    '
    ' LST          LST time to convert
    ' dLongitude   observer's longitude
    '
    ' Returns the LST converted to GST.
    '**************************************
    Public Shared Function LSTtoGST(ByVal LST As Double, ByVal dLongitude As Double) As Double
        Dim GST As Double

        GST = LST - (dLongitude / 15.0)
        If (GST < 0) Then
            GST = GST + 24
        End If
        If (GST > 24) Then
            GST = GST - 24
        End If

        Return GST
    End Function

    '************************************************
    ' Convert UT to GST
    '
    ' UT           UT time to convert
    ' dateObj      date as an object
    '
    ' Returns GST time for the given UT & date
    '************************************************
    Public Shared Function UTtoGST(ByVal UT As Double, ByVal dateObj As ASTDate) As Double
        Dim GST, JD, JD0, dDays, dT, dR, dB, T0 As Double

        ' must use iDay rather than dDay because dDay may contain time of day
        ' which will throw the results off
        JD = dateToJD(dateObj.getMonth, CDbl(dateObj.getiDay), dateObj.getYear)
        JD0 = dateToJD(1, 0.0, dateObj.getYear())           ' JD for Jan 0.0
        dDays = JD - JD0                                    ' # days since Jan 0.0
        dT = (JD0 - 2415020.0) / 36525.0
        dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
        dB = 24.0 - dR + 24.0 * (dateObj.getYear() - 1900)
        T0 = 0.0657098 * (dDays) - dB
        GST = T0 + UT * 1.002738

        If (GST < 0.0) Then GST = GST + 24.0
        If (GST > 24.0) Then GST = GST - 24.0

        Return GST
    End Function

    '************************************************
    ' Convert UT to LCT
    '
    ' UT            time to convert
    ' DST           true if on daylight saving time
    ' timeZone      observer's time zone
    ' lon           observer's longitude
    ' dateAdjust    amount by which to adjust date
    '
    ' Returns LCT and also dateAdjust
    '************************************************
    Public Shared Function UTtoLCT(ByVal UT As Double, ByVal DST As Boolean, ByVal timeZone As TimeZoneType,
                                   ByVal lon As Double, ByRef dateAdjust As Integer) As Double

        Dim LCT As Double

        dateAdjust = 0      ' amount to adjust date by if the conversion results in next or prev day

        LCT = UT + timeZoneAdjustment(timeZone, lon)
        If LCT > 24 Then            ' next day
            LCT = LCT - 24.0
            dateAdjust = 1
        End If
        If LCT < 0 Then             ' previous day
            LCT = LCT + 24.0
            dateAdjust = -1
        End If

        If DST Then LCT = LCT + 1

        Return LCT
    End Function

    '------------------------------------------------------------------
    ' Private routines used only in this class
    '------------------------------------------------------------------

    '-------------------------------------------------
    ' Convert time to an HMS-formatted string.
    '
    ' This function is overloaded to allow time to be
    ' passed as an object or as neg, h, m, s.
    '
    ' timeObj      time object to be converted
    ' neg          true if time is negative
    ' h            integer hours
    ' m            integer minutes
    ' s            decimal seconds
    '
    ' Returns time as a string
    '-------------------------------------------------
    Private Shared Function HMStoStr(ByVal timeObj As ASTTime) As String
        Return HMStoStr(timeObj.bNeg, timeObj.iHours, timeObj.iMinutes, timeObj.dSeconds)
    End Function
    Private Shared Function HMStoStr(ByVal neg As Boolean, ByVal h As Integer, ByVal m As Integer, ByVal s As Double) As String
        Dim result, str As String
        Dim n As Integer
        Dim x As Double

        ' Due to round off and truncation errors, build up the result starting with s, then m, then h and worry
        ' about h, m, and s values being out of bounds. So, convert s to a string, convert the string back to a
        ' number, and adjust both s and m if required. Similarly, handle m and do h last.
        str = Format(s, "00.00")
        x = Double.Parse(str)
        If x >= 60.0 Then
            str = "00.00"
            m = m + 1
        End If
        result = str

        str = Format(m, "00")
        n = Integer.Parse(str)
        If n >= 60 Then
            str = "00"
            n = Abs(h) + 1
        Else
            n = Abs(h)
        End If
        result = str & ":" & result

        result = Format(n, "#0") & ":" & result
        If neg Then result = "-" & result
        Return result
    End Function

End Class
