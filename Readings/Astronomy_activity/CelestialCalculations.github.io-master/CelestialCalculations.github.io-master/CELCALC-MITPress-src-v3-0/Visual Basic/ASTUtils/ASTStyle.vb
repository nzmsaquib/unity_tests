﻿'*******************************************************************
'                       Copyright (c) 2018
'                     Author: J. L. Lawrence
'
' This class defines various items to help standardize items
' across the programs, particularly GUI items. Some items,
' such as fonts, are not provided here but must be set
' individually in the Visual Basic form editor because it
' is too difficult to modify the code the form editor
' generates and keep it in synch with the designer.
'*******************************************************************

Option Explicit On
Option Strict On

Public Class ASTStyle
    '********************************************************************
    ' Items for the GUI areas with which a user will interact
    '********************************************************************
    ' Proportional font
    Public Const OUT_TEXT_FONT = "Microsoft Sans Serif"

    ' Fixed width font
    Public Const FW_OUT_TEXT_FONT = "Courier New"

    '*********************************************************************
    ' Define format strings for various items to provide consistency
    ' across programs
    '*********************************************************************
    Public Const mVFormat = "##0.00"                ' format for visual magnitude
    Public Const decDegFormat = "0.000000"          ' Decimal degrees
    Public Const decHoursFormat = "0.000000"        ' decimal hours
    Public Const temperatureFormat = "0.00"         ' Temperature (C and F)
    Public Const JDFormat = "########0.00###"       ' Julian day number format
    Public Const latlonFormat = "##0.00000"         ' latitude/longitude format
    Public Const epochFormat = "#0.0"               ' format for Epoch
    Public Const genFloatFormat = "#0.0#####"       ' General format for real numbers
    Public Const genFloatFormat8 = "#0.0#######"    ' General format for real numbers
End Class
