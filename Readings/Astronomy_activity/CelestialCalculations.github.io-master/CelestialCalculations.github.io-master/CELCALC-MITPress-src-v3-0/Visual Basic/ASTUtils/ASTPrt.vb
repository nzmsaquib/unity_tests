﻿'**************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class implements methods that print to a
' scrollable text area that the invoking routine
' sets up. The methods are collected into a
' class to standardize behavior across the programs.
'**************************************************************

Option Explicit On
Option Strict On

Imports System.Drawing
Imports System.Windows.Forms

Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTStyle

Public Class ASTPrt
    Public Const CENTERTXT = True                   ' Center text when it is printed

    ' Save a reference to the invoking routine's scrollable text area. This is saved when the
    ' calling routine creates a class instance.
    Private outputTxtBox As RichTextBox = Nothing

    ' newFont is a temporary used when we need to change font characteristics. In the code below,
    ' before creating a new font we dispose of the old one to avoid memory leaks.
    Private newFont As Font = Nothing

    ' Flags to make code below more readable
    Private Const SET_PROP_FONT = True
    Private Const SET_FW_FONT = Not SET_PROP_FONT
    Private Const SET_BOLD = True
    Private Const SET_ITALIC = Not SET_BOLD

    '***************************************************************************
    ' Create an output box
    '
    ' outBox       output box area in the GUI for where to display output
    '***************************************************************************
    Sub New(ByVal outBox As RichTextBox)
        'Since we must create an object instance and we must set outputTxtBox, there's no need
        'to check outputTxtBox for null in the rest of this class
        If IsNothing(outBox) Then
            CriticalErrMsg("An output text area cannot be null. Aborting program ...")
            Application.Exit()
        End If

        Me.outputTxtBox = outBox
        Me.outputTxtBox.SelectionAlignment = HorizontalAlignment.Left
        Me.outputTxtBox.Cursor = Cursors.No
        Me.clearTextArea()
        Me.resetCursor()
    End Sub

    '************************************************************************************
    ' Define various methods to write text to the caller's scrollable text box area
    '************************************************************************************

    '***************************************************************************
    ' Set the font to be a fixed width font
    '***************************************************************************
    Public Sub setFixedWidthFont()
        setFontFamily(SET_FW_FONT)
    End Sub

    '***************************************************************************
    ' Set the font to be a proportional font
    '***************************************************************************
    Public Sub setProportionalFont()
        setFontFamily(SET_PROP_FONT)
    End Sub

    '***************************************************************************
    ' Set the font to be, or not be, bold
    '
    ' boldFont         true if font is to be set to bold
    '***************************************************************************
    Public Sub setBoldFont(ByVal boldFont As Boolean)
        setFontStyle(SET_BOLD, boldFont)
    End Sub

    '***************************************************************************
    ' Set the font to be, or not be, italicized
    '
    ' italicFont       true if font is to be italicized
    '***************************************************************************
    Public Sub setItalicFont(ByVal italicFont As Boolean)
        setFontStyle(SET_ITALIC, italicFont)
    End Sub

    '***************************************************************************
    ' Clear the caller's text output area
    '***************************************************************************
    Public Sub clearTextArea()
        Me.outputTxtBox.Clear()
        Me.resetCursor()
        Me.outputTxtBox.SelectionAlignment = HorizontalAlignment.Left
        Me.setProportionalFont()
        Me.setBoldFont(False)
        Me.setItalicFont(False)
    End Sub

    '***************************************************************************
    ' Outputs text to the caller's scrollable output area and optionally
    ' centers it.
    '
    ' This method is overloaded to allow center parameter to be optional.
    ' The default is not to center the text.
    '
    ' txt          text to write to the scrollable output area
    ' center       true if text is to be centered in the output area
    '***************************************************************************
    Public Sub print(ByVal txt As String)
        Me.print(txt, Not CENTERTXT)
    End Sub
    Public Sub print(ByVal txt As String, ByVal center As Boolean)
        ' Must save current font and reset at the end because printing a newline
        ' resets the font attributes back to whatever the form default is
        Dim curFont As Font = Me.outputTxtBox.SelectionFont
        If (center = CENTERTXT) Then
            Me.outputTxtBox.SelectionAlignment = HorizontalAlignment.Center
        Else
            Me.outputTxtBox.SelectionAlignment = HorizontalAlignment.Left
        End If
        Me.outputTxtBox.AppendText(txt)
        Me.outputTxtBox.SelectionFont = curFont
    End Sub

    '*****************************************************************************
    ' Outputs text and a newline to the caller's scrollable output area,
    ' and optionally centers it.
    '
    ' This method is overloaded to allow center and txt to be optional parameters.
    ' The default it to not center the text. If no text is passed in, the action
    ' is simply to print a newline to the output area.
    '
    ' txt          text to write to the scrollable output area
    ' center       true if text is to be centered in the output area
    '*****************************************************************************
    Public Sub println()
        print(vbNewLine)
    End Sub
    Public Sub println(ByVal txt As String)
        println(txt, Not CENTERTXT)
    End Sub
    Public Sub println(ByVal txt As String, ByVal center As Boolean)
        ' Must save current font and reset at the end because printing a newline
        ' resets the font attributes back to whatever the form default is
        Dim curFont As Font = Me.outputTxtBox.SelectionFont
        print(txt & vbNewLine, center)
        Me.outputTxtBox.SelectionFont = curFont
    End Sub

    '***************************************************************************
    ' Resets the cursor to the beginning of the scrollable text area.
    ' This method is intended to be used at the end of a lengthy series
    ' of prints so that the scrollable list presented to the user will be
    ' at the beginning of the list, not the end.
    '***************************************************************************
    Public Sub resetCursor()
        Me.outputTxtBox.SelectionStart = 0
        Me.outputTxtBox.ScrollToCaret()
    End Sub

    '--------------------------------------------------------------------------
    ' Private methods used only in this class
    '--------------------------------------------------------------------------
    Private Sub setCursorAtEnd()
        ' Do nothing because insert position is always at the end.
        ' This method is included to simplify porting to other languages
        ' where the cursor/caret has to be explicitly set.
    End Sub

    '----------------------------------------------------
    ' Set font to either proportional or fixed width
    '
    ' propFont      true if setting proportional font,
    '               false if setting fixed width font
    '----------------------------------------------------
    Private Sub setFontFamily(ByVal propFont As Boolean)
        'Position the cursor to the end of the text so that effect is only from that point forward.
        Me.setCursorAtEnd()
        Dim curFont As Font = Me.outputTxtBox.SelectionFont
        Dim fontName As String

        If (propFont) Then
            fontName = OUT_TEXT_FONT
        Else
            fontName = FW_OUT_TEXT_FONT
        End If

        If Not IsNothing(newFont) Then
            newFont.Dispose()
            newFont = Nothing
        End If

        If (curFont.Bold) Then
            If (curFont.Italic) Then
                newFont = New Font(fontName, curFont.Size, FontStyle.Italic Or FontStyle.Bold)
            Else
                newFont = New Font(fontName, curFont.Size, FontStyle.Bold)
            End If
        Else
            If (curFont.Italic) Then
                newFont = New Font(fontName, curFont.Size, FontStyle.Italic)
            Else
                newFont = New Font(fontName, curFont.Size, FontStyle.Regular)
            End If
        End If
        outputTxtBox.SelectionFont = newFont
    End Sub

    '------------------------------------------------------------------
    ' Set font style to be either bold or italic
    '
    ' setBold       true if this affects bold style, false if it
    '               affects italic style
    ' setFlag       true if set the style, false if not set the style
    '------------------------------------------------------------------
    Private Sub setFontStyle(ByVal setBold As Boolean, ByVal setFlag As Boolean)
        Dim curFont As Font = Me.outputTxtBox.SelectionFont
        Dim boolToCheck As Boolean
        Dim style1, style2 As FontStyle

        If (setBold) Then
            boolToCheck = curFont.Italic
            style1 = FontStyle.Bold
            style2 = FontStyle.Italic
        Else
            boolToCheck = curFont.Bold
            style1 = FontStyle.Italic
            style2 = FontStyle.Bold
        End If

        Me.setCursorAtEnd()

        If Not IsNothing(newFont) Then
            newFont.Dispose()
            newFont = Nothing
        End If

        If (setFlag) Then
            If (boolToCheck) Then
                newFont = New Font(curFont.FontFamily, curFont.Size, FontStyle.Italic Or FontStyle.Bold)
            Else
                newFont = New Font(curFont.FontFamily, curFont.Size, style1)
            End If
        Else
            If (boolToCheck) Then
                newFont = New Font(curFont.FontFamily, curFont.Size, style2)
            Else
                newFont = New Font(curFont.FontFamily, curFont.Size, FontStyle.Regular)
            End If
        End If
        outputTxtBox.SelectionFont = newFont
    End Sub
End Class