﻿'****************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides the ability to load launch sites
' from a data file and to store them in an accessible
' structure. Only one launch sites data file can
' be loaded at a time.
'
' A launch site data file **MUST** be in the format
' specifically designed for this book. The format can be
' gleaned by opening the sample Launch-Sites.dat data file,
' which is under the data files directory, and examining
' its contents. The data file format Is straightforward 
' and is documented in comments within the data file itself.
'****************************************************************

Option Explicit On
Option Strict On

Imports System.IO

Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr

Public Class ASTSites

    ' Note: Launch site data bases are likely fairly small, so they aren't sorted by this class.
    ' This means that any searches must be brute force through all the sites.

    ' file types typically associated with Launch Site data files in this program
    Private Const SitesdataPrompt = "Open a Launch Sites data file ..."
    Private Const SitesdataFilename = "Launch-Sites.dat"            ' this is the default sites data file
    Private Const SitesdataPathname = "..\Ast Data Files\"
    Private Const SitesdataExt = ".dat"
    Private Const SitesdataFilter = "Launch Sites Data|*.*"

    Private Shared SitesLoaded As Boolean = False
    Private Shared SitesFilename As String = ""                  ' The currently loaded Sites data file

    '-----------------------------------------------------------
    ' Define a private class and database for the Launch sites
    '-----------------------------------------------------------
    Friend Class LaunchSite
        Friend sName As String
        Friend dLat As Double
    End Class

    ' This array holds launch sites data
    Private Shared SitesDB As List(Of LaunchSite) = New List(Of LaunchSite)

    '********************************************************
    ' Define 'getter' methods to return information about
    ' the Launch Sites database.
    '
    ' Warning: No check is made to be sure idx is valid.
    '********************************************************
    Public Shared Function getSitesFilename() As String
        Return SitesFilename
    End Function
    Public Shared Function getSiteLat(ByVal idx As Integer) As Double
        Return SitesDB(idx).dLat
    End Function
    Public Shared Function getSiteName(ByVal idx As Integer) As String
        Return SitesDB(idx).sName
    End Function
    Public Shared Function getNumSites() As Integer
        Return SitesDB.Count
    End Function

    '*************************************************************
    ' Public methods for manipulating the Launch Sites database
    '*************************************************************

    '*********************************************************
    ' Check to see if a Launch Sites database has 
    ' been loaded.
    '
    ' Returns true if Launch Sites db is loaded, else false
    '*********************************************************
    Public Shared Function areSitesLoaded() As Boolean
        Return SitesLoaded
    End Function

    '*********************************************************
    ' Clear all the currently loaded Launch Sites data
    '*********************************************************
    Public Shared Sub clearLaunchSites()
        ' Delete all of the Launch Sites data. In some languages, we'd need
        ' to loop through the database and and delete objects individually.
        SitesDB.Clear()
        SitesDB = New List(Of LaunchSite)
        SitesLoaded = False
        SitesFilename = ""
    End Sub

    '******************************************************************************
    ' Search the currently loaded sites database and return an index into the
    ' database for the requested site. The search performed is **not**
    ' case sensitive, and a partial match is counted as successful.
    '
    ' name                  name of the site to find
    '
    ' If successful, returns an index into the currently loaded sites database
    ' for the object requested. If unsuccessful, -1 is returned. Note that
    ' this assumes 0-based indexing!
    '******************************************************************************
    Public Shared Function findSiteName(ByVal name As String) As Integer
        Dim i As Integer
        Dim targ, strTmp As String

        If Not areSitesLoaded() Then Return -1

        targ = LCase(removeWhitespace(name))
        For i = 0 To getNumSites() - 1
            strTmp = LCase(removeWhitespace(getSiteName(i)))
            If InStr(strTmp, targ) <> 0 Then Return i
        Next

        Return -1
    End Function

    '***************************************************************
    ' Loads Launch Sites database from disk.
    '
    ' Returns true if successful, else false.
    '***************************************************************
    Public Shared Function loadLaunchSites() As Boolean
        Dim fChooser As ASTFileIO
        Dim fullFilename As String = ""
        Dim br As StreamReader
        Dim i As Integer = 0
        Dim strIn, name As String
        Dim dLat As Double
        Dim readError As Boolean = True

        clearLaunchSites()

        fChooser = New ASTFileIO(SitesdataPrompt, SitesdataFilter, SitesdataExt)

        If Not fChooser.showFileBrowser(fullFilename) Then Return False

        ' See if the file exists
        If Not doesFileExist(fullFilename) Then
            ErrMsg("The Launch Sites data file specified does not exist", "Sites Data File Does Not Exist")
            Return False
        End If

        ' Open and read in the data file
        br = New System.IO.StreamReader(fullFilename)

        ' Validate that this is a Launch Sites data file
        If IsNothing(readTillStr(br, "<LaunchSites>", HIDE_ERRORS)) Then
            ErrMsg("The specified file does not appear to" & vbNewLine &
                   "be a valid Launch Sites data file - try again.", "Invalid Sites Data File")
            br.Close()
            Return False
        End If

        ' Look for the beginning of the data section, then cycle through
        ' and extract each launch site.
        If IsNothing(readTillStr(br, "<Data>")) Then
            ErrMsg("The specified file does not appear to" & vbNewLine &
                   "be a valid Launch Sites data file - try again.", "Invalid Launch Sites Data File")
            br.Close()
            Return False
        End If

        Do While br.Peek() <> -1
            name = ""
            dLat = 0.0
            readError = False
            strIn = getNonBlankLine(br, readError)
            If readError Then Exit Do

            strIn = Trim(strIn)
            If InStr(LCase(strIn), "</data>") > 0 Then Exit Do

            If String.Compare(strIn, "<Site>", True) <> 0 Then
                ErrMsg("Expected <Site> tag but found '" & strIn & "' instead", "Invalid Tag")
                readError = True
                Exit Do
            End If

            strIn = getSimpleTaggedValue(br, "<Name>", readError)
            If readError Then
                ErrMsg("Expected <Name> tag but found '" & strIn & "' instead", "Missing <Name> tag")
                Exit Do
            Else
                name = strIn.Trim()
            End If

            strIn = removeWhitespace(getSimpleTaggedValue(br, "<Lat>", readError))
            If readError Or (Not isValidReal(strIn, dLat, HIDE_ERRORS)) Then
                readError = True
                ErrMsg("Invalid latitude for site " & i + 1 & vbNewLine &
                       "at string [" & strIn & "]", "Invalid site latitude")
                Exit Do
            End If

            SitesDB.Add(New LaunchSite)
            SitesDB(i).sName = name
            SitesDB(i).dLat = dLat
            i = i + 1

            ' Read and throw away the ending site tag
            strIn = getNonBlankLine(br, readError)
            If readError Then Exit Do
        Loop

        br.Close()

        If Not readError Then
            SitesLoaded = True
            SitesFilename = fullFilename
        End If

        Return SitesLoaded
    End Function

End Class
