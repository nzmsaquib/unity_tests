﻿'*************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides an "About Box" for the programs.
'*************************************************************

Option Explicit On
Option Strict On
Imports System.Windows.Forms

Imports ASTUtils.ASTBookInfo

Public Class ASTAboutBox
    Sub New(ByVal ChapterTitle As String)
        ' Since this is a form, InitializeComponent is required.
        InitializeComponent()
        Me.BookTitle.Text = BOOK_TITLE
        Me.BookEdition.Text = BOOK_EDITION
        Me.BookCopyright.Text = BOOK_COPYRIGHT
        Me.BookAuthor.Text = BOOK_AUTHOR
        Me.ChapterTitle.Text = ChapterTitle & vbNewLine & vbNewLine & "(S/W Version " & CODE_VER & ")"
    End Sub

    ' Handle closing the about box
    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

    ' Display the About Box and center it on the parent
    Public Sub showAboutBox()
        Me.StartPosition = FormStartPosition.CenterParent
        Me.ShowDialog()
    End Sub

End Class