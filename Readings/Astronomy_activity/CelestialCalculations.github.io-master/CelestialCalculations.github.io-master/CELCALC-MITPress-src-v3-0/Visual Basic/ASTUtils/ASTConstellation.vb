﻿'*****************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides useful information about the 88 modern
' constellations, including their boundaries, a method for
' determining what constellation a given RA/Decl falls within,
' and other useful methods for accessing constellation information.
' All data and methods are declared shared because it doesn't make
' sense to have multiple constellation databases in an application.
'
' Unless otherwise noted, RA is given in hours, Decl is given in
' degrees, and both coordinates are w.r.t. Epoch 2000.0.
'*************************************************************

Option Explicit On
Option Strict On

Imports System.IO
Imports System.Math

Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTTime

Public Class ASTConstellation
    ' define the class instance variables that will contain the data for an individual constellation
    Private sName As String                         ' the constellation's name
    Private sAbbrevName As String                   ' the constellation's international 3-character abbreviation
    Private sMeaning As String                      ' what the constellation's name means (e.g., Andromeda means 'The Chained Maiden')
    Private dCenterRA As Double                     ' right ascension for the center of the constellation
    Private dCenterDecl As Double                   ' declination for the center of the constellation.
    Private sBrightestStar As String                ' name of the brightest star in the constellation
    Private dStarRA As Double                       ' right ascension for the brightest star in the constellation
    Private dStarDecl As Double                     ' declination for the brightest star in the constellation

    Private Const NUM_CONSTELLATIONS = 88           ' # of modern constellations
    ' we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
    Private Shared prt As ASTPrt = Nothing

    ' Name of the data file containing the constellation data, its location,
    ' and defaults in case the user must find it.
    Private Const dataPrompt = "Open Constellations Data File ..."
    Private Const dataFilename = "ConstellationData.dat"
    Private Const dataPathname = "..\Ast Data Files\"
    Private Const dataExt = ".dat"
    Private Const dataFilter = "Constellation Data|*.*"

    ' avoid initializing more than once
    Private Shared constDBReadIn As Boolean = False

    ' The only access to the constellations database (constDB) that is allowed external
    ' to this class is to get elements within the database, which is provided through 
    ' 'get' accessors defined below. The constellation database is an array of the
    ' class instance variables.
    Private Shared constDB(NUM_CONSTELLATIONS - 1) As ASTConstellation

    '*********************************************************************
    ' Initialize a new instance. The first time is to read in the
    ' constellations data and save it. Every instance after that is a
    ' single constellation object added to the constDB array.
    '
    ' prtInstance           instance for the user's scrollable text area
    '*********************************************************************
    Sub New()
        ' do nothing
    End Sub
    Sub New(ByVal prtInstance As ASTPrt)
        initConstellations(prtInstance)
    End Sub

    '*******************************************************************************
    ' Define 'get' accessors. There is no need for 'set' accessors because only the
    ' methods in this class set the class instance variables. Note that the
    ' constellations are stored as an array, so a 'get' must specify which
    ' constellation is desired from that array. 0-based indexing is assumed!
    '*******************************************************************************
    Public Shared Function getNumConstellations() As Integer
        Return NUM_CONSTELLATIONS
    End Function
    Public Shared Function getConstName(ByVal idx As Integer) As String
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return Nothing
        Else
            Return constDB(idx).sName
        End If
    End Function
    Public Shared Function getConstAbbrevName(ByVal idx As Integer) As String
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return Nothing
        Else
            Return constDB(idx).sAbbrevName
        End If
    End Function
    Public Shared Function getConstMeaning(ByVal idx As Integer) As String
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return Nothing
        Else
            Return constDB(idx).sMeaning
        End If
    End Function
    Public Shared Function getConstCenterRA(ByVal idx As Integer) As Double
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return UNKNOWN_RA_DECL
        Else
            Return constDB(idx).dCenterRA
        End If
    End Function
    Public Shared Function getConstCenterDecl(ByVal idx As Integer) As Double
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return UNKNOWN_RA_DECL
        Else
            Return constDB(idx).dCenterDecl
        End If
    End Function
    Public Shared Function getConstBrightestStarName(ByVal idx As Integer) As String
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return Nothing
        Else
            Return constDB(idx).sBrightestStar
        End If
    End Function
    Public Shared Function getConstBrightestStarRA(ByVal idx As Integer) As Double
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return UNKNOWN_RA_DECL
        Else
            Return constDB(idx).dStarRA
        End If
    End Function
    Public Shared Function getConstBrightestStarDecl(ByVal idx As Integer) As Double
        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            Return UNKNOWN_RA_DECL
        Else
            Return constDB(idx).dStarDecl
        End If
    End Function

    '--------------------------------------------------------------------------------------------
    ' Define an internal class and database for boundaries data, which define the boundaries
    ' for a constellation. The boundaries database can't be part of constDB because boundaries
    ' are not sorted in the same order as the constellations database. The boundaries database
    ' is sorted in a very specific way to make it easier to find what constellation a given
    ' RA/Decl falls within. Note that all RA/Decl coordinates in the boundaries database are
    ' defined w.r.t. the <b>1875.0 Epoch</b>, not J2000.0.
    '--------------------------------------------------------------------------------------------
    Friend Class BoundaryData
        Private dRALower As Double                  ' Lower RA boundary for a constellation
        Private dRAUpper As Double                  ' Upper RA boundary for this constellation
        Private dDecl As Double                     ' Declination boundary for this constellation
        Private idx As Integer                      ' Index into the constDB for this boundary

        '-------------------------------
        ' Define 'get' accessor methods
        '-------------------------------
        Friend Function getRALower() As Double
            Return Me.dRALower
        End Function
        Friend Function getRAUpper() As Double
            Return Me.dRAUpper
        End Function
        Friend Function getDecl() As Double
            Return Me.dDecl
        End Function
        Friend Function getidx() As Integer
            Return Me.idx
        End Function

        '-------------------------------
        ' Define 'set' accessor methods
        '-------------------------------
        Friend Sub setRALower(ByVal RA As Double)
            Me.dRALower = RA
        End Sub
        Friend Sub setRAUpper(ByVal RA As Double)
            Me.dRAUpper = RA
        End Sub
        Friend Sub setDecl(ByVal Decl As Double)
            Me.dDecl = Decl
        End Sub
        Friend Sub setidx(ByVal i As Integer)
            Me.idx = i
        End Sub
    End Class

    '--------------------------------------------------------------------------
    ' Define some other data items that are used only in this class.
    '--------------------------------------------------------------------------

    ' Various constants used in the precession and findConstellation methods
    Private Const CDR As Double = 0.017453292519943
    Private Const CONVH As Double = 0.2617993878
    Private Const CONVD As Double = 0.01745329251994
    Private Const PI4 As Double = 6.28318530717948
    Private Const E75 As Double = 1875.0
    Private Const RA_IDX As Integer = 0
    Private Const DECL_IDX As Integer = 1

    ' Create a dynamic array for a database of constellation boundaries
    Private Shared boundariesDB(0) As BoundaryData

    '*************************************************************************************************************
    ' Public methods for getting/displaying/manipulating constellation info
    '*************************************************************************************************************

    '***********************************************************
    ' Gets whether the constellations database has been loaded.
    '
    ' Returns true if constellations have been loaded.
    '***********************************************************
    Public Shared Function areConstellationsLoaded() As Boolean
        Return constDBReadIn
    End Function

    '*******************************************
    ' Displays a list of all the constellations
    ' in the user's scrollable output text area
    '*******************************************
    Public Shared Sub displayAllConstellations()
        Dim i As Integer

        checkInitConstellationsDone()

        prt.println(String.Format("{0,-20:s} {1,5:s} {2,-30:s} {3,-25:s}", "Constellation Name", "Abrv", "   Meaning", "Brightest Star"))
        prt.println(String.Format("{0,90:s}", "=").Replace(" ", "="))
        For i = 0 To NUM_CONSTELLATIONS - 1
            prt.println(String.Format("{0,-20:s} {1,5:s} {2,-30:s} {3,-25:s}", getConstName(i), getConstAbbrevName(i),
                                      getConstMeaning(i), getConstBrightestStarName(i)))

        Next
    End Sub

    '********************************************************
    ' Displays data for a constellation.
    '
    ' idx           index into constDB for the constellation
    '               to display. 0-based	indexing is assumed!
    '********************************************************
    Public Shared Sub displayConstellation(ByVal idx As Integer)
        Dim i, n As Integer

        checkInitConstellationsDone()

        If (idx < 0) Or (idx > NUM_CONSTELLATIONS - 1) Then
            prt.println("Constellation does not exist")
            Exit Sub
        End If

        prt.setBoldFont(True)
        prt.println(getConstName(idx) & " (" & getConstAbbrevName(idx) & ")", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("The Constellation's name means '" & getConstMeaning(idx) & "'")
        prt.print("Its center is located at " & timeToStr(getConstCenterRA(idx), HMSFORMAT) & " RA, ")
        prt.println(angleToStr(getConstCenterDecl(idx), DMSFORMAT) & " Decl (Epoch 2000.0)")
        prt.println()

        prt.print("Its Brightest Star (" & getConstBrightestStarName(idx) & ") ")
        prt.print("is located at " & timeToStr(getConstBrightestStarRA(idx), HMSFORMAT) & " RA, ")
        prt.println(angleToStr(getConstBrightestStarDecl(idx), DMSFORMAT) & " Decl (Epoch 2000.0)")
        prt.println()

        prt.println("The Constellation's boundaries (for Epoch 1875.0) are:")
        prt.setFixedWidthFont()

        prt.println(String.Format("{0,-12:s} {1,-12:s} {2,-16:s}", "   Lower RA", "   Upper RA", "       Decl"))
        prt.println(String.Format("{0,43:s}", "=").Replace(" ", "="))

        For i = 0 To UBound(boundariesDB) - 1
            n = boundariesDB(i).getidx()
            If n = idx Then
                prt.println(String.Format("{0,12:s} {1,12:s} {2,16:s}", timeToStr(boundariesDB(i).getRALower(), HMSFORMAT),
                                          timeToStr(boundariesDB(i).getRAUpper, HMSFORMAT),
                                          angleToStr(boundariesDB(i).getDecl, DMSFORMAT)))
            End If
        Next
        prt.setProportionalFont()
    End Sub

    '***********************************************************************
    ' Searches the constellations database and returns the index into 
    ' the database for the requested constellation by its abbreviated name.
    ' The search performed is **not** case sensitive, but an exact match,
    ' ignoring white space, is required.
    '
    ' targ                  Abbreviated name of the constellation to find
    '
    ' If successful, returns an index into the constellations database for
    ' the constellation. If unsuccessful, -1 is returned. Note that this
    ' assumes 0-based indexing!
    '***********************************************************************
    Public Shared Function findConstellationByAbbrvName(ByVal targ As String) As Integer
        Dim targStr As String
        Dim i As Integer

        checkInitConstellationsDone()

        targStr = removeWhitespace(targ)
        For i = 0 To NUM_CONSTELLATIONS - 1
            ' In Visual Basic, the 'True' parameter means to ignore case
            If String.Compare(targStr, getConstAbbrevName(i), True) = 0 Then Return i
        Next

        Return -1           ' not found
    End Function

    '****************************************************************
    ' Searches the constellations database and returns an index into
    ' the database for all constellations that contain the target
    ' substring in their 'meaning' field. The search performed is not
    ' case sensitive.
    '
    ' targ              target substring to search for
    ' result            dynamic array of integers that will contain
    '                   the indices into the constellations database
    '                   for the requested constellations.
    '
    ' If unsuccessful, the list is empty and a -1 is returned. If
    ' successful, return the number of entries - 1 that contained the
    ' target string. 0-based indexing is assumed for the indices!
    '****************************************************************
    Public Shared Function findConstellationsByMeaning(ByVal targ As String,
                            ByRef result() As Integer) As Integer

        Dim n As Integer = -1
        Dim i As Integer
        Dim targStr, tmpStr As String

        checkInitConstellationsDone()

        targStr = LCase(removeWhitespace(targ))
        ReDim result(0)

        For i = 0 To NUM_CONSTELLATIONS - 1
            tmpStr = LCase(removeWhitespace(getConstMeaning(i)))
            If InStr(tmpStr, targStr) <> 0 Then
                n = n + 1
                If (n > 0) Then ReDim Preserve result(UBound(result) + 1)
                result(n) = New Integer
                result(n) = i
            End If
        Next

        Return n
    End Function

    '***********************************************************************
    ' Searches the constellations database and returns the index into 
    ' the database for the requested constellation by its name.
    ' The search performed is **not** case sensitive, but an exact match,
    ' ignoring white space, is required.
    '
    ' targ                  Constellation name to find
    '
    ' If successful, returns an index into the constellations database for
    ' the constellation. If unsuccessful, -1 is returned. Note that this
    ' assumes 0-based indexing!
    '***********************************************************************
    Public Shared Function findConstellationByName(ByVal targ As String) As Integer
        Dim targStr As String
        Dim i As Integer

        checkInitConstellationsDone()

        targStr = targ.Trim()

        For i = 0 To NUM_CONSTELLATIONS - 1
            ' In Visual Basic, the 'TRUE' parameter means to ignore case
            If String.Compare(targStr, getConstName(i), True) = 0 Then Return i
        Next

        Return -1           ' not found
    End Function

    '**************************************************************************************
    ' Find the constellation that a particular RA/Decl falls within.
    '
    ' The code for this method was converted from the C and Fortran
    ' code found in the VizieR archives, http://cdsarc.u-strasbg.fr/viz-bin/Cat?VI/42. The
    ' code is translated somewhat literally, taking into account that array indexing in
    ' Visual BASIC is 0 based whereas Fortran is not.
    '
    ' RAIn              right ascension in decimal hours (actually, hour angle)
    ' DeclIn            declination in decimal degrees
    ' EpochIn			epoch in which the RAIn/DeclIn are given
    '
    ' Returns an index into the constellations database for the constellation in which the
    ' RA/Decl falls, or -1 if there is a problem
    '**************************************************************************************
    Public Shared Function findConstellationFromCoord(ByVal RAIn As Double, ByVal DeclIn As Double,
                                                      ByVal EpochIn As Double) As Integer

        ' The author of the C program from which this translation came used a macro to adjust arrays
        ' (e.g., translate X[i] to x[i-1]) to take the indexing base into account. The C author also
        ' used macros to define the trig functions (e.g., #define DCOS	cos). Neither of these 
        ' conventions are preserved in the translation below. A major alteration to the original algorithm is that
        ' the boundariesDB (boundaries database) is memory resident rather than constantly
        ' opening and reading a datafile containing the constellation boundaries as
        ' the original program did.

        Dim ARAD, DRAD, A, D, RAH, RA, DEC, RAL, RAU, DECL, DECD As Double
        Dim precResult(1) As Double             ' RA is index RA_IDX, Decl is index DECL_IDX
        Dim iConstOut As Integer = -1
        Dim idx, i As Integer

        checkInitConstellationsDone()

        RAH = RAIn
        DECD = DeclIn

        ' Convert to radians and then precess the position to the 1875.0 Epoch
        ARAD = CONVH * RAH
        DRAD = CONVD * DECD
        precResult = HGTPrecession(ARAD, DRAD, EpochIn, E75)
        A = precResult(RA_IDX)
        D = precResult(DECL_IDX)
        If (A < 0.0) Then A = A + PI4
        If (A >= PI4) Then A = A - PI4

        ' Convert radians back to degrees
        RA = A / CONVH
        DEC = D / CONVD

        ' Now find the constellation such that the Declination entered is higher than
        ' the lower boundary of the constellation when the upper and lower
        ' Right Ascensions for the constellation bound the entered
        ' Right Ascension

        For i = 0 To UBound(boundariesDB)
            RAL = boundariesDB(i).getRALower
            RAU = boundariesDB(i).getRAUpper
            DECL = boundariesDB(i).getDecl

            If (DECL > DEC) Then Continue For
            If (RAU <= RA) Then Continue For
            If (RAL > RA) Then Continue For

            ' If the constellation has been found, save the result and continue
            ' to the next boundariesDB entry. Otherwise continue the search by
            ' returning to RAU
            If (RA >= RAL) And (RA < RAU) And (DECL <= DEC) Then
                idx = boundariesDB(i).getidx()
                iConstOut = idx
            ElseIf RAU < RA Then
                Continue For
            Else
                ErrMsg("Constellation not found for RA/DECL" & RAIn & "/" & DeclIn, "Constellation not found")
            End If
            Exit For
        Next

        Return iConstOut

    End Function

    '----------------------------------------------------------------------------------------------
    ' Private methods used only in this class.
    '----------------------------------------------------------------------------------------------

    '----------------------------------------------------------------------------------
    ' Checks to see if this class has been properly initialized. This must
    ' be done to be sure none of the shared procedures (e.g., displayAllConstellations)
    ' is invoked before the class has been initialized. This method doesn't
    ' return anything. It displays an error message and aborts the running
    ' program if there is a problem.
    '----------------------------------------------------------------------------------
    Private Shared Sub checkInitConstellationsDone()
        If Not constDBReadIn Then
            CriticalErrMsg("The constellations database has not been loaded ..." & vbNewLine &
                           "Aborting program ...", ABORT_PROG)
        End If
    End Sub

    '-----------------------------------------------------------------------------------------------
    ' This method performs a precession correction to convert an RA/Decl from one Epoch to another.
    '
    ' The algorithm used here in the Herget Precession algorithm published on page 9
    ' of PUBL. CINCINNATI OBS. NO. 24. The code below was converted from the C and Fortran
    ' code found in the VizieR archives, http://cdsarc.u-strasbg.fr/viz-bin/Cat?VI/42. The
    ' code is translated almost literally, taking into account that array indexing in
    ' Visual BASIC is 0 based whereas Fortran is not. The author of the C program from which this
    ' translation came used a macro to adjust arrays (e.g., translate X[i] to x[i-1])
    ' to take the indexing base into account. The C author also used macros to define
    ' the trig functions (e.g., #define DCOS	cos) which are not preserved
    ' in the translation below.
    ' 
    ' RAIn              Right Ascension in radians for EpochIn
    ' DeclIn    		Declination in radians for EpochIn
    ' EpochIn   		Epoch that RAIn/DeclIn is given in
    ' EpochOut  		Epoch to convert to
    '
    ' Returns a 2 element vector with the precessed RA in item RA_IDX and the
    ' precessed Decl in item DECL_IDX of the array
    '-----------------------------------------------------------------------------------------------
    Private Shared Function HGTPrecession(ByVal RAIn As Double, ByVal DeclIn As Double, ByVal EpochIn As Double,
                                          ByVal EpochOut As Double) As Double()
        Dim result(1) As Double             ' RA is index 0, Decl is index 1
        Dim EP1, EP2 As Double
        Dim x1(2) As Double
        Dim x2(2) As Double
        Dim r(2, 2) As Double
        Dim T, ST, A, B, C, CSR, SINA, SINB, SINC, COSA, COSB, COSC As Double
        Dim i, j As Integer

        EP1 = 0.0
        EP2 = 0.0

        ' Compute input direction cosines
        A = Cos(DeclIn)
        x1(0) = A * Cos(RAIn)
        x1(1) = A * Sin(RAIn)
        x1(2) = Sin(DeclIn)

        ' Set up rotation matrix (R)
        ' Don't compare Epochs directly since equality comparison for real numbers is problematic due to roundoff errors

        If ((Abs(EP1 - EpochIn) < ASTMath.EPS) And (Abs(EP2 - EpochOut) < ASTMath.EPS)) Then
            ' do nothing
        Else
            EP1 = EpochIn
            EP2 = EpochOut
            CSR = CDR / 3600.0
            T = 0.001 * (EP2 - EP1)
            ST = 0.001 * (EP1 - 1900.0)
            A = CSR * T * (23042.53 + ST * (139.75 + 0.06 * ST) + T * (30.23 - 0.27 * ST + 18.0 * T))
            B = CSR * T * T * (79.27 + 0.66 * ST + 0.32 * T) + A
            C = CSR * T * (20046.85 - ST * (85.33 + 0.37 * ST) + T * (-42.67 - 0.37 * ST - 41.8 * T))
            SINA = Sin(A)
            SINB = Sin(B)
            SINC = Sin(C)
            COSA = Cos(A)
            COSB = Cos(B)
            COSC = Cos(C)
            r(0, 0) = COSA * COSB * COSC - SINA * SINB
            r(0, 1) = -COSA * SINB - SINA * COSB * COSC
            r(0, 2) = -COSB * SINC
            r(1, 0) = SINA * COSB + COSA * SINB * COSC
            r(1, 1) = COSA * COSB - SINA * SINB * COSC
            r(1, 2) = -SINB * SINC
            r(2, 0) = COSA * SINC
            r(2, 1) = -SINA * SINC
            r(2, 2) = COSC
        End If

        ' Perform the rotation to get the direction cosines at EpochOut
        For i = 0 To 2
            x2(i) = 0.0
            For j = 0 To 2
                x2(i) = x2(i) + r(i, j) * x1(j)
            Next
        Next

        result(0) = Atan2(x2(1), x2(0))       'precessed RA
        If result(0) < 0 Then result(0) = 6.28318530717948 + result(0)
        result(1) = Asin(x2(2))             ' precessed Decl

        Return result
    End Function

    '---------------------------------------------------------------------------
    ' Does a one-time initialization by reading in the constellation data file.
    '---------------------------------------------------------------------------
    Private Shared Sub initConstellations(ByVal prtInstance As ASTPrt)
        Dim fullFilename As String = IO.Path.Combine(dataPathname, dataFilename)
        Dim fChooser As ASTFileIO
        Dim br As StreamReader

        If constDBReadIn Then Return ' ignore if we've already read the constellations data

        If IsNothing(prtInstance) Then
            CriticalErrMsg("An output text area cannot be null. Aborting program ...", ABORT_PROG)
        End If

        prt = prtInstance

        ' See if the file exists where it is expected. If not, ask the user to find it.
        If doesFileExist(fullFilename) <> True Then
            ErrMsg("The constellations data file (" & dataFilename & ")" & vbNewLine &
                   "is missing. Please click 'OK' and then manually" & vbNewLine &
                   "find the constellations data file", "Missing Constellations Data File")

            ' Create a file browser form instance
            fChooser = New ASTFileIO(dataPrompt, dataFilter, dataFilename, dataExt)
            If fChooser.fileBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then
                fullFilename = fChooser.fileBrowser.FileName
            Else
                CriticalErrMsg("The Constellations Data File is required but was" & vbNewLine &
                               "not found. Aborting program ...", ABORT_PROG)
            End If
        End If

        ' Validate that the file is indeed a constellations data file
        br = New System.IO.StreamReader(fullFilename)
        If IsNothing(readTillStr(br, "<Constellations>", HIDE_ERRORS)) Then
            br.Close()
            CriticalErrMsg("The file specified is not a Constellations data" & vbNewLine &
                           "file and could not be processed. Aborting program ...", ABORT_PROG)
        End If

        ' Now that we've found the constellations data file, read in the data.
        ' Note that we must read the basic data and then the sorted data because
        ' that's how the data file is structured.
        If Not readBasicConstData(br) Then
            br.Close()
            CriticalErrMsg("The Constellations Data File is required but could" & vbNewLine &
                           "not be processed. Aborting program ...", ABORT_PROG)
        End If

        ' Although full initialization has not been completed, enough has been done
        ' to have constellations in place to now read in the constellation boundaries.
        ' We **must** set the flag here because reading in the sorted boundaries data assumes
        ' that the basic constellations have already been successfully read in.
        constDBReadIn = True

        If Not readSortedBoundariesData(br) Then
            br.Close()
            CriticalErrMsg("The Constellations Data File is required but could" & vbNewLine &
                           "not be processed. Aborting program ...", ABORT_PROG)
        End If

        'Close the file.
        br.Close()
    End Sub

    '------------------------------------------------------------------------
    ' Reads the constellation data file to extract basic info about
    ' the constellations.
    '
    ' br        buffered stream to read from
    '
    ' Returns true if successfully processed the basic constellation info.
    '------------------------------------------------------------------------
    Private Shared Function readBasicConstData(ByVal br As StreamReader) As Boolean

        ' count will be used to count the # of constellations actually read. If it differs from
        ' NUM_CONSTELLATIONS, we have an error and must abort. i will be used as the constDB index
        ' and must start at 0 since VBasic arrays are 0-based.
        Dim delimiter As Char = ","c                ' fields are comma delimited
        Dim count As Integer = 0
        Dim i As Integer = -1
        Dim strIn As String
        Dim targ As String = "</data>"
        Dim sParts() As String
        Dim readError As Boolean = False

        ' Look for the start of the constellations data
        If IsNothing(readTillStr(br, "<Data>")) Then Return False

        Do While br.Peek() <> -1
            strIn = getNonBlankLine(br, readError)
            If IsNothing(strIn) Or readError Then       ' pretend we are at the end
                strIn = "</data>"
            Else
                targ = removeWhitespace(LCase(strIn))
            End If
            If InStr(targ, "</data>") > 0 Then           ' found end of data section
                If count = NUM_CONSTELLATIONS Then
                    Return True
                Else
                    CriticalErrMsg("The constellations data file (" & dataFilename & ")" & vbNewLine &
                                   "may be corrupted. It contains " & count & " Constellations but" & vbNewLine &
                                    "should contain " & NUM_CONSTELLATIONS & ". Thus, we must abort ...", ABORT_PROG)
                    Return False            ' actually, this will never be executed, but is included to make the compiler happy
                End If
            End If

            If (count >= NUM_CONSTELLATIONS) Then Return False ' make sure we don't try to store too much data in constDB

            count = count + 1
            i = i + 1

            ' Create a new constellation object and add it to the array
            constDB(i) = New ASTConstellation()

            ' Now parse the input string
            sParts = strIn.Split(delimiter)
            If sParts.Length <> 8 Then              ' there must be exactly 8 fields
                CriticalErrMsg("Error while reading the constellation basic data" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If

            constDB(i).sAbbrevName = sParts(0).Trim()
            constDB(i).sName = sParts(1).Trim()
            constDB(i).sMeaning = sParts(7).Trim()
            constDB(i).sBrightestStar = sParts(4).Trim()

            ' Get the RA/Decl coordinates for the center of the constellation
            If Not isValidReal(sParts(2), constDB(i).dCenterRA, HIDE_ERRORS) Then
                CriticalErrMsg("Invalid RA for the constellation center" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If
            If Not isValidReal(sParts(3), constDB(i).dCenterDecl, HIDE_ERRORS) Then
                CriticalErrMsg("Invalid Decl for the constellation center" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If

            ' Get the RA/Decl coordinates for the brightest star in the constellation
            If Not isValidReal(sParts(5), constDB(i).dStarRA, HIDE_ERRORS) Then
                CriticalErrMsg("Invalid RA for the Brightest Star" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If
            If Not isValidReal(sParts(6), constDB(i).dStarDecl, HIDE_ERRORS) Then
                CriticalErrMsg("Invalid Decl for the Brightest Star" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If
        Loop
        Return True
    End Function

    '------------------------------------------------------------------------
    ' Reads the sorted database of constellations from the constellations
    ' data file and puts it into the boundariesDB structure.
    '
    ' br        buffered stream to read from
    '
    ' Returns true if successfully processed the constellation boundaries.
    '------------------------------------------------------------------------
    Private Shared Function readSortedBoundariesData(ByVal br As StreamReader) As Boolean
        Dim delimiter As Char = ","c                ' fields are comma delimited
        Dim count As Integer = 0
        Dim i As Integer = -1
        Dim RADecl As Double
        Dim idx As Integer
        Dim strIn As String
        Dim targ As String = "</data>"
        Dim sParts() As String
        Dim readError As Boolean = False

        ' Look for the start of the constellation boundaries data
        If IsNothing(readTillStr(br, "<SortedBoundaries>")) Then Return False
        If IsNothing(readTillStr(br, "<Data>")) Then Return False

        Do While br.Peek() <> -1
            strIn = getNonBlankLine(br, readError)
            If IsNothing(strIn) Or readError Then   ' pretend we're at the end and let rest of processing handle the problem
                strIn = "</data>"
            Else
                targ = removeWhitespace(LCase(strIn))
            End If

            If InStr(targ, "</data>") > 0 Then Return True ' found end of the data sections

            ' Add a new boundary data object. We only need to add new space after the 0th
            ' index because the definition of boundariesDB above starts with one entry
            i = i + 1
            If (i > 0) Then ReDim Preserve boundariesDB(UBound(boundariesDB) + 1)
            boundariesDB(i) = New BoundaryData

            ' Now parse the input string
            sParts = strIn.Split(delimiter)
            If sParts.Length <> 4 Then              ' there must be exactly 4 fields
                CriticalErrMsg("Error while reading the constellation boundaries data" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If

            ' Get the constellation boundaries (RA/Decl)
            If isValidReal(sParts(0), RADecl, HIDE_ERRORS) Then
                boundariesDB(i).setRALower(RADecl)
            Else
                CriticalErrMsg("Invalid Lower RA in the constellation boundaries data" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If
            If isValidReal(sParts(1), RADecl, HIDE_ERRORS) Then
                boundariesDB(i).setRAUpper(RADecl)
            Else
                CriticalErrMsg("Invalid Upper RA in the constellation boundaries data" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If
            If isValidReal(sParts(2), RADecl, HIDE_ERRORS) Then
                boundariesDB(i).setDecl(RADecl)
            Else
                CriticalErrMsg("Invalid Declination in the constellation boundaries data" & vbNewLine &
                               "at string [" & strIn & "]")
                Return False
            End If

            ' Find and store the index into the constDB for this boundary
            targ = sParts(3).Trim()
            idx = findConstellationByAbbrvName(targ)
            If idx < 0 Then
                CriticalErrMsg("Could not match boundary with a Constellation" & vbNewLine &
                               "at string [" & strIn & "]", ABORT_PROG)
            End If
            boundariesDB(i).setidx(idx)
        Loop

        Return True
    End Function
End Class
