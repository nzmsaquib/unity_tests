﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class implements a Lat/Lon object and related methods,
' such as time zones and validating lat/lon strings. In the
' validation routines, lat/lon can usually be entered in either
' decimal (e.g., 95.63W) or DMS format (e.g., 9d 15m 33sN).
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle

Public Class ASTLatLon
    ' Class instance variables. Lat/lon objects hold both the DMS and decimal version of the lat/lon.
    ' POS_DIRECTION is for North latitudes and East Longitudes

    ' Latitude fields
    Private bLatValid As Boolean                ' true if the object contains a valid latitude
    Private bLatDir As Boolean                  ' North latitude if bLatDir = POS_DIRECTION, else South Latitude
    Private latAngle As ASTAngle               ' actual value for the latitude
    ' Longitude fields
    Private bLonValid As Boolean                ' true if the object contains a valid longitude
    Private bLonDir As Boolean                  ' East longitude if bLonDir = POS_DIRECTION, else West Longitude
    Private lonAngle As ASTAngle               ' actual value for the longitude

    ' The numeric value of the time zone type is the time zone adjustment. These are
    ' negative because West longitudes are negative.
    Public Enum TimeZoneType
        LONGITUDE = 0
        EST = -5
        CST = -6
        MST = -7
        PST = -8
    End Enum

    ' These flags are used just to make the code more readable
    Private Const DO_LATITUDE As Boolean = True
    Private Const DO_LONGITUDE As Boolean = Not DO_LATITUDE

    ' Since much of the usage of this class involves validating that a string has a valid lat
    ' and/or lon, assume a new object is invalid until it is proven otherwise.
    Sub New()
        ' latitude fields
        bLatValid = False
        bLatDir = POS_DIRECTION
        latAngle = New ASTAngle()
        ' longitude fields
        bLonValid = False
        bLonDir = POS_DIRECTION               ' assume East longitude by default
        lonAngle = New ASTAngle()
    End Sub

    '********************************************************************
    ' Define 'get' accessors for the object's fields
    '********************************************************************
    ' Latitude fields
    Public Function isLatValid() As Boolean
        Return Me.bLatValid
    End Function
    Public Function isNorthLat() As Boolean
        Return Me.bLatDir
    End Function
    Public Function getLatAngle() As ASTAngle
        Return Me.latAngle
    End Function
    Public Function getLat() As Double
        Return Me.latAngle.getDecAngle()
    End Function
    ' Longitude fields
    Public Function isLonValid() As Boolean
        Return Me.bLonValid
    End Function
    Public Function isEastLon() As Boolean
        Return Me.bLonDir
    End Function
    Public Function getLonAngle() As ASTAngle
        Return Me.lonAngle
    End Function
    Public Function getLon() As Double
        Return Me.lonAngle.getDecAngle()
    End Function

    '***********************************************************************************
    ' Define some general methods for validating that a string contains a valid lat/lon
    ' and for converting a lat/lon to a displayable string
    '***********************************************************************************

    '***************************************************
    ' Convert a lat/lon object to a string
    '
    ' latlonObj         object to convert to a string
    ' formatFlag        flag for how to format the
    '                   result (DMSFORMAT or DECFORMAT)
    '
    ' Returns a printable lat/lon string
    '***************************************************
    Public Shared Function lat_lonToStr(ByVal latlonObj As ASTLatLon, ByVal formatFlag As Boolean) As String
        Return latToStr(latlonObj, formatFlag) & ", " & LonToStr(latlonObj, formatFlag)
    End Function

    '**************************************
    ' Convert a string to a time zone type
    '
    ' tzStr     string to convert
    '
    ' Returns tzStr converted to a time
    ' zone type.
    '**************************************
    Public Shared Function strToTimeZone(ByVal tzStr As String) As TimeZoneType
        If String.Compare(tzStr, "PST", True) = 0 Then
            Return TimeZoneType.PST
        ElseIf String.Compare(tzStr, "MST", True) = 0 Then
            Return TimeZoneType.MST
        ElseIf String.Compare(tzStr, "CST", True) = 0 Then
            Return TimeZoneType.CST
        ElseIf String.Compare(tzStr, "EST", True) = 0 Then
            Return TimeZoneType.EST
        End If
        Return TimeZoneType.LONGITUDE
    End Function

    '**********************************
    ' Calculate a time zone adjustment
    '
    ' tZone     time zone
    '
    ' Returns a time zone adjustment
    '**********************************
    Public Shared Function timeZoneAdjustment(ByVal tZone As TimeZoneType, ByVal lon As Double) As Double
        If (tZone = TimeZoneType.LONGITUDE) Then
            Return Math.Round(lon / 15.0)
        Else
            Return tZone
        End If
    End Function

    '***************************** Latitude methods *********************************************************

    '*****************************************************************************************************
    ' Checks a string to see if it contains a valid latitude value. The string format can be
    ' either DMS (###d ##m ##.##s[N or S]) or decimal (###.######[N or S]). If N/S is omitted,
    ' the latitude is assumed to be N. Specifying a sign in addition to, or in lieu of, N or S
    ' is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling routine
    ' display its own message.
    '
    ' This routine returns the validated values in the latlonObj (latitude only is set). The 'degrees'
    ' field in the latitude angle will always be non-negative with bLatDir (returned by isNorthLat)
    ' being POS_DIRECTION if the latitude is a North latitude. The real value in the angle object
    ' (use getLat()) will be negative if the value is for a South latitude, and bLatDir will
    ' be set to NEG_DIRECTION.
    '
    ' This method is overloaded to allow multiple ways to pass in parameters
    '
    ' inputStr              string to be checked for a valid latitude
    ' latlonObj             object to contain the resulting latitude, if valid
    ' flag                  whether to display error messages
    '
    ' Returns true if the string has a valid latitude, otherwise false. Also sets the latitude in
    ' the latlonObj object.
    '*****************************************************************************************************
    Public Shared Function isValidLat(ByVal inputStr As String, ByRef latlonObj As ASTLatLon, ByVal flag As Boolean) As Boolean
        Return validateLatOrLon(inputStr, DO_LATITUDE, latlonObj, flag)
    End Function
    Public Shared Function isValidLat(ByVal inputStr As String, ByRef latlonObj As ASTLatLon) As Boolean
        Return isValidLat(inputStr, latlonObj, SHOW_ERRORS)
    End Function

    '***************************************************************
    ' Converts latitude to a printable string as determined by
    ' formatFlag. This method is overloaded to allow the latitude
    ' to be passed as an object or by D, M, and S.
    '
    ' latlonObj             object to be converted to a string
    ' formatFlag            what format is desired (DMSFORMAT or
    '                       DECFORMAT)
    ' pos                   true if the latitude is positive
    ' d                     latitude degrees
    ' m                     latitude minutes
    ' s                     latitude seconds
    '
    ' Returns a printable string for the latitude
    '***************************************************************
    Public Shared Function latToStr(ByVal latlonObj As ASTLatLon, ByVal formatFlag As Boolean) As String
        If (formatFlag = DMSFORMAT) Then
            Return latOrLonToDMSStr(DO_LATITUDE, latlonObj.bLatDir, latlonObj.latAngle.getDegrees(),
                                    latlonObj.latAngle.getMinutes(), latlonObj.latAngle.getSeconds())
        Else
            Return latOrLonToDecStr(DO_LATITUDE, latlonObj.getLat())
        End If
    End Function
    Public Shared Function latToStr(ByVal pos As Boolean, ByVal d As Integer, ByVal m As Integer,
                                    ByVal s As Double, ByVal formatFlag As Boolean) As String

        If (formatFlag = DMSFORMAT) Then
            Return latOrLonToDMSStr(DO_LATITUDE, pos, d, m, s)
        Else
            ' Note: have to do not pos because DMStoDEC uses true to indicate negative value
            Return latOrLonToDecStr(DO_LATITUDE, DMStoDec(Not pos, d, m, s))
        End If
    End Function

    '***************************** Longitude methods *********************************************************

    '**************************************************************************************************
    ' Checks a string to see if it contains a valid longitude value. The string format can be
    ' either DMS (###d ##m ##.##s[W or E]) or decimal (###.######[W or E]). If W/E is omitted,
    ' the longitude is assumed to be W. Specifying a sign in addition to, or in lieu of, W or D
    ' is invalid. Do not display any error message unless flag is SHOW_ERRORS to let the calling
    ' routine display its own message.
    '
    ' This routine returns the validated values in the latlonObj (longitude only is set). The 'degrees' 
    ' field in the longitude angle will always be non-negative with bLonDir (returned by isEastLon)
    ' being POS_DIRECTION if the longitude is an East longitude. The real value in the angle object
    ' (use getLon()) will be negative if the value is for a West longitude, and bLonDir will
    ' be set to NEG_DIRECTION.
    '
    ' This method is overloaded to allow multiple ways to pass in parameters
    '
    ' inputStr              string to be checked for a valid longitude
    ' latlonObj             object to contain the resulting longitude, if valid
    ' flag                  whether to display error messages
    '
    ' Returns true if the string has a valid longitude, otherwise false. Also sets the longitude in
    ' the latlonObj object.
    '**************************************************************************************************
    Public Shared Function isValidLon(ByVal inputStr As String, ByRef latlonObj As ASTLatLon, ByVal flag As Boolean) As Boolean
        Return validateLatOrLon(inputStr, DO_LONGITUDE, latlonObj, flag)
    End Function
    Public Shared Function isValidLon(ByVal inputstr As String, ByRef latlonObj As ASTLatLon) As Boolean
        Return isValidLon(inputstr, latlonObj, SHOW_ERRORS)
    End Function

    '***************************************************************
    ' Converts longitude to a printable string as determined by
    ' formatFlag. This method is overloaded to allow the longitude
    ' to be passed as an object or by D, M, and S.
    '
    ' latlonObj             object to be converted to a string
    ' formatFlag            what format is desired (DMSFORMAT or
    '                       DECFORMAT)
    ' pos                   true if the longitude is positive
    ' d                     longitude degrees
    ' m                     longitude minutes
    ' s                     longitude seconds
    '
    ' Returns a printable string for the longitude
    '***************************************************************
    Public Shared Function lonToStr(ByVal latlonObj As ASTLatLon, ByVal formatFlag As Boolean) As String
        If (formatFlag = DMSFORMAT) Then
            Return latOrLonToDMSStr(DO_LONGITUDE, latlonObj.bLonDir, latlonObj.lonAngle.getDegrees(),
                                    latlonObj.lonAngle.getMinutes(), latlonObj.lonAngle.getSeconds())
        Else
            Return latOrLonToDecStr(DO_LONGITUDE, latlonObj.getLon())
        End If
    End Function
    Public Shared Function lonToStr(ByVal pos As Boolean, ByVal d As Integer, ByVal m As Integer,
                                    ByVal s As Double, ByVal formatFlag As Boolean) As String

        If (formatFlag = DMSFORMAT) Then
            Return latOrLonToDMSStr(DO_LONGITUDE, pos, d, m, s)
        Else
            ' Note: have to do not pos because DMStoDEC uses true to indicate negative value
            Return latOrLonToDecStr(DO_LONGITUDE, DMStoDec(Not pos, d, m, s))
        End If
    End Function

    '---------------------------------------------------------------------------------------------------
    ' Methods only used internally to this class
    '---------------------------------------------------------------------------------------------------

    '---------------------------------------------------------------------------------------
    ' Checks a string to see if it contains a valid latitude or longitude value. The
    ' string format can be either DMS (###d ##m ##.##s[N/S or W/E]) or decimal
    ' (###.######[N/S or W/E]). If the direction is omitted, a positive direction
    ' (i.e., N or E) is assumed. Specifying a sign in addition to, or in lieu of, 
    ' N/S or W/E is invalid. Do not display any error message if flag is HIDE_ERRORS to
    ' let the calling routine display its own message. This routine is only used internally
    ' by isValidLat and isValidLon.
    '
    ' inputStr              string to validate
    ' latlonFlag            either DO_LATITUDE or DO_LONGITUDE
    ' returnObj             object to modify with the value
    ' flag                  either HIDE_ERRORS or SHOW_ERRORS
    '
    ' Returns true if the string is valid, and puts the result in returnObj
    '---------------------------------------------------------------------------------------
    Private Shared Function validateLatOrLon(ByVal inputStr As String, ByVal latlonFlag As Boolean,
      ByRef returnObj As ASTLatLon, ByVal flag As Boolean) As Boolean

        Dim iLen As Integer
        Dim strDir As String
        Dim strLatLon As String
        Dim cPos, cNeg As String
        Dim bDirection As Boolean = POS_DIRECTION
        Dim angleObj As ASTAngle = New ASTAngle()

        If (latlonFlag = DO_LATITUDE) Then
            strLatLon = "Latitude"
            cPos = "N"
            cNeg = "S"
            returnObj.bLatValid = False
        Else
            strLatLon = "Longitude"
            cPos = "E"
            cNeg = "W"
            returnObj.bLonValid = False
        End If

        strDir = "[" & cPos & "," & cNeg & "]"

        inputStr = RemoveWhitespace(inputStr)
        iLen = Len(inputStr)
        If (iLen <= 0) Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter a valid " & strLatLon & " in the form ###d ##m ##.##s" & strDir & vbNewLine &
                   "or the form ###.######" & strDir & ".", "Invalid " & strLatLon & " format", flag)
            Return False
        End If

        ' See if the user specified a sign.
        If (InStr(inputStr, "-") > 0) Then
            ErrMsg("Do not enter a sign [-,+]. Use either '" & cPos & "' or '" &
                   cNeg & "' for " & strLatLon & ".", "Invalid " & strLatLon & " format", flag)
            Return False
        End If
        If (InStr(inputStr, "+") > 0) Then
            ErrMsg("Do not enter a sign [-,+]. Use either '" & cPos & "' or '" &
                   cNeg & "' for " & strLatLon & ".", "Invalid " & strLatLon & " format", flag)
            Return False
        End If

        ' Figure out whether this is positive or negative direction. Note that we could have ##.##sS to contend with
        ' since the user could express DMS format with S to indicate direction. Thus, we can't
        ' just convert to upper case and check for 'S'.
        If (inputStr.Substring(iLen - 1, 1) = cNeg) Then
            bDirection = NEG_DIRECTION
            iLen = iLen - 1
            inputStr = inputStr.Substring(0, iLen)      'get rid of the negative direction
        ElseIf (inputStr.Substring(iLen - 1, 1) = cPos) Then
            bDirection = POS_DIRECTION
            iLen = iLen - 1
            inputStr = inputStr.Substring(0, iLen)      'get rid of the positive direction
        End If

        ' Let the Angle class do the work to validate that inputStr has a valid angle
        If Not isValidAngle(inputStr, angleObj, HIDE_ERRORS) Then
            ErrMsg("The " & strLatLon & " entered is invalid. Please enter a" & vbNewLine &
                   "valid " & strLatLon & " in the form ###d ##m ##.##s" & strDir & vbNewLine &
                   "or the form ###.######" & strDir & ".", "Invalid " & strLatLon & " format", flag)
            Return False
        End If

        If (latlonFlag = DO_LATITUDE) Then
            If (Abs(angleObj.getDecAngle()) > 90.0) Then
                ErrMsg("The latitude value is out of range.", "Invalid Latitude", flag)
                Return False
            End If
            returnObj.bLatDir = bDirection
            returnObj.bLatValid = True

            ' Because the 'N/S' was stripped off, the angle is always positive. So, we must
            ' check to see if it has to be converted to a negative angle.
            If Not returnObj.bLatDir Then angleObj = DecToDMS(-angleObj.getDecAngle())

            returnObj.latAngle = angleObj
        Else
            If (Abs(angleObj.getDecAngle()) > 180.0) Then
                ErrMsg("The longitude value is out of range.", "Invalid Longitude", flag)
                Return False
            End If
            returnObj.bLonDir = bDirection
            returnObj.bLonValid = True

            ' Because the 'W/E' was stripped off, the angle is always positive. So, we must
            ' check to see if it has to be converted to a negative angle.
            If Not returnObj.bLonDir Then angleObj = DecToDMS(-angleObj.getDecAngle())

            returnObj.lonAngle = angleObj
        End If

        Return True

    End Function

    '---------------------------------------------------------------
    ' Converts lat or lon decimal value to a displayable string.
    '
    ' bLat              DO_LATITUDE or DO_LONGITUDE
    ' LatOrLon          decimal latitude or longitude
    '
    ' Returns latitude or longitude as a printable string
    '--------------------------------------------------------------
    Private Shared Function latOrLonToDecStr(ByVal bLat As Boolean, ByVal LatOrLon As Double) As String
        Dim result As String

        result = Format(Abs(LatOrLon), latlonFormat)
        If (bLat = DO_LATITUDE) Then
            If (LatOrLon >= 0.0) Then
                result = result & "N"
            Else
                result = result & "S"
            End If
        Else
            If (LatOrLon >= 0.0) Then
                result = result & "E"
            Else
                result = result & "W"
            End If
        End If
        Return result
    End Function

    '--------------------------------------------------------------
    ' Converts lat or lon DMS values to a displayable DMS string.
    '
    ' bLat              DO_LATITUDE or DO_LONGITUDE
    ' pos               true if a positive latitude/longitude
    ' d                 degrees
    ' m                 minutes
    ' s                 seconds
    '
    ' Returns latitude or longitude as a printable string
    '--------------------------------------------------------------
    Private Shared Function latOrLonToDMSStr(ByVal bLat As Boolean, ByVal pos As Boolean,
                                             ByVal d As Integer, ByVal m As Integer, ByVal s As Double) As String
        Dim result As String

        result = Format(Abs(d), "##0") & "d " + Format(m, "#0") & "m " & Format(s, "#0.0#") & "s"
        If (bLat = DO_LATITUDE) Then
            If (pos = POS_DIRECTION) Then
                result = result & "N"
            Else
                result = result & "S"
            End If
        Else
            If (pos = POS_DIRECTION) Then
                result = result & "E"
            Else
                result = result & "W"
            End If
        End If
        Return result
    End Function

End Class
