﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class implements a class for creating star charts.
' It uses the Visual Basic GDI+/.NET graphics routines.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Drawing.Drawing2D

Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg

Public Class ASTCharts
    '-------------------------------------- Graphics Notes ---------------------------------------------
    ' The GDI+/.NET graphics package provides multiple methods for manipulating graphics via
    ' matrix transformations as well as methods such as RotateTransformation and ScaleTransformation
    ' that do the same thing without requiring the calling routine to deal with matrices. Note
    ' that however the transformations are accomplished, they affect all graphical objects,
    ' including text that is drawn via the DrawString method. Thus, if one does a rotation to a
    ' line or rectangle, text will be rotated as well. For our purposes, rotating the text is
    ' undesirable and we must implement our own DrawString method to account for this.
    '
    ' Since GDI+ assumes that the coordinate (0,0) is the top left point on the drawing surface,
    ' it is convenient to do a transformation to move the point (0,0) to be the bottom left point
    ' on the drawing surface. Although this can be done with the built-in GDI+ methods for
    ' transformations, it is easier in our case to do the simple transformations we need through
    ' affine transformation matrices. This makes it easier to translate the code to other languages
    ' that may not have all the methods GDI+ provides, but also makes it easier to undo how various
    ' transformations impact graphical text objects.
    '
    ' The method setWorldCoordSysOrigin below moves the coordinate system origin from the screen's
    ' upper left corner to the bottom left corner. It does so with an affine transformation
    ' matrix that is equivalent to a translate(0,yMax) transformation followed by a scale(1,-1)
    ' transformation. Both transformations are required because while the translate transformation
    ' moves the coordinate system origin, it does **not** change the direction of the positive
    ' y-axis. The scale transformation Is required to make the y-axis increase from bottom to
    ' top in the coordinate system. Changing the direction of the y-axis could be done with
    ' a rotation transformation of 90 degrees, but the equivalent scale(1,-1) is done instead
    ' to avoid the need to use sine/cosine calculations to do the transformation.
    '
    ' It Is also a simple matter to do a transformation that maps the range for a coordinate, such
    ' as right ascension and declination, to the width/height of the drawing surface. However, this
    ' requires a scaling transformation that will stretch and distort text and make it more difficult
    ' to draw circles that will appear on the screen as circles. Hence, excepting the aforementioned
    ' scale(1,-1) transformation, we will avoid scaling transformations so that we will not to have to
    ' worry about how graphics objects, such as text, are stretched as a result of a
    ' scaling transformation.
    ' 
    ' Note that our drawing surface will be the same area in the GUI that is used for displaying
    ' text, which is given to this class by the GUI when a chart object is created.
    '-------------------------------------- Graphics Notes ---------------------------------------------

    ' Flag indicating whether to do a rectangular plot (TRUE) or circular plot (FALSE)
    Private Const RECT_PLOT As Boolean = True

    ' Define default pens and drawing backgrounds
    Private Shared whitePen As Pen = New Pen(Brushes.White)
    Private Shared blackPen As Pen = New Pen(Brushes.Black)
    Private Shared whiteBKG As System.Drawing.Color = Color.White
    Private Shared blackBKG As System.Drawing.Color = Color.Black
    Private Shared whiteBrush As Drawing.SolidBrush = CType(Brushes.White, SolidBrush)
    Private Shared blackBrush As Drawing.SolidBrush = CType(Brushes.Black, SolidBrush)

    ' Define the actual colors to use
    Private currentPen As Pen = blackPen
    Private currentBKG As System.Drawing.Color = whiteBKG
    Private currentBrush As System.Drawing.SolidBrush = blackBrush

    ' Store the drawing surface that the invoking routine gives us.
    ' drawingCanvas is the GUI area on which to draw while starChart
    ' is the actual graphics object being drawn. The dimensions of the
    ' drawing surface are calculated and are 0,0 to canvasWidth,canvasHeight.
    Private drawingCanvas As RichTextBox = Nothing
    Private starChart As Graphics = Nothing
    Private canvasWidth, canvasHeight As Integer

    ' Define font to be used for any text displayed on a chart
    Private Shared canvasFont As Font = New System.Drawing.Font("Arial", 10, FontStyle.Bold)

    ' The clippingRect structure is used as a rectangular bounding region for
    ' producing RA/Decl plots. The clipping region is adjusted for RA/Decl
    ' labeling. The clippingCircle structure is used as a circular bounding region
    ' for producing a chart for an observer's horizon and it too is adjusted
    ' to allow compass directions to be displayed. Both of these structures
    ' are initialized here so that they can be reused, but are calculated
    ' later to conform to the actual size of drawingCanvas at the time a
    ' star chart plot is begun. Both clipping areas are in screen coordinates.
    Private clippingRect As Rectangle = New Rectangle(0, 0, 50, 50)
    Private clippingCircle As GraphicsPath = New GraphicsPath

    ' We also need scaling factors to allow converting an RA/Decl pair to
    ' the x/y coordinates in the clipping rectangle.
    Private xScale, yScale As Double

    ' For a horizon coordinates chart, plot a circle centered in the display area. The
    ' circle will be of size horizonRadius, where the radius is adjusted based on the actual
    ' canvas width and height and adjusted to allow compass directions to be displayed
    ' outside the plotting circle.
    Private horizonRadius, centerX, centerY As Double

    '***********************************************************************
    ' Create a Star Chart instance. Save the form that the calling routine
    ' sent to be used as the drawing canvas.
    '
    ' canvas            area in GUI to use for a drawing canvas
    '***********************************************************************
    Sub New(ByVal canvas As Object)
        'Since we must create an ASTCharts object instance to use this class
        ' and that can only be done via a New operation, there's no need to
        ' check canvas or drawingCanvas for null anywhere in the rest of this class
        If IsNothing(canvas) Then
            CriticalErrMsg("The drawing canvas for star charts cannot be null. Aborting program ...")
            Application.Exit()
        End If
        drawingCanvas = CType(canvas, RichTextBox)
    End Sub

    '**************************************************
    ' This method labels an object located at Alt, Az.
    '
    ' txt           label to display
    ' Alt, Az       coordinates for the object
    ' brushColor    color to use for the text
    '**************************************************
    Public Sub drawLabelAltAz(ByVal txt As String, ByVal Alt As Double, ByVal Az As Double,
                              ByVal brushColor As Brush)
        Dim txtFontSize As SizeF
        Dim txtWidth, txtHeight As Integer
        Dim pt As PointF
        Dim x, y As Integer

        ' If object is below the horizon, no need to try to label it
        If Alt < 0.0 Then Return

        txtFontSize = starChart.MeasureString(txt, canvasFont)
        txtWidth = CInt(txtFontSize.Width)
        txtHeight = CInt(txtFontSize.Height)

        pt = AltAz2XY(Alt, Az)
        y = CInt(pt.Y - (txtHeight / 2.0))
        x = CInt(pt.X - (txtWidth / 2.0))
        drawString(txt, x, y, canvasFont, brushColor, Not RECT_PLOT)
    End Sub

    '**************************************************
    ' This method labels an object located at RA, Decl.
    '
    ' txt           label to display
    ' RA, Decl      coordinates for the object
    ' brushColor    color to use for the text
    '**************************************************
    Public Sub drawLabelRADecl(ByVal txt As String, ByVal RA As Double, ByVal Decl As Double,
                               ByVal brushColor As Brush)
        Dim txtFontSize As SizeF
        Dim txtWidth, txtHeight As Integer
        Dim pt As PointF
        Dim x, y As Integer

        txtFontSize = starChart.MeasureString(txt, canvasFont)
        txtWidth = CInt(txtFontSize.Width)
        txtHeight = CInt(txtFontSize.Height)

        pt = RADecl2XY(RA, Decl)
        y = CInt(pt.Y - (txtHeight / 2.0))
        x = CInt(pt.X + (txtWidth / 2.0))       ' add because x decreases going left to right
        drawString(txt, x, y, canvasFont, brushColor, RECT_PLOT)
    End Sub

    '*********************************************************
    ' Initialize a drawing canvas surface for a star chart.
    '
    ' bkgWhite      true if user wants a white background,
    '               else use a black background
    ' eqChart       true if doing an equatorial coordinates
    '               chart, otherwise do a horizon coordinates
    '               chart
    '*********************************************************
    Public Sub initDrawingCanvas(ByVal bkgWhite As Boolean, ByVal eqChart As Boolean)
        ' Create a star chart canvas. We have to do this because the size of the
        ' window may have changed since the last time a star chart was drawn.
        ' Since we have to create a new canvas, we should dispose any earlier one.
        If Not IsNothing(starChart) Then
            starChart.Dispose()
        End If

        starChart = drawingCanvas.CreateGraphics
        starChart.SmoothingMode = SmoothingMode.AntiAlias

        If bkgWhite Then
            currentBKG = whiteBKG
            currentPen = blackPen
            currentBrush = blackBrush
        Else
            currentBKG = blackBKG
            currentPen = whitePen
            currentBrush = whiteBrush
        End If
        currentPen.Width = 1
        starChart.Clear(currentBKG)

        setWorldCoordSysOrigin()

        If eqChart Then
            initEQCanvas()
        Else
            initHorizonCanvas()
        End If
    End Sub

    '******************************************************
    ' Plot an object, given its horizon coordinates, on a
    ' circular plotting surface.
    '
    ' Alt,Az                horizon coordinates to plot
    ' mV                    visual magnitude of the object
    ' brushColor            color to use for plotting
    '******************************************************
    Public Sub plotAltAz(ByVal Alt As Double, ByVal Az As Double, ByVal mV As Double)
        plotAltAz(Alt, Az, mV, currentBrush)
    End Sub
    Public Sub plotAltAz(ByVal Alt As Double, ByVal Az As Double, ByVal mV As Double,
                         ByVal brushColor As Brush)
        Dim pt As PointF

        ' If object is below the horizon, no need to try to plot it
        If Alt < 0.0 Then Return

        pt = AltAz2XY(Alt, Az)
        drawFilledCircle(pt.X, pt.Y, mVtoRadius(mV), brushColor)
    End Sub

    '******************************************************
    ' Plot an object, given its equatorial coordinates, on
    ' a rectangular plotting surface.
    '
    ' RA, Decl              equatorial coordinates to plot
    ' mV                    visual magnitude of the object
    ' brushColor            color to use for plotting
    '******************************************************
    Public Sub plotRADecl(ByVal RA As Double, ByVal Decl As Double, ByVal mV As Double)
        plotRADecl(RA, Decl, mV, currentBrush)
    End Sub
    Public Sub plotRADecl(ByVal RA As Double, ByVal Decl As Double, ByVal mV As Double,
                          ByVal brushColor As Brush)
        Dim x, y As Double

        x = CInt(Round(RA2X(RA)))
        y = CInt(Round(Decl2Y(Decl)))

        drawFilledCircle(x, y, mVtoRadius(mV), brushColor)
    End Sub

    '------------------------------------------------------------------------------------------------
    ' Private routines used only in this class
    '------------------------------------------------------------------------------------------------

    '-----------------------------------------------------------
    ' Convert Alt and Az to the appropriate x,y coordinates for
    ' plotting. This assumes that the x,y coordinates are for
    ' a circular plotting surface.
    '
    ' Alt, Az       horizon coordinates to convert
    '
    ' Returns a point structure containing the converted
    ' coordinates.
    '-----------------------------------------------------------
    Private Function AltAz2XY(ByVal Alt As Double, ByVal Az As Double) As PointF
        Dim pt As PointF = New PointF(0.0, 0.0)
        Dim x, y As Double

        x = COS_D(Alt) * SIN_D(Az)
        y = COS_D(Alt) * COS_D(Az)

        pt.X = CSng(x * horizonRadius + centerX)
        pt.Y = CSng(y * horizonRadius + centerY)
        Return pt
    End Function

    '-----------------------------------------------------------
    ' Convert RA and Decl to the appropriate x,y coordinates for
    ' plotting. This assumes that the x,y coordinates are for
    ' a rectangular plotting surface.
    '
    ' RA            RA to convert to an coordinate
    ' Decl          Decl to convert to a y coordinate
    '
    ' Returns x, y, or a point structure depending upon which
    ' of the overloaded methods were invoked.
    '-----------------------------------------------------------
    Private Function Decl2Y(ByVal Decl As Double) As Double
        Return Decl * yScale
    End Function
    Private Function RA2X(ByVal RA As Double) As Double
        Return RA * xScale
    End Function
    Private Function RADecl2XY(ByVal RA As Double, ByVal Decl As Double) As PointF
        Dim pt As PointF = New PointF(0.0, 0.0)

        pt.X = CSng(RA2X(RA))
        pt.Y = CSng(Decl2Y(Decl))
        Return pt
    End Function

    '----------------------------------------------------------
    ' Draw a circle in the current or user-specified color.
    '
    ' x,y           coordinates for the center of the circle
    ' radius        radius of the circle to draw
    ' penColor      color to use for outlined circle
    '
    ' Visual Basic GDI+ draws ellipses by doing a bounding box
    ' described by left, top, width, height coordinates
    '----------------------------------------------------------
    Private Sub drawCircle(ByVal x As Double, ByVal y As Double, ByVal radius As Double)
        drawCircle(x, y, radius, currentPen)
    End Sub
    Private Sub drawCircle(ByVal x As Double, ByVal y As Double, ByVal radius As Double,
                           ByVal penColor As Pen)
        Dim iX, iY, iRadius As Integer

        iX = CInt(Round(x - radius))
        iY = CInt(Round(y - radius))
        iRadius = CInt(Round(radius))

        ' GDI+ requires a pen for a circle outline and a brush for a filled circle
        starChart.DrawEllipse(penColor, iX, iY, 2 * iRadius, 2 * iRadius)
    End Sub

    '--------------------------------------------------------------
    ' Draw a filled circle in the current or user-specified color.
    '
    ' x,y           coordinates for the center of the circle
    ' radius        radius of the circle to draw
    ' brushColor    color to use for filled circle
    '
    ' Visual Basic GDI+ draws ellipses by doing a bounding box
    ' described by left, top, width, height coordinates
    '--------------------------------------------------------------
    Private Sub drawFilledCircle(ByVal x As Double, ByVal y As Double, ByVal radius As Double)
        drawFilledCircle(x, y, radius, currentBrush)
    End Sub
    Private Sub drawFilledCircle(ByVal x As Double, ByVal y As Double, ByVal radius As Double,
                           ByVal brushColor As Brush)
        Dim iX, iY, iRadius As Integer

        iX = CInt(Round(x - radius))
        iY = CInt(Round(y - radius))
        iRadius = CInt(Round(radius))

        ' GDI+ requires a pen for a circle outline and a brush for a filled circle
        starChart.FillEllipse(brushColor, iX, iY, 2 * iRadius, 2 * iRadius)
    End Sub

    '--------------------------------------------------------------------------------------
    ' This method displays a string in graphics mode. GDI+ has a DrawString method, but
    ' that method is affected by any transformations that are done on the graphics
    ' surface (see setWorldCoordSysOrigin). The method implemented here does the equivalent
    ' of the GDI+/.NET DrawString, but is not impacted by prior transformations done in
    ' setWorldCoordSysOrigin. It does so by creating a GraphicsPath object that will hold
    ' the string to be printed, and then does a local transformation on the GraphicsPath
    ' object that affects only the text object to be displayed. The string will be displayed
    ' such that the top left corner of the string will be at position x,y.
    '
    ' txt               text string to display
    ' x,y               coordinates for where the string is to be displayed relative
    '                   to the current transformations in the graphics object
    ' fnt               font to use for displaying the string
    ' bColor            brush color to use to display the text
    ' eqPlot            RECT_PLOT if this is for an equatorial coordinates plot, else this
    '                   is for a horizon coordinates plot
    '--------------------------------------------------------------------------------------
    Private Sub drawString(ByVal txt As String, ByVal x As Double, ByVal y As Double, ByVal fnt As Font,
                           ByVal bColor As Brush)
        drawString(txt, x, y, fnt, bColor, Not RECT_PLOT)
    End Sub
    Private Sub drawString(ByVal txt As String, ByVal x As Double, ByVal y As Double, ByVal fnt As Font,
                           ByVal bColor As Brush, ByVal eqPlot As Boolean)
        Dim fFam As FontFamily = New FontFamily(fnt.Name)
        Dim emsize As Integer
        Dim txtPath As GraphicsPath = New GraphicsPath
        Dim xform, xformRect As Matrix
        Dim iX, iY As Integer

        iX = CInt(Round(x))
        iY = CInt(Round(y))

        emsize = CInt(starChart.DpiY * fnt.SizeInPoints / 72)  ' convert fontsize to ems

        ' Text must be added to the path before the transformation is done!
        txtPath.AddString(txt, fFam, fnt.Style, emsize, New Point(0, 0), StringFormat.GenericTypographic)

        ' If this draw string is for the RA/Decl plot, we have an extra transformation to undo
        xformRect = New Matrix(-1, 0, 0, 1, 0, 0)
        If eqPlot = RECT_PLOT Then
            txtPath.Transform(xformRect)
        End If

        ' Create a transform to undo locally what the setWorldCoordSysOrigin method did, then draw the string
        xform = New Matrix(1, 0, 0, -1, iX, iY)
        txtPath.Transform(xform)
        Me.starChart.FillPath(bColor, txtPath)

        ' Dispose of all the objects we created
        txtPath.Dispose()
        fFam.Dispose()
        xform.Dispose()
        xformRect.Dispose()
    End Sub

    '----------------------------------------------------------
    ' Initialize the drawing canvas for a star chart based on
    ' equatorial coordinates. The drawing area will be a
    ' rectangle with the RA on the y-axis and the Decl plotted
    ' on the x-axis. RA decreases from 24h on the left of the
    ' display to 0h on the right. Decl increases from -180 degrees
    ' at the bottom to 180 degrees at the top. The size of the
    ' plotting rectangle is adjusted to allow RA/Decl marks and
    ' labels on the plotting rectangle boundary.
    '----------------------------------------------------------
    Private Sub initEQCanvas()
        Dim i, x, y As Integer
        Dim st As String
        Dim RAFontSize, DeclFontSize As SizeF
        Dim RAFontWidth, RAFontHeight, DeclFontWidth, DeclFontHeight As Integer
        Dim xPad As Integer = 7
        Dim yPad As Integer = 5
        Dim xTicLen As Integer = 5
        Dim yTicLen As Integer = 5
        Dim xform As Matrix

        ' Figure out how much to adjust the plotting rectangle to allow
        ' tic marks and RA/Decl labels outside the plotting rectangle.
        RAFontSize = starChart.MeasureString("00h", canvasFont)
        DeclFontSize = starChart.MeasureString("+180" & Chr(176), canvasFont) ' chr(176) is the symbol for degrees
        RAFontWidth = CType(RAFontSize.Width, Integer)
        RAFontHeight = CType(RAFontSize.Height, Integer)
        DeclFontWidth = CType(DeclFontSize.Width, Integer)
        DeclFontHeight = CType(DeclFontSize.Height, Integer)

        ' Initially set the plotting rectangle relative to the current origin, which
        ' is left bottom. This will be adjusted later to be an actual clipping rectangle.
        clippingRect.X = DeclFontWidth + xPad
        clippingRect.Width = canvasWidth - clippingRect.X - xPad
        clippingRect.Y = RAFontHeight + yPad + yTicLen
        clippingRect.Height = canvasHeight - clippingRect.Y - yPad

        xScale = clippingRect.Width / 24.0
        yScale = clippingRect.Height / 360.0

        starChart.DrawRectangle(currentPen, clippingRect)

        ' At this point, the coordinate system origin (0,0) is the bottom left
        ' of the plotting surface. We need to move the origin so that it is in
        ' the plotting rectangle just drawn and set the x/y limits to be fully
        ' within the plotting rectangle. Also, because we're plotting RA on the
        ' x-axis and Decl on the y-axis, it would be useful to do a scaling
        ' transformation to map the x range to be [24.0, 0.0] and y to be
        ' [-180.0, 180.0] to match RA and Decl ranges. However, a scaling
        ' transformation would also scale text for the RA/Decl labels and the
        ' circles used to plot stars. So, we'll settle for just (1) translating
        ' the current coordinate system origin to be what is presently the point
        ' (clippingRect.X, clippingRect.Y), (2) making the x-axis range from 
        ' clippingRect.Width to 0, and (3) making the y-axis range from -clippingRect.height/2
        ' to clippingRect.Height/2. We'll do the entire transformation with a single
        ' affine transformation matrix that first makes the x-axis range from clippingRect.Width
        ' to 0 and then translates so that the origin (0,0) is at the left and middle of the
        ' plotting rectangle. Doing this then allows RA/Decl coordinates to be very
        ' easily mapped to the plotting rectangle via a simple scaling.

        xform = New Matrix(-1, 0, 0, 1, clippingRect.X + clippingRect.Width, clippingRect.Y + CSng(clippingRect.Height / 2.0))
        starChart.MultiplyTransform(xform, MatrixOrder.Prepend)

        ' Now adjust the clipping rectangle to be the proper size. We'll apply
        ' the clipping area later because we want to draw the tic marks and
        ' labels outside the clipping area
        x = clippingRect.Width
        y = clippingRect.Height
        clippingRect.X = 0
        clippingRect.Width = x
        clippingRect.Y = -CInt(Round(y / 2.0))
        clippingRect.Height = y

        ' Draw the RA tic marks and labels on the x-axis
        y = CInt(Round(Decl2Y(-180.0)))
        For i = 1 To 24
            x = CInt(RA2X(i))
            starChart.DrawLine(currentPen, x, y, x, y - yTicLen)
            If i Mod 2 = 0 Then
                st = Format(i, "00h")
                x = x + CInt(RAFontWidth / 2)
                drawString(st, x, y - yTicLen, canvasFont, currentBrush, RECT_PLOT)
            End If
        Next

        ' Now draw the Decl tic marks and labels on the y-axis
        x = CInt(Round(RA2X(24.0)))
        For i = -180 To 170 Step 10
            y = CInt(Round(Decl2Y(i)))
            starChart.DrawLine(currentPen, x, y, x + xTicLen, y)
            If i Mod 20 = 0 Then
                st = String.Format("{0:+0;-0;0}" & Chr(176), i)
                DeclFontSize = starChart.MeasureString(st, canvasFont)
                DeclFontWidth = CInt(DeclFontSize.Width)
                y = y + CInt(DeclFontHeight / 2)
                drawString(st, x + xTicLen + DeclFontWidth, y, canvasFont, currentBrush, RECT_PLOT)
            End If
        Next

        ' Finally, set the clipping rectangle as a clipping region
        starChart.SetClip(clippingRect)
    End Sub

    '------------------------------------------------------
    ' Initialize the drawing canvas for a star chart, drawn
    ' as a circular chart, of horizon coordinates.
    '
    ' Using the current size of the drawing canvas surface,
    ' determine the diameter of a circle to use in plotting
    ' the star chart objects. The diameter will be the canvas
    ' width or height, whichever is smaller. This diameter
    ' will be adjusted so that compass directions can be
    ' displayed outside the circle.
    '------------------------------------------------------
    Public Sub initHorizonCanvas()
        Dim WFontSize As SizeF
        Dim WFontHeight, WFontWidth As Integer
        Dim diameter As Double

        ' Determine where the center of the plotting circle will be
        centerX = canvasWidth / 2.0
        centerY = canvasHeight / 2.0

        ' Calculate size of adjustment needed for the compass directions
        ' based on the size of the font that will be used. We'll use
        ' the character W as representative of the font's width/height
        WFontSize = starChart.MeasureString("W", canvasFont)
        WFontHeight = CInt(WFontSize.Height)
        WFontWidth = CInt(WFontSize.Width)

        ' Figure out a proper circle to represent the observer's horizon,
        ' which is adjusted for the compass direction strings
        If canvasWidth > canvasHeight Then
            diameter = canvasHeight - 2 * WFontHeight - 2
        Else
            diameter = canvasWidth - 2 * WFontWidth - 2
        End If
        horizonRadius = diameter / 2.0

        ' Draw the compass directions
        drawString("N", centerX - 0.25 * WFontWidth, centerY + horizonRadius + WFontHeight,
                   canvasFont, currentBrush)
        drawString("S", centerX - 0.25 * WFontWidth, centerY - horizonRadius,
                   canvasFont, currentBrush)
        drawString("W", centerX - horizonRadius - WFontWidth, centerY + WFontHeight / 2.0,
                   canvasFont, currentBrush)
        drawString("E", centerX + horizonRadius + 1, centerY + WFontHeight / 2.0,
                   canvasFont, currentBrush)

        ' Draw a circle representing the plotting surface
        drawCircle(centerX, centerY, horizonRadius)

        ' Set up a clipping region. Dispose of the old one and create a new one.
        clippingCircle.Dispose()
        clippingCircle = New GraphicsPath
        clippingCircle.AddEllipse(CInt(centerX - horizonRadius), CInt(centerY - horizonRadius),
                                  CInt(2 * horizonRadius), CInt(2 * horizonRadius))
        starChart.SetClip(clippingCircle)
    End Sub

    '-------------------------------------------------------
    ' Determine how big a "dot" to plot for an object based
    ' upon its mV.
    ' 
    ' mV                    visual magnitude of the object
    '
    ' Returns the radius of a circle to plot for the object
    ' where a larger radius represents a brighter object.
    '-------------------------------------------------------
    Private Shared Function mVtoRadius(ByVal mV As Double) As Integer
        Dim rdot As Integer

        ' Brightness increases/decreases by 1 magnitude every 2.512 units
        rdot = CInt(mV0RADIUS - (mV / 2.512))
        If rdot < mVMIN_RADIUS Then
            rdot = mVMIN_RADIUS
        ElseIf rdot > mVMAX_RADIUS Then
            rdot = mVMAX_RADIUS
        End If

        Return rdot
    End Function

    '----------------------------------------------------------------------
    ' This method does the transformation necessary to move the coordinate
    ' system origin (0,0) as GDI+ provides it from being the top
    ' left point on the drawing surface to the bottom left of the drawing surface,
    ' and changes the y-axis orientation so that y values increase going from
    ' the origin upwards as in a normal Cartesian coordinate system.
    ' This method also sets the variables canvasWidth and canvasHeight for the
    ' drawing surface. Hence, the net result of this method is that the
    ' coordinate system origin (0,0) is moved to (0, -canvasHeight) with (0,0)
    ' being the bottom left point and (canvasWidth,canvasHeight) being the
    ' top right point on the drawing surface.
    '--------------------------------------------------------------------
    Private Sub setWorldCoordSysOrigin()
        Dim xform As Matrix

        starChart.ResetTransform()          ' remove any prior transformations
        canvasWidth = drawingCanvas.ClientSize.Width
        canvasHeight = drawingCanvas.ClientSize.Height

        ' Move origin from top left corner to bottom left corner
        ' and change direction of the y-axis. This is equivalent
        ' to translate(0,canvasHeight) followed by scale(1,-1)
        xform = New Matrix(1, 0, 0, -1, 0, canvasHeight)
        starChart.MultiplyTransform(xform)
        xform.Dispose()
    End Sub
End Class
