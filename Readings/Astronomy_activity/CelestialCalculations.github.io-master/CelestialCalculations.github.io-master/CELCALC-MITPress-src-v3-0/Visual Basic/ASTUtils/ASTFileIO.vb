﻿'*************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides the GUI to let a user choose a filename
' and path for loading a data file, typically a star catalog
' but also other files (e.g., constellations data file)
' if the file is not where it was expected to be.
'*************************************************************

Option Explicit On
Option Strict On

Imports System.IO

Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTStr

Public Class ASTFileIO

    '*************************************************************
    ' Create a file chooser instance. This method is overloaded
    ' to allow defaults or explicitly setting choose data.
    '
    ' title             title for the file browser window
    ' filter            filter to use for which files to list
    ' defaultFName      default filename to open
    ' defaultExt        default file extension
    '*************************************************************
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Me.fileBrowser.AddExtension = True
        Me.fileBrowser.Title = "Choose a file to open ..."
    End Sub
    Public Sub New(ByVal title As String, ByVal filter As String, ByVal defaultExt As String)
        ' This call is required by the designer.
        InitializeComponent()
        Me.fileBrowser.DefaultExt = defaultExt
        Me.fileBrowser.AddExtension = True
        Me.fileBrowser.Filter = filter
        Me.fileBrowser.Title = title
    End Sub
    Public Sub New(ByVal title As String, ByVal filter As String, ByVal defaultFName As String,
                   ByVal defaultExt As String)
        ' This call is required by the designer.
        InitializeComponent()

        Me.fileBrowser.DefaultExt = defaultExt
        Me.fileBrowser.AddExtension = True
        Me.fileBrowser.Filter = filter
        Me.fileBrowser.FileName = defaultFName
        Me.fileBrowser.Title = title
    End Sub

    '*************************************************************
    ' Determines whether a specified file already exists. This
    ' method does not create the file if it does not exist.
    '
    ' fullFName         full pathname and filename
    '
    ' Returns true if the file exists, else false.
    '*************************************************************
    Public Shared Function doesFileExist(ByVal fullFName As String) As Boolean
        Return System.IO.File.Exists(fullFName)
    End Function

    '*******************************************************************
    ' Reads the input file to get a non-blank line.
    ' 
    ' br    				file to read from
    ' errFlag               whether an error occured
    '
    ' Returns the next non-blank line in the file, if one exists.
    ' If the end of file is reached without finding a non-blank line,
    ' the value returned is null and errFlag is set to true.
    ' errFlag is not modified unless an error occurred, which allows
    ' the calling program to have several calls in a row and then only
    ' has to check errFlag for the last call.
    '*******************************************************************
    Public Shared Function getNonBlankLine(ByVal br As StreamReader, ByRef errFlag As Boolean) As String
        Dim strIn As String

        ' It's possible that an error occured in an earlier invocation. If so,
        ' then just return.
        If errFlag Then Return Nothing

        Do While br.Peek() <> -1
            strIn = br.ReadLine().Trim()
            If strIn.Length > 0 Then Return strIn
        Loop

        ' We only get here if eof was reached
        errFlag = True
        Return Nothing
    End Function

    '*******************************************************************
    ' Reads the input file looking for a tag, then returns the value
    ' found for that tag. For example, a tag might be <Epoch> and
    ' could exist in the file in multiple ways: (1) <Epoch>, next line
    ' has value, next line has </Epoch>, (2) <Epoch>value</Epoch> on a 
    ' single line, (3) <Epoch>, next line has value</Epoch>, and so on.
    ' This method is only intended to be used for XML-style tags that have
    ' simple values and not for tags (such as <Data>) that will likely
    ' have multiple lines of input. If used for a multi-line tag, only the
    ' first line will be extracted as the tag value.
    ' 
    ' br    				file to read from
    ' tag   				Simple "XML-like" tag to search for
    ' errFlag               whether an error occured
    '
    ' Returns a string containing the value field for the given tag,
    ' or null if the tag exists but has no value field or if the
    ' tag doesn't exist. If the tag doesn't exist, errFlag is set to
    ' true. It is not modified unless an error occurred, which allows
    ' the calling program to have several calls in a row and then only
    ' has to check errFlag for the last call.
    '*******************************************************************
    Public Shared Function getSimpleTaggedValue(ByVal br As StreamReader, ByVal tag As String,
                                                ByRef errFlag As Boolean) As String
        Dim endTag, strIn As String

        ' It's possible that an error occured in an earlier invocation. If so,
        ' then just return.
        If errFlag Then Return Nothing

        ' Construct an end tag, which is by replacing the 1st occurence of '<' with '</'
        endTag = Replace(tag, "<", "</", 1, 1)

        strIn = readTillStr(br, tag, HIDE_ERRORS)                        ' readTillStr will worry about upper vs lowercase

        If IsNothing(strIn) Then
            errFlag = True
            Return Nothing
        End If

        strIn = Replace(strIn, tag, "", 1, 1).Trim()        ' remove the tag from the line and see what's left
        If IsNothing(strIn) Or (strIn.Length() <= 0) Then   ' value, if it exists, must be on the next line
            strIn = getNonBlankLine(br, errFlag)
            If errFlag Then Return Nothing
        End If

        strIn = strIn.Trim()

        ' Replace the endTag, if it's there, with a null string. Whatever is left over
        ' is the result we need to return.
        If strIn.Length > 0 Then
            strIn = Replace(strIn, endTag, "").Trim()
        End If
        Return strIn
    End Function

    '*************************************************************
    ' Reads the input file until the expected string is found
    ' or an error occurs.
    '
    ' br            buffered stream to read from
    ' target        string to search for (not case sensitive)
    ' showErrors    true if error messages should be displayed
    '
    ' Returns the target string if it was found, else a null
    ' string is returned.
    '*************************************************************
    Public Shared Function readTillStr(ByVal br As StreamReader, ByVal target As String) As String
        Return readTillStr(br, target, True)
    End Function
    Public Shared Function readTillStr(ByVal br As StreamReader, ByVal target As String, ByVal showErrors As Boolean) As String
        Dim targ, strIn, str2In As String
        Dim errFlag As Boolean = False

        targ = removeWhitespace(LCase(target))

        Do While br.Peek() <> -1
            strIn = getNonBlankLine(br, errFlag)
            If errFlag Then Return Nothing
            str2In = removeWhitespace(LCase(strIn))
            If InStr(str2In, targ) > 0 Then Return strIn ' found what we're looking for
        Loop

        ' If we get here, the target was not found
        If showErrors Then CriticalErrMsg("Expected but did not find '" & target & "'")
        Return Nothing
    End Function

    '********************************************************************
    ' Display a file browser. This method is needed because fileBrowser
    ' is declared as Friend and therefore not visible outside
    ' the assembly in which ASTFileIO is declared (i.e., not visible
    ' outside of methods in ASTFileIO)
    '
    ' filename                  filename user selects
    '
    ' Returns true if user selects a file and sets filename accordingly.
    ' However, true doesn't necessarily mean that the file exists
    ' because the user could have typed in an invalid filename.
    '********************************************************************
    Public Function showFileBrowser(ByRef filename As String) As Boolean
        If Not Me.fileBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then Return False

        filename = Me.fileBrowser.FileName
        Return True
    End Function
End Class
