﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides a class for creating real objects and validating
' that a string has a real value.
'**********************************************************

Option Explicit On
Option Strict On

Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg

Public Class ASTReal

    '********************************************************************************
    ' Define some general methods for validating that a string has a valid real
    ' number in it.
    '********************************************************************************

    '*****************************************************************
    ' Check to see if a valid number was entered, but don't display
    ' any error messages unless flag is SHOW_ERRORS.
    '
    ' This function is overloaded to allow flag to be an optional
    ' parameter. The default is to display error messages.
    '
    ' inputStr             string that has the value to be checked
    ' result               value returned if the data is valid
    ' flag                 flag indicating whether to show errors
    '
    ' returns true if the number was valid, else false
    '*****************************************************************
    Public Shared Function isValidReal(ByVal inputStr As String, ByRef result As Double) As Boolean
        Return isValidReal(inputStr, result, SHOW_ERRORS)
    End Function
    Public Shared Function isValidReal(ByVal inputStr As String, ByRef result As Double, ByVal flag As Boolean) As Boolean
        inputStr = Trim(inputStr)

        If Not Double.TryParse(inputStr, result) Then
            ErrMsg("Either nothing was entered or what was entered" & vbNewLine & "is invalid. Please enter a valid number.",
                   "Invalid Number", flag)
            Return False
        End If

        Return True
    End Function
End Class
