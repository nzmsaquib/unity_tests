﻿'****************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides the ability to load and view star catalogs.
' The catalog data files **MUST** be in the format specifically
' designed for this book. The format can be gleaned by opening
' any of the star catalog data files and examining them. The
' format is straightforward.
'
' The data files provided with this book were created from
' publicly available sources, particularly those data files
' maintained in the NASA HEASARC archives, which are at the URL
' http://heasarc.gsfc.nasa.gov/docs/archive.html as of the
' time this book was written.
'
' Because the catalogs can be so large, static methods and
' data are defined in this class so that the class can easily
' enforce having only one catalog loaded at a time. The calling
' routine must create a class instance so that things get
' initialized properly.
'****************************************************************

Option Explicit On
Option Strict On

Imports System.IO
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTConstellation
Imports ASTUtils.ASTFileIO
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Public Class ASTCatalog
    ' Define the class instance variables, the set of which serve as a 'header' that
    ' provides basic information about the catalog.
    Private Shared cat_type As String               ' type of catalog
    Private Shared sDate As String                  ' most recent date that the catalog was updated
    Private Shared sSource As String                ' source from which the catalog came
    Private Shared sDescription As String           ' brief overall description of the objects in the catalog
    Private Shared nObjects As Integer = 0          ' how many objects are contained in the catalog
    Private Shared nConstellations As Integer = 0   ' how many constellations the objects in the catalog cover
    Private Shared bEpochKnown As Boolean           ' true if the Epoch on which this catalog is based is known
    Private Shared Epoch As Double                  ' the Epoch, if known, to which object coordinates are referenced.
    Private Shared sAuthor As String                ' the author(s) that created the catalog
    Private Shared bmVProvided As Boolean           ' true if the catalog provides visual magnitudes for the objects in the catalog
    Private Shared dmVLimit As Double               ' The limiting visual magnitude that was used to filter the catalog to reduce its size.

    '****************************************************************
    ' Initialize a new instance. This is really only done once and
    ' after that the class instance variable are reused.
    '
    ' prtInstance       instance for the user's scrollable text area
    '****************************************************************
    Sub New()
        ' do nothing
    End Sub
    Sub New(ByVal prtInstance As ASTPrt)
        initStarCatalogs(prtInstance)
    End Sub

    '**************************************************
    ' 'getters' for the catalog instance variables. No
    ' 'setters' are provided because only methods in
    ' this class set the catalog instance variables.
    '**************************************************
    Public Shared Function getCatAuthor() As String
        Return sAuthor
    End Function
    Public Shared Function getCatDate() As String
        Return sDate
    End Function
    Public Shared Function getCatDescription() As String
        Return sDescription
    End Function
    Public Shared Function getCatEpoch() As Double
        Return Epoch
    End Function
    Public Shared Function getCatEpochKnown() As Boolean
        Return bEpochKnown
    End Function
    Public Shared Function getCatmVLimit() As Double
        Return dmVLimit
    End Function
    Public Shared Function getCatmVProvided() As Boolean
        Return bmVProvided
    End Function
    Public Shared Function getCatType() As String
        Return cat_type
    End Function
    Public Shared Function getCatNumConst() As Integer
        Return nConstellations
    End Function
    Public Shared Function getCatNumObjs() As Integer
        Return nObjects
    End Function
    Public Shared Function getCatSource() As String
        Return sSource
    End Function

    '-----------------------------------------------------------
    ' Define a private class and database for the space objects
    ' in the currently loaded catalog.
    '-----------------------------------------------------------
    Private Class SpaceObj
        Private sName As String             ' the object's primary name
        Private sAltName As String          ' object's alternate name
        Private dRA As Double               ' right ascension (in decimal hours) for the object w.r.t. the catalog's Epoch
        Private dDecl As Double             ' declination (in decimal degrees) for the object w.r.t. the catalog's Epoch
        Private bmVKnown As Boolean         ' true if the catalog provides a visual magnitude for the object
        Private dmV As Double               ' object's visual magnitude, if known
        Private sComment As String          ' optional comment contained in the catalog about the object
        Private idx As Integer              ' index into the constellations database for the constellation that this object lies within

        '----------------------------------------------------------
        ' Create a new SpaceObj instance with the values passed in
        '----------------------------------------------------------
        Friend Sub New(ByVal Name As String, ByVal AltName As String,
                       ByVal RA As Double, ByVal Decl As Double,
                       ByVal mVKnown As Boolean, ByVal mV As Double,
                       ByVal Comment As String, ByVal i As Integer)
            sName = Name
            sAltName = AltName
            dRA = RA
            dDecl = Decl
            bmVKnown = mVKnown
            If bmVKnown Then
                dmV = mV
            Else
                dmV = UNKNOWN_mV
            End If
            sComment = Comment
            idx = i
        End Sub

        '-----------------------------------------------------------------------------
        ' Define 'getters' for the space objects. 'Setters' are not needed because
        ' the instance variables are set as part of creating an instance (i.e., new)
        '-----------------------------------------------------------------------------
        Friend Function getObjAltName() As String
            Return Me.sAltName
        End Function
        Friend Function getObjComment() As String
            Return Me.sComment
        End Function
        Friend Function getObjDecl() As Double
            Return Me.dDecl
        End Function
        Friend Function getObjIndex() As Integer
            Return Me.idx
        End Function
        Friend Function getObjmV() As Double
            Return Me.dmV
        End Function
        Friend Function getObjmVKnown() As Boolean
            Return Me.bmVKnown
        End Function
        Friend Function getObjName() As String
            Return Me.sName
        End Function
        Friend Function getObjRA() As Double
            Return Me.dRA
        End Function
    End Class

    ' Database of the objects in the currently loaded star catalog
    Private Shared StarCatalog As List(Of SpaceObj) = New List(Of SpaceObj)

    '******************************************************
    ' Public methods for getting star catalog object data
    '
    ' idx           which star catalog object is needed
    '               (assumes 0-based indexing!)
    '
    ' Warning: no check is made to be sure idx is in range
    '******************************************************
    Public Shared Function getCatObjAltName(ByVal idx As Integer) As String
        Return StarCatalog(idx).getObjAltName
    End Function
    Public Shared Function getCatObjComment(ByVal idx As Integer) As String
        Return StarCatalog(idx).getObjComment
    End Function
    Public Shared Function getCatObjConstIdx(ByVal idx As Integer) As Integer
        Return StarCatalog(idx).getObjIndex
    End Function
    Public Shared Function getCatObjDecl(ByVal idx As Integer) As Double
        Return StarCatalog(idx).getObjDecl
    End Function
    Public Shared Function getCatObjmV(ByVal idx As Integer) As Double
        Return StarCatalog(idx).getObjmV
    End Function
    Public Shared Function getCatObjmVKnown(ByVal idx As Integer) As Boolean
        Return StarCatalog(idx).getObjmVKnown
    End Function
    Public Shared Function getCatObjName(ByVal idx As Integer) As String
        Return StarCatalog(idx).getObjName
    End Function
    Public Shared Function getCatObjRA(ByVal idx As Integer) As Double
        Return StarCatalog(idx).getObjRA
    End Function

    ' Define the ways a star catalog can be sorted and a method for converting the
    ' enum type to a printable string. The following items must be kept in synch!
    Public Enum CatalogSortField
        CONSTELLATION = 0               ' by constellation name
        CONST_AND_OBJNAME = 1           ' by constellation name and then by object names
        OBJNAME = 2                     ' by object name
        OBJ_ALTNAME = 3                 ' by an object's alternate name
        RA = 4                          ' by an object's right ascension
        DECL = 5                        ' by an object's declination
        VISUAL_MAGNITUDE = 6            ' by an object's visual magnitude
    End Enum
    Private Shared EnumStrings(6) As String
    Private Shared Sub LoadEnumStrs()
        EnumStrings(0) = "Constellation"
        EnumStrings(1) = "Constellation and Object Name"
        EnumStrings(2) = "Object's Name"
        EnumStrings(3) = "Object's Alternate Name"
        EnumStrings(4) = "Object's Right Ascension"
        EnumStrings(5) = "Object's Declination"
        EnumStrings(6) = "Object's Visual Magnitude"
    End Sub
    ' Gets a printable string for the CatalogSortField enumerated type
    Public Shared Function sortFieldToStr(ByVal sortField As CatalogSortField) As String
        Return EnumStrings(sortField)
    End Function

    '----------------------------------------------------------------------------
    ' Define some other data items that are used only in this class.
    '----------------------------------------------------------------------------
    ' we'll need an instance of ASTPrt so that we can do output to the user's scrollable text area
    Private Shared prt As ASTPrt = Nothing

    ' Since the catalogs can be quite large, it may take a long time to display all objects. The MAX_OBJS_TO_PRT constant
    ' is the maximum number of objects to display before asking the user if they want to continue. However, if there
    ' are OBJS_LIMIT or less, this limit is ignored since the objects will still print in a reasonable amount of time.
    Private Const MAX_OBJS_TO_PRT = 1000
    Private Const OBJS_LIMIT = 2500

    ' Prompts and other data for allowing the user to find a star catalog
    Private Const dataPrompt = "Open a Star Catalog ..."
    Private Const dataExt = ".dat"
    Private Const dataFilter = "Star Catalog|*.*"

    ' avoid initializing more than once
    Private Shared classInitialized As Boolean = False      ' keep track of whether the class has been initialized
    Private Shared catalogLoaded As Boolean = False         ' keep track of whether a catalog is loaded

    ' As per the format requirements, objects in the catalog data files are to be sorted
    ' in ascending order by constellation and then in ascending order by object name
    ' within the constellations. However, once a catalog is loaded, the user may want
    ' the catalog sorted in some other order. The boolean saveCurrentSortOrder is needed
    ' because we need a way to pass the desired sorting order to the various comparators defined below.
    ' These two "save" flags are also used to avoid sorting when we don't need to.
    Private Shared saveCurrentSortOrder As Boolean = ASCENDING_ORDER
    Private Shared saveCurrentSortField As CatalogSortField = CatalogSortField.CONST_AND_OBJNAME

    '*************************************************************************************************************
    ' Public methods for getting/displaying/manipulating catalog info
    '*************************************************************************************************************

    '*************************************************************
    ' Clears all the currently loaded catalog data including all
    ' space objects in the catalog
    '*************************************************************
    Public Shared Sub clearCatalogAndSpaceObjects()
        ' Clear and recreate the various objects that need to be recreated.
        ' First, clear the catalog header information
        cat_type = ""
        sDate = ""
        sSource = ""
        sDescription = ""
        nObjects = 0
        nConstellations = 0
        Epoch = DEFAULT_EPOCH
        bEpochKnown = False
        bmVProvided = False
        dmVLimit = UNKNOWN_mV
        sAuthor = ""

        saveCurrentSortOrder = ASCENDING_ORDER
        saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME

        ' Now delete all of the space objects in the star catalog
        ' In some languages, we'd need to loop through the space catalog
        ' and delete objects individually.
        StarCatalog.Clear()

        StarCatalog = New List(Of SpaceObj)

        catalogLoaded = False
    End Sub

    '*************************************************************
    ' Displays all space objects in the currently loaded catalog
    ' by however the catalog is currently sorted.
    '
    ' mVFilter          filter for displaying only those objects
    '                   that are at least as bright as mVFilter
    '
    ' This method is overloaded so that passing mVFilter is
    ' optional. If not specified, all objects are displayed.
    '*************************************************************
    Public Shared Sub displayAllCatalogObjects()
        displayAllCatalogObjects(UNKNOWN_mV)
    End Sub
    Public Shared Sub displayAllCatalogObjects(ByVal mVFilter As Double)
        Dim i, prtLimit As Integer
        Dim lineCount As Integer = 0
        Dim queryForm As ASTQuery = New ASTQuery()
        Dim showObj As Boolean

        If Not isInitAndLoaded() Then Return

        ' See if we need to bother about pausing during display
        prtLimit = OBJS_LIMIT + 10          ' this really means to ignore pausing during display
        If getCatNumObjs() > OBJS_LIMIT Then
            If AbortMsg("Displaying " & getCatNumObjs() & " objects may take a long time.") Then Return

            If queryForm.showQueryForm("Enter Max # of Objects to Display at a Time" & vbNewLine &
                             "(Recommend no More Than " & MAX_OBJS_TO_PRT & " Objects)") = Windows.Forms.DialogResult.OK Then
                If Not isValidInt(queryForm.getData1(), prtLimit, HIDE_ERRORS) Then prtLimit = MAX_OBJS_TO_PRT
            End If
        End If

        displayObjHeader()

        For i = 0 To getCatNumObjs() - 1
            lineCount = lineCount + 1
            If lineCount > prtLimit Then
                If AbortMsg("Displayed " & i & " of " & getCatNumObjs() & " objects") Then Return
                lineCount = 0
            End If

            ' See if this object should be filtered out
            If StarCatalog(i).getObjmVKnown() Then
                showObj = (StarCatalog(i).getObjmV <= mVFilter)
            Else
                showObj = True      ' show object even if we don't know its mV value
            End If

            If showObj Then displayObject(i + 1, i)
        Next
    End Sub

    '****************************************************************
    ' Displays all space objects in the currently loaded catalog
    ' by constellation.
    '
    ' idx           index into constellations database for the
    '               constellation of interest
    ' sortOrder     sort in ascending order if ASCENDING_ORDER, else
    '               sort in descending order
    ' mVFilter      filter for displaying only those objects
    '               that are at least as bright as mVFilter
    '
    ' This method is overloaded so that passing mVFilter is
    ' optional. If not specified, all objects are displayed.  
    '****************************************************************
    Public Shared Sub displayAllObjsByConstellation(ByVal idx As Integer, ByVal sortOrder As Boolean)
        displayAllObjsByConstellation(idx, sortOrder, UNKNOWN_mV)
    End Sub
    Public Shared Sub displayAllObjsByConstellation(ByVal idx As Integer, ByVal sortOrder As Boolean,
                                            ByVal mVFilter As Double)
        Dim i As Integer
        Dim iStart, iEnd, iNumObjs As Integer         ' How many objects to print and where they stop/end in the Star Catalog
        Dim prtLimit, lineCount As Integer
        Dim queryForm As ASTQuery = New ASTQuery()
        Dim showObj As Boolean

        If Not isInitAndLoaded() Then Return

        If (idx < 0) Or (idx > getNumConstellations() - 1) Then
            ErrMsg("The Constellation requested does not exist", "Invalid Constellation Name")
            Return
        End If

        iStart = -1
        iEnd = -1
        iNumObjs = 0
        lineCount = 0

        ' We need to be sure the star catalog is already sorted by constellation name
        sortStarCatalog(CatalogSortField.CONST_AND_OBJNAME, sortOrder)   ' this will sort only if we need to

        ' First, go through the objects and find out how many we need to display
        For i = 0 To getCatNumObjs() - 1
            If StarCatalog(i).getObjIndex = idx Then
                iNumObjs = iNumObjs + 1
                iEnd = i
                If iStart < 0 Then iStart = i
            ElseIf iStart >= 0 Then         ' we reached the end of the objects in this constellation
                Exit For
            End If
        Next

        ' See if we need to bother about pausing during display
        prtLimit = OBJS_LIMIT + 10          ' this really means to ignore pausing during display
        If iNumObjs > OBJS_LIMIT Then
            If AbortMsg("Displaying " & iNumObjs & " objects may take a long time.") Then Return
            If queryForm.showQueryForm("Enter Max # of Objects to Display at a Time" & vbNewLine &
                             "(Recommend no More Than " & MAX_OBJS_TO_PRT & " Objects)") = Windows.Forms.DialogResult.OK Then
                If Not isValidInt(queryForm.getData1(), prtLimit, HIDE_ERRORS) Then prtLimit = MAX_OBJS_TO_PRT
            End If
        End If

        prt.println("The currently loaded catalog has " & iNumObjs & " objects in the constellation " &
                    getConstName(idx) & " (" & getConstAbbrevName(idx) + ")")
        displayObjHeader()
        If (iNumObjs <= 0) Then Return

        For i = iStart To iEnd
            lineCount = lineCount + 1
            If lineCount > prtLimit Then
                If AbortMsg("Displaying " & (i - iStart + 1) & " of " & iNumObjs & " objects") Then Return
                lineCount = 0
            End If

            ' See if this object should be filtered out
            If StarCatalog(i).getObjmVKnown() Then
                showObj = (StarCatalog(i).getObjmV <= mVFilter)
            Else
                showObj = True      ' show object if we don't know its mV value
            End If

            If showObj Then displayObject(i - iStart + 1, i)
        Next
    End Sub

    '****************************************************************
    ' Displays all space objects in the currently loaded catalog by
    ' range, however the catalog is currently sorted.
    '
    ' nStart        the range start (assuming 0-based numbering
    '               so that 0 is the 1st item)
    ' nEnd          the end of the range (assuming 0-based numbering)
    ' mVFilter      filter for displaying only those objects
    '               that are at least as bright as mVFilter
    '
    ' This method is overloaded so that passing mVFilter is
    ' optional. If not specified, all objects are displayed.  
    '****************************************************************
    Public Shared Sub displayAllObjsByRange(ByVal nStart As Integer, ByVal nEnd As Integer)
        displayAllObjsByRange(nStart, nEnd, UNKNOWN_mV)
    End Sub
    Public Shared Sub displayAllObjsByRange(ByVal nStart As Integer, ByVal nEnd As Integer,
                                            ByVal mVFilter As Double)
        Dim iStart, iEnd, iNumObjs, lineCount, prtLimit As Integer
        Dim queryForm As ASTQuery = New ASTQuery()
        Dim showObj As Boolean

        If Not isInitAndLoaded() Then Return

        ' First, be sure that nStart <= nEnd and that both are in range
        If nStart > nEnd Then
            iStart = nEnd
            iEnd = nStart
        Else
            iStart = nStart
            iEnd = nEnd
        End If
        If iStart < 0 Then iStart = 0
        If nEnd > getCatNumObjs() - 1 Then iEnd = getCatNumObjs() - 1

        lineCount = 0
        iNumObjs = iEnd - iStart + 1        ' number of objects to display

        ' See if we need to bother about pausing during display
        prtLimit = OBJS_LIMIT + 10          ' this really means to ignore pausing during display
        If iNumObjs > OBJS_LIMIT Then
            If AbortMsg("Displaying " & iNumObjs & " objects may take a long time.") Then Return
            If queryForm.showQueryForm("Enter Max # of Objects to Display at a Time" & vbNewLine &
                             "(Recommend no More Than " & MAX_OBJS_TO_PRT & " Objs)") = Windows.Forms.DialogResult.OK Then
                If Not isValidInt(queryForm.getData1(), prtLimit, HIDE_ERRORS) Then prtLimit = MAX_OBJS_TO_PRT
            End If
        End If

        prt.println("The currently loaded catalog has " & iNumObjs & " objects in the requested index range")
        displayObjHeader()

        For i = iStart To iEnd
            lineCount = lineCount + 1
            If lineCount > prtLimit Then
                If AbortMsg("Displaying " & (i - iStart + 1) & " of " & iNumObjs & " objects") Then Return
                lineCount = 0
            End If

            ' See if this object should be filtered out
            If StarCatalog(i).getObjmVKnown() Then
                showObj = (StarCatalog(i).getObjmV <= mVFilter)
            Else
                showObj = True      ' show object if we don't know its mV value
            End If

            If showObj Then displayObject(i + 1, i)
        Next
    End Sub

    '*******************************************
    ' Displays the catalog header information
    '*******************************************
    Public Shared Sub displayCatalogInfo()
        Dim str As String

        If Not isInitAndLoaded() Then Return

        prt.println("Catalog Type: " & getCatType(), CENTERTXT)
        prt.println(getCatDescription(), CENTERTXT)
        If getCatmVProvided() Then
            str = Format(getCatmVLimit, mVFormat)
        Else
            str = "***Visual Magnitude not provided***"
        End If
        prt.println("Limiting Visual Magnitude for Catalog: " & str, CENTERTXT)
        If getCatEpochKnown() Then
            str = Format(getCatEpoch, epochFormat)
        Else
            str = "***Epoch not provided***"
        End If
        prt.println("Catalog Epoch: " & str, CENTERTXT)
        prt.println("Catalog Author(s): " & getCatAuthor(), CENTERTXT)
        prt.println()
        prt.println("Catalog Date: " & getCatDate())
        prt.println("Original Source: " & getCatSource())
        prt.println()
        prt.println("Catalog covers " & getCatNumConst() & " Constellations and has a total of " &
                    getCatNumObjs() & " Objects")
    End Sub

    '*******************************************************************
    ' Displays all information in the catalog about the i-th space
    ' object entry in the currently loaded and sorted catalog
    '
    ' i             which object to display (assumes 0-based indexing).
    '*******************************************************************
    Public Shared Sub displayFullObjInfo(ByVal i As Integer)
        Dim idx As Integer
        Dim strRA, strDecl As String

        If Not isInitAndLoaded() Then Return

        If (i < 0) Or (i >= getCatNumObjs()) Then
            prt.println("The object specified does not exist in the currently loaded catalog")
            Return
        End If

        idx = StarCatalog(i).getObjIndex()
        strRA = timeToStr(StarCatalog(i).getObjRA(), HMSFORMAT)
        strDecl = angleToStr(StarCatalog(i).getObjDecl(), DMSFORMAT)

        prt.println("The object named '" & StarCatalog(i).getObjName() & "' is in the constellation " &
                    getConstName(idx) & " (" & getConstAbbrevName(idx) & ")")
        prt.println("It's alternate name (if any) in this catalog is: " & StarCatalog(i).getObjAltName())
        prt.println("The object is located at " & strRA & " RA, " & strDecl & " Decl")
        If StarCatalog(i).getObjmVKnown Then
            prt.println("It's visual magnitude is " & Format(StarCatalog(i).getObjmV, mVFormat))
        Else
            prt.println("It's visual magnitude is not given in the catalog")
        End If
        prt.println("Additional information (if any) in the catalog about this object: " &
                    StarCatalog(i).getObjComment())
    End Sub

    '*****************************************************************
    ' Finds an object in the current catalog given its alternate name.
    ' Searches are not case sensitive.
    '
    ' searchStr             The name to search for. An exact match,
    '                       ignoring whitespace and case, is required.
    '
    ' Returns index into the StarCatalog list if successful, else -1.
    ' 0-based indexing is assumed!
    '*****************************************************************
    Public Shared Function findObjByAltName(ByVal searchStr As String) As Integer
        Dim targ As String
        Dim i As Integer

        targ = removeWhitespace(searchStr)

        For i = 0 To nObjects - 1
            ' In Visual Basic, the 'TRUE' parameter means to ignore case
            If String.Compare(targ, removeWhitespace(StarCatalog(i).getObjAltName()), True) = 0 Then Return i
        Next

        Return -1       ' not found
    End Function

    '**********************************************************************
    ' Searches the currently loaded star catalog and returns an index into
    ' the database for all objects that contain the target substring
    ' in their 'comments' field. Searches are not case sensitive.
    '
    ' targ              target substring to search for
    ' result            dynamic array of integers that will contain
    '                   the indices into the star catalog
    '                   for the requested objects.

    ' If unsuccessful, the list is empty and a -1 is returned. If
    ' successful, return the number of entries - 1 that contained the
    ' target string. 0-based indexing is assumed for the indices!
    '**********************************************************************
    Public Shared Function findObjsByComments(ByVal targ As String, ByRef result() As Integer) As Integer
        Dim n As Integer = -1
        Dim i As Integer
        Dim targStr, tmpStr As String

        targStr = LCase(removeWhitespace(targ))
        ReDim result(0)

        For i = 0 To nObjects - 1
            tmpStr = LCase(removeWhitespace(StarCatalog(i).getObjComment()))
            If InStr(tmpStr, targStr) <> 0 Then
                n = n + 1
                If (n > 0) Then ReDim Preserve result(UBound(result) + 1)
                result(n) = New Integer
                result(n) = i
            End If
        Next

        Return n
    End Function

    '*****************************************************************
    ' Finds an object in the current catalog given its name.
    ' Searches are not case sensitive.
    '
    ' searchStr             The name to search for. An exact match,
    '                       ignoring whitespace and case, is required.
    '
    ' Returns index into the StarCatalog list if successful, else -1.
    ' 0-based indexing is assumed!
    '*****************************************************************
    Public Shared Function findObjByName(ByVal searchStr As String) As Integer
        Dim targ As String
        Dim i As Integer

        targ = removeWhitespace(searchStr)

        For i = 0 To nObjects - 1
            ' In Visual Basic, the 'TRUE' parameter means to ignore case
            If String.Compare(targ, removeWhitespace(StarCatalog(i).getObjName()), True) = 0 Then Return i
        Next

        Return -1           ' not found
    End Function

    '**********************************************************************
    ' Puts up a browser window and gets a star catalog filename.
    '
    ' fullFilename        full name (file and path) of the catalog file
    '
    ' Returns true if user selected a file. The file selected is returned
    ' in fullFileName. This routine does **not** check to see if the file is
    ' a valid catalog, but it **does** check to see that the file exists.
    '**********************************************************************
    Public Shared Function getCatFileToOpen(ByRef fullFilename As String) As Boolean
        Dim fChooser As ASTFileIO

        fChooser = New ASTFileIO(dataPrompt, dataFilter, dataExt)
        If Not fChooser.fileBrowser.ShowDialog() = Windows.Forms.DialogResult.OK Then Return False

        fullFilename = fChooser.fileBrowser.FileName
        ' See if the file exists
        If Not doesFileExist(fullFilename) Then
            ErrMsg("The Star Catalog file specified does not exist", "Catalog Does Not Exist")
            Return False
        End If

        Return True
    End Function

    '*************************************************
    ' Checks to see if a star catalog has been loaded
    '
    ' Returns true if a star catalog has been loaded.
    '*************************************************
    Public Shared Function isCatalogLoaded() As Boolean
        Return catalogLoaded And classInitialized
    End Function

    '********************************************************
    ' Loads a catalog from disk. The catalog file must have
    ' all the fields even If they have no value for them and
    ' the catalog must be in the format designed for this book.
    '
    ' filename      Full filename (including path) to load
    '
    ' Returns true if successful, else false.
    '********************************************************
    Public Shared Function loadFormattedStarCatalog(ByVal filename As String) As Boolean
        Dim delimiter As Char = ","c                ' fields are comma delimited
        Dim br As StreamReader
        Dim bTmp As Boolean             ' a throwaway value that is never used but is needed below
        Dim skipline As Boolean
        Dim errorOccurred As Boolean = False
        Dim strIn As String
        Dim targ As String = "</data>"
        Dim sParts() As String
        Dim Name, AltName, Comment, ConstAbbrvName As String
        Dim ConstIdx As Integer
        Dim RA, Decl, mV As Double
        Dim mVGiven As Boolean

        clearCatalogAndSpaceObjects()

        ' Open and read in the star catalog file
        br = New System.IO.StreamReader(filename)

        If IsNothing(readTillStr(br, "<Catalog>", HIDE_ERRORS)) Then
            ErrMsg("The data file specified is not a Star Catalog" & vbNewLine &
                   "data file. Try again ...", "Invalid Star Catalog")
            Return False
        End If

        ' Get the basic catalog header information. Each tag must be
        ' present or else it is an error. A check could be made after
        ' each invocation of getSimpleTaggedValue, but that would
        ' complicate the code and make it messier. So, we let the
        ' getSimpleTaggedValue set the error flag for us and only
        ' check once at the end.
        cat_type = getSimpleTaggedValue(br, "<CatalogType>", errorOccurred)
        sDescription = getSimpleTaggedValue(br, "<Description>", errorOccurred)

        strIn = getSimpleTaggedValue(br, "<VisualMagnitudeLimit>", errorOccurred)
        If (strIn.Length() <= 0) Or errorOccurred Then             ' tag may have been there w/o a value so take defaults
            dmVLimit = UNKNOWN_mV
            bmVProvided = False
        Else
            bTmp = isValidReal(strIn, dmVLimit, HIDE_ERRORS)
            bmVProvided = True
        End If

        strIn = getSimpleTaggedValue(br, "<Epoch>", errorOccurred)
        If (strIn.Length() <= 0) Or errorOccurred Then
            Epoch = DEFAULT_EPOCH
            bEpochKnown = False
        Else
            bTmp = isValidReal(strIn, Epoch, HIDE_ERRORS)
            bEpochKnown = True
        End If

        sAuthor = getSimpleTaggedValue(br, "<Author>", errorOccurred)
        sDate = getSimpleTaggedValue(br, "<Date>", errorOccurred)
        sSource = getSimpleTaggedValue(br, "<Source>", errorOccurred)

        ' Now get the data about the space objects
        strIn = readTillStr(br, "<Data>")
        If IsNothing(strIn) Then
            errorOccurred = True
        End If

        Do While br.Peek() <> -1
            strIn = getNonBlankLine(br, errorOccurred)
            If IsNothing(strIn) Or errorOccurred Then
                strIn = "</data>"
            Else
                targ = removeWhitespace(LCase(strIn))
            End If
            If InStr(targ, "</data>") > 0 Then Exit Do ' found end of data section

            skipline = False
            RA = UNKNOWN_RA_DECL
            Decl = UNKNOWN_RA_DECL

            sParts = strIn.Split(delimiter)

            ' Get the various fields, but skip if RA or Decl is missing
            If sParts.Length <> 7 Then Continue Do ' there should be exactly 7 fields; if not, skip

            Name = sParts(0).Trim()
            ConstAbbrvName = sParts(4).Trim()
            AltName = sParts(5).Trim()
            Comment = sParts(6).Trim()

            ' Get RA, Decl, and mV. Skip if RA or Decl is missing
            If Not isValidReal(sParts(1), RA, HIDE_ERRORS) Then skipline = True
            If Not isValidReal(sParts(2), Decl, HIDE_ERRORS) Then skipline = True
            If Not isValidReal(sParts(3), mV, HIDE_ERRORS) Then
                mVGiven = False
                mV = UNKNOWN_mV
            Else
                mVGiven = True
            End If

            If (skipline) Then Continue Do

            ConstIdx = findConstellationByAbbrvName(ConstAbbrvName)
            If (ConstIdx < 0) Then Continue Do      ' Skip object if constellation not found
            StarCatalog.Add(New SpaceObj(Name, AltName, RA, Decl, mVGiven, mV, Comment, ConstIdx))
        Loop

        If errorOccurred Then       ' if an error occurred, then reset back to having no catalog
            clearCatalogAndSpaceObjects()
        Else
            nObjects = StarCatalog.Count()
            nConstellations = countConstellations()
            catalogLoaded = True
            saveCurrentSortOrder = ASCENDING_ORDER
            saveCurrentSortField = CatalogSortField.CONST_AND_OBJNAME
        End If

        ' Close the file and quit. Note that errorOccurred is the opposite of what we need to return
        br.Close()
        Return Not errorOccurred
    End Function

    '***************************************************************
    ' Sorts the catalog of space objects.
    '
    ' sortField         what field to sort on
    ' sortOrder         sort in ascending order if ASCENDING_ORDER,
    '                   else sort in descending order
    '***************************************************************
    Public Shared Sub sortStarCatalog(ByVal sortField As CatalogSortField, ByVal sortOrder As Boolean)
        If Not isInitAndLoaded() Or ((sortOrder = saveCurrentSortOrder) And (sortField = saveCurrentSortField)) Then
            ' Nothing to do
            Return
        End If

        saveCurrentSortOrder = sortOrder
        saveCurrentSortField = sortField

        Select Case sortField
            Case CatalogSortField.CONSTELLATION
                StarCatalog.Sort(AddressOf constComparator)
            Case CatalogSortField.CONST_AND_OBJNAME
                StarCatalog.Sort(AddressOf constAndObjNameComparator)
            Case CatalogSortField.OBJNAME
                StarCatalog.Sort(AddressOf spaceObjNameComparator)
            Case CatalogSortField.OBJ_ALTNAME
                StarCatalog.Sort(AddressOf spaceObjAltNameComparator)
            Case CatalogSortField.RA
                StarCatalog.Sort(AddressOf spaceObjRAComparator)
            Case CatalogSortField.DECL
                StarCatalog.Sort(AddressOf spaceObjDeclComparator)
            Case CatalogSortField.VISUAL_MAGNITUDE
                StarCatalog.Sort(AddressOf spaceObjmVComparator)
        End Select
    End Sub

    '----------------------------------------------------------------------------------------------
    ' Private methods used only in this class.
    '----------------------------------------------------------------------------------------------

    '----------------------------------------------------------------------
    ' Counts the number of unique constellations in the currently
    ' loaded catalog. The count is put into the catalog data structure.
    ' The objects in the catalog **MUST** be grouped by constellation
    ' before this method is invoked. All properly formatted catalogs
    ' are stored on disk sorted by constellation, so they should be OK
    ' when loaded. How the objects are sorted does not matter as long
    ' as they are grouped by constellation.
    '
    ' Note that the StarCatalog database has an index that points
    ' into the constellations database, so we can compare the indices
    ' without needing to actually find out the constellation name
    ' and do a string compare.
    '
    ' Returns a count of the number of unique constellations in the
    ' currently loaded star catalog.
    '----------------------------------------------------------------------
    Private Shared Function countConstellations() As Integer
        Dim count As Integer = 0
        Dim idx As Integer = -1
        Dim i As Integer

        For i = 0 To StarCatalog.Count() - 1
            If StarCatalog(i).getObjIndex() <> idx Then
                count = count + 1
                idx = StarCatalog(i).getObjIndex()
            End If
        Next
        nConstellations = count
        Return count
    End Function

    '---------------------------------------------------------
    ' Displays catalog information for one specific object
    ' from the catalog.
    '
    ' i         Since this method is intended to be invoked
    '           from inside a loop, this parameter is a
    '           counter from the calling method's loop.
    ' idx       index into the StarCatalog for the object
    '           to display
    '---------------------------------------------------------
    Private Shared Sub displayObject(ByVal i As Integer, ByVal idx As Integer)
        Dim sTmp, strRA, strDecl As String
        Dim sConstAbbrvName As String = " "
        Dim iConstName As Integer

        If Not isInitAndLoaded() Then Return
        If (idx < 0) Or (idx > getCatNumObjs()) Then Return

        strRA = timeToStr(StarCatalog(idx).getObjRA(), HMSFORMAT)
        strDecl = angleToStr(StarCatalog(idx).getObjDecl(), DMSFORMAT)

        If StarCatalog(idx).getObjmVKnown() Then
            sTmp = Format(StarCatalog(idx).getObjmV(), mVFormat)
        Else
            sTmp = "  ? "
        End If

        iConstName = StarCatalog(idx).getObjIndex
        If (iConstName >= 0) And (iConstName < getNumConstellations()) Then
            sConstAbbrvName = getConstAbbrevName(iConstName)
        Else
            sConstAbbrvName = "ERR"
        End If

        ' This format string must match the one in displayObjHeader
        prt.println(String.Format("{0,5:d} {1,-18:s} {2,12:s} {3,16:s} {4,6:s}   {5,-12:s}  {6,-5:s}",
                                  i, StarCatalog(idx).getObjName(), strRA, strDecl, sTmp,
                                  StarCatalog(idx).getObjAltName(), sConstAbbrvName))
    End Sub

    '-----------------------------------------------------------
    ' Displays a header for the objects that will be displayed
    ' after the header. This method must be kept in synch with
    ' displayObject so that the header and the columns that are
    ' printed are consistent.
    '-----------------------------------------------------------
    Private Shared Sub displayObjHeader()
        If Not isInitAndLoaded() Then Return
        prt.println(String.Format("      {0,-18:s} {1,-12:s} {2,-16:s} {3,6:s}   {4,-12:s} {5,5:s}",
                                  "Obj Name", "      RA", "        Decl", "mV ", "Alt Name", "Const"))
    End Sub

    '------------------------------------------------------
    ' Does a one-time initialization required for loading
    ' or using star catalogs
    '
    ' prtInstance 		instance for performing output to
    '                   the application's scrollable text
    '                   output area
    '------------------------------------------------------
    Private Shared Sub initStarCatalogs(ByVal prtInstance As ASTPrt)
        If classInitialized Then Return ' we've already initialized, so just return

        prt = prtInstance

        LoadEnumStrs()

        clearCatalogAndSpaceObjects()
        classInitialized = True

        If IsNothing(prtInstance) Then
            CriticalErrMsg("An output text area cannot be null. Aborting program ...", ABORT_PROG)
        End If

    End Sub

    '-----------------------------------------------------------
    ' Checks to see if this class has been properly initialized
    ' and a catalog has been loaded.
    '-----------------------------------------------------------
    Private Shared Function isInitAndLoaded() As Boolean
        If Not classInitialized Then
            CriticalErrMsg("Catalogs have not been initialized ... Catalog data" & vbNewLine &
                           "cannot be displayed in the scrollable text area. Aborting program ...", ABORT_PROG)
        End If

        Return isCatalogLoaded()
    End Function

    '-------------------------------------------------------------------------------------------
    ' Comparators for sorting the objects in a catalog
    '-------------------------------------------------------------------------------------------

    '-------------------------------------------------------------
    ' Comparator for sorting catalog objects by the Constellation
    ' they are in, and then by the object's name
    '-------------------------------------------------------------
    Private Shared Function constAndObjNameComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        returnValue = StrComp(getConstName(obj1.getObjIndex()), getConstName(obj2.getObjIndex()))

        ' If returnValue is 0, then the two objects are in the same constellation. So sort them
        ' by their object name. We need to do a special alphanumeric comparison so that, for example,
        ' A9 sorts before A11
        If returnValue = 0 Then returnValue = compareAlphaNumeric(obj1.getObjName(), obj2.getObjName())

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function

    '------------------------------------------------
    ' Comparator for sorting catalog objects by the
    ' constellation they are in.
    '------------------------------------------------
    Private Shared Function constComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        returnValue = StrComp(getConstName(obj1.getObjIndex()), getConstName(obj2.getObjIndex()))

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function

    '------------------------------------------------
    ' Comparator for sorting catalog objects by an
    ' object's alternate name.
    '------------------------------------------------
    Private Shared Function spaceObjAltNameComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        ' We need to do a special alphanumeric comparison so that, for example,
        ' A9 sorts before A11
        returnValue = compareAlphaNumeric(obj1.getObjAltName(), obj2.getObjAltName())

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function

    '----------------------------------------
    ' Comparator for sorting catalog objects
    ' by declination
    '----------------------------------------
    Private Shared Function spaceObjDeclComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        If obj1.getObjDecl() > obj2.getObjDecl() Then
            returnValue = 1
        ElseIf obj1.getObjDecl() < obj2.getObjDecl() Then
            returnValue = -1
        Else
            returnValue = 0
        End If

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function

    '--------------------------------------------
    ' Comparator for sorting catalog objects by
    ' visual magnitude
    '--------------------------------------------
    Private Shared Function spaceObjmVComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        If obj1.getObjmV() > obj2.getObjmV() Then
            returnValue = 1
        ElseIf obj1.getObjmV() < obj2.getObjmV() Then
            returnValue = -1
        Else
            returnValue = 0
        End If

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function

    '------------------------------------------------
    ' Comparator for sorting catalog objects by an
    ' object's name.
    '------------------------------------------------
    Private Shared Function spaceObjNameComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        ' We need to do a special alphanumeric comparison so that, for example,
        ' A9 sorts before A11
        returnValue = compareAlphaNumeric(obj1.getObjName(), obj2.getObjName())

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function

    '----------------------------------------
    ' Comparator for sorting catalog objects
    ' by right ascension
    '----------------------------------------
    Private Shared Function spaceObjRAComparator(ByVal obj1 As SpaceObj, ByVal obj2 As SpaceObj) As Integer
        Dim returnValue As Integer

        If obj1.getObjRA() > obj2.getObjRA() Then
            returnValue = 1
        ElseIf obj1.getObjRA() < obj2.getObjRA() Then
            returnValue = -1
        Else
            returnValue = 0
        End If

        If (saveCurrentSortOrder = ASCENDING_ORDER) Then
            Return returnValue
        Else
            Return -1 * returnValue
        End If
    End Function
End Class
