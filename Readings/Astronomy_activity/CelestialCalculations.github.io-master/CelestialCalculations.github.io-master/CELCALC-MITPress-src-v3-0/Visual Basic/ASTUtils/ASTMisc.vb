﻿'******************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Defines miscellaneous constants and other items that are used
' throughout the programs. They are collected here to ensure
' uniformity across all the programs. 
'******************************************************************

Option Explicit On
Option Strict On

Public Class ASTMisc

    '************************************************************************************
    ' Define various flags that indicate what to do in several of the utility methods
    '************************************************************************************
    Public Const SHOW_ERRORS = True                         ' Show errors when doing validations
    Public Const HIDE_ERRORS = Not SHOW_ERRORS              ' Don't show errors when doing validations to let invoking routine display its own msgs.

    Public Const ABORT_PROG = True                          ' Whether to abort the program if an error is detected

    Public Const HMSFORMAT = True                           ' Display time in HMS Format
    Public Const DMSFORMAT = HMSFORMAT                      ' Display angles in DMS format
    Public Const DECFORMAT = Not HMSFORMAT                  ' Display time/angles in decimal format

    Public Const POS_DIRECTION = True                       ' Direction is positive (e.g., North, East)
    Public Const NEG_DIRECTION = Not POS_DIRECTION          ' Direction is negative (e.g., South, West)

    Public Const ASCENDING_ORDER = True                     ' Sort lists in ascending order
    Public Const DESCENDING_ORDER = Not ASCENDING_ORDER     ' Sort lists in descending order

    '************************************************************************************
    ' Define various constants relative to astronomy
    '************************************************************************************
    Public Const AST_UNDEFINED = 99999999.999999       ' Value indicating that something is undefined

    Public Const DEFAULT_EPOCH = 2000.0                ' Default Epoch to use when none is specified
    Public Const UNKNOWN_RA_DECL = AST_UNDEFINED       ' Value when RA or Decl is unknown. Any object with this RA or Decl value must be ignored!
    Public Const UNKNOWN_mV = AST_UNDEFINED            ' Value to use when the visual magnitude is unknown
    Public Const mV_NAKED_EYE = 6.5                    ' Approximate visual magnitude limit for objects visible with the naked eye
    Public Const mV_BINOCULARS = 10.0                  ' Approximate visual magnitude limit for objects visible with binoculars
    Public Const mV_10INCH = 14.7                      ' Approximate visual magnitude limit for objects visible with a 10-inch telescope

    '*************************************************************************
    ' Define various constants related to creating star charts. These
    ' values are likely to need to be adjusted (Or computed dynamically)
    ' to match a particular computer's graphics resolution.
    '*************************************************************************/

    'Define radius of a dot to represent a magnitude 0 object And the max/min size of dots.
    Public Const mV0RADIUS = 5
    Public Const mVMAX_RADIUS = 12
    Public Const mVMIN_RADIUS = 1

    ' Since star catalogs can be quite large, it may take a long time to plot all objects.
    ' The MAX_OBJS_TO_PLOT constant is the recommended maximum number of objects to plot
    ' before asking the user if they want to continue. However, if there are OBJS_LIMIT or
    ' less, this limit is ignored since the objects will still plot in a reasonable amount of time.
    ' The proper value of these constants depends upon a particular computer's CPU speed
    ' and its graphics card speed.
    Public Const MAX_OBJS_TO_PLOT = 50000
    Public Const OBJS_LIMIT = 75000

End Class
