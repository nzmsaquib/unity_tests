﻿'***************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides several string and character manipulation utilities.
' These methods are provided to simplify translating the code
' to other languages.'
'***************************************************************

Option Explicit On
Option Strict On

Imports System.Text.RegularExpressions
Imports System.Math

Public Class ASTStr

    '*********************************************************************
    ' Abbreviates a string on the right, if necessary.
    '
    ' strIn			input string
    ' maxlen		maximum length not counting ellipses
    '
    ' returns rightmost maxlen characters of the input string
    ' along with ellipses if necessary
    '*********************************************************************
    Public Shared Function abbrevRight(ByVal strIn As String, ByVal maxlen As Integer) As String
        Dim str As String

        If Len(strIn) >= maxlen Then
            str = "... " & Right(strIn, maxlen)
        Else
            str = strIn
        End If

        Return str
    End Function

    '*********************************************************************
    ' Compares two strings that may contain alphanumeric characters so
    ' that a string such as 'A9' is "less" than the string 'A11'. If the
    ' strings are strictly numeric, a numeric comparison is done.
    ' Alphabetic comparisons are not case sensitive.
    '
    ' str1             first string to compare
    ' str2             second string to compare
    '
    ' returns -1 if str1 < str2, 0 if str1=str2, and 1 if str1> str2
    '
    ' Note that this method only supports integers (e.g., numbers such as
    ' 5.9 vs 3.01 may not compare as expected) and the maximum size integer
    ' in the string is limited to that which can be represented by an int
    ' data type.
    '*********************************************************************
    Public Shared Function compareAlphaNumeric(ByVal str1 As String, ByVal str2 As String) As Integer
        Dim idx1 As Integer = 0
        Dim idx2 As Integer = 0
        Dim n1, n2 As Integer
        Dim len1 As Integer = Len(str1)
        Dim len2 As Integer = Len(str2)
        Dim result As Integer = 0
        Dim s1, s2 As String

        ' The strategy is to recursively extract a numeric or alphabetic substring
        ' from both strings and compare them. We have to do this until we reach
        ' the end of a string or until two substrings are not equal

        While ((idx1 < len1) And (idx2 < len2))
            s1 = extractAlphaNumSubstr(str1, idx1)
            idx1 += Len(s1)

            s2 = extractAlphaNumSubstr(str2, idx2)
            idx2 += Len(s2)

            If IsNumeric(s1.Chars(0)) And IsNumeric(s2.Chars(0)) Then ' Compare as numbers
                Integer.TryParse(s1, n1)
                Integer.TryParse(s2, n2)
                If (n1 < n2) Then
                    Return -1
                ElseIf (n1 = n2) Then
                    Return 0
                Else
                    Return 1
                End If
            Else
                result = String.Compare(s1, s2, True)
            End If

            If (result <> 0) Then Return result ' We only need to continue comparison if they're still equal
        End While

        ' If we get here, either the strings are truly equal, or one is longer than the other.
        If (len1 < len2) Then
            Return -1
        ElseIf (len1 > len2) Then
            Return 1
        Else
            Return 0
        End If

    End Function

    '**************************************************************
    ' Counts all occurrences of a specified character in a string
    '
    ' str          string that is to be examined
    ' ch           character to look for in the input string
    '
    ' returns a count of the number of times ch appears in str
    '**************************************************************
    Public Shared Function countChars(ByVal str As String, ByVal ch As Char) As Integer
        Dim count As Integer = 0

        If Len(str) <= 0 Then Return 0

        For Each c As Char In str
            If c = ch Then count += 1
        Next

        Return count
    End Function

    '****************************************************************
    ' Extract a substring from the input string that is all numeric
    ' or all alphabetic.
    '
    ' str              the input string
    ' start            where in str to begin extraction
    '
    ' returns a purely numeric or a purely alphabetic string
    '****************************************************************
    Public Shared Function extractAlphaNumSubstr(ByVal str As String, ByVal start As Integer) As String
        Dim result As String = ""
        Dim c As Char
        Dim i As Integer
        Dim n As Integer = Len(str)

        If (start >= n) Then Return "" ' already reached end of string

        c = str.Chars(start)
        If IsNumeric(c) Then        ' accumulate digits
            result = result + c
            i = start + 1
            While (i < n)
                c = str.Chars(i)
                If IsNumeric(c) Then
                    result = result + c
                    i += 1
                Else
                    i = n + 1           ' force breaking out of the loop
                End If
            End While
        Else                            ' accumulate alphabetic characters
            result = result + c
            i = start + 1
            While (i < n)
                c = str.Chars(i)
                If IsNumeric(c) Then
                    i = n + 1             ' force breaking out of the loop
                Else
                    result = result + c
                    i += 1
                End If
            End While
        End If

        Return result
    End Function

    '******************************************************************
    ' Convert a number to a string and insert commas.
    ' This makes large numbers easier to read.
    '
    ' x             number to convert
    '
    ' Returns a string with commas in it (e.g., 1,245,980.1234)
    '******************************************************************
    Public Shared Function insertCommas(ByVal x As Double) As String
        Dim st As String
        Dim sparts() As String
        Dim delimiter As Char = "."c
        Dim x1 As Double = Abs(x)

        ' FormatNumber by itself will not work because we would have
        ' to specify the max number of digits after the decimal point,
        ' which we can't predict a priori.
        st = x1.ToString
        sparts = st.Split(delimiter)
        st = FormatNumber(Fix(x1), 0)
        If sparts.Length > 1 Then
            st = st & "." & sparts(1)
        End If
        If (x < 0.0) Then st = "-" & st
        Return st
    End Function

    '**************************************************************
    ' Removes all whitespace (spaces, tabs, ctrl-lf, etc.) from a
    ' string, including embedded whitespace.
    '
    ' str          input string to be trimmed
    '
    ' returns trimmed string
    '**************************************************************
    Public Shared Function removeWhitespace(ByVal str As String) As String
        Dim regWhitespace As New Regex("\s")

        If Len(str) <= 0 Then Return String.Empty

        Return regWhitespace.Replace(str, String.Empty)
    End Function

    '****************************************************************
    ' Removes all occurrences of strTarget in strIn with strReplace
    '
    ' strIn            input string to be modified
    ' strTarget        target string to be searched for in strIn
    ' strReplace       replacement string
    '
    ' Returns the string with the requested replacement.
    '****************************************************************
    Public Shared Function replaceStr(ByVal strIn As String, ByVal strTarget As String, ByVal strReplace As String) As String
        Dim regReplace As New Regex(strTarget)

        If Len(strIn) <= 0 Then Return String.Empty

        Return regReplace.Replace(strIn, strReplace)
    End Function
End Class
