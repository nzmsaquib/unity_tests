﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class provides a GUI to allow a user to enter
' 1 or 2 lines of data.'
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Windows.Forms

Public Class ASTQuery
    '********************************************************
    ' Create a query form instance and initialize everything
    '********************************************************
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        Me.lblLabel1.Text = ""
        Me.txtData1.Text = ""
        Me.lblLabel2.Text = ""
        Me.txtData2.Text = ""
    End Sub

    '********************************************************************************************
    ' Public methods that calling routines can use
    '********************************************************************************************

    '*****************************************************
    ' Display the query form and center it on the parent.
    ' This method is overloaded to allow 1 or 2 data items
    ' to be entered.
    '
    ' sLabel1       1st label to display
    ' sLabel2       2nd label to display
    '*****************************************************
    Public Function showQueryForm(ByVal sLabel1 As String) As DialogResult
        Me.lblLabel1.Text = sLabel1
        Me.txtData1.Text = ""
        Me.lblLabel2.Visible = False
        Me.txtData2.Visible = False

        Me.StartPosition = FormStartPosition.CenterParent
        Return Me.ShowDialog()
    End Function
    Public Function showQueryForm(ByVal sLabel1 As String, ByVal sLabel2 As String) As DialogResult
        Me.lblLabel1.Text = sLabel1
        Me.txtData1.Text = ""
        Me.lblLabel2.Visible = True
        Me.lblLabel2.Text = sLabel2
        Me.txtData2.Visible = True
        Me.txtData2.Text = ""

        Me.StartPosition = FormStartPosition.CenterParent
        Return Me.ShowDialog()
    End Function

    '****************************************************************
    ' Provide 'get' functions to return the data from the query form
    '****************************************************************
    Public Function getData1() As String
        Return txtData1.Text
    End Function
    Public Function getData2() As String
        Return txtData2.Text
    End Function

    '-------------------------------------------------------------------------------------------
    ' Private methods used only in this class
    '-------------------------------------------------------------------------------------------

    '---------------------------------
    ' Handle closing the query dialog
    '---------------------------------
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub
End Class
