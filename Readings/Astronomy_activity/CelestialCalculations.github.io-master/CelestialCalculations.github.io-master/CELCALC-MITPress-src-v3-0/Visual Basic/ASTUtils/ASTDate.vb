﻿'**************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides a class for a date object and related methods.
'
' Methods provided include validating strings in the form
' mm/dd/yyyy where all fields are required and yyyy could
' be negative. The validate methods ensure that 1 <= mm <= 12
' and that the day is in an appropriate range for the mm
' (including checking for leap years for Gregorian dates).
'
' Note that different languages have date-related routines, but 
' they are not used here because (a) we need to allow
' dates to be negative and (b) implementing these routines
' rather than using a language's native date routines makes
' it easier to translate to a different programming language.
'**************************************************************

Option Explicit On
Option Strict On

Imports ASTUtils.ASTInt
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr

Public Class ASTDate
    ' Class instance variables.
    Private bValid As Boolean           ' true if object holds a valid date
    Private iMonth As Integer
    Private iDay As Integer             ' provide both int and double day to support Julian day number calculations
    Private dDay As Double
    Private iYear As Integer

    Public Shared DaysOfWeek As String() = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}

    '***********************************************************
    ' Constructor to create a date object. Since much of the
    ' usage of this class involves validating that a string has
    ' a valid date, assume a new object is invalid until it is 
    ' proven otherwise.
    '***********************************************************
    Sub New()
        bValid = False
        iMonth = 1
        iDay = 1
        dDay = 1.0
        iYear = 0
    End Sub

    '********************************************************************
    ' Define 'get' and 'set' accessors for the object's fields.
    '********************************************************************
    Public Function isValidDateObj() As Boolean
        Return Me.bValid
    End Function
    Public Function getMonth() As Integer
        Return Me.iMonth
    End Function
    Public Function getiDay() As Integer
        Return Me.iDay
    End Function
    Public Function getdDay() As Double
        Return Me.dDay
    End Function
    Public Function getYear() As Integer
        Return Me.iYear
    End Function
    Public Sub setMonth(ByVal month As Integer)
        Me.iMonth = month
    End Sub
    Public Sub setiDay(ByVal day As Integer)
        Me.iDay = day
        Me.dDay = CDbl(day)
    End Sub
    Public Sub setdDay(ByVal day As Double)
        Me.dDay = day
        Me.iDay = CInt(day)
    End Sub
    Public Sub setYear(ByVal year As Integer)
        Me.iYear = year
    End Sub

    '********************************************************************
    ' Define a method for returning today's current date. This is
    ' easy to do, but is provided as a method to ease portability to
    ' other languages.
    '********************************************************************
    Public Shared Function getCurrentDate() As String
        Return replaceStr(DateString, "-", "/")
    End Function

    '********************************************************************************
    ' Define some general methods for validating that a string contains a valid date.
    '********************************************************************************

    '*****************************************************
    ' Check a string to see if contains a valid Date.
    ' Do not display any error message unless flag is
    ' SHOW_ERRORS to let the calling routine display
    ' its own message.
    '
    ' This function is overloaded to allow flexibility
    ' in whether error messages are displayed (default
    ' is to show errors) and whether the day must be
    ' an integer or can include a fractional part
    ' of the day.
    '
    ' inputStr          string to be validated
    ' dateObj           date object for the returned date
    ' flag              whether to display error messages
    ' allowFrac         if true, allow fractional part of
    '                   a day to be in the input string
    '                   and allow day to be 0
    '
    ' Returns true if the string has a valid date.
    ' Returns the date in dateObj.
    '*****************************************************
    ' Check a string to see if contains a valid Date. Display an error messages if an error is found.
    Public Shared Function isValidDate(ByVal inputStr As String, ByRef dateObj As ASTDate) As Boolean
        Return isValidDate(inputStr, dateObj, SHOW_ERRORS)
    End Function
    ' Check a string to see if it contains a valid Date. Day must be an integer.
    Public Shared Function isValidDate(ByVal inputStr As String, ByRef dateObj As ASTDate, ByVal flag As Boolean) As Boolean
        Return isValidDate(inputStr, dateObj, flag, False)
    End Function
    Public Shared Function isValidDate(ByVal inputStr As String, ByRef dateObj As ASTDate,
                                       ByVal flag As Boolean, ByVal allowFrac As Boolean) As Boolean
        Dim parts() As String                   ' This will contain the tokens extracted from inputStr
        Dim iMaxDay As Integer = 28             ' Max day for February
        Dim iMinDay As Integer = 1              ' minimum day (could be 0 if allowFrac is true)

        dateObj.bValid = False
        If allowFrac Then iMinDay = 0 ' allow 0 so routines can convert a '0 day' to a JD

        inputStr = removeWhitespace(inputStr)

        If Len(inputStr) <= 0 Then
            ErrMsg("Either nothing was entered or what was entered is invalid." & vbNewLine &
                   "Please enter a valid Date in the form mm/dd/yyyy", "Invalid Date Format", flag)
            Return False
        End If

        parts = Split(inputStr, "/")

        ' Ubound is 1 less than the number of tokens since Visual Basic starts array indices at 0
        If UBound(parts) <> 2 Then
            ErrMsg("The Date entered is not in a valid format. Please enter" & vbNewLine &
                   "a valid Date in the form mm/dd/yyyy", "Invalid Date Format", flag)
            Return False
        End If

        ' Get the year
        If Not isValidInt(parts(2), dateObj.iYear, HIDE_ERRORS) Then
            ErrMsg("The Year entered is not valid. Please enter" & vbNewLine &
                   "a valid Date in the form mm/dd/yyyy", "Invalid Date (Year)", flag)
            Return False
        End If

        ' Get month
        If Not isValidInt(parts(0), dateObj.iMonth, HIDE_ERRORS) Then
            ErrMsg("The Month entered is invalid. Please enter" & vbNewLine &
                   "a valid Date in the form mm/dd/yyyy", "Invalid Date (Month)", flag)
            Return False
        End If
        If ((dateObj.iMonth < 1) Or (dateObj.iMonth > 12)) Then
            ErrMsg("The Month entered is out of range. Please enter" & vbNewLine &
                   "a valid Date in the form mm/dd/yyyy", "Invalid Date (Month)", flag)
            Return False
        End If

        ' Get day and validate it according to the month
        If allowFrac Then       ' allow user to enter fractional days or 0
            If Not isValidReal(parts(1), dateObj.dDay, HIDE_ERRORS) Then
                ErrMsg("The Day entered is not a valid value. Please enter" & vbNewLine &
                       "a valid Date in the form mm/dd.dd/yyyy", "Invalid Date (Day)", flag)
                Return False
            End If
            dateObj.iDay = CInt(dateObj.dDay)
        Else
            If Not isValidInt(parts(1), dateObj.iDay, HIDE_ERRORS) Then
                ErrMsg("The Day entered is not a valid value. Please enter" & vbNewLine &
                       "a valid Date in the form mm/dd/yyyy", "Invalid Date (Day)", flag)
                Return False
            End If
            dateObj.dDay = CDbl(dateObj.iDay)
        End If

        If ((dateObj.iDay < iMinDay) Or (dateObj.iDay > 31)) Then
            ErrMsg("The Day entered is out of range. Please enter" & vbNewLine &
                   "a valid Date in the form mm/dd/yyyy", "Invalid Date (Day)", flag)
            Return False
        End If
        ' Check February
        If (dateObj.iMonth = 2) Then
            If isLeapYear(dateObj.iYear) Then iMaxDay = 29
            If (dateObj.iDay > iMaxDay) Then
                ErrMsg("The Day entered for Feburary is out of range. Please" & vbNewLine &
                       "enter a valid Date in the form mm/dd/yyyy", "Invalid Date (Day)", flag)
                Return False
            End If
        End If
        ' Check April, June, Sept, Nov
        If ((dateObj.iMonth = 4) Or (dateObj.iMonth = 6) Or (dateObj.iMonth = 9) Or (dateObj.iMonth = 11)) Then
            If (dateObj.iDay > 30) Then
                ErrMsg("The Day entered is out of range. Please enter" & vbNewLine &
                       "a valid Date in the form mm/dd/yyyy", "Invalid Date (Day)", flag)
                Return False
            End If
        End If

        dateObj.bValid = True

        Return True
    End Function

    '*****************************************************
    ' Convert a date to a string.
    '
    ' This function is overloaded to allow the date to be
    ' passed in as an object or by month, day, year. Also
    ' allows day to be an integer or a decimal value to
    ' handle fractional days.
    '
    ' dateObj          date to be converted as an object
    ' month            month as an integer
    ' day              day as an integer or a double
    ' year             year as an integer
    '
    ' Returns date as a string.
    '*****************************************************
    ' Convert m, d, year values to a displayable string when caller passes a date object or m/d/y
    Public Shared Function dateToStr(ByVal dateObj As ASTDate) As String
        Return dateToStr(dateObj.iMonth, dateObj.iDay, dateObj.iYear)
    End Function
    Public Shared Function dateToStr(ByVal month As Integer, ByVal day As Double, ByVal year As Integer) As String
        Return Format(month, "00") & "/" & Format(day, "00.00") & "/" & year
    End Function
    Public Shared Function dateToStr(ByVal month As Integer, ByVal day As Integer, ByVal year As Integer) As String
        Return Format(month, "00") & "/" & Format(day, "00") & "/" & year
    End Function

    '************************************************************************************
    ' These methods perform various date related calculations, such as determining
    ' if a year is a leap year and converting a date to a Julian day number.
    '************************************************************************************

    '***********************************************************
    ' Calculate how many days into a year a specific date is
    '
    ' iMonth, iDay, iYear           the date in question
    '***********************************************************
    Public Shared Function daysIntoYear(ByVal iMonth As Integer, ByVal iDay As Integer, ByVal iYear As Integer) As Integer
        Dim T, N As Integer

        If isLeapYear(iYear) Then
            T = 1
        Else
            T = 2
        End If

        N = Trunc(275.0 * iMonth / 9.0) - T * Trunc((iMonth + 9) / 12.0) + iDay - 30
        Return N

    End Function

    '***********************************************************
    ' Calculate date when given the number of days into a year
    '
    ' iYear                     year under consideration
    ' N                         number of days into iYear
    '
    ' Returns the calendar date that is iDays into the year. No
    ' check is made to be sure N is in range.
    '***********************************************************
    Public Shared Function daysIntoYear2Date(ByVal iYear As Integer, N As Integer) As ASTDate
        Dim iMonth, iDay, iA, iB, iC, iE As Integer
        Dim calendarDate As ASTDate = New ASTDate()

        If isLeapYear(iYear) Then
            iA = 1523
        Else
            iA = 1889
        End If

        iB = Trunc((N + iA - 122.1) / 365.25)
        iC = N + iA - Trunc(365.25 * iB)
        iE = Trunc(iC / 30.6001)

        If (iE < 13.5) Then
            iMonth = iE - 1
        Else
            iMonth = iE - 13
        End If

        iDay = iC - Trunc(30.6001 * iE)

        calendarDate.setYear(iYear)
        calendarDate.setMonth(iMonth)
        calendarDate.setiDay(iDay)

        Return calendarDate
    End Function

    '*********************************************************************
    ' Checks to see if a year is a leap year. By definition, a leap year
    ' must be in the Gregorian calendar (i.e., greater than 1581). This
    ' function will return false if the year is not a leap year or if the
    ' year is < 1582.
    '
    ' year         year to check
    '
    ' Returns true if year is a leap year.
    '*********************************************************************
    Public Shared Function isLeapYear(ByVal year As Integer) As Boolean
        If (year < 1582) Then Return False
        Return ((year Mod 4 = 0) And (year Mod 100 <> 0) Or (year Mod 400 = 0))
    End Function

    '******************************************************
    ' Converts a calendar date to a Julian day number.
    '
    ' This function is overloaded to allow the date to be
    ' passed as an object or by month, day, year.
    '
    ' dateObj       date to be converted as an object
    ' month         month to be converted
    ' day           day, including fractional part of
    '               the day, to be converted
    ' year          year to be converted
    '
    ' Returns the Julian day number for the given date.
    '******************************************************
    Public Shared Function dateToJD(ByVal dateObj As ASTDate) As Double
        Return dateToJD(dateObj.iMonth, dateObj.dDay, dateObj.iYear)
    End Function
    Public Shared Function dateToJD(ByVal month As Integer, ByVal day As Double, ByVal year As Integer) As Double
        Dim dT, dTemp, dA, dB, JD, dY, dM As Double

        If (month > 2) Then
            dY = CDbl(year)
            dM = CDbl(month)
        Else
            dY = CDbl(year - 1)
            dM = CDbl(month + 12)
        End If

        If (year < 0) Then
            dT = 0.75
        Else
            dT = 0.0
        End If

        dTemp = CDbl(year) + CDbl(month) / 100.0 + day / 10000.0
        If (dTemp >= 1582.1015) Then
            dA = Trunc(dY / 100.0)
            dB = 2.0 - dA + Trunc(dA / 4.0)
        Else
            dA = 0.0
            dB = 0.0
        End If

        JD = dB + Trunc(365.25 * dY - dT) + Trunc(30.6001 * (dM + 1)) + day + 1720994.5

        Return JD
    End Function

    '******************************************************
    ' Convert a Julian day number to a calendar date.
    '
    ' JD           Julian day number to convert
    '
    ' Returns date as a date object
    '******************************************************
    Public Shared Function JDtoDate(ByVal JD As Double) As ASTDate
        Dim dateObj As ASTDate = New ASTDate()
        Dim JD1, fJD1 As Double
        Dim iJD1, iA, iB, iC, iD, iE, iG As Int64
        Dim iMonth, iYear As Integer
        Dim dDay As Double

        JD1 = JD + 0.5
        iJD1 = Trunc(JD1)
        fJD1 = Frac(JD1)
        If (iJD1 > 2299160) Then
            iA = Trunc((iJD1 - 1867216.25) / 36524.25)
            iB = iJD1 + 1 + iA - Trunc(CDbl(iA) / 4.0)
        Else
            iB = iJD1
        End If

        iC = iB + 1524
        iD = Trunc((iC - 122.1) / 365.25)
        iE = Trunc(365.25 * iD)
        iG = Trunc((iC - iE) / 30.6001)
        dDay = iC - iE + fJD1 - Trunc(30.6001 * iG)

        If (iG > 13.5) Then
            iMonth = CType(iG - 13, Integer)
        Else
            iMonth = CType(iG - 1, Integer)
        End If
        If (iMonth > 2.5) Then
            iYear = CType(iD - 4716, Integer)
        Else
            iYear = CType(iD - 4715, Integer)
        End If

        dateObj.bValid = True
        dateObj.iMonth = iMonth
        dateObj.dDay = dDay
        dateObj.iDay = Trunc(dDay)
        dateObj.iYear = iYear

        Return dateObj
    End Function

End Class
