﻿'*******************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class solves Kepler's equation via a simple iteration
' scheme and by the Newton/Raphson method.
'*******************************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils.ASTMath
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTStyle

Public Class ASTKepler

    ' Default max iterations and minimum termination criteria for Kepler's equation
    Public Const KeplerMaxIterations = 2500
    Public Const KeplerMinCriteria = 0.00000001

    ' Define possible ways to determine the true anomaly. The ASTKepler class
    ' provides algorithms for solving Kepler's equation. Solving the equation of
    ' the center is specific to the Sun and Moon.
    Public Enum TrueAnomalyType
        SOLVE_EQ_OF_CENTER
        SOLVE_SIMPLE_ITERATION_KEPLER
        SOLVE_NEWTON_METHOD_KEPLER
    End Enum

    '*****************************************************************
    ' Solve Kepler's equation via a simple iteration method.
    '
    ' prt               object that is the caller's text output area
    ' dMA               mean anomaly in degrees
    ' dEcc              orbital eccentricity
    ' dTerm             termination criteria in radians
    ' iter              number of iterations performed
    '
    ' Returns the eccentric anomaly and number of iterations
    ' performed to get a results. Results are in degrees.
    '
    ' Note that if prt is Nothing, no interim results are displayed.
    '*****************************************************************
    Public Shared Function calcSimpleKepler(ByVal dMA As Double, ByVal dEcc As Double,
                                            ByVal dTerm As Double, ByRef iter As Integer) As Double
        Return calcSimpleKepler(Nothing, dMA, dEcc, dTerm, iter)
    End Function
    Public Shared Function calcSimpleKepler(ByVal prt As ASTUtils.ASTPrt, ByVal dMA As Double, ByVal dEcc As Double,
                                            ByVal dTerm As Double, ByRef iter As Integer) As Double

        Dim dEA, dEANext, dMARad, delta As Double

        ' Validate the termination criteria
        If dTerm < KeplerMinCriteria Then dTerm = KeplerMinCriteria

        printlncond(prt, "    Use Simple Iteration to solve Kepler's equation")
        printlncond(prt, "    Iterate until difference is less than " & dTerm &
                    " radians (" & Format(rad2deg(dTerm) * 3600.0, genFloatFormat) & " arcseconds)")
        printlncond(prt)

        dMARad = deg2rad(dMA)
        dEA = dMARad
        printlncond(prt, "      E0 = " & Format(dEA, genFloatFormat))

        For iter = 1 To KeplerMaxIterations
            ' Note that the sine function here uses radians, not degrees!
            dEANext = dMARad + dEcc * Sin(dEA)
            delta = Abs(dEANext - dEA)
            printlncond(prt, "      E" & iter & " = " & Format(dEANext, genFloatFormat) & ", Delta = " &
                        Format(delta, genFloatFormat))
            dEA = dEANext
            If delta <= dTerm Then Exit For
        Next

        If iter >= KeplerMaxIterations Then
            printlncond(prt, "WARNING: Did not converge, so stopped after " & KeplerMaxIterations & " iterations.")
        End If
        printlncond(prt)

        dEA = rad2deg(dEA)
        Return dEA
    End Function

    '*****************************************************************
    ' Solve Kepler's equation via the Newton/Raphson method.
    '
    ' prt               object that is the caller's text output area
    ' dMA               mean anomaly in degrees
    ' dEcc              orbital eccentricity
    ' dTerm             termination criteria in radians
    ' iter              number of iterations performed
    '
    ' Returns the eccentric anomaly and number of iterations
    ' performed to get a results. Results are in degrees.
    '
    ' Note that if prt is Nothing, no interim results are displayed.
    '*****************************************************************
    Public Shared Function calcNewtonKepler(ByVal dMA As Double, ByVal dEcc As Double,
                                            ByVal dTerm As Double, ByRef iter As Integer) As Double
        Return calcNewtonKepler(Nothing, dMA, dEcc, dTerm, iter)
    End Function
    Public Shared Function calcNewtonKepler(ByVal prt As ASTUtils.ASTPrt, ByVal dMA As Double, ByVal dEcc As Double,
                                            ByVal dTerm As Double, ByRef iter As Integer) As Double
        Dim dEA, dEANext, dMARad, delta As Double

        ' Validate the termination criteria
        If dTerm < KeplerMinCriteria Then dTerm = KeplerMinCriteria

        printlncond(prt, "    Use the Newton/Raphson method to solve Kepler's equation")
        printlncond(prt, "    Iterate until difference is less than " & dTerm & " radians (" &
                    Format(rad2deg(dTerm) * 3600.0, genFloatFormat) & " arcseconds)")
        printlncond(prt)

        dMARad = deg2rad(dMA)
        If dEcc < 0.75 Then
            dEA = dMARad
        Else
            dEA = PI
        End If
        printlncond(prt, "      E0 = " & Format(dEA, genFloatFormat))

        For iter = 1 To KeplerMaxIterations
            ' Note that the sine & cosine functions here uses radians, not degrees!
            dEANext = dEA - (dEA - dEcc * Sin(dEA) - dMARad) / (1 - dEcc * Cos(dEA))
            delta = Abs(dEANext - dEA)
            printlncond(prt, "      E" & iter & " = " & Format(dEANext, genFloatFormat) & ", Delta = " &
                        Format(delta, genFloatFormat))
            dEA = dEANext
            If delta <= dTerm Then Exit For
        Next

        If iter >= KeplerMaxIterations Then
            printlncond(prt, "WARNING: Did not converge, so stopped after " & KeplerMaxIterations & " iterations.")
        End If

        printlncond(prt)
        dEA = rad2deg(dEA)
        Return dEA
    End Function

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------------------------------
    ' Provide some routines that allow printing text messages to
    ' a calling routine's scrollable text output area. These methods
    ' are provided to avoid tying this Kepler class to a particular
    ' chapter's GUI, but at the cost of the calling routine having to pass
    ' a prt reference.
    '
    ' prt           object that is the caller's output area
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Shared Sub printcond(ByVal prt As ASTUtils.ASTPrt, ByVal txt As String)
        If Not IsNothing(prt) Then prt.print(txt)
    End Sub
    Private Shared Sub printlncond(ByVal prt As ASTUtils.ASTPrt)
        If Not IsNothing(prt) Then prt.println()
    End Sub
    Private Shared Sub printlncond(ByVal prt As ASTUtils.ASTPrt, ByVal txt As String)
        If Not IsNothing(prt) Then prt.println(txt)
    End Sub
    Private Shared Sub printlncond(ByVal prt As ASTUtils.ASTPrt, ByVal txt As String, ByVal centerTxt As Boolean)
        If Not IsNothing(prt) Then prt.println(txt, centerTxt)
    End Sub

End Class
