﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' This class implements a class for performing coordinate
' system conversions and a coordinate object.
'**********************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils.ASTAngle
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTKepler
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTOrbits
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Public Class ASTCoord

    ' A generic coordinate is simply two or three angles. When an equatorial coordinate, the 1st angle
    ' is actually in hours rather than degrees. See below for how the class instance variables are
    ' to be used depending upon the coordinate system (e.g, equatorial, horizon) under consideration
    Private time1 As ASTTime
    Private angle1 As ASTAngle
    Private angle2 As ASTAngle
    Private angle3 As ASTAngle

    '***************************************
    ' Create an instance with given angles
    '
    ' ang1          value for 1st angle
    ' ang2          value for 2nd angle
    ' ang3          value for 3rd angle
    '***************************************
    Sub New()
        Me.time1 = New ASTTime(0.0)
        Me.angle1 = New ASTAngle(0.0)
        Me.angle2 = New ASTAngle(0.0)
        Me.angle3 = New ASTAngle(0.0)
    End Sub
    Sub New(ByVal ang1 As Double, ByVal ang2 As Double)
        Me.time1 = New ASTTime(ang1 / 15.0)
        Me.angle1 = New ASTAngle(ang1)
        Me.angle2 = New ASTAngle(ang2)
        Me.angle3 = New ASTAngle(0.0)
    End Sub
    Sub New(ByVal ang1 As Double, ByVal ang2 As Double, ByVal ang3 As Double)
        Me.time1 = New ASTTime(ang1 / 15.0)
        Me.angle1 = New ASTAngle(ang1)
        Me.angle2 = New ASTAngle(ang2)
        Me.angle3 = New ASTAngle(ang3)
    End Sub

    '********************************************************************
    ' Define 'get' and 'set' accessors for the object's fields
    '********************************************************************
    Public Function getAngle1() As ASTAngle
        Return Me.angle1
    End Function
    Public Function getAngle2() As ASTAngle
        Return Me.angle2
    End Function
    Public Function getAngle3() As ASTAngle
        Return Me.angle3
    End Function
    ' We'll use angle 1 for altitude, angle 2 for azimuth when this is a horizon/topocentric coordinate
    Public Function getAltAngle() As ASTAngle
        Return Me.angle1
    End Function
    Public Function getAzAngle() As ASTAngle
        Return Me.angle2
    End Function
    ' We'll use angle 1 for latitude, angle 2 for longitude when this
    'is an ecliptic or terrestrial coordinate
    Public Function getLatAngle() As ASTAngle
        Return Me.angle1
    End Function
    Public Function getLonAngle() As ASTAngle
        Return Me.angle2
    End Function
    ' We'll use time1 for hour angle and RA (in hours), angle 2 for decl when this is an equatorial coordinate
    Public Function getRAAngle() As ASTTime
        Return Me.time1
    End Function
    Public Function getHAAngle() As ASTTime
        Return Me.time1
    End Function
    Public Function getDeclAngle() As ASTAngle
        Return Me.angle2
    End Function
    ' For cartesian coordinates, use the order x, y, z
    Public Function getCartX() As ASTAngle
        Return Me.angle1
    End Function
    Public Function getCartY() As ASTAngle
        Return Me.angle2
    End Function
    Public Function getCartZ() As ASTAngle
        Return Me.angle3
    End Function
    ' For spherical coordinates, use the order r, alpha, phi
    Public Function getSpherR() As ASTAngle
        Return Me.angle1
    End Function
    Public Function getSpherAlpha() As ASTAngle
        Return Me.angle2
    End Function
    Public Function getSpherPhi() As ASTAngle
        Return Me.angle3
    End Function

    Public Sub setAngle1(ByVal angle As Double)
        Me.angle1.setDecAngle(angle)
    End Sub
    Public Sub setAngle2(ByVal angle As Double)
        Me.angle2.setDecAngle(angle)
    End Sub
    Public Sub setAngle3(ByVal angle As Double)
        Me.angle3.setDecAngle(angle)
    End Sub
    Public Sub setTime1(ByVal tme As Double)
        Me.time1.setDecTime(tme)
    End Sub

    '***********************************************************
    ' Calculate the obliquity of the ecliptic for a given epoch
    '
    ' Epoch             epoch for which obliquity is desired
    '
    ' Returns obliquity of the ecliptic calculated for the
    ' stated epoch.
    '***********************************************************
    Public Shared Function calcEclipticObliquity(ByVal Epoch As Double) As Double
        Dim JD, dT, dDE, dE As Double
        Dim iEpoch As Integer = Trunc(Epoch)

        JD = dateToJD(1, 0.0, iEpoch)
        dT = (JD - 2451545.0) / 36525.0
        dDE = 46.815 * dT + 0.0006 * dT * dT - 0.00181 * dT * dT * dT
        dE = 23.439292 - (dDE / 3600.0)
        Return dE
    End Function

    '**********************************************************
    ' Convert cartesian coordinates to spherical coordinates.
    '
    ' x, y, z                   cartesian coordinates
    '
    ' Returns spherical coordinates with angle1 as r,
    ' angle2 as alpha, and angle3 as phi
    '**********************************************************
    Public Shared Function CartesianToSpherical(ByVal x As Double, ByVal y As Double, ByVal z As Double) As ASTCoord
        Dim r, alpha, phi As Double
        Dim spherCoord As ASTCoord = New ASTCoord(0.0, 0.0, 0.0)

        r = Sqrt(x ^ 2 + y ^ 2 + z ^ 2)
        alpha = INVTAN_D(y / x) + quadAdjust(y, x)
        phi = INVCOS_D(z / r)

        spherCoord.angle1.setDecAngle(r)
        spherCoord.angle2.setDecAngle(alpha)
        spherCoord.angle3.setDecAngle(phi)

        Return spherCoord
    End Function

    '**********************************************************
    ' Convert ecliptic coordinates to equatorial coordinates.
    '
    ' EclLat, EclLon            ecliptic coordinates
    ' Epoch                     epoch coordinates are in
    '
    ' Returns equatorial coordinates with angle1 as the RA
    ' and angle2 as the Decl
    '**********************************************************
    Public Shared Function EclipticToEquatorial(ByVal EclLat As Double, ByVal EclLon As Double,
                                                ByVal Epoch As Double) As ASTCoord
        Dim dE, dT, dY, dX, dR, dQA, RA, Decl As Double
        Dim eqCoord As ASTCoord = New ASTCoord(0.0, 0.0)

        dE = calcEclipticObliquity(Epoch)
        dT = SIN_D(EclLat) * COS_D(dE) + COS_D(EclLat) * SIN_D(dE) * SIN_D(EclLon)
        Decl = INVSIN_D(dT)
        dY = SIN_D(EclLon) * COS_D(dE) - TAN_D(EclLat) * SIN_D(dE)
        dX = COS_D(EclLon)
        dR = INVTAN_D(dY / dX)
        dQA = quadAdjust(dY, dX)
        RA = dR + dQA
        RA = RA / 15.0

        eqCoord.time1.setDecTime(RA)
        eqCoord.angle2.setDecAngle(Decl)

        Return eqCoord
    End Function

    '**********************************************************
    ' Convert an equatorial coordinate expressed an hour angle
    ' and declination to a horizon coordinate.
    '
    ' HA            hour angle (in hours)
    ' Decl          declination in degrees
    ' Lat           observer latitude
    '
    ' Returns a coord object in which the 1st angle is the
    ' altitude, 2nd angle is the azimuth.
    '**********************************************************
    Public Shared Function HADecltoHorizon(ByVal HA As Double, ByVal Decl As Double,
                                           ByVal Lat As Double) As ASTCoord
        Dim horizCoord As ASTCoord = New ASTCoord(0.0, 0.0)
        Dim dHA, dT, Alt, Az, dT1, dT2 As Double

        dHA = HA * 15.0        ' Convert HA to degrees
        dT = SIN_D(Decl) * SIN_D(Lat) + COS_D(Decl) * COS_D(Lat) * COS_D(dHA)
        Alt = INVSIN_D(dT)
        dT1 = SIN_D(Decl) - SIN_D(Lat) * SIN_D(Alt)
        dT2 = dT1 / (COS_D(Lat) * COS_D(Alt))
        Az = INVCOS_D(dT2)

        dT1 = SIN_D(dHA)
        If (dT1 > 0) Then Az = 360.0 - Az

        horizCoord.angle1.setDecAngle(Alt)
        horizCoord.angle2.setDecAngle(Az)

        Return horizCoord
    End Function

    '*******************************************
    ' Convert Hour Angle to Right Ascension
    '
    ' HA            hour angle (in hours)
    ' LST           LST for the observer
    '
    ' Returns right ascension
    '*******************************************
    Public Shared Function HAtoRA(ByVal HA As Double, ByVal LST As Double) As Double
        Dim RA As Double

        RA = LST - HA
        If RA < 0.0 Then RA = RA + 24.0

        Return RA
    End Function

    '**********************************************************
    ' Convert a horizon coordinate to an equatorial coordinate
    ' expressed as an hour angle (in hours) and declination.
    '
    ' Alt           altitude
    ' Az            azimuth
    ' Lat           observer latitude
    '
    ' Returns a coord object in which the 1st angle is the hour
    ' angle (in hours), 2nd angle is the declination
    '**********************************************************
    Public Shared Function HorizontoHADecl(ByVal Alt As Double, ByVal Az As Double,
                                           ByVal Lat As Double) As ASTCoord
        Dim eqCoord As ASTCoord = New ASTCoord(0.0, 0.0)
        Dim dTmp, Decl, HA As Double

        dTmp = SIN_D(Alt) * SIN_D(Lat) + COS_D(Alt) * COS_D(Lat) * COS_D(Az)
        Decl = INVSIN_D(dTmp)

        dTmp = SIN_D(Alt) - SIN_D(Lat) * SIN_D(Decl)
        HA = dTmp / (COS_D(Lat) * COS_D(Decl))
        HA = INVCOS_D(HA)

        dTmp = SIN_D(Az)
        If (dTmp > 0) Then HA = 360 - HA
        HA = HA / 15.0

        eqCoord.time1.setDecTime(HA)
        eqCoord.angle2.setDecAngle(Decl)

        Return eqCoord
    End Function

    '******************************************************************
    ' Convert a horizon coordinate to right ascension and declination.
    '
    ' Alt           altitude in degrees
    ' Az            azimuth in degrees
    ' Lat           observer latitude
    ' LST           sidereal time for observer
    '
    ' Returns a coord object in which the 1st angle is the right
    ' ascension (in hours) and the 2nd angle is the declination.
    '******************************************************************
    Public Shared Function HorizontoRADecl(ByVal Alt As Double, ByVal Az As Double,
                                           ByVal Lat As Double, ByVal LST As Double) As ASTCoord
        Dim eqCoord As ASTCoord
        Dim RA As Double

        eqCoord = HorizontoHADecl(Alt, Az, Lat)
        RA = HAtoRA(eqCoord.time1.getDecTime, LST)
        eqCoord.time1.setDecTime(RA)

        Return eqCoord
    End Function

    '-----------------------------------------------------------
    ' Convert Keplerian elements to topocentric coordinates.
    '
    ' iStp              starting step for printing results
    ' obs               observer object w/date and LCT
    ' h_sea             observer's height above sea level (meters)
    ' DST               true if on Daylight Saving Time
    ' epochDate         calendar date for the epoch
    ' epochUT           UT time for the epoch
    ' inclin            inclination
    ' ecc               eccentricity
    ' axis              semi-major axis
    ' RAAN              RAAN
    ' argofperi         argument of perigee
    ' M0                mean anomaly
    ' MMotion           mean motion
    ' solveKepler       what method to use to solve Kepler's equation
    ' termCriteria  	termination criteria for stopping iteration to
    '                   solve Kepler's equation
    ' prt               window for displaying intermediate results.
    '                   If null, no itermediate results are displayed.
    '
    ' Returns topocentric coordinates for this observer.
    '
    ' Note: iStp Is used so that output printed out by this method
    ' can be merged in with the caller's computational steps.
    '-----------------------------------------------------------
    Public Shared Function Keplerian2Topo(ByVal obs As ASTObserver, ByVal h_sea As Double, ByVal DST As Boolean,
                                             ByVal epochDate As ASTDate, ByVal epochUT As Double,
                                             ByVal inclin As Double, ByVal ecc As Double,
                                             ByVal axis As Double, ByVal RAAN As Double,
                                             ByVal argofperi As Double, ByVal M0 As Double,
                                             ByVal MMotion As Double, ByVal solveKepler As TrueAnomalyType,
                                             ByVal termCriteria As Double) As ASTCoord
        Return Keplerian2Topo(1, obs, h_sea, DST, epochDate, epochUT, inclin, ecc, axis, RAAN,
                                 argofperi, M0, MMotion, solveKepler, termCriteria, Nothing)
    End Function

    Public Shared Function Keplerian2Topo(ByVal iStp As Integer, ByVal obs As ASTObserver, ByVal h_sea As Double,
                                             ByVal DST As Boolean, ByVal epochDate As ASTDate,
                                             ByVal epochUT As Double, ByVal inclin As Double,
                                             ByVal ecc As Double, ByVal axis As Double,
                                             ByVal RAAN As Double, ByVal argofperi As Double,
                                             ByVal M0 As Double, ByVal MMotion As Double,
                                             ByVal solveKepler As TrueAnomalyType,
                                             ByVal termCriteria As Double,
                                             ByVal prt As ASTPrt) As ASTCoord
        Dim topoCoord As ASTCoord = New ASTCoord(0.0, 0.0)
        Dim LCT, UT, GST, LST, LSTd, JD, JDe, deltaT, Mt As Double
        Dim adjustedDate As ASTDate = New ASTDate()
        Dim dateAdjust As Integer
        Dim R As ASTVect = New ASTVect()
        Dim V As ASTVect = New ASTVect()
        Dim R_obs As ASTVect = New ASTVect
        Dim Rp As ASTVect = New ASTVect
        Dim Rpp As ASTVect = New ASTVect
        Dim x, y, z, rp_e, r_eq, r_dist, obsLat As Double

        ' Do all the time-related calculations
        LCT = obs.getObsTime().getDecTime()
        UT = LCTtoUT(LCT, DST, obs.getObsTimeZone, obs.getObsLon, dateAdjust)
        ' adjust the date, if needed, since the LCTtoUT conversion could have changed the date
        adjustedDate = adjustDatebyDays(obs.getObsDate, dateAdjust)
        GST = UTtoGST(UT, adjustedDate)
        LST = GSTtoLST(GST, obs.getObsLon)
        printlncond(prt, iStp & ".  Convert the observer LCT to UT, GST, and LST and adjust date if needed.")
        iStp = iStp + 1
        printlncond(prt, "    LCT = " & timeToStr(obs.getObsTime(), DECFORMAT) &
                    " hours, date is " & dateToStr(obs.getObsDate))
        printcond(prt, "     UT = " & timeToStr(UT, DECFORMAT) & " hours (")
        If dateAdjust < 0 Then
            printcond(prt, "previous day ")
        ElseIf dateAdjust > 0 Then
            printcond(prt, "next day ")
        End If
        printlncond(prt, dateToStr(adjustedDate) & ")")
        printlncond(prt, "    GST = " & timeToStr(GST, DECFORMAT) & " hours")
        printlncond(prt, "    LST = " & timeToStr(LST, DECFORMAT) & " hours")
        printlncond(prt)

        JDe = dateToJD(epochDate.getMonth, epochDate.getdDay + (epochUT / 24.0), epochDate.getYear)
        printlncond(prt, iStp & ".  Compute the Julian day number for the epoch at which the")
        iStp = iStp + 1
        printlncond(prt, "    Keplerian elements were captured.")
        printlncond(prt, "    JDe = " & Format(JDe, JDFormat))
        printlncond(prt)

        JD = dateToJD(adjustedDate.getMonth, adjustedDate.getdDay + (UT / 24.0), adjustedDate.getYear)
        printlncond(prt, iStp & ".  Compute the Julian day number for the desired date, being sure to use the")
        iStp = iStp + 1
        printlncond(prt, "    Greenwich date and UT from step 3, and including the fractional part of the day.")
        printlncond(prt, "    JD = " & Format(JD, JDFormat))
        printlncond(prt)

        deltaT = JD - JDe
        printlncond(prt, iStp & ".  Compute the total number of elapsed days, including fractional days,")
        iStp = iStp + 1
        printlncond(prt, "    since the epoch.")
        printlncond(prt, "    deltaT = JD - JDe = " & Format(JD, JDFormat) & " - " & Format(JDe, JDFormat))
        printlncond(prt, "           = " & Format(deltaT, genFloatFormat8) & " days")
        printlncond(prt)

        Mt = M0 + 360 * deltaT * MMotion
        printlncond(prt, iStp & ".  Propagate the mean anomaly by deltaT days")
        iStp = iStp + 1
        printlncond(prt, "    Mt = M0 + 360*deltaT*(Mean Motion) = " & Format(M0, genFloatFormat8) & " + 360*" &
                    Format(deltaT, genFloatFormat8) & "*(" & Format(MMotion, genFloatFormat8) & ")")
        printlncond(prt, "       = " & Format(Mt, genFloatFormat8) & " degrees")
        printlncond(prt)

        printlncond(prt, iStp & ".  If necessary, adjust Mt to be in the range [0,360]")
        iStp = iStp + 1
        printcond(prt, "    Mt = Mt MOD 360 = " & Format(Mt, genFloatFormat8) & " MOD 360 = ")
        Mt = xMOD(Mt, 360.0)
        printlncond(prt, Format(Mt, genFloatFormat8) & " degrees")
        printlncond(prt)

        printlncond(prt, iStp & ".  Using Mt in place of M0, convert the Keplerian elements to a state vector")
        iStp = iStp + 1
        If Not Keplerian2State(inclin, ecc, axis, RAAN, argofperi, Mt,
                               solveKepler, termCriteria, R, V, prt) Then Return topoCoord
        printlncond(prt, "    Resulting State Vector:")
        printlncond(prt, "        [" & insertCommas(Round(R.x, 3)) & ", " & insertCommas(Round(R.y, 3)) &
                    ", " & insertCommas(Round(R.z, 3)) & "] (positional vector)")
        printlncond(prt, "        [" & insertCommas(Round(V.x, 3)) & ", " & insertCommas(Round(V.y, 3)) &
                    ", " & insertCommas(Round(V.z, 3)) & "] (velocity vector)")
        printlncond(prt, "    State Vector gives a velocity of " & Round(V.len, 3) & " km/s at a distance")
        printlncond(prt, "    of " & insertCommas(Round(R.len, 3)) & " km (" & insertCommas(Round(KM2Miles(R.len), 3)) &
                    " miles) above the center of the Earth.")
        printlncond(prt)

        printlncond(prt, iStp & ". Convert the positional vector (i.e., ECI coordinates) to topocentric coordinates.")
        iStp = iStp + 1
        LSTd = LST * 15.0
        obsLat = obs.getObsLat
        ' Convert obs location to ECI
        rp_e = EarthRadius + (h_sea / 1000)
        r_eq = rp_e * COS_D(obsLat)
        R_obs.setVect(r_eq * COS_D(LSTd), r_eq * SIN_D(LSTd), rp_e * SIN_D(obsLat))
        ' Compute range vector
        Rp.setVect(R.x - R_obs.x, R.y - R_obs.y, R.z - R_obs.z)
        ' Rotate range vector
        x = Rp.x * SIN_D(obsLat) * COS_D(LSTd) + Rp.y * SIN_D(obsLat) * SIN_D(LSTd) - Rp.z * COS_D(obsLat)
        y = -Rp.x * SIN_D(LSTd) + Rp.y * COS_D(LSTd)
        z = Rp.x * COS_D(obsLat) * COS_D(LSTd) + Rp.y * COS_D(obsLat) * SIN_D(LSTd) + Rp.z * SIN_D(obsLat)
        Rpp.setVect(x, y, z)
        ' Compute topo Alt, Az
        r_dist = Rpp.len()
        topoCoord.setAngle2(INVTAN2_D(-Rpp.y, Rpp.x))       ' Azimuth
        topoCoord.setAngle1(INVSIN_D(Rpp.z / r_dist))       ' Altitude

        printlncond(prt, "    Alt_topo = " & angleToStr(topoCoord.getAltAngle.getDecAngle, DMSFORMAT) & ", Az_topo = " &
                    angleToStr(topoCoord.getAzAngle.getDecAngle, DMSFORMAT))
        printlncond(prt)

        Return topoCoord
    End Function

    '*********************************************************************
    ' Convert Keplerian elements to a state vector
    '
    ' inclin            orbital inclination
    ' ecc               orbital eccentricity
    ' axis              length of the semi-major axis
    ' RAAN              RAAN
    ' argofperi         argument of perigee
    ' M0                mean anomaly
    ' solveKepler       which method to use to solve Kepler's equation
    ' termCriteria      when to stop iterating for Kepler's equation
    ' R                 positional state vector
    ' V                 veclocity state vector
    ' prt               area in which to display intermediate steps
    '
    ' Returns true if the conversion was successful and sets R and V
    '*********************************************************************
    Public Shared Function Keplerian2State(ByVal inclin As Double, ByVal ecc As Double,
                                     ByVal axis As Double, ByVal RAAN As Double,
                                     ByVal argofperi As Double, ByVal M0 As Double,
                                     ByVal solveKepler As TrueAnomalyType,
                                     ByVal termCriteria As Double,
                                     ByRef R As ASTVect, ByRef V As ASTVect,
                                     ByVal prt As ASTPrt) As Boolean
        Dim iter As Integer
        Dim RAANp, argofperip As Double
        Dim rho, EA, dv, dLen, x, y, z, A As Double
        Dim angleObj As ASTAngle = New ASTAngle()
        Dim Otype As Integer

        ' compute semi-latus rectum
        rho = axis * (1.0 - ecc ^ 2)

        ' Solve Kepler's equation for the eccentric anomaly and use that to get true anomaly
        If solveKepler = TrueAnomalyType.SOLVE_SIMPLE_ITERATION_KEPLER Then
            EA = calcSimpleKepler(prt, M0, ecc, termCriteria, iter)
        Else
            EA = calcNewtonKepler(prt, M0, ecc, termCriteria, iter)
        End If
        dv = Sqrt((1 + ecc) / (1 - ecc)) * TAN_D(EA / 2.0)
        dv = 2.0 * INVTAN_D(dv)

        ' Compute the positional vector
        dLen = rho / (1 + ecc * COS_D(dv))
        x = dLen * COS_D(dv)
        y = dLen * SIN_D(dv)
        z = 0.0
        R.setVect(x, y, z)

        A = Sqrt(muEarth / rho)

        ' Compute the velocity vector
        x = -A * SIN_D(dv)
        y = A * (ecc + COS_D(dv))
        z = 0.0
        V.setVect(x, y, z)

        ' Make adjustments based on orbit type
        Otype = getSatOrbitType(ecc, inclin)
        If (Otype < 0) Then
            ErrMsg("Orbit must be elliptical. The computed eccentricity" & vbNewLine & "and inclination are not supported.",
                   "Invalid Orbit type")
            Return False
        End If

        If (Otype = 1) Or (Otype = 2) Then
            RAANp = 0.0
        Else
            RAANp = RAAN
        End If
        If (Otype = 1) Or (Otype = 3) Then
            argofperip = 0.0
        Else
            argofperip = argofperi
        End If

        ' Do rotations to xlate R from perifocal to ECI coordinates
        R = RotateZ(-argofperip, R)
        R = RotateX(-inclin, R)
        R = RotateZ(-RAANp, R)

        ' Now do the same for the velocity vector
        V = RotateZ(-argofperip, V)
        V = RotateX(-inclin, V)
        V = RotateZ(-RAANp, V)

        Return True
    End Function

    '********************************************************
    ' Compute a precession correction
    '
    ' RAIn              Right Ascension in hours for EpochIn
    ' DeclIn    		Declination in degrees for EpochOut
    ' EpochIn   		Epoch that RAIn/DeclIn is given in
    ' EpochTo     		Epoch to convert to
    '
    ' Returns a coordinate object with the input coordinates
    ' precessed to EpochOut
    '********************************************************
    Public Shared Function PrecessEqCoord(ByVal RAIn As Double, ByVal DeclIn As Double,
                                          ByVal EpochIn As Double, ByVal EpochTo As Double) As ASTCoord
        Dim RAdeg, dT, dM, dNd, dNt, dDiff As Double
        Dim deltaRA, deltaDecl As Double
        Dim eqCoord As ASTCoord = New ASTCoord(0.0, 0.0)

        ' Convert RA to degrees
        RAdeg = RAIn * 15.0

        dT = (EpochTo - 1900.0) / 100.0
        dM = 3.07234 + 0.00186 * dT
        dNd = 20.0468 - 0.0085 * dT
        dNt = dNd / 15.0

        dDiff = (EpochTo - EpochIn)
        deltaRA = (dM + dNt * SIN_D(RAdeg) * TAN_D(DeclIn)) * dDiff
        deltaDecl = dNd * COS_D(RAdeg) * dDiff
        deltaRA = deltaRA / 3600.0
        deltaDecl = deltaDecl / 3600.0

        eqCoord.getRAAngle.setDecTime(RAIn + deltaRA)
        eqCoord.getDeclAngle.setDecAngle(DeclIn + deltaDecl)

        Return eqCoord
    End Function

    '***************************************************************
    ' Convert an equatorial coordinate expressed as right ascension
    ' and declination to a horizon coordinate.
    '
    ' RA            right ascension (in hours)
    ' Decl          declination in degrees
    ' Lat           observer latitude
    ' LST           sidereal time for observer
    '
    ' Returns a coord object in which the 1st angle is the altitude,
    ' 2nd angle is the azimuth.
    '***************************************************************
    Public Shared Function RADecltoHorizon(ByVal RA As Double, ByVal Decl As Double,
                                           ByVal Lat As Double, ByVal LST As Double) As ASTCoord
        Return HADecltoHorizon(RAtoHA(RA, LST), Decl, Lat)
    End Function

    '*******************************************
    ' Convert Right Ascension to Hour Angle
    '
    ' RA            right ascension (in hours)
    ' LST           LST for the observer
    '
    ' Returns Hour Angle
    '*******************************************
    Public Shared Function RAtoHA(ByVal RA As Double, ByVal LST As Double) As Double
        Dim HA As Double

        HA = LST - RA
        If (HA < 0.0) Then HA = HA + 24.0
        Return HA
    End Function

    '**********************************************************
    ' Convert spherical coordinates to cartesian coordinates.
    '
    ' r, alpha, phi                  spherical coordinates
    '
    ' Returns cartesian coordinates with angle1 as x,
    ' angle2 as y, and angle3 as z
    '**********************************************************
    Public Shared Function SphericalToCartesian(ByVal r As Double, ByVal alpha As Double, ByVal phi As Double) As ASTCoord
        Dim x, y, z As Double
        Dim cartCoord As ASTCoord = New ASTCoord(0.0, 0.0, 0.0)

        x = r * COS_D(alpha) * SIN_D(phi)
        y = r * SIN_D(alpha) * SIN_D(phi)
        z = r * COS_D(phi)

        cartCoord.angle1.setDecAngle(x)
        cartCoord.angle2.setDecAngle(y)
        cartCoord.angle3.setDecAngle(z)

        Return cartCoord
    End Function

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------------------------------
    ' Provide some routines that allow printing text messages to
    ' a calling routine's scrollable text output area. These methods
    ' are provided to avoid tying this Kepler class to a particular
    ' chapter's GUI, but at the cost of the calling routine having to pass
    ' a prt reference.
    '
    ' prt           object that is the caller's output area
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Shared Sub printcond(ByVal prt As ASTUtils.ASTPrt, ByVal txt As String)
        If Not IsNothing(prt) Then prt.print(txt)
    End Sub
    Private Shared Sub printlncond(ByVal prt As ASTUtils.ASTPrt)
        If Not IsNothing(prt) Then prt.println()
    End Sub
    Private Shared Sub printlncond(ByVal prt As ASTUtils.ASTPrt, ByVal txt As String)
        If Not IsNothing(prt) Then prt.println(txt)
    End Sub
    Private Shared Sub printlncond(ByVal prt As ASTUtils.ASTPrt, ByVal txt As String, ByVal centerTxt As Boolean)
        If Not IsNothing(prt) Then prt.println(txt, centerTxt)
    End Sub

End Class
