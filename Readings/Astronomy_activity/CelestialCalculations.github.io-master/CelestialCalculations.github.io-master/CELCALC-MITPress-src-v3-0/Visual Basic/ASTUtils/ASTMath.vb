﻿'*************************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Provides several math-related utilities.
'
' These methods are provided to be sure they perform the
' calculations as required by the programs and to simplify
' translating the code to another language. For example, the
' Visual Basic function Truncate already does what the Trunc
' function defined below does and hence Trunc is unnecessary
' for Visual Basic. But, the Trunc function is defined here
' because other languages, such as Java, do not have a
' built-in Truncate function.
' 
' All methods are static so that an instance of this
' class does not have to be created before using these methods.
'*************************************************************

Option Explicit On
Option Strict On

Imports System.Math

Imports ASTUtils.ASTVect

Public Class ASTMath

    ' Define a value to use when comparing small real numbers to see if they are the same. Comparing reals
    ' for strict equality will often fail due to roundoff errors, so just check for "close enough" in those cases.
    Public Const EPS = 0.00000005

    '----------------------------------------------------------------------
    ' Define some general math routines
    '----------------------------------------------------------------------

    '*******************************************************
    ' Find the cube root of a number.
    '
    ' x             value whose cube root is desired
    '
    ' Returns cube root of x
    '*******************************************************
    Public Shared Function cuberoot(ByVal x As Double) As Double
        Return (x) ^ (1 / 3)
    End Function

    '*******************************************************
    ' Return the fractional part of a number.
    ' Frac(1.5) = 0.5, Frac(-1.5) = 0.5, etc.
    '*******************************************************
    Public Shared Function Frac(ByVal x As Double) As Double
        ' The mathematically correct way to get the fractional part of a number is
        '		return Abs(x) - Int(Abs(x))
        ' However, due to roundoff errors, we use strings so that results match what a
        ' user would see if the intermediate steps are printed.

        Dim parts() As String
        Dim result As Double
        Dim temp As String = Format(x, "#0.00000000")

        parts = Split(temp, ".")
        Double.TryParse("0." & parts(1), result)

        Return result
    End Function

    '*********************************************************
    ' Determine if the number x is 'close' to the number y
    '
    ' Returns true if x is 'close' to y
    '*********************************************************
    Public Shared Function isClose(ByVal x As Double, ByVal y As Double) As Boolean
        Return Abs(x - y) <= EPS
    End Function

    '**************************************************
    ' Determine whether n is evenly divisible by m.
    ' That is, return true if (n MOD m) is 0, otherwise
    ' return false.
    '**************************************************
    Public Shared Function isEvenlyDivisible(ByVal n As Integer, ByVal m As Integer) As Boolean
        Return (xMOD(n, m) = 0)
    End Function

    '**************************************************************
    ' Gets the integer part of a number.
    ' Trunc(1.5) = 1, Trunc(-1.5) = -1, etc.
    '
    ' Some languages have built in functions that perform
    ' this operation, such as Visual Basic's Truncate function.
    ' Also, some languages have a FIX function, which is **NOT**
    ' the same as the function defined here.
    '**************************************************************
    Public Shared Function Trunc(ByVal x As Double) As Integer
        ' The mathematically correct way to truncate a number is to just
        ' convert it to an integer and account for the sign. That is,
        '		int i = (int) Math.abs(x);
        '		if (x < 0.0) i=i*-1;
        '		return i;
        ' However, due to roundoff errors, we use strings so that results match what a
        ' user would see if the intermediate steps are printed.
        Dim parts() As String
        Dim result As Integer
        Dim temp As String = Format(x, "#0.00000000")

        parts = Split(temp, ".")
        Integer.TryParse(parts(0), result)

        Return result
    End Function

    '**************************************************************
    ' Returns x modulo div. For example: 
    ' xMOD(270,180) = 90, xMOD(-270.87,180) = 89.13,
    ' xMOD(-400,360) = 320, xMOD(-100,8) = 4.
    '
    ' Some languages have built in functions that perform
    ' this operation, such as Visual Basic's MOD operator.
    ' However, they may not handle negative numbers the way
    ' required for this book. (Visual Basic will return a
    ' negative number if x is negative whereas this function
    ' always returns a positive number.)
    '**************************************************************
    Public Shared Function xMOD(ByVal x As Integer, ByVal div As Integer) As Integer
        If (x < 0) Then
            Return (x Mod div) + div
        Else
            Return x Mod div
        End If
    End Function
    Public Shared Function xMOD(ByVal x As Double, ByVal div As Double) As Double
        If (x < 0.0) Then
            Return (x Mod div) + div
        Else
            Return x Mod div
        End If
    End Function

    '*************************************************************************************************************
    ' Define some useful trigonometric routines to use degrees rather than radians.
    '*************************************************************************************************************

    '***************************************************
    ' Convert degrees to radians
    '***************************************************
    Public Shared Function deg2rad(ByVal deg As Double) As Double
        Return PI * deg / 180.0
    End Function
    ' Convert radians to degrees
    Public Shared Function rad2deg(ByVal rad As Double) As Double
        Return 180.0 * rad / PI
    End Function

    ' Various trig functions to accept degrees rather than radians
    Public Shared Function SIN_D(ByVal deg As Double) As Double
        Return Sin(deg2rad(deg))
    End Function
    Public Shared Function COS_D(ByVal deg As Double) As Double
        Return Cos(deg2rad(deg))
    End Function
    Public Shared Function TAN_D(ByVal deg As Double) As Double
        Return Tan(deg2rad(deg))
    End Function
    Public Shared Function INVSIN_D(ByVal x As Double) As Double
        Return rad2deg(Asin(x))
    End Function
    Public Shared Function INVCOS_D(ByVal x As Double) As Double
        Return rad2deg(Acos(x))
    End Function
    Public Shared Function INVTAN_D(ByVal x As Double) As Double
        Return rad2deg(Atan(x))
    End Function
    Public Shared Function INVTAN2_D(ByVal y As Double, ByVal x As Double) As Double
        Return INVTAN_D(y / x) + quadAdjust(y, x)
    End Function

    '***********************************************
    ' Compute a quadrant adjustment for the inv tan
    ' function given x and y.
    '***********************************************
    Public Shared Function quadAdjust(ByVal y As Double, ByVal x As Double) As Double
        Dim dQA As Double = 0.0

        If (y > 0.0) And (x < 0.0) Then dQA = 180.0
        If (y < 0.0) And (x < 0.0) Then dQA = 180.0
        If (y < 0.0) And (x > 0.0) Then dQA = 360.0

        Return dQA
    End Function

    '*************************************************************************************************************
    ' Define some simple distance conversion routines
    '*************************************************************************************************************
    
    '***********************************
    ' Convert kilometers to miles
    '
    ' km        kilometers to convert
    '
    ' Returns miles.
    '***********************************
    Public Shared Function KM2Miles(ByVal km As Double) As Double
        Return km * 0.62137119
    End Function

    '***********************************
    ' Convert miles to kilometers
    '
    ' miles         miles to convert
    '
    ' Returns kilometers.
    '***********************************
    Public Shared Function Miles2KM(ByVal miles As Double) As Double
        Return miles * 1.609344
    End Function

    '***********************************
    ' Convert AUs to kilometers
    '
    ' AU         AUs to convert
    '
    ' Returns kilometers.
    '***********************************
    Public Shared Function AU2KM(ByVal AU As Double) As Double
        Return AU * 149597870.691
    End Function

    '***********************************
    ' Convert kilometers to AUs
    '
    ' km         kms to convert
    '
    ' Returns AUs.
    '***********************************
    Public Shared Function KM2AU(ByVal KM As Double) As Double
        Return KM / 149597870.691
    End Function

    '*************************************************************************************************************
    ' Define a couple of rotations that are needed for the satellites chapter.
    '*************************************************************************************************************

    '***************************************************
    ' Rotate about the x-axis. This is the book's
    ' 'f' family of functions.
    '
    ' theta         angle, in degrees, to rotate
    ' V             vector to rotate
    '
    ' Returns the input vector rotated by theta degrees
    ' about the x-axis
    '***************************************************
    Public Shared Function RotateX(ByVal theta As Double, ByVal V As ASTVect) As ASTVect
        Dim F As ASTVect = New ASTVect()
        Dim x1, y1, z1 As Double

        x1 = V.x
        y1 = V.y * COS_D(theta) + V.z * SIN_D(theta)
        z1 = V.z * COS_D(theta) - V.y * SIN_D(theta)

        F.setVect(x1, y1, z1)
        Return F
    End Function

    '***************************************************
    ' Rotate about the z-axis. This is the book's
    ' 'g' family of functions.
    '
    ' theta         angle, in degrees, to rotate
    ' V             vector to rotate
    '
    ' Returns the input vector rotated by theta degrees
    ' about the z-axis
    '***************************************************
    Public Shared Function RotateZ(ByVal theta As Double, ByVal V As ASTVect) As ASTVect
        Dim G As ASTVect = New ASTVect()
        Dim x1, y1, z1 As Double

        x1 = V.x * COS_D(theta) + V.y * SIN_D(theta)
        y1 = V.y * COS_D(theta) - V.x * SIN_D(theta)
        z1 = V.z

        G.setVect(x1, y1, z1)
        Return G
    End Function

End Class
