﻿'**********************************************************
'                       Copyright (c) 2018
'                      Author: J. L. Lawrence
'
' Defines information about the current version of the
' the software, the book name, author, etc.
'**********************************************************

Option Explicit On
Option Strict On

Public Class ASTBookInfo
    Public Const CODE_VER = "BASIC-3.0"
    Public Const BOOK_TITLE = "Celestial Calculations"
    ' Public Const BOOK_EDITION = "First Edition"
    Public Const BOOK_EDITION = " "
    Public Const BOOK_AUTHOR = "J. L. Lawrence, PhD"
    Public Const BOOK_COPYRIGHT = "Copyright (c) 2018"
End Class
