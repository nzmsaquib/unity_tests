﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Chap3GUI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Chap3GUI))
        Me.ChapMenu = New System.Windows.Forms.MenuStrip()
        Me.mnuFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTimeConversions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLocalGreenwich = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemInstructions = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuitemAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelBookTitle = New System.Windows.Forms.Panel()
        Me.lblBookTitle = New System.Windows.Forms.Label()
        Me.chkboxShowInterimCalcs = New System.Windows.Forms.CheckBox()
        Me.chkboxDST = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtboxLon = New System.Windows.Forms.TextBox()
        Me.radbtnLon = New System.Windows.Forms.RadioButton()
        Me.radbtnPST = New System.Windows.Forms.RadioButton()
        Me.radbtnCST = New System.Windows.Forms.RadioButton()
        Me.panelTimeZones = New System.Windows.Forms.Panel()
        Me.radbtnMST = New System.Windows.Forms.RadioButton()
        Me.radbtnEST = New System.Windows.Forms.RadioButton()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.lblResults = New System.Windows.Forms.Label()
        Me.txtboxData2 = New System.Windows.Forms.TextBox()
        Me.txtboxData1 = New System.Windows.Forms.TextBox()
        Me.lblData1 = New System.Windows.Forms.Label()
        Me.lblData2 = New System.Windows.Forms.Label()
        Me.panelTextOut = New System.Windows.Forms.Panel()
        Me.txtboxOutputArea = New System.Windows.Forms.RichTextBox()
        Me.ChapMenu.SuspendLayout()
        Me.panelBookTitle.SuspendLayout()
        Me.panelTimeZones.SuspendLayout()
        Me.panelTextOut.SuspendLayout()
        Me.SuspendLayout()
        '
        'ChapMenu
        '
        Me.ChapMenu.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.ChapMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFile, Me.mnuTimeConversions, Me.mnuLocalGreenwich, Me.mnuHelp})
        Me.ChapMenu.Location = New System.Drawing.Point(0, 0)
        Me.ChapMenu.Name = "ChapMenu"
        Me.ChapMenu.Size = New System.Drawing.Size(850, 28)
        Me.ChapMenu.TabIndex = 0
        Me.ChapMenu.Text = "ChapMenu"
        '
        'mnuFile
        '
        Me.mnuFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuItemExit})
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Size = New System.Drawing.Size(44, 24)
        Me.mnuFile.Text = "File"
        '
        'mnuItemExit
        '
        Me.mnuItemExit.Name = "mnuItemExit"
        Me.mnuItemExit.Size = New System.Drawing.Size(108, 26)
        Me.mnuItemExit.Text = "Exit"
        '
        'mnuTimeConversions
        '
        Me.mnuTimeConversions.Name = "mnuTimeConversions"
        Me.mnuTimeConversions.Size = New System.Drawing.Size(137, 24)
        Me.mnuTimeConversions.Text = "Time Conversions"
        '
        'mnuLocalGreenwich
        '
        Me.mnuLocalGreenwich.Name = "mnuLocalGreenwich"
        Me.mnuLocalGreenwich.Size = New System.Drawing.Size(146, 24)
        Me.mnuLocalGreenwich.Text = "Local vs Greenwich"
        '
        'mnuHelp
        '
        Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuitemInstructions, Me.mnuitemAbout})
        Me.mnuHelp.Name = "mnuHelp"
        Me.mnuHelp.Size = New System.Drawing.Size(53, 24)
        Me.mnuHelp.Text = "Help"
        '
        'mnuitemInstructions
        '
        Me.mnuitemInstructions.Name = "mnuitemInstructions"
        Me.mnuitemInstructions.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemInstructions.Text = "Instructions"
        '
        'mnuitemAbout
        '
        Me.mnuitemAbout.Name = "mnuitemAbout"
        Me.mnuitemAbout.Size = New System.Drawing.Size(159, 26)
        Me.mnuitemAbout.Text = "About"
        '
        'panelBookTitle
        '
        Me.panelBookTitle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelBookTitle.BackColor = System.Drawing.Color.Navy
        Me.panelBookTitle.Controls.Add(Me.lblBookTitle)
        Me.panelBookTitle.Location = New System.Drawing.Point(0, 32)
        Me.panelBookTitle.Name = "panelBookTitle"
        Me.panelBookTitle.Size = New System.Drawing.Size(850, 49)
        Me.panelBookTitle.TabIndex = 1
        '
        'lblBookTitle
        '
        Me.lblBookTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblBookTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBookTitle.ForeColor = System.Drawing.Color.White
        Me.lblBookTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblBookTitle.Location = New System.Drawing.Point(0, 13)
        Me.lblBookTitle.Name = "lblBookTitle"
        Me.lblBookTitle.Padding = New System.Windows.Forms.Padding(1)
        Me.lblBookTitle.Size = New System.Drawing.Size(850, 27)
        Me.lblBookTitle.TabIndex = 0
        Me.lblBookTitle.Text = "Book Title"
        Me.lblBookTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkboxShowInterimCalcs
        '
        Me.chkboxShowInterimCalcs.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxShowInterimCalcs.AutoSize = True
        Me.chkboxShowInterimCalcs.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxShowInterimCalcs.Location = New System.Drawing.Point(170, 87)
        Me.chkboxShowInterimCalcs.Name = "chkboxShowInterimCalcs"
        Me.chkboxShowInterimCalcs.Size = New System.Drawing.Size(266, 24)
        Me.chkboxShowInterimCalcs.TabIndex = 2
        Me.chkboxShowInterimCalcs.TabStop = False
        Me.chkboxShowInterimCalcs.Text = "Show Intermediate Calculations"
        Me.chkboxShowInterimCalcs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkboxShowInterimCalcs.UseVisualStyleBackColor = True
        '
        'chkboxDST
        '
        Me.chkboxDST.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.chkboxDST.AutoSize = True
        Me.chkboxDST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkboxDST.Location = New System.Drawing.Point(482, 87)
        Me.chkboxDST.Name = "chkboxDST"
        Me.chkboxDST.Size = New System.Drawing.Size(189, 24)
        Me.chkboxDST.TabIndex = 3
        Me.chkboxDST.TabStop = False
        Me.chkboxDST.Text = "Daylight Saving Time"
        Me.chkboxDST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkboxDST.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(318, 1)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 20)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Time Zone"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(613, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(146, 20)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "(ex: 96.5E, 30.4W)"
        '
        'txtboxLon
        '
        Me.txtboxLon.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.txtboxLon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxLon.Location = New System.Drawing.Point(445, 28)
        Me.txtboxLon.Name = "txtboxLon"
        Me.txtboxLon.Size = New System.Drawing.Size(162, 27)
        Me.txtboxLon.TabIndex = 0
        Me.txtboxLon.TabStop = False
        Me.txtboxLon.Text = "180d 30m 15.56sW"
        '
        'radbtnLon
        '
        Me.radbtnLon.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnLon.AutoSize = True
        Me.radbtnLon.CausesValidation = False
        Me.radbtnLon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnLon.Location = New System.Drawing.Point(331, 29)
        Me.radbtnLon.Name = "radbtnLon"
        Me.radbtnLon.Size = New System.Drawing.Size(103, 24)
        Me.radbtnLon.TabIndex = 5
        Me.radbtnLon.Text = "Longitude"
        Me.radbtnLon.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnLon.UseVisualStyleBackColor = True
        '
        'radbtnPST
        '
        Me.radbtnPST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnPST.AutoSize = True
        Me.radbtnPST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnPST.Location = New System.Drawing.Point(23, 29)
        Me.radbtnPST.Name = "radbtnPST"
        Me.radbtnPST.Size = New System.Drawing.Size(62, 24)
        Me.radbtnPST.TabIndex = 1
        Me.radbtnPST.Text = "PST"
        Me.radbtnPST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnPST.UseVisualStyleBackColor = True
        '
        'radbtnCST
        '
        Me.radbtnCST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnCST.AutoSize = True
        Me.radbtnCST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnCST.Location = New System.Drawing.Point(176, 29)
        Me.radbtnCST.Name = "radbtnCST"
        Me.radbtnCST.Size = New System.Drawing.Size(63, 24)
        Me.radbtnCST.TabIndex = 3
        Me.radbtnCST.Text = "CST"
        Me.radbtnCST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnCST.UseVisualStyleBackColor = True
        '
        'panelTimeZones
        '
        Me.panelTimeZones.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.panelTimeZones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panelTimeZones.Controls.Add(Me.Label2)
        Me.panelTimeZones.Controls.Add(Me.Label1)
        Me.panelTimeZones.Controls.Add(Me.txtboxLon)
        Me.panelTimeZones.Controls.Add(Me.radbtnLon)
        Me.panelTimeZones.Controls.Add(Me.radbtnMST)
        Me.panelTimeZones.Controls.Add(Me.radbtnCST)
        Me.panelTimeZones.Controls.Add(Me.radbtnEST)
        Me.panelTimeZones.Controls.Add(Me.radbtnPST)
        Me.panelTimeZones.Location = New System.Drawing.Point(33, 117)
        Me.panelTimeZones.Name = "panelTimeZones"
        Me.panelTimeZones.Size = New System.Drawing.Size(785, 60)
        Me.panelTimeZones.TabIndex = 4
        '
        'radbtnMST
        '
        Me.radbtnMST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnMST.AutoSize = True
        Me.radbtnMST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnMST.Location = New System.Drawing.Point(99, 29)
        Me.radbtnMST.Name = "radbtnMST"
        Me.radbtnMST.Size = New System.Drawing.Size(65, 24)
        Me.radbtnMST.TabIndex = 2
        Me.radbtnMST.Text = "MST"
        Me.radbtnMST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnMST.UseVisualStyleBackColor = True
        '
        'radbtnEST
        '
        Me.radbtnEST.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.radbtnEST.AutoSize = True
        Me.radbtnEST.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radbtnEST.Location = New System.Drawing.Point(255, 29)
        Me.radbtnEST.Name = "radbtnEST"
        Me.radbtnEST.Size = New System.Drawing.Size(62, 24)
        Me.radbtnEST.TabIndex = 4
        Me.radbtnEST.Text = "EST"
        Me.radbtnEST.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.radbtnEST.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCalculate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalculate.Location = New System.Drawing.Point(722, 260)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(105, 32)
        Me.btnCalculate.TabIndex = 7
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'lblResults
        '
        Me.lblResults.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblResults.AutoSize = True
        Me.lblResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblResults.Location = New System.Drawing.Point(19, 266)
        Me.lblResults.Name = "lblResults"
        Me.lblResults.Size = New System.Drawing.Size(63, 20)
        Me.lblResults.TabIndex = 8
        Me.lblResults.Text = "Result"
        Me.lblResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtboxData2
        '
        Me.txtboxData2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtboxData2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxData2.Location = New System.Drawing.Point(23, 232)
        Me.txtboxData2.Name = "txtboxData2"
        Me.txtboxData2.Size = New System.Drawing.Size(397, 27)
        Me.txtboxData2.TabIndex = 6
        Me.txtboxData2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtboxData1
        '
        Me.txtboxData1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtboxData1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxData1.Location = New System.Drawing.Point(23, 199)
        Me.txtboxData1.Name = "txtboxData1"
        Me.txtboxData1.Size = New System.Drawing.Size(397, 27)
        Me.txtboxData1.TabIndex = 5
        Me.txtboxData1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblData1
        '
        Me.lblData1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblData1.AutoSize = True
        Me.lblData1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData1.Location = New System.Drawing.Point(426, 202)
        Me.lblData1.Name = "lblData1"
        Me.lblData1.Size = New System.Drawing.Size(55, 20)
        Me.lblData1.TabIndex = 10
        Me.lblData1.Text = "data 1"
        Me.lblData1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblData2
        '
        Me.lblData2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblData2.AutoSize = True
        Me.lblData2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblData2.Location = New System.Drawing.Point(426, 235)
        Me.lblData2.Name = "lblData2"
        Me.lblData2.Size = New System.Drawing.Size(55, 20)
        Me.lblData2.TabIndex = 11
        Me.lblData2.Text = "data 2"
        Me.lblData2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'panelTextOut
        '
        Me.panelTextOut.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelTextOut.Controls.Add(Me.txtboxOutputArea)
        Me.panelTextOut.Location = New System.Drawing.Point(0, 298)
        Me.panelTextOut.Name = "panelTextOut"
        Me.panelTextOut.Padding = New System.Windows.Forms.Padding(5)
        Me.panelTextOut.Size = New System.Drawing.Size(850, 496)
        Me.panelTextOut.TabIndex = 12
        '
        'txtboxOutputArea
        '
        Me.txtboxOutputArea.BackColor = System.Drawing.Color.White
        Me.txtboxOutputArea.Cursor = System.Windows.Forms.Cursors.No
        Me.txtboxOutputArea.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtboxOutputArea.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtboxOutputArea.Location = New System.Drawing.Point(5, 5)
        Me.txtboxOutputArea.Name = "txtboxOutputArea"
        Me.txtboxOutputArea.ReadOnly = True
        Me.txtboxOutputArea.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtboxOutputArea.Size = New System.Drawing.Size(840, 486)
        Me.txtboxOutputArea.TabIndex = 0
        Me.txtboxOutputArea.TabStop = False
        Me.txtboxOutputArea.Text = ""
        '
        'Chap3GUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(120.0!, 120.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(850, 795)
        Me.Controls.Add(Me.panelTextOut)
        Me.Controls.Add(Me.lblData2)
        Me.Controls.Add(Me.lblData1)
        Me.Controls.Add(Me.btnCalculate)
        Me.Controls.Add(Me.lblResults)
        Me.Controls.Add(Me.txtboxData2)
        Me.Controls.Add(Me.txtboxData1)
        Me.Controls.Add(Me.panelTimeZones)
        Me.Controls.Add(Me.chkboxDST)
        Me.Controls.Add(Me.chkboxShowInterimCalcs)
        Me.Controls.Add(Me.panelBookTitle)
        Me.Controls.Add(Me.ChapMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.ChapMenu
        Me.Name = "Chap3GUI"
        Me.Text = "Title"
        Me.ChapMenu.ResumeLayout(False)
        Me.ChapMenu.PerformLayout()
        Me.panelBookTitle.ResumeLayout(False)
        Me.panelTimeZones.ResumeLayout(False)
        Me.panelTimeZones.PerformLayout()
        Me.panelTextOut.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ChapMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents panelBookTitle As System.Windows.Forms.Panel
    Friend WithEvents lblBookTitle As System.Windows.Forms.Label
    Friend WithEvents mnuTimeConversions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLocalGreenwich As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemInstructions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuitemAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkboxShowInterimCalcs As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxDST As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtboxLon As System.Windows.Forms.TextBox
    Friend WithEvents radbtnLon As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnPST As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnCST As System.Windows.Forms.RadioButton
    Friend WithEvents panelTimeZones As System.Windows.Forms.Panel
    Friend WithEvents radbtnMST As System.Windows.Forms.RadioButton
    Friend WithEvents radbtnEST As System.Windows.Forms.RadioButton
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents lblResults As System.Windows.Forms.Label
    Friend WithEvents txtboxData2 As System.Windows.Forms.TextBox
    Friend WithEvents txtboxData1 As System.Windows.Forms.TextBox
    Friend WithEvents lblData1 As System.Windows.Forms.Label
    Friend WithEvents lblData2 As System.Windows.Forms.Label
    Friend WithEvents panelTextOut As System.Windows.Forms.Panel
    Friend WithEvents txtboxOutputArea As System.Windows.Forms.RichTextBox

End Class
