﻿'**********************************************************
'               Chapter 3 - Time Conversions
'                     Copyright (c) 2018
'                  Author: J. L. Lawrence
'
' This code implements the main GUI and required routines
' to do time conversions. The main GUI was built with the 
' Visual Basic form editor, so many "globals" are created
' external to the code here.
'**********************************************************

Option Explicit On
Option Strict On

Imports ASTUtils
Imports ASTUtils.ASTAboutBox
Imports ASTUtils.ASTBookInfo
Imports ASTUtils.ASTDate
Imports ASTUtils.ASTInt
Imports ASTUtils.ASTLatLon
Imports ASTUtils.ASTMath
Imports ASTUtils.ASTMisc
Imports ASTUtils.ASTMsg
Imports ASTUtils.ASTPrt
Imports ASTUtils.ASTReal
Imports ASTUtils.ASTStr
Imports ASTUtils.ASTStyle
Imports ASTUtils.ASTTime

Imports RunChap3.ChapMenuItem

Public Class Chap3GUI
    Private Const AboutBoxText = "Chapter 3 - Time Conversions"
    Private AboutBox As ASTAboutBox = New ASTAboutBox(AboutBoxText)

    ' An instance cannot be created until after this form (e.g., main GUI) is initialized
    Private prt As ASTPrt

    ' Each menu item selection sets this variable to know what to do when the calculation button is pressed
    Private calculationToDo As CalculationType = CalculationType.NO_CALC_SELECTED

    '***************************************************************
    ' Handle the application initialization
    '***************************************************************
    Public Sub New()
        ' The InitializeComponent() call is required by the designer.
        InitializeComponent()
        ' Set title of window GUI and the book title
        Me.Text = AboutBoxText
        lblBookTitle.Text = BOOK_TITLE

        ' Must create the ASTPrt instance **after** the InitializeComponent has been done
        prt = New ASTPrt(txtboxOutputArea)
        clearAllTextAreas()

        ' Create the menu items for the Time Conversions and Time Zones menus
        createChapMenus()

        ' Now initialize the rest of the GUI
        radbtnLon.Checked = True
        txtboxLon.Text = ""
        chkboxShowInterimCalcs.Checked = False
        chkboxDST.Checked = False
        calculationToDo = CalculationType.NO_CALC_SELECTED

        ' open GUI in the center of the screen
        Me.StartPosition = FormStartPosition.CenterScreen
    End Sub

    '-------------------------------------------------------
    ' Create all of the menu items for the Chapter 3 menus
    '-------------------------------------------------------
    Private Sub createChapMenus()
        ' Time Conversions menu
        mnuTimeConversions.DropDownItems.Add(createMenuItem("Leap Years", CalculationType.LEAP_YEARS))
        mnuTimeConversions.DropDownItems.Add("-")
        mnuTimeConversions.DropDownItems.Add(createMenuItem("Calendar Date to Julian Day Number", CalculationType.CALENDAR_2_JD))
        mnuTimeConversions.DropDownItems.Add(createMenuItem("Julian Day Number to Calendar Date", CalculationType.JD_2_CALENDAR))
        mnuTimeConversions.DropDownItems.Add("-")
        mnuTimeConversions.DropDownItems.Add(createMenuItem("Day of Week", CalculationType.DAY_OF_WEEK))
        mnuTimeConversions.DropDownItems.Add(createMenuItem("Date to Days into Year", CalculationType.DATE_2_DAYS_INTO_YEAR))
        mnuTimeConversions.DropDownItems.Add(createMenuItem("Days into Year to Date", CalculationType.DAYS_INTO_YEAR_2_DATE))

        'Local vs Greenwich menu
        mnuLocalGreenwich.DropDownItems.Add(createMenuItem("LCT to UT", CalculationType.LCT_2_UT))
        mnuLocalGreenwich.DropDownItems.Add(createMenuItem("UT to LCT", CalculationType.UT_2_LCT))
        mnuLocalGreenwich.DropDownItems.Add("-")
        mnuLocalGreenwich.DropDownItems.Add(createMenuItem("UT to GST", CalculationType.UT_2_GST))
        mnuLocalGreenwich.DropDownItems.Add(createMenuItem("GST to UT", CalculationType.GST_2_UT))
        mnuLocalGreenwich.DropDownItems.Add("-")
        mnuLocalGreenwich.DropDownItems.Add(createMenuItem("GST to LST", CalculationType.GST_2_LST))
        mnuLocalGreenwich.DropDownItems.Add(createMenuItem("LST to GST", CalculationType.LST_2_GST))
    End Sub

    '-----------------------------------------------------
    ' Create a ChapMenuItem instance and set its handler
    '-----------------------------------------------------
    Private Function createMenuItem(ByVal title As String, ByVal CalcType As CalculationType) As ChapMenuItem
        Dim tmp As ChapMenuItem

        ' Create the menu item and add a Click handler. No need to set font or other
        ' attributes since they get inherited from the menu to which this item is added
        tmp = New ChapMenuItem(CalcType)
        tmp.Text = title
        AddHandler tmp.Click, AddressOf ChapMenuItems_Clicks
        Return tmp
    End Function

    '---------------------------------------------------------------
    ' Routines below handle all menu items
    '---------------------------------------------------------------

    '-----------------------------------
    ' Handle the Exit menu item
    '-----------------------------------
    Private Sub mnuitemExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuItemExit.Click
        If PleaseConfirm("Are you sure you want to exit?", " ") Then Application.Exit()
    End Sub

    '--------------------------------------------------------------
    ' This handles all 'Time Conversions' and 'Local vs Greenwich'
    ' menu items. All we do here is capture what menu item is to be
    ' done when the 'Calculate' button is clicked.
    '--------------------------------------------------------------
    Private Sub ChapMenuItems_Clicks(sender As System.Object, e As System.EventArgs)
        Dim tmp As ChapMenuItem = CType(sender, ChapMenuItem)
        Dim calctype As CalculationType = tmp.eCalcType
        prt.clearTextArea()

        Select Case calctype
            '*************** Time Conversions menu items
            Case CalculationType.LEAP_YEARS
                setDataLabels("Year (in form YYYY)")
                lblResults.Text = "Determine if a year is a leap year"
            Case CalculationType.CALENDAR_2_JD
                setDataLabels("Date (in form MM/DD.dd/YYYY)")
                lblResults.Text = "Convert Calendar Date to a Julian Day Number"
            Case CalculationType.JD_2_CALENDAR
                setDataLabels("Julian Day Number (ex: 2455928.50)")
                lblResults.Text = "Convert Julian Day Number to a Calendar Date"
            Case CalculationType.DAY_OF_WEEK
                setDataLabels("Date (in form MM/DD/YYYY)")
                lblResults.Text = "Calculate the day of the week for a given date"
            Case CalculationType.DATE_2_DAYS_INTO_YEAR
                setDataLabels("Date (in form MM/DD/YYYY)")
                lblResults.Text = "Convert a Calendar Date to Days into the Year"
            Case CalculationType.DAYS_INTO_YEAR_2_DATE
                setDataLabels("Days into Year (e.g., 15)", "Year")
                lblResults.Text = "Convert Days into the Year to a Calendar Date"

                ' *** Local vs Greenwich menu items
            Case CalculationType.LCT_2_UT
                setDataLabels("Enter LCT time (hh:mm:ss.ss)", "(Set Time Zone or Longitude above)")
                lblResults.Text = "Convert Local Civil Time to Universal Time"
            Case CalculationType.UT_2_LCT
                setDataLabels("Enter UT time (hh:mm:ss.ss)", "(Set Time Zone or Longitude above)")
                lblResults.Text = "Convert Universal Time to Local Civil Time"
            Case CalculationType.UT_2_GST
                setDataLabels("Enter UT time (hh:mm:ss.ss)", "Date (in form MM/DD/YYYY)")
                lblResults.Text = "Convert Universal Time to Greenwich Sidereal Time"
            Case CalculationType.GST_2_UT
                setDataLabels("Enter GST time (hh:mm:ss.ss)", "Date (in form MM/DD/YYYY)")
                lblResults.Text = "Convert Greenwich Sidereal Time to Universal Time"
            Case CalculationType.GST_2_LST
                radbtnLon.Checked = True
                setDataLabels("Enter GST time (hh:mm:ss.ss)", "(Enter Longitude in the Time Zone box above)")
                lblResults.Text = "Convert Greenwich Sidereal Time to Local Sidereal Time"
            Case CalculationType.LST_2_GST
                radbtnLon.Checked = True
                setDataLabels("Enter LST time (hh:mm:ss.ss)", "(Enter Longitude in the Time Zone box above)")
                lblResults.Text = "Convert Local Sidereal Time to Greenwich Sidereal Time"
        End Select

        calculationToDo = calctype
    End Sub

    '-----------------------------------
    ' Handle the Instructions menu item
    '-----------------------------------
    Private Sub mnuitemInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemInstructions.Click
        clearAllTextAreas()

        prt.setBoldFont(True)
        prt.println("Instructions", CENTERTXT)
        prt.setBoldFont(False)
        prt.println()

        prt.println("First, check the 'Show Intermediate Calculations' checkbox if you want to see " &
                    "intermediate results as the various calculations are performed. Also check the 'Daylight Saving Time' " &
                    "checkbox if the times that will be entered are on Daylight Saving Time.")
        prt.println()

        prt.println("Second, select the appropriate time zone (EST, CST, etc.), or select the 'Longitude' radio " &
                    "button if a longitude will be entered rather than a time zone. If a longitude is to be entered, " &
                    "enter the longtiude in the text box beside the 'Longitude' radio button. Longitudes may be entered " &
                    "in decimal format (e.g., 96.5E, 30.45W) or DMS format (e.g., 96d 30m 18.5sW). The N, S, E, W direction " &
                    "must be capitalized. That is, 96.5e and 30.45w are both errors because the direction is not capitalized.")
        prt.println()

        prt.print("Third, after establishing the desired checkbox and radio button settings, select the desired " &
                  "calculation to perform from the 'Time Conversions' or the 'Local vs. Greenwich' menus. Once a " &
                  "menu item is selected, you will be asked to enter one or two lines of data in the text boxes beneath the " &
                  "Time Zone radio buttons. The values to be entered depend upon what menu item was selected. For " &
                  "converting between GST and LST, the longitude must be entered in the text box beside the 'Longitude' " &
                  "radio button ")
        prt.setBoldFont(True)
        prt.print("*and not*")
        prt.setBoldFont(False)
        prt.println(" in one of the text boxes below the Time Zone radio buttons. Note that time is " &
                    "entered in 24 hour format. Thus, 3:30:20 PM would be entered as 15:30:20.")
        prt.println()

        prt.print("Finally, after entering all required data, click the 'Calculate' button at the bottom " &
                  "right of the data input area. The selected computation (e.g., LCT to UT) will be done. " &
                  "If you selected the 'Show Intermedicate Calculations' checkbox, the steps required to find " &
                  "a solution will be shown in the scrollable text output area (i.e., where these instructions " &
                  "are being displayed). The final result will be shown just below the data input text boxes.")
        prt.resetCursor()
    End Sub

    '-----------------------------
    ' Handle the About menu item
    '-----------------------------
    Private Sub mnuitemAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuitemAbout.Click
        AboutBox.showAboutBox()
    End Sub

    '----------------------------------------------------------------------------------------------------------------
    ' Handle clicks on the various non-menu GUI components (checkbox, radio buttons, etc.)
    '----------------------------------------------------------------------------------------------------------------

    '----------------------------------------
    ' Handle a click on the calculate button
    '----------------------------------------
    Private Sub btnCalculate_Click(sender As System.Object, e As System.EventArgs) Handles btnCalculate.Click
        Select Case calculationToDo
            Case CalculationType.NO_CALC_SELECTED
                ErrMsg("No menu item has been selected. Choose a menu item from" & vbNewLine _
                          & "'Time Conversions' or 'Local vs Greenwich' and  try again.", "No Menu Item Selected")

                '*** Time Conversions menu items
            Case CalculationType.LEAP_YEARS
                calcLeapYear()
            Case CalculationType.CALENDAR_2_JD
                calcCalendar2JD()
            Case CalculationType.JD_2_CALENDAR
                calcJD2Calendar()
            Case CalculationType.DAY_OF_WEEK
                calcDayOfWeek()
            Case CalculationType.DATE_2_DAYS_INTO_YEAR
                calcDaysIntoYear()
            Case CalculationType.DAYS_INTO_YEAR_2_DATE
                calcDaysIntoYear2Date()

                ' *** Local vs Greenwich menu items
            Case CalculationType.LCT_2_UT
                calcLCT2UT()
            Case CalculationType.UT_2_LCT
                calcUT2LCT()
            Case CalculationType.UT_2_GST
                calcUT2GST()
            Case CalculationType.GST_2_UT
                calcGST2UT()
            Case CalculationType.GST_2_LST
                calcGST2LST()
            Case CalculationType.LST_2_GST
                calcLST2GST()
        End Select
    End Sub

    '-----------------------------------------------------------
    ' Handle changes to time zone and DST. Basically just need
    ' to clear the output text area and update the GUI. The
    ' actual settings will be queried later when needed.
    '-----------------------------------------------------------
    Private Sub radbtn_Click(sender As System.Object, e As System.EventArgs) Handles radbtnPST.CheckedChanged,
                             radbtnMST.CheckedChanged, radbtnCST.CheckedChanged, radbtnEST.CheckedChanged,
                             chkboxDST.CheckedChanged

        prt.clearTextArea()
    End Sub
    Private Sub radbtnLongitude_Click(sender As System.Object, e As System.EventArgs) Handles radbtnLon.CheckedChanged
        prt.clearTextArea()
        txtboxLon.Text = ""
    End Sub

    '----------------------------------------------------
    ' Determine what time zone radio button is selected
    '
    ' Returns a time zone type for whatever is the
    ' currently selected time zone radio button
    '----------------------------------------------------
    Private Function getSelectedRBStatus() As TimeZoneType
        If radbtnPST.Checked() Then
            Return TimeZoneType.PST
        ElseIf radbtnMST.Checked() Then
            Return TimeZoneType.MST
        ElseIf radbtnCST.Checked() Then
            Return TimeZoneType.CST
        ElseIf radbtnEST.Checked() Then
            Return TimeZoneType.EST
        Else
            Return TimeZoneType.LONGITUDE
        End If
    End Function

    '-------------------------------------------------------------------------------------------------------
    ' Routines to do the actual work for the Time Conversions menu
    '-------------------------------------------------------------------------------------------------------

    '------------------------------------------------
    ' Convert a calendar date to a Julian day number
    '------------------------------------------------
    Private Sub calcCalendar2JD()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim dateObj As ASTDate = New ASTDate()
        Dim iM, iY, iYear, iMonth As Integer
        Dim dT, dTemp, dA, dB, JD, dDay As Double
        Dim result As String
        Dim bGregorian As Boolean

        prt.clearTextArea()

        ' The "true" flag in isValidDate indicates that we will allow fractional days in the date
        ' as well as 0 (so that we can find the JD for 1/0/2000)
        If Not isValidDate(strInput, dateObj, SHOW_ERRORS, True) Then Return
        iYear = dateObj.getYear()
        iMonth = dateObj.getMonth()
        dDay = dateObj.getdDay()

        result = "Convert " & strInput
        If (dateObj.getYear() < 0) Then
            result = result & " BC"
        Else
            result = result & " AD"
        End If

        prt.setBoldFont(True)
        printlncond(result & " to a Julian day number", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1. If the month is greater than 2, set y = year and m = month")
        printlncond("   else set y = year - 1 and m = month + 12")
        printcond("   Since month = " & iMonth & ", ")
        If (iMonth > 2) Then
            iY = iYear
            iM = iMonth
            printlncond("which is greater than 2, set y = " & iYear)
            printlncond("   and m = " & iM)
        Else
            iY = iYear - 1
            iM = iMonth + 12
            printlncond("which is not greater than 2, set y = year - 1 = " & iY)
            printlncond("   and m = month + 12 = " & iM)
        End If
        printlncond()

        printlncond("2. If the year is less than 0, set T = 0.75 else set T = 0")
        printcond("   Since year = " & iYear & " is ")
        If (iYear < 0) Then
            dT = 0.75
            printlncond("less than 0, set T = 0.75")
        Else
            dT = 0.0
            printlncond("not less than 0, set T = 0")
        End If
        printlncond()

        printlncond("3. Determine if the date is in the Gregorian Calendar")
        printlncond("   The date is Gregorian if it is later than Oct. 15, 1582.")
        printlncond("   (Compare YYYY.MMDDdd against 1582.1015. If YYYY.MMDDdd is")
        printlncond("   greater than or equal to 1582.1015, the date is Gregorian.)")

        dTemp = CDbl(iYear) + (CDbl(iMonth) / 100.0) + (dDay / 10000.0)
        bGregorian = (dTemp >= 1582.1015)
        printcond("   Since YYYY.MMDDdd = " & Format(dTemp, "0.000000") & ", the date is ")
        If Not bGregorian Then printcond("not ")
        printlncond("Gregorian")
        printlncond()

        printlncond("4. If the date is Gregorian then compute")
        printlncond("   A = FIX(y / 100) and B = 2 - A + FIX(A / 4).")
        printlncond("   If the date is not Gregorian, set A = 0 and B = 0")
        printcond("   Since the date is ")
        If bGregorian Then
            dA = Trunc(CDbl(iY) / 100.0)
            dB = 2.0 - dA + Trunc(dA / 4.0)
            printlncond("Gregorian, A = FIX(" & iY & " / 100) = " & dA)
            printlncond("   and B = 2 - " & dA & " + FIX(" & dA & " / 4) = " & dB)
        Else
            dA = 0.0
            dB = 0.0
            printlncond("not Gregorian, A = 0 and B = 0")
        End If
        printlncond()

        JD = dB + Trunc(365.25 * iY - dT) + Trunc(30.6001 * (iM + 1)) + dDay + 1720994.5
        printlncond("5. Finally, JD = B + FIX(365.25*y - T) + FIX[30.6001*(m+1)] + day + 1720994.5")
        printlncond("               = " & dB & " + FIX(365.25*" & iY & "-" & dT &
                    ") + FIX[30.6001*(" & iM & "+1)]" & " + " & dDay & " + 1720994.5")
        printlncond("               = " & Format(JD, JDFormat))
        printlncond()

        result = strInput & " = " & Format(JD, JDFormat) & " JD"
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------
    ' Calculate the day of the week
    '-------------------------------
    Private Sub calcDayOfWeek()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim dateObj As ASTDate = New ASTDate()
        Dim N As Integer
        Dim JD, dA, dB As Double
        Dim result As String

        prt.clearTextArea()

        If Not isValidDate(strInput, dateObj) Then Return

        result = dateToStr(dateObj)

        prt.setBoldFont(True)
        printlncond("Determine what Day of the Week " & result & " falls on", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        JD = dateToJD(dateObj)
        printlncond("1. Convert " & result & " to a Julian day number at 00:00:00 UT")
        printlncond("   " & result & " = " & Format(JD, JDFormat) & " Julian day number at 00:00:00 UT")
        printlncond()

        dA = (JD + 1.5) / 7.0
        printlncond("2. Calculate A = (JD + 1.5) / 7 = (" & Format(JD, JDFormat) & " + 1.5) / 7 = " & Format(dA, JDFormat))
        printlncond()

        dB = 7.0 * Frac(dA)
        printlncond("3. Let B = 7 * FRAC(A) = 7 * FRAC(" & Format(dA, JDFormat) & ") = " & Format(dB, "#0.000000"))
        printlncond()

        N = CType(Math.Round(dB), Integer)
        printlncond("4. Finally, let N = ROUND(B) to get the day number for the day")
        printlncond("   of the week (0=Sunday, 1=Monday, etc.)")
        printlncond("   N = ROUND(B) = ROUND(" & Format(dB, "#0.000000") & ") = " & N)
        printlncond()

        If (N < 0) Or (N > 6) Then
            CriticalErrMsg("Internal error - N is out of range")
            Return
        End If

        result = result & " falls on " & DaysOfWeek(N)
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '--------------------------------------------------------
    ' Calculate how many days into a year a specific date is
    '-------------------------------------------------------
    Private Sub calcDaysIntoYear()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim T, N As Integer
        Dim dateObj As ASTDate = New ASTDate()
        Dim result As String
        Dim iYear, iMonth, iDay As Integer

        prt.clearTextArea()

        If Not isValidDate(strInput, dateObj) Then Return
        iYear = dateObj.getYear()
        iMonth = dateObj.getMonth()
        iDay = dateObj.getiDay()
        result = dateToStr(dateObj)

        prt.setBoldFont(True)
        printlncond("Determine the number of days into the year for " & result, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1. If this is a leap year, set T = 1 else set T = 2")
        printcond("   " & result & " is ")
        If isLeapYear(iYear) Then
            T = 1
        Else
            T = 2
            printcond("not ")
        End If
        printlncond("a leap year, so T = " & T)
        printlncond()

        N = Trunc(275.0 * iMonth / 9.0) - T * Trunc((iMonth + 9) / 12.0) + iDay - 30
        printlncond("2. The number of days into the year is given by the formula")
        printlncond("   N = FIX(275 * MONTH / 9) - T * FIX[(MONTH + 9) / 12] + DAY - 30")
        printlncond("     = FIX(275 * " & iMonth & " / 9) - " & T & " * FIX[(" & iMonth &
                    " + 9) / 12] + " & iDay & " - 30 = " & N)
        printlncond()

        result = result & " is " & N & " days into the year"
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '----------------------------------------------------------
    ' Calculate date when given the number of days into a year
    '----------------------------------------------------------
    Private Sub calcDaysIntoYear2Date()
        Dim strDOY As String = removeWhitespace(txtboxData1.Text)
        Dim strYear As String = removeWhitespace(txtboxData2.Text)
        Dim iMonth, iDay, iYear, N, iA, iB, iC, iE As Integer
        Dim result As String

        prt.clearTextArea()
        If Not isValidInt(strDOY, N, HIDE_ERRORS) Then
            ErrMsg("The number of days into the year is invalid - try again", "Days into Year")
            Return
        End If
        If (N < 0) Or (N > 365) Then
            ErrMsg("The number of days into the year is out of range - try again", "Days into Year")
            Return
        End If

        If Not isValidInt(strYear, iYear, HIDE_ERRORS) Then
            ErrMsg("The Year entered is invalid - try again", "Invalid Year")
            Return
        End If

        prt.setBoldFont(True)
        printlncond("Determine the date that is " & N & " days into the year " & iYear, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1. If Year is a leap year, set A = 1523, otherwise set A = 1889")
        printcond("   Since " & iYear & " is ")
        If isLeapYear(iYear) Then
            iA = 1523
        Else
            iA = 1889
            printcond("not ")
        End If
        printlncond("a leap year, A = " & iA)
        printlncond()

        iB = Trunc((N + iA - 122.1) / 365.25)
        printlncond("2. B = FIX[(N + A - 122.1) / 365.25] = FIX[(" & N & " + " & iA & " - 122.1) / 365.25]")
        printlncond("     = " & iB)
        printlncond()

        iC = N + iA - Trunc(365.25 * iB)
        printlncond("3. C = N + A - FIX(365.25 * B) = " & N & " + " & iA & " - FIX(365.25 * " & iB & ")")
        printlncond("     = " & iC)
        printlncond()

        iE = Trunc(iC / 30.6001)
        printlncond("4. E = FIX(C / 30.6001) = FIX(" & iC & " / 30.6001) = " & iE)
        printlncond()

        printlncond("5. If E < 13.5, then Month = E - 1, otherwise, Month = E - 13")
        If (iE < 13.5) Then
            iMonth = iE - 1
        Else
            iMonth = iE - 13
        End If
        printlncond("   Thus, Month = " & iMonth)
        printlncond()

        iDay = iC - Trunc(30.6001 * iE)
        printlncond("6. Finally, Day = C - FIX(30.6001 * E) = " & iC & " - FIX(30.6001 * " & iE & ") = " & iDay)
        printlncond()

        result = N & " days into the year " & iYear & " is the date " & dateToStr(iMonth, iDay, iYear)
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------------------------
    ' Convert a Julian day number to a calendar date
    '------------------------------------------------
    Private Sub calcJD2Calendar()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim JD, JD1, fJD1 As Double
        Dim iJD1, iA, iB, iC, iD, iE, iG As Int64
        Dim iMonth, iYear As Integer
        Dim dDay As Double
        Dim result As String

        prt.clearTextArea()

        If Not isValidReal(strInput, JD, HIDE_ERRORS) Then
            ErrMsg("Julian day number entered is invalid. Try again.", "Invalid JD")
            Return
        End If

        result = Format(JD, JDFormat)
        prt.setBoldFont(True)
        printlncond("Convert " & result & " JD to a Calendar Date", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("1.  Add 0.5 to the Julian day number")
        JD1 = JD + 0.5
        printlncond("    JD1 = JD + 0.5 = " & JD & " + 0.5 = " & JD1)
        printlncond()

        iJD1 = Trunc(JD1)
        fJD1 = Frac(JD1)
        printlncond("2.  Set I = FIX(JD1) and F = FRAC(JD1)")
        printlncond("    Thus, I = FIX(" & JD1 & ") = " & iJD1)
        printlncond("    and F = FRAC(" & JD1 & ") = " & Format(fJD1, "0.0####"))
        printlncond()

        printlncond("3.  If I is larger than 2299160 set A = FIX[(I - 1867216.25) / 36524.25]")
        printlncond("    and set B = I + 1 + A - FIX(A / 4). Otherwise, set B = I.")
        printlncond()
        printcond("    Since I is ")
        If (iJD1 > 2299160) Then
            iA = Trunc((iJD1 - 1867216.25) / 36524.25)
            iB = iJD1 + 1 + iA - Trunc(CDbl(iA) / 4.0)
            printlncond(" greater than 2299160,")
            printlncond("    A = FIX[(" & iJD1 & " - 1867216.25) / 36524.25] = " & iA)
            printlncond("    and B = " & iJD1 & " + 1 + " & iA & " - FIX(" & iA & " / 4) = " & iB)
        Else
            iB = iJD1
            printlncond(" not greater than 2299160, B = " & iB)
        End If
        printlncond()

        iC = iB + 1524
        printlncond("4.  Set C = B + 1524 = " & iB & " + 1524 = " & iC)
        printlncond()

        iD = Trunc((iC - 122.1) / 365.25)
        printlncond("5.  Set D = FIX[(C - 122.1) / 365.25] = FIX[(" & iC & " - 122.1) / 365.25]")
        printlncond("          = " & iD)
        printlncond()

        iE = Trunc(365.25 * iD)
        printlncond("6.  Set E = FIX(365.25 * D) = FIX(365.25 * " & iD & ") = " & iE)
        printlncond()

        iG = Trunc((iC - iE) / 30.6001)
        printlncond("7.  Set G = FIX[(C - E) / 30.6001] = FIX[(" & iC & " - " & iE & ") / 30.6001]")
        printlncond("          = " & iG)
        printlncond()

        dDay = iC - iE + fJD1 - Trunc(30.6001 * iG)
        printlncond("8.  The day is given by Day = C - E + F - FIX(30.6001 * G)")
        printlncond("                            = " & iC & " - " & iE & " + " & Format(fJD1, "0.0####") &
                    " - FIX(30.6001 * " & iG & ")")
        printlncond("                            = " & dDay)
        printlncond()

        printlncond("9.  The month is given by month = G - 1 if G is less than 13.5")
        printlncond("    and month = G - 13 if G is greater than 13.5")
        printcond("    Thus, since G is ")
        If (iG > 13.5) Then
            iMonth = CType(iG - 13, Integer)
            printlncond("greater than 13.5, month = G - 13 = " & iG & " - 13 = " & iMonth)
        Else
            iMonth = CType(iG - 1, Integer)
            printlncond("less than 13.5, month = G - 1 = " & iG & " - 1 = " & iMonth)
        End If
        printlncond()

        printlncond("10. Finally, the year is given by year = D - 4716 if month is more than 2.5")
        printlncond("    and year = D - 4715 if month is less than 2.5")
        printcond("    Thus, since month is ")
        If (iMonth > 2.5) Then
            iYear = CType(iD - 4716, Integer)
            printlncond("more than 2.5, year = D - 4716 = " & iD & " - 4716 = " & iYear)
        Else
            iYear = CType(iD - 4715, Integer)
            printlncond("less than 2.5, year = D - 4715 = " & iD & " - 4715 = " & iYear)
        End If
        printlncond()

        result = result & " JD = " & dateToStr(iMonth, dDay, iYear)
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '------------------------------
    ' See if a year is a leap year
    '------------------------------
    Private Sub calcLeapYear()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim result As String
        Dim iYear As Integer
        Dim sfYear As String
        Dim bPass As Boolean
        Dim bLeapYear As Boolean = False

        prt.clearTextArea()

        If Not isValidInt(strInput, iYear) Then Return

        If (iYear < 1582) Then
            ErrMsg("Year must be > 1581. Try again", "Invalid Year")
            lblResults.Text = ""
            Return
        End If

        sfYear = Format(iYear, "###0")
        result = "The year " & sfYear & " is "

        prt.setBoldFont(True)
        printlncond("Determine if " & sfYear & " is a leap year", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        printlncond("A year is a leap year if two conditions are satisfied:")
        printlncond("  (a) the year must be evenly divisible by 4")
        printlncond("  (b) if the year is a century year, it must also be evenly divisible by 400")
        printlncond()

        printlncond("1. Check condition 'a'")
        bPass = isEvenlyDivisible(iYear, 4)
        printlncond("   year / 4 = " & sfYear & " / 4 = " & iYear / 4)
        printcond("   So, " & sfYear & " is ")
        If Not bPass Then printcond("not ")
        printlncond("evenly divisible by 4.")
        If Not bPass Then printlncond("   No need to check any further.")

        If bPass Then
            printlncond()
            printlncond("2. Check condition 'b'. A year is a century year if it is evenly divisible by 100.")
            bPass = isEvenlyDivisible(iYear, 100)
            printlncond("   year / 100 = " & sfYear & " / 100 = " & iYear / 100)
            printcond("   So, " & sfYear & " is ")
            If Not bPass Then
                printcond("not ")
                bLeapYear = True
            End If
            printlncond("evenly divisible by 100.")
            If Not bPass Then printlncond("   No need to check any further since year is not a century year.")
        End If

        If bPass Then
            printlncond()
            printlncond("3. Since " & sfYear & " is a century year, check if it is evenly divisible by 400.")
            bPass = isEvenlyDivisible(iYear, 400)
            printlncond("   year / 400 = " & sfYear & " / 400 = " & iYear / 400)
            printcond("   So, " & sfYear & " is ")
            If bPass Then
                bLeapYear = True
            Else
                printcond("not ")
                bLeapYear = False
            End If
            printlncond("evenly divisible by 400.")
        End If

        If Not bLeapYear Then result = result & "not "
        result = result & "a leap year"
        printlncond()
        printlncond(result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Routines to do the actual work for the Local vs Greenwich menu
    '-------------------------------------------------------------------------------------------------------

    '--------------------
    ' Convert GST to LST
    '--------------------
    Private Sub calcGST2LST()
        Dim strData1 As String = removeWhitespace(txtboxData1.Text)
        Dim strLon As String = removeWhitespace(txtboxLon.Text)
        Dim timeObj As ASTTime = New ASTTime()
        Dim dLonObj As ASTLatLon = New ASTLatLon()
        Dim dLongitude, GST, LST, adjustment As Double
        Dim result, strtmp As String

        prt.clearTextArea()

        If Not isValidTime(strData1, timeObj) Then Return
        If Not isValidLon(strLon, dLonObj) Then Return

        dLongitude = dLonObj.getLon()
        strtmp = timeToStr(timeObj, HMSFORMAT)
        result = strtmp & " GST at Longitude " & strLon

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to LST", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        GST = timeObj.getDecTime()
        printlncond("1. Convert the GST entered to decimal format. GST = " & Format(GST, decHoursFormat))
        printlncond()

        adjustment = dLongitude / 15.0
        printlncond("2. Calculate an adjustment for longitude.")
        printlncond("   ADJUST = (Longitude) / 15 = (" & dLonObj.getLon() & ") / 15")
        printlncond("          = " & Format(adjustment, "###.######"))
        printlncond()

        LST = GST + adjustment
        printlncond("3. LST = GST + ADJUST = " & Format(GST, decHoursFormat) & " + (" &
                    Format(adjustment, "###.######") & ") = " & Format(LST, decHoursFormat))
        printlncond()

        printlncond("4. If LST is negative, add 24. If LST is greater than 24, subtract 24.")
        printlncond("   Otherwise, make no adjustment.")
        printcond("   Thus, LST = ")
        If (LST < 0) Then
            LST = LST + 24
            printcond(" LST + 24 = ")
        End If
        If (LST > 24) Then
            LST = LST - 24
            printcond(" LST - 24 = ")
        End If
        printlncond(Format(LST, decHoursFormat))
        printlncond()

        strtmp = timeToStr(LST, HMSFORMAT)
        printlncond("5. Finally, convert LST to hours, minutes, seconds")
        printlncond("   So, LST = " & strtmp)
        printlncond()

        result = result & " = " & strtmp & " LST"
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------
    ' Convert GST to UT
    '-------------------
    Private Sub calcGST2UT()
        Dim strData1 As String = removeWhitespace(txtboxData1.Text)
        Dim strData2 As String = removeWhitespace(txtboxData2.Text)
        Dim timeObj As ASTTime = New ASTTime()
        Dim dateObj As ASTDate = New ASTDate()
        Dim days As Integer
        Dim JD, JD0, dT, dR, dB, T0, UT, GST, dA As Double
        Dim result, strtmp As String
        Dim iYear As Integer

        prt.clearTextArea()

        If Not isValidTime(strData1, timeObj) Then Return
        If Not isValidDate(strData2, dateObj) Then Return

        iYear = dateObj.getYear()

        strtmp = timeToStr(timeObj, HMSFORMAT)
        result = strtmp & " GST on " & dateToStr(dateObj)

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to UT", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        JD = dateToJD(dateObj)
        printlncond("1.  Convert given date to a Julian day number")
        printlncond("    Thus, " & dateToStr(dateObj) & " = " & Format(JD, JDFormat) & " JD")
        printlncond()

        JD0 = dateToJD(1, 0, iYear)
        printlncond("2.  Calculate the Julian day number for January 0.0 of the given year")
        printlncond("    Thus, JD0 = " & dateToStr(1, 0.0, iYear) & " = " & Format(JD0, JDFormat))
        printlncond()

        days = CType(JD - JD0, Integer)
        printlncond("3.  Subtract step 2 from step 1 to get number of days into the year")
        printlncond("    This gives Days = " & days)
        printlncond()

        dT = (JD0 - 2415020.0) / 36525.0
        printlncond("4.  Let T = [(JD0 - 2415020.0) / 36525] = [(" & Format(JD0, JDFormat) & " - 2415020.0) / 36525]")
        printlncond("          = " & Format(dT, "#0.0########"))
        printlncond()

        dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
        printlncond("5.  Let R = 6.6460656 + T * 2400.051262 + T * T * 0.00002581")
        printlncond("          = 6.6460656+" & Format(dT, "#0.0########") & "*2400.05162" &
                    "+" & Format(dT, "#0.0########") & "*" & Format(dT, "#0.0########") & "*0.00002581")
        printlncond("          = " & Format(dR, "#0.0########"))
        printlncond()

        dB = 24.0 - dR + 24.0 * (iYear - 1900)
        printlncond("6.  Let B = 24 - R + 24*(year - 1900) = 24 - " & Format(dR, "#0.0########") &
                    " + 24*(" & iYear & " - 1900)")
        printlncond("          = " & Format(dB, "#0.0########"))
        printlncond()

        T0 = 0.0657098 * (days) - dB
        printlncond("7.  Let T0 = 0.0657098*days - B = 0.0657098*" & days & " - " & Format(dB, "#0.0########"))
        printlncond("           = " & Format(T0, "#0.0########"))
        printlncond()

        printlncond("8.  If the result of step 7 is negative, add 24 hours. If the result of step 7")
        printlncond("    is greater than 24, subtract 24 hours. Otherwise, make no adjustment.")
        printcond("    Thus, T0 = ")
        If (T0 < 0) Then
            T0 = T0 + 24
            printcond(" T0 + 24 = ")
        End If
        printlncond(Format(T0, "#0.0########"))
        printlncond()

        GST = timeObj.getDecTime()
        printlncond("9.  Convert the GST entered to decimal if HMS was entered. Thus, GST = " & Format(GST, decHoursFormat))
        printlncond()

        dA = GST - T0
        printcond("10. Let A = GST - T0 = " & Format(GST, decHoursFormat) & " - " & Format(T0, "#0.0########"))
        printlncond(" = " & Format(dA, "#0.0########"))
        printlncond()

        printlncond("11. If A is negative, add 24. Otherwise, make no adjustment.")
        printcond("    Thus, A = ")
        If (dA < 0) Then
            dA = dA + 24
            printcond(" A + 24 = ")
        End If
        printlncond(Format(dA, "#0.0########"))
        printlncond()

        UT = dA * 0.99727
        printlncond("12. UT = A * 0.997270 = " & Format(dA, "#0.0########") & " * 0.997270 = " & Format(UT, decHoursFormat))
        printlncond()

        strtmp = timeToStr(UT, HMSFORMAT)
        printlncond("13. Finally, convert UT to hours, minutes, seconds")
        printlncond("    So, UT = " & strtmp)
        printlncond()

        result = result & " = " & strtmp & " UT"
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------
    ' Convert LCT to UT
    '-------------------
    Private Sub calcLCT2UT()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim strLon As String = removeWhitespace(txtboxLon.Text)
        Dim timeObj As ASTTime = New ASTTime()
        Dim dLonObj As ASTLatLon = New ASTLatLon()
        Dim LCT, UT, decTime, dAdjustment, dT As Double
        Dim dLongitude As Double = 0.0
        Dim result As String
        Dim saveTZone As String = ""
        Dim dayChange As String = ""
        Dim strtmp As String = ""
        Dim radbtn As TimeZoneType = getSelectedRBStatus()

        prt.clearTextArea()

        If Not isValidTime(strInput, timeObj) Then Return
        result = timeToStr(timeObj, HMSFORMAT) & " LCT"

        ' If necessary, validate the longitude
        If radbtn = TimeZoneType.LONGITUDE Then
            If Not isValidLon(strLon, dLonObj) Then Return
            dLongitude = dLonObj.getLon()
            strLon = lonToStr(dLonObj, DECFORMAT)
        End If

        If chkboxDST.Checked Then
            saveTZone = "(DST, "
        Else
            saveTZone = "("
        End If

        If radbtn = TimeZoneType.LONGITUDE Then
            saveTZone = saveTZone & "at Longitude " & strLon & ")"
        Else
            saveTZone = saveTZone & "in " & radbtn.ToString & " time zone)"
        End If

        prt.setBoldFont(True)
        printlncond("Convert " & result & " " & saveTZone & " to UT", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        LCT = timeObj.getDecTime()
        printlncond("1. Convert LCT to decimal format. LCT = " & Format(LCT, decHoursFormat))
        printlncond()

        printlncond("2. If necessary, adjust for daylight saving time.")
        If chkboxDST.Checked Then
            decTime = LCT - 1
            printlncond("   Since this is on daylight saving time, subtract 1 hour.")
        Else
            decTime = LCT
            printlncond("   No adjustment is necessary since this is not daylight saving time.")
        End If
        printlncond("   This gives T = " & Format(decTime, decHoursFormat))
        printlncond()

        printlncond("3. Calculate a time zone adjustment")
        dAdjustment = timeZoneAdjustment(True, radbtn, dLongitude)
        printlncond()

        dT = decTime - dAdjustment
        printlncond("4. Subtract the time zone adjustment in step 3 from the")
        printlncond("   result of step 2 giving")
        printlncond("   UT = " & Format(decTime, decHoursFormat) & " - (" & Format(dAdjustment, decHoursFormat) &
                    ") = " & Format(dT, decHoursFormat))
        printlncond()

        UT = dT
        printlncond("5. If the result of step 4 is negative, add 24. If the result of step 4")
        printlncond("   is greater than 24, subtract 24. Otherwise, make no further adjustments.")
        printcond("   Thus, UT = " & Format(UT, decHoursFormat))
        If (dT > 24) Then
            UT = dT - 24.0
            dayChange = " (next day)"
            printcond(" - 24 = " & Format(UT, decHoursFormat) & dayChange)
        End If
        If (dT < 0) Then
            UT = dT + 24.0
            dayChange = " (previous day)"
            printcond(" + 24 = " & Format(UT, decHoursFormat) & dayChange)
        End If
        printlncond()
        printlncond()

        strtmp = timeToStr(UT, HMSFORMAT)
        printlncond("6. Finally, convert to hours, minutes, seconds format, which gives")
        printlncond("   UT = " & strtmp)
        printlncond()

        result = result & " " & saveTZone & " = " & strtmp & " UT" & dayChange
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '--------------------
    ' Convert LST to GST
    '--------------------
    Private Sub calcLST2GST()
        Dim strData1 As String = removeWhitespace(txtboxData1.Text)
        Dim strLon As String = removeWhitespace(txtboxLon.Text)
        Dim timeObj As ASTTime = New ASTTime()
        Dim dLonObj As ASTLatLon = New ASTLatLon()
        Dim dLongitude, GST, LST, adjustment As Double
        Dim result, strtmp As String

        prt.clearTextArea()

        If Not isValidTime(strData1, timeObj) Then Return
        If Not isValidLon(strLon, dLonObj) Then Return
        dLongitude = dLonObj.getLon()
        strtmp = timeToStr(timeObj, HMSFORMAT)
        result = strtmp & " LST at Longitude " & strLon

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to GST", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        LST = timeObj.getDecTime()
        printlncond("1. Convert the LST entered to decimal format. LST = " & Format(LST, decHoursFormat))
        printlncond()

        adjustment = dLongitude / 15.0
        printlncond("2. Calculate an adjustment for longitude.")
        printlncond("   ADJUST = (Longitude) / 15 = (" & dLonObj.getLon() & ") / 15")
        printlncond("          = " & Format(adjustment, "###.######"))
        printlncond()

        GST = LST - adjustment
        printlncond("3. GST = LST - ADJUST = " & Format(LST, decHoursFormat) & " - (" &
                    Format(adjustment, "###.######") & ") = " & Format(GST, decHoursFormat))
        printlncond()

        printlncond("4. If GST is negative, add 24. If GST is greater than 24, subtract 24.")
        printlncond("   Otherwise, make no adjustment.")
        printcond("   Thus, GST = ")
        If (GST < 0) Then
            GST = GST + 24
            printcond(" GST + 24 = ")
        End If
        If (GST > 24) Then
            GST = GST - 24
            printcond(" GST - 24 = ")
        End If
        printlncond(Format(GST, decHoursFormat))
        printlncond()

        strtmp = timeToStr(GST, HMSFORMAT)
        printlncond("5. Finally, convert GST to hours, minutes, seconds")
        printlncond("   So, GST = " & strtmp)
        printlncond()

        result = result & " = " & strtmp & " GST"
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------
    ' Convert UT to GST
    '-------------------
    Private Sub calcUT2GST()
        Dim strData1 As String = removeWhitespace(txtboxData1.Text)
        Dim strData2 As String = removeWhitespace(txtboxData2.Text)
        Dim timeObj As ASTTime = New ASTTime()
        Dim dateObj As ASTDate = New ASTDate()
        Dim days, JD, JD0, dT, dR, dB, T0, UT, GST As Double
        Dim result, strtmp As String
        Dim iYear As Integer

        prt.clearTextArea()

        If Not isValidTime(strData1, timeObj) Then Return
        If Not isValidDate(strData2, dateObj) Then Return
        iYear = dateObj.getYear()

        result = timeToStr(timeObj, HMSFORMAT) & " UT on " & dateToStr(dateObj)

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to GST", CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        JD = dateToJD(dateObj)
        printlncond("1.  Convert given date to a Julian day number")
        printlncond("    Thus, " & dateToStr(dateObj) & " = " & Format(JD, JDFormat) & " JD")
        printlncond()

        JD0 = dateToJD(1, 0, iYear)
        printlncond("2.  Calculate the Julian day number for January 0.0 of the given year")
        printlncond("    Thus, JD0 = " & dateToStr(1, 0.0, iYear) & " = " & Format(JD0, JDFormat))
        printlncond()

        days = JD - JD0
        printlncond("3.  Subtract step 2 from step 1 to get number of days into the year")
        printlncond("    This gives Days = " & days)
        printlncond()

        dT = (JD0 - 2415020.0) / 36525.0
        printlncond("4.  Let T = [(JD0 - 2415020.0) / 36525] = [(" & Format(JD0, JDFormat) & " - 2415020.0) / 36525]")
        printlncond("          = " & Format(dT, "#0.0########"))
        printlncond()

        dR = 6.6460656 + dT * 2400.051262 + dT * dT * 0.00002581
        printlncond("5.  Let R = 6.6460656 + T * 2400.051262 + T * T * 0.00002581")
        printlncond("          = 6.6460656+" & Format(dT, "#0.0########") & "*2400.05162" &
                    "+" & Format(dT, "#0.0########") & "*" & Format(dT, "#0.0########") & "*0.00002581")
        printlncond("          = " & Format(dR, "#0.0########"))
        printlncond()

        dB = 24.0 - dR + 24.0 * (iYear - 1900)
        printlncond("6.  Let B = 24 - R + 24*(year - 1900) = 24 - " & Format(dR, "#0.0########") &
                    " + 24*(" & iYear & " - 1900)")
        printlncond("          = " & Format(dB, "#0.0########"))
        printlncond()

        T0 = 0.0657098 * (days) - dB
        printlncond("7.  Let T0 = 0.0657098*days - B = 0.0657098*" & days & " - " & Format(dB, "#0.0########"))
        printlncond("           = " & Format(T0, "#0.0########"))
        printlncond()

        UT = timeObj.getDecTime()
        printlncond("8.  Convert the UT entered to decimal if HMS was entered. Thus, UT = " & Format(UT, decHoursFormat))
        printlncond()

        GST = T0 + UT * 1.002738
        printlncond("9.  GST = T0 + UT * 1.002738 = " & Format(T0, "##0.00####") & " + " & Format(UT, decHoursFormat) &
                    " * 1.002738 = " & Format(GST, decHoursFormat))
        printlncond()

        printlncond("10. If GST is negative, add 24. If GST is greater than 24, subtract 24.")
        printlncond("    Otherwise, make no adjustment.")
        printcond("    Thus, GST = ")
        If (GST < 0) Then
            GST = GST + 24
            printcond(" GST + 24 = ")
        End If
        If (GST > 24) Then
            GST = GST - 24
            printcond(" GST - 24 = ")
        End If
        printlncond(Format(GST, decHoursFormat))
        printlncond()

        strtmp = timeToStr(GST, HMSFORMAT)
        printlncond("11. Finally, convert GST to hours, minutes, seconds")
        printlncond("    So, GST = " & strtmp)
        printlncond()

        result = result & " = " & strtmp & " GST"
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------
    ' Convert UT to LCT
    '-------------------
    Private Sub calcUT2LCT()
        Dim strInput As String = removeWhitespace(txtboxData1.Text)
        Dim strLon As String = removeWhitespace(txtboxLon.Text)
        Dim timeObj As ASTTime = New ASTTime()
        Dim dLonObj As ASTLatLon = New ASTLatLon()
        Dim LCT, UT, dAdjustment, dT As Double
        Dim dLongitude As Double = 0.0
        Dim result As String
        Dim saveTZone As String = ""
        Dim dayChange As String = ""
        Dim strtmp As String = ""
        Dim radbtn As TimeZoneType = getSelectedRBStatus()

        prt.clearTextArea()

        If Not isValidTime(strInput, timeObj) Then Return
        result = timeToStr(timeObj, HMSFORMAT) & " UT"

        ' If necessary, validate the longitude
        If radbtn = TimeZoneType.LONGITUDE Then
            If Not isValidLon(strLon, dLonObj) Then Return
            dLongitude = dLonObj.getLon()
            strLon = lonToStr(dLonObj, DECFORMAT)
        End If

        If chkboxDST.Checked Then
            saveTZone = "(DST, "
        Else
            saveTZone = "("
        End If

        If radbtn = TimeZoneType.LONGITUDE Then
            saveTZone = saveTZone & "at Longitude " & strLon & ")"
        Else
            saveTZone = saveTZone & "in " & radbtn.ToString & " time zone)"
        End If

        prt.setBoldFont(True)
        printlncond("Convert " & result & " to LCT " & saveTZone, CENTERTXT)
        prt.setBoldFont(False)
        printlncond()

        prt.setFixedWidthFont()

        UT = timeObj.getDecTime()
        printlncond("1. Convert UT to decimal format. UT = " & Format(UT, decHoursFormat))
        printlncond()

        printlncond("2. Calculate a time zone adjustment")
        dAdjustment = timeZoneAdjustment(True, radbtn, dLongitude)
        printlncond()

        dT = UT + dAdjustment
        printlncond("3. Add the time zone adjustment from step 2 to the")
        printlncond("   result of step 1 giving")
        printlncond("   LCT = " & Format(UT, decHoursFormat) & " + (" & Format(dAdjustment, decHoursFormat) &
                    ") = " & Format(dT, decHoursFormat))
        printlncond()

        printlncond("4. If the result of step 3 is negative, add 24. If the result of step 3")
        printlncond("   is greater than 24, subtract 24. Otherwise, make no further adjustments.")
        printcond("   Thus, LCT = " & Format(dT, decHoursFormat))
        LCT = dT
        If (dT > 24) Then
            LCT = dT - 24.0
            dayChange = " (next day)"
            printcond(" - 24 = " & Format(LCT, decHoursFormat) & dayChange)
        End If
        If (dT < 0) Then
            LCT = dT + 24.0
            dayChange = " (previous day)"
            printcond(" + 24 = " & Format(LCT, decHoursFormat) & dayChange)
        End If
        printlncond()
        printlncond()

        printlncond("5. If necessary, adjust for daylight saving time.")
        If chkboxDST.Checked Then
            LCT = LCT + 1
            printlncond("   Since this is on daylight saving time, add 1 hour.")
        Else
            printlncond("   No adjustment is necessary since this is not daylight saving time.")
        End If
        printlncond("   This gives LCT = " & Format(LCT, decHoursFormat))
        printlncond()

        strtmp = timeToStr(LCT, HMSFORMAT)
        printlncond("6. Finally, convert to hours, minutes, seconds format, which gives")
        printlncond("   LCT = " & strtmp)
        printlncond()

        result = result & " = " & strtmp & " LCT " & dayChange & " " & saveTZone
        printlncond("Therefore, " & result)

        lblResults.Text = result
        prt.setProportionalFont()
        prt.resetCursor()
    End Sub

    '-------------------------------------------------------------------------------------------------------
    ' Some common helper routines used only in this class
    '-------------------------------------------------------------------------------------------------------

    '--------------------------------------
    ' Clear all the text areas in the GUI
    '    ' Clear all text areas
    '--------------------------------------
    Private Sub clearAllTextAreas()
        setDataLabels("", "")
        prt.clearTextArea()
        lblResults.Text = ""
    End Sub

    '--------------------------------------------------------------
    ' If the show interim calculations checkbox is checked, output
    ' txt to the scrollable output area. These are wrappers around
    ' ASTUtils.prt.println.
    '
    ' These methods are overloaded to allow flexibility in what
    ' parms are passed to the prt routines
    '
    ' txt           text to be displayed in the output area
    ' centerTxt     if true, center the text
    '--------------------------------------------------------------
    Private Sub printcond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.print(txt)
    End Sub
    Private Sub printlncond()
        If chkboxShowInterimCalcs.Checked Then prt.println()
    End Sub
    Private Sub printlncond(ByVal txt As String)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt)
    End Sub
    Private Sub printlncond(ByVal txt As String, ByVal centerTxt As Boolean)
        If chkboxShowInterimCalcs.Checked Then prt.println(txt, centerTxt)
    End Sub

    '--------------------------------------
    ' Set the GUI Data Labels. This method
    ' is overloaded to allow setting only
    ' one or both data labels.
    '
    ' d1        text for 1st data label
    ' d2        text for 2nd data label
    '-------------------------------------
    Private Sub setDataLabels(ByVal d1 As String)
        setDataLabels(d1, "")
    End Sub
    Private Sub setDataLabels(ByVal d1 As String, ByVal d2 As String)
        lblData1.Text = d1
        lblData2.Text = d2
        txtboxData1.Text = ""
        txtboxData2.Text = ""
    End Sub

    '------------------------------------------------
    ' Determine a time zone adjustment.
    '
    ' flag          if true, print out intermediate
    '               results
    ' radbtn        radio button setting from the GUI
    ' longitude     longitude value from the GUI
    '
    ' Returns a time zone adjustment.
    '------------------------------------------------
    Private Function timeZoneAdjustment(ByVal flag As Boolean, ByVal radbtn As TimeZoneType, ByVal longitude As Double) As Double
        Dim adjustment As Double

        If (radbtn = TimeZoneType.LONGITUDE) Then
            adjustment = Math.Round(longitude / 15.0)
            If (flag) Then
                printlncond("   For a given longitude, the time zone adjustment factor is")
                printlncond("   determined by the formula")
                printlncond("   Adjustment = ROUND(Longitude / 15)")
                printlncond("              = ROUND(" + Format(longitude, latlonFormat) & " / 15)")
                printlncond("              = ROUND(" + Format(longitude / 15.0, latlonFormat) & ") = " &
                            Format(adjustment, decHoursFormat))
            End If
        Else
            adjustment = radbtn

            If (flag) Then printlncond("   For the " & radbtn.ToString &
                " time zone, the time zone adjustment is " & Format(adjustment, decHoursFormat))
        End If
        Return adjustment
    End Function

End Class
