﻿'**********************************************************
'                     Copyright (c) 2018
'                   Author: J. L. Lawrence
'
' This class is used to associate extra items with a
' menu entry to make it easier to figure out
' what to do when the user clicks on a menu entry.
'**********************************************************

Option Explicit On
Option Strict On

Friend Class ChapMenuItem
    Inherits System.Windows.Forms.ToolStripMenuItem

    ' Define what menu item has been selected to know what to do when the calculate button is pressed
    Friend Enum CalculationType
        NO_CALC_SELECTED
        ' Time Conversions
        LEAP_YEARS
        CALENDAR_2_JD
        JD_2_CALENDAR
        DAY_OF_WEEK
        DATE_2_DAYS_INTO_YEAR
        DAYS_INTO_YEAR_2_DATE
        ' Local vs Greenwich
        LCT_2_UT
        UT_2_LCT
        UT_2_GST
        GST_2_UT
        GST_2_LST
        LST_2_GST
    End Enum

    Friend eCalcType As CalculationType  ' what type of calculation to perform

    Sub New(ByVal CalcType As CalculationType)
        eCalcType = CalcType
    End Sub
End Class
