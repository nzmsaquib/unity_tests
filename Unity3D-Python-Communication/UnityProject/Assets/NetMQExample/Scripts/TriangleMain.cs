﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class TriangleMain : MonoBehaviour
{
	public GameObject pt0, pt1, pt2;
	public GameObject line0, line1;

	private HelloRequester _helloRequester;

	GameObject requestingObject = null;

	public int delayer = 0;

	// Start is called before the first frame update
	void Start()
    {
		line0.GetComponent<LineRenderer>().positionCount = 2;
		line1.GetComponent<LineRenderer>().positionCount = 2;

		_helloRequester = new HelloRequester();

		// update geometry system string in the requester thread
		_helloRequester.geosystem =
			pt0.transform.position.x.ToString() + ", " +
			pt0.transform.position.y.ToString() + ", " +
			pt1.transform.position.x.ToString() + ", " +
			pt1.transform.position.y.ToString() + ", " +
			pt2.transform.position.x.ToString() + ", " +
			pt2.transform.position.y.ToString();

		_helloRequester.Start();
	}

	// Update is called once per frame
	void Update()
    {
		line0.GetComponent<LineRenderer>().SetPosition(0, pt0.transform.position);
		line0.GetComponent<LineRenderer>().SetPosition(1, pt1.transform.position);

		line1.GetComponent<LineRenderer>().SetPosition(0, pt0.transform.position);
		line1.GetComponent<LineRenderer>().SetPosition(1, pt2.transform.position);

		if (_helloRequester.serverUpdateCame && _helloRequester.requestGeometry)
		{
			_helloRequester.serverUpdateCame = false;

			// move the points according to the server update
			string[] coords = _helloRequester.serverUpdate.Split(',');

			pt0.transform.position = new Vector3(float.Parse(coords[0], CultureInfo.InvariantCulture.NumberFormat),
				float.Parse(coords[1], CultureInfo.InvariantCulture.NumberFormat), 0.0f);

			pt1.transform.position = new Vector3(float.Parse(coords[2], CultureInfo.InvariantCulture.NumberFormat),
				float.Parse(coords[3], CultureInfo.InvariantCulture.NumberFormat), 0.0f);

			pt2.transform.position = new Vector3(float.Parse(coords[4], CultureInfo.InvariantCulture.NumberFormat),
				float.Parse(coords[5], CultureInfo.InvariantCulture.NumberFormat), 0.0f);

			//_helloRequester.requestGeometry = false;
		}

		// when clicked on an object
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit) &&
				hit.collider.gameObject == pt0.gameObject)
			{
				//pt0.transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);

				GameObject.Find("pt0_text").transform.position = pt0.transform.position;

				requestingObject = pt0.gameObject;

				_helloRequester.requestGeometry = true;
			}

			else if (Physics.Raycast(ray, out hit) &&
				hit.collider.gameObject == pt1.gameObject)
			{
				//pt1.transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
				GameObject.Find("pt1_text").transform.position = pt1.transform.position;

				requestingObject = pt1.gameObject;

				_helloRequester.requestGeometry = true;
			}

			else if (Physics.Raycast(ray, out hit) &&
				hit.collider.gameObject == pt2.gameObject)
			{
				//pt2.transform.position = new Vector3(hit.point.x, hit.point.y, 0.0f);
				requestingObject = pt2.gameObject;
			}
		}

		// when mouse up
		else if (Input.GetMouseButtonUp(0))
		{
			_helloRequester.requestGeometry = false;
			requestingObject = null;
		}

		// when object is dragged or mouse button is down in general, and requesting geometry
		if (Input.GetMouseButton(0) && _helloRequester.requestGeometry)
		{
			// is there any object that was clicked in the beginning of this drag?
			if (requestingObject != null)
			{
				if (requestingObject == pt0.gameObject)
				{
					Vector3 hit = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					_helloRequester.geosystem =
					hit.x.ToString() + ", " +
					hit.y.ToString() + ", " +
					pt1.transform.position.x.ToString() + ", " +
					pt1.transform.position.y.ToString() + ", " +
					pt2.transform.position.x.ToString() + ", " +
					pt2.transform.position.y.ToString();
				}
				else if (requestingObject == pt1.gameObject)
				{
					Vector3 hit = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					_helloRequester.geosystem =
					pt0.transform.position.x.ToString() + ", " +
					pt0.transform.position.y.ToString() + ", " +
					hit.x.ToString() + ", " +
					hit.y.ToString() + ", " +
					pt2.transform.position.x.ToString() + ", " +
					pt2.transform.position.y.ToString();
				}
			}
		}

	}

	public void stopThread()
	{
		_helloRequester.Stop();
	}

	void OnDestroy()
	{
		_helloRequester.Stop();
	}
}
