# -*- coding: utf-8 -*-

"""This module will test the functions of Python-Solvespace."""

__author__ = "Saquib"
__copyright__ = "Copyright (C) 2019"
__license__ = "AGPL"
__email__ = "nzm.saquib@gmail.com"

from math import radians
import time
from slvs import ResultFlag, SolverSystem, make_quaternion
import zmq

def parseMessage(message):
    # the message contains the point p0, p1, p2 information from unity
    ps = [float(i) for i in message.split(',')]
    return ps

def setupAndSolve(ps):
    sys = SolverSystem()
    wp = sys.create_2d_base()

    p0 = sys.add_point_2d(ps[0], ps[1], wp)
    sys.dragged(p0, wp)
    
    p1 = sys.add_point_2d(ps[2], ps[3], wp)
    #sys.dragged(p1, wp)

    line0 = sys.add_line_2d(p0, p1, wp)

    p2 = sys.add_point_2d(ps[4], ps[5], wp)
    #sys.dragged(p2, wp)
    
    line1 = sys.add_line_2d(p1, p2, wp)

    line2 = sys.add_line_2d(p0, p2, wp)
    
    sys.angle(line0, line2, 90, wp)

    #sys.angle(line1, line2, 45, wp)

    result_flag = sys.solve()
    print (result_flag)
    
    # compose message to send back
    fm = str(sys.params(p0.params)[0]) + ", " + str(sys.params(p0.params)[1]) + ", "
    fm = fm + str(sys.params(p1.params)[0]) + ", " + str(sys.params(p1.params)[1]) + ", "
    fm = fm + str(sys.params(p2.params)[0]) + ", " + str(sys.params(p2.params)[1])

    print ("outgoing: " + fm)

    return fm

def main():
    try:
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.bind("tcp://*:5555")

        while True:
            #  Wait for next request from client
            message = socket.recv()
            print("Received request: %s" % message)

            #  Do some 'work'.
            #  Try reducing sleep time to 0.01 to see how blazingly fast it communicates
            #  In the real world usage, you just need to replace time.sleep() with
            #  whatever work you want python to do, maybe a machine learning task?
            if message != "":
                t0 = time.time()
                ps = parseMessage(message)
                msg = setupAndSolve(ps)
                t1 = time.time()
                print (t1 - t0)
            else:
                msg = "None"

            #  Send reply back to client
            #  In the real world usage, after you finish your work, send your output here
            socket.send(msg)

    except KeyboardInterrupt:
        # doesn't seem to reach here. Check thoroughly if it reaches here. Is the while loop stopping it somehow?
        #socket.setsockopt(zmq.LINGER, 0)
        context.term()


if __name__ == '__main__':
    main()
