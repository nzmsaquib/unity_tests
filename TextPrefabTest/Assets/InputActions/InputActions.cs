// GENERATED AUTOMATICALLY FROM 'Assets/InputActions/InputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions"",
    ""maps"": [
        {
            ""name"": ""NoyonActionMap"",
            ""id"": ""18724661-ba87-434f-b18e-1a2a9b6b10eb"",
            ""actions"": [
                {
                    ""name"": ""PenPosition"",
                    ""type"": ""PassThrough"",
                    ""id"": ""b3ba335d-c0e6-41a3-8970-62ddc025838b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""PenTip"",
                    ""type"": ""PassThrough"",
                    ""id"": ""bd166084-6931-43d1-ab01-e92e32ab5205"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2b2f4512-9c6e-410c-870e-73d164807305"",
                    ""path"": ""<Pen>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PenPosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""22e82d10-b4d8-42e7-a7d1-e7ea16a80d29"",
                    ""path"": ""<Pen>/tip"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""PenTip"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // NoyonActionMap
        m_NoyonActionMap = asset.FindActionMap("NoyonActionMap", throwIfNotFound: true);
        m_NoyonActionMap_PenPosition = m_NoyonActionMap.FindAction("PenPosition", throwIfNotFound: true);
        m_NoyonActionMap_PenTip = m_NoyonActionMap.FindAction("PenTip", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // NoyonActionMap
    private readonly InputActionMap m_NoyonActionMap;
    private INoyonActionMapActions m_NoyonActionMapActionsCallbackInterface;
    private readonly InputAction m_NoyonActionMap_PenPosition;
    private readonly InputAction m_NoyonActionMap_PenTip;
    public struct NoyonActionMapActions
    {
        private @InputActions m_Wrapper;
        public NoyonActionMapActions(@InputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @PenPosition => m_Wrapper.m_NoyonActionMap_PenPosition;
        public InputAction @PenTip => m_Wrapper.m_NoyonActionMap_PenTip;
        public InputActionMap Get() { return m_Wrapper.m_NoyonActionMap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(NoyonActionMapActions set) { return set.Get(); }
        public void SetCallbacks(INoyonActionMapActions instance)
        {
            if (m_Wrapper.m_NoyonActionMapActionsCallbackInterface != null)
            {
                @PenPosition.started -= m_Wrapper.m_NoyonActionMapActionsCallbackInterface.OnPenPosition;
                @PenPosition.performed -= m_Wrapper.m_NoyonActionMapActionsCallbackInterface.OnPenPosition;
                @PenPosition.canceled -= m_Wrapper.m_NoyonActionMapActionsCallbackInterface.OnPenPosition;
                @PenTip.started -= m_Wrapper.m_NoyonActionMapActionsCallbackInterface.OnPenTip;
                @PenTip.performed -= m_Wrapper.m_NoyonActionMapActionsCallbackInterface.OnPenTip;
                @PenTip.canceled -= m_Wrapper.m_NoyonActionMapActionsCallbackInterface.OnPenTip;
            }
            m_Wrapper.m_NoyonActionMapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @PenPosition.started += instance.OnPenPosition;
                @PenPosition.performed += instance.OnPenPosition;
                @PenPosition.canceled += instance.OnPenPosition;
                @PenTip.started += instance.OnPenTip;
                @PenTip.performed += instance.OnPenTip;
                @PenTip.canceled += instance.OnPenTip;
            }
        }
    }
    public NoyonActionMapActions @NoyonActionMap => new NoyonActionMapActions(this);
    public interface INoyonActionMapActions
    {
        void OnPenPosition(InputAction.CallbackContext context);
        void OnPenTip(InputAction.CallbackContext context);
    }
}
