﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CategoryFilterMenuBehavior : MonoBehaviour
{
	public GameObject parent_function;

	private void OnDestroy()
	{
		// refresh the parent function for updated children membership
		parent_function.GetComponent<functionLine_script>().updateChildrenMembershipFromCategoryFilter();
	}
}
