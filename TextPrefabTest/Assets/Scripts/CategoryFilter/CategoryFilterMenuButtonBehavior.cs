﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryFilterMenuButtonBehavior : MonoBehaviour
{
	public bool selected = false;

	public GameObject parent_function;

	public void onClick()
	{
		if (!selected)
		{
			selected = true;
			transform.GetChild(0).GetComponent<Text>().color = Color.green;
		}
		else
		{
			selected = false;
			transform.GetChild(0).GetComponent<Text>().color = Color.black;
		}
	}

    // Start is called before the first frame update
    void Start()
    {
		updateSelectedBoolean();

		// set text color when loading
		if (selected)
		{
			transform.GetChild(0).GetComponent<Text>().color = Color.green;
		}
		else
		{
			transform.GetChild(0).GetComponent<Text>().color = Color.black;
		}
	}

	private void OnDestroy()
	{
		// update parent function's category filter
		if (selected)
		{
			if (this.name == "set") parent_function.GetComponent<functionLine_script>().categoryFilter.set = true;
			if (this.name == "circle") parent_function.GetComponent<functionLine_script>().categoryFilter.circle = true;
			if (this.name == "box") parent_function.GetComponent<functionLine_script>().categoryFilter.box = true;
			if (this.name == "ellipse") parent_function.GetComponent<functionLine_script>().categoryFilter.ellipse = true;
			if (this.name == "arrow") parent_function.GetComponent<functionLine_script>().categoryFilter.arrow = true;
			if (this.name == "horizontal_ellipse") parent_function.GetComponent<functionLine_script>().categoryFilter.horizontal_ellipse = true;
			if (this.name == "triangle") parent_function.GetComponent<functionLine_script>().categoryFilter.triangle = true;
			if (this.name == "figure") parent_function.GetComponent<functionLine_script>().categoryFilter.figure = true;
		}
		else
		{
			if (this.name == "set") parent_function.GetComponent<functionLine_script>().categoryFilter.set = false;
			if (this.name == "circle") parent_function.GetComponent<functionLine_script>().categoryFilter.circle = false;
			if (this.name == "box") parent_function.GetComponent<functionLine_script>().categoryFilter.box = false;
			if (this.name == "ellipse") parent_function.GetComponent<functionLine_script>().categoryFilter.ellipse = false;
			if (this.name == "arrow") parent_function.GetComponent<functionLine_script>().categoryFilter.arrow = false;
			if (this.name == "horizontal_ellipse") parent_function.GetComponent<functionLine_script>().categoryFilter.horizontal_ellipse = false;
			if (this.name == "triangle") parent_function.GetComponent<functionLine_script>().categoryFilter.triangle = false;
			if (this.name == "figure") parent_function.GetComponent<functionLine_script>().categoryFilter.figure = false;
		}

	}

	// Update is called once per frame
	void Update()
    {
        
    }

	public void updateSelectedBoolean()
	{
		functionLine_script.CategoryFilter filter = parent_function.GetComponent<functionLine_script>().categoryFilter;
		if (this.name == "set") selected = filter.set;
		else if (this.name == "circle") selected = filter.circle;
		else if (this.name == "box") selected = filter.box;
		else if (this.name == "ellipse") selected = filter.ellipse;
		else if (this.name == "arrow") selected = filter.arrow;
		else if (this.name == "horizontal_ellipse") selected = filter.horizontal_ellipse;
		else if (this.name == "triangle") selected = filter.triangle;
		else if (this.name == "figure") selected = filter.figure;
	}
}
