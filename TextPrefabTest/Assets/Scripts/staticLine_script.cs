﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class staticLine_script : MonoBehaviour
{
	// object drawing properties
	public List<Vector3> points = new List<Vector3>();
	public Vector3 centroid;
	public float maxx = -100000f, maxy = -100000f, minx = 100000f, miny = 100000f;

	// pressure based line width
	public List<float> pressureValues = new List<float>();
	public AnimationCurve widthcurve = new AnimationCurve();
	float totalLength = 0f;
	List<float> cumulativeLength = new List<float>();
	int movingWindow = 3;

	public void computeCentroid()
	{
		float totalx = 0, totaly = 0, totalz = 0;
		for (int i = 0; i < points.Count; i++)
		{
			totalx += points[i].x;
			totaly += points[i].y;
			totalz += points[i].z;
		}
		centroid = new Vector3(totalx / points.Count, totaly / points.Count, points[0].z); // including z in the average calc. created problem
																						   // so just used a constant value from the points list.
	}

	public void computeBounds()
	{
		maxx = -100000f; maxy = -100000f; minx = 100000f; miny = 100000f;

		for (int i = 0; i < points.Count; i++)
		{
			if (maxx < points[i].x) maxx = points[i].x;
			if (maxy < points[i].y) maxy = points[i].y;
			if (minx > points[i].x) minx = points[i].x;
			if (miny > points[i].y) miny = points[i].y;
		}
	}

	public void fromGlobalToLocalPoints()
	{
		// transform all points from world to local coordinate with respect to the transform position of the set game object
		for (int i = 0; i < points.Count; i++)
		{
			points[i] = transform.InverseTransformPoint(points[i]);
		}
	}

	public void updateLengthFromPoints()
	{
		totalLength = 0f;
		cumulativeLength.Clear();
		for (int i = 1; i < points.Count; i++)
		{
			totalLength += Vector3.Distance(points[i - 1], points[i]);
			cumulativeLength.Add(totalLength);
		}
	}

	public void addPressureValue(float val)
	{
		pressureValues.Add(val);
	}

	public void reNormalizeCurveWidth()
	{
		// create a curve with as many points as the current number of pressure values
		int numPts = cumulativeLength.Count;
		widthcurve = new AnimationCurve();

		if (numPts > movingWindow)
		{
			List<float> smoothedPressureValues = smoothenList(pressureValues);

			for (int i = 0; i < numPts - movingWindow; i++)
			{
				widthcurve.AddKey(cumulativeLength[i] / totalLength, Mathf.Clamp(pressureValues[i], 0f, 1f));
			}
		}
	}

	public List<float> smoothenList(List<float> values)
	{
		// take the width curve and run a moving average operation
		return Enumerable
				.Range(0, values.Count - movingWindow)
				.Select(n => values.Skip(n).Take(movingWindow).Average())
				.ToList();
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
