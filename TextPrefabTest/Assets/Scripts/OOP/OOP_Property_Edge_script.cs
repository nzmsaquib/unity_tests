﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Michsky.UI.ModernUIPack;

public class OOP_Property_Edge_script : MonoBehaviour
{

	public GameObject source_object, objectToCopy, propertyTargetSet;

	public int count = 0;

	public bool showPropertyEdge = true;
	public bool doNotUpdate = false;

	// global stroke details
	public GameObject details_dropdown;
	public bool global_details_on_edge = true;

	private void Start()
	{
		objectToCopy = Resources.Load<GameObject>("Prefabs/penLine_timer_default");
		objectToCopy.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = Resources.Load<Mesh>("Models/TestPen");
		objectToCopy.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = Color.grey;

		// fix mesh offset manually. Other methods to fix the offset failed.
		objectToCopy.transform.GetChild(0).transform.localPosition = new Vector3(-17, 25, 40.5f);

		objectToCopy.transform.position = propertyTargetSet.transform.position - new Vector3(0, -10, 0);

		// Line renderer
		transform.GetComponent<LineRenderer>().positionCount = 2;
	}

	// Update is called once per frame
	void Update()
    {
		if (doNotUpdate) return;    // used during save operations, to halt creating elements in the property set.

		OnAbstractionLayerChange(); // control the visibility of the edge based on source and target visibility

		// update global stroke details
		applyGlobalStrokeDetails();

		if (source_object != null && propertyTargetSet != null)
		{
			// if count doesn't match the source object's count, update the number of elements in this set
			int source_count = 0;
			count = propertyTargetSet.transform.childCount - 2;

			if (source_object.tag == "set")
			{
				source_count = (int)source_object.GetComponent<setLine_script>().current_feature;
			}
			else if (source_object.tag == "function")
			{
				source_count = source_object.transform.childCount - 2;
			}

			if (count != source_count)
			{
				// if a set, then clean up all member penlines and instantiate new copies from the source
				if (source_object.tag == "set")
				{
					// clean up
					for (int i = 2; i < propertyTargetSet.transform.childCount; i++)
					{
						Destroy(propertyTargetSet.transform.GetChild(i).gameObject);
					}
					// instantiate
					for (int i = 2; i < source_object.transform.childCount; i++)
					{
						Vector3 offset = source_object.transform.GetChild(i).position - source_object.transform.position;
						GameObject temp = Instantiate(source_object.transform.GetChild(i).gameObject);
						temp.transform.position = propertyTargetSet.transform.position + offset;
						temp.transform.SetParent(propertyTargetSet.transform);
						if (propertyTargetSet.GetComponent<setLine_script>().abstraction_layer != 1)
						{
							// raise abs. layer
							temp.GetComponent<penLine_script>().parent_asked_to_change_layer = true;
						}
					}
				}

				// if a function, just make dummy pen objects
				else if (source_object.tag == "function")
				{
					if (count > source_count)
					{
						// source_count number of objects should stay
						for (int i = source_count + 2; i < propertyTargetSet.transform.childCount; i++)
						//while (propertyTargetSet.transform.childCount > (count - source_count + 2))
						{
							Destroy(propertyTargetSet.transform.GetChild(i).gameObject);
						}
						//count = source_count;
					}
					else if (count < source_count)
					{
						for (int i = 0; i < source_count - count; i++)
						{
							GameObject dummyobj = createPenObjectAtRandomPosition(objectToCopy, 5f);
							dummyobj.GetComponent<penLine_script>().parent_asked_to_change_layer = true;

							/*
							// if it's number of elements for a function, turn the gestureTemplate to "";
							if (source_object.tag == "function")
							{
								dummyobj.GetComponent<penLine_script>().gestureTemplate = "";
							}
							*/
						}
					}
				}
			}

			// Update positions and render
			if (showPropertyEdge)
			{
				Vector3 start = source_object.transform.GetChild(0).position + new Vector3(0, 0, 100);
				Vector3 end = propertyTargetSet.transform.GetChild(0).position + new Vector3(0, 0, 100);
				transform.GetComponent<LineRenderer>().SetPosition(0, start);
				transform.GetComponent<LineRenderer>().SetPosition(1, end);

				// update Edge Collider
				List<Vector2> edgepts = new List<Vector2> { start, end };
				transform.GetComponent<EdgeCollider2D>().points = edgepts.Select(x =>
				{
					return (Vector2)transform.GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
					//return new Vector2(pos.x, pos.y);
				}).ToArray();
			}
		}
		else
		{
			// source_object does not exist anymore, destroy the edge and the property set
			if (propertyTargetSet != null)
			{
				Destroy(propertyTargetSet.gameObject);
			}
			// destroy property edge
			Destroy(this.gameObject);
		}
    }

	private void Awake()
	{
		details_dropdown = GameObject.Find("Details_Dropdown");
	}

	// call this when property source is a set
	public void copyObjectsFromPropertySource()
	{

	}

	public void applyGlobalStrokeDetails()
	{
		// wait till the property edge is created
		if (source_object != null && propertyTargetSet != null)
		{
			bool state = details_dropdown.GetComponent<DropdownMultiSelect>().transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(5).GetComponent<Toggle>().isOn;

			if (state != global_details_on_edge)
			{
				// change needed
				if (!state)
				{
					showPropertyEdge = false;
					global_details_on_edge = false;
					transform.GetComponent<LineRenderer>().enabled = false;
				}
				else if (state)
				{
					showPropertyEdge = true;
					global_details_on_edge = true;
					transform.GetComponent<LineRenderer>().enabled = true;
				}
			}
		}
	}

	public void OnAbstractionLayerChange()
	{
		if (source_object != null && propertyTargetSet != null && global_details_on_edge == true)
		{
			if (source_object.tag == "set")
			{
				if (source_object.GetComponent<setLine_script>().abstraction_layer != 1)
				{
					showPropertyEdge = false;
					transform.GetComponent<LineRenderer>().enabled = false;
				}
				else
				{
					showPropertyEdge = true;
					transform.GetComponent<LineRenderer>().enabled = true;
				}
			}
			else if (source_object.tag == "function")
			{
				if (source_object.GetComponent<functionLine_script>().abstraction_layer != 1)
				{
					showPropertyEdge = false;
					transform.GetComponent<LineRenderer>().enabled = false;
				}
				else
				{
					showPropertyEdge = true;
					transform.GetComponent<LineRenderer>().enabled = true;
				}
			}
		}
	}

	public GameObject createPenObjectAtRandomPosition(GameObject penobj, float radius)
	{
		return Instantiate(penobj,
				propertyTargetSet.transform.position + new Vector3(
					UnityEngine.Random.Range(-radius, radius),
					UnityEngine.Random.Range(-radius, radius),
					-40f),
				Quaternion.identity, propertyTargetSet.transform);
	}
}
