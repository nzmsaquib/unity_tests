﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreatePropertyEdge : MonoBehaviour
{

	public bool start_copying = false;
	public GameObject toCopy;
	public List<Vector3> copy_path = new List<Vector3>();
	public float distance_threshold = 55f;

	public Vector3 propertyEdgePos;

	public GameObject source_object;

	public GameObject PropertyEdgePrefab;

	public GameObject paint_canvas;

	Pen currentPen;

	private void Awake()
	{
		//paint_canvas = GameObject.FindGameObjectWithTag("paintable_canvas_object");
	}

	public void onClick()
	{
		// Instantiate a set and a property edge
		if (source_object != null)
		{
			GameObject propertyset = null;

			if (source_object.tag == "set")
			{
				// create property set
				List<Vector3> pts = source_object.GetComponent<setLine_script>().fromLocalToGlobalPoints();
				propertyset = this.transform.GetComponent<CreatePrimitives>().CreateSet(pts);
				propertyset.transform.position = transform.position + new Vector3(-60, 0, 0);
				propertyset.transform.SetParent(paint_canvas.transform);
				propertyset.GetComponent<setLine_script>().is_this_an_oop_property = true;
				// set to symbolic layer
				propertyset.GetComponent<setLine_script>().retained_slider_value = 3;
			}
			else if (source_object.tag == "function")
			{
				// create property set
				List<Vector3> pts = source_object.GetComponent<functionLine_script>().fromLocalToGlobalPoints();
				propertyset = this.transform.GetComponent<CreatePrimitives>().CreateSet(pts);
				propertyset.transform.position = transform.position + new Vector3(0, -10, 0);
				propertyset.transform.SetParent(paint_canvas.transform);
				propertyset.GetComponent<setLine_script>().is_this_an_oop_property = true;
				// set to symbolic layer
				propertyset.GetComponent<setLine_script>().retained_slider_value = 3;
			}

			// create property edge
			GameObject propertyedge = Instantiate(PropertyEdgePrefab, propertyEdgePos, Quaternion.identity, paint_canvas.transform);
			propertyedge.GetComponent<OOP_Property_Edge_script>().source_object = source_object;
			propertyedge.GetComponent<OOP_Property_Edge_script>().propertyTargetSet = propertyset;
			propertyedge.name = "PropertyEdge_" + (++paint_canvas.GetComponent<Paintable_Script>().totalLines).ToString();
			propertyedge.tag = "oop_property_edge";
		}
	}

	
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

	}
}
