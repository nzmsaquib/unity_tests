﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;
using Symbolism;
//using TexDrawLib;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;

public class setLine_script : MonoBehaviour
{
	// actionable menu properties
	public enum Properties
	{
		count,	// int, number of elements present in the container/set
		capacity,	// how many elements can it hold
		isEmpty,	// bool returns if it contains a particular "type" of element
		name,	// string
		element	// return an element (index can be typed in a text box)
	};

	public Properties current_property = Properties.count;

	// programmable set/get properties and menu-selected properties
	public struct Features
	{
		public string name;
		public bool locked;
		public int count;
		public int capacity;
		public bool isEmpty;
		public GameObject element;
	};

	public Features features;

	public struct Visibility
	{
		public bool mesh, anchor, argument;
	};

	public Visibility visibility;

	// current feature and capacity
	public dynamic current_feature;
	public int capacity = 5;

	// is this a general set, or an OOP property set?
	public bool is_this_an_oop_property = false;

	// timer set related variables
	public bool timer_activated = false;
	public bool halt_timer_update_while_saving = false;
	public int timer_min, timer_max;
	public float timer_dt, stopwatch, last_stopwatch_mark;
	public GameObject set_timer_menu_prefab, set_timer_menu;
	public bool timer_menu_already_exists = false;
	public GameObject objectToCopy;
	public GameObject timer_object_edgeline = null;

	// OOP related variables
	public GameObject OOP_properties_submenu_prefab, oop_properties_submenu = null;

	// Category Filter variables
	public GameObject category_filter_submenu_prefab, category_filter_submenu = null;
	public struct CategoryFilter
	{
		public bool set, circle, box, ellipse, arrow, horizontal_ellipse, triangle, figure;
	};
	public CategoryFilter categoryFilter;

	// naming
	public string this_set_name = "x";
	public MathObject symbol_name;
	public MathObject gestureTEX1, gestureTEX2; // two layers of legibility that'll be used by parent function
	public Dictionary<string, int> gestureDict = new Dictionary<string, int>();

	public List<Vector3> points = new List<Vector3>();
	public Vector3 centroid;
	public Vector3 anchor_offset_from_center;
	public float maxx = -100000f, maxy = -100000f, minx = 100000f, miny = 100000f;

	public bool draggable_now = false;
	public int relayCount = 0;

	public Material set_line_material;

	public GameObject pan_button;

	public GameObject paintable_object;

	// feature list -- use a single variable for now
	public int total_count = 0;

	// abstraction layer related variables
	public int abstraction_layer = 1;
	public int prev_abstraction_layer = 1;
	public int retained_slider_value = 1;
	public Slider abs_slider;

	// parent's command for abstraction layer
	public bool parent_asked_to_lower_layer = false;

	// menu related variables
	public string menu_name = "";

	// other interaction variables
	private Vector3 touchDelta = new Vector3();

	// legible layer related variables
	public string gestureType = "", previous_gestureType = "";
	public bool updateGestureType = false;
	public Image gesture;

	// double function related variables
	public bool is_this_double_function_operand = false;
	public int double_function_operand_order = 0;
	string order_string = "";

	// play log related variables
	public Dictionary<string, Vector2> saved_positions = new Dictionary<string, Vector2>();
	public Dictionary<string, int> abstraction_log = new Dictionary<string, int>();

	// sprite related variables
	public string sprite_filename = "";

	// Pen Interaction
	Pen currentPen;

	// Action History
	public Sprite prev_snapshot, current_snapshot;
	public int prev_child_count, current_child_count;
	public List<GameObject> ChildrenHistory = new List<GameObject>();
	public List<GameObject> prevChildrenHistory = new List<GameObject>();

	// global stroke details
	public GameObject details_dropdown;
	public bool global_details_on_anchor = true;
	public bool global_details_on_mesh = true;

	public void computeCentroid()
	{
		float totalx = 0, totaly = 0, totalz = 0;
		for (int i = 0; i < points.Count; i++)
		{
			totalx += points[i].x;
			totaly += points[i].y;
			totalz += points[i].z;
		}
		centroid = new Vector3(totalx / points.Count, totaly / points.Count, totalz / points.Count);
	}

	public void computeBounds()
	{
		maxx = -100000f; maxy = -100000f; minx = 100000f; miny = 100000f;

		for (int i = 0; i < points.Count; i++)
		{
			if (maxx < points[i].x) maxx = points[i].x;
			if (maxy < points[i].y) maxy = points[i].y;
			if (minx > points[i].x) minx = points[i].x;
			if (miny > points[i].y) miny = points[i].y;
		}
	}

	public void calculateAnchorOffset() {
		anchor_offset_from_center = transform.GetChild(0).position - new Vector3(centroid.x, centroid.y, points[0].z);
		anchor_offset_from_center.z = 0;
	}

	public void fromGlobalToLocalPoints()
	{
		// transform all points from world to local coordinate with respect to the transform position of the set game object
		for (int i = 0; i < points.Count; i++)
		{
			points[i] = transform.InverseTransformPoint(points[i]);
		}
	}

	public List<Vector3> fromLocalToGlobalPoints()
	{
		// Assumes fromGlobalToLocalPoints() has already been called on points set
		List<Vector3> gbpoints = new List<Vector3>();

		for (int i = 0; i < points.Count; i++)
		{
			gbpoints.Add(transform.TransformPoint(points[i]));
		}

		return gbpoints;
	}

	public bool isInsidePolygon(Vector3 p)
	{
		int j = points.Count - 1;
		bool inside = false;
		for (int i = 0; i < points.Count; j = i++)
		{
			if (((points[i].y <= p.y && p.y < points[j].y) || (points[j].y <= p.y && p.y < points[i].y)) &&
			   (p.x < (points[j].x - points[i].x) * (p.y - points[i].y) / (points[j].y - points[i].y) + points[i].x))
				inside = !inside;
		}
		return inside;
	}

	// TODO: NEED A WAY TO CHECK PARENTHOOD OF A SET THAT ALIGNS WITH THE VISUALLY UNDERSTANDABLE HIERARCHY.
	// A SET CAN BE A MEMBER OF ANOTHER SET. IF THAT PARENT SET IS CONTAINED WITHIN A FUNCTION,
	// THE CHILD SET SHOULD STILL BELONG TO THE PARENT SET, NOT THE FUNCTION.
	public void checkHitAndMove()
	{
		// assumes a touch has registered and pan mode is selected
		if (Paintable_Script.pan_button.GetComponent<AllButtonsBehavior>().selected == true)
		{
			//currentPen = Pen.current;

			if (PenTouchInfo.PressedThisFrame)//currentPen.tip.wasPressedThisFrame)//(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
			{

				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				// check for a hit on the anchor object
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == transform.GetChild(0).gameObject)
				{

					draggable_now = true;

					Vector3 vec = Hit.point; //+ new Vector3(0, 0, -40); // Vector3.up * 0.1f;
											 //Debug.Log(vec);

					// enforce the same z coordinate as the rest of the points in the parent set object
					vec.z = -40f;

					touchDelta = transform.GetChild(0).position - vec;

					// change anchor color
					transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.gray;
				}
			}

			if (PenTouchInfo.PressedNow)//currentPen.tip.isPressed)//(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				//Debug.Log("anchor touched");

				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				// check for a hit on the anchor object
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == transform.GetChild(0).gameObject)
				{
					//Debug.Log(transform.name);

					draggable_now = true;

					Vector3 vec = Hit.point; //+ new Vector3(0, 0, -40); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					// enforce the same z coordinate as the rest of the points in the parent set object
					vec.z = -40f;

					Vector3 diff = vec - transform.GetChild(0).position + touchDelta;
					diff.z = 0;

					// update the set position. 
					transform.position += diff;

					// update the menu position if a menu is currently open/created
					GameObject menu_obj = GameObject.Find(menu_name);
					if (menu_obj != null)
					{
						menu_obj.transform.position = vec;
					}

					// if there are pen objects in the immediate hierarchy, and they have a path defined and abs. layer = 1, 
					// then update the red path to move with the set hierarchy
					for (int i = 2; i < transform.childCount; i++)
					{
						if (transform.GetChild(i).tag == "penline" &&
							transform.GetChild(i).GetComponent<penLine_script>().abstraction_layer == 1)
						{
							if (transform.GetChild(i).GetComponent<penLine_script>().translation_path.Count > 0)
								transform.GetChild(i).GetComponent<penLine_script>().calculateTranslationPathIfAlreadyDefined();
						}
					}

					// experimental: update parent membership as dragged
					checkAndUpdateMembership();

				}
			}

			// make sure touch ended while dragging the current anchor (i.e., draggable_now == true)
			else if (PenTouchInfo.ReleasedThisFrame && draggable_now == true) //(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && draggable_now == true)
			{
				draggable_now = false;

				touchDelta = new Vector3(); // reset touchDelta

				// change anchor color back
				transform.GetChild(0).GetComponent<MeshRenderer>().material.color = new Color(0.0838f, 0.0937f, 0.6132f, 1f);

				// TODO: as an added safety measure, we could use a raycast at the touch position to see if it hits the set anchor

				// Check and update membership
				checkAndUpdateMembership();
			}
		}
	}

	public void checkAndUpdateMembership()
	{
		// check if the set came out of any parent set's or function's area, and went into another's
		GameObject[] sets = GameObject.FindGameObjectsWithTag("set");
		GameObject[] functions = GameObject.FindGameObjectsWithTag("function");
		bool in_canvas = false;

		for (int i = 0; i < sets.Length; i++)
		{
			// is anchor ( child(0) ) inside the polygon?

			// TODO: CHECK IF ABSTRACTION LAYER IS HIGHER THAN 1, THEN DON'T CHECK FOR MEMBERSHIP
			// ^ THIS CREATES TROUBLE: ALL MEMBERS LOSE MEMBERSHIP AND CAN'T COME BACK TO VISIBLE FORM, NEED TO CHECK FURTHER
			// STATUS: SOLVED

			if (
				//(sets[i].transform.parent.tag != "set" && sets[i].transform.parent.tag != "function")
				//&&
				sets[i].GetComponent<setLine_script>().isInsidePolygon(
				sets[i].GetComponent<setLine_script>().transform.InverseTransformPoint(
				transform.GetChild(0).position))
				&&
				(sets[i].GetComponent<setLine_script>().abstraction_layer == 1 ||
				sets[i].GetComponent<setLine_script>().abstraction_layer == 2)
				)
			{
				transform.parent = sets[i].transform;
				in_canvas = true;

				// save log
				// save coordinates wrt parent center
				if (abstraction_layer == 1)
				{
					string tkey = sets[i].name + "|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
					if (!saved_positions.ContainsKey(tkey))
						saved_positions.Add(tkey, (Vector2)(transform.position - sets[i].transform.position));
				}

				break;
			}
		}

		// if it didn't end up in a set, check whether it went into any function
		if (!in_canvas)
		{
			for (int i = 0; i < functions.Length; i++)
			{
				// is anchor ( child(0) ) inside the polygon?

				// also check for category filter

				if (functions[i].GetComponent<functionLine_script>().isInsidePolygon(
					functions[i].GetComponent<functionLine_script>().transform.InverseTransformPoint(
					transform.GetChild(0).position))
					&&
					(functions[i].GetComponent<functionLine_script>().abstraction_layer == 1 ||
					functions[i].GetComponent<functionLine_script>().abstraction_layer == 2)
					&&
					functions[i].GetComponent<functionLine_script>().CheckCategoryFilterCriteria(this.gameObject)
					)
				{
					transform.parent = functions[i].transform;
					in_canvas = true;

					// save log
					// save coordinates wrt parent center
					if (abstraction_layer == 1)
					{
						string tkey = functions[i].name + "|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
						if (!saved_positions.ContainsKey(tkey))
							saved_positions.Add(tkey, transform.position - functions[i].transform.position);
					}

					break;
				}
			}
		}

		// if none of the sets or functions contain it, then it should be a child of the paintable canvas
		if (!in_canvas)
		{
			transform.parent = paintable_object.transform;
			// in case it was invisible, make it visible
			parent_asked_to_lower_layer = false;

			// save log
			string tkey = "paintable|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
			if (!saved_positions.ContainsKey(tkey))
				saved_positions.Add(tkey, transform.position);
		}
	}

	public void menuButtonClick(GameObject radmenu, Button btn, int buttonNumber)
	{
		if (buttonNumber == 1)
		{
			btn.onClick.AddListener(() => combineChildrenMeshes(radmenu));
		}
		else if (buttonNumber == 2)
		{
			btn.onClick.AddListener(() => deleteSet(radmenu));
		}
		else if (buttonNumber == 3)
		{
			btn.onClick.AddListener(() => exitMenu(radmenu));
		}
		else if (buttonNumber == 4)
		{
			btn.onClick.AddListener(() => createOOPmenu(radmenu));
		}
		else if (buttonNumber == 5)
		{
			btn.onClick.AddListener(() => createTimerMenu(radmenu));
		}
		else if (buttonNumber == 6)
		{
			btn.onClick.AddListener(() => flattenSet(radmenu));
		}
	}

	public void setPropertyAsCount(GameObject radmenu)
	{
		current_property = Properties.count;
		// Destroy the radial menu
		//Destroy(radmenu);
	}

	public void setPropertyAsCapacity(GameObject radmenu)
	{
		current_property = Properties.capacity;
		// Destroy the radial menu
		//Destroy(radmenu);
	}

	public void setPropertyAsName(GameObject radmenu)
	{
		current_property = Properties.name;
		// Destroy the radial menu
		//Destroy(radmenu);
	}

	
	public void combineChildrenMeshes(GameObject radmenu)
	{
		List<GameObject> pen_objs = new List<GameObject>();

		for (int k = 2; k < transform.childCount; k++)
		{
			if (transform.GetChild(k).tag == "penline")
			{
				pen_objs.Add(transform.GetChild(k).gameObject);
			}
		}

		// stop if no penline mesh objs found
		if (pen_objs.Count == 0) return;

		GameObject templine = transform.GetComponent<CreatePrimitives>().CreatePenLine(pen_objs.ToArray());

		// change the parent for the new pen, as it immediately checks for membership after creation, and gets destroyed with the current set.
		templine.transform.SetParent(paintable_object.transform);

		// Destroy this menu
		Destroy(radmenu);

		// Destroy this set
		Destroy(this.gameObject);

		// set back to pan mode
		pan_button.GetComponent<AllButtonsBehavior>().whenSelected();

	}

	public void exitMenu(GameObject radmenu)
	{
		menu_name = "";

		// set the label/name of the set
		this_set_name = radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().text;
		symbol_name = new Symbol(this_set_name);
		transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = this_set_name;

		// retain the abstraction slider value
		retained_slider_value = (int)abs_slider.value;

		// CREATE NEW OBJECTS OR DELETE OBJECTS, BASED ON THE COUNT TEXT BOX, WHEN TIMER IS NOT ACTIVATED
		if (!timer_activated)
		{
			// if the count on the count text box doesn't match current number of objects, then create new objects
			int cnt = Convert.ToUInt16(radmenu.transform.Find("_count_textbox").GetComponent<TMP_InputField>().text);
			//int ctnow = transform.childCount - 2;
			int ctnow = CountAllIconicChildren();
			if (cnt != CountAllIconicChildren())
			{
				if (transform.childCount > 2 && transform.GetChild(2).tag == "penline")
				{
					// pick the first child to copy
					fillSetAtRandomPositions(transform.GetChild(2).gameObject, cnt, 10f);
					// now delete the number of children that's required to equal the count
					deleteChildren(ctnow);
				}
				else
				{
					// create a dummy object
					GameObject dummy = Resources.Load<GameObject>("Prefabs/penLine_timer_default");
					dummy.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = Resources.Load<Mesh>("Models/TestPen");
					dummy.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = Color.black;

					// fix mesh offset manually. Other methods to fix the offset failed.
					dummy.transform.GetChild(0).transform.localPosition = new Vector3(-17, 25, 40.5f);

					// fill up array
					fillSetAtRandomPositions(dummy, cnt, 10f);
					// delete required number of children
					deleteChildren(ctnow);
				}

			}
		}

		// Destroy the radial menu
		Destroy(radmenu);
	}

	public void deleteSet(GameObject radmenu)
	{
		menu_name = "";

		// retain the abstraction slider value
		retained_slider_value = (int)abs_slider.value;

		// Destroy the radial menu
		Destroy(radmenu);
		// Destroy the set attached to this script
		Destroy(this.gameObject);
	}

	public void copySet(GameObject radmenu)
	{
		// create one copy of the pen object if only a single click was registered on this menu button
		Instantiate(this.gameObject, radmenu.transform.Find("copy").transform.position + new Vector3(0, 20, 0), Quaternion.identity, paintable_object.transform);

		// TODO (MAYBE): RENAME THE SET WITH A UNIQUE NAME. BUT CURRENTLY, NO PROCESS SEEMS TO RELY ON UNIQUE NAME

		// retain the abstraction slider value
		retained_slider_value = (int)abs_slider.value;

		// Destroy the radial menu
		Destroy(radmenu);
	}

	public void createOOPmenu(GameObject radmenu)
	{

		if (oop_properties_submenu == null)
		{
			oop_properties_submenu = Instantiate(OOP_properties_submenu_prefab,
				radmenu.transform.Find("oop_properties").transform.position + new Vector3(-65, 0, 0),
				Quaternion.identity, radmenu.transform);

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().source_object = this.gameObject;

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().paint_canvas = paintable_object;

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().propertyEdgePos = 
				oop_properties_submenu.transform.position + new Vector3(-65, 0, 0);

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<Button>().onClick.AddListener(
				() => oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().onClick());
		}
		else
		{
			Destroy(oop_properties_submenu);
		}
	}

	public void createTimerMenu(GameObject radmenu)
	{
		if (set_timer_menu == null)
		{
			set_timer_menu = Instantiate(set_timer_menu_prefab, 
				radmenu.transform.Find("timer").transform.position + new Vector3(-65, 0, 0), 
				Quaternion.identity, radmenu.transform);

			// set the submit button's onClick
			set_timer_menu.transform.Find("_submit_button").GetComponent<Button>().onClick.AddListener(() => submitTimer(set_timer_menu));

			// if timer is already created before, then copy the text from previous values
			if (timer_activated)
			{
				set_timer_menu.transform.Find("_maximum_input").GetComponent<TMP_InputField>().text = timer_max.ToString();
				set_timer_menu.transform.Find("_minimum_input").GetComponent<TMP_InputField>().text = timer_min.ToString();
				set_timer_menu.transform.Find("_dt_input").GetComponent<TMP_InputField>().text = timer_dt.ToString();
			}
		}
		else
		{
			// close the submenu
			Destroy(set_timer_menu);
		}
	}

	public void flattenSet(GameObject radmenu)
	{
		// Go through all children and extract all pen objects. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "set")
			{
				// "recursive" call to flatten the children set
				transform.GetChild(i).GetComponent<setLine_script>().flattenSet(null);
			}
		}

		// Then destroy the current set and menu
		if(radmenu != null) Destroy(radmenu);	// check null as a parent set might pass a null menu
		Destroy(this.gameObject);
	}

	public void submitTimer(GameObject timer_menu)
	{
		// if timer is already active, then start fresh, clean up everything first except the first element
		if (timer_activated && transform.childCount > 3)
		{
			for (int i = 3; i < transform.childCount; i++)
			{
				// destroy the gameobject
				Destroy(transform.GetChild(i).gameObject);
			}
			// reset the edge var
			if (timer_object_edgeline != null) timer_object_edgeline = null;
		}

		// Main set up.
		// the submit button was clicked, reset this set and set it up for timer
		timer_activated = true;

		// get the parameters from the text boxes
		if (!int.TryParse(timer_menu.transform.Find("_maximum_input").GetComponent<TMP_InputField>().text, out timer_max))
			timer_activated = false;
		if (!int.TryParse(timer_menu.transform.Find("_minimum_input").GetComponent<TMP_InputField>().text, out timer_min))
			timer_activated = false;
		if (!float.TryParse(timer_menu.transform.Find("_dt_input").GetComponent<TMP_InputField>().text, out timer_dt))
			timer_activated = false;

		if (timer_max < timer_min) timer_activated = false;
		if (timer_max < 1 || timer_min < 0 || timer_dt <= 0) timer_activated = false;
		if (timer_min == 0) timer_min = 1;	// timer_min can't be 0.

		if (timer_activated)
		{

			// is there a first (or at least one) element? If yes, then make it the object to be copied.
			if (transform.childCount > 2)
			{
				objectToCopy = transform.GetChild(2).gameObject;
				objectToCopy.transform.position = transform.position - new Vector3(0, -10, 0);
			}
			else
			{
				// use a default prefab otherwise.
				objectToCopy = Resources.Load<GameObject>("Prefabs/penLine_timer_default");
				objectToCopy.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = Resources.Load<Mesh>("Models/TestPen");
				objectToCopy.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = Color.grey;

				// fix mesh offset manually. Other methods to fix the offset failed.
				objectToCopy.transform.GetChild(0).transform.localPosition = new Vector3(-17, 25, 40.5f);

				objectToCopy.transform.position = transform.position - new Vector3(0, -10, 0);
			}

			// we need at least the timer_min number of elements, so instantiate that many times
			for (int i = 1; i < timer_min; i++)
			{
				// create a new object
				//GameObject newobj = Instantiate(objectToCopy, transform.position - new Vector3(0, -10, 0),
				//Quaternion.identity, transform);

				GameObject newobj = createPenObjectAtRandomPosition(objectToCopy, 10f);

				// are there edges associated with objectToCopy? Copy those too. -- creates weird behavior. Shelve this idea for now.
				GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
				for (int s = 0; s < edges.Length; s++)
				{
					if (edges[s].GetComponent<edgeLine_script>().target_object == objectToCopy)
					{
						
						newobj.GetComponent<penLine_script>().calculateTranslationPath();

						GameObject edge = Instantiate(edges[s], paintable_object.transform);
						edge.GetComponent<edgeLine_script>().target_object = newobj;
					}
				}
			}

			// start the stopwatch
			stopwatch = 0f;
			last_stopwatch_mark = 0f;
		}

		// destroy timer menu
		Destroy(timer_menu);
	}

	public void updateSetFromTimer()
	{
		if (timer_activated && !halt_timer_update_while_saving)
		{
			stopwatch += Time.deltaTime;
			if (stopwatch - last_stopwatch_mark >= timer_dt)
			{
				last_stopwatch_mark = stopwatch;
				int current_num_obj = transform.childCount - 2;

				if (current_num_obj < timer_max)
				{
					// create a new object
					//GameObject newobj = Instantiate(objectToCopy, objectToCopy.transform.position,
					//Quaternion.identity, transform);

					GameObject newobj = createPenObjectAtRandomPosition(objectToCopy, 10f);
					// copy the edgeline, if any. -- not doing it right now.

					GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
					for (int s = 0; s < edges.Length; s++)
					{
						if (edges[s].GetComponent<edgeLine_script>().target_object == objectToCopy)
						{

							newobj.GetComponent<penLine_script>().calculateTranslationPath();

							GameObject edge = Instantiate(edges[s], paintable_object.transform);
							edge.GetComponent<edgeLine_script>().target_object = newobj;
						}
					}

				}
				else
				{
					// reset
					// timer_min number of objects should stay.
					for (int i = timer_min + 2; i < timer_max + 2; i++)
					{
						// find and destroy any edge associated with this game object -- not doing it right now.
						GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
						for (int s = edges.Length - 1; s > -1; s--)
						{
							if (edges[s].GetComponent<edgeLine_script>().target_object == transform.GetChild(i).gameObject)
							{
								Destroy(edges[s]);
							}
						}

						// destroy the gameobject
						Destroy(transform.GetChild(i).gameObject);
					}

					// reset watch
					stopwatch = 0f;
					last_stopwatch_mark = 0f;
				}
			}
		}
	}

	public void onAbstractionLayerChange()
	{
		/*
		 * Set abstraction layers:
		 * 0: turn off everything
		 * 1: everything in this set is visible except legible layer, children are also visible
		 * 2: turn off drawn objects and other sets, display legible layer (TeXDraw text inside the lasso)
		 * 3: turn off drawn objects + legible layer, display symbol with value
		 * 
		 * Slider can have value between [1, 3], abs. layer = 0 can only be set and revoked by parent
		*/

		// first check if parent asked to change the layer, and override layering by abstraction slider if that's the case
		if (parent_asked_to_lower_layer) abstraction_layer = 0;
		// otherwise, if a menu exists, then check the abstraction slider
		else if (abs_slider != null) abstraction_layer = (int)abs_slider.value;
		// if parent is not forcing abstraction or there is no slider (from a set's menu), then use retained slider value from before
		else abstraction_layer = retained_slider_value;

		// If there was a change, then act. Otherwise no point updating every frame.
		if (abstraction_layer - prev_abstraction_layer != 0)
		//if (abstraction_layer > -1 && transform.childCount > 2)	// if the set has both anchor and meshobj (i.e. set is finished forming).
		{
			// 0, turn off everything
			if (abstraction_layer == 0)
			{
				// turn off _anchor, _text, _TeX, and _meshobj mesh renderers
				// _anchor -- all of it
				transform.GetChild(0).gameObject.SetActive(false);
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;    // sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj, turn them off 
				for (int i = 2; i < transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = true;
					}
					else if (transform.GetChild(i).tag == "set")
					{
						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = true;
					}
				}
			}

			// abstraction layer 1: everything is visible, except legible layer (TeXDraw textbox)
			else if (abstraction_layer == 1)
			{
				// _anchor
				transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
				// argument_label
				transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
				// _anchor text
				transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().enabled = true;
				// _TeX
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled = false;
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = true;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;    // sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj 
				for (int i = 2; i < transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = false;
					}
					else if (transform.GetChild(i).tag == "set")
					{
						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = false;
					}
				}

			}

			// abstraction layer 2: turn off drawn objects, display legible layer (TeXDraw text inside the lasso)
			else if (abstraction_layer == 2)
			{
				// _anchor
				transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
				// argument_label
				transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
				// _anchor text
				transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().enabled = true;
				// _TeX
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled = true;
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if(transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = true;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;	// sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj, turn them off 
				for (int i = 2; i < this.gameObject.transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = true;
					}

					else if (transform.GetChild(i).tag == "set")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = true;
					}
				}
			}

			// abstraction layer 3: turn off drawn objects + legible layer, display symbol with value

			// _meshobj of set objects, and _meshobj every drawn object inside
			else if (abstraction_layer == 3)
			{
				// _anchor
				transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
				// argument_label
				transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
				// _anchor text
				transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().enabled = true;
				// _TeX
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled = false;
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;    // sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj 
				for (int i = 2; i < this.gameObject.transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = true;
					}
					
					else if (transform.GetChild(i).tag == "set")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = true;
					}

				}
			}

			prev_abstraction_layer = abstraction_layer;

			// save to log
			abstraction_log.Add(transform.name + "|" +
							DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"), abstraction_layer);
		}
	}

	public void updateFeature()
	{
		// update count feature
		//current_feature = this.gameObject.transform.childCount - 2; // discount _anchor and _meshobj
		current_feature = CountAllIconicChildren();

		//this.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = total_count.ToString();

		// double function operand order
		if (is_this_double_function_operand) order_string = "<sup>" + double_function_operand_order.ToString() + "</sup>";
		else order_string = "";

		// update the text on the anchor
		if (abstraction_layer == 3)
		{
			// show symbol
			this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = order_string + this_set_name + " = " + current_feature.ToString();
		}
		else if(abstraction_layer == 2)
		{
			// show value of the selected property
			this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = order_string + this_set_name + " = " +  current_feature.ToString();
		}
		else if (abstraction_layer == 1)
		{
			// show value of the selected property
			this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = order_string + current_feature.ToString();
		}
	}

	public void updateLegibleLayer()
	{
		// assume there's only one type of object ("gesture type") in a set, so pick the first element
		// The text length (according to current recttransform size measurement) should be around: "TEXDraw blah" in each line.
		// Otherwise, add a new line and continue from there.

		// Update legible layer gesture type assignment here. Since it's called in Update(), we shouldn't update the text every frame.

		// "null" as a gesture type basically means that we should show a dot (or nothing) as the unit. \quad, \!, etc. different spacing exists
		// in latex.

		// TODO: ONLY UPDATE THE TEXT BOX WHEN A CHANGE OCCURS, NOT IN EVERY FRAME. THIS MIGHT SLOW DOWN THE FPS.

		// if abstraction layer turned it off, then don't bother updating the text
		//if (!transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled)
		//return;

		// clean up dictionary
		gestureDict.Clear();

		if (transform.childCount > 2)   // if a pen child exists
		{
			/*
			for (int i = 2; i < transform.childCount; i++)
			{
				if (transform.GetChild(i).tag == "penline") // if the first set member/child (index = 2) is a pen line
				{
					//gestureType = transform.GetChild(2).GetComponent<penLine_script>().gestureTemplate;
					if (gestureDict.ContainsKey(transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate))
					{
						gestureDict[transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate] += 1;
					}
					else
					{
						gestureDict.Add(transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate, 1);
					}
				}

				else if(transform.GetChild(i).tag == "set")  // for sets as children of this set
				{
					MergeGestureDicts(transform.GetChild(i).GetComponent<setLine_script>().gestureDict);
				}
			}
			*/

			gestureDict = CalculateGestureDict();

			updateGestureType = true;
		}
		else if(transform.childCount == 2)
		{
			//gestureType = "";

			updateGestureType = true;
		}

		if (updateGestureType) // some change occurred, update the text
		{
			//string txt = Convert.ToString(current_feature) + @" \" + gestureType;
			string txt = createLegibleStringFromDict();

			// format the text to limit each line to 10 characters, for terms after operations +, -, * etc. 
			// anchor is 0, _TeX is 1
			transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text = txt;

			// update the gesture TEX symbols, to be used in parent function's legible layers
			// convert to Int, because a set is not going to usually hold fractions, unless a function is evaluated instantly to create a set

			//gestureTEX1 = Convert.ToInt16(current_feature) * new Symbol("\\" + gestureType);
			gestureTEX1 = createLegibleMathObjectFromDict();

			//Debug.Log("changed legible layer text: " + gestureTEX2.ToString());

			updateGestureType = false;
			//gestureType = transform.GetChild(2).GetComponent<penLine_script>().gestureTemplate;
		}

	}

	public string createLegibleStringFromDict()
	{
		if (gestureDict.Count == 0)
			return "";

		string legible = "";
		List<string> keys = new List<string>(gestureDict.Keys);

		for (int i = 0; i < keys.Count - 1; i++)
		{
			legible = legible + gestureDict[keys[i]] + @" \" + keys[i] + " + ";
		}

		legible = legible + gestureDict[keys[keys.Count - 1]] + @" \" + keys[keys.Count - 1];

		return legible;
	}

	public MathObject createLegibleMathObjectFromDict()
	{

		if (gestureDict.Count == 0)
			return new Symbol("");

		List<string> keys = new List<string>(gestureDict.Keys);

		MathObject legible = gestureDict[keys[0]] * new Symbol("\\" + keys[0]);

		for (int i = 1; i < keys.Count; i++)
		{
			legible = legible + gestureDict[keys[i]] * new Symbol("\\" + keys[i]);
		}

		return legible;
	}

	public Dictionary<string, int> CalculateGestureDict()
	{
		Dictionary<string, int> dict = new Dictionary<string, int>();
		dict.Clear();

		if (transform.childCount > 2)   // if a child exists
		{
			for (int i = 2; i < transform.childCount; i++)
			{
				if (transform.GetChild(i).tag == "penline") // if the first set member/child (index = 2) is a pen line
				{
					//gestureType = transform.GetChild(2).GetComponent<penLine_script>().gestureTemplate;
					if (dict.ContainsKey(transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate))
					{
						dict[transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate] += 1;
					}
					else
					{
						dict.Add(transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate, 1);
					}
				}

				else if (transform.GetChild(i).tag == "set")  // for sets as children of this set, merge dicts
				{
					Dictionary<string, int> cdict = transform.GetChild(i).GetComponent<setLine_script>().CalculateGestureDict();

					List<string> keys = new List<string>(cdict.Keys);

					for (int k = 0; k < keys.Count; k++)
					{
						if (dict.ContainsKey(keys[k]))
						{
							dict[keys[k]] += cdict[keys[k]];

							//Debug.Log("old key");
						}
						else
						{
							dict.Add(keys[k], cdict[keys[k]]);

							//Debug.Log("new key");
						}
					}

					//MergeGestureDicts(transform.GetChild(i).GetComponent<setLine_script>().gestureDict);
				}
			}
		}

		return dict;
	}

	public void MergeGestureDicts(Dictionary<string, int> dict)
	{
		List<string> keys = new List<string>(dict.Keys);

		for (int i = 0; i < keys.Count; i++)
		{
			if (this.gestureDict.ContainsKey(keys[i]))
			{
				this.gestureDict[keys[i]] += dict[keys[i]];
			}
			else
			{
				this.gestureDict.Add(keys[i], dict[keys[i]]);
			}
		}
	}

	public void unlinkPenChildren(GameObject radmenu)
	{
		// Go through all children and extract all pen objects. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "set")
			{
				transform.GetChild(i).GetComponent<setLine_script>().unlinkPenChildren(null);
			}
			else if (transform.GetChild(i).tag == "function")
			{
				transform.GetChild(i).GetComponent<functionLine_script>().unlinkPenChildren(null);
			}
		}

		// Then destroy the current set and menu
		if (radmenu != null) Destroy(radmenu);
	}

	public void unlinkAllChildren()
	{
		// Go through all children and unlink to paint canvas. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "set")
			{
				transform.GetChild(i).GetComponent<setLine_script>().unlinkAllChildren();
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "function")
			{
				transform.GetChild(i).GetComponent<functionLine_script>().unlinkAllChildren();
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
		}
	}

	// keep checking every frame if it came out from a double function lasso or if it's still a child of it.
	public void checkIfThisIsPartOfDoubleFunction()
	{
		if (is_this_double_function_operand)
		{
			if (transform.parent.tag == "function" && transform.parent.GetComponent<functionLine_script>().is_this_a_double_function)
			{
				// keep it true
				is_this_double_function_operand = true;
			}
			else
			{
				// turn off double function bool
				is_this_double_function_operand = false;
				transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = this_set_name;
			}
		}
	}

	public void fillSetAtRandomPositions(GameObject obj, int num, float radius)
	{
		//GameObject dummy;
		// padding so that the object stays withing boundary of the set
		//float padding = 10f;

		
		// re-compute bounds with the local coordinates
		// computeBounds();

		for (int i = 0; i < num; i++)
		{
			createPenObjectAtRandomPosition(obj, radius);
		}

		//dummy = null;
		
	}

	public GameObject createPenObjectAtRandomPosition(GameObject obj, float radius)
	{
		return Instantiate(obj,
				transform.position + new Vector3(
					UnityEngine.Random.Range(-radius, radius),
					UnityEngine.Random.Range(-radius, radius),
					-40f),
				Quaternion.identity, transform);
	}

	public void deleteChildren(int num)
	{
		if (num > transform.childCount - 2) return;

		for (int i = transform.childCount - 1; i > transform.childCount - 1 - num; i--)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
	}

	public void relayDraggingInfoToChildren()
	{
		if (draggable_now)
		{
			for (int i = 2; i < transform.childCount; i++)
			{
				if (transform.GetChild(i).tag == "penline")
				{
					transform.GetChild(i).GetComponent<penLine_script>().draggable_now = true;
				}
				else if (transform.GetChild(i).tag == "set")
				{
					transform.GetChild(i).GetComponent<setLine_script>().draggable_now = true;
					// relay to set's children
					transform.GetChild(i).GetComponent<setLine_script>().relayDraggingInfoToChildren();
				}
			}

			relayCount++;
		}
		else if(!draggable_now && relayCount > 0)
		{
			for (int i = 2; i < transform.childCount; i++)
			{
				if (transform.GetChild(i).tag == "penline")
				{
					transform.GetChild(i).GetComponent<penLine_script>().draggable_now = false;
				}
				else if (transform.GetChild(i).tag == "set")
				{
					transform.GetChild(i).GetComponent<setLine_script>().draggable_now = false;
					// relay to set's children
					transform.GetChild(i).GetComponent<setLine_script>().relayDraggingInfoToChildren();
				}
			}

			relayCount = 0;
		}
	}

	public void applyGlobalStrokeDetails()
	{
		// act only if visible, and if set line is finished drawing
		if ((abstraction_layer == 1 || abstraction_layer == 2) && transform.childCount >= 2)
		{

			bool anchor_state = details_dropdown.GetComponent<DropdownMultiSelect>().transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(3).GetComponent<Toggle>().isOn;
			bool mesh_state = details_dropdown.GetComponent<DropdownMultiSelect>().transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(4).GetComponent<Toggle>().isOn;

			if (anchor_state != global_details_on_anchor)
			{
				// change needed
				if (!anchor_state)
				{
					hideAnchorAndLegible();
					global_details_on_anchor = false;
				}
				else if (anchor_state)
				{
					showAnchorAndLegible();
					global_details_on_anchor = true;
				}
			}

			if (mesh_state != global_details_on_mesh)
			{
				// change needed
				if (!mesh_state)
				{
					hideMesh();
					global_details_on_mesh = false;
				}
				else if (mesh_state)
				{
					showMesh();
					global_details_on_mesh = true;
				}
			}
		}
	}

	public void hideAnchorAndLegible()
	{
		transform.GetChild(0).gameObject.SetActive(false);
	}

	public void showAnchorAndLegible()
	{
		transform.GetChild(0).gameObject.SetActive(true);
	}

	public void hideMesh()
	{
		transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
	}

	public void showMesh()
	{
		transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
	}

	public int HighestMeshPointIndex()
	{
		// which local point is the highest? Return its index.
		// find maxy from all the points
		float mxy = -100000f;
		int ind = -1;
		for (int i = 0; i < points.Count; i++)
		{
			if (points[i].y > mxy)
			{
				mxy = points[i].y;
				ind = i;
			}
		}
		return ind;
	}

	public int LowestMeshPointIndex()
	{
		// which local point is the highest? Return its index.
		// find maxy from all the points
		float mny = 100000f;
		int ind = -1;
		for (int i = 0; i < points.Count; i++)
		{
			if (points[i].y < mny)
			{
				mny = points[i].y;
				ind = i;
			}
		}
		return ind;
	}

	public void removeAllChildren()
	{
		for (int i = 2; i < transform.childCount; i++)
		{
			Destroy(transform.GetChild(i).gameObject);
		}
	}

	public void exportMesh()
	{
		//GameObject trial = Instantiate(transform.GetChild(1).gameObject, transform.position, Quaternion.identity, transform);
		//trial.GetComponent<MeshFilter>().mesh = dummy;
		//trial.GetComponent<MeshFilter>().sharedMesh = dummy;
		//ObjExporter.MeshToFile(trial.GetComponent<MeshFilter>(), "Assets/Resources/Models/Test.obj");
	}

	public void loadDummyMesh()
	{
		Mesh dummy = Resources.Load<Mesh>("Models/Test");
		//GameObject trial = Instantiate(transform.GetChild(1).gameObject, transform.position, Quaternion.identity, transform);
		//trial.GetComponent<MeshFilter>().mesh = dummy;
		//trial.GetComponent<MeshFilter>().sharedMesh = dummy;
		//ObjExporter.MeshToFile(trial.GetComponent<MeshFilter>(), "Assets/Resources/Models/Test.obj");

		GameObject test = new GameObject("test");
		test.AddComponent<MeshFilter>();
		test.AddComponent<MeshRenderer>();
		test.GetComponent<MeshFilter>().mesh = dummy;
		test.GetComponent<MeshFilter>().sharedMesh = dummy;
	}

	public void loadLassoMesh(string filename)
	{
		Mesh dummy = Resources.Load<Mesh>("Models/" + filename);

		transform.GetChild(1).GetComponent<MeshFilter>().mesh = dummy;
		transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh = dummy;
	}

	public void UpdateActionHistory()
	{
		// keep count of children regardless of action history enabled, will help build prev snapshot
		//current_child_count = transform.childCount;

		if (transform.childCount < 2) return;	// allow drawing to finish

		UpdateChildrenHistory();

		if (Paintable_Script.ActionHistoryEnabled)
		{
			if (prevChildrenHistory.Count != ChildrenHistory.Count)
			{
				//Debug.Log("here");

				// find out what's added or missing
				GameObject change;
				if (prevChildrenHistory.Count > ChildrenHistory.Count)
				{
					change = prevChildrenHistory.Except(ChildrenHistory).ToList()[0];
				}
				else
					change = ChildrenHistory.Except(prevChildrenHistory).ToList()[0];

				if (change != null)
				{
					//Debug.Log(change.name);

					Sprite changesnap = TakeSnapshot(change);
					current_snapshot = TakeSnapshot();

					// Add an item to action history panel with the snapshots

					// is it plus or minus?
					string op = "";
					if (prevChildrenHistory.Count > ChildrenHistory.Count)
					{
						op = "-";
					}
					else op = "+";


					// update the action history panel with a new item
					GameObject actionhist = GameObject.Find("ActionHistory");
					//GameObject listtocopy = actionhist.transform.GetChild(0).GetChild(0).GetChild(1).gameObject;
					GameObject listtocopy = Resources.Load<GameObject>("Prefabs/ActionHistory/Item");
					GameObject newitem = Instantiate(listtocopy, actionhist.transform.GetChild(0).GetChild(0).transform);
					
					// customize the newitem with proper images and operator
					newitem.transform.Find("first_operand").GetComponent<Image>().sprite = prev_snapshot;
					newitem.transform.Find("second_operand").GetComponent<Image>().sprite = changesnap;
					newitem.transform.Find("result").GetComponent<Image>().sprite = current_snapshot;
					newitem.transform.Find("operator").GetComponent<TextMeshProUGUI>().text = op;

					actionhist.transform.GetChild(0).GetComponent<ScrollRect>().verticalNormalizedPosition = -0.5f;	// forces to 0, goes all the way down.
				}
			}
		}

		//prev_child_count = current_child_count;
		prevChildrenHistory.Clear();
		prevChildrenHistory.AddRange(ChildrenHistory);
		prev_snapshot = TakeSnapshot();
	}

	public Sprite TakeSnapshot()
	{
		// snapshot of current hierarchy
		RuntimePreviewGenerator.PreviewDirection = new Vector3(0, 0, 1);
		RuntimePreviewGenerator.BackgroundColor = new Color(0.3f, 0.3f, 0.3f, 0f);
		RuntimePreviewGenerator.OrthographicMode = true;

		Sprite action = Sprite.Create(RuntimePreviewGenerator.GenerateModelPreview(transform, 128, 128)
			, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 20f);
		//GameObject.Find("Action").GetComponent<Image>().sprite = action;
		return action;
	}

	public Sprite TakeSnapshot(GameObject obj)
	{
		RuntimePreviewGenerator.PreviewDirection = new Vector3(0, 0, 1);
		RuntimePreviewGenerator.BackgroundColor = new Color(0.3f, 0.3f, 0.3f, 0f);	// fully transparent
		RuntimePreviewGenerator.OrthographicMode = true;

		Sprite action = Sprite.Create(RuntimePreviewGenerator.GenerateModelPreview(obj.transform, 128, 128)
			, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 20f);
		//GameObject.Find("Action").GetComponent<Image>().sprite = action;
		return action;
	}

	public void UpdateChildrenHistory()
	{
		ChildrenHistory.Clear();

		for (int i = 2; i < transform.childCount; i++)
		{
			ChildrenHistory.Add(transform.GetChild(i).gameObject);
		}
	}

	public int CountAllIconicChildren()
	{
		// discount _anchor and _meshobj

		int total = 0;

		for (int i = 2; i < transform.childCount; i++)
		{
			if (transform.GetChild(i).tag == "set")
			{
				total += transform.GetChild(i).GetComponent<setLine_script>().CountAllIconicChildren();
			}
			else if (transform.GetChild(i).tag == "penline") total++;
		}
	
		return total;
	}

	// Start is called before the first frame update
	void Awake()
    {
		symbol_name = new Symbol(this_set_name);
		gestureTEX1 = new Symbol(@"\");
		gestureTEX2 = new Symbol(@"\");

		details_dropdown = GameObject.Find("Details_Dropdown");

		pan_button = GameObject.Find("Pan");

		visibility.mesh = true;
		visibility.anchor = true;
		visibility.argument = true;

		// needed for action history
		UpdateChildrenHistory();
	}

    // Update is called once per frame
    void Update()
    {
		if (set_timer_menu == null)
		{
			updateSetFromTimer();
		}
		checkHitAndMove();
		updateFeature();
		updateLegibleLayer();
		checkIfThisIsPartOfDoubleFunction();
		onAbstractionLayerChange();

		// Action History Update (optional, comment out when not needed)
		UpdateActionHistory();

		// apply global stroke details layer, at the end.
		applyGlobalStrokeDetails();
    }
}
