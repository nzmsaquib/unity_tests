﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class param_text_Script : MonoBehaviour
{
	public List<Vector3> translation_path = new List<Vector3>();

	public float upper_val = 5f;

	//public int index = 0;	//index in penline.getchild(4) key points hierarchy. By default, and in default case, there's only one element param_text_end, so index is 0.

	public float section_length = 5f;  // default is 5, unless key points are edited by the user.

	private void Awake()
	{
		upper_val = 5f;
	}
}
