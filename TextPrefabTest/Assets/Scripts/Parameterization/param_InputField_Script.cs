﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class param_InputField_Script : MonoBehaviour, ISelectHandler
{
	public GameObject penLineObjectParent;

	public float current_val;

	public int keypoint_index;	// index in the penline.child(4) hierarchy, sibling index. 

    // Update is called once per frame
    void Update()
    {
		if (transform.GetComponent<InputField>().isFocused == false)
		{

			//UpdateParameterFromInput();		
			//UpdatePenParameterFromInput();
			updatePenParentFromInput();

			// finally, destroy the text field
			Destroy(this.gameObject);
		}
    }

	public void OnSelect(BaseEventData eventData)
	{
		
	}

	public void OnDeselect(BaseEventData eventData)
	{
		//Destroy(this.gameObject);
	}

	public void OnSubmit(BaseEventData eventData)
	{
		//UpdateParameterFromInput();
		//UpdatePenParameterFromInput();
		updatePenParentFromInput();
	}

	public void updatePenParentFromInput()
	{
		int newval = int.Parse(transform.GetComponent<InputField>().text);

		if (newval < current_val)
			return;
		else if (newval > current_val)
		{
			// update all key points to shift the new change
			for (int i = keypoint_index; i < penLineObjectParent.transform.GetChild(4).childCount; i++)
			{
				penLineObjectParent.transform.GetChild(4).GetChild(i).GetComponent<param_text_Script>().upper_val += newval - current_val;

				// update the section lengths too
				if (i == 0) penLineObjectParent.transform.GetChild(4).GetChild(i).GetComponent<param_text_Script>().section_length = 5f;
				else
				{
					penLineObjectParent.transform.GetChild(4).GetChild(i).GetComponent<param_text_Script>().section_length =
						(int)(penLineObjectParent.transform.GetChild(4).GetChild(i).GetComponent<param_text_Script>().upper_val -
						penLineObjectParent.transform.GetChild(4).GetChild(i - 1).GetComponent<param_text_Script>().upper_val);
				}
			}
			penLineObjectParent.GetComponent<penLine_script>().max_parameter_value += (int)(newval - current_val);
		}
	}

	/*
	public void UpdatePenParameterFromInput()
	{
		if (keypoint_object == null)   // the last text box
		{
			penLineObjectParent.GetComponent<penLine_script>().max_parameter_value = int.Parse(transform.GetComponent<InputField>().text);
			// update end text label
			penLineObjectParent.transform.GetChild(1).GetComponent<TextMeshPro>().text =
			transform.GetComponent<InputField>().text;
		}
		else
		{
			float textfieldval = float.Parse(transform.GetComponent<InputField>().text);
			// find out its serial among the key points made so far. Compare using two decimal places accuracy.
			int serial = -1;
			decimal cv = decimal.Round((decimal)current_val, 2);

			if (penLineObjectParent.GetComponent<penLine_script>().translation_sections_indices.TryGetValue((float)cv, out serial))
			{

			}
			// leave out the first and last section indices
			for (int i = 1; i < penLineObjectParent.GetComponent<penLine_script>().translation_sections_indices.Count - 1; i++)
			{
				float lval = 0f;// penLineObjectParent.GetComponent<penLine_script>().translation_sections_indices[
				if (decimal.Round((decimal)lval, 2) == cv)
				{
					serial = i;

					break;
				}
			}

			if (serial != -1)
			{
				// compare two values, allow only increasing values along the series, not decreasing on any key point.
				if (textfieldval > current_val)
				{
					// cummulatively add up for key point values.
					Debug.Log("valid input.");
				}
			}
		}
	}

	public void UpdateParameterFromInput()
	{
		// search for the edge that contains the parent pen object
		// then find the source object from the edge, and set the capacity of the source object

		// TODO: PARSE THE TEXT WITH PROPER VALIDATION AND FORMATTING

		GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
		bool edgefound = false;
		for (int i = 0; i < edges.Length; i++)
		{
			if (edges[i].GetComponent<edgeLine_script>().target_object.name == transform.parent.name)
			{
				//Debug.Log("found edge target.");
				edgefound = true;
				
				if (edges[i].GetComponent<edgeLine_script>().source_object.tag == "set")
				{
					edges[i].GetComponent<edgeLine_script>().source_object.GetComponent<setLine_script>().capacity
						= int.Parse(transform.GetComponent<InputField>().text);
				}
				else if (edges[i].GetComponent<edgeLine_script>().source_object.tag == "function")
				{
					edges[i].GetComponent<edgeLine_script>().source_object.GetComponent<functionLine_script>()
						.max_function_value = int.Parse(transform.GetComponent<InputField>().text);
				}
				
				// update end text label
				penLineObjectParent.transform.GetChild(1).GetComponent<TextMeshPro>().text =
				transform.GetComponent<InputField>().text;

				break;
			}
		}

		// no edge found, update the text label and pen object's max_parameter_value
		if (!edgefound)
		{
			//Debug.Log("retaining original value.");
			penLineObjectParent.GetComponent<penLine_script>().max_parameter_value = 
				int.Parse(transform.GetComponent<InputField>().text);

			penLineObjectParent.transform.GetChild(1).GetComponent<TextMeshPro>().text =
				transform.GetComponent<InputField>().text;

		}
	}

	*/

}
