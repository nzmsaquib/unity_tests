﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class edgeLine_InputField_Script : MonoBehaviour, ISelectHandler
{
	public GameObject edgeLineObjectParent;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (transform.GetComponent<InputField>().isFocused == false)
		{
			edgeLineObjectParent.GetComponent<edgeLine_script>().full_condition_text =
				transform.GetComponent<InputField>().text;

			edgeLineObjectParent.GetComponent<edgeLine_script>().parseConditionString();

			Debug.Log(edgeLineObjectParent.GetComponent<edgeLine_script>().parse_result.ToString());

			edgeLineObjectParent.GetComponent<edgeLine_script>().updateTEXLabel();

			// finally, destroy the text field
			Destroy(this.gameObject);
		}
	}

	public void OnSelect(BaseEventData eventData)
	{

	}

	public void OnDeselect(BaseEventData eventData)
	{
		//Destroy(this.gameObject);
	}

	public void OnSubmit(BaseEventData eventData)
	{
		edgeLineObjectParent.GetComponent<edgeLine_script>().full_condition_text =
				transform.GetComponent<InputField>().text;

		edgeLineObjectParent.GetComponent<edgeLine_script>().parseConditionString();

		edgeLineObjectParent.GetComponent<edgeLine_script>().updateTEXLabel();
	}




}
