﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;
using TMPro;
using uPIe;
using PDollarGestureRecognizer;

using Jobberwocky.GeometryAlgorithms.Source.API;
using Jobberwocky.GeometryAlgorithms.Source.Core;
using Jobberwocky.GeometryAlgorithms.Source.Parameters;

using Symbolism;
using Symbolism.AlgebraicExpand;
using Symbolism.RationalizeExpression;

using BezierSolution;

public class Paintable_Script : MonoBehaviour
{

	// PAN AND ZOOM RELATED PARAMETERS
	public Camera main_camera;
	public float finger_move_tolerance = 100f;
	private static float zoom_multiplier = 0.5f;
	public static float zoom_min = 200;
	public static float zoom_max = 850;
	public float pan_amount = 5f;
	public Vector2 panTouchStart;
	public bool previousTouchEnded;
	public bool okayToPan = true;
	public bool panZoomLocked = false;

	// Length, area, distance units
	public static float unitScale = 0.025f;

	public GameObject PenLine;
	public GameObject SetLine;
	public GameObject EdgeLinePrefab;
	public GameObject SpawnEdgePrefab;
	public GameObject FunctionLine;
	public GameObject templine;
	public GameObject StaticPenLine;

	public GameObject ParameterUIField;
	public GameObject EdgeUIField;
	public GameObject SpawnEdgeMenu;
	public GameObject TextInputField;

	public GameObject radial_menu;  // prefab
	public GameObject set_radial_menu;
	public GameObject pen_radial_menu;
	public GameObject function_radial_menu;
	public GameObject set_timer_submenu;
	public GameObject oop_properties_submenu;
	public GameObject category_filter_submenu_prefab;
	public GameObject penline_rotation_menu, penline_scale_menu;
	public Slider abstraction_slider; // prefab
	public GameObject popup_radial;
	public Canvas canvas_radial;

	public static GameObject pencil_button;
	public static GameObject pan_button;
	public static GameObject select_button;
	public static GameObject edge_button;
	public static GameObject function_button;
	public static GameObject eraser_button;
	public static GameObject staticpen_button;
	public static GameObject strokeconversion_button;
	public static GameObject pathdefinition_button;
	public static GameObject textinput_button;
	public static GameObject objectcopy_button;

	// background materials
	public Material default_background_material, grid_material;

	// line, set, function materials
	public Material set_line_material, function_line_material;

	//private VectorLine tempLine;
	//public List<VectorLine> all_lines = new List<VectorLine>();

	// predictive stroke
	private List<Gesture> trainingSet = new List<Gesture>();
	private List<Point> predictive_points = new List<Point>();
	private float predictive_stroke_up_time = 0f;
	private int strokeID = 0;
	public Sprite[] commonShapes;

	// stroke conversion
	public static Vector3[] vertices = new Vector3[0];

	public int totalLines = 0;

	public bool double_finger_activated = false;

	public GameObject edge_start, edge_end;

	public static Pen currentPen;
	public Touchscreen touchScreen;

	// pen tap detection
	float penPressElapsed = 0f;
	System.DateTime penPressStartTime;

	// I/O operations
	public bool IOInProgress = false;

	// objects history
	public List<GameObject> history = new List<GameObject>();

	// Action History
	public static bool ActionHistoryEnabled = false;

	// hacks
	public bool stickfigure = false;
	public static bool allowOpacity = false;

	void Awake()
	{
		// Input System Enhanced Touch
		EnhancedTouchSupport.Enable();
	}

	// Start is called before the first frame update
	void Start()
    {
		//main_camera.GetComponent<MobileTouchCamera>().enabled = false;

		touchScreen = Touchscreen.current;
		//touchScreen = InputSystem.AddDevice<Touchscreen>();

		pencil_button = GameObject.Find("Pencil");
		pan_button = GameObject.Find("Pan");
		select_button = GameObject.Find("Select");
		edge_button = GameObject.Find("Edge_draw");
		function_button = GameObject.Find("FunctionButton");
		eraser_button = GameObject.Find("Eraser");
		staticpen_button = GameObject.Find("StaticPen");
		strokeconversion_button = GameObject.Find("Stroke_Conversion");
		pathdefinition_button = GameObject.Find("Path_Definition");
		textinput_button = GameObject.Find("Text_Input");
		objectcopy_button = GameObject.Find("Copy");

		/*
		// Symbolism library test
		Symbol a = new Symbol("a");
		Symbol b = new Symbol("b");

		MathObject result, result2, result3;

		//Debug.Log((a + a + b).ToString());

		result = a + a + b - (a + 2 * a) + (b * a) + a;
		Debug.Log(result.ToString());

		result = b - a;
		Debug.Log(result.StandardForm().ToString());
		result2 = result / b;
		Debug.Log(result2.StandardForm().ToString());
		//Debug.Log(result2.AlgebraicExpand().ToString());
		//Debug.Log(result2.RationalizeExpression().ToString());

		//Debug.Log(result2.Term().ToString());
		result3 = (12 * a - 6 * b) / (7 * b - 4 * a);

		//Debug.Log(result3.AlgebraicExpand().ToString());
		//Debug.Log(result3.RationalizeExpression().ToString());

		Debug.Log(result3.RationalizeExpression().Numerator().StandardForm().ToString());
		Debug.Log(result3.RationalizeExpression().Denominator().StandardForm().ToString());
		*/

		/*
		// Test creating primitives
		List<Vector3> testpath = new List<Vector3>();
		testpath.Add(new Vector3(0, 0, -40)); testpath.Add(new Vector3(100, 20, -40)); testpath.Add(new Vector3(200, 80, -40));
		transform.GetComponent<CreatePrimitives>().CreatePenLine(testpath, Color.red, 2f, null);

		testpath = new List<Vector3>();
		testpath.Add(new Vector3(0, 0, -40)); testpath.Add(new Vector3(200, 40, -40)); testpath.Add(new Vector3(400, 160, -40));
		transform.GetComponent<CreatePrimitives>().CreateSet(testpath);

		testpath = new List<Vector3>();
		testpath.Add(new Vector3(-10, -10, -40)); testpath.Add(new Vector3(400, 80, -40)); testpath.Add(new Vector3(800, 320, -40));
		transform.GetComponent<CreatePrimitives>().CreateFunction(testpath);
		//*/

		// Predictive stroke
		// Load pre-made gestures
		TextAsset[] gesturesXml = Resources.LoadAll<TextAsset>("GestureSet/BasicShapeTemplates/");
		foreach (TextAsset gestureXml in gesturesXml)
			trainingSet.Add(GestureIO.ReadGestureFromXML(gestureXml.text));

		commonShapes = Resources.LoadAll<Sprite>("Shapes/CommonShapes2");

	}

	// Update is called once per frame
	void Update()
	{

		// TODO/NOTE: TRY MOVING THE PAN/ZOOM/TAP SECTION AT THE BEGINNING (HERE), TO CHECK IF THE BUTTON SELECTION ISSUE (BUTTONS SOMETIMES DON'T
		// RESPOND) WOULD RESOLVE. THE ORDER SHOULD BE: CHECK TAP, PREVENT UNWANTED TOUCH IN CASE A DRAWING MODE IS SELECTED, REST OF THE INTERACTIONS.

		#region prevent unwanted touch on canvas
		// prevent touch or click being registered on the canvas when a gui button is clicked
		if (AllButtonsBehavior.isPointerOverPencil || AllButtonsBehavior.isPointerOverSelect || AllButtonsBehavior.isPointerOverPan
		|| AllButtonsBehavior.isPointerOverEdgeButton || AllButtonsBehavior.isPointerOverFunctionButton ||
		AllButtonsBehavior.isPointerOverEraserButton || AllButtonsBehavior.isPointerOverStaticPen || AllButtonsBehavior.isPointerOverStrokeConvert
		|| AllButtonsBehavior.isPointerOverPathDefinition || AllButtonsBehavior.isPointerOverTextInput || AllButtonsBehavior.isPointerOverCopy)
		{
			return;
		}
		#endregion

		// TEST AREA: TEST ANYTHING THAT'S SUITABLE FOR UPDATE()
		#region Test Area
		/*
		if (currentPen.tip.isPressed)
		{
			Debug.Log(new Vector2(currentPen.position.x.ReadValue(), currentPen.position.y.ReadValue()).ToString());
		}

		//touchScreen.touches[0].phase == UnityEngine.InputSystem.TouchPhase.Began;
		//touchScreen.touches.Count
		//currentPen.tip.wasPressedThisFrame
		//currentPen.delta.EvaluateMagnitude()
		//touchScreen.touches[0].position

		*/
		#endregion

		#region skip during i/o operations
		if (IOInProgress)
		{
			return;
		}
		#endregion

		#region set input variables needed for every frame update
		currentPen = Pen.current;
		var activeTouches = UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches;
		#endregion

		// PENCIL BRUSH
		#region pencil
		if (pencil_button.GetComponent<AllButtonsBehavior>().selected &&
			!pencil_button.GetComponent<AllButtonsBehavior>().isPredictivePen)
		{
			//Debug.Log("entered");
			//if (Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			if(PenTouchInfo.PressedThisFrame)//currentPen.tip.wasPressedThisFrame)
			{
				// start drawing a new line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					totalLines++;
					templine = Instantiate(PenLine, vec, Quaternion.identity, this.gameObject.transform);
					templine.GetComponent<TrailRenderer>().material.color = Color.black;

					templine.name = "penLine_" + totalLines.ToString();
					templine.tag = "penline";

					templine.GetComponent<penLine_script>().points.Add(vec);

					// Initiate the length display (use an existing text box used for translation parameterization)
					templine.transform.GetChild(1).localScale = new Vector3(4, 4, 1);
					// fix the position of the text
					templine.transform.GetChild(1).transform.position = vec;

					templine.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
					templine.transform.GetChild(1).GetComponent<BoxCollider2D>().enabled = false;

					// color and width
					templine.GetComponent<TrailRenderer>().material.color =
						pencil_button.GetComponent<AllButtonsBehavior>().pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;

					templine.GetComponent<penLine_script>().pen_line_material.color =
						pencil_button.GetComponent<AllButtonsBehavior>().pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;

					templine.GetComponent<TrailRenderer>().widthMultiplier =
						pencil_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

					templine.GetComponent<LineRenderer>().widthMultiplier =
						pencil_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

					// disable the argument_label button, currently it's at the 3rd index
					templine.transform.GetChild(2).gameObject.SetActive(false);

					// add to history
					history.Add(templine);

				}
			}

			//else if (Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved && templine != null)
			else if (templine != null &&
				PenTouchInfo.PressedNow //currentPen.tip.isPressed
				&& (PenTouchInfo.penPosition - 
				(Vector2)templine.GetComponent<penLine_script>().points[templine.GetComponent<penLine_script>().points.Count-1]).magnitude > 0f)
			{
				// add points to the last line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;

					templine.GetComponent<TrailRenderer>().transform.position = vec;
					templine.GetComponent<penLine_script>().points.Add(vec);

					// Show the distance, format to a fixed decimal place.
					templine.GetComponent<penLine_script>().calculateLengthAttributeFromPoints();
					templine.transform.GetChild(1).GetComponent<TextMeshPro>().text =
						templine.GetComponent<penLine_script>().attribute.Length.ToString("F1");

					// pressure based pen width
					templine.GetComponent<penLine_script>().updateLengthFromPoints();
					templine.GetComponent<penLine_script>().addPressureValue(PenTouchInfo.pressureValue);
					templine.GetComponent<penLine_script>().reNormalizeCurveWidth();
					templine.GetComponent<TrailRenderer>().widthCurve = templine.GetComponent<penLine_script>().widthcurve;
				}
			}

			else if (templine != null && 
				//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended || Input.GetTouch(0).phase == UnityEngine.TouchPhase.Canceled))
				PenTouchInfo.ReleasedThisFrame)
					//currentPen.tip.wasReleasedThisFrame)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition); //currentPen.position.ReadValue());// Input.GetTouch(0).position);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{
					if (templine.GetComponent<penLine_script>().points.Count > 8)
					{
						templine = transform.GetComponent<CreatePrimitives>().FinishPenLine(templine);

						// set templine to null, otherwise, if an existing touch from color picker makes it to the canvas,
						// then the object jumps to the color picker (as a new templine hasn't been initialized from touch.begin).
						templine = null;
					}
					else
					{
						// delete the templine, not enough points
						Destroy(templine);
					}
				}
				else
				{
					// the touch didn't end on a line, destroy the line
					Destroy(templine);
				}

			}

		}
		#endregion

		#region predictive pencil
		if (pencil_button.GetComponent<AllButtonsBehavior>().selected &&
			pencil_button.GetComponent<AllButtonsBehavior>().isPredictivePen)
		{
			// set/reset predictive stroke up time, how long has passed after the drawing was done?

			if (PenTouchInfo.PressedThisFrame)//currentPen.tip.wasPressedThisFrame)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			{
				// reset clock
				predictive_stroke_up_time = 0f;

				// start drawing a new line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40f); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					totalLines++;
					templine = Instantiate(PenLine, vec, Quaternion.identity, this.gameObject.transform);
					templine.GetComponent<TrailRenderer>().material.color = Color.black;

					templine.name = "temp_penLine_" + totalLines.ToString();
					templine.tag = "penline";

					predictive_points.Add(new Point(vec.x, vec.y, strokeID));
					strokeID++;

					// Initiate the length display (use an existing text box used for translation parameterization)
					templine.transform.GetChild(1).localScale = new Vector3(4, 4, 1);
					// fix the position of the text
					templine.transform.GetChild(1).transform.position = vec;

					templine.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
					templine.transform.GetChild(1).GetComponent<BoxCollider2D>().enabled = false;

					// color and width
					templine.GetComponent<TrailRenderer>().material.color =
						pencil_button.GetComponent<AllButtonsBehavior>().pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;

					templine.GetComponent<TrailRenderer>().startWidth =
						pencil_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

					templine.GetComponent<TrailRenderer>().endWidth =
						pencil_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

					// disable the argument_label button, currently it's at the 3rd index
					templine.transform.GetChild(2).gameObject.SetActive(false);

				}
			}

			else if ( PenTouchInfo.PressedNow && //currentPen.tip.isPressed && //Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved && 
				templine != null)
			{
				// add points to the last line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40f); // Vector3.up * 0.1f;

					templine.GetComponent<TrailRenderer>().transform.position = vec;
					templine.GetComponent<penLine_script>().points.Add(vec);

					predictive_points.Add(new Point(vec.x, vec.y, strokeID));

					// Show the distance, format to a fixed decimal place.
					templine.GetComponent<penLine_script>().calculateLengthAttributeFromPoints();
					templine.transform.GetChild(1).GetComponent<TextMeshPro>().text =
						templine.GetComponent<penLine_script>().attribute.Length.ToString("F1");
				}
			}

			else if ( PenTouchInfo.ReleasedThisFrame && //currentPen.tip.wasReleasedThisFrame && //Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended 
				templine != null)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{
					// reset trail renderer
					// templine.GetComponent<TrailRenderer>().Clear();

					// start clock
					predictive_stroke_up_time = 0f;

					// set templine to null, otherwise, if an existing touch from color picker makes it to the canvas,
					// then the object jumps to the color picker (as a new templine hasn't been initialized from touch.begin).
					templine = null;
				}
			}
		}

		if (//Input.touchCount == 0 && 
			pencil_button.GetComponent<AllButtonsBehavior>().selected &&
			pencil_button.GetComponent<AllButtonsBehavior>().isPredictivePen)
		{

			predictive_stroke_up_time += Time.fixedDeltaTime;

			// if there's something to predict, then do it when the designated time has passed
			if (predictive_points.Count > 10 && predictive_stroke_up_time > 0.8f)	//1f = 1sec
			{

				Gesture candidate = new Gesture(predictive_points.ToArray());
				Result gestureResult = PointCloudRecognizer.Classify(candidate, trainingSet.ToArray());

				string message = gestureResult.GestureClass + " " + gestureResult.Score;
				Debug.Log(message);

				// COLLECT AND CLEAN UP
				float total_length = 0f;
				int total_strokes = 0;

				// destroy temp_penLine created for the candidate strokes
				GameObject[] all_penlines = GameObject.FindGameObjectsWithTag("penline");
				//List<GameObject> temp_penlines = new List<GameObject>();
				foreach (GameObject pen in all_penlines)
				{
					if (pen.name.Contains("temp_penLine_"))
					{
						// Get the total Length of all temp_penLine_
						total_length += pen.GetComponent<penLine_script>().attribute.Length;

						// increase stroke count
						total_strokes++;

						// Destroy the temp penline
						Destroy(pen);
					}
				}

				// hack for stick figure: assign anything with multiple strokes to stick figure
				if (total_strokes > 1) stickfigure = true;

				// find the average position of all the points
				Vector3 avg_stroke_position, total = new Vector3();
				for (int k = 0; k < predictive_points.Count; k++)
				{
					total.x += predictive_points[k].X;
					total.y += predictive_points[k].Y;
					total.z = -40f;
				}
				avg_stroke_position = total / predictive_points.Count;

				// find the bounding box 
				float maxx = -100000f, maxy = -100000f, minx = 100000f, miny = 100000f;

				for (int i = 0; i < predictive_points.Count; i++)
				{
					if (maxx < predictive_points[i].X) maxx = predictive_points[i].X;
					if (maxy < predictive_points[i].Y) maxy = predictive_points[i].Y;
					if (minx > predictive_points[i].X) minx = predictive_points[i].X;
					if (miny > predictive_points[i].Y) miny = predictive_points[i].Y;
				}

				// Now use the closest gesture from the result to create a penLine from it
				totalLines++;
				templine = Instantiate(PenLine, avg_stroke_position, Quaternion.identity, this.gameObject.transform);

				templine.name = "penLine_" + totalLines.ToString();
				templine.tag = "penline";

				// add to history
				history.Add(templine);

				// use Clipper to inflate/deflate the polygon?
				// ..

				/*
				// copy the gesture template into the templine's points
				for (int i = 0; i < gestureResult.MatchedGesture.Points.Length; i++)
				{
					Point gp = gestureResult.MatchedGesture.Points[i];
					templine.GetComponent<penLine_script>().points.Add(new Vector3(gp.X, gp.Y, -40f));
				}
				*/

				// copy the predictive stroke(s) into the templine's points
				for (int i = 0; i < predictive_points.Count; i++)
				{
					Point gp = predictive_points[i];
					templine.GetComponent<penLine_script>().points.Add(new Vector3(gp.X, gp.Y, -40.5f));
				}

				// set the gesture/legible layer fields
				templine.GetComponent<penLine_script>().isPredictionDone = true;
				templine.GetComponent<penLine_script>().gestureTemplate = gestureResult.GestureClass;

				bool spriteFound = false;
				for (int s = 0; s < commonShapes.Length; s++)
				{
					if (commonShapes[s].name == gestureResult.GestureClass)
					{
						templine.GetComponent<penLine_script>().recognized_sprite = commonShapes[s];
						spriteFound = true;
						Debug.Log("found sprite.");					
						break;
					}
				}
				if (spriteFound == false)
				{
					// choose the first one as default
					templine.GetComponent<penLine_script>().recognized_sprite = commonShapes[0];
				}

				templine.GetComponent<penLine_script>().recognized_sprite =
					Resources.Load<Sprite>("ShapeSprites/" + gestureResult.GestureClass);

				// hack for stick figure
				if (stickfigure)
				{
					templine.GetComponent<penLine_script>().gestureTemplate = "legrectangle";
					templine.GetComponent<penLine_script>().recognized_sprite = Resources.Load<Sprite>("ShapeSprites/legstick");
					//commonShapes[8];	// the 9th element is stick figure in CommonShapes2.
					stickfigure = false;
				}

				// this one is needed for the penline script functions
				templine.GetComponent<penLine_script>().paintable_object =
					GameObject.FindGameObjectWithTag("paintable_canvas_object");

				// compute centroid and bounds
				templine.GetComponent<penLine_script>().computeCentroid();
				templine.GetComponent<penLine_script>().computeBounds();

				// clean up the drawn points list
				predictive_points.Clear();

				// set line renderer width
				templine.GetComponent<LineRenderer>().startWidth =
				pencil_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

				templine.GetComponent<LineRenderer>().endWidth =
					pencil_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

				// update the line renderer
				templine.GetComponent<LineRenderer>().positionCount = templine.GetComponent<penLine_script>().points.Count;
				templine.GetComponent<LineRenderer>().SetPositions(templine.GetComponent<penLine_script>().points.ToArray());

				// bake into mesh: create a new child object. Direct mesh addition in the Set messes up the transform of the mesh
				// for some reason.
				GameObject meshObj = new GameObject("_meshobj");
				meshObj.AddComponent<MeshFilter>();
				meshObj.AddComponent<MeshRenderer>();

				var lineRenderer = templine.GetComponent<LineRenderer>();
				var meshFilter = meshObj.GetComponent<MeshFilter>();
				//Mesh mesh = new Mesh();
				//lineRenderer.BakeMesh(mesh, true);

				Mesh mesh = templine.GetComponent<penLine_script>().SpriteToMesh();
				//Mesh mesh = templine.GetComponent<penLine_script>().SpriteToMesh();
				//Mesh mesh = templine.GetComponent<penLine_script>().createQuad(maxx - minx, maxy - miny);

				meshFilter.sharedMesh = mesh;

				var meshRenderer = meshObj.GetComponent<MeshRenderer>();
				meshRenderer.sharedMaterial = templine.GetComponent<penLine_script>().pen_line_material;

				// calculate scale factor for the mesh
				float scaleFactor = (maxx - minx) /	meshFilter.sharedMesh.bounds.size.x;

				// scale the mesh game object according to scale factor calculated for predictive strokes
				meshObj.transform.localScale = new Vector3(scaleFactor, scaleFactor, 1);

				// get rid of the line renderer?
				// templine.GetComponent<LineRenderer>().enabled = false;
				Destroy(templine.GetComponent<LineRenderer>());

				// disable trail renderer, no longer needed
				templine.GetComponent<TrailRenderer>().enabled = false;

				//Destroy(templine.GetComponent<LineRenderer>());

				// add a collider
				templine.AddComponent<BoxCollider>();

				//Debug.Log("box collider before: " + templine.GetComponent<BoxCollider>().center.ToString());
				//templine.GetComponent<BoxCollider>().size = new Vector3(scaleFactor, scaleFactor, 0);
				templine.GetComponent<BoxCollider>().size = new Vector3(maxx - minx, maxy - miny, 0);
				templine.GetComponent<BoxCollider>().center = new Vector3(0, 0, 0);
				//Debug.Log("box collider after: " + templine.GetComponent<BoxCollider>().center.ToString());

				// set collider trigger
				templine.GetComponent<BoxCollider>().isTrigger = true;

				// disable the collider because we are in the pen mode right now. Pan mode enables back all colliders.
				templine.GetComponent<BoxCollider>().enabled = false;

				templine.transform.position = meshObj.GetComponent<MeshFilter>().sharedMesh.bounds.center;

				// update the previous_position variable for templine for checkMove()
				templine.GetComponent<penLine_script>().previous_position = templine.transform.position;

				// set the boxcollider coordinates
				// this is not needed actually, owing to meshobj being at 0,0.
				//templine.GetComponent<BoxCollider>().center = meshObj.transform.position;
				//templine.GetComponent<BoxCollider>().center = meshObj.transform.position;

				// Need to understand this better: setting the parent transform here stops extra offset
				// Ivan: this might just be a visualization error in Unity?
				meshObj.transform.SetParent(templine.transform);

				templine.transform.position = templine.GetComponent<penLine_script>().centroid;

				// set sibling index. I want this to be Child 0, and the param_text component child 1.
				meshObj.transform.SetAsFirstSibling();

				
				// set mesh renderer color
				meshObj.GetComponent<MeshRenderer>().material.color =
					pencil_button.GetComponent<AllButtonsBehavior>().pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;

				// Update the attributes from the temp_penLine_ and bounding box
				templine.GetComponent<penLine_script>().attribute.Length = total_length;

				// Save the area of the bounding box 
				templine.GetComponent<penLine_script>().attribute.Area =
					meshObj.GetComponent<MeshFilter>().sharedMesh.bounds.size.x *
					meshObj.GetComponent<MeshFilter>().sharedMesh.bounds.size.y * unitScale * unitScale;

				// set current_attribute of penLine
				templine.GetComponent<penLine_script>().current_attribute = total_length;

				// set the _name field of penline
				templine.GetComponent<penLine_script>()._name = "line";
				templine.GetComponent<penLine_script>().symbol_name = new Symbol("line");

				// now transform all points in the line script to local positions
				templine.GetComponent<penLine_script>().fromGlobalToLocalPoints();

				// set up the text labels for transformation GUI (param_text)
				templine.transform.GetChild(1).localScale = new Vector3(4, 4, 1);
				templine.transform.GetChild(2).localScale = new Vector3(4, 4, 1);
				templine.transform.GetChild(4).GetChild(0).localScale = new Vector3(4, 4, 1);

				// set the box collider as the size of the rect transform
				templine.transform.GetChild(4).GetChild(0).GetComponent<BoxCollider2D>().size =
					templine.transform.GetChild(4).GetChild(0).GetComponent<RectTransform>().sizeDelta;

				templine.transform.GetChild(4).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
				templine.transform.GetChild(4).GetChild(0).GetComponent<BoxCollider2D>().enabled = false;

				templine.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
				templine.transform.GetChild(1).GetComponent<BoxCollider2D>().enabled = false;

				templine.transform.GetChild(2).GetComponent<MeshRenderer>().enabled = false;
				templine.transform.GetChild(2).GetComponent<BoxCollider2D>().enabled = false;

				// Set up the argument_label button and text.
				// get the highest point on the pen line, in local coordinates
				templine.transform.GetChild(3).gameObject.SetActive(true);

				templine.GetComponent<penLine_script>().computeBounds();
				templine.GetComponent<penLine_script>().computeCentroid();

				templine.transform.GetChild(3).transform.localPosition =
					new Vector3(templine.GetComponent<penLine_script>().centroid.x,
					templine.GetComponent<penLine_script>().maxy, 0);   // z is already -40, hence putting 0 here in local.

				templine.transform.GetChild(3).GetComponent<RectTransform>().sizeDelta = new Vector2(20, 20);

				templine.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = "";

				templine.transform.GetChild(3).gameObject.SetActive(false); // turn it off now. Needed only for double function.

				// Finally, check membership. Does this belong to any set or function?
				templine.GetComponent<penLine_script>().checkAndUpdateMembership();

				// set templine to null, otherwise, if an existing touch from color picker makes it to the canvas,
				// then the object jumps to the color picker (as a new templine hasn't been initialized from touch.begin).
				templine = null;
			}

		}
		#endregion

		// SET BRUSH
		#region set
		if (select_button.GetComponent<AllButtonsBehavior>().selected)
		{

			if (PenTouchInfo.PressedThisFrame) //currentPen.tip.wasPressedThisFrame) // (Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			{
				// start drawing a new line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					totalLines++;
					templine = Instantiate(SetLine, vec, Quaternion.identity, this.gameObject.transform);
					//templine.GetComponent<TrailRenderer>().material.color = Color.black;

					//templine.transform.localScale = new Vector3(1f, 1f, 1f);

					templine.name = "Set_" + totalLines.ToString();
					templine.tag = "set";

					templine.GetComponent<setLine_script>().points.Add(vec);

					//templine.GetComponent<TrailRenderer>().transform.position = vec + new Vector3(0.0001f, 0.0001f, 0f);

					// disable the argument_label button
					templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);

					// add to history
					history.Add(templine);
				}
			}

			else if (PenTouchInfo.PressedNow && templine != null) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved && templine != null)
			{
				// add points to the last line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;

					templine.GetComponent<TrailRenderer>().transform.position = vec;
					templine.GetComponent<setLine_script>().points.Add(vec);

				}
			}

			else if (PenTouchInfo.ReleasedThisFrame && templine != null) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended && templine != null)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{
					if (templine.GetComponent<setLine_script>().points.Count > 20)
					{
						// set the gesture/legible layer fields: set previous_gestureType to initiate change in updateLegibleLayer().
						templine.GetComponent<setLine_script>().previous_gestureType = "";
						templine.GetComponent<setLine_script>().updateGestureType = true;

						// this one is needed for the setline script functions
						templine.GetComponent<setLine_script>().paintable_object =
							GameObject.FindGameObjectWithTag("paintable_canvas_object");

						//templine.GetComponent<LineRenderer>().useWorldSpace = false;
						templine.GetComponent<LineRenderer>().positionCount = templine.GetComponent<setLine_script>().points.Count;
						templine.GetComponent<LineRenderer>().SetPositions(templine.GetComponent<setLine_script>().points.ToArray());

						// compute centroid and bounds
						templine.GetComponent<setLine_script>().computeCentroid();
						templine.GetComponent<setLine_script>().computeBounds();

						//Debug.Log(templine.GetComponent<LineRenderer>().positionCount);
						templine.GetComponent<LineRenderer>().Simplify(1.2f);
						Vector3[] pts = new Vector3[templine.GetComponent<LineRenderer>().positionCount];
						templine.GetComponent<LineRenderer>().GetPositions(pts);
						templine.GetComponent<setLine_script>().points = new List<Vector3>(pts);
						//Debug.Log(templine.GetComponent<LineRenderer>().positionCount);

						// set transform position
						templine.transform.position = new Vector3(templine.GetComponent<setLine_script>().centroid.x,
							templine.GetComponent<setLine_script>().centroid.y, 0);

						// now transform all points in the line script to local positions
						templine.GetComponent<setLine_script>().fromGlobalToLocalPoints();

						//Debug.Log("transform position: " + templine.transform.position.ToString());

						// bake into mesh: create a new child object. Direct mesh addition in the Set messes up the transform of the mesh
						// for some reason.
						GameObject meshObj = new GameObject("_meshobj");
						meshObj.AddComponent<MeshFilter>();
						//meshObj.AddComponent<BoxCollider>();
						meshObj.AddComponent<MeshRenderer>();
						meshObj.transform.SetParent(templine.transform);

						var lineRenderer = templine.GetComponent<LineRenderer>();
						var meshFilter = meshObj.GetComponent<MeshFilter>();
						Mesh mesh = new Mesh();
						lineRenderer.BakeMesh(mesh, true);
						meshFilter.sharedMesh = mesh;

						var meshRenderer = meshObj.GetComponent<MeshRenderer>();
						//meshRenderer.sharedMaterial = templine.GetComponent<Material>();
						meshRenderer.sharedMaterial = set_line_material;

						// get rid of the line renderer?
						templine.GetComponent<LineRenderer>().enabled = false;

						// set anchor position now, after gameobject transform. Order matters.
						templine.transform.GetChild(0).position =
							templine.transform.TransformPoint(
								templine.transform.GetComponent<setLine_script>().points[
									templine.GetComponent<setLine_script>().LowestMeshPointIndex()]
							);
						templine.transform.GetChild(0).position += new Vector3(0, 0, -40f); // set z separately
						//new Vector3(templine.GetComponent<setLine_script>().centroid.x, templine.GetComponent<setLine_script>().miny, -40f);

						// remember the anchor offset
						templine.GetComponent<setLine_script>().calculateAnchorOffset();

						// set the properties of the draggable anchor
						templine.transform.GetChild(0).Rotate(new Vector3(90, 0, 0));

						templine.transform.GetChild(0).localScale = new Vector3(100f, 10f, 100f) / 2f;

						// disable the anchor collider for now for free drawing of next objects. Pan mode enables the collider back.
						templine.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;

						// create a material instance
						templine.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = templine.transform.GetChild(0).GetComponent<MeshRenderer>().materials[0];

						//	------- WHY DOESN'T THIS ROTATION WORK FROM INSIDE THE SCRIPT???? --------
						//templine.transform.GetChild(0).GetChild(0).transform.Rotate(new Vector3(90, 0, 0));
						//templine.transform.GetChild(0).Find("_text").transform.Rotate(new Vector3(90, 0, 0));

						// The following takes care of some details based on where the anchor is, to move the text ahead and make it visible
						templine.transform.GetChild(0).Find("_text").transform.localPosition = new Vector3(0f, -1.5f, 0f);
						templine.transform.GetChild(0).Find("_text").GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;

						// set up the TeX legible layer text box
						templine.transform.GetChild(0).Find("_TeX").transform.position = templine.transform.position;
							//templine.GetComponent<setLine_script>().centroid;

						/*
						 * // Fit the sizeDelta within bounding box
						*/
						float bxdiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.x -
							templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.x;
						float bydiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.y -
							templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.y;
						float range1param = Mathf.InverseLerp(30f, 300f, bxdiff);
						//float range1param = Mathf.Lerp(30f, 300f, bxdiff);
						float sd = Mathf.LerpUnclamped(1, 5, range1param); // possible values for sizeDelta = [1, 5].
						templine.transform.GetChild(0).Find("_TeX").GetComponent<RectTransform>().sizeDelta = new Vector2(sd, sd);

						// turn off the text component until abstraction slider asks for the legible layer
						templine.transform.GetChild(0).Find("_TeX").GetComponent<TEXDraw>().enabled = false;

						// disable trail renderer, no longer needed
						templine.GetComponent<TrailRenderer>().enabled = false;

						// Set up the argument_label button and text.
						templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);

						templine.transform.GetChild(0).GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(0.5f, 0.5f);
						templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0.5f, 0.5f);   // TMP text
						templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
							templine.GetComponent<setLine_script>().this_set_name;

						// set the argument label position at the highest (max y) of points. Use global position (TransformPoint)
						templine.transform.GetChild(0).GetChild(2).transform.position =
							templine.transform.TransformPoint(
								templine.transform.GetComponent<setLine_script>().points[
							templine.GetComponent<setLine_script>().HighestMeshPointIndex()]
							);

						templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);

						// check what pencil lines were included when drawing the set, and make them children of this set
						// find all game objects under Paintable canvas
						// the pen lines should be made a child after the transform position of the parent is calculated.
						// otherwise, there might be unwanted offset.

						GameObject[] penLines = GameObject.FindGameObjectsWithTag("penline");

						// transform.childcount changes dynamically as we change the children's parent to a set,
						// so don't use it in an increasing for-loop: it ends up leaving out alternate elements
						//for (int i = transform.childCount - 1; i > -1; i--)
						for(int i = 0; i < penLines.Length; i++)
						{
							// find all pen lines
							//if (transform.GetChild(i).name.Contains("penLine_"))
							//{
								// check if the lines are inside the drawn set polygon -- in respective local coordinates
								if (templine.GetComponent<setLine_script>().isInsidePolygon(
									templine.GetComponent<setLine_script>().transform.InverseTransformPoint(
									//transform.GetChild(i).transform.position)
									penLines[i].transform.position)
									))
								{
								//transform.GetChild(i).SetParent(templine.transform);
								penLines[i].transform.SetParent(templine.transform);
								//Debug.Log("parent found");

							}
							//}
						}

						// update the feature and text on the anchor
						templine.GetComponent<setLine_script>().updateFeature();

						// check and update membership
						templine.GetComponent<setLine_script>().checkAndUpdateMembership();

						// if it got included inside a function, then keep track in the function. Needed for action based division recognition.

						templine = null;

					}
					else
					{
						// delete the templine, not enough points
						Destroy(templine);
					}
				}

			}

		}

		#endregion

		// FUNCTION BRUSH
		#region function
		if (function_button.GetComponent<AllButtonsBehavior>().selected)
		{

			if (PenTouchInfo.PressedThisFrame)//currentPen.tip.wasPressedThisFrame) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			{
				// start drawing a new line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					totalLines++;
					templine = Instantiate(FunctionLine, vec, Quaternion.identity, this.gameObject.transform);
					//templine.GetComponent<TrailRenderer>().material.color = Color.black;

					//templine.transform.localScale = new Vector3(1f, 1f, 1f);

					templine.name = "Function_" + totalLines.ToString();
					templine.tag = "function";

					templine.GetComponent<functionLine_script>().points.Add(vec);

					//templine.GetComponent<TrailRenderer>().transform.position = vec + new Vector3(0.0001f, 0.0001f, 0f);

					// disable the argument_label button
					templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);

					// add to history
					history.Add(templine);
				}
			}

			else if (PenTouchInfo.PressedNow && templine != null) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved && templine != null)
			{
				// add points to the last line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;

					templine.GetComponent<TrailRenderer>().transform.position = vec;
					templine.GetComponent<functionLine_script>().points.Add(vec);

				}
			}

			else if (PenTouchInfo.ReleasedThisFrame && templine != null) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended && templine != null)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{
					if (templine.GetComponent<functionLine_script>().points.Count > 20)
					{
						// set the gesture/legible layer fields: set previous_gestureType to initiate change in updateLegibleLayer().
						templine.GetComponent<functionLine_script>().previous_gestureType = "";
						templine.GetComponent<functionLine_script>().updateGestureType = true;

						// this one is needed for the setline script functions
						templine.GetComponent<functionLine_script>().paintable_object =
							GameObject.FindGameObjectWithTag("paintable_canvas_object");

						//templine.GetComponent<LineRenderer>().useWorldSpace = false;
						templine.GetComponent<LineRenderer>().positionCount = templine.GetComponent<functionLine_script>().points.Count;
						templine.GetComponent<LineRenderer>().SetPositions(templine.GetComponent<functionLine_script>().points.ToArray());

						// compute centroid and bounds
						templine.GetComponent<functionLine_script>().computeCentroid();
						templine.GetComponent<functionLine_script>().computeBounds();

						//Debug.Log(templine.GetComponent<LineRenderer>().positionCount);
						templine.GetComponent<LineRenderer>().Simplify(1.2f);
						Vector3[] pts = new Vector3[templine.GetComponent<LineRenderer>().positionCount];
						templine.GetComponent<LineRenderer>().GetPositions(pts);
						templine.GetComponent<functionLine_script>().points = new List<Vector3>(pts);
						//Debug.Log(templine.GetComponent<LineRenderer>().positionCount);

						// set transform position
						templine.transform.position = new Vector3(templine.GetComponent<functionLine_script>().centroid.x,
							templine.GetComponent<functionLine_script>().centroid.y, 0);

						// now transform all points in the line script to local positions
						templine.GetComponent<functionLine_script>().fromGlobalToLocalPoints();

						//Debug.Log("transform position: " + templine.transform.position.ToString());

						// bake into mesh: create a new child object. Direct mesh addition in the Set messes up the transform of the mesh
						// for some reason.
						GameObject meshObj = new GameObject("_meshobj");
						meshObj.AddComponent<MeshFilter>();
						//meshObj.AddComponent<BoxCollider>();
						meshObj.AddComponent<MeshRenderer>();
						meshObj.transform.SetParent(templine.transform);

						var lineRenderer = templine.GetComponent<LineRenderer>();
						var meshFilter = meshObj.GetComponent<MeshFilter>();
						Mesh mesh = new Mesh();
						lineRenderer.BakeMesh(mesh, true);
						meshFilter.sharedMesh = mesh;

						var meshRenderer = meshObj.GetComponent<MeshRenderer>();
						//meshRenderer.sharedMaterial = templine.GetComponent<Material>();
						meshRenderer.sharedMaterial = function_line_material;

						// tried changing the shader properties, doesn't work. need to check back later.
						//int repcountid = templine.transform.GetChild(1).GetComponent<MeshRenderer>().materials[0].shader.FindPropertyIndex("Repeat Count");
						//templine.transform.GetChild(1).GetComponent<MeshRenderer>().materials[0].SetFloat(repcountid, 2f);

						//Debug.Log(templine.transform.GetChild(1).GetComponent<MeshRenderer>().materials[0].GetFloat(repcountid));

						// get rid of the line renderer?
						templine.GetComponent<LineRenderer>().enabled = false;

						// set anchor position now, after gameobject transform. Order matters.
						templine.transform.GetChild(0).position =
							templine.transform.TransformPoint(
								templine.transform.GetComponent<functionLine_script>().points[
									templine.GetComponent<functionLine_script>().LowestMeshPointIndex()]
							);
						templine.transform.GetChild(0).position += new Vector3(0, 0, -40f); // set z separately

						// remember the anchor offset
						templine.GetComponent<functionLine_script>().calculateAnchorOffset();

						// set the properties of the draggable anchor -- children objects and components are affected too.
						templine.transform.GetChild(0).Rotate(new Vector3(90, 0, 0));
						// scale it so it's a tapered in the horizontal direction (to accommodate the text)
						templine.transform.GetChild(0).localScale = new Vector3(200f, 10f, 100f) / 2f; // prev. value: new Vector3(100f, 10f, 100f) / 2f

						// turn off the anchor box collider for now. Pan mode enables it back.
						templine.transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;

						// create a material instance
						templine.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = templine.transform.GetChild(0).GetComponent<MeshRenderer>().materials[0];

						//	------- WHY DOESN'T THIS ROTATION WORK FROM INSIDE THE SCRIPT???? --------
						//templine.transform.GetChild(0).GetChild(0).transform.Rotate(new Vector3(90, 0, 0));
						//templine.transform.GetChild(0).Find("_text").transform.Rotate(new Vector3(90, 0, 0));

						// The following takes care of some details based on where the anchor is, to move the text ahead and make it visible
						templine.transform.GetChild(0).Find("_anchor_TeX").transform.localPosition = new Vector3(0f, -1.5f, 0f);
						//templine.transform.GetChild(0).Find("_text").GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;
						// undo the horizontal tapered scaling for the text rect transform
						templine.transform.GetChild(0).Find("_anchor_TeX").transform.localScale = new Vector3(0.005f, 0.01f, 0.005f);

						// set text container size wrt anchor
						//templine.transform.GetChild(0).Find("_text").GetComponent<TextMeshProUGUI>().GetComponent<TextContainer>().width = 

						// set up the TeX legible layer text box
						templine.transform.GetChild(0).Find("_TeX").transform.position =
							templine.GetComponent<functionLine_script>().centroid;

						/*
						 * // Fit the sizeDelta within bounding box
						*/

						float bxdiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.x -
							templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.x;
						float bydiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.y -
							templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.y;
						float range1param = Mathf.InverseLerp(30f, 300f, bxdiff);
						//float range1param = Mathf.Lerp(30f, 300f, bxdiff);
						float sd = Mathf.LerpUnclamped(5, 20, range1param);	// possible values for sizeDelta = [1, 5].
						templine.transform.GetChild(0).Find("_TeX").GetComponent<RectTransform>().sizeDelta = new Vector2(sd, sd);

						// turn off the text component until abstraction slider asks for the legible layer
						templine.transform.GetChild(0).Find("_TeX").GetComponent<TEXDraw>().enabled = false;

						// disable trail renderer, no longer needed
						templine.GetComponent<TrailRenderer>().enabled = false;

						// Set up the argument_label button and text.
						templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);

						templine.transform.GetChild(0).GetChild(2).transform.localScale = new Vector3(0.3f, 0.6f, 1f);	// undo the scale of anchor, and make it smaller too.
						templine.transform.GetChild(0).GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(0.6f, 0.6f);
						templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0.3f, 0.3f);   // TMP text
						templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
							templine.GetComponent<functionLine_script>().currentOperator();

						templine.transform.GetChild(0).GetChild(2).position =
							templine.transform.TransformPoint(
								templine.transform.GetComponent<functionLine_script>().points[
									templine.GetComponent<functionLine_script>().HighestMeshPointIndex()]
							);
						templine.transform.GetChild(0).GetChild(2).position += new Vector3(0, 0, -40f); // set z separately

						// set up the _top_TeX label with category filters
						templine.transform.GetChild(0).GetChild(3).position =
							templine.transform.TransformPoint(
								templine.transform.GetComponent<functionLine_script>().points[
									templine.GetComponent<functionLine_script>().HighestMeshPointIndex()]);

						templine.transform.GetChild(0).GetChild(3).localPosition += new Vector3(0, 0, -0.3f);	// move slightly upwards locally, z is y b/c of anchor rotation
						templine.transform.GetChild(0).GetChild(3).localScale = new Vector3(0.005f, 0.01f, 1); // undo the scale of anchor, and make it smaller too.

						templine.transform.GetChild(0).GetChild(3).GetComponent<TEXDraw>().text = "(), " + templine.name + ", \\legbox";

						// check what pencil lines were included when drawing the set, and make them children of this set
						// find all game objects under Paintable canvas
						// the pen lines should be made a child after the transform position of the parent is calculated.
						// otherwise, there might be unwanted offset.

						// transform.childcount changes dynamically as we change the children's parent to a set,
						// so don't use it in an increasing for-loop: it ends up leaving out alternate elements
						for (int i = transform.childCount - 1; i > -1; i--)
						{
							// find all sets/containers
							if (transform.GetChild(i).tag == "set")
							{
								// check if they are inside the drawn function polygon
								if (templine.GetComponent<functionLine_script>().isInsidePolygon(
									templine.GetComponent<functionLine_script>().transform.InverseTransformPoint(
									transform.GetChild(i).GetChild(0).transform.position)	// check if the anchor falls inside the function lasso. NOT the default transform.position.
									))
								{
									transform.GetChild(i).SetParent(templine.transform);
									//Debug.Log("parent found");
								}
							}

							// find all functions
							else if (transform.GetChild(i).tag == "function")
							{
								// check if they are inside the drawn function polygon
								if (templine.GetComponent<functionLine_script>().isInsidePolygon(
									templine.GetComponent<functionLine_script>().transform.InverseTransformPoint(
									transform.GetChild(i).GetChild(0).transform.position)   // check if the anchor falls inside the function lasso. NOT the default transform.position.
									))
								{
									transform.GetChild(i).SetParent(templine.transform);
									//Debug.Log("parent found");
								}
							}

							// find all penlines
							else if (transform.GetChild(i).tag == "penline")
							{
								// check if they are inside the drawn function polygon
								if (templine.GetComponent<functionLine_script>().isInsidePolygon(
									templine.GetComponent<functionLine_script>().transform.InverseTransformPoint(
									transform.GetChild(i).GetComponent<penLine_script>().transform.position)
									))
								{
									transform.GetChild(i).SetParent(templine.transform);
									//Debug.Log("parent found");
								}
							}
						}

						// update the feature and text on the anchor
						templine.GetComponent<functionLine_script>().updateFeature();

						templine = null;
					}
					else
					{
						// delete the templine, not enough points
						Destroy(templine);
					}
				}

			}

		}

		#endregion


		// PAN BUTTON FUNCTIONALITY -- MOVE OBJECTS WITH SINGLE TAP, ZOOM IN OR OUT AND MOVE CANVAS WITH DOUBLE FINGERS
		// DOUBLE FINGER
		#region pan zoom (and tap delegation)

		if (activeTouches.Count == 2 && !panZoomLocked) // && pan_button.GetComponent<PanButtonBehavior>().selected)
		{
			// disable the set or function rotate capability for now.

			/*
			// DO A RAYCAST TO CHECK IF AT LEAST ONE FINGER FELL ON A SET OR FUNCTION ANCHOR
			Vector2 touch1 = Input.GetTouch(0).position;
			Vector2 touch2 = Input.GetTouch(1).position;
			Vector2 gradient;
			//double_touch_rotate = true;
			//if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Ended) double_touch_rotate = false;
			var ray1 = Camera.main.ScreenPointToRay(touch1);
			var ray2 = Camera.main.ScreenPointToRay(touch2);
			RaycastHit Hit1, Hit2;
			if (Physics.Raycast(ray1, out Hit1) && 
				(Hit1.collider.transform.parent.tag == "set" || Hit1.collider.transform.parent.tag == "function"))
			{
				Transform touchedSet = Hit1.collider.transform.parent;
				if(touchedSet.GetChild(0).GetChild(2).GetComponent<ArgumentLabelButtonBehavior>().buttonPressed)
				{
					//Debug.Log("hit object");
					gradient = (touch1 - touch2).normalized;
					// angle difference with (0,1) vector
					float angle = Vector2.SignedAngle(new Vector2(0f, 1f), gradient);
					angle -= Hit1.collider.transform.parent.transform.eulerAngles.z;
					Hit1.collider.transform.parent.transform.RotateAround(Hit1.collider.transform.position, new Vector3(0, 0, 1), angle);
					//Hit.collider.transform.parent.transform.Rotate(new Vector3(0, 0, 1), angle, Space.World);
				}
			}
			else if (Physics.Raycast(ray2, out Hit2) &&
				(Hit2.collider.transform.parent.tag == "set" || Hit2.collider.transform.parent.tag == "function"))
			{
				//Debug.Log("hit ray2");
				gradient = (touch2 - touch1).normalized;
				// angle difference with (0,1) vector
				float angle = Vector2.SignedAngle(new Vector2(0f, 1f), gradient);
				angle -= Hit2.collider.transform.parent.transform.eulerAngles.z;
				Hit2.collider.transform.parent.transform.RotateAround(Hit2.collider.transform.position, new Vector3(0, 0, 1), angle);
			}
			else
			{

			*/
			// NO ANCHOR TAPPED, JUST ZOOM IN/PAN
			//main_camera.GetComponent<MobileTouchCamera>().enabled = true;

			UnityEngine.InputSystem.EnhancedTouch.Touch touchzero = activeTouches[0];
			UnityEngine.InputSystem.EnhancedTouch.Touch touchone = activeTouches[1];

			Vector2 touchzeroprevpos = touchzero.screenPosition - touchzero.delta;
			Vector2 touchoneprevpos = touchone.screenPosition - touchone.delta;

			float prevmag = (touchzeroprevpos - touchoneprevpos).magnitude;
			float currmag = (touchzero.screenPosition - touchone.screenPosition).magnitude;

			float difference = currmag - prevmag;

			//Debug.Log("diff: " + difference + ", multiplier: " + zoom_multiplier + 
				//", camera size: " + (Camera.main.orthographicSize - zoom_multiplier * difference).ToString());
			//Debug.Log("Clamped: " + Mathf.Clamp(zoom_mulitplier * (Camera.main.orthographicSize - difference), zoom_min, zoom_max));

			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - zoom_multiplier * difference, zoom_min, zoom_max);

			// CHECK AND DELETE INCOMPLETE LINES
			deleteTempLineIfDoubleFinger();

				// show zoom percentage text
				// Assuming these parameters for orthographic size: min: 200, max: 500
				int zoom = (int)( (1f - ((main_camera.orthographicSize - zoom_min) / zoom_max) ) * 100f);
				GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text =
					zoom.ToString("F0") + "%";
			//}

		}
		else if (activeTouches.Count == 1 && !panZoomLocked)
		{

			// Only pan when the touch is on top of the canvas. Otherwise,
			var ray = Camera.main.ScreenPointToRay(activeTouches[0].screenPosition);
			RaycastHit Hit;

			if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
			{

				// EnhanchedTouch acts weirdly in the sense that the Start phase is not detected many times,
				// only Moved, Stationary, and Ended phases are detected most of the times.
				// So, introducing a bool that will only update the start pan position if a touch ended before.

				if (activeTouches[0].phase == UnityEngine.InputSystem.TouchPhase.Ended)
				{
					previousTouchEnded = true;
				}

				//if (touchScreen.touches[0].phase.ReadValue() == UnityEngine.InputSystem.TouchPhase.Began)
				else if (activeTouches[0].phase == UnityEngine.InputSystem.TouchPhase.Moved && previousTouchEnded && okayToPan)
				{
					panTouchStart = Camera.main.ScreenToWorldPoint(activeTouches[0].screenPosition);
					//Debug.Log("touch start: " + panTouchStart.ToString());

					previousTouchEnded = false;
				}

				//else if (touchScreen.touches[0].phase.ReadValue() == UnityEngine.InputSystem.TouchPhase.Moved)
				else if (activeTouches[0].phase == UnityEngine.InputSystem.TouchPhase.Moved && !previousTouchEnded && okayToPan)
				{
					Vector2 panDirection = panTouchStart - (Vector2)Camera.main.ScreenToWorldPoint(activeTouches[0].screenPosition);
					Camera.main.transform.position += (Vector3)panDirection;
				}
			}

			// if not panning, check for tap
			if (activeTouches[0].isTap)
			{
				// delegate single tap to short tap function
				//Debug.Log("tap detected.");
				OnShortTap(activeTouches[0].screenPosition);
				OnLongTap(activeTouches[0].screenPosition);
			}
		}
		else
		{
			previousTouchEnded = true;
			//main_camera.GetComponent<MobileTouchCamera>().enabled = false;
			GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text = "";
		}

		// PEN TIP ACCUMULATION BASED TAP DETECTION
		// Only detect tap in pan mode, stroke conversion mode
		if (pan_button.GetComponent<AllButtonsBehavior>().selected || strokeconversion_button.GetComponent<AllButtonsBehavior>().selected)
		{
			if (PenTouchInfo.PressedThisFrame) { penPressElapsed = 0f; }
			else if (PenTouchInfo.PressedNow) { penPressElapsed += Time.deltaTime; }
			else if (PenTouchInfo.ReleasedThisFrame)
			{
				if (penPressElapsed < 0.3f)
				{
					OnShortTap(PenTouchInfo.penPosition);
					OnLongTap(PenTouchInfo.penPosition);

					penPressElapsed = 0f;
				}
			}
		}

		#endregion

		// EDGE LINE
		#region edge
		if (edge_button.GetComponent<AllButtonsBehavior>().selected && edge_button.GetComponent<AllButtonsBehavior>().isCausalEdge)
		{
			if (PenTouchInfo.PressedThisFrame)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			{
				// start drawing a new line if it originated from a set or function anchor
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && (Hit.collider.transform.parent.tag == "set" ||
					Hit.collider.transform.parent.tag == "function"))
				{
					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;

					totalLines++;
					templine = Instantiate(EdgeLinePrefab, vec, Quaternion.identity, this.gameObject.transform);
					templine.GetComponent<TrailRenderer>().material.color = Color.black;

					templine.name = "edgeLine_" + totalLines.ToString();
					templine.tag = "edgeline";

					templine.GetComponent<edgeLine_script>().points.Add(vec);

					// assign the edge start object -- parent of the anchor
					edge_start = Hit.collider.transform.parent.gameObject;

					// for all penline objects, create an anchor on top of them for a possible edge end
					GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
					for (int i = 0; i < penobjs.Length; i++)
					{
						// if the pen objects are visible through abstraction layer, then show a blue dot on top to connect the edge.
						if (penobjs[i].GetComponent<penLine_script>().abstraction_layer == 1)
						{
							GameObject tempcyl = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
							tempcyl.tag = "temp_edge_primitive";
							tempcyl.transform.position = penobjs[i].transform.position + new Vector3(0f, 0f, -3f);
							tempcyl.transform.localScale = new Vector3(20f, 20f, 20f);
							tempcyl.transform.Rotate(new Vector3(90f, 0f, 0f));
							tempcyl.transform.parent = penobjs[i].transform;
							tempcyl.GetComponent<Renderer>().material.color = Color.blue;
						}
					}

					// set up the line renderer
					templine.GetComponent<LineRenderer>().positionCount = 2;
					templine.GetComponent<LineRenderer>().SetPosition(0, vec + new Vector3(0, 0, -2f));
					templine.GetComponent<LineRenderer>().SetPosition(1, vec + new Vector3(1f, 0, -2f));

				}
			}

			else if (PenTouchInfo.PressedNow)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved)
			{
				// add points to the last line, but check that if an edge line has been created already
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object"
					&& edge_start != null)
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;


					//templine.GetComponent<TrailRenderer>().transform.position = vec;

					templine.GetComponent<LineRenderer>().SetPosition(1, vec + new Vector3(0, 0, -2f));

					templine.GetComponent<edgeLine_script>().points.Add(vec);
				}
			}

			else if (PenTouchInfo.ReleasedThisFrame)//currentPen.tip.wasReleasedThisFrame)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "temp_edge_primitive"
					&& edge_start != null)
				{
					if (templine.GetComponent<edgeLine_script>().points.Count > 2)
					{
						// assign the end of edge object
						edge_end = Hit.collider.transform.parent.gameObject;

						// compute centroid and bounds
						templine.GetComponent<edgeLine_script>().computeCentroid();
						templine.GetComponent<edgeLine_script>().computeBounds();

						// now create the edge in edge script
						if (edge_end != null && edge_start != null)
						{
							// TODO: IF AN EDGELINE ALREADY EXISTS CONNECTING THESE TWO NODES, THEN DON'T CREATE ANOTHER ONE, DESTROY THE CURRENT.

							templine.GetComponent<edgeLine_script>().source_object = edge_start;
							templine.GetComponent<edgeLine_script>().target_object = edge_end;

							templine.GetComponent<edgeLine_script>().edge_set = true;

							// set line renderer end point
							templine.GetComponent<LineRenderer>().SetPosition(1, edge_end.transform.position);

							// assuming edge_start is always an anchor
							var edgepoints = new List<Vector3>() { templine.GetComponent<LineRenderer>().GetPosition(0),
								templine.GetComponent<LineRenderer>().GetPosition(1)};

							templine.GetComponent<EdgeCollider2D>().points = edgepoints.Select(x =>
							{
								var pos = templine.GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
								return new Vector2(pos.x, pos.y);
							}).ToArray();

							templine.GetComponent<EdgeCollider2D>().edgeRadius = 10;

							// set up TEXDraw text
							templine.transform.GetChild(0).GetComponent<TEXDraw>().GetComponent<RectTransform>().sizeDelta = new Vector2(40, 30);

							templine.GetComponent<edgeLine_script>().updateTEXLabel();

							//Vector2[] edgepts = { edge_start.transform.position, edge_end.transform.position };
							//templine.GetComponent<EdgeCollider2D>().points = edgepts;

							// set line renderer texture scale
							var linedist = Vector3.Distance(templine.GetComponent<LineRenderer>().GetPosition(0),
								templine.GetComponent<LineRenderer>().GetPosition(1));
							templine.GetComponent<LineRenderer>().materials[0].mainTextureScale = new Vector2(linedist, 1);


							// set edge_end and edge_start back to null
							edge_end = null;
							edge_start = null;
						}

					}
					else
					{
						// delete the templine, not enough points
						Destroy(templine);
						edge_start = null;
						edge_end = null;
					}
				}

				// in case a touch was rendered on the canvas (or not on the blue cylinders) and a line didn't finish drawing from source
				else if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag != "temp_edge_primitive"
					&& edge_start != null)
				{
					// delete the templine, not correctly drawn from feasible source to target
					Destroy(templine);
					edge_start = null;
					edge_end = null;
				}

				// in all other cases, to be safe, just delete the entire edgeline structure
				else if (edge_start != null)
				{
					Destroy(templine);
					edge_start = null;
					edge_end = null;
				}

				// the touch has ended, destroy all temp edge cylinders now
				GameObject[] tempcyls = GameObject.FindGameObjectsWithTag("temp_edge_primitive");
				for (int k = 0; k < tempcyls.Length; k++)
				{
					Destroy(tempcyls[k]);
				}
			}
		}
		#endregion

		// SPAWN EDGE LINE
		#region spawn edge
		if (edge_button.GetComponent<AllButtonsBehavior>().selected && !edge_button.GetComponent<AllButtonsBehavior>().isCausalEdge)
		{
			if (PenTouchInfo.PressedThisFrame)//currentPen.tip.wasPressedThisFrame)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			{
				// start drawing a new line if it originated from a set or function anchor
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && (Hit.collider.transform.parent.tag == "set" ||
					Hit.collider.transform.parent.tag == "function"))
				{
					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;

					totalLines++;
					templine = Instantiate(SpawnEdgePrefab, vec, Quaternion.identity, this.gameObject.transform);

					templine.name = "spawnEdge_" + totalLines.ToString();
					templine.tag = "spawn_edge";

					templine.GetComponent<SpawnEdgeScript>().points.Add(vec);

					// assign the edge start object -- parent of the anchor
					edge_start = Hit.collider.transform.parent.gameObject;

					// for all penline objects, create an anchor on top of them for a possible edge end
					GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
					for (int i = 0; i < penobjs.Length; i++)
					{
						// if the pen objects are visible through abstraction layer, then show a blue dot on top to connect the edge.
						if (penobjs[i].GetComponent<penLine_script>().abstraction_layer == 1)
						{
							GameObject tempcyl = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
							tempcyl.tag = "temp_edge_primitive";
							tempcyl.transform.position = penobjs[i].transform.position + new Vector3(0f, 0f, -3f);
							tempcyl.transform.localScale = new Vector3(20f, 20f, 20f);
							tempcyl.transform.Rotate(new Vector3(90f, 0f, 0f));
							tempcyl.transform.parent = penobjs[i].transform;
							tempcyl.GetComponent<Renderer>().material.color = Color.blue;
						}
					}

					// set up the line renderer
					templine.GetComponent<LineRenderer>().positionCount = 2;
					templine.GetComponent<LineRenderer>().SetPosition(0, vec + new Vector3(0, 0, -2f));
					templine.GetComponent<LineRenderer>().SetPosition(1, vec + new Vector3(1f, 0, -2f));

				}
			}

			else if (PenTouchInfo.PressedNow)//currentPen.tip.isPressed)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved)
			{
				// add points to the last line, but check that if an edge line has been created already
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object"
					&& edge_start != null)
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;


					//templine.GetComponent<TrailRenderer>().transform.position = vec;

					templine.GetComponent<LineRenderer>().SetPosition(1, vec + new Vector3(0, 0, -2f));

					templine.GetComponent<SpawnEdgeScript>().points.Add(vec);
				}
			}

			else if (PenTouchInfo.ReleasedThisFrame)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "temp_edge_primitive"
					&& edge_start != null)
				{
					if (templine.GetComponent<SpawnEdgeScript>().points.Count > 2)
					{
						// assign the end of edge object
						edge_end = Hit.collider.transform.parent.gameObject;

						// now create the edge in edge script
						if (edge_end != null && edge_start != null)
						{
							// TODO: IF AN EDGELINE ALREADY EXISTS CONNECTING THESE TWO NODES, THEN DON'T CREATE ANOTHER ONE, DESTROY THE CURRENT.

							templine.GetComponent<SpawnEdgeScript>().source_object = edge_start;
							templine.GetComponent<SpawnEdgeScript>().target_object = edge_end;

							templine.GetComponent<SpawnEdgeScript>().edge_set = true;

							// set line renderer end point
							templine.GetComponent<LineRenderer>().SetPosition(1, edge_end.transform.position);

							// assuming edge_start is always an anchor
							var edgepoints = new List<Vector3>() { templine.GetComponent<LineRenderer>().GetPosition(0),
								templine.GetComponent<LineRenderer>().GetPosition(1)};

							templine.GetComponent<EdgeCollider2D>().points = edgepoints.Select(x =>
							{
								var pos = templine.GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
								return new Vector2(pos.x, pos.y);
							}).ToArray();

							templine.GetComponent<EdgeCollider2D>().edgeRadius = 10;

							

							// set line renderer texture scale
							var linedist = Vector3.Distance(templine.GetComponent<LineRenderer>().GetPosition(0),
								templine.GetComponent<LineRenderer>().GetPosition(1));
							templine.GetComponent<LineRenderer>().materials[0].mainTextureScale = new Vector2(linedist, 1);


							// set edge_end and edge_start back to null
							edge_end = null;
							edge_start = null;
						}

					}
					else
					{
						// delete the templine, not enough points
						Destroy(templine);
						edge_start = null;
						edge_end = null;
					}
				}

				// in case a touch was rendered on the canvas (or not on the blue cylinders) and a line didn't finish drawing from source
				else if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag != "temp_edge_primitive"
					&& edge_start != null)
				{
					// delete the templine, not correctly drawn from feasible source to target
					Destroy(templine);
					edge_start = null;
					edge_end = null;
				}

				// in all other cases, to be safe, just delete the entire edgeline structure
				else if (edge_start != null)
				{
					Destroy(templine);
					edge_start = null;
					edge_end = null;
				}

				// the touch has ended, destroy all temp edge cylinders now
				GameObject[] tempcyls = GameObject.FindGameObjectsWithTag("temp_edge_primitive");
				for (int k = 0; k < tempcyls.Length; k++)
				{
					Destroy(tempcyls[k]);
				}
			}
		}
		#endregion

		// ERASER BRUSH
		#region eraser
		if (PenTouchInfo.PressedNow && eraser_button.GetComponent<AllButtonsBehavior>().selected)
		{
			var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);

			RaycastHit Hit;
			RaycastHit2D hit2d;

			if (Physics.Raycast(ray, out Hit))
			{
				// handle individually -- in case a type requires special treatment or extra code in the future
				// delete edges which has ties to erased nodes (sets, penlines, functions etc.)

				// set anchor
				if (Hit.collider.gameObject.transform.parent.tag == "set")
				{
					string possible_edge_node_name = Hit.collider.gameObject.transform.parent.name;
					searchNodeAndDeleteEdge(possible_edge_node_name);
					Destroy(Hit.collider.gameObject.transform.parent.gameObject);
				}
				// pen line
				else if (Hit.collider.gameObject.tag == "penline")
				{
					string possible_edge_node_name = Hit.collider.gameObject.name;
					searchNodeAndDeleteEdge(possible_edge_node_name);
					Destroy(Hit.collider.gameObject);
				}
				// function anchor
				else if (Hit.collider.gameObject.transform.parent.tag == "function")
				{
					string possible_edge_node_name = Hit.collider.gameObject.transform.parent.name;
					searchNodeAndDeleteEdge(possible_edge_node_name);
					Destroy(Hit.collider.gameObject.transform.parent.gameObject);
				}
				// edgeline -- the 3D check doesn't work, added an additional 2D check below for edge collider
				else if (Hit.collider.gameObject.tag == "edgeline")
				{
					Destroy(Hit.collider.gameObject);
				}
			}

			hit2d = Physics2D.GetRayIntersection(ray);
			if (hit2d.collider != null && hit2d.collider.gameObject.tag == "edgeline")
			{
				Destroy(hit2d.collider.gameObject);
			}
			else if (hit2d.collider != null && hit2d.collider.gameObject.tag == "spawn_edge")
			{
				hit2d.collider.gameObject.GetComponent<SpawnEdgeScript>().cleanUp();	// clear all spawned elements, if any in the scene.
				Destroy(hit2d.collider.gameObject);
			}
			else if (hit2d.collider != null && hit2d.collider.gameObject.tag == "static_pen_line")
			{
				Destroy(hit2d.collider.gameObject);
			}
			else if (hit2d.collider != null && hit2d.collider.gameObject.tag == "oop_property_edge" &&
				hit2d.collider.transform.GetComponent<OOP_Property_Edge_script>().showPropertyEdge)
			{
				Destroy(hit2d.collider.transform.GetComponent<OOP_Property_Edge_script>().propertyTargetSet);
				Destroy(hit2d.collider.gameObject);
			}
		}
		#endregion

		// STATIC PEN BRUSH
		#region static_pen
		if (staticpen_button.GetComponent<AllButtonsBehavior>().selected)
		{
			//Debug.Log("entered");
			if (PenTouchInfo.PressedThisFrame) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Began)
			{
				// start drawing a new line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -10); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					totalLines++;
					templine = Instantiate(StaticPenLine, vec, Quaternion.identity, this.gameObject.transform);
					//templine.GetComponent<TrailRenderer>().material.color = Color.black;

					templine.name = "static_line" + totalLines.ToString();
					templine.tag = "static_pen_line";

					templine.GetComponent<staticLine_script>().points.Add(vec);

					// color and width
					templine.GetComponent<TrailRenderer>().material.color =
						staticpen_button.GetComponent<AllButtonsBehavior>().pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;

					templine.GetComponent<TrailRenderer>().widthMultiplier =
						staticpen_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

					templine.GetComponent<LineRenderer>().widthMultiplier =
						staticpen_button.GetComponent<AllButtonsBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;

					//templine.GetComponent<TrailRenderer>().endWidth =
					//staticpen_button.GetComponent<StaticPenButtonBehavior>().penWidthSliderInstance.GetComponent<Slider>().value;
				}
			}

			else if (PenTouchInfo.PressedNow && templine != null)//(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Moved && templine != null)
			{
				// add points to the last line
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -10); // Vector3.up * 0.1f;

					templine.GetComponent<TrailRenderer>().transform.position = vec;
					templine.GetComponent<staticLine_script>().points.Add(vec);

					templine.GetComponent<staticLine_script>().updateLengthFromPoints();
					templine.GetComponent<staticLine_script>().addPressureValue(PenTouchInfo.pressureValue);
					templine.GetComponent<staticLine_script>().reNormalizeCurveWidth();
					templine.GetComponent<TrailRenderer>().widthCurve = templine.GetComponent<staticLine_script>().widthcurve;
				}
			}

			else if (PenTouchInfo.ReleasedThisFrame && templine != null) //(Input.GetTouch(0).phase == UnityEngine.TouchPhase.Ended && templine != null)
			{
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{
					if (templine.GetComponent<staticLine_script>().points.Count > 2)
					{

						// compute centroid and bounds
						templine.GetComponent<staticLine_script>().computeCentroid();
						templine.GetComponent<staticLine_script>().computeBounds();

						templine.transform.position = templine.GetComponent<staticLine_script>().centroid;

						/*
						// UNIFORMLY DISTRIBUTE THE POINTS ALONG THE PATH
						GameObject spline = new GameObject("spline");
						spline.AddComponent<BezierSpline>();
						BezierSpline bs = spline.transform.GetComponent<BezierSpline>();
						bs.Initialize(templine.GetComponent<staticLine_script>().points.Count);
						for (int ss = 0; ss < templine.GetComponent<staticLine_script>().points.Count; ss++)
						{
							bs[ss].position = templine.GetComponent<staticLine_script>().points[ss];
						}

						// Now sample 50 points across the spline with a [0, 1] parameter sweep
						templine.GetComponent<staticLine_script>().points = new List<Vector3>(50);
						for (int i = 0; i < 50; i++)
						{
							templine.GetComponent<staticLine_script>().points.Add(bs.GetPoint(Mathf.InverseLerp(0, 49, i)));
						}

						Destroy(spline);
						//*/

						templine.GetComponent<LineRenderer>().positionCount = templine.GetComponent<staticLine_script>().points.Count;
						templine.GetComponent<LineRenderer>().SetPositions(templine.GetComponent<staticLine_script>().points.ToArray());

						templine.GetComponent<LineRenderer>().numCapVertices = 15;
						templine.GetComponent<LineRenderer>().numCornerVertices = 15;

						/*
						//Debug.Log(templine.GetComponent<LineRenderer>().positionCount);
						templine.GetComponent<LineRenderer>().Simplify(0.3f);
						Vector3[] pts = new Vector3[templine.GetComponent<LineRenderer>().positionCount];
						templine.GetComponent<LineRenderer>().GetPositions(pts);
						templine.GetComponent<staticLine_script>().points = new List<Vector3>(pts);
						//Debug.Log(templine.GetComponent<LineRenderer>().positionCount);
						*/

						// disable trail renderer, no longer needed
						templine.GetComponent<TrailRenderer>().enabled = false;

						// set line renderer color
						// if static pen is selected, the color picker now should be available.
						templine.GetComponent<LineRenderer>().material.color =
							staticpen_button.GetComponent<AllButtonsBehavior>().pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;
						//.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<ColorPicker>().Result;

						// set line renderer width
						templine.GetComponent<LineRenderer>().widthCurve = templine.GetComponent<staticLine_script>().widthcurve;

						// add points for edge collider
						Vector3[] arr = new Vector3[templine.GetComponent<LineRenderer>().positionCount];
						templine.GetComponent<LineRenderer>().GetPositions(arr);
						var edgepoints = new List<Vector3>();
						edgepoints.AddRange(arr);

						templine.GetComponent<EdgeCollider2D>().points = edgepoints.Select(x =>
						{
							var pos = templine.GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
							return new Vector2(pos.x, pos.y);
						}).ToArray();

						templine.GetComponent<EdgeCollider2D>().edgeRadius = 10;

						// set templine to null, otherwise, if an existing touch from color picker makes it to the canvas,
						// then points get added to the last templine (as a new templine hasn't been initialized from touch.begin).
						templine = null;
					}
					else
					{
						// delete the templine, not enough points
						Destroy(templine);
					}
				}

			}
		}
		#endregion

		// TEXT FORMULA INPUT
		#region text formula input

		if (textinput_button.GetComponent<AllButtonsBehavior>().selected)
		{
			if (PenTouchInfo.PressedThisFrame)
			{
				// create a textbox input prefab
				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
				RaycastHit Hit;
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject.tag == "paintable_canvas_object")
				{

					Vector3 vec = Hit.point + new Vector3(0, 0, -40); // Vector3.up * 0.1f;
																	  //Debug.Log(vec);

					templine = Instantiate(TextInputField, vec, Quaternion.identity, canvas_radial.transform);

					templine.GetComponent<TextInputUI>().paintable = this.gameObject;
				}
			}
		}

		#endregion

		// COPY FUNCTIONALITIES
		#region copy_behaviors

			// copy can be done by touch and pen both
			if (activeTouches.Count == 1 && pan_button.GetComponent<AllButtonsBehavior>().selected)
		{

			if (activeTouches[0].phase == UnityEngine.InputSystem.TouchPhase.Began ||
				activeTouches[0].phase == UnityEngine.InputSystem.TouchPhase.Moved)
			{
				// check if copy button of a menu object is touched
				var ray = Camera.main.ScreenPointToRay(activeTouches[0].screenPosition);
				RaycastHit Hit;

				if (Physics.Raycast(ray, out Hit) && Hit.collider.transform.gameObject.name == "copy" && Hit.collider.transform.parent.name.Contains("pen_menu_"))
				{
					//Debug.Log("copy collider.");

					Hit.collider.transform.GetComponent<CopyPenObject>().start_copying = true;
					//Hit.collider.transform.GetComponent<CopyPenObject>().copy_touch = Input.GetTouch(0);

					// NOT NEEDED: disable long tap functionalities, otherwise it brings up the menu for the next copied object
					//GameObject.Find("InputTouches").GetComponent<TapDetector>().enabled = false;
				}

				if (Physics.Raycast(ray, out Hit) && Hit.collider.transform.gameObject.name == "copy" && Hit.collider.transform.parent.name.Contains("set_menu_"))
				{
					//Debug.Log("copy collider.");

					Hit.collider.transform.GetComponent<CopySet>().start_copying = true;
					//Hit.collider.transform.GetComponent<CopyPenObject>().copy_touch = Input.GetTouch(0);

					// NOT NEEDED: disable long tap functionalities, otherwise it brings up the menu for the next copied object
					//GameObject.Find("InputTouches").GetComponent<TapDetector>().enabled = false;
				}
			}
		}

		// Pen based copy
		if (PenTouchInfo.PressedNow && pan_button.GetComponent<AllButtonsBehavior>().selected)
		{
			// check if copy button of a menu object is touched
			var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);
			RaycastHit Hit;

			if (Physics.Raycast(ray, out Hit) && Hit.collider.transform.gameObject.name == "copy" && Hit.collider.transform.parent.name.Contains("pen_menu_"))
			{
				//Debug.Log("copy collider.");

				Hit.collider.transform.GetComponent<CopyPenObject>().start_copying = true;
				//Hit.collider.transform.GetComponent<CopyPenObject>().copy_touch = Input.GetTouch(0);

				// NOT NEEDED: disable long tap functionalities, otherwise it brings up the menu for the next copied object
				//GameObject.Find("InputTouches").GetComponent<TapDetector>().enabled = false;
			}

			if (Physics.Raycast(ray, out Hit) && Hit.collider.transform.gameObject.name == "copy" && Hit.collider.transform.parent.name.Contains("set_menu_"))
			{
				//Debug.Log("copy collider.");

				Hit.collider.transform.GetComponent<CopySet>().start_copying = true;
				//Hit.collider.transform.GetComponent<CopyPenObject>().copy_touch = Input.GetTouch(0);

				// NOT NEEDED: disable long tap functionalities, otherwise it brings up the menu for the next copied object
				//GameObject.Find("InputTouches").GetComponent<TapDetector>().enabled = false;
			}
		}

		#endregion

		// HANDLE ANY RELEVANT KEY INPUT FOR PAINTABLE'S OPERATIONS
		handleKeyInteractions();
	}

	void OnApplicationQuit()
	{
		EnhancedTouchSupport.Disable();
	}

	#region TapFunctions

	//called when a multi-Tap event is detected
	void OnShortTap(Vector2 pos)
	{
		//if (tap.count >= 2) Debug.Log("double tap");

		//do a raycast base on the position of the tap
		Ray ray = Camera.main.ScreenPointToRay(pos);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		{
			// DOUBLE FUNCTION ARGUMENT SELECTION
			#region double function argument selection
			//if the tap lands on a set/function anchor or a pen object, then set them as the function arguments.
			// NOTE: IF ELSE ENDS UP NOT CHECKING THE THIRD BLOCK (PENLINE), BECAUSE 
			// BOTH PENLINE AND FUNCTIONS BELONG TO FUNCTIONS AS CHILDREN IN THIS CASE, SO THE FIRST ONE (FUNCTION)
			// GETS CHECKED AND MOVED ON, WHEN A PENLINE IS TAPPED. THUS THE PENLINE DOESN'T GET RECOGNIZED.

			if (hit.collider.transform.parent.gameObject.tag == "set" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// if the parent is a function and is currently waiting for an operand to be tapped
				if (hit.collider.transform.parent.parent.tag == "function"
					&& hit.collider.transform.parent.parent.GetComponent<functionLine_script>().halt_live_update_for_double_function)
				{
					// has this been made an operand already, under the current function? If not, make this an operand
					if (!hit.collider.transform.parent.GetComponent<setLine_script>().is_this_double_function_operand)
					{
						hit.collider.transform.parent.parent.GetComponent<functionLine_script>().assignOperand(hit.collider.transform.parent.gameObject);
					}
				}
			}

			if (hit.collider.transform.parent.gameObject.tag == "function" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// if the parent is a function and is currently waiting for an operand to be tapped
				if (hit.collider.transform.parent.parent.tag == "function"
					&& hit.collider.transform.parent.parent.GetComponent<functionLine_script>().halt_live_update_for_double_function)
				{
					// has this been made an operand already, under the current function? If not, make this an operand
					if (!hit.collider.transform.parent.GetComponent<functionLine_script>().is_this_double_function_operand)
					{
						hit.collider.transform.parent.parent.GetComponent<functionLine_script>().assignOperand(hit.collider.transform.parent.gameObject);
					}
				}
			}
			
			if (hit.collider.transform.tag == "penline" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// if the parent is a function and is currently waiting for an operand to be tapped
				if (hit.collider.transform.parent.tag == "function"
					&& hit.collider.transform.parent.GetComponent<functionLine_script>().halt_live_update_for_double_function)
				{
					// has this been made an operand already? If not, make this an operand
					if (!hit.collider.transform.GetComponent<penLine_script>().is_this_double_function_operand)
					{
						hit.collider.transform.parent.GetComponent<functionLine_script>().assignOperand(hit.collider.transform.gameObject);
					}
				}

			}

			#endregion

			// STROKE CONVERSION

			if (hit.collider.transform.tag == "penline" && strokeconversion_button.GetComponent<AllButtonsBehavior>().selected)
			{
				templine = null;
				//Vector3[] vertices;
				//Debug.Log("stroke-converting pen object " + hit.collider.name);
				if (hit.collider.transform.GetComponent<penLine_script>().isThisAnImage)
				{
					vertices = hit.collider.transform.GetComponent<penLine_script>().fromLocalToGlobalPoints().ToArray();	// points already contains sprite vertices
				}
				else
				{
					vertices = hit.collider.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh.vertices;
				}

				// Compute convex hull. A concavity value less than 30 may produce tighter shape, but point in polygon test starts failing at that point.
				var hullAPI = new HullAPI();
				var hull = hullAPI.Hull2D(new Hull2DParameters() { Points = vertices, Concavity = 30 });

				vertices = hull.vertices;

				// sort vertices according to angle

				// Store angles between texture's center and vertex position vector
				float[] angles = new float[vertices.Length];

				// Calculate the angle between each vertex and the texture's /center
				for (int i = 0; i < vertices.Length; i++)
				{
					angles[i] = AngleBetweenVectors(vertices[i], hit.collider.transform.position);
					//vertexArray[i] -= texCenter;   // Offset vertex about texture center
				}

				// Sort angles into ascending order, use to put vertices in clockwise order
				Array.Sort(angles, vertices);

				if (strokeconversion_button.GetComponent<AllButtonsBehavior>().isFunctionStrokeConversion)
				{
					// Create a function at the position of the mesh vertices (original pen line drawing position)
					templine = transform.GetComponent<CreatePrimitives>().CreateFunction(vertices.ToList());

					if (hit.collider.transform.GetComponent<penLine_script>().isThisAnImage)
					{
						templine.GetComponent<functionLine_script>().sprite_filename = hit.collider.transform.GetComponent<penLine_script>().sprite_filename;
					}
				}
				else if (!strokeconversion_button.GetComponent<AllButtonsBehavior>().isFunctionStrokeConversion)
				{
					// Create a set at the position of the mesh vertices (original pen line drawing position)
					templine = transform.GetComponent<CreatePrimitives>().CreateSet(vertices.ToList());

					if (hit.collider.transform.GetComponent<penLine_script>().isThisAnImage)
					{
						templine.GetComponent<setLine_script>().sprite_filename = hit.collider.transform.GetComponent<penLine_script>().sprite_filename;
					}
				}

				// Move the set/function to the current coordinate of the pen object
				templine.transform.position = hit.collider.transform.position;

				// Copy the _meshobj of penline to functionline's _meshobj's child
				GameObject penmeshobj = hit.collider.transform.GetChild(0).gameObject;
				GameObject tocopy = Instantiate(penmeshobj, this.transform);

				tocopy.transform.position = templine.transform.GetChild(1).position;

				tocopy.transform.SetParent(templine.transform.GetChild(1));

				// there's an offset for pencil converted objects, fixing the localposition to (0,0,0) fixes it
				//if (!hit.collider.transform.GetComponent<penLine_script>().isThisAnImage)
					tocopy.transform.localPosition = new Vector3(0, 0, 0);

				tocopy.name = "_stroke_converted_mesh";

				templine = null;
				
				// Delete the pen line object
				Destroy(hit.collider.gameObject);
			}

			// UNDO ANY PENDING DOUBLE FUNCTION ACTIVITY IF TAPPED ON PAINTABLE, OR CLOSE ANY OPEN MENU
			if (hit.collider.transform.tag == "paintable_canvas_object" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// any pending double function activity?
				GameObject[] funcs = GameObject.FindGameObjectsWithTag("function");
				for (int i = 0; i < funcs.Length; i++)
				{
					if (funcs[i].GetComponent<functionLine_script>().is_this_a_double_function &&
						(funcs[i].GetComponent<functionLine_script>().first_operand == null ||
						funcs[i].GetComponent<functionLine_script>().second_operand == null))
					{
						funcs[i].GetComponent<functionLine_script>().resetOperands();
					}
				}

				// any open menu? destroy them
				if (canvas_radial.transform.childCount > 0)
				{
					for (int i = 0; i < canvas_radial.transform.childCount; i++)
					{
						Destroy(canvas_radial.transform.GetChild(i).gameObject);
					}
				}
				if (GameObject.Find("Canvas").transform.Find("Stroke_Conversion_Selection_Menu") != null)
				{
					Destroy(GameObject.Find("Canvas").transform.Find("Stroke_Conversion_Selection_Menu").gameObject);
				}
			}
			else if (hit.collider.transform.tag == "paintable_canvas_object" && edge_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// delete any spawn edge menu, if any
				if (canvas_radial.transform.childCount > 0)
				{
					for (int i = 0; i < canvas_radial.transform.childCount; i++)
					{
						Destroy(canvas_radial.transform.GetChild(i).gameObject);
					}
				}
			}

			// MENU CREATION

			// SET MENU
			//if the tap lands on a set anchor, then bring up the set menu. 
			if (hit.collider.transform.parent.gameObject.tag == "set" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// Make sure they aren't waiting to be tapped for a double function parent.
				if (hit.collider.transform.parent.parent.tag == "function" &&
					hit.collider.transform.parent.parent.GetComponent<functionLine_script>().halt_live_update_for_double_function)
				{
					return;
				}

				// There should be only one menu open at a time. Don't create anything new if the previous menu is not closed or destroyed.
				if (canvas_radial.transform.childCount > 0)
				{
					return;
				}

				GameObject[] setobjs = GameObject.FindGameObjectsWithTag("set");
				for (int i = 0; i < setobjs.Length; i++)
				{
					if (hit.collider.transform == setobjs[i].transform.GetChild(0).transform)
					{
						//Debug.Log("set anchor long tapped.");

						// change anchor material instance color
						setobjs[i].transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.gray;

						// create a menu and show it
						GameObject radmenu = Instantiate(set_radial_menu,
							setobjs[i].transform.GetChild(0).transform.position + new Vector3(0f, 0f, -50f),
							Quaternion.identity,
							canvas_radial.transform);

						// update name
						radmenu.name = "set_menu_" + setobjs[i].name;
						setobjs[i].GetComponent<setLine_script>().menu_name = radmenu.name;

						// update parent_set of menu behavior script
						radmenu.GetComponent<SetMenuBehavior>().parent_set = setobjs[i].gameObject;

						// manual resizing required -- can't figure out a way to resize in the editor that's persistent.
						radmenu.transform.Find("count").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("name").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("combine").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("delete").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("close").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("oop_properties").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("timer").GetComponent<RectTransform>().sizeDelta = new Vector2(43, 43);
						radmenu.transform.Find("flatten").GetComponent<RectTransform>().sizeDelta = new Vector2(33, 33);

						// resize the box collider of the copy button (for long press detection)
						//radmenu.transform.Find("copy").GetComponent<BoxCollider>().size = new Vector3(45, 45, 1);

						// set the copy button's instantiation object
						//radmenu.transform.Find("copy").GetComponent<CopySet>().toCopy = setobjs[i];

						// property: count. set up the text box
						//setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
						//radmenu.transform.Find("count").GetComponent<Button>(), 0);

						radmenu.transform.Find("_count_textbox").GetComponent<TMP_InputField>().text = setobjs[i].GetComponent<setLine_script>().current_feature.ToString();
						radmenu.transform.Find("_count_textbox").transform.position = radmenu.transform.Find("count").transform.position;
						radmenu.transform.Find("_count_textbox").GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;
						radmenu.transform.Find("_count_textbox").GetComponent<RectTransform>().sizeDelta = new Vector2(30, 30);

						// property: name (just setup the text box)
						radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().text = setobjs[i].GetComponent<setLine_script>().this_set_name;
						radmenu.transform.Find("_name_textbox").transform.position = radmenu.transform.Find("name").transform.position;
						radmenu.transform.Find("_name_textbox").GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;
						radmenu.transform.Find("_name_textbox").GetComponent<RectTransform>().sizeDelta = new Vector2(30, 30);

						// property: capacity
						//setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
						//radmenu.transform.Find("uPIeMenuOptionButton#1").GetComponent<Button>(), 1);

						// property: combine children pen objects and delete set
						setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("combine").GetComponent<Button>(), 1);

						// property: delete set
						setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("delete").GetComponent<Button>(), 2);

						// property: exit
						setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("close").GetComponent<Button>(), 3);

						// property: copy
						//setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
						//radmenu.transform.Find("copy").GetComponent<Button>(), 4);

						// property: oop properties
						setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("oop_properties").GetComponent<Button>(), 4);

						setobjs[i].GetComponent<setLine_script>().OOP_properties_submenu_prefab = oop_properties_submenu;

						// initiate timer
						setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("timer").GetComponent<Button>(), 5);
						setobjs[i].GetComponent<setLine_script>().set_timer_menu_prefab = set_timer_submenu;

						// action: flatten
						setobjs[i].GetComponent<setLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("flatten").GetComponent<Button>(), 6);

						// create an abstraction slider
						Slider abstract_slider = Instantiate(abstraction_slider,
							setobjs[i].transform.GetChild(0).transform.position + new Vector3(50f, 0f, -2f),
							Quaternion.identity,
							radmenu.transform);

						abstract_slider.name = "abstract_slider_" + setobjs[i].name;
						// set the value to any previous value of the slider or the default value
						abstract_slider.value = setobjs[i].GetComponent<setLine_script>().abstraction_layer;
						setobjs[i].GetComponent<setLine_script>().abs_slider = abstract_slider;

						//abstract_slider.onValueChanged.AddListener(delegate { onAbstractionSliderChange(setobjs[i].transform, abstract_slider.name); });
						//abstract_slider

						break;
					}
				}
			}

			// PEN OBJECT MENU
			// if the tap lands on a drawn object (penline), bring up the pen object menu
			if (hit.collider.gameObject.tag == "penline" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// Make sure they aren't waiting to be tapped for a double function parent.
				if (hit.collider.transform.parent.tag == "function" &&
					hit.collider.transform.parent.GetComponent<functionLine_script>().halt_live_update_for_double_function)
				{
					return;
				}

				// There should be only one menu open at a time. Don't create anything new if the previous menu is not closed or destroyed.
				if (canvas_radial.transform.childCount > 0)
				{
					return;
				}

				GameObject penobj = hit.collider.gameObject;

				// create a menu and show it
				GameObject radmenu = Instantiate(pen_radial_menu,
					penobj.transform.position + new Vector3(0f, 0f, -50f),
					//main_camera.WorldToScreenPoint(penobj.transform.position),
					Quaternion.identity,
					canvas_radial.transform);

				// update name
				radmenu.name = "pen_menu_" + penobj.name;
				penobj.transform.GetComponent<penLine_script>().menu_name = radmenu.name;

				// disable the name textbox
				// radmenu.transform.Find("_name_textbox").gameObject.SetActive(false);

				// add extra buttons
				//radmenu.GetComponent<uPIeMenu>().AddMenuOptionAndRealign(true, true, false);

				// manual resizing required -- can't figure out a way to resize in the editor that's persistent.
				radmenu.transform.Find("translation").GetComponent<RectTransform>().sizeDelta = new Vector2(45, 40);
				radmenu.transform.Find("attribute").GetComponent<RectTransform>().sizeDelta = new Vector2(45, 32);
				radmenu.transform.Find("attribute").GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(40, 28);
				radmenu.transform.Find("attribute").GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(40, 28);
				radmenu.transform.Find("delete").GetComponent<RectTransform>().sizeDelta = new Vector2(45, 45);
				radmenu.transform.Find("rotation").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
				radmenu.transform.Find("scale").GetComponent<RectTransform>().sizeDelta = new Vector2(30, 30);
				radmenu.transform.Find("opacity").GetComponent<RectTransform>().sizeDelta = new Vector2(40, 40);

				radmenu.transform.Find("_name_textbox").GetComponent<RectTransform>().sizeDelta = new Vector2(60, 30);
				radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().text =
					penobj.GetComponent<penLine_script>()._name;

				radmenu.transform.Find("_attribute_label").GetComponent<RectTransform>().sizeDelta = new Vector2(30, 20);
				//radmenu.transform.Find("_attribute_label").transform.position =
				//radmenu.transform.Find("attribute").transform.position + new Vector3(0, -75f, 0);
				radmenu.transform.Find("_attribute_label").GetComponent<TextMeshProUGUI>().text =
					penobj.GetComponent<penLine_script>().current_attribute.ToString("F1");
				// set the sprite according to current attribute
				if (penobj.GetComponent<penLine_script>().current_attribute == penobj.GetComponent<penLine_script>().attribute.Length)
				{
					radmenu.transform.Find("attribute").GetComponent<Image>().sprite =
							radmenu.transform.Find("attribute").GetChild(1).GetComponent<Image>().sprite;
				}
				else
				{
					radmenu.transform.Find("attribute").GetComponent<Image>().sprite =
							radmenu.transform.Find("attribute").GetChild(0).GetComponent<Image>().sprite;
				}

				// resize the box collider of the copy button (for long press detection)
				//radmenu.transform.Find("copy").GetComponent<BoxCollider>().size = new Vector3(50, 50, 1);
				// set the copy button's instantiation object
				//radmenu.transform.Find("copy").GetComponent<CopyPenObject>().toCopy = penobj;

				// update submenu prefabs of penline_script
				penobj.GetComponent<penLine_script>().rotation_menu_prefab = penline_rotation_menu;
				penobj.GetComponent<penLine_script>().scale_menu_prefab = penline_scale_menu;

				// disable the attribute submenu
				radmenu.transform.Find("attribute").GetChild(0).gameObject.SetActive(false);
				radmenu.transform.Find("attribute").GetChild(1).gameObject.SetActive(false);

				// property: translation
				penobj.transform.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("translation").GetComponent<Button>(), 0);

				// property: attribute
				penobj.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("attribute").GetComponent<Button>(), 3);

				// property: delete
				penobj.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("delete").GetComponent<Button>(), 4);

				// property: exit
				penobj.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("rotation").GetComponent<Button>(), 5);

				// property: copy object
				penobj.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("scale").GetComponent<Button>(), 6);

				// attribute: area
				penobj.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("attribute").GetChild(0).GetComponent<Button>(), 7);

				// attribute: length
				penobj.GetComponent<penLine_script>().menuButtonClick(radmenu,
					radmenu.transform.Find("attribute").GetChild(1).GetComponent<Button>(), 8);

			}

			// FUNCTION MENU
			//if the tap lands on a function anchor, then bring up the function menu.
			if (hit.collider.transform.parent.gameObject.tag == "function" && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				// Make sure they aren't waiting to be tapped for a double function parent.
				if (hit.collider.transform.parent.parent.tag == "function" &&
					hit.collider.transform.parent.parent.GetComponent<functionLine_script>().halt_live_update_for_double_function)
				{
					return;
				}

				// There should be only one menu open at a time. Don't create anything new if the previous menu is not closed or destroyed.
				if (canvas_radial.transform.childCount > 0)
				{
					return;
				}

				GameObject[] funcobjs = GameObject.FindGameObjectsWithTag("function");
				for (int i = 0; i < funcobjs.Length; i++)
				{
					if (hit.collider.transform == funcobjs[i].transform.GetChild(0).transform)
					{
						//Debug.Log("function anchor long tapped.");

						// change anchor material instance color
						funcobjs[i].transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.gray;

						// create a menu and show it
						GameObject radmenu = Instantiate(function_radial_menu,
							funcobjs[i].transform.GetChild(0).transform.position + new Vector3(0f, 0f, -70f),
							Quaternion.identity,
							canvas_radial.transform);

						// update name
						radmenu.name = "function_menu_" + funcobjs[i].name;
						funcobjs[i].GetComponent<functionLine_script>().menu_name = radmenu.name;

						// update parent_function of menu behavior script
						radmenu.GetComponent<FunctionMenuBehavior>().parent_function = funcobjs[i].gameObject;

						// manual resizing required -- can't figure out a way to resize in the editor that's persistent.
						radmenu.transform.Find("operator").GetComponent<RectTransform>().sizeDelta = new Vector2(45, 45);
						radmenu.transform.Find("double_function").GetComponent<RectTransform>().sizeDelta = new Vector2(45, 45);
						radmenu.transform.Find("unlink_elements").GetComponent<RectTransform>().sizeDelta = new Vector2(35, 35);
						radmenu.transform.Find("flatten_all").GetComponent<RectTransform>().sizeDelta = new Vector2(32, 32);
						radmenu.transform.Find("close").GetComponent<RectTransform>().sizeDelta = new Vector2(40, 40);
						radmenu.transform.Find("oop_properties").GetComponent<RectTransform>().sizeDelta = new Vector2(40, 40);
						radmenu.transform.Find("name").GetComponent<RectTransform>().sizeDelta = new Vector2(45, 45);
						radmenu.transform.Find("category_filter").GetComponent<RectTransform>().sizeDelta = new Vector2(40, 40);


						// property: name (just setup the text box)
						if (funcobjs[i].GetComponent<functionLine_script>().user_given_function_name == "")
						{
							radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().text = funcobjs[i].name;
						}
						else
						{
							radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().text = 
								funcobjs[i].GetComponent<functionLine_script>().user_given_function_name;
						}
						radmenu.transform.Find("_name_textbox").transform.position = radmenu.transform.Find("name").transform.position + new Vector3(0, 0, -150);
						radmenu.transform.Find("_name_textbox").GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;
						//radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().characterLimit = 1;
						radmenu.transform.Find("_name_textbox").GetComponent<RectTransform>().sizeDelta = new Vector2(60, 30);

						// property: operator (just setup the text box)
						radmenu.transform.Find("_operator_textbox").GetComponent<TMP_InputField>().text =
							funcobjs[i].GetComponent<functionLine_script>().currentOperator();  // default operation/function is +
						radmenu.transform.Find("_operator_textbox").transform.position = radmenu.transform.Find("operator").transform.position;
						radmenu.transform.Find("_operator_textbox").GetChild(0).GetChild(2).GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;
						radmenu.transform.Find("_operator_textbox").GetComponent<TMP_InputField>().characterLimit = 1;
						radmenu.transform.Find("_operator_textbox").GetComponent<RectTransform>().sizeDelta = new Vector2(30, 30);

						/*
						// property: delete set
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("uPIeMenuOptionButton#3").GetComponent<Button>(), 3);
						*/

						// property: exit
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("close").GetComponent<Button>(), 5);

						// property: double function
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("double_function").GetComponent<Button>(), 2);

						// property: flatten one
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("unlink_elements").GetComponent<Button>(), 3);

						// property: flatten all
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("flatten_all").GetComponent<Button>(), 4);

						// property: oop properties
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("oop_properties").GetComponent<Button>(), 6);

						funcobjs[i].GetComponent<functionLine_script>().OOP_properties_submenu_prefab = oop_properties_submenu;

						// property: oop properties
						funcobjs[i].GetComponent<functionLine_script>().menuButtonClick(radmenu,
							radmenu.transform.Find("category_filter").GetComponent<Button>(), 7);

						funcobjs[i].GetComponent<functionLine_script>().category_filter_submenu_prefab = category_filter_submenu_prefab;

						// create an abstraction slider
						Slider abstract_slider = Instantiate(abstraction_slider,
							funcobjs[i].transform.GetChild(0).transform.position + new Vector3(60f, 0f, -2f),
							Quaternion.identity,
							radmenu.transform);

						abstract_slider.name = "abstract_slider_" + funcobjs[i].name;

						// default abstraction_slider prefab max value is 3. Reset for function.
						//abstract_slider.maxValue = 4;

						// set the value to any previous value of the slider or the default value
						abstract_slider.value = funcobjs[i].GetComponent<functionLine_script>().abstraction_layer;

						funcobjs[i].GetComponent<functionLine_script>().abs_slider = abstract_slider;

						//abstract_slider.onValueChanged.AddListener(delegate { onAbstractionSliderChange(setobjs[i].transform, abstract_slider.name); });
						//abstract_slider

						break;
					}
				}
			}


			// ===========================
			// ANYTHING ELSE FOR SHORT TAP?

		}

	}
	

	// THIS IS CURRENTLY CALLED UNDER SHORT TAP DETECTION TOO. LONG TAPS ARE HARD WRT INTERACTION EXPERIENCE
	void OnLongTap(Vector2 pos)
	{
		//Debug.Log("long-tap");

		// all of this should happen in pan mode, to avoid drawing errors

		// CHECK IF WE ARE USING TWO FINGERS, THEN DO NOTHING
		//if (Input.touchCount > 1) return;
		
		//do a raycast base on the position of the tap
		Ray ray = Camera.main.ScreenPointToRay(pos);
		RaycastHit hit;
		RaycastHit2D hit2d;

		if (Physics.Raycast(ray, out hit, Mathf.Infinity))
		{
			// IF NEEDED, PLACE THE PEN, SET, FUNCTION MENUS HERE.
			// ===================================================

			// PEN OBJECT MENU COPY BUTTON
			if (hit.collider.transform.name == "copy" && hit.collider.transform.parent.name.Contains("pen_menu_") && pan_button.GetComponent<AllButtonsBehavior>().selected)
			{
				//Debug.Log("pen copy menu object long tapped. Touches: " + Input.touchCount.ToString());

				//hit.collider.transform.GetComponent<CopyPenObject>().start_copying = true;
				//hit.collider.transform.GetComponent<CopyPenObject>().copy_touch = Input.GetTouch(0);
				
			//hit.collider.transform.GetComponent<CopyPenObject>().start_location = hit.collider.transform.position + new Vector3(0, 20, 0);
			}

			// PEN SELECTION MENU
			if (hit.collider.transform.name == "Pencil")
			{
				Debug.Log("hit pencil.");
			}

			// EDGELINE PROGRAMMING TEXT BOX
			if (hit.collider.gameObject.tag == "edgeline")
			{
				// show the text
				//hit.collider.transform.GetChild(0).gameObject.SetActive(true);
				Debug.Log("hit TEXDraw");
			}
		}

		// PARAMETER TEXT LABEL GUI
		hit2d = Physics2D.GetRayIntersection(ray);
		if (hit2d.collider != null && hit2d.collider.gameObject.tag == "parameter_text_label" && pan_button.GetComponent<AllButtonsBehavior>().selected)
		{
			// instantiate a parameter UIField 
			GameObject paramfield;

			// the text is a keypoint on the translation line

			paramfield = Instantiate(ParameterUIField,
						   new Vector3(hit2d.point.x, hit2d.point.y, -20f),
						   Quaternion.identity,
						   hit2d.collider.transform.parent.parent); // set the parent transform as penline

			paramfield.GetComponent<param_InputField_Script>().penLineObjectParent = hit2d.collider.transform.parent.parent.gameObject;

			paramfield.GetComponent<param_InputField_Script>().current_val = hit2d.collider.transform.GetComponent<param_text_Script>().upper_val;
				//float.Parse(hit2d.collider.transform.GetComponent<TextMeshPro>().text);

			paramfield.GetComponent<param_InputField_Script>().keypoint_index = hit2d.collider.transform.GetSiblingIndex();

			paramfield.GetComponent<InputField>().text = hit2d.collider.transform.GetComponent<TextMeshPro>().text;

			paramfield.GetComponent<InputField>().ActivateInputField();

			/*
			// populate the param field if there's an edge connecting it, otherwise enter the default value currently on the pen object

			// search for the edge that contains the parent pen object
			// then find the source object from the edge, and set the capacity of the source object

			GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
			bool edgefound = false;
			for (int i = 0; i < edges.Length; i++)
			{
				if (edges[i].GetComponent<edgeLine_script>().target_object.name == transform.parent.name)
				{
					//Debug.Log("found edge target.");
					edgefound = true;

					if (edges[i].GetComponent<edgeLine_script>().source_object.tag == "set")
					{
						paramfield.GetComponent<InputField>().text =
							edges[i].GetComponent<edgeLine_script>().source_object.GetComponent<setLine_script>().capacity.ToString();
					}
					else if (edges[i].GetComponent<edgeLine_script>().source_object.tag == "function")
					{
						paramfield.GetComponent<InputField>().text =
							edges[i].GetComponent<edgeLine_script>().source_object.GetComponent<functionLine_script>()
							.max_function_value.ToString();
					}

					break;
				}
			}

			// no edge found, load the value from penline script
			if (!edgefound)
			{
				paramfield.GetComponent<InputField>().text = hit2d.collider.transform.parent.GetComponent<penLine_script>()
					.max_parameter_value.ToString();
			}

			*/

		}

		// EDGE CONDITION INPUT FIELD
		if (hit2d.collider != null && hit2d.collider.gameObject.tag == "edgeline")
		{
			// instantiate a edge UIField 
			GameObject edgefield = Instantiate(EdgeUIField,
							new Vector3(hit2d.point.x, hit2d.point.y, -20f),
							Quaternion.identity,
							hit2d.collider.transform); // set the parent transform as edgeline

			edgefield.GetComponent<InputField>().ActivateInputField();

			edgefield.GetComponent<edgeLine_InputField_Script>().edgeLineObjectParent = hit2d.collider.gameObject;
		}

		// SPAWN EDGE MENU SETUP
		if (hit2d.collider != null && hit2d.collider.gameObject.tag == "spawn_edge")
		{
			// instantiate a spawn edge menu 
			GameObject spawnedgemenu = Instantiate(SpawnEdgeMenu,
							new Vector3(hit2d.point.x, hit2d.point.y, 10f),
							Quaternion.identity,
							canvas_radial.transform); // set the parent transform as paintable

			//edgefield.GetComponent<InputField>().ActivateInputField();

			spawnedgemenu.GetComponent<SpawnEdgeMenuBehavior>().SpawnEdgeParent = hit2d.collider.gameObject;

			spawnedgemenu.transform.Find("_initial_slider").GetComponent<Slider>().value =
				hit2d.collider.gameObject.GetComponent<SpawnEdgeScript>().initRandomness;

			spawnedgemenu.transform.Find("_dt_input").GetComponent<InputField>().text =
				hit2d.collider.gameObject.GetComponent<SpawnEdgeScript>().dt.ToString();
		}
	}

	#endregion

	void searchNodeAndDeleteEdge(string node_name)
	{
		GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
		List<GameObject> edgeList = new List<GameObject>(edges);
		for (int i = 0; i < edgeList.Count; i++)
		{
			GameObject source = edgeList[i].GetComponent<edgeLine_script>().source_object;
			GameObject target = edgeList[i].GetComponent<edgeLine_script>().target_object;
			
			if (source.name == node_name || target.name == node_name)
			{
				Destroy(edgeList[i].gameObject);
				break;
			}
		}
	}

	void deleteAllIncompleteEdges()
	{
		GameObject[] edges = GameObject.FindGameObjectsWithTag("edgeline");
		List<GameObject> edgeList = new List<GameObject>(edges);
		for(int i = 0; i < edgeList.Count; i++)
		{
			GameObject source = edgeList[i].GetComponent<edgeLine_script>().source_object;
			GameObject target = edgeList[i].GetComponent<edgeLine_script>().target_object;
			Debug.Log(source == null);
			Debug.Log(source.name);
			Debug.Log(target.name);
			if (source == null || target == null)
			{
				Destroy(edgeList[i].gameObject);
			}
		}
	}

	void deleteTempLineIfDoubleFinger()
	{
		// Should only be called when necessary -- to get rid of incomplete lines when a double finger is detected and we are not in the pan mode.
		if (Input.touchCount > 1 && templine != null && pan_button.GetComponent<AllButtonsBehavior>().selected == false)
		{
			Destroy(templine);
		}
	}

	float mapToRange(float s, float a1, float a2, float b1, float b2)
	{
		return b1 + (s - a1) * (b2 - b1) / (a2 - a1);
	}

	Vector2 ConvertVector2Control(UnityEngine.InputSystem.Controls.Vector2Control v)
	{
		return new Vector2(v.x.ReadValue(), v.y.ReadValue());
	}

	public static float AngleBetweenVectors(Vector2 a, Vector2 b)
	{
		float angle = (float)Mathf.Atan2(a.y - b.y,
										 a.x - b.x);
		return angle;
	}

	public void cleanUpRadialCanvas()
	{
		// any open menu? destroy them
		if (canvas_radial.transform.childCount > 0)
		{
			for (int i = 0; i < canvas_radial.transform.childCount; i++)
			{
				Destroy(canvas_radial.transform.GetChild(i).gameObject);
			}
		}
	}

	void handleKeyInteractions()
	{
		if (Input.GetKeyUp(KeyCode.L))
		{
			panZoomLocked = !panZoomLocked;
		}

		if (Input.GetKeyUp(KeyCode.Escape))
		{
			cleanUpRadialCanvas();
		}

		if (Input.GetKeyUp(KeyCode.F10))
		{
			allowOpacity = !allowOpacity;
			Debug.Log("allow opacity: " + allowOpacity.ToString());
		}

		if (Input.GetKeyUp(KeyCode.Delete))
		{
			// delete game objects from history, starting with the latest
			if (history.Count > 0)
			{
				Destroy(history[history.Count - 1]);
				history.RemoveAt(history.Count - 1);
			}
		}

		// test thumbnail
		if (Input.GetKeyUp(KeyCode.T))
		{
			/*
			if (gameObject.transform.childCount > 0)
			{
				RuntimePreviewGenerator.PreviewDirection = new Vector3(0, 0, 1);
				RuntimePreviewGenerator.BackgroundColor = new Color(0.3f, 0.3f, 0.3f, 0f);
				RuntimePreviewGenerator.OrthographicMode = true;

				Sprite action = Sprite.Create(RuntimePreviewGenerator.GenerateModelPreview(gameObject.transform.GetChild(0), 128, 128)
					, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 20f);
				GameObject.Find("Action").GetComponent<Image>().sprite = action;
			}
			*/
		}

		// test adding to action history from one template
		if (Input.GetKeyUp(KeyCode.M))
		{
			// This test works
			/*
			GameObject actionhist = GameObject.Find("ActionHistory");
			GameObject listtocopy = actionhist.transform.GetChild(0).GetChild(0).GetChild(1).gameObject;
			GameObject newitem = Instantiate(listtocopy, listtocopy.transform.parent);
			*/

			ActionHistoryEnabled = !ActionHistoryEnabled;

			if (ActionHistoryEnabled) GameObject.Find("Canvas").transform.Find("ActionHistory").gameObject.SetActive(true);
			else GameObject.Find("Canvas").transform.Find("ActionHistory").gameObject.SetActive(false);
		}
	}

}
