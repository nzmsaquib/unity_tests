﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Symbolism;
using TMPro;

public class edgeLine_script : MonoBehaviour
{

	public List<Vector3> points = new List<Vector3>();
	public Vector3 centroid;
	public float maxx = -100000f, maxy = -100000f, minx = 100000f, miny = 100000f;

	public bool draggable_now = false;

	public Material edge_line_material;

	public bool edge_set = false;

	// this should be tuned according to the nature of edge and parameterization from slider UI elements
	public int map_factor = 60;

	public string source_name;
	public string target_name;
	public string source_property;
	public string target_property;

	public GameObject source_object, target_object;

	// equation and <, > constraints
	public float rhs_equation = 0f;
	public string full_condition_text;
	public string condition_operator;
	// initially, there's no condition being set, so the update actions should happen continuously
	private bool condition_met = true;
	public enum ParsingResult
	{
		Success,	// parse successful. either of =, <, > is present and found a number with it
		Fail,       // either of =, <, > is present but did not find a number in rhs
		Gibberish	// couldn't find a =, <, >
	}

	public ParsingResult parse_result;

	private bool no_problem_in_parsing = true;

	// if target pen object is being copied, then don't apply the updateFeature method yet, let it be still
	public bool target_is_being_copied = false;

	public void computeCentroid()
	{
		float totalx = 0, totaly = 0, totalz = 0;
		for (int i = 0; i < points.Count; i++)
		{
			totalx += points[i].x;
			totaly += points[i].y;
			totalz += points[i].z;
		}
		centroid = new Vector3(totalx / points.Count, totaly / points.Count, points[0].z); // including z in the average calc. created problem
																						   // so just used a constant value from the points list.
	}

	public void computeBounds()
	{
		maxx = -100000f; maxy = -100000f; minx = 100000f; miny = 100000f;

		for (int i = 0; i < points.Count; i++)
		{
			if (maxx < points[i].x) maxx = points[i].x;
			if (maxy < points[i].y) maxy = points[i].y;
			if (minx > points[i].x) minx = points[i].x;
			if (miny > points[i].y) miny = points[i].y;
		}
	}


	public void updateEdge()
	{
		if (edge_set)
		{
			if (parse_result == ParsingResult.Success)
			{
				updateConditionFromEquation();
				if (condition_met)
				{
					mapParamFromEdge();
				}
				else
				{
					// shoot the target back to 0.
					setTargetToZero();
				}
			}
			else if (parse_result == ParsingResult.Fail)
			{
				// cannot update from equation because it doesn't have the right syntax even though there's =, <, >. Don't do anything
			}
			else if (parse_result == ParsingResult.Gibberish)
			{
				// the text is gibberish, or there's no text with a =, <, or >. Just update the parameter without any condition.
				mapParamFromEdge();
			}

			/*
			 * Don't need it at this point, all colliders are disabled in graph mode, so things can't be moved around
			 */
			/*
			// visualization: if edge is set and currently in edge line mode, update line renderer in case the nodes moved
			if (GameObject.Find("Edge_draw").GetComponent<EdgeButtonBehavior>().selected)
			{
				transform.GetComponent<LineRenderer>().SetPosition(0, source_object.transform.position);
				transform.GetComponent<LineRenderer>().SetPosition(1, target_object.transform.position);
			}
			*/
		}

	}

	public void mapParamFromEdge()
	{
		if (source_object != null && target_object != null)
		{
			//float map_param;
			if (source_object.GetComponent<setLine_script>() != null)
			{
				//map_param = (float)source_object.GetComponent<setLine_script>().current_feature /
					//(float)source_object.GetComponent<setLine_script>().capacity; // divide by the capacity. need to normalize.

				target_object.GetComponent<penLine_script>().updateFeatureAction((float)source_object.GetComponent<setLine_script>().current_feature);
				//,(float)source_object.GetComponent<setLine_script>().capacity);
			}
			else
			{
				//map_param = (float)source_object.GetComponent<functionLine_script>().current_feature /
				//(float)source_object.GetComponent<functionLine_script>().max_function_value; // divide by the capacity. need to normalize.
				
				target_object.GetComponent<penLine_script>().updateFeatureAction((float)source_object.GetComponent<functionLine_script>().current_feature);
				//,(float)source_object.GetComponent<functionLine_script>().max_function_value);
			}

		}
	}

	public void setTargetToZero()
	{
		target_object.GetComponent<penLine_script>().updateFeatureAction(0);
	}

	public void updateTEXLabel()
	{
		if (edge_set)
		{
			// set the label
			if (source_object.tag == "set")
			{
				transform.GetChild(0).GetComponent<TEXDraw>().text = source_object.GetComponent<setLine_script>().this_set_name
					+ " " + full_condition_text;
			}
			else if (source_object.tag == "function")
			{
				transform.GetChild(0).GetComponent<TEXDraw>().text = 
					source_object.GetComponent<functionLine_script>().symbolic_expression.ToString()
					+ " " + full_condition_text;
			}

			// TODO: MAYBE SET ROTATION FIRST AND THEN ANGLE? OTHERWISE TOO FAR
			// set angle
			Vector3 v = target_object.transform.position - source_object.transform.position;
			transform.GetChild(0).GetComponent<TEXDraw>().transform.rotation = Quaternion.FromToRotation(Vector3.right, v);

			// set position
			transform.GetChild(0).GetComponent<TEXDraw>().transform.position =
				(source_object.transform.position + target_object.transform.position) / 2f;
		}
	}

	public void updateConditionFromEquation()
	{

		if (edge_set)
		{
			// is the source a set or a function?
			if (source_object.tag == "set")
			{
				if (checkCondition(source_object.GetComponent<setLine_script>().current_feature, rhs_equation))
				{
					condition_met = true;
				}
				else condition_met = false;
			}
			else if (source_object.tag == "function")
			{
				if (checkCondition(source_object.GetComponent<functionLine_script>().current_feature, rhs_equation))
				{
					condition_met = true;
				}
				else condition_met = false;
			}
		}
	}

	private bool checkCondition(float arg1, float arg2)
	{
		if (condition_operator == "=")
		{
			if (arg1 == arg2) return true;
			else return false;
		}
		else if (condition_operator == ">")
		{
			if (arg1 > arg2) return true;
			else return false;
		}
		else if (condition_operator == "<")
		{
			if (arg1 < arg2) return true;
			else return false;
		}
		else
			return false;
	}

	public void parseConditionString()
	{
		// get rid of all whitespaces
		full_condition_text.Replace(" ", "");
		// add a character in the beginning so splitting returns at least two elements
		full_condition_text = " " + full_condition_text;

		Debug.Log(full_condition_text);

		string[] spl = full_condition_text.Split('=');
		if (spl.Length > 1)
		{
			if (float.TryParse(spl[1], out rhs_equation))
			{
				parse_result = ParsingResult.Success;
				condition_operator = "=";
				return;
			}
			else
			{
				parse_result = ParsingResult.Fail;
				condition_operator = "";
				return;
			}
		}

		spl = full_condition_text.Split('<');
		if (spl.Length > 1)
		{
			if (float.TryParse(spl[1], out rhs_equation))
			{
				parse_result = ParsingResult.Success;
				condition_operator = "<";
				return;
			}
			else
			{
				parse_result = ParsingResult.Fail;
				condition_operator = "";
				return;
			}
		}

		spl = full_condition_text.Split('>');
		if (spl.Length > 1)
		{
			if (float.TryParse(spl[1], out rhs_equation))
			{
				parse_result = ParsingResult.Success;
				condition_operator = ">";
				return;
			}
			else
			{
				parse_result = ParsingResult.Fail;
				condition_operator = "";
				return;
			}
		}

		// if it reaches this point, then the parsing wasn't possible, the edge should act as it is, w/o any condition.
		//no_problem_in_parsing = true;
		parse_result = ParsingResult.Gibberish;
		condition_operator = "";

	}

	void Awake()
	{
		// start so that edge line acts normally without any condition
		parse_result = ParsingResult.Gibberish;
	}

	// Update is called once per frame
	void Update()
    {
		updateEdge();
    }
}
