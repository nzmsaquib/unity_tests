﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using Symbolism;

public class TextInputUI : MonoBehaviour, ISelectHandler
{
	string toparse = "";

	char[] ops = { '+', '-', '*', '/' };

	public static float set_radius = 60f;
	public static float function_radius = 200f;

	public GameObject paintable;

	bool alreadyCreated = false;

	// Start is called before the first frame update
	void Awake()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
		
		if (transform.GetComponent<TMP_InputField>().isFocused == false)
		{
			// process text input, if any
			parseInputAndCreate();

			// finally, destroy the text field
			if (alreadyCreated)
			{
				//paintable.GetComponent<Paintable_Script>().pa
				Destroy(this.gameObject);
			}
		}
		
	}

	public void OnSelect(BaseEventData eventData)
	{

	}

	public void OnDeselect(BaseEventData eventData)
	{
		// process text input, if any
		parseInputAndCreate();

		// finally, destroy the text field
		Destroy(this.gameObject);
	}

	public void OnSubmit(BaseEventData eventData)
	{
		// process text input, if any
		parseInputAndCreate();

		// finally, destroy the text field
		Destroy(this.gameObject);
	}

	public void parseInputAndCreate()
	{
		toparse = this.GetComponent<TMP_InputField>().text;

		// handle individual cases. When pressing submit/enter key, many copies of primitives get created. So make sure of create only one copy.
		// if contains "=", then create a set
		if (toparse.Split('=').Length > 1 && !alreadyCreated)
		{
			string setname = toparse.Split('=')[0];
			int val = int.Parse(toparse.Split('=')[1]);

			// create a set primitive
			List<Vector3> points = new List<Vector3>();
			Vector3 center = transform.position;
			points.Add(center + Vector3.up * set_radius);
			points.Add(center + Vector3.right * set_radius);
			points.Add(center + Vector3.down * set_radius);
			points.Add(center + Vector3.left * set_radius);

			GameObject templine = transform.GetComponent<CreatePrimitives>().CreateSet(points);

			templine.GetComponent<setLine_script>().this_set_name = setname;
			templine.GetComponent<setLine_script>().symbol_name = new Symbol(setname);
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = setname;

			templine.transform.SetParent(paintable.transform);

			GameObject dummy = Resources.Load<GameObject>("Prefabs/penLine_timer_default");
			dummy.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = Resources.Load<Mesh>("Models/TestPen");
			dummy.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = Color.black;

			// fill up the set with the desired value
			templine.GetComponent<setLine_script>().fillSetAtRandomPositions(dummy, val, 10f);

			templine.GetComponent<setLine_script>().retained_slider_value = 3;  // collapse the set into symbolic

			alreadyCreated = true;

			//Destroy(this.gameObject);
		}

		// if contains an arithmetic operator, create a function with sets as arguments
		else if (toparse.Split(ops).Length > 1 && !alreadyCreated)
		{
			char op = '+';
			if (toparse.Contains("+")) op = '+';
			else if (toparse.Contains("-")) op = '-';
			else if (toparse.Contains("*")) op = '*';
			else if (toparse.Contains("/")) op = '/';

			// create a function primitive
			List<Vector3> points = new List<Vector3>();
			Vector3 center = transform.position;
			points.Add(center + Vector3.up * function_radius);
			points.Add(center + Vector3.right * function_radius);
			points.Add(center + Vector3.down * function_radius);
			points.Add(center + Vector3.left * function_radius);

			GameObject templine = transform.GetComponent<CreatePrimitives>().CreateFunction(points);

			// set function operator
			if (toparse.Contains("+")) templine.GetComponent<functionLine_script>().current_op = templine.GetComponent<functionLine_script>().opPlus;
			else if (toparse.Contains("-")) templine.GetComponent<functionLine_script>().current_op = templine.GetComponent<functionLine_script>().opMinus;
			else if (toparse.Contains("*")) templine.GetComponent<functionLine_script>().current_op = templine.GetComponent<functionLine_script>().opMult;
			else if (toparse.Contains("/")) templine.GetComponent<functionLine_script>().current_op = templine.GetComponent<functionLine_script>().opDiv;

			// update argument_label
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
				templine.GetComponent<functionLine_script>().currentOperator();

			templine.transform.SetParent(paintable.transform);

			GameObject func = templine;

			// create two sets
			// ========================
			// first argument
			points.Clear();
			center = transform.position + Vector3.left * 100;
			points.Add(center + Vector3.up * set_radius);
			points.Add(center + Vector3.right * set_radius);
			points.Add(center + Vector3.down * set_radius);
			points.Add(center + Vector3.left * set_radius);

			templine = transform.GetComponent<CreatePrimitives>().CreateSet(points);

			templine.GetComponent<setLine_script>().this_set_name = toparse.Split(op)[0];
			templine.GetComponent<setLine_script>().symbol_name = new Symbol(toparse.Split(op)[0]);
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = toparse.Split(op)[0];

			templine.transform.SetParent(func.transform);

			GameObject dummy = Resources.Load<GameObject>("Prefabs/penLine_timer_default");
			dummy.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = Resources.Load<Mesh>("Models/TestPen");
			dummy.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = Color.black;

			// fill up the set with the desired value
			//templine.GetComponent<setLine_script>().fillSetAtRandomPositions(dummy, 0, 10f);

			templine.GetComponent<setLine_script>().retained_slider_value = 3;  // collapse the set into symbolic

			// =======================
			// second argument

			points.Clear();
			center = transform.position + Vector3.right * 100;
			points.Add(center + Vector3.up * set_radius);
			points.Add(center + Vector3.right * set_radius);
			points.Add(center + Vector3.down * set_radius);
			points.Add(center + Vector3.left * set_radius);

			templine = transform.GetComponent<CreatePrimitives>().CreateSet(points);

			templine.GetComponent<setLine_script>().this_set_name = toparse.Split(op)[1];
			templine.GetComponent<setLine_script>().symbol_name = new Symbol(toparse.Split(op)[1]);
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = toparse.Split(op)[1];

			templine.transform.SetParent(func.transform);

			// fill up the set with the desired value
			//templine.GetComponent<setLine_script>().fillSetAtRandomPositions(dummy, 0, 10f);

			templine.GetComponent<setLine_script>().retained_slider_value = 3;  // collapse the set into symbolic

			// ====================
			// collapse the function to symbolic
			func.GetComponent<functionLine_script>().retained_slider_value = 3;

			// update feature
			func.GetComponent<functionLine_script>().updateFeature();

			alreadyCreated = true;
		}
		else
		{
			// do nothing
		}
	}
}