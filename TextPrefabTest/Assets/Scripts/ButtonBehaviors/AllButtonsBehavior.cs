﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class AllButtonsBehavior : MonoBehaviour
{
	public bool selected = false;

	public GameObject ColorPicker, PenWidthSlider;
	public GameObject pickerInstance, penWidthSliderInstance;

	// for predictive pen or freeform pen selection menu
	public bool isPredictivePen = false;

	// for set or function stroke conversion menu
	public bool isFunctionStrokeConversion = false;

	// for path definition selection menu
	public bool isContinuousPathDefinition = true;

	// for edge type selection menu
	public bool isCausalEdge = true;

	public static bool isPointerOverStaticPen = false;
	public static bool isPointerOverEdgeButton = false;
	public static bool isPointerOverEraserButton = false;
	public static bool isPointerOverFunctionButton = false;
	public static bool isPointerOverPan = false;
	public static bool isPointerOverPencil = false;
	public static bool isPointerOverSelect = false;
	public static bool isPointerOverStrokeConvert = false;
	public static bool isPointerOverPathDefinition = false;
	public static bool isPointerOverCopy = false;
	public static bool isPointerOverTextInput = false;

	GameObject[] buttons;

	public void whenSelected()
	{
		selected = true;

		// change icon color
		transform.GetComponent<Image>().color = new Color(0f, 0f, 0f, 1f); //new Color(1, 1, 1, 0.5f);

		// change scale
		transform.localScale = new Vector3(1.25f, 1.25f, 1.25f);

		if (this.name == "Pan")
		{
			// enable all colliders to move primitives around
			enableAllPenObjectColliders();
			enableAllSetAnchorColliders();
			enableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//showGlobalSlider();
		}
		else if (this.name == "Select")
		{
			disableAllPenObjectColliders();
			disableAllSetAnchorColliders();
			disableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}
		else if (this.name == "Pencil")
		{
			// allow drawing over existing pen/set etc. objects without interfering
			disableAllPenObjectColliders();
			disableAllSetAnchorColliders();
			disableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();

			// Create a color picker and a pen width slider
			if (pickerInstance == null)
			{
				pickerInstance = Instantiate(ColorPicker, new Vector3(100, 100, 0), Quaternion.identity, transform.parent);
				pickerInstance.name = "PencilPicker";
				// restructure some of the sizes
				pickerInstance.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
				pickerInstance.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
				pickerInstance.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

				pickerInstance.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(10, 62);
				pickerInstance.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(15, 10, 0);
				pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(90, 90);
				pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);

				//pickerInstance.GetComponent<RectTransform>().localPosition = new Vector3(-178, -470, 0);
				pickerInstance.GetComponent<RectTransform>().position = new Vector3(-5, 10, 0);

				//pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().offsetMin = new Vector2(-110, 35);   // left and bottom
				//pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().offsetMax = new Vector2(110, -35);   // right and top
			}
			pickerInstance.gameObject.SetActive(true);

			if (penWidthSliderInstance == null)
			{
				penWidthSliderInstance = Instantiate(PenWidthSlider, new Vector3(0, 0, 0), Quaternion.identity, transform.parent);
				penWidthSliderInstance.name = "penSlider";

				penWidthSliderInstance.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
				penWidthSliderInstance.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
				penWidthSliderInstance.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

				penWidthSliderInstance.GetComponent<RectTransform>().position = new Vector3(80, 10, 0);
			}
			penWidthSliderInstance.gameObject.SetActive(true);
		}

		else if (this.name == "StaticPen")
		{
			// Create a color picker and a pen width slider
			if (pickerInstance == null)
			{
				pickerInstance = Instantiate(ColorPicker, new Vector3(100, 100, 0), Quaternion.identity, transform.parent);
				pickerInstance.name = "staticPencilPicker";

				pickerInstance.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
				pickerInstance.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
				pickerInstance.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

				// restructure some of the sizes
				pickerInstance.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(10, 62);
				pickerInstance.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(15, 10, 0);
				pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(90, 90);
				pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);

				pickerInstance.GetComponent<RectTransform>().position = new Vector3(-5, 10, 0);

				//pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().offsetMin = new Vector2(-110, 35);   // left and bottom
				//pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().offsetMax = new Vector2(110, -35);   // right and top
			}
			pickerInstance.gameObject.SetActive(true);

			if (penWidthSliderInstance == null)
			{
				penWidthSliderInstance = Instantiate(PenWidthSlider, new Vector3(0, 0, 0), Quaternion.identity, transform.parent);
				penWidthSliderInstance.name = "staticPenSlider";

				penWidthSliderInstance.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
				penWidthSliderInstance.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
				penWidthSliderInstance.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

				penWidthSliderInstance.GetComponent<RectTransform>().position = new Vector3(80, 10, 0);
			}
			penWidthSliderInstance.gameObject.SetActive(true);
		}

		else if (this.name == "Eraser")
		{
			enableAllPenObjectColliders();
			enableAllSetAnchorColliders();
			enableAllFunctionAnchorColliders();
			showAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		else if (this.name == "Edge_draw")
		{
			disableAllPenObjectColliders();
			enableAllSetAnchorColliders();
			enableAllFunctionAnchorColliders();
			showAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		else if (this.name == "FunctionButton")
		{
			disableAllPenObjectColliders();
			disableAllSetAnchorColliders();
			disableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		else if (this.name == "Stroke_Conversion")
		{
			enableAllPenObjectColliders();
			disableAllSetAnchorColliders();
			disableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		else if (this.name == "Path_Definition")
		{
			enableAllPenObjectColliders();
			disableAllSetAnchorColliders();
			disableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		else if (this.name == "Copy")
		{
			enableAllPenObjectColliders();
			enableAllSetAnchorColliders();
			enableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		else if (this.name == "Text_Input")
		{
			disableAllPenObjectColliders();
			disableAllSetAnchorColliders();
			disableAllFunctionAnchorColliders();
			hideAllEdgeLines();
			disableAllRecordPathBools();

			//hideGlobalSlider();
		}

		// deselect all other buttons
		for (int i = 0; i < buttons.Length; i++)
		{
			if (buttons[i].name == this.name) continue;

			buttons[i].GetComponent<AllButtonsBehavior>().whenDeselected();
		}
	}

	public void whenDeselected()
	{
		selected = false;

		// change icon color
		transform.GetComponent<Image>().color = new Color(0.4f, 0.4f, 0.4f, 1f);

		// change scale
		transform.localScale = new Vector3(1f, 1f, 1f);


		if (this.name == "Pencil")
		{
			if (pickerInstance != null)
			{
				pickerInstance.gameObject.SetActive(false);
			}

			if (penWidthSliderInstance != null)
			{
				penWidthSliderInstance.gameObject.SetActive(false);
			}
		}

		if (this.name == "StaticPen")
		{
			if (pickerInstance != null)
			{
				pickerInstance.gameObject.SetActive(false);
			}

			if (penWidthSliderInstance != null)
			{
				penWidthSliderInstance.gameObject.SetActive(false);
			}
		}
	}

	public void showGlobalSlider()
	{
		transform.parent.Find("Global_Stroke_Details").gameObject.SetActive(true);
	}

	public void hideGlobalSlider()
	{
		transform.parent.Find("Global_Stroke_Details").gameObject.SetActive(false);
	}

	public void enableAllPenObjectColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void disableAllPenObjectColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void enableAllSetAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] setobjs = GameObject.FindGameObjectsWithTag("set");
		for (int i = 0; i < setobjs.Length; i++)
		{
			setobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void disableAllSetAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] setobjs = GameObject.FindGameObjectsWithTag("set");
		for (int i = 0; i < setobjs.Length; i++)
		{
			setobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void enableAllFunctionAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] funcobjs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcobjs.Length; i++)
		{
			funcobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void disableAllFunctionAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] funcobjs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcobjs.Length; i++)
		{
			funcobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void showAllEdgeLines()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("edgeline");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().SetPosition(0,
			edgeobjs[i].GetComponent<edgeLine_script>().source_object.transform.GetChild(0).transform.position);
			edgeobjs[i].GetComponent<LineRenderer>().SetPosition(1,
			edgeobjs[i].GetComponent<edgeLine_script>().target_object.transform.position);

			var edgepoints = new List<Vector3>() { edgeobjs[i].GetComponent<LineRenderer>().GetPosition(0),
								edgeobjs[i].GetComponent<LineRenderer>().GetPosition(1)};

			edgeobjs[i].GetComponent<EdgeCollider2D>().points = edgepoints.Select(x =>
			{
				var pos = edgeobjs[i].GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
				return new Vector2(pos.x, pos.y);
			}).ToArray();

			edgeobjs[i].GetComponent<LineRenderer>().enabled = true;
			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = true;

			edgeobjs[i].transform.GetChild(0).GetComponent<TEXDraw>().enabled = true;
			edgeobjs[i].GetComponent<edgeLine_script>().updateTEXLabel();

			// set line renderer texture scale
			var linedist = Vector3.Distance(edgeobjs[i].GetComponent<LineRenderer>().GetPosition(0),
				edgeobjs[i].GetComponent<LineRenderer>().GetPosition(1));
			edgeobjs[i].GetComponent<LineRenderer>().materials[0].mainTextureScale = new Vector3(linedist, 1, 1);
		}

		showAllSpawnEdges();
	}

	public void hideAllEdgeLines()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("edgeline");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().enabled = false;

			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = false;

			edgeobjs[i].transform.GetChild(0).GetComponent<TEXDraw>().enabled = false;
		}

		hideAllSpawnEdges();
	}

	public void showAllSpawnEdges()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("spawn_edge");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().SetPosition(0,
			edgeobjs[i].GetComponent<SpawnEdgeScript>().source_object.transform.GetChild(0).transform.position);
			edgeobjs[i].GetComponent<LineRenderer>().SetPosition(1,
			edgeobjs[i].GetComponent<SpawnEdgeScript>().target_object.transform.position);

			var edgepoints = new List<Vector3>() { edgeobjs[i].GetComponent<LineRenderer>().GetPosition(0),
								edgeobjs[i].GetComponent<LineRenderer>().GetPosition(1)};

			edgeobjs[i].GetComponent<EdgeCollider2D>().points = edgepoints.Select(x =>
			{
				var pos = edgeobjs[i].GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
				return new Vector2(pos.x, pos.y);
			}).ToArray();

			edgeobjs[i].GetComponent<LineRenderer>().enabled = true;
			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = true;

			// set line renderer texture scale
			var linedist = Vector3.Distance(edgeobjs[i].GetComponent<LineRenderer>().GetPosition(0),
				edgeobjs[i].GetComponent<LineRenderer>().GetPosition(1));
			edgeobjs[i].GetComponent<LineRenderer>().materials[0].mainTextureScale = new Vector3(linedist, 1, 1);
		}
	}

	public void hideAllSpawnEdges()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("spawn_edge");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().enabled = false;

			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = false;
		}
	}

	public void showAllPropertyEdgeLines()
	{
		GameObject[] pedges = GameObject.FindGameObjectsWithTag("oop_property_edge");
		for (int i = 0; i < pedges.Length; i++)
		{
			pedges[i].GetComponent<OOP_Property_Edge_script>().showPropertyEdge = true;
		}
	}

	public void hideAllPropertyEdgeLines()
	{
		GameObject[] pedges = GameObject.FindGameObjectsWithTag("oop_property_edge");
		for (int i = 0; i < pedges.Length; i++)
		{
			pedges[i].GetComponent<OOP_Property_Edge_script>().showPropertyEdge = false;
		}
	}

	public void disableAllRecordPathBools()
	{
		// in case a new button is pressed in the middle of a discrete record path, stop the recording.
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<penLine_script>().record_path_enable = false;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        buttons = GameObject.FindGameObjectsWithTag("canvas_mode_button");
	}

    // Update is called once per frame
    void Update()
    {
		if (EventSystem.current.IsPointerOverGameObject(0))
		{
			if(this.name == "Pan")
				isPointerOverPan = true;
			else isPointerOverPan = false;

			if (this.name == "Select")
				isPointerOverSelect = true;
			else isPointerOverSelect = false;

			if (this.name == "Pencil")
				isPointerOverPencil = true;
			else isPointerOverPencil = false;

			if (this.name == "Edge_draw")
				isPointerOverEdgeButton = true;
			else isPointerOverEdgeButton = false;

			if (this.name == "FunctionButton")
				isPointerOverFunctionButton = true;
			else isPointerOverFunctionButton = false;

			if (this.name == "StaticPen")
				isPointerOverStaticPen = true;
			else isPointerOverStaticPen = false;

			if (this.name == "Text_Input")
				isPointerOverTextInput = true;
			else isPointerOverTextInput = false;

			if (this.name == "Copy")
				isPointerOverCopy = true;
			else isPointerOverCopy = false;

			if (this.name == "Path_Definition")
				isPointerOverPathDefinition = true;
			else isPointerOverPathDefinition = false;

			if (this.name == "Stroke_Conversion")
				isPointerOverStrokeConvert = true;
			else isPointerOverStrokeConvert = false;
		}
		else
		{
			isPointerOverPan = false;
			isPointerOverSelect = false;
			isPointerOverPencil = false;
			isPointerOverEdgeButton = false;
			isPointerOverFunctionButton = false;
			isPointerOverStaticPen = false;
			isPointerOverTextInput = false;
			isPointerOverCopy = false;
			isPointerOverPathDefinition = false;
			isPointerOverStrokeConvert = false;
		}
	}
}
