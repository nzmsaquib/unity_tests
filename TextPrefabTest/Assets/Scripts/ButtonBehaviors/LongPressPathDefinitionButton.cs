﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LongPressPathDefinitionButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
	[SerializeField]
	[Tooltip("How long must pointer be down on this object to trigger a long press")]
	private float holdTime = 1f;

	private Vector2 pressPoint;
	private GameObject radmenu;

	public GameObject path_definition_menu;
	public Canvas canvas_radial;
	public Canvas canvas_button;

	public Sprite continuous_image, discrete_image;

	//private bool held = false;
	//public UnityEvent onClick = new UnityEvent();

	public UnityEvent onLongPress = new UnityEvent();

	public void OnPointerDown(PointerEventData eventData)
	{
		//held = false;
		if (eventData.selectedObject.transform.parent.name == "Canvas")
		{
			Invoke("OnLongPress", holdTime);

			pressPoint = eventData.pressPosition;
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");
	}

	void OnLongPress()
	{
		//held = true;
		onLongPress.Invoke();

		//Debug.Log("button long pressed. " + worldCoord.ToString());
	}

	public void CreatePathDefinitionSelectionMenu()
	{
		// create a menu and show it, if one doesn't exist
		if (GameObject.Find("Path_Definition_Selection_Menu") == null)
		{
			radmenu = Instantiate(path_definition_menu,
				pressPoint + new Vector2(0f, -50f),
				Quaternion.identity,
				canvas_button.transform);

			radmenu.name = "Path_Definition_Selection_Menu";

			//radmenu.transform.Find("set_selection").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
			//radmenu.transform.Find("function_selection").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);

			// onClick events
			radmenu.transform.Find("path_continuous").GetComponent<Button>().onClick.AddListener(() => SelectContinuous());
			radmenu.transform.Find("path_discrete").GetComponent<Button>().onClick.AddListener(() => SelectDiscrete());
		}

	}

	public void SelectContinuous()
	{
		GameObject.Find("Path_Definition").GetComponent<Button>().GetComponent<Image>().sprite = continuous_image;
		GameObject.Find("Path_Definition").GetComponent<AllButtonsBehavior>().isContinuousPathDefinition = true;

		Destroy(radmenu);
	}

	public void SelectDiscrete()
	{
		GameObject.Find("Path_Definition").GetComponent<Button>().GetComponent<Image>().sprite = discrete_image;
		GameObject.Find("Path_Definition").GetComponent<AllButtonsBehavior>().isContinuousPathDefinition = false;

		Destroy(radmenu);
	}
}
