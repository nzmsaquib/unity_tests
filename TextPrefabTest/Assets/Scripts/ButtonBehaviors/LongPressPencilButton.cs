﻿// Taken from: https://forum.unity.com/threads/long-press-gesture-on-ugui-button.264388/
// See discussions etc. to understand the script's behavior

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using uPIe;

public class LongPressPencilButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
	[SerializeField]
	[Tooltip("How long must pointer be down on this object to trigger a long press")]
	private float holdTime = 1f;

	private Vector3 worldCoord;
	private GameObject radmenu;

	public GameObject pen_selection_menu;
	public Canvas canvas_radial;

	public Sprite predictive_pen_image, standard_pen_image, straightline_pen_image;

	//private bool held = false;
	//public UnityEvent onClick = new UnityEvent();

	public UnityEvent onLongPress = new UnityEvent();

	public void OnPointerDown(PointerEventData eventData)
	{
		//held = false;
		if (eventData.selectedObject.transform.parent.name == "Canvas")
		{
			Invoke("OnLongPress", holdTime);

			//worldCoord = Camera.main.ScreenToWorldPoint(GetComponent<RectTransform>().position);
			worldCoord = Camera.main.ScreenToWorldPoint(eventData.pressPosition);
			// set z, otherwise it gets set to camera position.z: -880f.
			worldCoord.z = -50f;
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");

		//if (!held)
		//    onClick.Invoke();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");
	}

	void OnLongPress()
	{
		//held = true;
		onLongPress.Invoke();

		//Debug.Log("button long pressed. " + worldCoord.ToString());
	}

	public void CreatePencilSelectionMenu()
	{
		// create a menu and show it
		radmenu = Instantiate(pen_selection_menu,
			worldCoord + new Vector3(0f, -50f, 0f),
			Quaternion.identity,
			canvas_radial.transform);

		radmenu.transform.Find("predictive_pen").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
		radmenu.transform.Find("standard_pen").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);

		// onClick events
		radmenu.transform.Find("predictive_pen").GetComponent<Button>().onClick.AddListener(() => SelectPredictivePen());
		radmenu.transform.Find("standard_pen").GetComponent<Button>().onClick.AddListener(() => SelectStandardPen());
	}

	public void SelectPredictivePen()
	{
		GameObject.Find("Pencil").GetComponent<Button>().GetComponent<Image>().sprite = predictive_pen_image;
		GameObject.Find("Pencil").GetComponent<AllButtonsBehavior>().isPredictivePen = true;

		Destroy(radmenu);
	}

	public void SelectStandardPen()
	{
		GameObject.Find("Pencil").GetComponent<Button>().GetComponent<Image>().sprite = standard_pen_image;
		GameObject.Find("Pencil").GetComponent<AllButtonsBehavior>().isPredictivePen = false;

		Destroy(radmenu);
	}
}