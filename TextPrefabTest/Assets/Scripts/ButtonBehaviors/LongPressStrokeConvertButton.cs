﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LongPressStrokeConvertButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
	[SerializeField]
	[Tooltip("How long must pointer be down on this object to trigger a long press")]
	private float holdTime = 1f;

	private Vector3 worldCoord;
	private Vector2 pressPoint;
	private GameObject radmenu;

	public GameObject stroke_conversion_menu;
	public Canvas canvas_radial;
	public Canvas canvas_button;

	public Sprite set_image, function_image;

	//private bool held = false;
	//public UnityEvent onClick = new UnityEvent();

	public UnityEvent onLongPress = new UnityEvent();

	public void OnPointerDown(PointerEventData eventData)
	{
		//held = false;
		if (eventData.selectedObject.transform.parent.name == "Canvas")
		{
			Invoke("OnLongPress", holdTime);

			//worldCoord = Camera.main.ScreenToWorldPoint(GetComponent<RectTransform>().position);
			worldCoord = Camera.main.ScreenToWorldPoint(eventData.pressPosition);
			// set z, otherwise it gets set to camera position.z: -880f.
			worldCoord.z = -50f;

			pressPoint = eventData.pressPosition;
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");

		//if (!held)
		//    onClick.Invoke();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");
	}

	void OnLongPress()
	{
		//held = true;
		onLongPress.Invoke();

		//Debug.Log("button long pressed. " + worldCoord.ToString());
	}

	public void CreateStrokeConvertSelectionMenu()
	{
		// create a menu and show it, if one doesn't exist
		if (GameObject.Find("Stroke_Conversion_Selection_Menu") == null)
		{
			radmenu = Instantiate(stroke_conversion_menu,
				pressPoint + new Vector2(0f, -50f),
				Quaternion.identity,
				canvas_button.transform);

			radmenu.name = "Stroke_Conversion_Selection_Menu";

			//radmenu.transform.Find("set_selection").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
			//radmenu.transform.Find("function_selection").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);

			// onClick events
			radmenu.transform.Find("set_selection").GetComponent<Button>().onClick.AddListener(() => SelectSetPen());
			radmenu.transform.Find("function_selection").GetComponent<Button>().onClick.AddListener(() => SelectFunctionPen());
		}

	}

	public void SelectSetPen()
	{
		GameObject.Find("Stroke_Conversion").GetComponent<Button>().GetComponent<Image>().sprite = set_image;
		GameObject.Find("Stroke_Conversion").GetComponent<AllButtonsBehavior>().isFunctionStrokeConversion = false;

		Destroy(radmenu);
	}

	public void SelectFunctionPen()
	{
		GameObject.Find("Stroke_Conversion").GetComponent<Button>().GetComponent<Image>().sprite = function_image;
		GameObject.Find("Stroke_Conversion").GetComponent<AllButtonsBehavior>().isFunctionStrokeConversion = true;

		Destroy(radmenu);
	}
}
