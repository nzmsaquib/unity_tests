﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PencilButtonBehavior : MonoBehaviour
{
	public bool selected = false;
	public bool isPredictivePen = false;

	public static bool isPointerOverPencil = false;

	public GameObject paintable_object;

	public GameObject ColorPicker, PenWidthSlider;
	public GameObject pickerInstance, penWidthSliderInstance;

	public void whenSelected()
	{
		selected = true;

		// change icon color
		transform.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

		// allow drawing over existing pen/set etc. objects without interfering
		disableAllPenObjectColliders();
		disableAllSetAnchorColliders();
		disableAllFunctionAnchorColliders();
		hideAllEdgeLines();

		hideGlobalSlider();

		transform.parent.Find("Select").GetComponent<SelectButtonBehavior>().whenDeselected();
		transform.parent.Find("Pan").GetComponent<PanButtonBehavior>().whenDeselected();
		transform.parent.Find("Edge_draw").GetComponent<EdgeButtonBehavior>().whenDeselected();
		transform.parent.Find("FunctionButton").GetComponent<FunctionButtonBehavior>().whenDeselected();
		transform.parent.Find("Eraser").GetComponent<EraserButtonBehavior>().whenDeselected();
		transform.parent.Find("StaticPen").GetComponent<StaticPenButtonBehavior>().whenDeselected();

		// Create a color picker and a pen width slider
		if (pickerInstance == null)
		{
			pickerInstance = Instantiate(ColorPicker, new Vector3(100, 100, 0), Quaternion.identity, transform.parent);
			pickerInstance.name = "PencilPicker";
			// restructure some of the sizes
			pickerInstance.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
			pickerInstance.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
			pickerInstance.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

			pickerInstance.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(10, 62);
			pickerInstance.transform.GetChild(0).GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(15, 10, 0);
			pickerInstance.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(90, 90);
			pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);

			//pickerInstance.GetComponent<RectTransform>().localPosition = new Vector3(-178, -470, 0);
			pickerInstance.GetComponent<RectTransform>().position = new Vector3(-5, 10, 0);

			//pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().offsetMin = new Vector2(-110, 35);   // left and bottom
			//pickerInstance.transform.GetChild(0).GetComponent<RectTransform>().offsetMax = new Vector2(110, -35);   // right and top
		}
		pickerInstance.gameObject.SetActive(true);

		if (penWidthSliderInstance == null)
		{
			penWidthSliderInstance = Instantiate(PenWidthSlider, new Vector3(0, 0, 0), Quaternion.identity, transform.parent);
			penWidthSliderInstance.name = "penSlider";

			penWidthSliderInstance.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
			penWidthSliderInstance.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
			penWidthSliderInstance.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);

			penWidthSliderInstance.GetComponent<RectTransform>().position = new Vector3(80, 10, 0);
		}
		penWidthSliderInstance.gameObject.SetActive(true);
	}

	public void whenDeselected()
	{
		selected = false;

		// change icon color
		transform.GetComponent<Image>().color = new Color(1, 1, 1, 1);

		if (pickerInstance != null)
		{
			pickerInstance.gameObject.SetActive(false);
		}

		if (penWidthSliderInstance != null)
		{
			penWidthSliderInstance.gameObject.SetActive(false);
		}
	}

	public void hideGlobalSlider()
	{
		transform.parent.Find("Global_Stroke_Details").gameObject.SetActive(false);
	}

	public void disableAllPenObjectColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void disableAllSetAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] setobjs = GameObject.FindGameObjectsWithTag("set");
		for (int i = 0; i < setobjs.Length; i++)
		{
			setobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void disableAllFunctionAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] funcobjs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcobjs.Length; i++)
		{
			funcobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void enableAllPenObjectColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void hideAllEdgeLines()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("edgeline");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().enabled = false;

			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = false;

			edgeobjs[i].transform.GetChild(0).GetComponent<TEXDraw>().enabled = false;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
		paintable_object = GameObject.FindGameObjectWithTag("paintable_canvas_object");
    }

    // Update is called once per frame
    void Update()
    {
		if (EventSystem.current.IsPointerOverGameObject(0))
		{
			isPointerOverPencil = true;
		}
		else
		{
			isPointerOverPencil = false;
		}
    }
}
