﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class EraserButtonBehavior : MonoBehaviour
{
	public bool selected = false;

	public static bool isPointerOverEraserButton = false;

	public GameObject paintable_object;

	public void whenSelected()
	{
		selected = true;

		// change icon color
		transform.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

		enableAllPenObjectColliders();
		enableAllSetAnchorColliders();
		enableAllFunctionAnchorColliders();
		showAllEdgeLines();

		hideGlobalSlider();

		transform.parent.Find("Select").GetComponent<SelectButtonBehavior>().whenDeselected();
		transform.parent.Find("Pencil").GetComponent<PencilButtonBehavior>().whenDeselected();
		transform.parent.Find("Edge_draw").GetComponent<EdgeButtonBehavior>().whenDeselected();
		transform.parent.Find("Pan").GetComponent<PanButtonBehavior>().whenDeselected();
		transform.parent.Find("FunctionButton").GetComponent<FunctionButtonBehavior>().whenDeselected();
		transform.parent.Find("StaticPen").GetComponent<StaticPenButtonBehavior>().whenDeselected();
	}

	public void whenDeselected()
	{
		selected = false;

		// change icon color
		transform.GetComponent<Image>().color = new Color(1, 1, 1, 1);
	}

	public void hideGlobalSlider()
	{
		transform.parent.Find("Global_Stroke_Details").gameObject.SetActive(false);
	}

	public void enableAllPenObjectColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void enableAllSetAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] setobjs = GameObject.FindGameObjectsWithTag("set");
		for (int i = 0; i < setobjs.Length; i++)
		{
			setobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void enableAllFunctionAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] funcobjs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcobjs.Length; i++)
		{
			funcobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
		}
	}

	public void showAllEdgeLines()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("edgeline");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().SetPosition(0,
			edgeobjs[i].GetComponent<edgeLine_script>().source_object.transform.GetChild(0).transform.position);
			edgeobjs[i].GetComponent<LineRenderer>().SetPosition(1,
			edgeobjs[i].GetComponent<edgeLine_script>().target_object.transform.position);

			var edgepoints = new List<Vector3>() { edgeobjs[i].GetComponent<LineRenderer>().GetPosition(0),
								edgeobjs[i].GetComponent<LineRenderer>().GetPosition(1)};

			edgeobjs[i].GetComponent<EdgeCollider2D>().points = edgepoints.Select(x =>
			{
				var pos = edgeobjs[i].GetComponent<EdgeCollider2D>().transform.InverseTransformPoint(x);
				return new Vector2(pos.x, pos.y);
			}).ToArray();

			edgeobjs[i].GetComponent<LineRenderer>().enabled = true;
			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = true;

			edgeobjs[i].transform.GetChild(0).GetComponent<TEXDraw>().enabled = true;
			edgeobjs[i].GetComponent<edgeLine_script>().updateTEXLabel();

			// set line renderer texture scale
			var linedist = Vector3.Distance(edgeobjs[i].GetComponent<LineRenderer>().GetPosition(0),
				edgeobjs[i].GetComponent<LineRenderer>().GetPosition(1));
			edgeobjs[i].GetComponent<LineRenderer>().materials[0].mainTextureScale = new Vector3(linedist, 1, 1);
		}
	}

	// Start is called before the first frame update
	void Start()
	{
		paintable_object = GameObject.FindGameObjectWithTag("paintable_canvas_object");
	}

	// Update is called once per frame
	void Update()
	{
		if (EventSystem.current.IsPointerOverGameObject(0))
		{
			isPointerOverEraserButton = true;
		}
		else
		{
			isPointerOverEraserButton = false;
		}
	}
}
