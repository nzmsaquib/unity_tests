﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ArgumentLabelButtonBehavior : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

	public bool buttonPressed;

	public void OnPointerDown(PointerEventData eventData)
	{
		buttonPressed = true;
		//Debug.Log("button down");
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		buttonPressed = false;
		//Debug.Log("button up");
	}
}