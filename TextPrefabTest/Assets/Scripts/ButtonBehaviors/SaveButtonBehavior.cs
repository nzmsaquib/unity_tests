﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using SimpleFileBrowser;
using System.IO.Compression;

public class SaveButtonBehavior : MonoBehaviour
{
	public bool iAmDisabled = false;

	public DateTime sessionStart;

	//public string savefilename;

	public static ES3File savefile;

	public void disableMyself()
	{
		iAmDisabled = true;
	}

	public void enableMyself()
	{
		iAmDisabled = false;
	}

	public void SaveDialog()
	{
		// don't save when some critical operation is running
		if (iAmDisabled) return;

		// Change the mode to pan
		transform.parent.Find("Pan").GetComponent<AllButtonsBehavior>().whenSelected();

		FileBrowser.SetFilters(false, new FileBrowser.Filter("Noyon sketches", ".es3"));

		FileBrowser.SetDefaultFilter(".es3");

		FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");

		FileBrowser.AddQuickLink("Users", "C:\\Users", null);

		StartCoroutine(ShowSaveDialogCoroutine());
	}

	IEnumerator ShowSaveDialogCoroutine()
	{
		// Show a save file dialog and wait for a response from user
		// Save file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
		yield return FileBrowser.WaitForSaveDialog(false, null, "Save Sketch", "Save Sketch");

		// Dialog is closed
		// Print whether a file is chosen (FileBrowser.Success)
		// and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
		Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);

		if (FileBrowser.Success)
		{
			// If a file was chosen, read its bytes via FileBrowserHelpers
			// Contrary to File.ReadAllBytes, this function works on Android 10+, as well
			//byte[] bytes = FileBrowserHelpers.ReadBytesFromFile(FileBrowser.Result);
			//savefilename = FileBrowser.Result;

			savefile = new ES3File(FileBrowser.Result);

			// NOTE for OOP Property Edge: BEFORE SAVING ANYTHING, CLEAN UP ALL TARGET SETS' PEN OBJECTS, OTHERWISE THE DUMMY PENOBJS MESS WITH
			// ACTUAL NUMBER OF PEN OBJECTS, AND EVENTUALLY THEIR SAVING AND LOADING

			GameObject[] proplines = GameObject.FindGameObjectsWithTag("oop_property_edge");

			for (int i = 0; i < proplines.Length; i++)
			{
				// Halt update so new pen objects are not filled until saving is done
				proplines[i].GetComponent<OOP_Property_Edge_script>().doNotUpdate = true;

				// remove all children of the target property set
				proplines[i].GetComponent<OOP_Property_Edge_script>().propertyTargetSet.GetComponent<setLine_script>().removeAllChildren();
			}

			//penLine_timer_default(Clone)

			// NOTE: DO THE SAME FOR SET OBJECTS WITH ACTIVATED TIMER: CLEAN UP DUMMY PEN OBJECTS
			GameObject[] sets = GameObject.FindGameObjectsWithTag("set");

			for (int i = 0; i < sets.Length; i++)
			{
				if (sets[i].GetComponent<setLine_script>().timer_activated)
				{
					// halt timer update
					sets[i].GetComponent<setLine_script>().halt_timer_update_while_saving = true;

					if (sets[i].GetComponent<setLine_script>().objectToCopy.name.Contains("penLine_timer_default"))
					{
						// remove all dummy children of the timer set.
						sets[i].GetComponent<setLine_script>().removeAllChildren();
					}

					else
					{
						// If not dummy child, remove all children of the timer set, except the first element. objectToCopy refers to this pen obj. too.
						sets[i].GetComponent<setLine_script>().deleteChildren(sets[i].transform.childCount - 3);
					}
				}
			}

			// NOTE: STOP ALL SPAWN EDGE ACTIVITIES, THAT DESTROYS ALL CURRENTLY SPAWNED ELEMENTS TOO
			GameObject[] sp = GameObject.FindGameObjectsWithTag("spawn_edge");
			for (int i = 0; i < sp.Length; i++)
			{
				sp[i].GetComponent<SpawnEdgeScript>().play = false;
			}

			/*
			// NOTE: CLEAN UP DUMMY PEN OBJECTS THAT HAVE "(Cloned)" IN THEIR NAMES
			GameObject[] pens = GameObject.FindGameObjectsWithTag("penline");
			for (int i = pens.Length; i > -1; i--)
			{
				if (pens[i].name.Contains("Cloned"))
				{
					Destroy(pens[i]);
				}
			}
			*/

			// Let the things destined to be removed disappear, and call SaveSketch next frame
			Invoke("SaveSketch", 0);
			
			//SaveSketch();
		}
	}

	public void SaveSketch()
	{

		// Assign a filename: not needed with file browser dialog
		//DateTime dt = DateTime.Now;
		//savefilename = dt.ToString("yyyy-MM-dd-hh-mm-ss") + ".es3";

		// Halt all interaction operations while I/O in progress
		GameObject.FindGameObjectWithTag("paintable_canvas_object").GetComponent<Paintable_Script>().IOInProgress = true;

		// Show a message
		//GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text = "Saving/Loading...";
		StartCoroutine(ShowMessageCoRoutine());

		// dummy save
		//ES3.Save<bool>("For.SaveData.ES3", true);

		// REMEMBER THE HIERARCHY, AND THEN UNLINK EVERYTHING SO EVERYONE'S PARENT IS Paintable
		savefile.Save<string>("PaintableHierarchy", Traverse(GameObject.FindGameObjectWithTag("paintable_canvas_object")));

		GameObject[] plines = GameObject.FindGameObjectsWithTag("penline");
		GameObject[] setlines = GameObject.FindGameObjectsWithTag("set");
		GameObject[] funclines = GameObject.FindGameObjectsWithTag("function");

		for (int i = 0; i < setlines.Length; i++)
		{
			// Children primitives (either penline or setline)
			for (int j = 2; j < setlines[i].transform.childCount; j++)
			{
				savefile.Save<string>(setlines[i].name + ".Children.Name." + j.ToString(), setlines[i].transform.GetChild(j).name);
			}
		}

		for (int i = 0; i < funclines.Length; i++)
		{
			// Children primitives (penline, setline, or functionline)
			for (int j = 2; j < funclines[i].transform.childCount; j++)
			{
				savefile.Save<string>(funclines[i].name + ".Children.Name." + j.ToString(), funclines[i].transform.GetChild(j).name);
			}
		}

		// NOW, UNLINK ALL CHILDREN
		for (int i = 0; i < funclines.Length; i++)
		{
			funclines[i].GetComponent<functionLine_script>().unlinkAllChildren();
		}

		for (int i = 0; i < setlines.Length; i++)
		{
			setlines[i].GetComponent<setLine_script>().unlinkAllChildren();
		}

		// MOVE ON TO SAVING EVERY OBJECT, THEY ARE UNDER PAINTABLE'S TRANSFORM	

		// FIND ALL STATIC LINES AND SAVE
		GameObject[] slines = GameObject.FindGameObjectsWithTag("static_pen_line");

		savefile.Save<int>("totalStaticLines", slines.Length);

		for (int i = 0; i < slines.Length; i++)
		{
			// transform
			string sname = slines[i].name;
			savefile.Save<string>(sname + ".staticLineName", sname);
			savefile.Save<Transform>(sname + ".Transform", slines[i].transform);

			// Line Renderer
			savefile.Save<Material>(sname + ".LineRenderer.Material", slines[i].GetComponent<LineRenderer>().material);
			savefile.Save<int>(sname + ".LineRenderer.positionCount", slines[i].GetComponent<LineRenderer>().positionCount);
			Vector3[] pos = new Vector3[slines[i].GetComponent<LineRenderer>().positionCount];
			slines[i].GetComponent<LineRenderer>().GetPositions(pos);
			savefile.Save<Vector3[]>(sname + ".LineRenderer.points", pos);
			savefile.Save<float>(sname + ".LineRenderer.startWidth", slines[i].GetComponent<LineRenderer>().startWidth);
			savefile.Save<float>(sname + ".LineRenderer.endWidth", slines[i].GetComponent<LineRenderer>().endWidth);

			// Edge Collider 2D
			savefile.Save<Vector2[]>(sname + ".EdgeCollider2D.points", slines[i].GetComponent<EdgeCollider2D>().points);
			savefile.Save<float>(sname + ".EdgeCollider2D.radius", slines[i].GetComponent<EdgeCollider2D>().edgeRadius);
		}

		// FIND ALL OOP PROPERTY EDGES AND SAVE
		GameObject[] proplines = GameObject.FindGameObjectsWithTag("oop_property_edge");

		savefile.Save<int>("totalPropertyEdges", proplines.Length);

		for (int i = 0; i < proplines.Length; i++)
		{
			// transform
			string propname = proplines[i].name;
			savefile.Save<string>(propname + ".propertyEdgeName", propname);
			savefile.Save<Transform>(propname + ".Transform", proplines[i].transform);

			// source and target 
			savefile.Save<string>(propname + ".source_object.Name", proplines[i].GetComponent<OOP_Property_Edge_script>().source_object.name);
			savefile.Save<string>(propname + ".propertyTargetSet.Name", proplines[i].GetComponent<OOP_Property_Edge_script>().propertyTargetSet.name);

			// visibility
			savefile.Save<bool>(propname + ".showPropertyEdge", proplines[i].GetComponent<OOP_Property_Edge_script>().showPropertyEdge);
		}

		// FIND ALL PEN LINES AND SAVE

		int originalpennum = 0;

		for (int i = 0; i < plines.Length; i++)
		{
			if (plines[i].name.Contains("(Clone)")) continue;   // skip if it's a clone without a specific pen line name format -- they are redudant pieces

			originalpennum++;

			// transform
			string pname = plines[i].name;
			savefile.Save<string>(pname + ".penLineName", pname);
			savefile.Save<Transform>(pname + ".Transform", plines[i].transform);
			savefile.Save<Vector3>(pname + ".Transform.Position", plines[i].transform.position);
			savefile.Save<Vector3>(pname + ".Transform.localPosition", plines[i].transform.localPosition);
			//Debug.Log(pname + " transform.position: " + plines[i].transform.position);

			// Trail Renderer
			savefile.Save<Material>(pname + ".TrailRenderer.Material", plines[i].GetComponent<TrailRenderer>().material);
			savefile.Save<float>(pname + ".TrailRenderer.startWidth", plines[i].GetComponent<TrailRenderer>().startWidth);
			savefile.Save<float>(pname + ".TrailRenderer.endWidth", plines[i].GetComponent<TrailRenderer>().endWidth);
			savefile.Save<bool>(pname + ".TrailRenderer.enabled", plines[i].GetComponent<TrailRenderer>().enabled);

			// Line Renderer and path
			//if (plines[i].GetComponent<LineRenderer>() != null)
			if((plines[i].GetComponent<LineRenderer>() != null) && plines[i].transform.GetComponent<penLine_script>().translation_path.Count > 2)
			{
				savefile.Save<bool>(pname + ".HasPath", true);
				savefile.Save<Material>(pname + ".LineRenderer.Material", plines[i].GetComponent<LineRenderer>().material);
				savefile.Save<int>(pname + ".LineRenderer.positionCount", plines[i].GetComponent<LineRenderer>().positionCount);
				Vector3[] pos = new Vector3[plines[i].GetComponent<LineRenderer>().positionCount];
				plines[i].GetComponent<LineRenderer>().GetPositions(pos);
				savefile.Save<Vector3[]>(pname + ".LineRenderer.points", pos);
				savefile.Save<float>(pname + ".LineRenderer.startWidth", plines[i].GetComponent<LineRenderer>().startWidth);
				savefile.Save<float>(pname + ".LineRenderer.endWidth", plines[i].GetComponent<LineRenderer>().endWidth);
				savefile.Save<bool>(pname + ".LineRenderer.enabled", plines[i].GetComponent<LineRenderer>().enabled);

				// continuous or discrete path
				savefile.Save<bool>(pname + ".Script.has_continuous_path", plines[i].GetComponent<penLine_script>().has_continuous_path);

				// translation paths and param_list text boxes
				// save the number of sections
				savefile.Save<int>(pname + ".param_list.childCount", plines[i].transform.GetChild(4).childCount);
				// save the translation path in each section AFTER param_end, and save upper_val
				for (int k = 1; k < plines[i].transform.GetChild(4).childCount; k++)
				{
					// name, position, box collider size
					savefile.Save<string>(pname + ".param_list." + k.ToString() + ".name", plines[i].transform.GetChild(4).GetChild(k).name);

					savefile.Save<Vector3>(pname + ".param_list." + k.ToString() + ".position",
						plines[i].transform.GetChild(4).GetChild(k).position);

					savefile.Save<Vector2>(pname + ".param_list." + k.ToString() + ".BoxCollider2D.size",
						plines[i].transform.GetChild(4).GetChild(k).GetComponent<BoxCollider2D>().size);

					savefile.Save<List<Vector3>>(pname + ".param_list." + k.ToString() + ".translation_path",
						plines[i].transform.GetChild(4).GetChild(k).GetComponent<param_text_Script>().translation_path);

					savefile.Save<float>(pname + ".param_list." + k.ToString() + ".upper_val",
						plines[i].transform.GetChild(4).GetChild(k).GetComponent<param_text_Script>().upper_val);

					savefile.Save<float>(pname + ".param_list." + k.ToString() + ".section_length",
						plines[i].transform.GetChild(4).GetChild(k).GetComponent<param_text_Script>().section_length);
				}
			}
			else
			{
				savefile.Save<bool>(pname + ".HasPath", false);
			}

			// Box Collider
			savefile.Save<BoxCollider>(pname + ".BoxCollider", plines[i].GetComponent<BoxCollider>());
			savefile.Save<bool>(pname + ".BoxCollider.enabled", plines[i].GetComponent<BoxCollider>().enabled);

			// penLine_script vars
			savefile.Save<string>(pname + ".Script._name", plines[i].GetComponent<penLine_script>()._name);
			savefile.Save<string>(pname + ".Script.current_property", plines[i].GetComponent<penLine_script>().current_property.ToString());
			savefile.Save<int>(pname + ".Script.max_parameter_value", plines[i].GetComponent<penLine_script>().max_parameter_value);
			savefile.Save<int>(pname + ".Script.min_parameter_value", plines[i].GetComponent<penLine_script>().min_parameter_value);
			savefile.Save<float>(pname + ".Script.current_attribute", plines[i].GetComponent<penLine_script>().current_attribute);
			savefile.Save<List<Vector3>>(pname + ".Script.points", plines[i].GetComponent<penLine_script>().points);
			savefile.Save<Vector3>(pname + ".Script.centroid", plines[i].GetComponent<penLine_script>().centroid);
			savefile.Save<float>(pname + ".Script.maxx", plines[i].GetComponent<penLine_script>().maxx);
			savefile.Save<float>(pname + ".Script.maxy", plines[i].GetComponent<penLine_script>().maxy);
			savefile.Save<float>(pname + ".Script.minx", plines[i].GetComponent<penLine_script>().minx);
			savefile.Save<float>(pname + ".Script.miny", plines[i].GetComponent<penLine_script>().miny);
			savefile.Save<Material>(pname + ".Script.pen_line_material", plines[i].GetComponent<penLine_script>().pen_line_material);
			savefile.Save<Vector3>(pname + ".Script.previous_position", plines[i].GetComponent<penLine_script>().previous_position);
			savefile.Save<List<Vector3>>(pname + ".Script.recorded_path", plines[i].GetComponent<penLine_script>().recorded_path);
			savefile.Save<Vector3>(pname + ".Script.position_before_record", plines[i].GetComponent<penLine_script>().position_before_record);
			savefile.Save<List<Vector3>>(pname + ".Script.translation_path", plines[i].GetComponent<penLine_script>().translation_path);
			savefile.Save<bool>(pname + ".Script.is_prediction_done", plines[i].GetComponent<penLine_script>().isPredictionDone);
			savefile.Save<string>(pname + ".Script.gestureTemplate", plines[i].GetComponent<penLine_script>().gestureTemplate);
			savefile.Save<Sprite>(pname + ".Script.recognized_sprite", plines[i].GetComponent<penLine_script>().recognized_sprite);
			savefile.Save<bool>(pname + ".Script.is_this_double_function_operand", plines[i].GetComponent<penLine_script>().is_this_double_function_operand);
			savefile.Save<float>(pname + ".Script.Attribute.Length", plines[i].GetComponent<penLine_script>().attribute.Length);
			savefile.Save<float>(pname + ".Script.Attribute.Area", plines[i].GetComponent<penLine_script>().attribute.Area);


			// Mesh object -- save the mesh
			GameObject meshobj = plines[i].transform.GetChild(0).gameObject;
			savefile.Save<Transform>(pname + ".MeshObject.Transform", meshobj.transform);
			if (plines[i].transform.GetChild(0).GetComponent<MeshRenderer>() != null)
			{
				savefile.Save<bool>(pname + ".MeshObject.Exists", true);
				savefile.Save<Mesh>(pname + ".MeshObject.Mesh", meshobj.GetComponent<MeshFilter>().sharedMesh);
				savefile.Save<Material>(pname + ".MeshObject.MeshRenderer.Material", meshobj.GetComponent<MeshRenderer>().material);
				savefile.Save<bool>(pname + ".MeshObject.MeshRenderer.enabled", meshobj.GetComponent<MeshRenderer>().enabled);
			}
			else
			{
				savefile.Save<bool>(pname + ".MeshObject.Exists", false);
				savefile.Save<Sprite>(pname + ".MeshObject.Sprite", meshobj.GetComponent<SpriteRenderer>().sprite);
				savefile.Save<bool>(pname + ".MeshObject.SpriteRenderer.enabled", meshobj.GetComponent<SpriteRenderer>().enabled);
			}

			//parameter text boxes and translation paths
			GameObject t0 = plines[i].transform.GetChild(4).GetChild(0).gameObject; // param_end
			GameObject t1 = plines[i].transform.GetChild(1).gameObject;	// param_start
			GameObject t2 = plines[i].transform.GetChild(2).gameObject;	// param_float
			GameObject t3 = plines[i].transform.GetChild(3).gameObject; // argument_label

			// param text end
			savefile.Save<RectTransform>(pname + ".ParamTextEnd.Transform", t0.GetComponent<RectTransform>());
			savefile.Save<bool>(pname + ".ParamTextEnd.MeshRenderer.enabled", t0.GetComponent<MeshRenderer>().enabled);
			savefile.Save<string>(pname + ".ParamTextEnd.Text", t0.GetComponent<TextMeshPro>().text);
			savefile.Save<BoxCollider2D>(pname + ".ParamTextEnd.BoxCollider2D", t0.GetComponent<BoxCollider2D>());
			savefile.Save<bool>(pname + ".ParamTextEnd.BoxCollider2D.enabled", t0.GetComponent<BoxCollider2D>().enabled);
			savefile.Save<List<Vector3>>(pname + ".ParamTextEnd.translation_path", t0.GetComponent<param_text_Script>().translation_path);
			savefile.Save<float>(pname + ".ParamTextEnd.upper_val", t0.GetComponent<param_text_Script>().upper_val);

			// param text start
			savefile.Save<RectTransform>(pname + ".ParamTextStart.Transform", t1.GetComponent<RectTransform>());
			//ES3.Save<Material>(pname + ".ParamTextStart.MeshRenderer.Material", t2.GetComponent<MeshRenderer>().material, savefilename);
			savefile.Save<bool>(pname + ".ParamTextStart.MeshRenderer.enabled", t1.GetComponent<MeshRenderer>().enabled);
			savefile.Save<string>(pname + ".ParamTextStart.Text", t1.GetComponent<TextMeshPro>().text);
			savefile.Save<BoxCollider2D>(pname + ".ParamTextStart.BoxCollider2D", t1.GetComponent<BoxCollider2D>());
			savefile.Save<bool>(pname + ".ParamTextStart.BoxCollider2D.enabled", t1.GetComponent<BoxCollider2D>().enabled);

			// param text float
			savefile.Save<RectTransform>(pname + ".ParamTextFloat.Transform", t2.GetComponent<RectTransform>());
			//ES3.Save<Material>(pname + ".ParamTextFloat.MeshRenderer.Material", t3.GetComponent<MeshRenderer>().material, savefilename);
			savefile.Save<bool>(pname + ".ParamTextFloat.MeshRenderer.enabled", t2.GetComponent<MeshRenderer>().enabled);
			savefile.Save<string>(pname + ".ParamTextFloat.Text", t2.GetComponent<TextMeshPro>().text);
			savefile.Save<BoxCollider2D>(pname + ".ParamTextFloat.BoxCollider2D", t2.GetComponent<BoxCollider2D>());
			savefile.Save<bool>(pname + ".ParamTextFloat.BoxCollider2D.enabled", t2.GetComponent<BoxCollider2D>().enabled);

			// argument_label
			savefile.Save<RectTransform>(pname + ".ArgumentLabel.RectTransform", t3.GetComponent<RectTransform>());
			//ES3.Save<RectTransform>(pname + ".ArgumentLabel.Text.RectTransform", t4.transform.GetChild(0).GetComponent<RectTransform>(), savefilename);
			//ES3.Save<GameObject>(pname + ".ArgumentLabel.Text.GameObject", t4.transform.GetChild(0).gameObject, savefilename);
			savefile.Save<string>(pname + ".ArgumentLabel.Text.Text", 
				t3.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text);
			savefile.Save<bool>(pname + ".ArgumentLabel.enabled", t3.gameObject.activeSelf);
		}

		savefile.Save<int>("totalPenLines", originalpennum);

		// FIND ALL SET OBJECTS AND SAVE

		savefile.Save<int>("totalSetLines", setlines.Length);

		for (int i = 0; i < setlines.Length; i++)
		{
			// transform
			string setname = setlines[i].name;
			savefile.Save<string>(setname + ".setLineName", setname);
			savefile.Save<Transform>(setname + ".Transform", setlines[i].transform);
			savefile.Save<Vector3>(setname + ".Position", setlines[i].transform.position);

			// setLine_script vars
			savefile.Save<int>(setname + ".Script.current_feature", (int)setlines[i].GetComponent<setLine_script>().current_feature);
			savefile.Save<int>(setname + ".Script.capacity", setlines[i].GetComponent<setLine_script>().capacity);

			savefile.Save<bool>(setname + ".Script.timer_activated", setlines[i].GetComponent<setLine_script>().timer_activated);
			savefile.Save<int>(setname + ".Script.timer_min", setlines[i].GetComponent<setLine_script>().timer_min);
			savefile.Save<int>(setname + ".Script.timer_max", setlines[i].GetComponent<setLine_script>().timer_max);
			savefile.Save<float>(setname + ".Script.timer_dt", setlines[i].GetComponent<setLine_script>().timer_dt);

			if (setlines[i].GetComponent<setLine_script>().objectToCopy != null && setlines[i].GetComponent<setLine_script>().timer_activated)
			{
				savefile.Save<string>(setname + ".Script.objectToCopy.Name", setlines[i].GetComponent<setLine_script>().objectToCopy.name);
			}

			savefile.Save<string>(setname + ".Script.this_set_name", setlines[i].GetComponent<setLine_script>().this_set_name);
			savefile.Save<string>(setname + ".Script.symbol_name", setlines[i].GetComponent<setLine_script>().symbol_name.ToString());
			savefile.Save<string>(setname + ".Script.gestureTEX1", setlines[i].GetComponent<setLine_script>().gestureTEX1.ToString());

			savefile.Save<List<Vector3>>(setname + ".Script.points", setlines[i].GetComponent<setLine_script>().points);
			savefile.Save<Vector3>(setname + ".Script.centroid", setlines[i].GetComponent<setLine_script>().centroid);
			savefile.Save<Vector3>(setname + ".Script.anchor_offset_from_center", setlines[i].GetComponent<setLine_script>().anchor_offset_from_center);
			savefile.Save<float>(setname + ".Script.maxx", setlines[i].GetComponent<setLine_script>().maxx);
			savefile.Save<float>(setname + ".Script.maxy", setlines[i].GetComponent<setLine_script>().maxy);
			savefile.Save<float>(setname + ".Script.minx", setlines[i].GetComponent<setLine_script>().minx);
			savefile.Save<float>(setname + ".Script.miny", setlines[i].GetComponent<setLine_script>().miny);

			savefile.Save<Material>(setname + ".Script.set_line_material", setlines[i].GetComponent<setLine_script>().set_line_material);

			savefile.Save<int>(setname + ".Script.abstraction_layer", setlines[i].GetComponent<setLine_script>().abstraction_layer);
			savefile.Save<int>(setname + ".Script.prev_abstraction_layer", setlines[i].GetComponent<setLine_script>().prev_abstraction_layer);
			savefile.Save<int>(setname + ".Script.retained_slider_value", setlines[i].GetComponent<setLine_script>().retained_slider_value);
			savefile.Save<bool>(setname + ".Script.parent_asked_to_lower_layer", setlines[i].GetComponent<setLine_script>().parent_asked_to_lower_layer);
			savefile.Save<bool>(setname + ".Script.updateGestureType", setlines[i].GetComponent<setLine_script>().updateGestureType);

			savefile.Save<bool>(setname + ".Script.is_this_double_function_operand", setlines[i].GetComponent<setLine_script>().is_this_double_function_operand);

			savefile.Save<bool>(setname + ".Script.is_this_an_oop_property", setlines[i].GetComponent<setLine_script>().is_this_an_oop_property);

			// _anchor object
			savefile.Save<RectTransform>(setname + "._anchor.RectTransform", setlines[i].transform.GetChild(0).GetComponent<RectTransform>());
			// _anchor/_text
			savefile.Save<string>(setname + "._anchor._text.Text", setlines[i].transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text);
			// _anchor/_TeX
			savefile.Save<string>(setname + "._anchor._TeX.Text", setlines[i].transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text);
			// _anchor/argument_label/Text
			savefile.Save<string>(setname + ".argument_label.Text.Text", setlines[i].transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text);

			// _meshobj object
			savefile.Save<Transform>(setname + "._meshobj.Transform", setlines[i].transform.GetChild(1).transform);
			savefile.Save<Vector3>(setname + "._meshobj.Position", setlines[i].transform.GetChild(1).transform.position);
			savefile.Save<Mesh>(setname + "._meshobj.Mesh", setlines[i].transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh);
			// if _stroke_converted_mesh exists, save it
			if (setlines[i].transform.GetChild(1).childCount > 0)
			{
				if (setlines[i].transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
				{
					savefile.Save<bool>(setname + "._meshobj.MeshNotSprite", true);
					savefile.Save<Mesh>(setname + "._meshobj._stroke_converted_mesh",
						setlines[i].transform.GetChild(1).GetChild(0).GetComponent<MeshFilter>().sharedMesh);
					savefile.Save<Material>(setname + "._meshobj._stroke_converted_mesh.Material",
						setlines[i].transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().sharedMaterial);
				}
				else
				{
					savefile.Save<bool>(setname + "._meshobj.MeshNotSprite", false);
					savefile.Save<string>(setname + "._meshobj._stroke_converted_mesh",
						//setlines[i].transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().sprite);
						setlines[i].transform.GetComponent<setLine_script>().sprite_filename);
				}

				savefile.Save<Vector3>(setname + "._meshobj._stroke_converted_mesh.Position",
					setlines[i].transform.GetChild(1).GetChild(0).transform.position);
			}

		}


		// FIND ALL FUNCTION OBJECTS AND SAVE

		savefile.Save<int>("totalFunctionLines", funclines.Length);

		for (int i = 0; i < funclines.Length; i++)
		{
			// transform
			string funcname = funclines[i].name;
			savefile.Save<string>(funcname + ".funcLineName", funcname);
			savefile.Save<Transform>(funcname + ".Transform", funclines[i].transform);
			savefile.Save<Vector3>(funcname + ".Position", funclines[i].transform.position);

			// user given function name
			savefile.Save<string>(funcname + ".Script.user_given_function_name", funclines[i].GetComponent<functionLine_script>().user_given_function_name);

			// setLine_script vars
			savefile.Save<int>(funcname + ".Script.max_function_value", (int)funclines[i].GetComponent<functionLine_script>().max_function_value);

			savefile.Save<string>(funcname + ".Script.currentOperator", funclines[i].GetComponent<functionLine_script>().currentOperator());

			savefile.Save<List<Vector3>>(funcname + ".Script.points", funclines[i].GetComponent<functionLine_script>().points);
			savefile.Save<Vector3>(funcname + ".Script.centroid", funclines[i].GetComponent<functionLine_script>().centroid);
			savefile.Save<Vector3>(funcname + ".Script.anchor_offset_from_center", funclines[i].GetComponent<functionLine_script>().anchor_offset_from_center);
			savefile.Save<float>(funcname + ".Script.maxx", funclines[i].GetComponent<functionLine_script>().maxx);
			savefile.Save<float>(funcname + ".Script.maxy", funclines[i].GetComponent<functionLine_script>().maxy);
			savefile.Save<float>(funcname + ".Script.minx", funclines[i].GetComponent<functionLine_script>().minx);
			savefile.Save<float>(funcname + ".Script.miny", funclines[i].GetComponent<functionLine_script>().miny);

			savefile.Save<Material>(funcname + ".Script.function_line_material", funclines[i].GetComponent<functionLine_script>().function_line_material);

			savefile.Save<bool>(funcname + ".Script.is_this_a_double_function", funclines[i].GetComponent<functionLine_script>().is_this_a_double_function);

			savefile.Save<int>(funcname + ".Script.abstraction_layer", funclines[i].GetComponent<functionLine_script>().abstraction_layer);
			savefile.Save<int>(funcname + ".Script.prev_abstraction_layer", funclines[i].GetComponent<functionLine_script>().prev_abstraction_layer);
			savefile.Save<int>(funcname + ".Script.retained_slider_value", funclines[i].GetComponent<functionLine_script>().retained_slider_value);
			savefile.Save<bool>(funcname + ".Script.parent_asked_to_lower_layer", funclines[i].GetComponent<functionLine_script>().parent_asked_to_lower_layer);
			savefile.Save<bool>(funcname + ".Script.updateGestureType", funclines[i].GetComponent<functionLine_script>().updateGestureType);

			savefile.Save<bool>(funcname + ".Script.is_this_double_function_operand", funclines[i].GetComponent<functionLine_script>().is_this_double_function_operand);

			if (funclines[i].GetComponent<functionLine_script>().first_operand != null)
			{
				// store the first operand name
				savefile.Save<string>(funcname + ".Script.first_operand.Name", funclines[i].GetComponent<functionLine_script>().first_operand.name);
			}

			if (funclines[i].GetComponent<functionLine_script>().second_operand != null)
			{
				// store the second operand name
				savefile.Save<string>(funcname + ".Script.second_operand.Name", funclines[i].GetComponent<functionLine_script>().second_operand.name);
			}

			// _anchor object
			savefile.Save<RectTransform>(funcname + "._anchor.RectTransform", funclines[i].transform.GetChild(0).GetComponent<RectTransform>());
			// _anchor/_anchor_TeX
			savefile.Save<string>(funcname + "._anchor._anchor_TeX.Text", funclines[i].transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().text);
			// _anchor/_TeX
			savefile.Save<string>(funcname + "._anchor._TeX.Text", funclines[i].transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text);
			// _anchor/argument_label/Text
			savefile.Save<string>(funcname + ".argument_label.Text.Text", funclines[i].transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text);

			// _meshobj object
			savefile.Save<Transform>(funcname + "._meshobj.Transform", funclines[i].transform.GetChild(1).transform);
			savefile.Save<Vector3>(funcname + "._meshobj.Position", funclines[i].transform.GetChild(1).transform.position);
			savefile.Save<Mesh>(funcname + "._meshobj.Mesh", funclines[i].transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh);
			// if _stroke_converted_mesh exists, save it
			if (funclines[i].transform.GetChild(1).childCount > 0)
			{
				if (funclines[i].transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
				{
					savefile.Save<bool>(funcname + "._meshobj.MeshNotSprite", true);

					savefile.Save<Mesh>(funcname + "._meshobj._stroke_converted_mesh",
						funclines[i].transform.GetChild(1).GetChild(0).GetComponent<MeshFilter>().sharedMesh);

					savefile.Save<Material>(funcname + "._meshobj._stroke_converted_mesh.Material",
						funclines[i].transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().sharedMaterial);
				}
				else
				{
					savefile.Save<bool>(funcname + "._meshobj.MeshNotSprite", false);

					savefile.Save<string>(funcname + "._meshobj._stroke_converted_mesh",
						//funclines[i].transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().sprite);
						funclines[i].transform.GetComponent<functionLine_script>().sprite_filename);
				}

				savefile.Save<Vector3>(funcname + "._meshobj._stroke_converted_mesh.Position",
					funclines[i].transform.GetChild(1).GetChild(0).transform.position);
			}

			// save category filter state
			// set, circle, box, ellipse, arrow, horizontal_ellipse, triangle, figure
			bool[] catfilter = new bool[8]; // currently, there are 8 categories
			catfilter[0] = funclines[i].GetComponent<functionLine_script>().categoryFilter.set;
			catfilter[1] = funclines[i].GetComponent<functionLine_script>().categoryFilter.circle;
			catfilter[2] = funclines[i].GetComponent<functionLine_script>().categoryFilter.box;
			catfilter[3] = funclines[i].GetComponent<functionLine_script>().categoryFilter.ellipse;
			catfilter[4] = funclines[i].GetComponent<functionLine_script>().categoryFilter.arrow;
			catfilter[5] = funclines[i].GetComponent<functionLine_script>().categoryFilter.horizontal_ellipse;
			catfilter[6] = funclines[i].GetComponent<functionLine_script>().categoryFilter.triangle;
			catfilter[7] = funclines[i].GetComponent<functionLine_script>().categoryFilter.figure;

			savefile.Save<bool[]>(funcname + ".Script.categoryFilter", catfilter);
		}


		// FIND ALL EDGELINES AND SAVE
		GameObject[] edgelines = GameObject.FindGameObjectsWithTag("edgeline");

		savefile.Save<int>("totalEdgeLines", edgelines.Length);

		for (int i = 0; i < edgelines.Length; i++)
		{
			// transform
			string edgename = edgelines[i].name;
			savefile.Save<string>(edgename + ".edgeLineName", edgename);
			savefile.Save<Transform>(edgename + ".Transform", edgelines[i].transform);

			// full condition text
			savefile.Save<string>(edgename + ".full_condition_text", edgelines[i].GetComponent<edgeLine_script>().full_condition_text);

			// source and target object names
			savefile.Save<string>(edgename + ".source_object.Name", edgelines[i].GetComponent<edgeLine_script>().source_object.name);
			savefile.Save<string>(edgename + ".target_object.Name", edgelines[i].GetComponent<edgeLine_script>().target_object.name);

			// TeX text
			//ES3.Save<string>(edgename + "._TeX.text", edgelines[i].transform.GetChild(0).GetComponent<TEXDraw>().text, savefilename);

			// Edge Collider 2D
			savefile.Save<Vector2[]>(edgename + ".EdgeCollider2D.points", edgelines[i].GetComponent<EdgeCollider2D>().points);
			savefile.Save<float>(edgename + ".EdgeCollider2D.radius", edgelines[i].GetComponent<EdgeCollider2D>().edgeRadius);
		}

		// FIND ALL SPAWN EDGES AND SAVE
		GameObject[] spawnedges = GameObject.FindGameObjectsWithTag("spawn_edge");

		savefile.Save<int>("totalSpawnEdges", spawnedges.Length);

		for (int i = 0; i < spawnedges.Length; i++)
		{
			string edgename = spawnedges[i].name;
			savefile.Save<string>(edgename + ".spawnEdgeName", edgename);

			// source and target object names
			savefile.Save<string>(edgename + ".source_object.Name", spawnedges[i].GetComponent<SpawnEdgeScript>().source_object.name);
			savefile.Save<string>(edgename + ".target_object.Name", spawnedges[i].GetComponent<SpawnEdgeScript>().target_object.name);
		}

		// sync save file (write from cache)
		savefile.Sync();

		Debug.Log("Objects saved.");

		// Resume all interaction operations
		GameObject.FindGameObjectWithTag("paintable_canvas_object").GetComponent<Paintable_Script>().IOInProgress = false;

		// Resume updates (dummy pen objects filling) for all OOP property edges
		for (int i = 0; i < proplines.Length; i++)
		{
			proplines[i].GetComponent<OOP_Property_Edge_script>().doNotUpdate = false;
		}

		// Restore in the next frame
		//Invoke("RestoreHierarchy", 0);
		RestoreHierarchy();

		// Resume updates (dummy pen objects filling) for all sets
		for (int i = 0; i < setlines.Length; i++)
		{
			setlines[i].GetComponent<setLine_script>().halt_timer_update_while_saving = false;
		}

		//savefile.Clear();

	}

	private void RestoreHierarchy()
	{
		// RESET THE HIERARCHY STRUCTURE BACK

		string[] keys = savefile.GetKeys();

		List<string> objnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".Children.Name."))
			{
				objnames.Add(keys[i].Split(new char[] { '.' })[0]);
			}
		}

		for (int i = 0; i < objnames.Count; i++)
		{
			//Debug.Log("Looking at parent: " + objnames[i]);

			GameObject parent = GameObject.Find(objnames[i]);
			for (int k = 0; k < keys.Length; k++)
			{
				if (keys[k].Contains(objnames[i] + ".Children.Name."))
				{
					GameObject child = GameObject.Find(savefile.Load<string>(keys[k]));

					child.transform.SetParent(parent.transform);

					//Debug.Log("parent " + parent.name + " set for child: " + child.name);
				}
			}
		}

		Debug.Log("Saving: Hierarchy restored.");
	}


	public void saveLogs()
	{
		// file name: start time till now.
		string filepath = @"C:\PlayLogs\" + sessionStart.ToString("yyyy-MM-dd-hh-mm-ss") + "---" +
			DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".log";

		// image name: start time till now.
		string imagepath = @"C:\PlayLogs\" + sessionStart.ToString("yyyy-MM-dd-hh-mm-ss") + "---" +
			DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".png";

		// save a screenshot
		ScreenCapture.CaptureScreenshot(imagepath, 2);

		// save all logs from pen objects
		GameObject[] pens = GameObject.FindGameObjectsWithTag("penline");
		GameObject[] sets = GameObject.FindGameObjectsWithTag("set");
		GameObject[] functions = GameObject.FindGameObjectsWithTag("function");

		EnsureDirectoryExists(filepath);

		// The using statement automatically flushes AND CLOSES the stream and calls 
		// IDisposable.Dispose on the stream object.
		using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(filepath)
			)
		{
			for (int i = 0; i < pens.Length; i++)
			{
				foreach (KeyValuePair<string, Vector2> kvp in pens[i].GetComponent<penLine_script>().saved_positions)
				{
					file.WriteLine(pens[i].name + "|" + kvp.Key + "|" + kvp.Value);
					//Console.WriteLine("Key = {0}, Value = {1}",	kvp.Key, kvp.Value);
				}

				// clear the saved_positions dictionary
				pens[i].GetComponent<penLine_script>().saved_positions.Clear();
			}

			for (int i = 0; i < sets.Length; i++)
			{
				// saved positions
				foreach (KeyValuePair<string, Vector2> kvp in sets[i].GetComponent<setLine_script>().saved_positions)
				{
					file.WriteLine(sets[i].name + "|" + kvp.Key + "|" + kvp.Value);
					//Console.WriteLine("Key = {0}, Value = {1}",	kvp.Key, kvp.Value);
				}

				// clear the saved_positions dictionary
				sets[i].GetComponent<setLine_script>().saved_positions.Clear();

				// abstraction log
				foreach (KeyValuePair<string, int> kvp in sets[i].GetComponent<setLine_script>().abstraction_log)
				{
					file.WriteLine("abstraction_layer|" + kvp.Key + "|" + kvp.Value);
					//Console.WriteLine("Key = {0}, Value = {1}",	kvp.Key, kvp.Value);
				}

				// clear the dictionary
				sets[i].GetComponent<setLine_script>().abstraction_log.Clear();
			}

			for (int i = 0; i < functions.Length; i++)
			{
				foreach (KeyValuePair<string, Vector2> kvp in functions[i].GetComponent<functionLine_script>().saved_positions)
				{
					file.WriteLine(functions[i].name + "|" + kvp.Key + "|" + kvp.Value);
					//Console.WriteLine("Key = {0}, Value = {1}",	kvp.Key, kvp.Value);
				}

				// clear the saved_positions dictionary
				functions[i].GetComponent<functionLine_script>().saved_positions.Clear();

				foreach (KeyValuePair<string, int> kvp in functions[i].GetComponent<functionLine_script>().abstraction_log)
				{
					file.WriteLine("abstraction_layer|" + kvp.Key + "|" + kvp.Value);
					//Console.WriteLine("Key = {0}, Value = {1}",	kvp.Key, kvp.Value);
				}

				// clear the dictionary
				functions[i].GetComponent<functionLine_script>().abstraction_log.Clear();
			}

			file.WriteLine("===");

			// save the full hierarchy
			file.WriteLine(Traverse(GameObject.FindGameObjectWithTag("paintable_canvas_object")));

			file.WriteLine("===");

			// Save all polygons
			// If they are not found (i.e. deleted at this point), then use a generic polygon? Maybe this can be handled in Mathematica.

			// penlines
			for (int i = 0; i < pens.Length; i++)
			{
				// a clone cannot be edited for its shape, so let's save time by skipping their polygons
				if (pens[i].name.Contains("(Clone)")) continue;

				// otherwise, write the polygon to file
				file.WriteLine(@"#" + pens[i].name);
				for (int j = 0; j < pens[i].GetComponent<penLine_script>().points.Count; j++)
				{
					file.WriteLine(((Vector2)(pens[i].GetComponent<penLine_script>().points[j])).ToString());
				}
			}

			// save all setLines (points). 
			for (int i = 0; i < sets.Length; i++)
			{
				file.WriteLine(@"#" + sets[i].name);
				for (int j = 0; j < sets[i].GetComponent<setLine_script>().points.Count; j++)
				{
					file.WriteLine(((Vector2)(sets[i].GetComponent<setLine_script>().points[j])).ToString());
				}
			}

			// save all functionLines (points). 
			for (int i = 0; i < functions.Length; i++)
			{
				file.WriteLine(@"#" + functions[i].name);
				for (int j = 0; j < functions[i].GetComponent<functionLine_script>().points.Count; j++)
				{
					file.WriteLine(((Vector2)(functions[i].GetComponent<functionLine_script>().points[j])).ToString());
				}
			}

		}
	}

	private static void EnsureDirectoryExists(string filePath)
	{
		System.IO.FileInfo fi = new System.IO.FileInfo(filePath);
		if (!fi.Directory.Exists)
		{
			System.IO.Directory.CreateDirectory(fi.DirectoryName);
		}
	}

	public string Traverse(GameObject obj)
	{
		string message = obj.name;
		foreach (Transform child in obj.transform)
		{
			if (child.tag == "penline" || child.tag == "set" || child.tag == "function")
			{
				message += ",{" + Traverse(child.gameObject) + "}";
			}
		}
		return message;
	}

	/*
	public string outputHierarchy()
	{
		//Debug.Log("----- Debug root -----");
		foreach (Transform go in GameObject.FindGameObjectWithTag("paintable_canvas_object").transform)
		{
			if (go.transform.parent == null)
			{
				DebugDeeper(go.transform);
			}
		}
	}

	void DebugDeeper(Transform trans)
	{
		int children = trans.GetChildCount();
		Debug.Log(trans.name + " has " + children);

		for (int child = 0; child < children; child++)
		{
			DebugDeeper(trans.GetChild(child));
		}
	}
	*/

	IEnumerator ShowMessageCoRoutine()
	{
		while (GameObject.FindGameObjectWithTag("paintable_canvas_object").GetComponent<Paintable_Script>().IOInProgress)
		{
			GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text = "Saving/Loading...";

			yield return null;
		}

		GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text = "";

		yield return null;
	}

	void Start()
	{
		// repeat saveLogs() every t seconds, starting after 10 s.
		// InvokeRepeating("saveLogs", 10.0f, 30.0f);

		// note the start time for the session
		sessionStart = DateTime.Now;
	}

	// Update is called once per frame
	void Update()
    {

    }

	void OnApplicationQuit()
	{
		// The produced zip files from this code do not open. 
		/*
		// zip the log files
		transform.GetComponent<CompressFileAndDirectory>().CompressDirectory(@"C:\PlayLogs", @"D:\playlog-" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".zip");
		// clean up the directory
		if (System.IO.Directory.Exists(@"C:\PlayLogs")) { System.IO.Directory.Delete(@"C:\PlayLogs", true); }
		// recreate the directory
		System.IO.Directory.CreateDirectory(@"C:\PlayLogs");
		*/
	}
}
