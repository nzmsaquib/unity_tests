﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class LongPressEdgeDrawBehavior : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
	[SerializeField]
	[Tooltip("How long must pointer be down on this object to trigger a long press")]
	private float holdTime = 1f;

	private Vector2 pressPoint;
	private GameObject radmenu;

	public GameObject edge_menu;
	public Canvas canvas_radial;
	public Canvas canvas_button;

	public Sprite edge_image, spawn_image;

	//private bool held = false;
	//public UnityEvent onClick = new UnityEvent();

	public UnityEvent onLongPress = new UnityEvent();

	public void OnPointerDown(PointerEventData eventData)
	{
		//held = false;
		if (eventData.selectedObject.transform.parent.name == "Canvas")
		{
			Invoke("OnLongPress", holdTime);

			pressPoint = eventData.pressPosition;
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");

		//if (!held)
		//    onClick.Invoke();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		CancelInvoke("OnLongPress");
	}

	void OnLongPress()
	{
		//held = true;
		onLongPress.Invoke();

		//Debug.Log("button long pressed. " + worldCoord.ToString());
	}

	public void CreateEdgeSelectionMenu()
	{
		// create a menu and show it, if one doesn't exist
		if (GameObject.Find("Edge_Selection_Menu") == null)
		{
			radmenu = Instantiate(edge_menu,
				pressPoint + new Vector2(0f, -50f),
				Quaternion.identity,
				canvas_button.transform);

			radmenu.name = "Edge_Selection_Menu";

			//radmenu.transform.Find("set_selection").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);
			//radmenu.transform.Find("function_selection").GetComponent<RectTransform>().sizeDelta = new Vector2(50, 50);

			// onClick events
			radmenu.transform.Find("causal_edge").GetComponent<Button>().onClick.AddListener(() => SelectCausalEdge());
			radmenu.transform.Find("spawn_edge").GetComponent<Button>().onClick.AddListener(() => SelectSpawnEdge());
		}

	}

	public void SelectCausalEdge()
	{
		GameObject.Find("Edge_draw").GetComponent<Button>().GetComponent<Image>().sprite = edge_image;
		GameObject.Find("Edge_draw").GetComponent<AllButtonsBehavior>().isCausalEdge = true;

		Destroy(radmenu);
	}

	public void SelectSpawnEdge()
	{
		GameObject.Find("Edge_draw").GetComponent<Button>().GetComponent<Image>().sprite = spawn_image;
		GameObject.Find("Edge_draw").GetComponent<AllButtonsBehavior>().isCausalEdge = false;

		Destroy(radmenu);
	}
}
