﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using Symbolism;
using TMPro;
using SimpleFileBrowser;

public class LoadButtonBehavior : MonoBehaviour
{
	public GameObject paintable;
	public GameObject PenLine;
	public GameObject SetLine;
	public GameObject EdgeLinePrefab;
	public GameObject FunctionLine;
	public GameObject templine;
	public GameObject StaticPenLine;
	public GameObject PropertyEdgePrefab;
	public GameObject SpawnEdgePrefab;

	public GameObject ParameterUIField;
	public GameObject EdgeUIField;

	public Material set_line_material;
	public Material function_line_material;

	private bool loadInTheNextFrame = false;

	public ES3File loadfile;

	public string loadfilename = "";

	public bool iAmDisabled = false;


	public void disableMyself()
	{
		iAmDisabled = true;
	}

	public void enableMyself()
	{
		iAmDisabled = false;
	}

	public void LoadDialog()
	{
		// don't save when some critical operation is running
		if (iAmDisabled) return;

		// Change the mode to pan
		transform.parent.Find("Pan").GetComponent<AllButtonsBehavior>().whenSelected();

		FileBrowser.SetFilters(false, new FileBrowser.Filter("Noyon sketches", ".es3"), new FileBrowser.Filter("Images", ".png"));

		FileBrowser.SetDefaultFilter(".es3");

		FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");

		FileBrowser.AddQuickLink("Resources", "D:\\newtest\\unity_tests\\TextPrefabTest\\Assets\\Resources\\ExampleImages", null);

		StartCoroutine(ShowLoadDialogCoroutine());
	}

	IEnumerator ShowLoadDialogCoroutine()
	{
		// Show a save file dialog and wait for a response from user
		// Save file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
		yield return FileBrowser.WaitForLoadDialog(false, null, "Load Sketch/Image", "Load");

		// Dialog is closed
		// Print whether a file is chosen (FileBrowser.Success)
		// and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
		Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);

		if (FileBrowser.Success)
		{
			loadfilename = FileBrowser.Result;

			if (loadfilename.Contains(".png"))
			{
				LoadImage();
			}

			else if (loadfilename.Contains(".es3"))
			{
				loadfile = new ES3File(FileBrowser.Result);

				Invoke("LoadSketch", 0);
			}
		}
	}

	// Destroy() ONLY TAKES EFFECT AT THE END OF THE FRAME
	public void LoadSketch()
	{
		//loadInTheNextFrame = true;

		// Halt all interaction operations while I/O in progress
		GameObject.FindGameObjectWithTag("paintable_canvas_object").GetComponent<Paintable_Script>().IOInProgress = true;

		// Clean everything under paintable
		for (int i = 0; i < paintable.transform.childCount; i++)
		{
			Destroy(paintable.transform.GetChild(i).gameObject);
		}

		Invoke("LoadObjects", 0);
	}

	public void LoadObjects()
	{

		//loadfilename = findLastSavedFileName();

		// if no files exist or some i/o problem occurred, then return without loading
		//if (loadfilename == "") return;

		string[] keys = loadfile.GetKeys();

		// set totalines variable in Paintable script to keys.length + 1. To avoid possible name collisions
		// after objects are loaded and then new objects are drawn.
		paintable.GetComponent<Paintable_Script>().totalLines = keys.Length + 1;

		//  RETRIEVE PAINTABLE HIERARCHY
		//string pt = ES3.Load<string>("PaintableHierarchy", loadfilename);
		//Debug.Log(Traverse(pt.Replace("Paintable", "")));

		// LOAD ALL STATIC LINES INTO PAINTABLE CANVAS

		#region load static lines

		int numslines = loadfile.Load<int>("totalStaticLines");

		List<string> snames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".staticLineName")) snames.Add(keys[i].Replace(".staticLineName",""));	// or just load through ES3
		}

		for (int i = 0; i < numslines; i++)
		{
			templine = Instantiate(StaticPenLine, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = snames[i];
			templine.tag = "static_pen_line";

			// transform
			loadfile.LoadInto<Transform>(snames[i] + ".Transform", templine.transform);

			// line renderer
			templine.GetComponent<LineRenderer>().material = loadfile.Load<Material>(snames[i] + ".LineRenderer.Material");
			templine.GetComponent<LineRenderer>().positionCount = loadfile.Load<int>(snames[i] + ".LineRenderer.positionCount");
			templine.GetComponent<LineRenderer>().SetPositions(
				loadfile.Load<Vector3[]>(snames[i] + ".LineRenderer.points"));
			templine.GetComponent<LineRenderer>().startWidth = loadfile.Load<float>(snames[i] + ".LineRenderer.startWidth");
			templine.GetComponent<LineRenderer>().endWidth = loadfile.Load<float>(snames[i] + ".LineRenderer.endWidth");

			// edge collider 2D
			templine.GetComponent<EdgeCollider2D>().points =
				loadfile.Load<Vector2[]>(snames[i] + ".EdgeCollider2D.points");
			templine.GetComponent<EdgeCollider2D>().edgeRadius =
				loadfile.Load<float>(snames[i] + ".EdgeCollider2D.radius");
		}

		templine = null;

		#endregion

		// LOAD PENLINE OBJECTS

		#region load pen objects

		// load all pen lines in paintable canvas
		int numplines = loadfile.Load<int>("totalPenLines");

		List<string> pnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".penLineName")) pnames.Add(keys[i].Replace(".penLineName", ""));    // or just load through ES3
		}

		for (int i = 0; i < numplines; i++)
		{
			templine = Instantiate(PenLine, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = pnames[i];
			templine.tag = "penline";

			/*
			// if this pen object belongs to a set or function, find it and make its parent.
			bool penFoundItsParent = false;
			for (int k = 0; k < setnames.Count; k++)
			{
				for (int s = 0; s < keys.Length; s++)
				{
					if (keys[s].Contains(setnames[k] + ".Children.Name."))
					{
						string childName = ES3.Load<string>(keys[s], loadfilename);
						if (childName == pnames[i])
						{
							penFoundItsParent = true;
							templine.transform.SetParent(GameObject.Find(setnames[k]).transform);
							Debug.Log("pen " + pnames[i] + " parent found " + setnames[k]);
							break;
						}
					}
				}

				if (penFoundItsParent) break;
			}

			// reset parent to paintable, just in case Transform changed their parents or a set/function parent was not found
			if (!penFoundItsParent)
				templine.transform.SetParent(paintable.transform);
			*/

			// transform
			//ES3.LoadInto<Transform>(pnames[i] + ".Transform", loadfilename, templine.transform);
			//templine.transform.position = ES3.Load<Vector3>(pnames[i] + ".Transform.Position", loadfilename);
			//templine.transform.localPosition = ES3.Load<Vector3>(pnames[i] + ".Transform.localPosition", loadfilename);

			//Debug.Log(pnames[i] + " loaded position: " + templine.transform.position);

			// box collider
			templine.AddComponent<BoxCollider>();
			loadfile.LoadInto<BoxCollider>(pnames[i] + ".BoxCollider", templine.GetComponent<BoxCollider>());
			templine.GetComponent<BoxCollider>().enabled = loadfile.Load<bool>(pnames[i] + ".BoxCollider.enabled");
			templine.GetComponent<BoxCollider>().center = new Vector3(0, 0, 0);

			// penLine_script vars
			templine.GetComponent<penLine_script>()._name = loadfile.Load<string>(pnames[i] + ".Script._name");
			templine.GetComponent<penLine_script>().symbol_name = new Symbol(templine.GetComponent<penLine_script>()._name);

			Enum.TryParse(loadfile.Load<string>(pnames[i] + ".Script.current_property"),
				out templine.GetComponent<penLine_script>().current_property);

			templine.GetComponent<penLine_script>().max_parameter_value = loadfile.Load<int>(pnames[i] + ".Script.max_parameter_value");
			templine.GetComponent<penLine_script>().min_parameter_value = loadfile.Load<int>(pnames[i] + ".Script.min_parameter_value");

			templine.GetComponent<penLine_script>().current_attribute = loadfile.Load<float>(pnames[i] + ".Script.current_attribute");
			templine.GetComponent<penLine_script>().points = loadfile.Load<List<Vector3>>(pnames[i] + ".Script.points");
			templine.GetComponent<penLine_script>().centroid = loadfile.Load<Vector3>(pnames[i] + ".Script.centroid");
			templine.GetComponent<penLine_script>().maxx = loadfile.Load<float>(pnames[i] + ".Script.maxx");
			templine.GetComponent<penLine_script>().minx = loadfile.Load<float>(pnames[i] + ".Script.minx");
			templine.GetComponent<penLine_script>().maxy = loadfile.Load<float>(pnames[i] + ".Script.maxy");
			templine.GetComponent<penLine_script>().miny = loadfile.Load<float>(pnames[i] + ".Script.miny");

			templine.GetComponent<penLine_script>().pen_line_material = loadfile.Load<Material>(pnames[i] + ".Script.pen_line_material");
			templine.GetComponent<penLine_script>().previous_position = loadfile.Load<Vector3>(pnames[i] + ".Script.previous_position");
			templine.GetComponent<penLine_script>().recorded_path = loadfile.Load<List<Vector3>>(pnames[i] + ".Script.recorded_path");
			templine.GetComponent<penLine_script>().position_before_record = loadfile.Load<Vector3>(pnames[i] + ".Script.position_before_record");
			templine.GetComponent<penLine_script>().translation_path = loadfile.Load<List<Vector3>>(pnames[i] + ".Script.translation_path");
			templine.GetComponent<penLine_script>().isPredictionDone = loadfile.Load<bool>(pnames[i] + ".Script.is_prediction_done");
			templine.GetComponent<penLine_script>().gestureTemplate = loadfile.Load<string>(pnames[i] + ".Script.gestureTemplate");
			templine.GetComponent<penLine_script>().recognized_sprite = loadfile.Load<Sprite>(pnames[i] + ".Script.recognized_sprite");
			templine.GetComponent<penLine_script>().is_this_double_function_operand =
				loadfile.Load<bool>(pnames[i] + ".Script.is_this_double_function_operand");

			templine.GetComponent<penLine_script>().attribute.Length = loadfile.Load<float>(pnames[i] + ".Script.Attribute.Length");
			templine.GetComponent<penLine_script>().attribute.Area = loadfile.Load<float>(pnames[i] + ".Script.Attribute.Area");

			// mesh object
			GameObject meshObj = new GameObject("_meshobj");
			meshObj.AddComponent<MeshFilter>();
			meshObj.AddComponent<MeshRenderer>();
			//ES3.LoadInto<GameObject>(pnames[i] + ".MeshObject", loadfilename, meshObj);

			// Setting a separate transform here messes up the positioning, No need to load the transform
			// as it is at 0,0 wrt to the parent anyway.
			//ES3.LoadInto<Transform>(pnames[i] + ".MeshObject.Transform", loadfilename, meshObj.transform);

			// only need the scale from the saved transform
			Transform mesht = loadfile.Load<Transform>(pnames[i] + ".MeshObject.Transform");
			meshObj.transform.localScale = mesht.localScale;
			Destroy(mesht.gameObject);

			meshObj.GetComponent<MeshFilter>().sharedMesh =
				loadfile.Load<Mesh>(pnames[i] + ".MeshObject.Mesh");
			meshObj.GetComponent<MeshRenderer>().material =
				loadfile.Load<Material>(pnames[i] + ".MeshObject.MeshRenderer.Material");
			meshObj.GetComponent<MeshRenderer>().enabled =
				loadfile.Load<bool>(pnames[i] + ".MeshObject.MeshRenderer.enabled");

			// \/\/ There's an offset between the loaded pen object's saved global position and the mesh coords. and center
			// so, put them together first, set mesh's parent, and then re-translate back the whole pen object to saved position. \/\/
			templine.transform.position = meshObj.GetComponent<MeshFilter>().sharedMesh.bounds.center;

			meshObj.transform.SetParent(templine.transform);
			// set sibling index. I want this to be Child 0, and the param_text component child 1.
			meshObj.transform.SetAsFirstSibling();

			// \/\/ now translate the whole object back to saved position again
			templine.transform.position = loadfile.Load<Vector3>(pnames[i] + ".Transform.Position");



			// parameter text boxes
			// param_text_end
			RectTransform rt = loadfile.Load<RectTransform>(pnames[i] + ".ParamTextEnd.Transform");
			templine.transform.GetChild(4).GetChild(0).GetComponent<RectTransform>().sizeDelta = rt.sizeDelta;
			templine.transform.GetChild(4).GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(4, 4, 1);
			templine.transform.GetChild(4).GetChild(0).GetComponent<RectTransform>().localPosition = rt.localPosition;

			templine.transform.GetChild(4).GetChild(0).GetComponent<MeshRenderer>().enabled =
				loadfile.Load<bool>(pnames[i] + ".ParamTextEnd.MeshRenderer.enabled");
			templine.transform.GetChild(4).GetChild(0).GetComponent<TextMeshPro>().text = loadfile.Load<string>(pnames[i] + ".ParamTextEnd.Text");
			BoxCollider2D b2d = loadfile.Load<BoxCollider2D>(pnames[i] + ".ParamTextEnd.BoxCollider2D");
			templine.transform.GetChild(4).GetChild(0).GetComponent<BoxCollider2D>().enabled = b2d.enabled;
			templine.transform.GetChild(4).GetChild(0).GetComponent<BoxCollider2D>().isTrigger = b2d.isTrigger;
			templine.transform.GetChild(4).GetChild(0).GetComponent<BoxCollider2D>().size = b2d.size;

			templine.transform.GetChild(4).GetChild(0).GetComponent<param_text_Script>().translation_path =
				loadfile.Load<List<Vector3>>(pnames[i] + ".ParamTextEnd.translation_path");

			templine.transform.GetChild(4).GetChild(0).GetComponent<param_text_Script>().upper_val =
				loadfile.Load<float>(pnames[i] + ".ParamTextEnd.upper_val");

			// destroy the gameobjects Easy Save created. Loading something into a variable causes Easy Save to create new objects
			Destroy(rt.gameObject);
			Destroy(b2d.gameObject);

			// param_text_start
			rt = loadfile.Load<RectTransform>(pnames[i] + ".ParamTextStart.Transform");
			templine.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = rt.sizeDelta;
			templine.transform.GetChild(1).GetComponent<RectTransform>().localScale = new Vector3(4, 4, 1);
			templine.transform.GetChild(1).GetComponent<RectTransform>().localPosition = rt.localPosition;

			templine.transform.GetChild(1).GetComponent<MeshRenderer>().enabled =
				loadfile.Load<bool>(pnames[i] + ".ParamTextStart.MeshRenderer.enabled");
			templine.transform.GetChild(1).GetComponent<TextMeshPro>().text = loadfile.Load<string>(pnames[i] + ".ParamTextStart.Text");
			b2d = loadfile.Load<BoxCollider2D>(pnames[i] + ".ParamTextStart.BoxCollider2D");
			templine.transform.GetChild(1).GetComponent<BoxCollider2D>().enabled = b2d.enabled;
			templine.transform.GetChild(1).GetComponent<BoxCollider2D>().isTrigger = b2d.isTrigger;
			templine.transform.GetChild(1).GetComponent<BoxCollider2D>().size = b2d.size;

			// destroy the gameobjects Easy Save created. Loading something into a variable causes Easy Save to create new objects
			Destroy(rt.gameObject);
			Destroy(b2d.gameObject);

			// param_text_float
			rt = loadfile.Load<RectTransform>(pnames[i] + ".ParamTextFloat.Transform");
			templine.transform.GetChild(2).GetComponent<RectTransform>().sizeDelta = rt.sizeDelta;
			templine.transform.GetChild(2).GetComponent<RectTransform>().localScale = new Vector3(4, 4, 1);
			templine.transform.GetChild(2).GetComponent<RectTransform>().localPosition = rt.localPosition;

			templine.transform.GetChild(2).GetComponent<MeshRenderer>().enabled =
				loadfile.Load<bool>(pnames[i] + ".ParamTextFloat.MeshRenderer.enabled");
			templine.transform.GetChild(2).GetComponent<TextMeshPro>().text = loadfile.Load<string>(pnames[i] + ".ParamTextFloat.Text");
			b2d = loadfile.Load<BoxCollider2D>(pnames[i] + ".ParamTextFloat.BoxCollider2D");
			templine.transform.GetChild(2).GetComponent<BoxCollider2D>().enabled = b2d.enabled;
			templine.transform.GetChild(2).GetComponent<BoxCollider2D>().isTrigger = b2d.isTrigger;
			templine.transform.GetChild(2).GetComponent<BoxCollider2D>().size = b2d.size;

			// destroy the gameobjects Easy Save created. Loading something into a variable causes Easy Save to create new objects
			Destroy(rt.gameObject);
			Destroy(b2d.gameObject);

			// trail renderer
			templine.GetComponent<TrailRenderer>().material = loadfile.Load<Material>(pnames[i] + ".TrailRenderer.Material");
			templine.GetComponent<TrailRenderer>().startWidth = loadfile.Load<float>(pnames[i] + ".TrailRenderer.startWidth");
			templine.GetComponent<TrailRenderer>().endWidth = loadfile.Load<float>(pnames[i] + ".TrailRenderer.endWidth");
			templine.GetComponent<TrailRenderer>().enabled = loadfile.Load<bool>(pnames[i] + ".TrailRenderer.enabled");

			// line renderer and path
			if (loadfile.Load<bool>(pnames[i] + ".HasPath"))
			{
				templine.GetComponent<LineRenderer>().material = loadfile.Load<Material>(pnames[i] + ".LineRenderer.Material");
				templine.GetComponent<LineRenderer>().positionCount = loadfile.Load<int>(pnames[i] + ".LineRenderer.positionCount");
				templine.GetComponent<LineRenderer>().SetPositions(loadfile.Load<Vector3[]>(pnames[i] + ".LineRenderer.points"));
				templine.GetComponent<LineRenderer>().startWidth = loadfile.Load<float>(pnames[i] + ".LineRenderer.startWidth");
				templine.GetComponent<LineRenderer>().endWidth = loadfile.Load<float>(pnames[i] + ".LineRenderer.endWidth");
				templine.GetComponent<LineRenderer>().enabled = loadfile.Load<bool>(pnames[i] + ".LineRenderer.enabled");

				// load param_list text and individual translation paths

				// continuous or discrete path?
				templine.GetComponent<penLine_script>().has_continuous_path = loadfile.Load<bool>(pnames[i] + ".Script.has_continuous_path");

				// translation paths and param_list text boxes
				// save the number of sections
				int childct = loadfile.Load<int>(pnames[i] + ".param_list.childCount");

				// save the translation path in each section, and upper_val
				for (int k = 1; k < childct; k++)
				{
					// instantiate from param_end
					GameObject newparamtext = Instantiate(templine.transform.GetChild(4).GetChild(0).gameObject, templine.transform.GetChild(4));

					// name, position, box collider size
					newparamtext.name = loadfile.Load<string>(pnames[i] + ".param_list." + k.ToString() + ".name");
					newparamtext.transform.position = loadfile.Load<Vector3>(pnames[i] + ".param_list." + k.ToString() + ".position");
					newparamtext.GetComponent<BoxCollider2D>().size = loadfile.Load<Vector2>(pnames[i] + ".param_list." + k.ToString() + ".BoxCollider2D.size");

					newparamtext.GetComponent<param_text_Script>().translation_path =
						loadfile.Load<List<Vector3>>(pnames[i] + ".param_list." + k.ToString() + ".translation_path");

					newparamtext.GetComponent<param_text_Script>().upper_val =
						loadfile.Load<float>(pnames[i] + ".param_list." + k.ToString() + ".upper_val");

					newparamtext.GetComponent<param_text_Script>().section_length =
						loadfile.Load<float>(pnames[i] + ".param_list." + k.ToString() + ".section_length");

					// text label and label position
					newparamtext.GetComponent<TextMeshPro>().text = newparamtext.GetComponent<param_text_Script>().upper_val.ToString();
				}

				// update full translation path
				templine.GetComponent<penLine_script>().updateFullTranslationPathFromAllChildren();

				templine.GetComponent<penLine_script>().setupLineRendererFromTranslationPath();

				// show the line renderer
				templine.GetComponent<penLine_script>().showTranslationPath();

				// Update properties of text display
				templine.GetComponent<penLine_script>().createParamGUIText();
			}

			// argument_label
			rt = loadfile.Load<RectTransform>(pnames[i] + ".ArgumentLabel.RectTransform");
			templine.transform.GetChild(3).GetComponent<RectTransform>().sizeDelta = rt.sizeDelta;
			templine.transform.GetChild(3).GetComponent<RectTransform>().localPosition = rt.localPosition;

			templine.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text =
				loadfile.Load<string>(pnames[i] + ".ArgumentLabel.Text.Text");
			templine.transform.GetChild(3).gameObject.SetActive(loadfile.Load<bool>(pnames[i] + ".ArgumentLabel.enabled"));

			// destroy the gameobjects Easy Save created. Loading something into a variable causes Easy Save to create new objects
			Destroy(rt.gameObject);

			// set things that are set during creation: paintable_object, ...

			// this one is needed for the penline script functions
			templine.GetComponent<penLine_script>().paintable_object =
				GameObject.FindGameObjectWithTag("paintable_canvas_object");


		}

		templine = null;

		#endregion


		// LOAD ALL SET OBJECTS

		#region load sets

		// load all set lines in paintable canvas
		int numsetlines = loadfile.Load<int>("totalSetLines");

		List<string> setnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".setLineName")) setnames.Add(keys[i].Replace(".setLineName", ""));    // or just load through ES3
		}

		for (int i = 0; i < numsetlines; i++)
		{
			templine = Instantiate(SetLine, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = setnames[i];
			templine.tag = "set";

			// transform
			Transform t = loadfile.Load<Transform>(setnames[i] + ".Transform");
			//ES3.LoadInto<Transform>(setnames[i] + ".Transform", loadfilename, templine.transform);

			templine.transform.position = loadfile.Load<Vector3>(setnames[i] + ".Position");
			templine.transform.localRotation = t.localRotation;
			templine.transform.localScale = t.localScale;

			Destroy(t.gameObject);

			templine.GetComponent<TrailRenderer>().enabled = false;
			templine.GetComponent<LineRenderer>().enabled = false;

			// setLine_script vars
			templine.GetComponent<setLine_script>().current_feature = loadfile.Load<int>(setnames[i] + ".Script.current_feature");
			templine.GetComponent<setLine_script>().capacity = loadfile.Load<int>(setnames[i] + ".Script.capacity");

			templine.GetComponent<setLine_script>().timer_activated = loadfile.Load<bool>(setnames[i] + ".Script.timer_activated");
			templine.GetComponent<setLine_script>().timer_min = loadfile.Load<int>(setnames[i] + ".Script.timer_min");
			templine.GetComponent<setLine_script>().timer_max = loadfile.Load<int>(setnames[i] + ".Script.timer_max");
			templine.GetComponent<setLine_script>().timer_dt = loadfile.Load<float>(setnames[i] + ".Script.timer_dt");

			if (templine.GetComponent<setLine_script>().timer_activated)
			{
				// load the name of penline that is objectToCopy

			}

			templine.GetComponent<setLine_script>().this_set_name = loadfile.Load<string>(setnames[i] + ".Script.this_set_name");
			templine.GetComponent<setLine_script>().symbol_name = new Symbol(loadfile.Load<string>(setnames[i] + ".Script.symbol_name"));

			templine.GetComponent<setLine_script>().points = loadfile.Load<List<Vector3>>(setnames[i] + ".Script.points");
			templine.GetComponent<setLine_script>().centroid = loadfile.Load<Vector3>(setnames[i] + ".Script.centroid");
			templine.GetComponent<setLine_script>().anchor_offset_from_center = loadfile.Load<Vector3>(setnames[i] + ".Script.anchor_offset_from_center");
			templine.GetComponent<setLine_script>().maxx = loadfile.Load<float>(setnames[i] + ".Script.maxx");
			templine.GetComponent<setLine_script>().maxy = loadfile.Load<float>(setnames[i] + ".Script.maxy");
			templine.GetComponent<setLine_script>().minx = loadfile.Load<float>(setnames[i] + ".Script.minx");
			templine.GetComponent<setLine_script>().miny = loadfile.Load<float>(setnames[i] + ".Script.miny");

			templine.GetComponent<setLine_script>().set_line_material = loadfile.Load<Material>(setnames[i] + ".Script.set_line_material");

			templine.GetComponent<setLine_script>().retained_slider_value = loadfile.Load<int>(setnames[i] + ".Script.abstraction_layer");
			//templine.GetComponent<setLine_script>().prev_abstraction_layer = ES3.Load<int>(setnames[i] + ".Script.prev_abstraction_layer", loadfilename);
			//templine.GetComponent<setLine_script>().retained_slider_value = ES3.Load<int>(setnames[i] + ".Script.retained_slider_value", loadfilename);
			templine.GetComponent<setLine_script>().parent_asked_to_lower_layer = loadfile.Load<bool>(setnames[i] + ".Script.parent_asked_to_lower_layer");

			templine.GetComponent<setLine_script>().is_this_double_function_operand = loadfile.Load<bool>(setnames[i] + ".Script.is_this_double_function_operand");

			templine.GetComponent<setLine_script>().updateGestureType = true;


			// _anchor object
			RectTransform rt = loadfile.Load<RectTransform>(setnames[i] + "._anchor.RectTransform");
			templine.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = rt.sizeDelta;
			templine.transform.GetChild(0).GetComponent<RectTransform>().localPosition = rt.localPosition;
			templine.transform.GetChild(0).GetComponent<RectTransform>().localScale = rt.localScale;
			templine.transform.GetChild(0).GetComponent<RectTransform>().localRotation = rt.localRotation;

			Destroy(rt.gameObject);

			// create a material instance
			templine.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = templine.transform.GetChild(0).GetComponent<MeshRenderer>().materials[0];

			// mesh object
			GameObject meshObj = new GameObject("_meshobj");
			meshObj.AddComponent<MeshFilter>();
			meshObj.AddComponent<MeshRenderer>();

			//ES3.LoadInto<Transform>(setnames[i] + "._meshobj.Transform", loadfilename, meshObj.transform);

			meshObj.GetComponent<MeshFilter>().sharedMesh = loadfile.Load<Mesh>(setnames[i] + "._meshobj.Mesh");
			meshObj.GetComponent<MeshRenderer>().sharedMaterial = set_line_material;
			//ES3.Load<Material>(setnames[i] + "._meshobj.Mesh.Material", loadfilename);

			//Transform mesht = ES3.Load<Transform>(setnames[i] + "._meshobj.Transform", loadfilename);
			//Destroy(mesht.gameObject);

			meshObj.transform.position = loadfile.Load<Vector3>(setnames[i] + "._meshobj.Position");

			meshObj.transform.SetParent(templine.transform);

			// _stroke_converted_mesh, if exists
			if (loadfile.KeyExists(setnames[i] + "._meshobj._stroke_converted_mesh"))
			{
				GameObject strokemeshobj = new GameObject("_stroke_converted_mesh");
				bool meshnotsprite = loadfile.Load<bool>(setnames[i] + "._meshobj.MeshNotSprite");
				if (meshnotsprite)
				{
					strokemeshobj.AddComponent<MeshFilter>();
					strokemeshobj.AddComponent<MeshRenderer>();

					strokemeshobj.GetComponent<MeshFilter>().sharedMesh = loadfile.Load<Mesh>(setnames[i] + "._meshobj._stroke_converted_mesh");
					strokemeshobj.GetComponent<MeshRenderer>().sharedMaterial =
						loadfile.Load<Material>(setnames[i] + "._meshobj._stroke_converted_mesh.Material");
				}
				else
				{
					strokemeshobj.AddComponent<SpriteRenderer>();

					string sprite_filename = loadfile.Load<string>(setnames[i] + "._meshobj._stroke_converted_mesh");
					strokemeshobj.GetComponent<SpriteRenderer>().sprite = IMG2Sprite.instance.LoadNewSprite(sprite_filename, 2);
						//Resources.Load<Sprite>("ExampleImages/" + sprite_filename.Replace(".png", ""));
				}

				strokemeshobj.transform.position = loadfile.Load<Vector3>(setnames[i] + "._meshobj._stroke_converted_mesh.Position");

				strokemeshobj.transform.SetParent(meshObj.transform);
			}

			// The following takes care of some details based on where the anchor is, to move the text ahead and make it visible
			templine.transform.GetChild(0).Find("_text").transform.localPosition = new Vector3(0f, -1.5f, 0f);
			templine.transform.GetChild(0).Find("_text").GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;

			// set up the TeX legible layer text box
			templine.transform.GetChild(0).Find("_TeX").transform.position = templine.transform.position;
			//templine.GetComponent<setLine_script>().centroid;

			/*
			 * // Fit the sizeDelta within bounding box
			*/
			float bxdiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.x -
				templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.x;
			float bydiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.y -
				templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.y;
			float range1param = Mathf.InverseLerp(30f, 300f, bxdiff);
			//float range1param = Mathf.Lerp(30f, 300f, bxdiff);
			float sd = Mathf.LerpUnclamped(1, 5, range1param); // possible values for sizeDelta = [1, 5].
			templine.transform.GetChild(0).Find("_TeX").GetComponent<RectTransform>().sizeDelta = new Vector2(sd, sd);

			// turn off the text component until abstraction slider asks for the legible layer
			templine.transform.GetChild(0).Find("_TeX").GetComponent<TEXDraw>().enabled = false;

			// argument label

			templine.transform.GetChild(0).GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(0.5f, 0.5f);
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0.5f, 0.5f);   // TMP text
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
				templine.GetComponent<setLine_script>().this_set_name;

			// set the argument label position at the highest (max y) of points. Use global position (TransformPoint)
			templine.transform.GetChild(0).GetChild(2).transform.position =
				templine.transform.TransformPoint(
					templine.transform.GetComponent<setLine_script>().points[
				templine.GetComponent<setLine_script>().HighestMeshPointIndex()]
				);

			templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);

			// Texts
			templine.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = loadfile.Load<string>(setnames[i] + "._anchor._text.Text");
			templine.transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text = loadfile.Load<string>(setnames[i] + "._anchor._TeX.Text");
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = loadfile.Load<string>(setnames[i] + ".argument_label.Text.Text");

			// set things that are set during creation: paintable_object, ...
			templine.GetComponent<setLine_script>().paintable_object =
				GameObject.FindGameObjectWithTag("paintable_canvas_object");

			// Is this OOP property set?
			templine.GetComponent<setLine_script>().is_this_an_oop_property = loadfile.Load<bool>(setnames[i] + ".Script.is_this_an_oop_property");
			if (templine.GetComponent<setLine_script>().is_this_an_oop_property)
			{
				// set to symbolic layer
				templine.GetComponent<setLine_script>().retained_slider_value = 3;
			}
		}

		templine = null;

		#endregion


		// LOAD ALL FUNCTION OBJECTS

		#region load functions

		int numfunclines = loadfile.Load<int>("totalFunctionLines");

		List<string> funcnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".funcLineName")) funcnames.Add(keys[i].Replace(".funcLineName", ""));    // or just load through ES3
		}

		for (int i = 0; i < numfunclines; i++)
		{
			templine = Instantiate(FunctionLine, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = funcnames[i];
			templine.tag = "function";

			// transform
			Transform temp = loadfile.Load<Transform>(funcnames[i] + ".Transform");
			// copy transform properties
			templine.transform.localPosition = temp.localPosition;
			templine.transform.localScale = temp.localScale;
			templine.transform.localRotation = temp.localRotation;

			Destroy(temp.gameObject);

			// user given function name
			templine.GetComponent<functionLine_script>().user_given_function_name = loadfile.Load<string>(funcnames[i] + ".Script.user_given_function_name");

			templine.GetComponent<TrailRenderer>().enabled = false;
			templine.GetComponent<LineRenderer>().enabled = false;

			// functionLine_script vars
			templine.GetComponent<functionLine_script>().max_function_value =
				loadfile.Load<int>(funcnames[i] + ".Script.max_function_value");

			templine.GetComponent<functionLine_script>().points = loadfile.Load<List<Vector3>>(funcnames[i] + ".Script.points");
			templine.GetComponent<functionLine_script>().centroid = loadfile.Load<Vector3>(funcnames[i] + ".Script.centroid");
			templine.GetComponent<functionLine_script>().anchor_offset_from_center = loadfile.Load<Vector3>(funcnames[i] + ".Script.anchor_offset_from_center");
			templine.GetComponent<functionLine_script>().maxx = loadfile.Load<float>(funcnames[i] + ".Script.maxx");
			templine.GetComponent<functionLine_script>().maxy = loadfile.Load<float>(funcnames[i] + ".Script.maxy");
			templine.GetComponent<functionLine_script>().minx = loadfile.Load<float>(funcnames[i] + ".Script.minx");
			templine.GetComponent<functionLine_script>().miny = loadfile.Load<float>(funcnames[i] + ".Script.miny");

			templine.GetComponent<functionLine_script>().function_line_material =
				loadfile.Load<Material>(funcnames[i] + ".Script.function_line_material");

			templine.GetComponent<functionLine_script>().is_this_a_double_function =
				loadfile.Load<bool>(funcnames[i] + ".Script.is_this_a_double_function");

			templine.GetComponent<functionLine_script>().abstraction_layer = loadfile.Load<int>(funcnames[i] + ".Script.abstraction_layer");
			templine.GetComponent<functionLine_script>().prev_abstraction_layer = loadfile.Load<int>(funcnames[i] + ".Script.prev_abstraction_layer");
			templine.GetComponent<functionLine_script>().retained_slider_value = loadfile.Load<int>(funcnames[i] + ".Script.retained_slider_value");
			templine.GetComponent<functionLine_script>().parent_asked_to_lower_layer = loadfile.Load<bool>(funcnames[i] + ".Script.parent_asked_to_lower_layer");

			templine.GetComponent<functionLine_script>().is_this_double_function_operand = loadfile.Load<bool>(funcnames[i] + ".Script.is_this_double_function_operand");

			// set the gesture/legible layer fields: set previous_gestureType to initiate change in updateLegibleLayer().
			templine.GetComponent<functionLine_script>().previous_gestureType = "";
			templine.GetComponent<functionLine_script>().updateGestureType = true;


			// category filter
			// order: set, circle, box, ellipse, arrow, horizontal_ellipse, triangle, figure
			bool[] catfilter = loadfile.Load<bool[]>(funcnames[i] + ".Script.categoryFilter");
			templine.GetComponent<functionLine_script>().categoryFilter.set = catfilter[0];
			templine.GetComponent<functionLine_script>().categoryFilter.circle = catfilter[1];
			templine.GetComponent<functionLine_script>().categoryFilter.box = catfilter[2];
			templine.GetComponent<functionLine_script>().categoryFilter.ellipse = catfilter[3];
			templine.GetComponent<functionLine_script>().categoryFilter.arrow = catfilter[4];
			templine.GetComponent<functionLine_script>().categoryFilter.horizontal_ellipse = catfilter[5];
			templine.GetComponent<functionLine_script>().categoryFilter.triangle = catfilter[6];
			templine.GetComponent<functionLine_script>().categoryFilter.figure = catfilter[7];

			// _anchor object
			RectTransform rt = loadfile.Load<RectTransform>(funcnames[i] + "._anchor.RectTransform");
			templine.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = rt.sizeDelta;
			templine.transform.GetChild(0).GetComponent<RectTransform>().localPosition = rt.localPosition;
			templine.transform.GetChild(0).GetComponent<RectTransform>().localScale = rt.localScale;
			templine.transform.GetChild(0).GetComponent<RectTransform>().localRotation = rt.localRotation;

			Destroy(rt.gameObject);

			// create a material instance
			templine.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial = templine.transform.GetChild(0).GetComponent<MeshRenderer>().materials[0];

			// mesh object
			GameObject meshObj = new GameObject("_meshobj");
			meshObj.AddComponent<MeshFilter>();
			meshObj.AddComponent<MeshRenderer>();

			//ES3.LoadInto<Transform>(setnames[i] + "._meshobj.Transform", loadfilename, meshObj.transform);

			meshObj.GetComponent<MeshFilter>().sharedMesh = loadfile.Load<Mesh>(funcnames[i] + "._meshobj.Mesh");
			meshObj.GetComponent<MeshRenderer>().sharedMaterial = function_line_material;
			//ES3.Load<Material>(setnames[i] + "._meshobj.Mesh.Material", loadfilename);

			meshObj.transform.position = loadfile.Load<Vector3>(funcnames[i] + "._meshobj.Position");

			meshObj.transform.SetParent(templine.transform);

			// _stroke_converted_mesh, if exists
			if (loadfile.KeyExists(funcnames[i] + "._meshobj._stroke_converted_mesh"))
			{
				GameObject strokemeshobj = new GameObject("_stroke_converted_mesh");
				bool meshnotsprite = loadfile.Load<bool>(funcnames[i] + "._meshobj.MeshNotSprite");
				if (meshnotsprite)
				{
					strokemeshobj.AddComponent<MeshFilter>();
					strokemeshobj.AddComponent<MeshRenderer>();

					strokemeshobj.GetComponent<MeshFilter>().sharedMesh = loadfile.Load<Mesh>(funcnames[i] + "._meshobj._stroke_converted_mesh");
					strokemeshobj.GetComponent<MeshRenderer>().sharedMaterial =
						loadfile.Load<Material>(funcnames[i] + "._meshobj._stroke_converted_mesh.Material");
				}
				else
				{
					strokemeshobj.AddComponent<SpriteRenderer>();

					string sprite_filename = loadfile.Load<string>(funcnames[i] + "._meshobj._stroke_converted_mesh");
					strokemeshobj.GetComponent<SpriteRenderer>().sprite = IMG2Sprite.instance.LoadNewSprite(sprite_filename, 2);
					//Resources.Load<Sprite>("ExampleImages/" + sprite_filename.Replace(".png", ""));
				}

				strokemeshobj.transform.position = loadfile.Load<Vector3>(funcnames[i] + "._meshobj._stroke_converted_mesh.Position");

				strokemeshobj.transform.SetParent(meshObj.transform);
			}

			// The following takes care of some details based on where the anchor is, to move the text ahead and make it visible
				templine.transform.GetChild(0).Find("_anchor_TeX").transform.localPosition = new Vector3(0f, -1.5f, 0f);
			// undo the horizontal tapered scaling for the text rect transform
			templine.transform.GetChild(0).Find("_anchor_TeX").transform.localScale = new Vector3(0.005f, 0.01f, 0.005f);

			// set up the TeX legible layer text box
			templine.transform.GetChild(0).Find("_TeX").transform.position =
				templine.GetComponent<functionLine_script>().centroid;

			/*
			 * // Fit the sizeDelta within bounding box (for legible layer)
			*/
			float bxdiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.x -
				templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.x;
			float bydiff = templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.max.y -
				templine.transform.GetChild(1).GetComponent<MeshFilter>().sharedMesh.bounds.min.y;
			float range1param = Mathf.InverseLerp(30f, 300f, bxdiff);
			//float range1param = Mathf.Lerp(30f, 300f, bxdiff);
			float sd = Mathf.LerpUnclamped(1, 5, range1param); // possible values for sizeDelta = [1, 5].
			templine.transform.GetChild(0).Find("_TeX").GetComponent<RectTransform>().sizeDelta = new Vector2(sd, sd);

			// turn off the text component until abstraction slider asks for the legible layer
			templine.transform.GetChild(0).Find("_TeX").GetComponent<TEXDraw>().enabled = false;

			// set up the _top_TeX label with category filters
			templine.transform.GetChild(0).GetChild(3).position =
				templine.transform.TransformPoint(
					templine.transform.GetComponent<functionLine_script>().points[
						templine.GetComponent<functionLine_script>().HighestMeshPointIndex()]);

			templine.transform.GetChild(0).GetChild(3).localPosition += new Vector3(0, 0, -0.8f);   // move slightly upwards locally, z is y b/c of anchor rotation
			templine.transform.GetChild(0).GetChild(3).localScale = new Vector3(0.005f, 0.01f, 1); // undo the scale of anchor, and make it smaller too.

			templine.transform.GetChild(0).GetChild(3).GetComponent<TEXDraw>().text = "(), " + templine.name + ", \\legbox";


			// load the operator
			string op = loadfile.Load<string>(funcnames[i] + ".Script.currentOperator");
			// set the current operator to new operator
			if (op == "+") templine.GetComponent<functionLine_script>().current_op =
					templine.GetComponent<functionLine_script>().opPlus;
			else if (op == "*") templine.GetComponent<functionLine_script>().current_op =
					templine.GetComponent<functionLine_script>().opMult;
			else if (op == "/") templine.GetComponent<functionLine_script>().current_op =
					templine.GetComponent<functionLine_script>().opDiv;
			else if (op == "-") templine.GetComponent<functionLine_script>().current_op =
					templine.GetComponent<functionLine_script>().opMinus;

			// Set up the argument_label button and text.
			templine.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);

			templine.transform.GetChild(0).GetChild(2).transform.localScale = new Vector3(0.3f, 0.6f, 1f);  // undo the scale of anchor, and make it smaller too.
			templine.transform.GetChild(0).GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(0.6f, 0.6f);
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(0.3f, 0.3f);   // TMP text
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
				templine.GetComponent<functionLine_script>().currentOperator();

			templine.transform.GetChild(0).GetChild(2).position =
							templine.transform.TransformPoint(
								templine.transform.GetComponent<functionLine_script>().points[
									templine.GetComponent<functionLine_script>().HighestMeshPointIndex()]
							);
			templine.transform.GetChild(0).GetChild(2).position += new Vector3(0, 0, -40f); // set z separately

			// Texts
			templine.transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().text =
				loadfile.Load<string>(funcnames[i] + "._anchor._anchor_TeX.Text");
			templine.transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text =
				loadfile.Load<string>(funcnames[i] + "._anchor._TeX.Text");
			templine.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
				loadfile.Load<string>(funcnames[i] + ".argument_label.Text.Text");

			// set things that are set during creation: paintable_object, ...

			// this one is needed for the functionline script functions
			templine.GetComponent<functionLine_script>().paintable_object =
				GameObject.FindGameObjectWithTag("paintable_canvas_object");

			// halt live update until hierarchy is restored.
			templine.GetComponent<functionLine_script>().halt_live_update_for_double_function = true;

			// While loading a sketch, hierarchical functions may have the dynamic variable "current_feature" value as null
			// This throws an exception when trying to evaluate values. Turns out that calling updateFeature() sets up current_feature properly,
			// and gets rid of the error. Because, the first chunk of the code (unaffected by halt_live_update_for_double_function) casts
			// the dynamic value to specific types.
			templine.GetComponent<functionLine_script>().updateFeature();

			// set first and second operands, if they exist. Otherwise, turn off double function operand
			// note: we do this later after all functions are loaded.


		}

		templine = null;

		#endregion


		// LOAD PROPERTY EDGES

		#region load property edges

		int numproplines = loadfile.Load<int>("totalPropertyEdges");

		List<string> propnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".propertyEdgeName")) propnames.Add(keys[i].Replace(".propertyEdgeName", ""));    // or just load through ES3
		}

		for (int i = 0; i < numproplines; i++)
		{
			templine = Instantiate(PropertyEdgePrefab, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = propnames[i];
			templine.tag = "oop_property_edge";

			templine.GetComponent<OOP_Property_Edge_script>().source_object = GameObject.Find(
				loadfile.Load<string>(propnames[i] + ".source_object.Name"));

			templine.GetComponent<OOP_Property_Edge_script>().propertyTargetSet = GameObject.Find(
				loadfile.Load<string>(propnames[i] + ".propertyTargetSet.Name"));

			templine.GetComponent<OOP_Property_Edge_script>().showPropertyEdge = loadfile.Load<bool>(propnames[i] + ".showPropertyEdge");
		}

		templine = null;

		#endregion

		// LOAD SPAWN EDGES
		#region load spawn edges

		int numspawnedges = loadfile.Load<int>("totalSpawnEdges");

		List<string> spawnnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".spawnEdgeName")) spawnnames.Add(keys[i].Replace(".spawnEdgeName", ""));    // or just load through ES3
		}

		for (int i = 0; i < numspawnedges; i++)
		{
			templine = Instantiate(SpawnEdgePrefab, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = spawnnames[i];
			templine.tag = "spawn_edge";

			templine.GetComponent<SpawnEdgeScript>().source_object = GameObject.Find(
				loadfile.Load<string>(spawnnames[i] + ".source_object.Name"));

			templine.GetComponent<SpawnEdgeScript>().target_object = GameObject.Find(
				loadfile.Load<string>(spawnnames[i] + ".target_object.Name"));

			templine.GetComponent<EdgeCollider2D>().edgeRadius = 10;

			templine.GetComponent<SpawnEdgeScript>().edge_set = true;
		}

		templine = null;
		
		#endregion

		// RESTORE HIERARCHY OF OBJECTS
		RestoreHierarchy();

		// RESTORE ANY DOUBLE FUNCTION OPERAND REFERENCE
		loadDoubleFunctionOperands();

		// UNHALT ALL FUNCTIONS, RESUME EVALUATIONS
		unhaltFunctionLiveUpdates();

		Debug.Log("Loading: Objects loaded.");

		// FOR TIMER SETS WITH REAL PEN LINES (AS OPPOSED TO DUMMY PEN OBJECTS), CREATE REFERENCES TO PENLINES FOR obectToCopy
		// ALSO, RESTART THOSE TIMERS?
		GameObject[] sets = GameObject.FindGameObjectsWithTag("set");

		for (int i = 0; i < sets.Length; i++)
		{
			if (sets[i].GetComponent<setLine_script>().timer_activated)
			{
				if (sets[i].transform.childCount > 2)
				{
					sets[i].GetComponent<setLine_script>().objectToCopy = sets[i].transform.GetChild(2).gameObject;
				}
				else
				{
					// use a default prefab otherwise.
					sets[i].GetComponent<setLine_script>().objectToCopy = 
						Resources.Load<GameObject>("Prefabs/penLine_timer_default");
					sets[i].GetComponent<setLine_script>().objectToCopy.transform.GetChild(0).GetComponent<MeshFilter>().sharedMesh = 
						Resources.Load<Mesh>("Models/TestPen");
					sets[i].GetComponent<setLine_script>().objectToCopy.transform.GetChild(0).GetComponent<MeshRenderer>().sharedMaterial.color = Color.grey;

					// fix mesh offset manually. Other methods to fix the offset failed.
					sets[i].GetComponent<setLine_script>().objectToCopy.transform.GetChild(0).transform.localPosition = new Vector3(-17, 25, 40.5f);

					sets[i].GetComponent<setLine_script>().objectToCopy.transform.position = sets[i].transform.position - new Vector3(0, -10, 0);
				}
			}
		}

		// Restore hierarchy in the next frame
		//Invoke("RestoreHierarchy", 0);

		Invoke("loadEdgeLines", 0);
		
		// resume interaction input and set to pan mode in loadEdgeLines()
	}


	private void RestoreHierarchy()
	{
		// RESET THE HIERARCHY STRUCTURE

		string[] keys = loadfile.GetKeys();

		List<string> objnames = new List<string>();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".Children.Name."))
			{
				objnames.Add(keys[i].Split(new char[] { '.' })[0]);
			}
		}

		for (int i = 0; i < objnames.Count; i++)
		{
			//Debug.Log("Looking at parent: " + objnames[i]);

			GameObject parent = GameObject.Find(objnames[i]);
			for (int k = 0; k < keys.Length; k++)
			{
				if (keys[k].Contains(objnames[i] + ".Children.Name."))
				{
					GameObject child = GameObject.Find(loadfile.Load<string>(keys[k]));

					child.transform.SetParent(parent.transform);

					//Debug.Log("parent " + parent.name + " set for child: " + child.name);
				}
			}
		}

		Debug.Log("Loading: Hierarchy restored.");
	}

	private void loadDoubleFunctionOperands()
	{
		// go through functions, which ones are double functions?
		GameObject[] funcs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcs.Length; i++)
		{
			if (funcs[i].GetComponent<functionLine_script>().is_this_a_double_function)
			{
				// are there first and second operands saved?
				string fopname = "", sopname = "";
				if (loadfile.KeyExists(funcs[i].name + ".Script.first_operand.Name"))
				{
					fopname = loadfile.Load<string>(funcs[i].name + ".Script.first_operand.Name");
				}
				if (loadfile.KeyExists(funcs[i].name + ".Script.second_operand.Name"))
				{
					sopname = loadfile.Load<string>(funcs[i].name + ".Script.second_operand.Name");
				}
				if (fopname != "" && sopname != "")
				{
					funcs[i].GetComponent<functionLine_script>().first_operand = GameObject.Find(fopname);
					funcs[i].GetComponent<functionLine_script>().second_operand = GameObject.Find(sopname);

					//Debug.Log("found two operands.");

					if (funcs[i].GetComponent<functionLine_script>().first_operand == null ||
						funcs[i].GetComponent<functionLine_script>().second_operand == null)
					{
						//Debug.Log("operands searched with named but not found.");

						// not found in the game hierarchy, convert to simple function
						funcs[i].GetComponent<functionLine_script>().is_this_a_double_function = false;
						funcs[i].GetComponent<functionLine_script>().current_op = funcs[i].GetComponent<functionLine_script>().opPlus;
					}
				}
				else
				{
					//Debug.Log("operand names not present in the file.");
					// not found in saved file, turn this into a simple function
					funcs[i].GetComponent<functionLine_script>().is_this_a_double_function = false;
					funcs[i].GetComponent<functionLine_script>().current_op = funcs[i].GetComponent<functionLine_script>().opPlus;
				}
			}
		}
	}

	private void unhaltFunctionLiveUpdates()
	{
		GameObject[] funcs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcs.Length; i++)
		{
			funcs[i].GetComponent<functionLine_script>().halt_live_update_for_double_function = false;
		}
	}


	private void loadEdgeLines()
	{
		// LOAD ALL EDGE LINES INTO PAINTABLE CANVAS
		int numedgelines = loadfile.Load<int>("totalEdgeLines");

		List<string> edgenames = new List<string>();

		string[] keys = loadfile.GetKeys();

		for (int i = 0; i < keys.Length; i++)
		{
			if (keys[i].Contains(".edgeLineName")) edgenames.Add(keys[i].Replace(".edgeLineName", ""));    // or just load through ES3
		}

		for (int i = 0; i < numedgelines; i++)
		{
			templine = Instantiate(EdgeLinePrefab, new Vector3(0, 0, 0), Quaternion.identity, paintable.transform);

			templine.name = edgenames[i];
			templine.tag = "edgeline";

			// transform
			loadfile.LoadInto<Transform>(edgenames[i] + ".Transform", templine.transform);

			// source and target objects
			templine.GetComponent<edgeLine_script>().source_object = GameObject.Find(
				loadfile.Load<string>(edgenames[i] + ".source_object.Name"));

			templine.GetComponent<edgeLine_script>().target_object = GameObject.Find(
				loadfile.Load<string>(edgenames[i] + ".target_object.Name"));

			// full condition text
			templine.GetComponent<edgeLine_script>().full_condition_text = loadfile.Load<string>(edgenames[i] + ".full_condition_text");

			// edge collider 2D
			templine.GetComponent<EdgeCollider2D>().points =
				loadfile.Load<Vector2[]>(edgenames[i] + ".EdgeCollider2D.points");
			templine.GetComponent<EdgeCollider2D>().edgeRadius =
				loadfile.Load<float>(edgenames[i] + ".EdgeCollider2D.radius");

			// set up TEXDraw text
			templine.transform.GetChild(0).GetComponent<TEXDraw>().GetComponent<RectTransform>().sizeDelta = new Vector2(40, 30);

			// set up the edge causal mechanism
			templine.GetComponent<edgeLine_script>().edge_set = true;

			templine.GetComponent<edgeLine_script>().parseConditionString();

			templine.GetComponent<edgeLine_script>().updateTEXLabel();

		}

		templine = null;

		
		resumeInteractionInput();

		// Change the mode to pan
		transform.parent.Find("Pan").GetComponent<AllButtonsBehavior>().whenSelected();
	}

	private void resumeInteractionInput()
	{
		// Resume all interaction operations
		GameObject.FindGameObjectWithTag("paintable_canvas_object").GetComponent<Paintable_Script>().IOInProgress = false;
	}

	public string findLastSavedFileName()
	{
		//string[] files = ES3.GetFiles();
		string[] files = Directory.GetFiles(Application.persistentDataPath, "*.es3");
		//Debug.Log(files[0]);

		List<string> tobesorted = new List<string>();
		for (int i = 0; i < files.Length; i++)
		{
			if (files[i].Contains(".es3")) tobesorted.Add(files[i]);
		}
		if (tobesorted.Count > 0)
		{
			tobesorted.Sort();
			/*
			for(int k = 0; k < tobesorted.Count; k++)
				Debug.Log(tobesorted[k]);
			*/
			return tobesorted[tobesorted.Count - 1];
		}
		else
			return "";
	}

	/*
	public GameObject TraverseHierarchy(string h)
	{
		string inner, parent;
		char[] end = { '{' };
		char[] start = { ',', '{' };
		GameObject obj;
		//if (h.Substring(0, 2) == ",{" && h.EndsWith("}"))
		//{
		h.Remove(0, 2);
		h.Remove(h.Length - 1, 1);
		if (h.Split(',').Length == 1)
		{
			obj = new GameObject(h);
			return obj;
		}
		else
		{

		}

		//}
		GameObject obj = new GameObject
		string message = obj.name;
		foreach (Transform child in obj.transform)
		{
			if (child.tag == "penline" || child.tag == "set" || child.tag == "function")
			{
				message += ",{" + Traverse(child.gameObject) + "}";
			}
		}
		return message;
	}
	*/

	public void LoadImage()
	{
		//string[] spl = loadfilename.Split('\\');
		//Sprite sprite = Resources.Load<Sprite>("ExampleImages/" + spl[spl.Length - 1].Replace(".png",""));

		transform.GetComponent<CreatePrimitives>().CreatePenLine(loadfilename);// spl[spl.Length - 1]);
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
