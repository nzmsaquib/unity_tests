﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SelectButtonBehavior : MonoBehaviour
{
	public bool selected = false;

	public static bool isPointerOverSelect = false;

	public void whenSelected()
	{
		selected = true;

		// change icon color
		transform.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);

		disableAllPenObjectColliders();
		disableAllSetAnchorColliders();
		disableAllFunctionAnchorColliders();
		hideAllEdgeLines();

		hideGlobalSlider();

		transform.parent.Find("Pan").GetComponent<PanButtonBehavior>().whenDeselected();
		transform.parent.Find("Pencil").GetComponent<PencilButtonBehavior>().whenDeselected();
		transform.parent.Find("Edge_draw").GetComponent<EdgeButtonBehavior>().whenDeselected();
		transform.parent.Find("FunctionButton").GetComponent<FunctionButtonBehavior>().whenDeselected();
		transform.parent.Find("Eraser").GetComponent<EraserButtonBehavior>().whenDeselected();
		transform.parent.Find("StaticPen").GetComponent<StaticPenButtonBehavior>().whenDeselected();
	}

	public void whenDeselected()
	{
		selected = false;

		// change icon color
		transform.GetComponent<Image>().color = new Color(1, 1, 1, 1);
	}

	public void hideGlobalSlider()
	{
		transform.parent.Find("Global_Stroke_Details").gameObject.SetActive(false);
	}

	public void disableAllPenObjectColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = 0; i < penobjs.Length; i++)
		{
			penobjs[i].GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void disableAllSetAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] setobjs = GameObject.FindGameObjectsWithTag("set");
		for (int i = 0; i < setobjs.Length; i++)
		{
			setobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void disableAllFunctionAnchorColliders()
	{
		// disable the pen box colliders that are immediate children of the paintable object
		GameObject[] funcobjs = GameObject.FindGameObjectsWithTag("function");
		for (int i = 0; i < funcobjs.Length; i++)
		{
			funcobjs[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
		}
	}

	public void hideAllEdgeLines()
	{
		GameObject[] edgeobjs = GameObject.FindGameObjectsWithTag("edgeline");
		for (int i = 0; i < edgeobjs.Length; i++)
		{
			edgeobjs[i].GetComponent<LineRenderer>().enabled = false;

			edgeobjs[i].GetComponent<EdgeCollider2D>().enabled = false;

			edgeobjs[i].transform.GetChild(0).GetComponent<TEXDraw>().enabled = false;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (EventSystem.current.IsPointerOverGameObject(0))
		{
			isPointerOverSelect = true;
		}
		else
		{
			isPointerOverSelect = false;
		}
	}
}
