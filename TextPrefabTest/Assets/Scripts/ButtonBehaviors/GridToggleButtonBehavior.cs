﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class GridToggleButtonBehavior : MonoBehaviour
{

	public bool selected = false;

	public static bool isPointerOverGridButton = false;

	public void whenSelected()
	{
		selected = !selected;

		if (selected)
		{
			GameObject.Find("Paintable").GetComponent<MeshRenderer>().material = GameObject.Find("Paintable").GetComponent<Paintable_Script>().grid_material;
			GameObject.Find("text_message_worldspace").transform.position = new Vector3(0f, 0f, -3.5f);
			GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text = "(0, 0)";
		}
		else
		{
			GameObject.Find("Paintable").GetComponent<MeshRenderer>().material = GameObject.Find("Paintable").GetComponent<Paintable_Script>().default_background_material;
			GameObject.Find("text_message_worldspace").GetComponent<TextMeshProUGUI>().text = "";
		}

	}


	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (EventSystem.current.IsPointerOverGameObject(0))
		{
			isPointerOverGridButton = true;
		}
		else
		{
			isPointerOverGridButton = false;
		}
	}
}
