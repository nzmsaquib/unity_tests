﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.

public class AbstractSliderEvents : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

	public bool buttonPressed;

	public void OnPointerDown(PointerEventData eventData)
	{
		buttonPressed = true;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		buttonPressed = false;
	}
}