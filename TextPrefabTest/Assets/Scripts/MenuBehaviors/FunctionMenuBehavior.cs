﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionMenuBehavior : MonoBehaviour
{

	public GameObject parent_function;

	private void OnDestroy()
	{
		// change anchor material instance color
		parent_function.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = new Color(0.1076f, 0.3867f, 0.1386f, 0);

		parent_function.GetComponent<functionLine_script>().exitMenu(this.gameObject);
	}
}
