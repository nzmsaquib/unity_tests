﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Michsky.UI.ModernUIPack;

public class PenLineRotationMenuBehavior : MonoBehaviour
{
	public GameObject parent_penline;

	private void OnDestroy()
	{
		// retain slider value and update 
		if (transform.Find("_radial_slider").GetComponent<RadialSlider>().currentValue > 0)
		{
			parent_penline.GetComponent<penLine_script>().max_possible_rotation = transform.Find("_radial_slider").GetComponent<RadialSlider>().currentValue;
			parent_penline.GetComponent<penLine_script>().apply_rotation = true;
		}
		else
		{
			parent_penline.GetComponent<penLine_script>().max_possible_rotation = 0f;
			parent_penline.GetComponent<penLine_script>().apply_rotation = false;
			// set rotation back to 0
			parent_penline.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
		}

		if (transform.Find("_min_textbox").GetComponent<TMP_InputField>().text != null)
		{
			parent_penline.GetComponent<penLine_script>().min_rotation_val = float.Parse(transform.Find("_min_textbox").GetComponent<TMP_InputField>().text);
		}
		else
		{
			parent_penline.GetComponent<penLine_script>().min_rotation_val = 0f;
		}

		if (transform.Find("_max_textbox").GetComponent<TMP_InputField>().text != null)
		{
			parent_penline.GetComponent<penLine_script>().max_rotation_val = float.Parse(transform.Find("_max_textbox").GetComponent<TMP_InputField>().text);
		}
		else
		{
			parent_penline.GetComponent<penLine_script>().max_rotation_val = 5f;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
