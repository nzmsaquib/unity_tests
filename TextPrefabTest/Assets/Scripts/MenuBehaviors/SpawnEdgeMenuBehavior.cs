﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnEdgeMenuBehavior : MonoBehaviour
{
	public GameObject SpawnEdgeParent;

	public Sprite play_image, stop_image;

	public void onClickPlayButton()
	{
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().play = !SpawnEdgeParent.GetComponent<SpawnEdgeScript>().play;

		// retain initial randomness slider value
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().initRandomness = transform.Find("_initial_slider").GetComponent<Slider>().value;

		// retain dt
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().dt = float.Parse(transform.Find("_dt_input").GetComponent<InputField>().text);

		// retain path toggle
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().loop_path = transform.Find("_path_toggle").GetComponent<Toggle>().isOn;

		// change images as necessary
		if (!SpawnEdgeParent.GetComponent<SpawnEdgeScript>().play)
		{
			// clicked stop, should show play
			transform.Find("_play_stop").GetComponent<Button>().GetComponent<Image>().sprite = play_image;
		}
		else
		{
			// clicked play, should show stop
			transform.Find("_play_stop").GetComponent<Button>().GetComponent<Image>().sprite = stop_image;
		}
	}

	public void onClickPathToggle()
	{
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().show_paths = transform.Find("_path_toggle").GetComponent<Toggle>().isOn;
			//!SpawnEdgeParent.GetComponent<SpawnEdgeScript>().show_paths;
	}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnDestroy()
	{
		// accept all the changes and update the parent object

		// retain initial randomness slider value
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().initRandomness = transform.Find("_initial_slider").GetComponent<Slider>().value;

		// retain dt
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().dt = float.Parse(transform.Find("_dt_input").GetComponent<InputField>().text);

		// retain path toggle
		SpawnEdgeParent.GetComponent<SpawnEdgeScript>().loop_path = transform.Find("_path_toggle").GetComponent<Toggle>().isOn;
	}
}
