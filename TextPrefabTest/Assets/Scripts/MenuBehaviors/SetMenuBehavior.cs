﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMenuBehavior : MonoBehaviour
{
	public GameObject parent_set;

	private void OnDestroy()
	{
		// change anchor material instance color
		parent_set.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = new Color(0.0838f, 0.0937f, 0.6132f, 1f);

		parent_set.GetComponent<setLine_script>().exitMenu(this.gameObject);
	}
}
