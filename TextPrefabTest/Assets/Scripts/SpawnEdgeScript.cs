﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEdgeScript : MonoBehaviour
{
	public List<Vector3> points = new List<Vector3>();
	public Vector3 centroid;

	public bool draggable_now = false;

	public Material edge_line_material;

	public GameObject paintable;

	public bool edge_set = false;

	public bool play = false;

	// time related variables
	public float dt = 1f;
	public float elapsedTime = 0f;

	// initial position randomness
	public float initRandomness = 0.5f;

	// show paths of particles
	public bool show_paths = true;

	// should particles repeat or just do one run?
	public bool loop_path = true;

	// update amount (int)
	public float updateAmount = 1;

	public GameObject source_object, target_object;

	// pen object inventory
	Dictionary<GameObject, float> spawned = new Dictionary<GameObject, float>();
	List<GameObject> keys;

	public void updateEdge()
	{
		if (edge_set && play)
		{
			// update time 
			elapsedTime += Time.deltaTime;

			// is it time to spawn? Then spawn one particle and reset elapsed
			if (elapsedTime > dt)
			{
				elapsedTime = 0f;

				int numparticles = 0;
				if (source_object.tag == "set") numparticles = source_object.GetComponent<setLine_script>().current_feature;
				else if (source_object.tag == "function") numparticles = (int)source_object.GetComponent<functionLine_script>().current_feature;
				if (spawned.Keys.Count <= numparticles)
				{
					Vector3 randomvecamount = new Vector3(Random.Range(-40 * initRandomness, 40 * initRandomness), Random.Range(-40 * initRandomness, 40 * initRandomness), 0f);

					GameObject sp = Instantiate(target_object,
						target_object.transform.position + randomvecamount,
						Quaternion.identity, 
						paintable.transform);

					sp.name += "_spawned_" + gameObject.name + "_" + source_object.name;

					// For some reason, Unity does not copy the upper_val in param_script_text for discrete sections. Copy them here.
					for (int i = 0; i < sp.transform.GetChild(4).childCount; i++)
					{
						sp.transform.GetChild(4).GetChild(i).GetComponent<param_text_Script>().upper_val =
							target_object.transform.GetChild(4).GetChild(i).GetComponent<param_text_Script>().upper_val;
					}

					// change heightScale of perlin noise
					sp.GetComponent<penLine_script>().heightScale *= Random.value;

					//sp.GetComponent<penLine_script>().createParamGUIText();
					sp.GetComponent<penLine_script>().hideTranslationPath();

					spawned.Add(sp, 0f);
				}
				else
				{
					// needed to loop. Clean up everything. Comment out to stop loop and just do one run.
					//cleanUp();
				}

				// check on all other particles and update value if dt reached
				// delete/repeat if end of line reached
				keys = new List<GameObject>(spawned.Keys);
				for (int i = 0; i < keys.Count; i++)
				{
					spawned[keys[i]] += updateAmount;

					// check show_paths and update path visibility
					if (target_object.GetComponent<penLine_script>().global_details_on_path && keys[i].GetComponent<LineRenderer>() != null)
					{
						keys[i].GetComponent<LineRenderer>().enabled = true;
					}
					else if (!target_object.GetComponent<penLine_script>().global_details_on_path && keys[i].GetComponent<LineRenderer>() != null)
					{
						keys[i].GetComponent<LineRenderer>().enabled = false;
					}

					///*
					int childct = keys[i].transform.GetChild(4).childCount;
					if (loop_path && spawned[keys[i]] >= keys[i].transform.GetChild(4).GetChild(childct - 1).GetComponent<param_text_Script>().upper_val)
					{
						// destroy game object and remove from spawned list, 
						// or repeat the path
						spawned[keys[i]] = 0;	// send it back to beginning of path
					}
					//*/

					//Debug.Log("spawned " + i.ToString() + spawned[keys[i]].ToString());
					//keys[i].GetComponent<penLine_script>().updateFeatureAction(spawned[keys[i]]);

				}
			}

			// check on all other particles and update feature action continuously (needed for particles to keep moving with lerp)
			keys = new List<GameObject>(spawned.Keys);
			for (int i = 0; i < keys.Count; i++)
			{
				keys[i].GetComponent<penLine_script>().updateFeatureAction(spawned[keys[i]]);
			}

		}

		else if (edge_set && !play)
		{
			// clean up everything, if there's anything.

			cleanUp();
		}
	}

	public void cleanUp()
	{
		// clean up all pen objects with name containing "_spawned"
		GameObject[] penobjs = GameObject.FindGameObjectsWithTag("penline");
		for (int i = penobjs.Length - 1; i > -1; i--)
		{
			if (penobjs[i].name.Contains("_spawned_" + gameObject.name + "_" + source_object.name))
			{
				Debug.Log(penobjs[i].name);
				Destroy(penobjs[i]);
			}
		}

		// clean up the spawned list
		spawned.Clear();
	}

	void Awake()
    {
		paintable = GameObject.FindGameObjectWithTag("paintable_canvas_object");
    }

    // Update is called once per frame
    void Update()
    {
		updateEdge();
    }
}
