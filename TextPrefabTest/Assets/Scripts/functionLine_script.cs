﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using TMPro;
using Symbolism;
using Symbolism.AlgebraicExpand;
using Symbolism.RationalizeExpression;
using Symbolism.Substitute;
using Michsky.UI.ModernUIPack;

public class functionLine_script : MonoBehaviour
{
	public Func<float, float, float> opPlus = (a, b) => a + b;
	public Func<float, float, float> opMinus = (a, b) => a - b;
	public Func<float, float, float> opMult = (a, b) => a * b;
	public Func<float, float, float> opDiv = (a, b) => a / b;

	// current operation for the function brush
	public Func<float, float, float> current_op;

	// actionable menu properties
	public enum Properties
	{
		count,  // int, number of containers present in the function
		capacity,   // how many elements can it hold
		value,
		isEmpty,    // bool returns if it contains a particular "type" of element
		name,   // string
		element // return an element (index can be typed in a text box)
	};

	public Properties current_property = Properties.value;

	// programmable set/get properties and menu-selected properties
	public struct Features
	{
		public string name;
		public float value;
		public bool locked;
		public int count;
		public int capacity;
		public bool isEmpty;
		public GameObject element;
	};

	public Features features;

	public dynamic current_feature;

	// OOP related variables
	public GameObject OOP_properties_submenu_prefab, oop_properties_submenu = null;

	// Category Filter variables
	public GameObject category_filter_submenu_prefab, category_filter_submenu = null;
	public struct CategoryFilter
	{
		public bool set, circle, box, ellipse, arrow, horizontal_ellipse, triangle, figure;
	};
	public CategoryFilter categoryFilter;

	public int max_function_value = 5;

	// this function's symbolic expression
	public string symbolic_expr = "";
	public MathObject symbolic_expression;

	public List<Vector3> points = new List<Vector3>();
	public Vector3 centroid;
	public Vector3 anchor_offset_from_center;
	public float maxx = -100000f, maxy = -100000f, minx = 100000f, miny = 100000f;

	public bool draggable_now = false;
	public int relayCount = 0;

	public Material function_line_material;

	public Button pan_button;

	public GameObject paintable_object;

	// feature list -- use a single variable for now
	public int total_count = 0;

	// double function related variable
	public bool halt_live_update_for_double_function = false;
	public bool is_this_a_double_function = false;
	public int operand_count = 0;
	public GameObject first_operand, second_operand;

	public int double_function_operand_order = 0;
	string order_string = "";

	// abstraction layer related variables
	public int abstraction_layer = 1;
	public int prev_abstraction_layer = 1;
	public int retained_slider_value = 1;
	public Slider abs_slider;

	// parent's command for abstraction layer
	public bool parent_asked_to_lower_layer = false;

	// legible layer related variables
	public string gestureType = "", previous_gestureType = "";
	public bool updateGestureType = false;
	public MathObject legible1, legible2;

	// menu related variables
	public string menu_name = "";

	// other interaction variables
	private Vector3 touchDelta = new Vector3();

	// double function related variables
	public bool is_this_double_function_operand = false;

	// anchor, argument label, _top_TeX label etc. related
	public Vector3 _top_TeX_upper_local_position_increment = new Vector3(0, 0, -0.3f);
	public Vector3 _top_TeX_lower_local_position_increment = new Vector3(0, 0, -0.7f);
	public string filtered_categories = "\\legbox";
	public string user_given_function_name = "";

	// play log related variables
	public Dictionary<string, Vector2> saved_positions = new Dictionary<string, Vector2>();
	public Dictionary<string, int> abstraction_log = new Dictionary<string, int>();

	// sprite related variables
	public string sprite_filename = "";

	// Pen interaction
	Pen currentPen;

	// Action History
	public Sprite prev_snapshot, current_snapshot;
	public int prev_child_count, current_child_count;
	public List<GameObject> ChildrenHistory = new List<GameObject>();
	public List<GameObject> prevChildrenHistory = new List<GameObject>();

	// global stroke details
	public GameObject details_dropdown;
	public bool global_details_on_anchor = true;
	public bool global_details_on_mesh = true;

	public void computeCentroid()
	{
		float totalx = 0, totaly = 0, totalz = 0;
		for (int i = 0; i < points.Count; i++)
		{
			totalx += points[i].x;
			totaly += points[i].y;
			totalz += points[i].z;
		}
		centroid = new Vector3(totalx / points.Count, totaly / points.Count, totalz / points.Count);
	}

	public void computeBounds()
	{
		maxx = -100000f; maxy = -100000f; minx = 100000f; miny = 100000f;

		for (int i = 0; i < points.Count; i++)
		{
			if (maxx < points[i].x) maxx = points[i].x;
			if (maxy < points[i].y) maxy = points[i].y;
			if (minx > points[i].x) minx = points[i].x;
			if (miny > points[i].y) miny = points[i].y;
		}
	}

	public void calculateAnchorOffset()
	{
		anchor_offset_from_center = transform.GetChild(0).position - new Vector3(centroid.x, centroid.y, points[0].z);
		anchor_offset_from_center.z = 0;
	}

	public void fromGlobalToLocalPoints()
	{
		// transform all points from world to local coordinate with respect to the transform position of the set game object
		for (int i = 0; i < points.Count; i++)
		{
			points[i] = transform.InverseTransformPoint(points[i]);
		}
	}

	public List<Vector3> fromLocalToGlobalPoints()
	{
		// Assumes fromGlobalToLocalPoints() has already been called on points set
		List<Vector3> gbpoints = new List<Vector3>();

		for (int i = 0; i < points.Count; i++)
		{
			gbpoints.Add(transform.TransformPoint(points[i]));
		}

		return gbpoints;
	}

	public bool isInsidePolygon(Vector3 p)
	{
		int j = points.Count - 1;
		bool inside = false;
		for (int i = 0; i < points.Count; j = i++)
		{
			if (((points[i].y <= p.y && p.y < points[j].y) || (points[j].y <= p.y && p.y < points[i].y)) &&
			   (p.x < (points[j].x - points[i].x) * (p.y - points[i].y) / (points[j].y - points[i].y) + points[i].x))
				inside = !inside;
		}
		return inside;
	}

	public void checkHitAndMove()
	{
		// assumes a touch has registered and pan mode is selected
		if (Paintable_Script.pan_button.GetComponent<AllButtonsBehavior>().selected == true)
		{
			//currentPen = Pen.current;

			if (PenTouchInfo.PressedThisFrame)//currentPen.tip.wasPressedThisFrame)//(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
			{

				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);//currentPen.position.ReadValue());
				RaycastHit Hit;

				// check for a hit on the anchor object
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == transform.GetChild(0).gameObject)
				{

					draggable_now = true;

					Vector3 vec = Hit.point; //+ new Vector3(0, 0, -40); // Vector3.up * 0.1f;
											 //Debug.Log(vec);

					// enforce the same z coordinate as the rest of the points in the parent set object
					vec.z = -40f;
					
					touchDelta = transform.GetChild(0).position - vec;

					// change anchor color
					transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.gray;
				}
			}

			if (PenTouchInfo.PressedNow)//currentPen.tip.isPressed)//(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				//Debug.Log("anchor touched");

				var ray = Camera.main.ScreenPointToRay(PenTouchInfo.penPosition);//currentPen.position.ReadValue());
				RaycastHit Hit;

				// check for a hit on the anchor object
				if (Physics.Raycast(ray, out Hit) && Hit.collider.gameObject == transform.GetChild(0).gameObject)
				{
					//Debug.Log(transform.name);

					draggable_now = true;

					Vector3 vec = Hit.point; //+ new Vector3(0, 0, -40); // Vector3.up * 0.1f;
											 //Debug.Log(vec);

					// enforce the same z coordinate as the rest of the points in the parent set object
					vec.z = -40f;

					Vector3 diff = vec - transform.GetChild(0).position + touchDelta;
					diff.z = 0;

					// don't move right away, move if a threshold has been crossed
					// 5 seems to work well in higher zoom levels and for my finger
					//if (Vector3.Distance(transform.position, vec) > 5)
						// update the function position. 
						transform.position += diff;

					// update the menu position if a menu is currently open/created
					GameObject menu_obj = GameObject.Find(menu_name);
					if (menu_obj != null)
					{
						GameObject.Find(menu_name).transform.position = vec;
					}

					// if there are pen objects in the immediate hierarchy, and they have a path defined and abs. layer = 1, 
					// then update the red path to move with the function hierarchy
					for (int i = 2; i < transform.childCount; i++)
					{
						if (transform.GetChild(i).tag == "penline" &&
							transform.GetChild(i).GetComponent<penLine_script>().abstraction_layer == 1)
						{
							if(transform.GetChild(i).GetComponent<penLine_script>().translation_path.Count > 0)
								transform.GetChild(i).GetComponent<penLine_script>().calculateTranslationPathIfAlreadyDefined();
						}
					}
				}
			}

			else if (PenTouchInfo.ReleasedThisFrame && draggable_now)//(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended && draggable_now)
			{
				draggable_now = false;

				touchDelta = new Vector3(); // reset touchDelta

				// change anchor color
				transform.GetChild(0).GetComponent<MeshRenderer>().material.color = new Color(0.1076f, 0.3867f, 0.1386f, 0);

				// TODO: as an added safety measure, we could use a raycast at the touch position to see if it hits the set anchor
				// This might be the reason for weird function behavior when changing the slider or moving the function around??
				// Comments from penline object's checkHitAndMove() Touch.ended block:
				// // important: touch can start and end when interacting with other UI elements like a set's slider.
				// // so, double check that this touch was still on top of the penline object, not, say, on a slider.

				// check if the function came out of any parent function's area, and went into another function
				GameObject[] functions = GameObject.FindGameObjectsWithTag("function");
				bool in_canvas = false;

				for (int i = 0; i < functions.Length; i++)
				{
					// is anchor ( child(0) ) inside the polygon?
					if (functions[i].GetComponent<functionLine_script>().isInsidePolygon(
						functions[i].GetComponent<functionLine_script>().transform.InverseTransformPoint(
						transform.GetChild(0).position))
						&&
						(functions[i].GetComponent<functionLine_script>().abstraction_layer == 1 ||
						functions[i].GetComponent<functionLine_script>().abstraction_layer == 2)
						)
					{
						transform.parent = functions[i].transform;
						in_canvas = true;

						// save log
						// save coordinates wrt parent center
						if (abstraction_layer == 1)
						{
							string tkey = functions[i].name + "|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
							if (!saved_positions.ContainsKey(tkey))
								saved_positions.Add(tkey, (Vector2)(transform.position - functions[i].transform.position));
						}

						break;
					}
				}

				// if none of the sets or functions contain it, then it should be a child of the paintable canvas
				if (!in_canvas)
				{
					transform.parent = paintable_object.transform;
					// in case it was invisible, make it visible
					parent_asked_to_lower_layer = false;

					// save log
					string tkey = "paintable|" + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");
					if (!saved_positions.ContainsKey(tkey))
						saved_positions.Add(tkey, (Vector2) transform.position);
				}
			}
		}
	}

	public void menuButtonClick(GameObject radmenu, Button btn, int buttonNumber)
	{
		if (buttonNumber == 2)
		{
			btn.onClick.AddListener(() => initiateDoubleFunction(radmenu));
		}
		else if (buttonNumber == 3)
		{
			btn.onClick.AddListener(() => unlinkPenChildren(radmenu));
		}
		else if (buttonNumber == 4)
		{
			btn.onClick.AddListener(() => flattenAll(radmenu));
		}
		else if (buttonNumber == 5)
		{
			btn.onClick.AddListener(() => exitMenu(radmenu));
		}
		else if (buttonNumber == 6)
		{
			btn.onClick.AddListener(() => createOOPmenu(radmenu));
		}
		else if (buttonNumber == 7)
		{
			btn.onClick.AddListener(() => createCategoryFiltermenu(radmenu));
		}
	}

	public void setPropertyAsCount(GameObject radmenu)
	{
		current_property = Properties.count;
		// Destroy the radial menu
		Destroy(radmenu);
	}

	public void setPropertyAsCapacity(GameObject radmenu)
	{
		current_property = Properties.capacity;
		// Destroy the radial menu
		Destroy(radmenu);
	}

	public void exitMenu(GameObject radmenu)
	{
		menu_name = "";
		// set the operator/function
		string op = radmenu.transform.Find("_operator_textbox").GetComponent<TMP_InputField>().text;

		// take the first character only
		if (op.Length > 1) op = op[0].ToString();
		// if not an operator character, then default back to +
		if (op.IndexOfAny("+-*/".ToCharArray()) == -1) op = "+";
		// validate the operator if this function is a double function
		op = validateOperatorForDoubleFunction(op);

		// set the current operator to new operator
		if (op == "+") current_op = opPlus;
		else if (op == "*") current_op = opMult;
		else if (op == "/") current_op = opDiv;
		else if (op == "-") current_op = opMinus;

		// update argument_label
		transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = currentOperator();

		// update the value of the current_feature based on new operation
		updateFeature();

		// retain the abstraction slider value
		retained_slider_value = (int)abs_slider.value;

		// Update the category filter: done in individual category buttons

		// Update the _top_TeX function name, if provided
		user_given_function_name = radmenu.transform.Find("_name_textbox").GetComponent<TMP_InputField>().text;

		// Destroy the radial menu
		Destroy(radmenu);
	}

	public void createOOPmenu(GameObject radmenu)
	{

		if (oop_properties_submenu == null)
		{
			oop_properties_submenu = Instantiate(OOP_properties_submenu_prefab,
				radmenu.transform.Find("oop_properties").transform.position + new Vector3(0, -40, 0),
				Quaternion.identity, radmenu.transform);

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().source_object = this.gameObject;

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().paint_canvas = paintable_object;

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().propertyEdgePos =
				oop_properties_submenu.transform.position + new Vector3(0, 0, 0);

			oop_properties_submenu.transform.Find("_num_elements").GetComponent<Button>().onClick.AddListener(
				() => oop_properties_submenu.transform.Find("_num_elements").GetComponent<CreatePropertyEdge>().onClick());
		}
		else
		{
			Destroy(oop_properties_submenu);
		}
	}

	public void createCategoryFiltermenu(GameObject radmenu)
	{

		if (category_filter_submenu == null)
		{
			category_filter_submenu = Instantiate(category_filter_submenu_prefab,
				radmenu.transform.Find("category_filter").transform.position + new Vector3(0, -40, 0),
				Quaternion.identity, radmenu.transform);

			category_filter_submenu.GetComponent<CategoryFilterMenuBehavior>().parent_function = this.gameObject;

			// initiate the buttons with current function, set the onClick() events, leave out the last button
			for (int i = 0; i < category_filter_submenu.transform.childCount - 1; i++)
			{
				//category_filter_submenu.transform.GetChild(i).GetComponent<Button>().onClick.AddListener(
				//() => category_filter_submenu.transform.GetChild(i).GetComponent<CategoryFilterMenuButtonBehavior>().onClick());

				category_filter_submenu.transform.GetChild(i).GetComponent<CategoryFilterMenuButtonBehavior>().parent_function = this.gameObject;
			}
		}
		else
		{
			// Before destroying, save the category filter: done in CategoryFilterMenuBehavior.onDestroy()

			Destroy(category_filter_submenu);
		}
	}

	public void flattenOne(GameObject radmenu)
	{
		// Go through all children and extract all pen, set, and function objects. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			transform.GetChild(i).SetParent(paintable_object.transform);
		}

		// Then destroy the current set and menu
		Destroy(radmenu);
		Destroy(this.gameObject);
	}

	public void unlinkPenChildren(GameObject radmenu)
	{
		// Go through all children and extract all pen, set, and function objects. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "set")
			{
				transform.GetChild(i).GetComponent<setLine_script>().unlinkPenChildren(null);
			}
			else if (transform.GetChild(i).tag == "function")
			{
				transform.GetChild(i).GetComponent<functionLine_script>().unlinkPenChildren(null);
			}
		}

		// Then destroy the current set and menu
		if(radmenu != null)	Destroy(radmenu);
	}

	public void unlinkAllChildren()
	{
		// Go through all children and unlink to paint canvas. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "set")
			{
				transform.GetChild(i).GetComponent<setLine_script>().unlinkAllChildren();
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "function")
			{
				transform.GetChild(i).GetComponent<functionLine_script>().unlinkAllChildren();
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
		}
	}

	public void flattenAll(GameObject radmenu)
	{
		// Go through all children and extract all pen objects. 
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).SetParent(paintable_object.transform);
			}
			else if (transform.GetChild(i).tag == "set")
			{
				// "recursive" call to flatten the children set
				transform.GetChild(i).GetComponent<setLine_script>().flattenSet(null);
			}
			else if (transform.GetChild(i).tag == "function")
			{
				// "recursive" call to flatten the children function
				transform.GetChild(i).GetComponent<functionLine_script>().flattenAll(null);
			}
		}

		// Then destroy the current set and menu
		if (radmenu != null) Destroy(radmenu);  // check null as a parent function might pass a null menu
		Destroy(this.gameObject);
	}

	public void deleteFunction(GameObject radmenu)
	{
		menu_name = "";

		// retain the abstraction slider value
		retained_slider_value = (int)abs_slider.value;

		// Destroy the radial menu
		Destroy(radmenu);
		// Destroy the set attached to this script
		Destroy(this.gameObject);
	}

	public void initiateDoubleFunction(GameObject radmenu)
	{
		menu_name = "";

		// set the operator/function
		string op = radmenu.transform.Find("_operator_textbox").GetComponent<TMP_InputField>().text;

		// take the first character only
		if (op.Length > 1) op = op[0].ToString();
		// if not an operator character, then default back to +
		if (op.IndexOfAny("+-*/".ToCharArray()) == -1) op = "+";
		// validate the operator if this function is a double function
		op = validateOperatorForDoubleFunction(op);

		// set the current operator to new operator
		if (op == "+") current_op = opPlus;
		else if (op == "*") current_op = opMult;
		else if (op == "/") current_op = opDiv;
		else if (op == "-") current_op = opMinus;

		// is this already a double function? Then first reset the operands, if any operand exists.
		if (is_this_a_double_function)
		{
			resetOperands();
		}

		// We create a hold on the live update for this function until two operands are identified by the user
		halt_live_update_for_double_function = true;
		is_this_a_double_function = true;
		
		// retain the abstraction slider value
		retained_slider_value = (int)abs_slider.value;

		Destroy(radmenu);
	}

	public void assignOperand(GameObject operand)
	{
		if (halt_live_update_for_double_function)
		{
			if (operand_count == 0)
			{
				operand_count++;
				first_operand = operand;
				if (first_operand.tag == "penline")
				{
					first_operand.GetComponent<penLine_script>().is_this_double_function_operand = true;
					// mark the first operand with their operand order label (argument_label)
					first_operand.transform.GetChild(3).gameObject.SetActive(true);
					first_operand.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = "1";
				}

				else if (first_operand.tag == "set")
				{
					first_operand.GetComponent<setLine_script>().is_this_double_function_operand = true;
					// mark the first operand with their operand order label (argument_label)
					// first_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = "1";
					first_operand.transform.GetComponent<setLine_script>().double_function_operand_order = 1;
				}

				else if (first_operand.tag == "function")
				{
					first_operand.GetComponent<functionLine_script>().is_this_double_function_operand = true;
					// mark the first operand with their operand order label (argument_label)
					// first_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = "1";
					first_operand.transform.GetComponent<functionLine_script>().double_function_operand_order = 1;
				}

			}
			else if (operand_count >= 1)
			{
				operand_count++;
				second_operand = operand;

				if (second_operand.tag == "penline")
				{
					second_operand.GetComponent<penLine_script>().is_this_double_function_operand = true;
					// mark the first operand with their operand order label (argument_label)
					second_operand.transform.GetChild(3).gameObject.SetActive(true);
					second_operand.transform.GetChild(3).GetChild(0).GetComponent<TextMeshProUGUI>().text = "2";
				}

				else if (second_operand.tag == "set")
				{
					second_operand.GetComponent<setLine_script>().is_this_double_function_operand = true;
					// mark the first operand with their operand order label (argument_label)
					// second_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = "2";
					second_operand.transform.GetComponent<setLine_script>().double_function_operand_order = 2;
				}

				else if (second_operand.tag == "function")
				{
					second_operand.GetComponent<functionLine_script>().is_this_double_function_operand = true;
					// mark the first operand with their operand order label (argument_label)
					// second_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = "2";
					first_operand.transform.GetComponent<functionLine_script>().double_function_operand_order = 2;
				}

				// resume normal operation
				halt_live_update_for_double_function = false;
			}
		}
	}

	public void onAbstractionLayerChange()
	{
		/*
		 * Function abstraction layers:
		 * 0: turn off everything
		 * 1: everything in this function is visible except legible layer, children are also visible
		 * 2: turn off drawn objects + sets + other functions, display legible layer 1
		 * 3: turn off drawn objects + legible layer, display symbolic expression
		 * 
		 * Slider can have value between [1, 3], abs. layer = 0 can only be set and revoked by parent
		*/

		// first check if parent asked to raise the layer, and override layering by abstraction slider if that's the case
		if (parent_asked_to_lower_layer) abstraction_layer = 0;
		// otherwise, check the slider value
		else if (abs_slider != null) abstraction_layer = (int)abs_slider.value;
		// the slider doesn't exist (b/c menu doesn't exist), check the retained slider value
		else abstraction_layer = retained_slider_value;

		// If there was a change, then act. Otherwise no point updating every frame.
		if (abstraction_layer - prev_abstraction_layer != 0)
		{
			// 0, turn off everything
			if (abstraction_layer == 0)
			{
				// turn off _anchor, _text, _TeX, and _meshobj mesh renderers
				// _anchor -- all of it
				transform.GetChild(0).gameObject.SetActive(false);
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;    // sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj, turn them off 
				for (int i = 2; i < transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = true;
					}
					else if (transform.GetChild(i).tag == "set")
					{
						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = true;
					}
					else if (transform.GetChild(i).tag == "function")
					{
						transform.GetChild(i).GetComponent<functionLine_script>().parent_asked_to_lower_layer = true;
					}
				}
			}

			// abstraction layer 1: everything is visible, except legible layer (TeXDraw textbox)
			if (abstraction_layer == 1)
			{
				// _anchor
				transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
				// argument_label
				transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
				// _anchor text
				transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().enabled = true;
				// _TeX
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled = false;
				// _top_TeX
				transform.GetChild(0).GetChild(3).GetComponent<TEXDraw>().enabled = true;
				transform.GetChild(0).GetChild(3).transform.position = transform.TransformPoint(points[HighestMeshPointIndex()]);
				transform.GetChild(0).GetChild(3).transform.localPosition += _top_TeX_upper_local_position_increment;
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = true;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;    // sprite instead of mesh
				}

				// go through the children except the function's _anchor and _meshobj 
				for (int i = 2; i < transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = false;
					}
					else if (transform.GetChild(i).tag == "set")
					{
						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = false;
					}
					else if (transform.GetChild(i).tag == "function")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<functionLine_script>().parent_asked_to_lower_layer = false;
					}
				}

			}

			// 2 & 3: turn off drawn objects + sets + other functions, display legible layer 1 or legible layer 2.
			else if (abstraction_layer == 2)
			{
				// _anchor
				transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
				// argument_label
				transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
				// _anchor text
				transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().enabled = true;
				// _TeX
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled = true;
				// _top_TeX
				transform.GetChild(0).GetChild(3).GetComponent<TEXDraw>().enabled = true;
				transform.GetChild(0).GetChild(3).transform.position = transform.TransformPoint(points[HighestMeshPointIndex()]);
				transform.GetChild(0).GetChild(3).transform.localPosition += _top_TeX_upper_local_position_increment;
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = true;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = true;    // sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj, turn them off 
				for (int i = 2; i < this.gameObject.transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = true;
					}

					else if (transform.GetChild(i).tag == "set")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = true;
					}

					else if (transform.GetChild(i).tag == "function")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<functionLine_script>().parent_asked_to_lower_layer = true;
					}
				}
			}

			else if (abstraction_layer == 3)
			{
				// _anchor
				transform.GetChild(0).gameObject.SetActive(true);
				transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
				transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
				// argument_label
				transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
				// _anchor text
				transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().enabled = true;
				// _TeX
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().enabled = false;
				// _top_TeX
				transform.GetChild(0).GetChild(3).GetComponent<TEXDraw>().enabled = true;
				transform.GetChild(0).GetChild(3).transform.position = transform.TransformPoint(points[LowestMeshPointIndex()]);
				transform.GetChild(0).GetChild(3).transform.localPosition += _top_TeX_lower_local_position_increment;
				// _meshobj
				transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
				// _stroke_converted_mesh (if it exists)
				if (transform.GetChild(1).childCount > 0)
				{
					if (transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>() != null)
						transform.GetChild(1).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
					else
						transform.GetChild(1).GetChild(0).GetComponent<SpriteRenderer>().enabled = false;    // sprite instead of mesh
				}

				// go through the children except the set's _anchor and _meshobj 
				for (int i = 2; i < this.gameObject.transform.childCount; i++)
				{
					if (transform.GetChild(i).tag == "penline")
					{
						transform.GetChild(i).GetComponent<penLine_script>().parent_asked_to_change_layer = true;
					}

					else if (transform.GetChild(i).tag == "set")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<setLine_script>().parent_asked_to_lower_layer = true;
					}

					else if (transform.GetChild(i).tag == "function")
					{
						// TODO: SET BOX COLLIDER TO FALSE SO THAT THINGS CAN'T BE DRAGGED ACCIDENTALLY OR INVISIBLY?

						transform.GetChild(i).GetComponent<functionLine_script>().parent_asked_to_lower_layer = true;
					}
				}
			}

			prev_abstraction_layer = abstraction_layer;

			// save to log
			abstraction_log.Add(transform.name + "|" +
							DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"), abstraction_layer);
		}
	}

	public void updateFeature()
	{
		// evaluate the function bubble, including all children inside

		// after the feature updates are done, apply the function at the current level
		// update the symbolic expression for this function too
		
		dynamic val;
		if (current_op == opPlus || current_op == opMinus)
			val = 0;
		// for cummulative multiplication, need 1 as a starting value
		else
		{
			// if no children, then multiplication should give 0.
			if (transform.childCount > 2)	// counting for _anchor and _meshobj
				val = 1;
			else
				val = 0;
		}

		symbolic_expression = new Symbol("");
		legible1 = new Symbol("");

		//Debug.Log("starting symbolic: " + symbolic_expression.ToString());

		// DOUBLE FUNCTION
		if (is_this_a_double_function && !halt_live_update_for_double_function)
		{
			float first = 0, second = 0;
			MathObject firstm = new Symbol(""), secondm = new Symbol("");
			MathObject firstleg = new Symbol(""), secondleg = new Symbol("");

			if (first_operand.tag == "penline")
			{
				first = first_operand.GetComponent<penLine_script>().current_attribute;
				firstm = first_operand.GetComponent<penLine_script>().symbol_name;
				firstleg = new Symbol("\\" + first_operand.GetComponent<penLine_script>().gestureTemplate);
			}
			else if (first_operand.tag == "function")
			{
				first = first_operand.GetComponent<functionLine_script>().current_feature;
				firstm = first_operand.GetComponent<functionLine_script>().symbolic_expression;
				firstleg = first_operand.GetComponent<functionLine_script>().legible1;
			}
			else if (first_operand.tag == "set")
			{
				first = first_operand.GetComponent<setLine_script>().current_feature;
				firstm = first_operand.GetComponent<setLine_script>().symbol_name;
				firstleg = first_operand.GetComponent<setLine_script>().gestureTEX1;
			}

			if (second_operand.tag == "penline")
			{
				second = second_operand.GetComponent<penLine_script>().current_attribute;
				secondm = second_operand.GetComponent<penLine_script>().symbol_name;
				secondleg = new Symbol("\\" + second_operand.GetComponent<penLine_script>().gestureTemplate);
			}
			else if (second_operand.tag == "function")
			{
				second = second_operand.GetComponent<functionLine_script>().current_feature;
				secondm = second_operand.GetComponent<functionLine_script>().symbolic_expression;
				secondleg = second_operand.GetComponent<functionLine_script>().legible1;
			}
			else if (second_operand.tag == "set")
			{
				second = second_operand.GetComponent<setLine_script>().current_feature;
				secondm = second_operand.GetComponent<setLine_script>().symbol_name;
				secondleg = second_operand.GetComponent<setLine_script>().gestureTEX1;
			}

			// check for some exceptions, like divide by zero, any operand == infinity, etc.
			if (second == 0 && currentOperator() == "/") current_feature = double.PositiveInfinity;
			else if (second == double.PositiveInfinity || first == double.PositiveInfinity) current_feature = double.PositiveInfinity;
			else current_feature = current_op(first, second);

			//if(double.IsInfinity(current_feature))

			// symbolic and legible
			symbolic_expression = assembleTwoSymbols(firstm, secondm, false, currentOperator());
			legible1 = assembleTwoSymbols(firstleg, secondleg, false, currentOperator());

			// don't go on to the next portion
			return;
		}

		// Debug.Log("am I crossing?");

		// FOR SUMMATION AND PRODUCT (MULTIPLE ARGUMENTS, NOT DOUBLE)
		for (int i = 0; i < transform.childCount; i++)
		{
			// apply the function and collect result so far
			if (transform.GetChild(i).name.Contains("Function_"))
			{
				// check for infinity from any division done in the children
				if (transform.GetChild(i).GetComponent<functionLine_script>().current_feature == double.PositiveInfinity)
				{
					val = double.PositiveInfinity;
				}
				else
				{
					// While loading a sketch, hierarchical functions may have the dynamic variable "current_feature" value as null
					// Take appropriate measures for it.
					//if (transform.GetChild(i).GetComponent<functionLine_script>().current_feature == null)
						//transform.GetChild(i).GetComponent<functionLine_script>().current_feature = 0f;
					val = current_op(val, (float) transform.GetChild(i).GetComponent<functionLine_script>().current_feature);
				}

				if (symbolic_expression.ToString() == "")
				{
					symbolic_expression = transform.GetChild(i).GetComponent<functionLine_script>().symbolic_expression;    //copy first expr.
					legible1 = transform.GetChild(i).GetComponent<functionLine_script>().legible1;
				}
				else
				{
					symbolic_expression = assembleTwoSymbols(symbolic_expression,
						transform.GetChild(i).GetComponent<functionLine_script>().symbolic_expression,
						true,
						currentOperator()
						);

					legible1 = assembleTwoSymbols(legible1,
						transform.GetChild(i).GetComponent<functionLine_script>().legible1,
						true,
						currentOperator()
						);

				}

			}
			else if (transform.GetChild(i).name.Contains("Set_"))
			{
				// evaluate the children set
				val = current_op(val, transform.GetChild(i).GetComponent<setLine_script>().current_feature);

				// for the first element, don't add an operator in the expression
				if (symbolic_expression.ToString() == "")
				{
					symbolic_expression = transform.GetChild(i).GetComponent<setLine_script>().symbol_name;
					legible1 = transform.GetChild(i).GetComponent<setLine_script>().gestureTEX1;
				}
				else
				{
					symbolic_expression = assembleTwoSymbols(symbolic_expression, 
						transform.GetChild(i).GetComponent<setLine_script>().symbol_name, false, currentOperator());

					legible1 = assembleTwoSymbols(legible1,
						transform.GetChild(i).GetComponent<setLine_script>().gestureTEX1, false, currentOperator());
				}
			}

			else if (transform.GetChild(i).tag == "penline")
			{
				// get the pen line attribute
				val = current_op(val, transform.GetChild(i).GetComponent<penLine_script>().current_attribute);

				// for the first element, don't add an operator in the expression
				if (symbolic_expression.ToString() == "")
				{
					symbolic_expression = transform.GetChild(i).GetComponent<penLine_script>().symbol_name;
					legible1 = new Symbol("\\" + transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate);
				}
				else
				{
					symbolic_expression = assembleTwoSymbols(symbolic_expression,
						transform.GetChild(i).GetComponent<penLine_script>().symbol_name, false, currentOperator());

					legible1 = assembleTwoSymbols(legible1,
						new Symbol("\\" + transform.GetChild(i).GetComponent<penLine_script>().gestureTemplate), false, currentOperator());
				}
			}
		}

		// update the current feature
		current_feature = val;
		// evaluate the symbolic mathobject expression

	}

	public void updateAnchorLabel()
	{
		// double function operand order
		//if (is_this_double_function_operand) order_string = "^" + double_function_operand_order.ToString() + " ";
		//else order_string = "";

		// update the text on the anchor
		if (abstraction_layer == 3)
		{
			// show symbol
			this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().text = toLatexExpression(symbolic_expression);
		}
		else if (abstraction_layer == 2)
		{
			// show value of the selected property
			this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().text = toLatexExpression(symbolic_expression);
		}
		else if (abstraction_layer == 1)
		{
			// show value of the selected property
			this.gameObject.transform.GetChild(0).GetChild(0).GetComponent<TEXDraw>().text = current_feature.ToString("F0");
		}
	}

	public void updateTopTeXLabel()
	{
		transform.GetChild(0).GetChild(3).GetComponent<TEXDraw>().text = constructTopTeXString();
	}

	public string currentOperator()
	{
		if (current_op == opPlus) return "+";
		else if (current_op == opMult) return "*";
		else if (current_op == opMinus) return "-";
		else if (current_op == opDiv) return "/";
		else return ",";
	}

	public MathObject assembleTwoSymbols(MathObject sym1, MathObject sym2, bool includeParenthesis, string op)
	{
		if (!includeParenthesis)
		{
			if (op == "+")
				return sym1 + sym2;
			else if (op == "-")
				return sym1 - sym2;
			else if (op == "*")
				return sym1 * sym2;
			else if (op == "/")
				return sym1 / sym2;
			else
				return sym1;    // need this default case, otherwise c# complains
		}
		else
		{
			if (op == "+")
				return sym1 + sym2;
			else if (op == "-")
				return sym1 - sym2;
			else if (op == "*")
				return sym1 * sym2;
			else if (op == "/")
				return sym1 / sym2;
			else
				return sym1;    // need this default case, otherwise c# complains
			/*
			if (op == "+")
				return sym1 + (sym2);
			else if (op == "-")
				return sym1 - (sym2);
			else if (op == "*")
				return sym1 * (sym2);
			else if (op == "/")
				return sym1 / (sym2);
			else
				return sym1;    // need this default case, otherwise c# complains
			*/
		}
	}

	public void updateLegibleLayer()
	{
		// update TeXDraw legible layer
		// show as full expression, and also as a unit.
		// i.e., full expression = 2 triangle / 4 line, and unit = 0.5 triangle/line.

		/*
		MathObject rationalized = legible2.RationalizeExpression();
		string legible2_numerator = rationalized.Numerator().ToString();
		string legible2_denominator = rationalized.Denominator().ToString();

		//Debug.Log(legible2_numerator);
		*/

		// format the text to limit each line to 10 characters, for terms after operations +, -, * etc. 
		// anchor is 0, _TeX is 1
		if (abstraction_layer == 2)
		{
			transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text = toLatexExpression(legible1);
		}
		/*
		else if (abstraction_layer == 3)
		{
			if(legible2_denominator == "")	// if no denominator then just show the numerator
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text =
					Convert.ToString(current_feature) + " [" + legible2_numerator + "]";
			else
				transform.GetChild(0).GetChild(1).GetComponent<TEXDraw>().text =
					Convert.ToString(current_feature) + " [\frac{" + legible2_numerator + "}{" + legible2_denominator + "}]";
		}
		*/
	}

	public string toLatexExpression(MathObject mathexp)
	{
		// simple expression parser to get rid of * sign and use \frac{}{} for / sign.
		//char[] delimiterChars = { '*', '\\' };

		string lexp = mathexp.StandardForm();

		string[] terms = lexp.Split('/');
		if (terms.Length <= 1)  // no / sign.
			return String.Join("", mathexp.Numerator().AlgebraicExpand().StandardForm().Split('*'));	// clean up * signs.
		else if (terms.Length == 2)
		{
			// TODO: This might not work for more than one fraction. Need a proper parsing and expression tree. Or a proper recursion.
			/*
			lexp = "";
			for (int i = 0; i < terms.Length; i++)
			{
				lexp += "\\frac{" + terms[i] + "}{" + terms[i + 1] + "}";	// index outside bounds exception. Fix. When doing y/x.
			}
			return lexp;
			*/
			string toreturn = "\\frac{" + mathexp.Numerator().AlgebraicExpand().StandardForm() + "}{" + mathexp.Denominator().StandardForm() + "}";
			return String.Join("", toreturn.Split('*'));    // clean up * signs.
		}
		else
			return lexp;

	}

	public string parseFractionString(string hterm)
	{
		// recursive fraction parsing
		if (hterm.Split('/').Length == 2)
		{
		
		}
		return hterm;
	}

	public void checkIfDoubleFunctionArgumentsAreStillThere()
	{
		// if live updating is halted, then don't do anything, updateFeature won't be checked in Update() anyway. But this helps when loading objects from file.
		if (halt_live_update_for_double_function) return;

		if (first_operand != null)
		{
			if (first_operand.transform.parent != transform)
			{
				// not a double function anymore
				is_this_a_double_function = false;
				// reset the current operator to +
				current_op = opPlus;

				resetOperands();
			}
		}

		if (second_operand != null)
		{
			if (second_operand.transform.parent != transform)
			{
				// not a double function anymore
				is_this_a_double_function = false;
				// reset the current operator to +
				current_op = opPlus;

				resetOperands();
			}
		}
	}

	public void resetOperands()
	{
		// reset the first_operand and second_operand game objects
		if (first_operand != null)
		{
			if (first_operand.tag == "penline")
			{
				first_operand.transform.GetChild(4).gameObject.SetActive(false);
				first_operand.GetComponent<penLine_script>().is_this_double_function_operand = false;
				// reset first operand
				first_operand = null;
			}

			// if operand is set 
			else if (first_operand.tag == "set")
			{
				first_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
					first_operand.GetComponent<setLine_script>().this_set_name;

				first_operand.GetComponent<setLine_script>().is_this_double_function_operand = false;
				// reset first operand
				first_operand = null;
			}

			// if operand is function 
			else if (first_operand.tag == "function")
			{
				first_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
					first_operand.GetComponent<functionLine_script>().currentOperator();

				first_operand.GetComponent<functionLine_script>().is_this_double_function_operand = false;
				// reset first operand
				first_operand = null;
			}
		}

		if (second_operand != null)
		{
			if (second_operand.tag == "penline")
			{
				second_operand.transform.GetChild(4).gameObject.SetActive(false);
				second_operand.GetComponent<penLine_script>().is_this_double_function_operand = false;
				// reset second operand
				second_operand = null;
			}

			else if (second_operand.tag == "set")
			{
				second_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
					second_operand.GetComponent<setLine_script>().this_set_name;

				second_operand.GetComponent<setLine_script>().is_this_double_function_operand = false;
				// reset first operand
				second_operand = null;
			}

			else if (second_operand.tag == "function")
			{
				second_operand.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text =
					second_operand.GetComponent<functionLine_script>().currentOperator();

				second_operand.GetComponent<functionLine_script>().is_this_double_function_operand = false;
				// reset first operand
				second_operand = null;
			}
		}

		operand_count = 0;
	}

	public string validateOperatorForDoubleFunction(string op)
	{
		if (!is_this_a_double_function)
		{
			// - and / can't be there if this is not a double function
			if (op.IndexOfAny("-/".ToCharArray()) > -1)
			{
				return "+";
			}
			else return op;
		}
		else
			return op;
	}

	// keep checking every frame if it came out from a double function lasso or if it's still a child of it.
	public void checkIfThisIsPartOfDoubleFunction()
	{
		if (is_this_double_function_operand)
		{
			if (transform.parent.tag == "function" && transform.parent.GetComponent<functionLine_script>().is_this_a_double_function)
			{
				// keep it true
				is_this_double_function_operand = true;
			}
			else
			{
				// turn off double function bool
				is_this_double_function_operand = false;
				transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<TextMeshProUGUI>().text = currentOperator();
			}
		}
	}

	public void applyGlobalStrokeDetails()
	{
		// act only if visible, and if function line is finished drawing
		// both _anchor and _meshobj need to be there for function to be finished drawing
		if ((abstraction_layer == 1 || abstraction_layer == 2) && transform.childCount >= 2)   
		{

			bool anchor_state = details_dropdown.GetComponent<DropdownMultiSelect>().transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(0).GetComponent<Toggle>().isOn;
			bool mesh_state = details_dropdown.GetComponent<DropdownMultiSelect>().transform.GetChild(1).GetChild(0).GetChild(1).GetChild(0).GetChild(1).GetComponent<Toggle>().isOn;

			if (anchor_state != global_details_on_anchor)
			{
				// change needed
				if (!anchor_state)
				{
					hideAnchorAndLegible();
					global_details_on_anchor = false;
				}
				else if (anchor_state)
				{
					showAnchorAndLegible();
					global_details_on_anchor = true;
				}
			}

			if (mesh_state != global_details_on_mesh)
			{
				// change needed
				if (!mesh_state)
				{
					hideMesh();
					global_details_on_mesh = false;
				}
				else if (mesh_state)
				{
					showMesh();
					global_details_on_mesh = true;
				}
			}
		}
	}

	public void hideAnchorAndLegible()
	{
		transform.GetChild(0).gameObject.SetActive(false);
	}

	public void showAnchorAndLegible()
	{
		transform.GetChild(0).gameObject.SetActive(true);
	}

	public void hideMesh()
	{
		transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
	}

	public void showMesh()
	{
		transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
	}

	public int HighestMeshPointIndex()
	{
		// which local point is the highest? Return its index.
		// find maxy from all the points
		float mxy = -100000f;
		int ind = -1;
		for (int i = 0; i < points.Count; i++)
		{
			if (points[i].y > mxy)
			{
				mxy = points[i].y;
				ind = i;
			}
		}
		return ind;
	}

	public int LowestMeshPointIndex()
	{
		// which local point is the highest? Return its index.
		// find maxy from all the points
		float mny = 100000f;
		int ind = -1;
		for (int i = 0; i < points.Count; i++)
		{
			if (points[i].y < mny)
			{
				mny = points[i].y;
				ind = i;
			}
		}
		return ind;
	}

	public bool CheckCategoryFilterCriteria(GameObject obj)
	{
		if (obj.tag == "set" && categoryFilter.set)
		{
			return true;
		}

		if (obj.tag == "penline")
		{
			string gestureType = obj.GetComponent<penLine_script>().gestureTemplate;

			if (gestureType == "legcircle" && categoryFilter.circle) return true;
			else if (gestureType == "legrectangle" && categoryFilter.box) return true;
			else if (gestureType == "legrectangle" && categoryFilter.figure) return true;	// treat figure as a box
			else if (gestureType == "leglegvertellipse" && categoryFilter.ellipse) return true;
			else if (gestureType == "legrightarrow" && categoryFilter.arrow) return true;
			else if (gestureType == "leghorzellipse" && categoryFilter.horizontal_ellipse) return true;
			else if (gestureType == "legtriangle" && categoryFilter.triangle) return true;
			else return false;
		}

		return false;
	}

	public void updateChildrenMembershipFromCategoryFilter()
	{
		for (int i = transform.childCount - 1; i > 1; i--)
		{
			if (transform.GetChild(i).tag == "set")
			{
				transform.GetChild(i).GetComponent<setLine_script>().checkAndUpdateMembership();
			}
			else if (transform.GetChild(i).tag == "penline")
			{
				transform.GetChild(i).GetComponent<penLine_script>().checkAndUpdateMembership();
			}
		}
	}

	public string constructTopTeXString()
	{
		string operand_order = "";
		string funcname = "";

		if (is_this_double_function_operand && transform.parent.tag == "function")
		{
			// am I the first or second operand? check with the parent
			if (transform.parent.GetComponent<functionLine_script>().first_operand != null &&
				transform.parent.GetComponent<functionLine_script>().first_operand == this.gameObject)
			{
				operand_order = "(1)";
			}
			else if (transform.parent.GetComponent<functionLine_script>().second_operand != null &&
				transform.parent.GetComponent<functionLine_script>().second_operand == this.gameObject)
			{
				operand_order = "(2)";
			}
		}

		if (user_given_function_name == "")
		{
			funcname = this.name;
		}
		else
		{
			funcname = user_given_function_name;
		}

		// construct category filter string
		filtered_categories = "";
		if (categoryFilter.circle) filtered_categories += "\\legcircle ";
		if (categoryFilter.box) filtered_categories += "\\legrectangle ";
		if (categoryFilter.ellipse) filtered_categories += "\\legvertellipse ";
		if (categoryFilter.arrow) filtered_categories += "\\legrightarrow ";
		if (categoryFilter.horizontal_ellipse) filtered_categories += "\\leghorzellipse ";
		if (categoryFilter.triangle) filtered_categories += "\\legtriangle ";
		if (categoryFilter.figure) filtered_categories += "\\legrectangle ";

		// combine all
		return operand_order + " " + funcname + " ( " + filtered_categories + ")";
	}

	public void relayDraggingInfoToChildren()
	{
		if (draggable_now)
		{
			for (int i = 2; i < transform.childCount; i++)
			{
				if (transform.GetChild(i).tag == "penline")
				{
					transform.GetChild(i).GetComponent<penLine_script>().draggable_now = true;
				}
				else if (transform.GetChild(i).tag == "set")
				{
					transform.GetChild(i).GetComponent<setLine_script>().draggable_now = true;
					// relay to set's children
					transform.GetChild(i).GetComponent<setLine_script>().relayDraggingInfoToChildren();
				}
				else if (transform.GetChild(i).tag == "function")
				{
					transform.GetChild(i).GetComponent<functionLine_script>().draggable_now = true;
					// relay to function's children
					transform.GetChild(i).GetComponent<functionLine_script>().relayDraggingInfoToChildren();
				}
			}
			relayCount++;
		}
		else if(!draggable_now && relayCount > 0)
		{
			for (int i = 2; i < transform.childCount; i++)
			{
				if (transform.GetChild(i).tag == "penline")
				{
					transform.GetChild(i).GetComponent<penLine_script>().draggable_now = false;
				}
				else if (transform.GetChild(i).tag == "set")
				{
					transform.GetChild(i).GetComponent<setLine_script>().draggable_now = false;
					// relay to set's children
					transform.GetChild(i).GetComponent<setLine_script>().relayDraggingInfoToChildren();
				}
				else if (transform.GetChild(i).tag == "function")
				{
					transform.GetChild(i).GetComponent<functionLine_script>().draggable_now = false;
					// relay to function's children
					transform.GetChild(i).GetComponent<functionLine_script>().relayDraggingInfoToChildren();
				}
			}

			relayCount = 0;
		}
	}

	public void UpdateActionHistory()
	{
		// keep count of children regardless of action history enabled, will help build prev snapshot
		//current_child_count = transform.childCount;

		if (transform.childCount < 2) return;	// allow drawing to finish

		UpdateChildrenHistory();

		if (Paintable_Script.ActionHistoryEnabled)
		{
			if (prevChildrenHistory.Count != ChildrenHistory.Count)
			{
				Debug.Log("here");

				// find out what's added or missing
				GameObject change;
				if (prevChildrenHistory.Count > ChildrenHistory.Count)
				{
					change = prevChildrenHistory.Except(ChildrenHistory).ToList()[0];
				}
				else
					change = ChildrenHistory.Except(prevChildrenHistory).ToList()[0];

				if (change != null)
				{
					Debug.Log(change.name);

					Sprite changesnap = TakeSnapshot(change);
					current_snapshot = TakeSnapshot();

					// Add an item to action history panel with the snapshots

					// which operator is it?
					string op = "";
					if (prevChildrenHistory.Count > ChildrenHistory.Count)
					{
						op = "-";
					}
					else
					{
						// if all children are lists of same size, then it's either multiplication or division 
						bool isdivmult = true;
						for (int i = 2; i < transform.childCount; i++)
						{
							if (transform.GetChild(i).tag != "set") { isdivmult = false; break; }
						}

						// if all children are not lists or are not of the same size, then it's addition
						if (!isdivmult) op = "+";
						else
						{
							// multiplication or division?

						}
					}


					// update the action history panel with a new item
					GameObject actionhist = GameObject.Find("ActionHistory");
					//GameObject listtocopy = actionhist.transform.GetChild(0).GetChild(0).GetChild(1).gameObject;
					GameObject listtocopy = Resources.Load<GameObject>("Prefabs/ActionHistory/Item");
					GameObject newitem = Instantiate(listtocopy, actionhist.transform.GetChild(0).GetChild(0).transform);

					// customize the newitem with proper images and operator
					newitem.transform.Find("first_operand").GetComponent<Image>().sprite = prev_snapshot;
					newitem.transform.Find("second_operand").GetComponent<Image>().sprite = changesnap;
					newitem.transform.Find("result").GetComponent<Image>().sprite = current_snapshot;
					newitem.transform.Find("operator").GetComponent<TextMeshProUGUI>().text = op;

					actionhist.transform.GetChild(0).GetComponent<ScrollRect>().verticalNormalizedPosition = -0.5f; // forces to 0, goes all the way down.
				}
			}
		}

		//prev_child_count = current_child_count;
		prevChildrenHistory.Clear();
		prevChildrenHistory.AddRange(ChildrenHistory);
		prev_snapshot = TakeSnapshot();
	}

	public Sprite TakeSnapshot()
	{
		// snapshot of current hierarchy
		RuntimePreviewGenerator.PreviewDirection = new Vector3(0, 0, 1);
		RuntimePreviewGenerator.BackgroundColor = new Color(0.3f, 0.3f, 0.3f, 0f);
		RuntimePreviewGenerator.OrthographicMode = true;

		Sprite action = Sprite.Create(RuntimePreviewGenerator.GenerateModelPreview(transform, 128, 128)
			, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 20f);
		//GameObject.Find("Action").GetComponent<Image>().sprite = action;
		return action;
	}

	public Sprite TakeSnapshot(GameObject obj)
	{
		RuntimePreviewGenerator.PreviewDirection = new Vector3(0, 0, 1);
		RuntimePreviewGenerator.BackgroundColor = new Color(0.3f, 0.3f, 0.3f, 0f);  // fully transparent
		RuntimePreviewGenerator.OrthographicMode = true;

		Sprite action = Sprite.Create(RuntimePreviewGenerator.GenerateModelPreview(obj.transform, 128, 128)
			, new Rect(0, 0, 128, 128), new Vector2(0.5f, 0.5f), 20f);
		//GameObject.Find("Action").GetComponent<Image>().sprite = action;
		return action;
	}

	public void UpdateChildrenHistory()
	{
		ChildrenHistory.Clear();

		for (int i = 2; i < transform.childCount; i++)
		{
			ChildrenHistory.Add(transform.GetChild(i).gameObject);
		}
	}

	// Start is called before the first frame update
	void Awake()
    {
		// assign current operator
		current_op = opPlus;

		symbolic_expression = new Symbol(symbolic_expr);

		details_dropdown = GameObject.Find("Details_Dropdown");

		// Assign default category filter
		categoryFilter.set = true; categoryFilter.box = true; categoryFilter.figure = true;
		categoryFilter.arrow = false; categoryFilter.circle = false; categoryFilter.ellipse = false;
		categoryFilter.horizontal_ellipse = false; categoryFilter.triangle = false;
	}

    // Update is called once per frame
    void Update()
    {
		// before everything, check if arguments are missing in double function. That determines how updateFeature() will work

		checkIfDoubleFunctionArgumentsAreStillThere();

		if (!halt_live_update_for_double_function)
		{
			updateFeature();
		}

		checkIfThisIsPartOfDoubleFunction();

		checkHitAndMove();
		//relayDraggingInfoToChildren();
		onAbstractionLayerChange();

		updateAnchorLabel();
		updateLegibleLayer();
		updateTopTeXLabel();

		// optional: action history update. comment out if action history is not needed. takes thumbnail snapshot every frame.
		//UpdateActionHistory();

		// apply global stroke details layer, at the end.
		applyGlobalStrokeDetails();
	}
}
